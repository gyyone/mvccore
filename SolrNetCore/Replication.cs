﻿using Newtonsoft.Json;
using System.Linq;

namespace SolrNetCore
{
    public class Replication
    {
        [JsonProperty("details")]
        public ReplicationDetail details { set; get; }
    }
    public class ResponseHeader
    {
        public int status { set; get; }
        public int QTime { set; get; }
    }
    public class ResponseRestore
    {
        public ResponseHeader responseHeader { set; get; }
        public RestoreStatus restorestatus { set; get; }
    }
    public class RestoreStatus
    {
        public string snapshotName { set; get; }
        public string status { set; get; }
    }
}
