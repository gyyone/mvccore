﻿using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace SolrNetCore
{
    public class CloudCores:SolrBase
    {
        public CloudCores(SolrAuth solrAuth) : base(solrAuth)
        {
        }
        public void ClearAllCore()
        {
            List<string> corelist = ListCollectionCore();
            foreach(var core in corelist)
            {
                DeleteCore(core);
            }
        }
        public RestApiResponse Create(string corename, int numShards = 1, int replicationFactor = 1, int maxShardsPerNode = 6)
        {
            //return DeleteCore(corename);
            if (ListCollectionCore().Contains(corename))
            {
                return new RestApiResponse() { Error = "" };
            }
            string postapi = string.Format("{0}/solr/admin/collections?action={1}&name={2}&numShards={3}&replicationFactor={4}&maxShardsPerNode={5}", _SolrAuth.HostUrl, "CREATE", corename, numShards, replicationFactor, maxShardsPerNode);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            return msr;
        }          

        public RestApiResponse Rename(string corename, string newName)
        {
            string postapi = string.Format("{0}/solr/admin/collections?action={1}&core={2}&other={3}", _SolrAuth.HostUrl, "RENAME", corename, newName);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            return msr;
        }

        public RestApiResponse DeleteCore(string corename)
        {
            if (!ListCollectionCore().Contains(corename))
            {
                return new RestApiResponse() { Error = "" };
            }
            string postapi = string.Format("{0}/solr/admin/collections?action={1}&name={2}&wt=json", _SolrAuth.HostUrl, "DELETE", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            return msr;
        }
        public List<string> ListCollectionCore()
        {
            try
            {
                string postapi = string.Format("{0}/solr/admin/collections?action={1}&awt=json", _SolrAuth.HostUrl, "LIST");
                RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                ListCollection temp = JsonConvert.DeserializeObject<ListCollection>(result.Result);
                return temp.collections;
            }
            catch(Exception ex)
            {
                return new List<string>();
            }
     

        }
    }


}
