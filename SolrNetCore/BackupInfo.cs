﻿using Newtonsoft.Json;

namespace SolrNetCore
{
    public class BackupInfo
    {
        [JsonProperty("status")]
        public string status { set; get; }
        [JsonProperty("fileCount")]
        public long fileCount { set; get; }
        [JsonProperty("snapshotName")]
        public string snapshotName { set; get; }
    }
}
