﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestApi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;


namespace SolrNetCore
{
    public class Doc : SolrBase
    {
        public Doc(SolrAuth solrauth) : base(solrauth)
        {
        }
        public ResponseDoc Commit(string coreName)
        {
            string postapi = string.Format("{0}/solr/{1}/update/json/docs?commit=true", _SolrAuth.HostUrl, coreName);

            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            return base.HandleResponseDoc(msr);
        }
        public RestApiResponse Insert(string coreName, List<Dictionary<string, object>> updatevalue)
        {
            string postapi = string.Format("{0}/solr/{1}/update/json/docs", _SolrAuth.HostUrl, coreName);
            RestApiResponse msr = ServerApi.Post(postapi, updatevalue, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;
        }
        public RestApiResponse Insert(string coreName, Dictionary<string, object> updatevalue)
        {          
            string postapi = string.Format("{0}/solr/{1}/update/json/docs", _SolrAuth.HostUrl, coreName);
            RestApiResponse msr = ServerApi.Post(postapi, updatevalue, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;
        }
        public RestApiResponse Update(string coreName, Dictionary<string,object> updatevalue)
        {

            Dictionary<string, object> setvalue = new Dictionary<string, object>();
            foreach (var key in updatevalue)
            {
                if (key.Key.Equals("id"))
                {
                    setvalue.Add(key.Key, key.Value.ToString());
                }
                else
                {
                    setvalue.Add(key.Key, new Dictionary<string, object>() { { "set", key.Value } });
                }

            }

            string postapi = string.Format("{0}/solr/{1}/update?wt=json", _SolrAuth.HostUrl, coreName);
       
            RestApiResponse msr = ServerApi.Post(postapi,new List<Dictionary<string, object>>() { setvalue }, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;
     
        }

        public RestApiResponse Clear(string coreName)
        {
            string postapi = string.Format("{0}/solr/{1}/update", _SolrAuth.HostUrl, coreName);
            //string deletedidstr = "<add><delete>" + string.Join("OR ", deleteid.Select(x => "<query>id:" + x + "</query>")) + "</delete></add>";
            //XmlDocument xml = new XmlDocument();
            //xml.LoadXml(deletedidstr);
            Dictionary<string, Dictionary<string, string>> deleteDic = new Dictionary<string, Dictionary<string, string>>();
            deleteDic["delete"] = new Dictionary<string, string>();
            deleteDic["delete"]["query"] = "*:*";
             

            RestApiResponse msr = ServerApi.Post(postapi, deleteDic, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;
        }
        public RestApiResponse Delete(string coreName, List<string> deleteid)
        {
            string postapi = string.Format("{0}/solr/{1}/update", _SolrAuth.HostUrl, coreName);
            //string deletedidstr = "<add><delete>" + string.Join("OR ", deleteid.Select(x => "<query>id:" + x + "</query>")) + "</delete></add>";
            //XmlDocument xml = new XmlDocument();
            //xml.LoadXml(deletedidstr);
            Dictionary<string, Dictionary<string, string>> deleteDic = new Dictionary<string, Dictionary<string, string>>();
            deleteDic["delete"] = new Dictionary<string, string>();
            deleteDic["delete"]["query"] = "id:(" + string.Join(" OR ", deleteid) + ")";


            RestApiResponse msr = ServerApi.Post(postapi, deleteDic, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;
        }

        public RestApiResponse Delete(string coreName, string deleteid)
        {     
            return Delete(coreName, new List<string>() { deleteid});
        }

      
    }

}
