﻿using Newtonsoft.Json;

namespace SolrNetCore
{
    public class SolrSystem
    {
        [JsonProperty("mode")]
        public string Mode { set; get; }
        [JsonProperty("solr_home")]
        public string SolrHome { set; get; }
    }
    public class PingStatus
    {
        [JsonProperty("status")]
        public string Status { set; get; }
        [JsonProperty("QTime")]
        public int QTime { set; get; }
    }
}
