﻿using Newtonsoft.Json;

namespace SolrNetCore
{
    public class SolrField
    {
        public SolrField()
        {
            indexed = true;
            stored = true;
            docValues = false;
            sortMissingFirst = false;
            sortMissingLast = false;
            multiValued = false;
            //omitNorms = true;
            useDocValuesAsStored = true;
            large = false;
        }
        public string name { set; get; }
        public string type { set; get; }
        public bool stored { set; get; }
        public bool indexed { set; get; }
        [JsonIgnore]
        public bool docValues { set; get; }
        public bool sortMissingFirst { set; get; }
        public bool sortMissingLast { set; get; }
        public bool multiValued { set; get; }
        [JsonProperty("default")]
        public object default_value { set; get; }
        //public bool omitNorms { set; get; }
        //public bool omitTermFreqAndPositions { set; get; }
        public bool required { set; get; }
        public bool useDocValuesAsStored { set; get; }
        public bool large { set; get; }

    }
    public class AddField: SolrField
    {

    }
    public class DelField
    {
        public string name { set; get; }
    }
    public class ReplaceField:SolrField
    {
     
    }




}
