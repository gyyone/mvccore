﻿using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Util;
using System.Threading.Tasks;
using RestApi;

namespace SolrNetCore
{
    public class Log
    {
        public DateTime LogTime { set; get; }
        public string Text { set; get; }
    }
    public partial class SolrApi
    {
   
        public Cores Core { set; get; }
        public Fields Field { set; get; }
        public Doc Document { set; get; }
        public Query DocQuery { set; get; }
        public SolrAuth SolrAuthentication { set; get; }
        SolrSystem SystemInfo { set; get; }
        public static bool IsCoreUp(SolrAuth auth, string corename)
        {
            try
            {
                string postapi = string.Format($"{auth.HostUrl}/solr/{corename}/admin/ping");
                RestApiResponse msr = ServerApi.Post(postapi, null, auth.Authentication);
                if(msr == null || msr.Result == null)
                {
                    return false;
                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                PingStatus temp = JsonConvert.DeserializeObject<PingStatus>(result.Result);
               
                return true;
            }
            catch
            {

            }
            return false;
        }
        public static bool IsSorUp(SolrAuth auth)
        {
            try
            {
                string postapi = string.Format("{0}/solr/admin/info/system", auth.HostUrl);
                RestApiResponse msr = ServerApi.Post(postapi, null, auth.Authentication);
                if (msr == null || msr.Result == null)
                {
                    return false;
                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                SolrSystem temp = JsonConvert.DeserializeObject<SolrSystem>(result.Result);
                return true;
            }
            catch
            {

            }
            return false;
        }
        public SolrSystem GetSystemInfo()
        {
            string postapi = string.Format("{0}/solr/admin/info/system", SolrAuthentication.HostUrl);
            RestApiResponse msr = ServerApi.Post(postapi, null, SolrAuthentication.Authentication);
           Task<string> result = msr.Result.Content.ReadAsStringAsync();
            SolrSystem temp = JsonConvert.DeserializeObject<SolrSystem>(result.Result);

            return temp;
        }
        public void SetUser(string username,string password)
        {      
            string postapi = string.Format("{0}/solr/admin/authentication", SolrAuthentication.HostUrl);
            Dictionary<string, Dictionary<string, string>> param = new Dictionary<string, Dictionary<string, string>>();
            param["set-user"] = new Dictionary<string, string>();
            //Set default password
            param["set-user"][username] = password;
            RestApiResponse msr = ServerApi.Post(postapi, param,SolrAuthentication.Authentication);
        }
        public void DeleteUser(string username, string password)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", SolrAuthentication.HostUrl);
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param["delete-user"] = new List<string>();
            //Set default password
            param["delete-user"].Add(username);
            RestApiResponse msr = ServerApi.Post(postapi, param, SolrAuthentication.Authentication);
        }
        public void SetUserRole(string username, List<string> roles)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", SolrAuthentication.HostUrl);
            Dictionary<string,Dictionary<string,List<string>>> param = new Dictionary<string, Dictionary<string,List<string>>>();
            param["set-user-role"] = new Dictionary<string, List<string>>();
            //Set default password
            param["set-user-role"].Add(username, roles);
            RestApiResponse msr = ServerApi.Post(postapi, param, SolrAuthentication.Authentication);
        }

        public void SetPermission(string username,string permissions,string role )
        {
            string postapi = string.Format("{0}/solr/admin/authentication", SolrAuthentication.HostUrl);

            Dictionary<string, Dictionary<string, string>> param = new Dictionary<string, Dictionary<string, string>>();
            param["set-permission"] = new Dictionary<string, string>();
            //Set default password
            param["set-permission"].Add("name", permissions);
            param["set-permission"].Add("role", role);
            RestApiResponse msr = ServerApi.Post(postapi, param, SolrAuthentication.Authentication);
        }
        public SolrApi()
        {

        }
        //https://lucene.apache.org/solr/guide/7_4/collections-api.html#collections-api
        public SolrApi(SolrAuth solrAuth)
        {
            SolrAuthentication = solrAuth;
            SystemInfo = GetSystemInfo();
            solrAuth.SolrHome = SystemInfo.SolrHome;
            string securitypath = solrAuth.SolrHome + "/security.json";
            if (!File.Exists(securitypath))
            {
                File.Copy(AppDomain.CurrentDomain.BaseDirectory + "/Files/security.json", securitypath);
                SetUser("admin", "admin");
            }          
            Core = new Cores(solrAuth);
            Field = new Fields(solrAuth);
            Document = new Doc(solrAuth);
            DocQuery = new Query(solrAuth);

        }
        public void Commit()
        {
            foreach (var core in Core.ListCollectionCore())
            {
                bool commited = false;
                while(!commited)
                {
                    try
                    {
                     
                        Commit(core);
                        commited = true;

                    }
                    catch
                    {
                        commited = false;                                           
                        System.Threading.Thread.Sleep(10000);
                    }
                }
          
            }
      
        }
        public void Commit(string coreName)
        {
            Document.Commit(coreName);
        }
        public void Commit(List<string> coreList)
        {
            foreach(var core in coreList)
            {
               Commit(core);
            }
        }
      
        public const string SystemDateFormat = "yyyy.MM.dd.HHmmssfff";
        public const string SystemDateFormatUi = "yyyy.MM.dd.HHmmss000";
        public static string GetTimeVersion()
        {
            return DateTime.Now.ToString(SystemDateFormat);
        }

    }

 
}
