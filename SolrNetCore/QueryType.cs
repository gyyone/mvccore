﻿namespace SolrNetCore
{
    public enum QueryType
    {       
        RangeQuery,
        TextQuery,
        ExactMatch,
        CustomMatch,

    }

}
