﻿using RestApi;

namespace SolrNetCore
{
    public class SolrAuth
    {
        public SolrAuth()
        {
            HostUrl = "http://localhost:8983";
            Authentication = new ApiAuth();
            SolrUser = "solr";
            SolrGroup = "solr";
        }
        public string HostUrl { set; get; }
        public string SolrHome { set; get; }
        public string SolrUser { set; get; }
        public string SolrGroup { set; get; }
        public ApiAuth Authentication { set; get; }
  
    }


}
