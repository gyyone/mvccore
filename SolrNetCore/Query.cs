﻿using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.Linq;


namespace SolrNetCore
{
    public class Query : SolrBase
    {
        public Query(SolrAuth solrAuth) : base(solrAuth)
        {
        }
        public ResponseDetail SelectById(string coreName, string id, List<string> showResult = null, string facet = "", int rows = int.MaxValue, int start = 0)
        {
            return SelectById(coreName,new List<string>() { id},showResult,facet,rows,start);
        }
        public ResponseDetail SelectDistinct(string coreName, string field,string text,ulong rows = 10000)
        {
            string postapi = $"{_SolrAuth.HostUrl}/solr/{coreName}/select?q={field}:*{text}*&rows={rows}&group=true&group.field={field}&group.main=true&fl={field}";
       
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            var t = msr.Result.Content.ReadAsStringAsync();
            ResponseDoc temp = JsonConvert.DeserializeObject<ResponseDoc>(t.Result);
            return temp.response;
        }

        public ResponseDetail SelectById(string coreName, List<string> idlist, List<string> showResult = null, string facet = "", int rows = int.MaxValue, int start = 0)
        {
            RestApiResponse msgresponse = GetById(coreName, idlist, new List<string>(), facet, rows, start);
            if (!string.IsNullOrEmpty(msgresponse.Error))
            {
                throw new System.Exception(msgresponse.Error);
            }
            else
            {
                var t = msgresponse.Result.Content.ReadAsStringAsync();
                ResponseDoc temp = JsonConvert.DeserializeObject<ResponseDoc>(t.Result);
                return temp.response;
            }
        }
        public ResponseDetail Select(string coreName, SearchTerm searchTerm, List<string> showResult, string facet = "", int rows = int.MaxValue, int start = 0,bool orQuery = false,string bq= "")
        {
            return Select(coreName,new List<SearchTerm>() { searchTerm}, showResult, facet, rows,start, orQuery,bq:bq);

        }
        public ResponseDetail Select(string coreName, List<SearchTerm> searchTerm, List<string> showResult, string facet = "", int rows = int.MaxValue - 1, int start = 0,bool orQuery =false,string bq="")
        {   
            RestApiResponse msgresponse =  Get(coreName, searchTerm, showResult, facet, rows, start, orQuery,bq:bq );
            if (!string.IsNullOrEmpty(msgresponse.Error))
            {
                throw new System.Exception(msgresponse.Error);
            }
            else
            {
                var t = msgresponse.Result.Content.ReadAsStringAsync();
                ResponseDoc temp = JsonConvert.DeserializeObject<ResponseDoc>(t.Result);
                return temp.response;
            }       
        }

        public ResponseDetail CustomQuery(string coreName,string query, List<string> showResult, string facet = "", int rows = int.MaxValue - 1, int start = 0)
        {
            string postapi = string.Format("{0}/solr/{1}/select", _SolrAuth.HostUrl, coreName);
            QueryItem qitem = new QueryItem();
            qitem.q = query;
            //Console.WriteLine($"query : {qitem.q} Table :{coreName}");
            qitem.facet = !string.IsNullOrEmpty(facet);
            qitem.facetJson = facet;
            int maxrowsAccept = int.MaxValue - start;
            qitem.rows = Math.Min(rows, maxrowsAccept);
            qitem.start = start;    
            if (showResult != null && showResult.Count > 0)
            {
                qitem.fl = string.Join(",", showResult);
            }  
            RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, QueryItem>() { { "params", qitem } }, _SolrAuth.Authentication);
            if (!string.IsNullOrEmpty(msr.Error))
            {
                throw new System.Exception(msr.Error);
            }
            else
            {
                var t = msr.Result.Content.ReadAsStringAsync();
                ResponseDoc temp = JsonConvert.DeserializeObject<ResponseDoc>(t.Result);
                return temp.response;
            }

        }



        private RestApiResponse GetById(string coreName, List<string> idlist, List<string> showResult, string facet = "", int rows = 10, int start = 0)
        {
            List<SearchTerm> searchTerm = new List<SearchTerm>();
            foreach(var id in idlist)
            {
                SearchTerm s = new SearchTerm();
                s.FieldId = "id";
                s.search = idlist;
                s.queryType = QueryType.ExactMatch;
                searchTerm.Add(s);
            }
            return Get(coreName, searchTerm,new List<string>(),facet,rows,start);

        }

        private  RestApiResponse Get(string coreName,List<SearchTerm> searchTerm, List<string> showResult,string facet="",int rows = int.MaxValue, int start = 0,bool orQuery = false,string bq = "")
        {
            string postapi = string.Format("{0}/solr/{1}/select", _SolrAuth.HostUrl, coreName);
            QueryItem qitem = new QueryItem();
            qitem.defType = "edismax";
            qitem.bq = bq;
            qitem.q = GetQuery(searchTerm, orQuery);
            //Console.WriteLine( $"query : {qitem.q} Table :{coreName}");
            qitem.facet = !string.IsNullOrEmpty(facet);
            qitem.facetJson = facet;
            int maxrowsAccept = int.MaxValue - start;
            qitem.rows = Math.Min(rows, maxrowsAccept);
            qitem.start = start;
            if(showResult != null && showResult.Count > 0)
            {
                qitem.fl = string.Join(",", showResult.Select(x=> $"{x}"));
            }         
            qitem.sort = GetSort(searchTerm);

            RestApiResponse msr = ServerApi.Post(postapi,new Dictionary<string, QueryItem>() { { "params", qitem } }, _SolrAuth.Authentication);

            return msr;
        }
        private string GetSort(List<SearchTerm> searchTerm)
        {
            List<string> queryList = new List<string>();
            foreach (var s in searchTerm)
            {
                if(!string.IsNullOrEmpty(s.Sort))
                {
                    queryList.Add(string.Format("{0} {1}", s.FieldId, s.Sort));
                }
                
            }
            return string.Join(",", queryList);
        }
        private string GetQuery(List<SearchTerm> searchTerm,bool OrField=false)
        {         
            List<string> queryList = new List<string>();
            foreach(var s in searchTerm)
            {
                s.search = s.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                if (!s.search.Any())
                {
                    continue;
                }
                switch (s.queryType)
                {
                    case QueryType.ExactMatch:
                        List<string> q_exact = s.search.Select(x => string.Format("{0}:\"{1}\"", s.FieldId, x)).ToList();
                        queryList.Add("(" + string.Join(" OR ", q_exact) + ")");
                        break;
                    case QueryType.RangeQuery:
                        while (s.search.Count <= 2)
                        {
                            s.search.Add("*");
                        }
                        queryList.Add(string.Format("{0}:[{1} TO {2}]", s.FieldId, s.search[0] == "*" || string.IsNullOrWhiteSpace(s.search[0]) ? "*": $"\"{s.search[0]}\"", s.search[1] == "*" || string.IsNullOrWhiteSpace(s.search[1]) ?"*" : $"\"{s.search[1]}\""));
                        break;
                    case QueryType.TextQuery:
                        List<string> q = s.search.Select(x => string.Format("{0}:{1}", s.FieldId, x)).ToList();
                        queryList.Add("(" + string.Join(" OR ", q) + ")");                  
                        break;
                    case QueryType.CustomMatch:
                        List<string> qregex = s.search.Select(x => string.Format("{0}:{1}", s.FieldId, x)).ToList();
                        queryList.Add("(" + string.Join(" OR ", qregex) + ")");
                        break;
                }
            }
            if(queryList.Count == 0)
            {
                queryList.Add("*:*");
            }
            if(OrField)
            {
                return string.Join(" OR ", queryList);
            }
            
            return string.Join(" AND ", queryList);
        }
    }

}
