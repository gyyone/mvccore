﻿using LogHelper;
using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolrNetCore
{
    public class Fields : SolrBase
    {
      
        public static List<string> typeCollection = new List<string>()
        {
                "ancestor_path",
                 "binary",
                 "boolean",
                 "booleans",
                 "delimited_payloads_float",
                 "delimited_payloads_int",
                 "delimited_payloads_string",
                 "descendent_path",
                 "location",
                 "location_rpt",
                 "lowercase",
                 "pdate",
                 "pdates",
                 "pdouble",
                 "pdoubles",
                 "pfloat",
                 "pfloats",
                 "phonetic_en",
                 "pint",
                 "pints",
                 "plong",
                 "plongs",
                 "point",
                 "random",
                 "string",
                 "strings",
                 "text_ar",
                 "text_bg",
                 "text_ca",
                 "text_cjk",
                 "text_cz",
                 "text_da",
                 "text_de",
                 "text_el",
                 "text_en",
                 "text_en_splitting",
                 "text_en_splitting_tight",
                 "text_es",
                 "text_eu",
                 "text_fa",
                 "text_fi",
                 "text_fr",
                 "text_ga",
                 "text_gen_sort",
                 "text_general",
                 "text_general_rev",
                 "text_gl",
                 "text_hi",
                 "text_hu",
                 "text_hy",
                 "text_id",
                 "text_it",
                 "text_ja",
                 "text_lv",
                 "text_nl",
                 "text_no",
                 "text_pt",
                 "text_ro",
                 "text_ru",
                 "text_sv",
                 "text_th",
                 "text_tr",
                 "text_ws"
            };
   
        public Fields(SolrAuth solrAuth) : base(solrAuth)
        {
        }

        public RestApiResponse AddField(string corename, string fieldname, string type, bool stored, object defaultvalue,bool multivalue)
        {
            if(fieldname.ToLower().Equals("id"))
            {
                return new RestApiResponse();
            }
            defaultvalue = defaultvalue ?? "";
            defaultvalue = defaultvalue.ToString();
            if (!GetFields(corename).Where(x => x.name.Equals(fieldname, StringComparison.OrdinalIgnoreCase)).Any())
            {
                
                string postapi = string.Format("{0}/solr/{1}/schema", _SolrAuth.HostUrl, corename);

                RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, AddField>() { { "add-field", new AddField() { name = fieldname, type = type, stored = stored, default_value = defaultvalue.ToString(),multiValued= multivalue } } },_SolrAuth.Authentication);
                base.HandleResponse(msr);             
                return msr;
            }
            else if(GetFields(corename).Where(x => x.name.Equals(fieldname, StringComparison.OrdinalIgnoreCase) && (x.type != type || x.stored != stored || x.multiValued != multivalue || x.default_value != defaultvalue) ).Any()
                )
            {           
                var res=  ReplaceField(corename, fieldname, type, stored, defaultvalue,multivalue);
                
                return res;
            }
            else
            {
                return new RestApiResponse();
            }
 
        }

        public RestApiResponse DeleteField(string corename, string fieldname)
        {
            string postapi = string.Format("{0}/solr/{1}/schema", _SolrAuth.HostUrl, corename);

            RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, DelField>() { { "delete-field", new DelField() { name = fieldname } } }, _SolrAuth.Authentication);
            
            base.HandleResponse(msr);
            return msr;
        }

        public RestApiResponse ReplaceField(string corename, string fieldname, string type, bool stored, object defaultValue,bool multivalue)
        {
            string postapi = string.Format("{0}/solr/{1}/schema", _SolrAuth.HostUrl, corename);
            List<SolrField> fields = GetFields(corename);
            SolrField field = fields.Where(x => x.name == fieldname).FirstOrDefault();
            if (field == null)
            {
                return new RestApiResponse() { };
            }
            if(!field.type.Equals(type) || field.stored != stored || (!field.default_value.Equals(defaultValue)&& defaultValue!=null && !string.IsNullOrWhiteSpace(defaultValue.ToString())) || field.multiValued != multivalue )
            {
                LogHandler.Debug($"Replacing Core{corename}  of Fields  {fieldname} {field.type} == {type} , {field.stored} == {stored} , {field.default_value} == '{defaultValue.ToString()}', {field.multiValued} == {multivalue}");
                RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, ReplaceField>() { { "replace-field", new ReplaceField() { name = fieldname, type = type, stored = stored, default_value = defaultValue, multiValued = multivalue } } }, _SolrAuth.Authentication);

                base.HandleResponse(msr);

                return msr;
            }
            return new RestApiResponse();
        }

        public List<SolrField> GetFields(string corename)
        {
            string postapi = string.Format("{0}/solr/{1}/schema/fields?wt=json", _SolrAuth.HostUrl, corename);

            RestApiResponse msr = ServerApi.Get(postapi,auth:_SolrAuth.Authentication);

            if(string.IsNullOrEmpty(msr.Error))
            {
                Task<string> t = msr.Result.Content.ReadAsStringAsync();
                FieldCollection collectionfield = JsonConvert.DeserializeObject<FieldCollection>(t.Result.ToString());
                return collectionfield.fields;
            }
            return new List<SolrField>() ;
        }
    }
    public class FieldCollection
    {
        public List<SolrField> fields { set; get; }
     
    }


}
