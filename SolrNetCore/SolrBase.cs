﻿using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace SolrNetCore
{
    public class SolrBase
    {
        public SolrAuth _SolrAuth { set; get; }
        public SolrBase(string hostUrl)
        {
            _SolrAuth = new SolrAuth();
            _SolrAuth.HostUrl = hostUrl;
        }
        public SolrBase(SolrAuth solrauth)
        {
            _SolrAuth = solrauth;
        }
 
        public void HandleResponse(RestApiResponse r)
        {
            if(!string.IsNullOrEmpty(r.Error))
            {
                throw new System.Exception(r.Error);
            }
            Task<string> result = r.Result.Content.ReadAsStringAsync();
            Dictionary<string, object> reply = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
            if (reply.ContainsKey("exception"))
            {
                throw new Exception(reply["exception"].ToString());
            }
        }
        public ResponseDoc HandleResponseDoc(RestApiResponse r)
        {
            if (!string.IsNullOrEmpty(r.Error))
            {
                throw new System.Exception(r.Error);
            }
            Task<string> t = r.Result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ResponseDoc>( t.Result.ToString());
        }
    }


}
