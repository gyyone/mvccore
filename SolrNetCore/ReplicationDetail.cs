﻿using Newtonsoft.Json;
using System.Linq;

namespace SolrNetCore
{
    public class ReplicationDetail
    {
        [JsonProperty("indexSize")]
        public string indexSize { set; get; }

        [JsonProperty("backup")]
        public string[] backup { set; get; }
        public string Status
        {
            get
            {
                if (backup != null)
                {
                    int index = backup.ToList().IndexOf("status");
                    if (index >= 0)
                    {
                        return backup[index + 1];
                    }
                }
                return "NA";
            }
        }
    }
}
