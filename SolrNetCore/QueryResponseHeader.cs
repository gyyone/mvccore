﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace SolrNetCore
{
    public class QueryResponseHeader
    {
        public bool zkConnected { set; get; }
        public int status { set; get; }

        public int QTime { set; get; }
        [JsonProperty("params")]
        public Dictionary<string, string> paramDic{set;get;}
        Dictionary<string, Dictionary<string, object>> facets { set; get; }
   
    }

}
