﻿using DirectoryHelper;
using Newtonsoft.Json;
using RestApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using Util;

namespace SolrNetCore
{

    public class Cores : SolrBase
    {
        public Cores(SolrAuth solrauth) : base(solrauth)
        {           
            filechecking.Add("managed-schema");
            filechecking.Add("params.json");
            filechecking.Add("protwords.txt");
            filechecking.Add("solrconfig.xml");
            filechecking.Add("stopwords.txt");
            filechecking.Add("synonyms.txt");
            UpdateCollection();
        }

        List<string> filechecking = new List<string>();
     
        public void ClearAllCore()
        {
            List<string> corelist = ListCollectionCore();
            foreach (var core in corelist)
            {
                DeleteCore(core);
            }
        }
        private void UpdateCollection()
        {
            //CoreCollection = ListCollectionCore();
        }

        public RestApiResponse Create(string corename)
        {
            //return DeleteCore(corename);
            if (ListCollectionCore().Contains(corename))
            {
                return new RestApiResponse() { Error = "" };
            }

            bool copyfile = false;
            foreach(var file in filechecking)
            {
                string path = _SolrAuth.SolrHome + "/" + corename + "/conf/" + file;
                if (!File.Exists(path))
                {
                    copyfile = true;                  
                }
            }

            if(copyfile)
            {
                string config = AppDomain.CurrentDomain.BaseDirectory + "Files/conf";
                string solrpath = _SolrAuth.SolrHome + "/" + corename;
                DirHelper.Copy(config, $"{solrpath}/conf");
               
                if(System.Runtime.InteropServices.RuntimeInformation
                                               .IsOSPlatform(OSPlatform.Linux))
                {
                    bool error;
                    List<string> output = CommandLineReader.Run($"/bin/chown", $" -R {_SolrAuth.SolrUser}:{_SolrAuth.SolrGroup} {solrpath}", out error);
                    if (error)
                    {
                        Console.WriteLine(string.Join(Environment.NewLine, output));
                    }

                }

     
            }
        

            string coreproperty = _SolrAuth.SolrHome + "/" + corename + "/core.properties";
            if (File.Exists(coreproperty))
            {
                File.Delete(coreproperty);
            }

            string postapi = string.Format("{0}/solr/admin/cores?action={1}&name={2}", _SolrAuth.HostUrl,"CREATE",corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            UpdateCollection();
            DirHelper.CreateDirectory(coreproperty);
            using (StreamWriter sw = new StreamWriter(coreproperty, false))
            {
                sw.WriteLine($"name={corename}");
            }
            return msr;
        }
        public bool BackupComplete(string corename, string backupName)
        {
            if (ListCollectionCore().Contains(corename))
            {           
                string postapi = $"{_SolrAuth.HostUrl}/solr/{corename}/replication?command=details&wt=json&name={backupName}";
                RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
                base.HandleResponse(msr);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();                
                Replication replicate = JsonConvert.DeserializeObject<Replication>(result.Result);
                return replicate.details.backup !=null && replicate.details.Status.ToLower() == "success";
            }

            return false;
        }
        public RestApiResponse Backup(string corename,string backuplocation,string backupName)
        {
            if (ListCollectionCore().Contains(corename))
            {
                string snapshot = $"{backuplocation}snapshot.{backupName}/";
                DirHelper.CreateDirectory(backuplocation);         
                if(Directory.Exists(snapshot))
                {               
                    Directory.Delete(snapshot, true);
                }
                string postapi = $"{_SolrAuth.HostUrl}/solr/{corename}/replication?command=backup&location={HttpUtility.UrlEncode(backuplocation)}&name={backupName}";
                RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
                
                base.HandleResponse(msr);
          
                return msr;
            }
            else
            {
                var rest = new RestApiResponse() { Error = "Collection Name Not Found" };
                base.HandleResponse(rest);
                return rest;
            }

        }
        public string RestoreStatus(string corename, string backupName)
        {
            if (ListCollectionCore().Contains(corename))
            {
                string postapi = $"{_SolrAuth.HostUrl}/solr/{corename}/replication?command=restorestatus&wt=json&name={backupName}";
                RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();

                ResponseRestore restorestate = JsonConvert.DeserializeObject<ResponseRestore>(result.Result);
                return restorestate.restorestatus.status;
            }

            return "fail to call status";
        }
        public RestApiResponse Restore(string corename, string backuplocation, string backupName)
        {
            if (!ListCollectionCore().Contains(corename))
            {
                Create(corename);
                UpdateCollection();
            }
            string snapshot = $"{backuplocation}snapshot.{backupName}/";   
            string postapi = $"{_SolrAuth.HostUrl}/solr/{corename}/replication?command=restore&location={backuplocation}&name={backupName}";
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            base.HandleResponse(msr);
            return msr;

        }
        public RestApiResponse Rename(string corename, string newName)
        {
            if (ListCollectionCore().Contains(corename))
            {               
              
                string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}&other={3}", _SolrAuth.HostUrl, "RENAME", corename, newName);
                RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
                string solrpath = _SolrAuth.SolrHome + "/" + corename;
                string solrPathNew = _SolrAuth.SolrHome + "/" + newName;
                var rest = Unload(newName);
                HandleResponse(rest);
                Directory.Move(solrpath, solrPathNew);              
                return Create(newName);
            }
            else
            {
                var rest = new RestApiResponse() { Error = "Collection Name Not Found" };
                base.HandleResponse(rest);
                return rest;
            }
    
        }

        public RestApiResponse Unload(string corename)
        {
            if (!ListCollectionCore().Contains(corename))
            {
                return new RestApiResponse() { Error = "" };
            }
            //string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "UNLOAD", corename);
            string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "UNLOAD", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            UpdateCollection();
            base.HandleResponse(msr);
            return msr;
        }

        public RestApiResponse Reload(string corename)
        {
            //string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "UNLOAD", corename);


            string coreproperty = _SolrAuth.SolrHome + "/" + corename + "/core.properties";
            if (File.Exists(coreproperty))
            { 
                File.Delete(coreproperty);
            }
            DirHelper.CreateDirectory(coreproperty);
            using (StreamWriter sw = new StreamWriter(coreproperty, false))
            {
                sw.WriteLine($"name={corename}");
            }
            string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "RELOAD", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            UpdateCollection();
            base.HandleResponse(msr);
            return msr;
        }

        public RestApiResponse DeleteCore(string corename)
        {
            if (!ListCollectionCore().Contains(corename))
            {
                return new RestApiResponse() { Error = "" };
            }
            //string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "UNLOAD", corename);
            string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}&deleteIndex=true&deleteDataDir=true&deleteInstanceDir=true", _SolrAuth.HostUrl, "UNLOAD", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            UpdateCollection();
       
            string solrpath = _SolrAuth.SolrHome + "/" + corename;
            if (Directory.Exists(solrpath))
            {
                Directory.Delete(solrpath, true);
            }
              

            base.HandleResponse(msr);
            return msr;
        }
        public List<string> ListCollectionCore()
        {
            string postapi = string.Format("{0}/solr/admin/cores", _SolrAuth.HostUrl);
            RestApiResponse msr = ServerApi.Post(postapi, null, _SolrAuth.Authentication);
            Task<string> result = msr.Result.Content.ReadAsStringAsync();
            ListCore temp = JsonConvert.DeserializeObject<ListCore>(result.Result);
            return temp.status.Keys.ToList();

        }
        public class ListCore
        {
            public Dictionary<string,object> status { set; get; }
        }
    }


}
