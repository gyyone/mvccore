﻿using System.Collections.Generic;

namespace SolrNetCore
{
    public class SearchTerm
    {
        public string FieldId { set; get; }
        public QueryType queryType { set; get; }
        public List<string> search { set; get; }
        public string Sort { set; get; }
        
    }

}
