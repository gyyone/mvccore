﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using Util;
using System.Linq;
using ApiService.Models;
using Newtonsoft.Json;
using ClassLib;
using JsonHelper;

namespace ApiService
{
    public static class Settings
    {
        public static string configpath = AppDomain.CurrentDomain.BaseDirectory + "config.json";
        public static void LoadSetting()
        {
            cfg =  JsonIO.Load<Config>(configpath);
            solrApi = new SolrNetCore.SolrApi(cfg.SolrAuthentication);
            Data.solrApi = solrApi;
            List<Dictionary<string, object>> tableproperty = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> fieldproperty = new List<Dictionary<string, object>>();
            dataStructure = new DBStructure();
            try
            {
                tableproperty = Settings.solrApi.DocQuery.CustomQuery(TableProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs;
                fieldproperty = Settings.solrApi.DocQuery.CustomQuery(TableProperty.FieldProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs;
            
                dataStructure.SetTablePropertyAndField(tableproperty.Select(x => Data.Table<TableProperty>.Cast(x)).ToList(), fieldproperty.Select(x => Data.Table<TableProperty.FieldProperty>.Cast(x)).ToList());
            }
            catch(Exception ex)
            {
            }

            
        }

        public static DBStructure dataStructure { set; get; }
        public static Config cfg { set; get; }
        public static SolrNetCore.SolrApi solrApi { set; get; }
        
    }

}
