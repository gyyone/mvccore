﻿using Akka.Actor;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Util;
using Newtonsoft.Json;

using ApiService.Controllers;
using ApiService.Models;
using ClassLib;
using Security;

namespace ApiService.Filter
{
    public class AuthorizeFilter : IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationFilterContext context)
        {

            try
            {

                if(context.ActionDescriptor.RouteValues["action"] == "InitializeDB")
                {
                    try
                    {
                        if(Settings.solrApi.Core.CoreCollection.Any())
                        {
                            context.Result = new ContentResult()
                            {
                                Content = "InitializeDB Fail. Database already initialized.",
                                StatusCode = 401
                            };
                        }
                
                        return;
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                    return;
                 
                }

                string token = context.HttpContext.Session.Get<string>(SessionName.Token);
                if (!string.IsNullOrWhiteSpace(token))
                {
                    return;
                }
                token = Unique.NewGuid();

                string authHeader = context.HttpContext.Request.Headers["Authorization"];
                if (authHeader != null && authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
                {
                    var aunticatestr = authHeader.Substring("Basic ".Length).Trim();
                    //System.Console.WriteLine(token);
                    var credentialstring = Encoding.UTF8.GetString(Convert.FromBase64String(aunticatestr));
                    var credentials = credentialstring.Split(':');
                    //Dont do password match here. It required case sensitive compare
                    Users user = Data.Table<Users>.Find(new Users() { UserId = credentials[0] }).FirstOrDefault();
                    //List<UserProfile_Roles> roles = user.Sub_Roles;
                    
                    if (user != null && user.UserId.ToLower() == credentials[0].ToLower() && user.Password == SimpleSHA.EncryptOneWay(credentials[1].ToString()))
                    {
                        context.HttpContext.Session.Set(SessionName.User, user);
                        context.HttpContext.Session.Set(SessionName.Token, token);
                        return;
                    }
                }

                context.Result = new ContentResult()
                {
                    Content = "You are unauthorized!",
                    StatusCode = 401
                };
            }
            catch(Exception ex)
            {
                context.Result = new ContentResult()
                {
                    Content = ex.ToString(),
                    StatusCode = 401
                };
            }
            
        }
    }



}
