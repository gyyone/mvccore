﻿using System.Collections.Generic;

namespace ApiService.Models
{
    public class DeleteItem
    {
        public string ClassId { set; get; }
        public List<string> DeleteIds { set; get; }
    }
}
