﻿namespace ApiService.Models
{
    public class DeleteItemCondition
    {
        public string ClassId { set; get; }
        public string Condition { set; get; }
    }
}
