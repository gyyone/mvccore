﻿using ApiService.Controllers;
using ClassLib;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Util;

namespace ApiService.Models
{
    public static class Data
    {
        public static SolrNetCore.SolrApi solrApi { set; get; }

        public static class Table<T> where T : new()
        {
            public static List<T> FindByList(List<T> objs, int rows = int.MaxValue, int start = 0, List<string> sort = null)
            {
                List<T> castedoutput = new List<T>();
                foreach (var o in objs)
                {
                    List<T> results = Find(o, rows, start, sort);
                    castedoutput.AddRange(results);
                }
                return castedoutput;
            }

            public static List<T> Find(T obj, int rows = int.MaxValue, int start = 0, List<string> sort = null)
            {
                Dictionary<string, object> finditem = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(obj));
                string classid = finditem["ClassID"].ToString();
                Dictionary<string, string> changed = JsonConvert.DeserializeObject<Dictionary<string, string>>(finditem["Changed"].ToString()); ;
                finditem.Remove("ClassID");
                finditem.Remove("Changed");

                Dictionary<string, object> searchTerm = new Dictionary<string, object>();
                foreach (var record in changed.Keys)
                {
                    if (record.Equals("ClassID") || record.Equals("Changed"))
                    {
                        continue;
                    }
                    List<string> output = TypeCheck.CastToList(finditem[record]);
                    if (output.Any())
                    {
                        searchTerm[record] = output;
                    }
                }
                List<string> querylist = new List<string>();

                foreach (var x in searchTerm)
                {
                    querylist.Add(x.Key + ":(" + string.Join(" OR ", TypeCheck.CastToList(x.Value)) + ")");
                }
                List<Dictionary<string, object>> results = Settings.solrApi.DocQuery.CustomQuery(classid, !querylist.Any() ? "*:*" : string.Join(" AND ", querylist), new List<string>() { "*" }, "", rows, start).docs;

                List<T> castedoutput = new List<T>();
                foreach (Dictionary<string, object> record in results)
                {
                    castedoutput.Add(Cast(record));
                }
                return castedoutput;
            }
            public static void Insert(T obj)
            {
                Dictionary<string, object> update = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(obj));
                string classid = update["ClassID"].ToString();
                Dictionary<string, string> changed = JsonConvert.DeserializeObject<Dictionary<string, string>>(update["Changed"].ToString()); ;
                update.Remove("ClassID");
                update.Remove("Changed");

                Dictionary<string, object> changevalue = new Dictionary<string, object>();
                foreach (var key in update.Keys.ToList())
                {
                    changevalue[key] = TypeCheck.CastToObject(update[key]);
                }
                //Change By Id checking
                changevalue[FieldNames.id] = update[FieldNames.id];
                Settings.solrApi.Document.Update(classid, changevalue);

            }
            public static void Update(T obj)
            {
                Dictionary<string, object> update = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(obj));
                string classid = update["ClassID"].ToString();
                Dictionary<string, string> changed = JsonConvert.DeserializeObject<Dictionary<string, string>>(update["Changed"].ToString()); ;
                update.Remove("ClassID");
                update.Remove("Changed");

                foreach (var key in update.Keys.ToList())
                {
                    if (update[key] == null || string.IsNullOrEmpty(update[key].ToString())
                        || !TypeCheck.CastToList(update[key]).Any())
                    {
                        update.Remove(key);
                    }
                }
                string query;
                List<string> querylist = new List<string>();
                if (update.ContainsKey(FieldNames.id) && update[FieldNames.id] != null && !string.IsNullOrEmpty(update[FieldNames.id].ToString()))
                {
                    querylist.Add($"id:{update[FieldNames.id]}");
                }
                else
                {
                    foreach (var unique in Settings.dataStructure.TableIdDic[classid].UniquesList)
                    {
                        List<string> queryvalue = new List<string>();
                        foreach (var row in unique.Value)
                        {

                            if (update.ContainsKey(row))
                            {
                                List<string> ouput = TypeCheck.CastToList(update[row]);
                                if (ouput.Any())
                                {
                                    queryvalue.Add(row + ":(" + string.Join(" AND ", ouput.Select(x => JsonConvert.SerializeObject(x))) + ")");
                                }

                            }
                        }
                        if (queryvalue.Any())
                        {
                            querylist.Add("(" + string.Join(" AND ", queryvalue) + ")");
                        }

                    }
                    update[FieldNames.id] = Unique.NewGuid();
                }

                query = string.Join(" OR ", querylist);
                List<Dictionary<string, object>> output = Settings.solrApi.DocQuery.CustomQuery(classid, query, new List<string>() { "*" }).docs;
                if (output.Count == 0)
                {
                    Settings.solrApi.Document.Insert(classid, update);
                }
                else
                {
                    Dictionary<string, object> findrecord = output.FirstOrDefault();

                    Dictionary<string, object> changevalue = new Dictionary<string, object>();
                    foreach (var key in update.Keys.ToList())
                    {
                        if (!findrecord.ContainsKey(key))
                        {
                            findrecord.Add(key, new object());
                        }

                        if (update.ContainsKey(key) && TypeCheck.IsMultiValue(update[key]))
                        {
                            var row = TypeCheck.CastToList(findrecord[key]);
                            var updatekey = TypeCheck.CastToList(update[key]);
                            //Data type is different then must update this field
                            if (TypeCheck.IsMultiValue(findrecord[key]))
                            {
                                //Check the value is match
                                //Remove it if does not match
                                if ((updatekey.Except(row).Any() || row.Except(updatekey).Any()))
                                {
                                    changevalue.Add(key, updatekey);
                                }
                            }
                            else
                            {
                                //Data Type Differen,just overwrite it
                                changevalue.Add(key, updatekey);
                            }
                        }
                        else
                        {
                            var row = findrecord[key].ToString();
                            //Do not update those value have no changes
                            if (update[key] != null && !row.Equals(update[key].ToString(), StringComparison.CurrentCulture))
                            {
                                changevalue.Add(key, TypeCheck.CastToObject(update[key]));
                            }
                        }
                    }
                    //Change By Id checking
                    changevalue[FieldNames.id] = findrecord[FieldNames.id];
                    Settings.solrApi.Document.Update(classid, changevalue);
                }
            }
            public static T Cast(Dictionary<string, object> row)
            {

                T data = new T();
                try
                {

                    var fieldproperty = data;
                    PropertyInfo[] propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

                    foreach (var columnid in row.Keys)
                    {
                        try
                        {
                            PropertyInfo p = propertyInfos.Where(x => x.GetCustomAttribute<JsonPropertyAttribute>() != null && x.GetCustomAttribute<JsonPropertyAttribute>().PropertyName == columnid).FirstOrDefault();
                            if (p != null && p.CanWrite)
                            {
                                if (p.PropertyType == typeof(string) || p.PropertyType == typeof(int) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                                    p.PropertyType == typeof(float) || p.PropertyType == typeof(bool))
                                {
                                    //Any Control Configure sort by numeric use this query
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = TypeCheck.CastToList(obj).FirstOrDefault();
                                    }
                                    else
                                    {
                                        text = obj.ToString();
                                    }

                                    p.SetValue(data, Convert.ChangeType(text, p.PropertyType));
                                }
                                else if(p.PropertyType == typeof(DateTime))
                                {
                                    try
                                    {
                                        p.SetValue(data, row[columnid].ToString().ConvertToDate());
                                    }
                                    catch(Exception ex)
                                    {

                                    }
                                }
                                else if (p.PropertyType.IsEnum)
                                {
                                    // int record = (int)Enum.Parse(p.PropertyType, row[column.ToString()].ToString());
                                    p.SetValue(data, Enum.Parse(p.PropertyType, row[columnid].ToString()));
                                }
                                else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                                {
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = TypeCheck.CastToList(obj).FirstOrDefault();
                                    }
                                    else
                                    {
                                        text = obj.ToString();
                                    }
                                    if (string.IsNullOrEmpty(text))
                                    {
                                        text = "{}";
                                    }

                                    p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                                }
                                else if (TypeCheck.IsMultiValueType(p.PropertyType) && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = obj.ToString();
                                    }
                                    else
                                    {
                                        text = $"[{obj.ToString()}";
                                    }
                                    if (string.IsNullOrEmpty(text))
                                    {
                                        text = "[]";
                                    }
                                    p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                                }
                                else if (p.PropertyType.GetProperties().Any())
                                {
                                    var settings = new JsonSerializerSettings();
                                    settings.Converters.Add(new StringEnumConverter { });
                                    settings.TypeNameHandling = TypeNameHandling.Auto;
                                    p.SetValue(data, JsonConvert.DeserializeObject(row[columnid].ToString(), p.PropertyType, settings));
                                }
                                else
                                {
                                    p.SetValue(data, row[columnid]);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return data;
            }

            public static Dictionary<string, object> CastObjectToDic(T data)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                PropertyInfo[] plist = data.GetType().GetProperties();
                foreach (var p in plist)
                {
                    if (p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                    {
                        continue;
                    }
                    string fieldid = p.GetCustomAttribute<JsonPropertyAttribute>() != null ? p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName : p.Name;
                    object objvalue = p.GetValue(data);
                    if(objvalue == null)
                    {
                        continue;
                    }
                    if (string.IsNullOrWhiteSpace(fieldid))
                    {
                        fieldid = p.Name;
                    }
                    if (p.Name == FieldNames.id)
                    {
                        param.Add(fieldid, objvalue);
                    }
                    else if (p.PropertyType == typeof(DateTime))
                    {
                        //Any Date version Control use this 
                        //_dataStructure.DbConnection.InsertOrUpdate();                        
                        param.Add(fieldid, ((DateTime)objvalue).ToDateVersion());
                    }
                    else if (p.PropertyType == typeof(string))
                    {
                        param.Add(fieldid, p.GetValue(data).ToString());
                    }
                    else if (p.PropertyType == typeof(int) ||
                             p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                             p.PropertyType == typeof(float))
                    {
                        //Any Control Configure sort by numeric use this query
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else if (p.PropertyType == typeof(bool))
                    {
                        //Unique must in varchar 50, and contain clean string
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
                        || p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    {
                        var settings = new JsonSerializerSettings();
                        settings.Converters.Add(new StringEnumConverter { });
                        settings.TypeNameHandling = TypeNameHandling.Auto;
                        param.Add(fieldid, JsonConvert.SerializeObject(objvalue, settings));

                    }
                    else if (p.PropertyType.IsEnum || TypeCheck.IsReserveType(p.PropertyType))
                    {     
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else
                    {
                        var settings = new JsonSerializerSettings();
                        settings.Converters.Add(new StringEnumConverter { });
                        settings.TypeNameHandling = TypeNameHandling.Auto;
                        param.Add(fieldid, JsonConvert.SerializeObject(objvalue, settings));

                    }

                }
                return param;
            }
        }
    }
}
