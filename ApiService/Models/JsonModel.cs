﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiService.Models
{
    public enum JSONCODE
    {
        Fail,
        Success,
        SessionExpired
    }

    public class JsonModel
    {
        public string Code { set; get; }
        public string Message { set; get; }
        public object Obj { set; get; }
    }
}
