﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ApiService.Models
{
    public class UpdateItemCondition: UpdateItem
    {
        public string Condition { set; get; }
    }

}
