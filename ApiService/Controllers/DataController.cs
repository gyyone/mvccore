﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Util;

using ClassLib;

namespace ApiService.Controllers
{

    [Route("[controller]")]
    public class DataController : BaseController
    {
       
        public const string ExceptionStr = "Exception";


    
        [HttpPost("UpdateList")]
        public JsonResult UpdateList([FromBody]List<UpdateItem> items)
        {
            foreach(var i in items)
            {
                JsonResult js =  Update(i);
                Dictionary<string,object> outresult = js.Value as Dictionary<string,object>;
                if(outresult.ContainsKey(ExceptionStr))
                {
                    return Json(outresult);
                }
            }
            return Json(new Dictionary<string,object>());
        }

        [HttpPost("Update")]
        public JsonResult Update([FromBody]UpdateItem item)
        {
            Dictionary<string, object> updateRecord = new Dictionary<string, object>();
            try
            {
                Users user = Request.HttpContext.Session.Get<Users>(SessionName.User);
             
                Dictionary<string, object> update = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Update);
                //Need to check permission of modify the field
                //List<string> changed = TypeCheck.CastToList(update["Changed"]);
                update.Remove("Changed");
                update.Remove("ClassID");
                foreach(var key in update.Keys.ToList())
                {
                    if(update[key] == null || string.IsNullOrEmpty(update[key].ToString())
                        || !TypeCheck.CastToList(update[key]).Any())
                    {
                        update.Remove(key);
                    }
                }
                string query;
                List<string> querylist = new List<string>();
                foreach (var unique in Settings.dataStructure.TableIdDic[item.ClassId].UniquesList)
                {
                    List<string> queryvalue = new List<string>();
                    foreach (var row in unique.Value)
                    {
                        
                        if (update.ContainsKey(row))
                        {
                            List<string> ouput = TypeCheck.CastToList(update[row]);
                            if(ouput.Any())
                            {
                                queryvalue.Add(row + ":(" + string.Join(" AND ", ouput.Select(x=> JsonConvert.SerializeObject(x))) + ")");
                            }
                
                        }                       
                    }
                    if(queryvalue.Any())
                    {
                        querylist.Add("(" + string.Join(" AND ", queryvalue) + ")");
                    }
                  
                }

                if(update.ContainsKey(FieldNames.id) && update[FieldNames.id] !=null && !string.IsNullOrEmpty(update[FieldNames.id].ToString()))
                {
                    querylist.Add($"id:{update[FieldNames.id]}");
                }
                else
                {
                    update[FieldNames.id] = Unique.NewGuid();
                }
                
                query = string.Join(" OR ", querylist);
                List<Dictionary<string, object>> output = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, query,new List<string>() { "*" }).docs;
                string parentIdid = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ParentId.ToString());
                string changeById = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ChangedBy);
                string accessRightId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.AccessRight.ToString());
                string dateVersionId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.DateVersion.ToString());
                List<Dictionary<string, object>> matchitems = new List<Dictionary<string, object>>();
                foreach (var o in output)
                {
                    bool match = true;
                    foreach (var unique in Settings.dataStructure.TableIdDic[item.ClassId].UniquesList)
                    {

                        foreach (var row in unique.Value)
                        {
                            if (update.ContainsKey(row))
                            {
                                List<string> temp = TypeCheck.CastToList(update[row]);
                                List<string> temp2 = TypeCheck.CastToList(o[row]);
                                if(temp.Except(temp2).Any() || temp2.Except(temp).Any())
                                {
                                    match = false;
                                    break;
                                }

                            }
                        }
                        if(!match)
                        {
                            break;
                        }
                    }
                    if(match)
                    {
                        matchitems.Add(o);
                    }
                    
                }


                if (matchitems.Count == 0)
                {       
                    SaveChange(user,item.ClassId, parentIdid, changeById, accessRightId, dateVersionId, update,false);
                }
                else if(matchitems.Count() >= 2)
                {
                    string uniqueKey = "Unique key conflict of following\r\n";
                    foreach(var x in  Settings.dataStructure.TableIdDic[item.ClassId].UniquesList)
                    {
                        uniqueKey +=   "Table : " + Settings.dataStructure.GetFullTablename(item.ClassId)  + " " + x.Key 
                            +" : "+ string.Join(",", x.Value.Select(y=>  Settings.dataStructure.FieldIdDic[y].GetCleanName())) + "\r\n"
                            + "query : " + query;
                    }
                    updateRecord =  new Dictionary<string, object>() { {
                           ExceptionStr, uniqueKey}};
                    return Json(updateRecord);
                }
                else
                {
                    Dictionary<string, object> findrecord = matchitems.FirstOrDefault();
                  
                    Dictionary<string, object> changevalue = new Dictionary<string, object>();
                    foreach (var key in update.Keys.ToList())
                    {
                        if (!findrecord.ContainsKey(key))
                        {
                            findrecord.Add(key, new object());
                        }

                        if (update.ContainsKey(key) && TypeCheck.IsMultiValue(update[key]))
                        {
                            var row = TypeCheck.CastToList(findrecord[key]);
                            var updatekey = TypeCheck.CastToList(update[key]);
                            //Data type is different then must update this field
                            if (TypeCheck.IsMultiValue(findrecord[key]))
                            {
                                //Check the value is match
                                //Remove it if does not match
                                if ((updatekey.Except(row).Any() || row.Except(updatekey).Any()))
                                {
                                    changevalue.Add(key, updatekey);
                                }
                            }
                            else
                            {
                                //Data Type Differen,just overwrite it
                                changevalue.Add(key, updatekey);
                            }
                        }
                        else
                        {
                            var row = findrecord[key].ToString();
                            //Do not update those value have no changes
                            if (update[key] != null && !row.Equals(update[key].ToString(), StringComparison.CurrentCulture))
                            {
                                changevalue.Add(key, TypeCheck.CastToObject(update[key]));
                            }
                        }
                    }

                    List<string> columns = GetColumn(item.ClassId, new List<string>() { "Modify", "Delete" });
                    var denialcolumn = changevalue.Keys.Except(columns).ToList();
                    if (denialcolumn.Any())
                    {
                        updateRecord =  new Dictionary<string, object>() { {
                            ExceptionStr, string.Join(",", denialcolumn.Select(x => Settings.dataStructure.FieldIdDic[x].GetCleanName())) + "is not allow to change" } };
                        return Json(updateRecord);
                    }
                    else
                    {
                        //Change By Id checking
                        changevalue[FieldNames.id] = findrecord[FieldNames.id];
                        if(update.ContainsKey(parentIdid))
                        {
                            changevalue[parentIdid] = update[parentIdid];
                        }
                        else if(findrecord.ContainsKey(parentIdid))
                        {
                            changevalue[parentIdid] = findrecord[parentIdid];
                        }
                        SaveChange(user,item.ClassId,parentIdid, changeById, accessRightId, dateVersionId, changevalue,true);
           
                    }

                }
                       
            }
            catch (Exception ex)
            {
                updateRecord = new Dictionary<string, object>() { {
                            ExceptionStr, ex.ToString()}};
            }      
            return Json(updateRecord);
        }

        private void SaveChange(Users user,string ClassId, string parentIdid, string changeById, string accessRightId, string dateVersionId, Dictionary<string, object> changevalue,bool update =false)
        {
            if (!changevalue.ContainsKey(changeById) || changevalue[changeById] == null ||
                (changevalue[changeById] != null && (string.IsNullOrWhiteSpace(changevalue[changeById].ToString()))
            || changevalue[changeById].ToString() == Id.UsersAccount.CurrentUser))
            {
                changevalue[changeById] = user.id;
            }
            else if (changevalue.ContainsKey(changeById))
            {
                changevalue[changeById] = TypeCheck.CastToList(changevalue[changeById]).FirstOrDefault();
            }

            //Date Version Checking
            if (!changevalue.ContainsKey(dateVersionId) ||
            (changevalue[dateVersionId] != null && string.IsNullOrWhiteSpace(changevalue[dateVersionId].ToString())))
            {
                changevalue[dateVersionId] = DBStructure.GetNowTimeVersion();
            }

            Dictionary<string, List<string>> accessRight = changevalue.ContainsKey(accessRightId) ? JsonConvert.DeserializeObject<Dictionary<string, List<string>>>((changevalue[accessRightId].ToString())) : new Dictionary<string, List<string>>();
            if (accessRight == null || !accessRight.Any() ||
                //Assign Access Right if empty string
                (accessRight.Count > 0 && !accessRight.Any(x => !string.IsNullOrWhiteSpace(x.Key))))
            {
                changevalue[accessRightId] = Access.GetAccess(user.id, new List<string>() { "Modify" });
            }
            else if (accessRight.ContainsKey(Id.UsersAccount.CurrentUser))
            {
                accessRight[user.id] = accessRight[Id.UsersAccount.CurrentUser];
                accessRight.Remove(Id.UsersAccount.CurrentUser);
                changevalue[accessRightId] = JsonConvert.SerializeObject(accessRight);
            }

            if (!changevalue.ContainsKey(parentIdid))
            {
                changevalue[parentIdid] = "#";
            }
            if(update)
            {
                Settings.solrApi.Document.Update(ClassId, changevalue);
            }
            else
            {
                Settings.solrApi.Document.Insert(ClassId, changevalue);
            }
            InsertTx(ClassId, changevalue);
        }

        [HttpPost("UpdateCondition")]
        public JsonResult UpdateCondition([FromBody]UpdateItemCondition item)
        {
            List<Dictionary<string, object>> updatelist = new List<Dictionary<string, object>>();
            try
            {
                Users user = Request.HttpContext.Session.Get<Users>(SessionName.User);

                Dictionary<string, object> update = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Update);
                Dictionary<string, object> condition = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Condition);
                Dictionary<string, object> changeonCondition = JsonConvert.DeserializeObject<Dictionary<string, object>>(condition["Changed"].ToString());
                //Need to check permission of modify the field
                //List<string> changed = TypeCheck.CastToList(update["Changed"]);
                update.Remove("Changed");
                update.Remove("ClassID");
                condition.Remove("Changed");
                condition.Remove("ClassID");
                foreach (var key in update.Keys.ToList())
                {
                    if (update[key] == null || string.IsNullOrEmpty(update[key].ToString())
                        || !TypeCheck.CastToList(update[key]).Any())
                    {
                        update.Remove(key);
                    }
                }
                if (update.ContainsKey(FieldNames.id))
                {
                    updatelist = new List<Dictionary<string, object>>(){ new Dictionary<string, object>() { {
                            ExceptionStr, "id is not allow in condition update"} } };
                    return Json(updatelist);
                }
                string parentIdid = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ParentId);
                string changeById = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ChangedBy);
                string accessRightId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.AccessRight);
                string dateVersionId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.DateVersion);
                string conditionquery;
                List<string> condquery = new List<string>();
                foreach (var condkey in changeonCondition.Keys)
                {
                    List<string> outputlist = TypeCheck.CastToList(condition[condkey]);
                    if(outputlist.Any() && changeonCondition.ContainsKey(condkey))
                    {
                        condquery.Add(condkey + ":(" + string.Join(" AND ", outputlist) + ")");
                    }
                  
                }
                conditionquery = string.Join(" AND ", condquery);
             
                List<Dictionary<string,object>> conditionRecord = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, conditionquery, new List<string>() { "*" },rows:30).docs;
                string uniqueQuery;
                if (conditionRecord.Count() == 0)
                {
                    return Json(updatelist);
                }
                else
                {
                    List<string> querylist = new List<string>();
                    List<List<string>> UniqueKey = new List<List<string>>();
                    foreach (var unique in Settings.dataStructure.TableIdDic[item.ClassId].UniquesList)
                    {
                        List<string> queryvalue = new List<string>();
                        UniqueKey.Add(unique.Value);
                        foreach (var row in unique.Value)
                        {                         
                            if (update.ContainsKey(row))
                            {
                                List<string> outputlist = TypeCheck.CastToList(update[row]);
                                if(outputlist.Any())
                                {
                                    queryvalue.Add(row + ":(" + string.Join(" AND ", TypeCheck.CastToList(update[row])) + ")");
                                }                               
                            }                   
                        }
                        if(queryvalue.Any())
                        {
                            querylist.Add("(" + string.Join(" AND ", queryvalue) + ")");
                        }
                        
                    }                 
                    uniqueQuery = string.Join(" OR ", querylist);                 
                    List<Dictionary<string, object>> uniquelistRecord = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, uniqueQuery, new List<string>() { "*" }).docs;

                    if(uniquelistRecord.Count == 0 && conditionRecord.Count() == 1)
                    {
                        update[FieldNames.id] = conditionRecord.FirstOrDefault()[FieldNames.id];
                        if(!update.ContainsKey(parentIdid) && conditionRecord.FirstOrDefault().ContainsKey(parentIdid))
                        {
                            update[parentIdid] = conditionRecord.FirstOrDefault()[parentIdid];
                        }
            
                        SaveChange(user, item.ClassId, parentIdid, changeById, accessRightId, dateVersionId, update,false);
               
                    }
                    else if(uniquelistRecord.Count == 0 && conditionRecord.Count() > 1)
                    {
                        SolrNetCore.ResponseDetail response;
                        //meaning update not touch any Unique Key. Safe to proceed
                        if (UniqueKey.Any(ukey=> ukey.Except(update.Keys).Count() == ukey.Count()))
                        {
                            int start = 0;
                            int processRecord = 5000;
                            do
                            {
                                response = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, conditionquery, new List<string>() { "*" }, rows: processRecord, start: start);
                                if (response.docs.Any())
                                {
                                    foreach(var record in response.docs)
                                    {
                                        var temp = update.ToDictionary(x => x.Key, y => 
                                            TypeCheck.IsMultiValue(y.Value) ? TypeCheck.CastToList(y.Value) :TypeCheck.CastToObject(y.Value)
                                            ); 

                                        temp[FieldNames.id] = record[FieldNames.id];
                                        if (!update.ContainsKey(parentIdid) && record.ContainsKey(parentIdid))
                                        {
                                            temp[parentIdid] = record[parentIdid];
                                        }
                                        //SaveChange(user, item.ClassId, parentIdid, changeById, accessRightId, dateVersionId, temp, false);
                                        updatelist.Add(temp);
                                    }                       
                                }
                                start= start  + processRecord;
                            }
                            while (response.docs.Any());
                        }
                        //Exact match Unique Key Update. Not Save to proceed
                        else if(UniqueKey.Any(ukey => ukey.Except(update.Keys).Count() == 0) )
                        {
                            updatelist = new List<Dictionary<string, object>>(){ new Dictionary<string, object>() { {
                            ExceptionStr, "Could not update duplicate record on unique field."} } };
                            return Json(updatelist);
                        }
                        else
                        {
                            // Meet Partial Unique Key
                            int start = 0;
                            int processRecord = 5000;
                          
                            Dictionary<string, List<string>> recordUniquevalues = new Dictionary<string, List<string>>();
                     
                            do
                            {
                                response = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, conditionquery, new List<string>() { "*" }, rows: processRecord, start: start);
                                if (response.docs.Any())
                                {
                                    foreach (var record in response.docs)
                                    {
                                        foreach(var u in Settings.dataStructure.TableIdDic[item.ClassId].UniquesList)
                                        {
                                            string key = "";
                                            foreach(var x in u.Value)
                                            {
                                                key += update.ContainsKey(x) ? update[x].ToString() : record[x].ToString();

                                            }                          
                                            if(!recordUniquevalues[u.Key].Contains(key))
                                            {
                                                recordUniquevalues[u.Key].Add(key);
                                            }
                                            else
                                            {
                                                updatelist = new List<Dictionary<string, object>>(){ new Dictionary<string, object>() { {
                                                ExceptionStr, "Could not update duplicate record on unique field."} } };
                                                return Json(updatelist);
                                            }
                                            
                                        }
                                        var temp = update.ToDictionary(x => x.Key, y => y.Value);
                                        temp[FieldNames.id] = record[FieldNames.id];
                                        if (!update.ContainsKey(parentIdid) && record.ContainsKey(parentIdid))
                                        {
                                            temp[parentIdid] = record[parentIdid];
                                        }

                                        updatelist.Add(temp);
                                    }
                                }
                                start = start + processRecord;
                            }
                            while (response.docs.Any());
                        }          
                    }
                }

                foreach (var record in updatelist)
                {
                    SaveChange(user, item.ClassId, parentIdid, changeById, accessRightId, dateVersionId, record,true);   
                }

            }
            catch (Exception ex)
            {
                updatelist = new List<Dictionary<string, object>>(){ new Dictionary<string, object>() { {
                            ExceptionStr, ex.ToString()} } };
            } 

            return Json(updatelist);
        }


        [HttpPost("Delete")]
        public JsonResult Delete([FromBody]DeleteItem item)
        {
            Dictionary<string, object> updateRecord = new Dictionary<string, object>();
            try
            {
                Users user = Request.HttpContext.Session.Get<Users>(SessionName.User);
                Settings.solrApi.Document.Delete(item.ClassId, item.DeleteIds);     
            }
            catch (Exception ex)
            {
                updateRecord = new Dictionary<string, object>() { {
                            ExceptionStr, ex.ToString()}};
            }
            return Json(updateRecord);
        }

        [HttpPost("DeleteCondition")]
        public JsonResult DeleteCondition([FromBody]DeleteItemCondition item)
        {
            List<Dictionary<string, object>> updatelist = new List<Dictionary<string, object>>();
            try
            {
                Users user = Request.HttpContext.Session.Get<Users>(SessionName.User);          
                Dictionary<string, object> condition = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Condition);
                Dictionary<string, object> changeonCondition = JsonConvert.DeserializeObject<Dictionary<string, object>>(condition["Changed"].ToString());
                condition.Remove("Changed");
                condition.Remove("ClassID");
               
                string parentIdid = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ParentId.ToString());
                string changeById = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.ChangedBy);
                string accessRightId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.AccessRight.ToString());
                string dateVersionId = Settings.dataStructure.GetFieldIdByName(item.ClassId, FieldNames.DateVersion.ToString());
                string conditionquery="";
                List<string> condquery = new List<string>();
                foreach (var condkey in condition.Keys)
                {
                    List<string> ouput = TypeCheck.CastToList(condition[condkey]);
                    if (changeonCondition.ContainsKey(condkey) && ouput.Any())
                    {
                        condquery.Add(condkey + ":(" + string.Join(" AND ", ouput) + ")");
                    }                   
                }
                if(condquery.Any())
                {
                    conditionquery = string.Join(" AND ", condquery);
                    List<Dictionary<string, object>> conditionRecord = Settings.solrApi.DocQuery.CustomQuery(item.ClassId, conditionquery, new List<string>() { "id" }, rows: 30).docs;
                    Settings.solrApi.Document.Delete(item.ClassId, conditionRecord.Select(x => x["id"].ToString()).ToList());
                }              


            }
            catch (Exception ex)
            {
                updatelist = new List<Dictionary<string, object>>(){ new Dictionary<string, object>() { {
                            ExceptionStr, ex.ToString()} } };
            }

            return Json(updatelist);
        }

        [HttpPost("Select")]
        public JsonResult Select([FromBody]FindItem finditem)
        {       
            Dictionary<string,object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(finditem.Search);
            Dictionary<string, string> changed = JsonConvert.DeserializeObject<Dictionary<string, string>>(temp["Changed"].ToString());
            string tableId = finditem.ClassId;
            temp.Remove("ClassID");
            temp.Remove("Changed");
            try
            {
                Dictionary<string, object> searchTerm = new Dictionary<string, object>();
                foreach(var record in changed.Keys)
                {
                    if (record.Equals("ClassID") || record.Equals("Changed"))
                    {
                        continue;
                    }
                    if (temp[record] is DateTime)
                    {
                        searchTerm[record] = ((DateTime)temp[record]).ToDateVersion();
                    }
                    else
                    {
                        List<string> output = TypeCheck.CastToList(temp[record]);
                        if (output.Any())
                        {
                            searchTerm[record] = output;
                        }
                    }

          
                }
                List<string> columnFields = GetColumn(tableId, new List<string>() { "View", "Modify", "Delete" });
                List<string> querylist = new List<string>();              
            
                foreach (var x in searchTerm)
                {
                    querylist.Add(x.Key + ":("+ string.Join(" OR ", TypeCheck.CastToList(x.Value).Select(y=> $"\"{y}\"").ToList()) + ")");                    
                }                    
                List<Dictionary<string,object>> results = Settings.solrApi.DocQuery.CustomQuery(tableId, !querylist.Any() ?"*:*" : string.Join(" AND ", querylist ) , !columnFields.Any() ? new List<string>() { "*"} : columnFields, "", finditem.Rows, finditem.Start).docs;

                return Json(results);
            }
            catch (Exception ex)
            {
                return Json(new List<Dictionary<string, object>>() {  new Dictionary<string, object>() { { ExceptionStr,ex.ToString()} }  });
            }                 
        }

        [HttpPost("SelectByList")]
        public JsonResult SelectByList([FromBody]List<FindItem> finditems)
        {
            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            List<Dictionary<string, object>> resultsList = new List<Dictionary<string, object>>();
            foreach (var item in finditems)
            {
                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Search);
                Dictionary<string, string> changed = JsonConvert.DeserializeObject<Dictionary<string, string>>(temp["Changed"].ToString());
                temp.Remove("Changed");
                temp.Remove("ClassID");
                string tableId = item.ClassId;
                try
                {
                    Dictionary<string, object> searchTerm = new Dictionary<string, object>();
                    foreach (var record in changed.Keys)
                    {
                        if (record.Equals("ClassID") || record.Equals("Changed"))
                        {
                            continue;
                        }
                        List<string> output = TypeCheck.CastToList(temp[record]);
                        if (output.Any())
                        {
                            searchTerm[record] = output;
                        }
                    }
                    List<string> columnFields = GetColumn(tableId, new List<string>() { "View", "Modify", "Delete" });
                    List<string> querylist = new List<string>();

                    foreach (var x in searchTerm)
                    {
                        querylist.Add(x.Key + ":(" + string.Join(" OR ", TypeCheck.CastToList(x.Value).Select(y => $"\"{y}\"").ToList()) + ")");
                    }
                    List<Dictionary<string, object>> results = Settings.solrApi.DocQuery.CustomQuery(tableId, !querylist.Any() ? "*:*" : string.Join(" AND ", querylist), !columnFields.Any() ? new List<string>() { "*" } : columnFields, "", item.Rows, item.Start).docs;

                    resultsList.AddRange(results);
                    
                }
                catch (Exception ex)
                {
                 
                    return Json(new List<Dictionary<string, object>>() { new Dictionary<string, object>() { { ExceptionStr, ex.ToString() } } });
                }
                
                return Json(resultsList);
            }
        
            return Json(new List<Dictionary<string, object>>());


        }

        [HttpPost("Commit")]
        public void Commit([FromBody]List<string> classIdlist)
        {
            foreach(var classid in classIdlist)
            {
                Settings.solrApi.Document.Commit(classid);
            }       
        }
       
    }
}
