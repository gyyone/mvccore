﻿using ApiService.Models;
using ClassLib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Util;

namespace ApiService.Controllers
{
    public class BaseController : Controller
    {  
        public BaseController()
        {
   
        }


        internal JsonModel ValidSession(ISession session)
        {
            var jm = new JsonModel();
            jm.Code = JSONCODE.Success.ToString();
      
            string sessiontoken = session.Get<string>(SessionName.Token);
            if (string.IsNullOrEmpty(sessiontoken))
            {
                jm.Obj = this.Url.Action("Index", "Home");
                jm.Code = JSONCODE.SessionExpired.ToString();
                return jm;
            }
            else if (session.Get<string>(SessionName.Token) == Id.DefaultToken)
            {
                jm.Code = JSONCODE.Success.ToString();
            }


            return jm;
        }

        internal List<string> GetColumn(string tableId, List<string> access)
        {
            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            return Settings.dataStructure.TableToFieldDic[tableId]
                .Select(x => x.Key).ToList();
        }

        internal void LogError(Dictionary<string, object> dic, Exception ex)
        {
            dic[ClassLib.Const.Exception] = ex.ToString();
        }
        internal void InsertTx(string tableid, string fieldId, string id, string changedby, string dateVersion, object value)
        {

            if (Settings.dataStructure.TableIdDic.ContainsKey(tableid))
            {
                Dictionary<string, object> tx = new Dictionary<string, object>();
                tx.Add(FieldNames.id.ToString(), Unique.NewGuid());
                tx.Add(TX.FieldsId.ChangedBy, changedby);
                tx.Add(TX.FieldsId.DateVersion, dateVersion);
                tx.Add(TX.FieldsId.RecordId, id);
                tx.Add(TX.FieldsId.FieldId, fieldId);
                if (TypeCheck.IsMultiValue(value))
                {
                    tx.Add(TX.FieldsId.Value, JsonConvert.SerializeObject(value));
                }
                else
                {
                    tx.Add(TX.FieldsId.Value, value);
                }

                Settings.solrApi.Document.Insert(TX.ClassID, tx);

            }

        }

        internal void InsertTx(string tableid, Dictionary<string, object> keyvalue)
        {
            foreach (var key in keyvalue.Keys.Where(x => !x.Equals(FieldNames.id)))
            {
                InsertTx(tableid, key, keyvalue[FieldNames.id].ToString(), keyvalue[Settings.dataStructure.GetFieldIdByName(tableid, FieldNames.ChangedBy)].ToString(), keyvalue[Settings.dataStructure.GetFieldIdByName(tableid, FieldNames.DateVersion)].ToString(), keyvalue[key]);
            }
        }
    }
}
