﻿using System;
using System.Collections.Generic;

namespace CoreApi
{
    public class QueryResult
    {
        public List<Dictionary<string, object>> Items { set; get; }
        public string Message { set; get; }
        public int TotalFound { set; get; }
        public int Start { set; get; }
        public bool Error { set; get; }
    }
    public class InsertOrUpdateResult    {
        public string Message { set; get; }
        public bool Error { set; get; }
    }
    public class DeleteResult
    {
        public string Message { set; get; }      
        public bool Error { set; get; }
    }
}
