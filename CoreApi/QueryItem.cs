﻿using System;
using System.Collections.Generic;

namespace CoreApi
{
   
    public class QueryItem
    {
        public QueryItem()
        {
            Start = 0;
            Limit = int.MaxValue;
            SearchTerm = new Dictionary<string, object>();
            Sort = new Dictionary<string, string>();
            TimeOut = 60;
        }
        public string TableId { set; get; }
        public Dictionary<string, object> SearchTerm { set; get; }
        public int Start { set; get; }
        public int Limit { set; get; }
        public Dictionary<string, string> Sort { set; get; }
        public List<string> Columns { set; get; }
        /// <summary>
        /// TimeOut In second
        /// </summary>
        public int TimeOut { set; get; }

        public void AddSort(string fieldid)
        {
            Sort[fieldid] = "asc";
        }
        public void AddSort(List<string> fieldids)
        {
            foreach (var fieldid in fieldids)
            {
                AddSort(fieldid);
            }
        }
        public void AddSortDesc(string fieldid)
        {
            Sort[fieldid] = "desc";
        }

        public void AddSortDesc(List<string> fieldids)
        {
            foreach(var fieldid in fieldids)
            {
                AddSortDesc(fieldid);
            }        
        }
        public void AddDateSearch(string fieldid, DateTime date)
        {
            SearchTerm[fieldid] = date;
        }
        public void AddDateSearch(string fieldid,DateTime Start, DateTime end)
        {
            SearchTerm[fieldid] = new List<object>() { Start , end};
        }
        public void AddRangeSearch(string fieldid, int Start, int end)
        {
            SearchTerm[fieldid] = new List<int>() { Start, end };
        }
        public void AddSearch(string fieldid, string text)
        {
            SearchTerm[fieldid] = text;
        }
        public void AddSearch(string fieldid, List<string> text)
        {
            SearchTerm[fieldid] = text;
        }
    }
    public class InsertOrUpdateItem
    {
        public string TableId { get; set; }
        public List<Dictionary<string,object>> updateItems { set; get; }
        public string ParentId { set; get; }

    }
    public class DeleteItem
    {
        public DeleteItem()
        {
            IdList = new List<string>();
            TimeOut = 60;
        }
        public List<string> IdList { set; get; }
        public int TimeOut { set; get; }
        public string TableId { get; set; }
    }
}
