﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;

namespace DirectoryHelper
{
    public interface IDirectoryHelper
    {
        void ChangeOwner(string filepath, string username, string group);
    }
    //public class UnixDirectory : IDirectoryHelper
    //{
    //    public void ChangeOwner(string filepath, string username, string group)
    //    {
    //        bool error;
    //        List<string> output = CommandLineReader.Run($"/bin/chown", " -R solr:solr ", out error);

    //    }
    //}
    public class WindowsDirectory : IDirectoryHelper
    {
        public void ChangeOwner(string filepath, string username, string group)
        {

        }
    }
    public static class DirHelper
    {
        public enum OSType
        {
            Windows,
            Unix,
        }
        public static string[] GetFiles(string FilePath, string[] Extension, SearchOption searchOption)
        {
            //string Files2 = Directory.GetFiles();
            var files = Directory.GetFiles(FilePath, "*.*", searchOption)
                .Where(f => Extension.Contains(Path.GetExtension(f).ToLower())).ToArray();
            return files;
        }
        public static void Delete(string source)
        {
            if (IsFile(source))
            {
                File.Delete(source);
            }
            else
            {
                var files = Directory.GetFiles(source);
                foreach (var file in files)
                {
                    //Copy File ** Not Include Directory
                    File.Delete(file);
                }
                var Directories = Directory.GetDirectories(source);
                foreach (var DirectoryFile in Directories)
                {
                    Delete(DirectoryFile);
                }
            }

        }
        public static void Move(string source, string destination)
        {
            CreateDirectory(destination);
            if (IsFile(source))
            {
                if (File.Exists(destination))
                {
                    File.Delete(destination);
                }
                File.Move(source, destination);
            }
            else
            {
                var files = Directory.GetFiles(source);

                foreach (var file in files)
                {
                    var filename = Path.GetFileName(file);
                    var DestinationFile = destination + @"\" + filename;
                    if (File.Exists(DestinationFile))
                    {
                        File.Delete(DestinationFile);
                    }
                    //Copy File ** Not Include Directory
                    File.Move(file, DestinationFile);
                }
                var Directories = Directory.GetDirectories(source);
                foreach (var DirectoryFile in Directories)
                {
                    Move(DirectoryFile, destination + @"\" + Path.GetFileName(DirectoryFile));
                }
            }

        }
        public static void Copy(string Source, string Destination)
        {
            Source = Source.Replace(@"\", "/");
            Destination = Destination.Replace(@"\", "/");
            var files = Directory.GetFiles(Source);
            CreateDirectory(Destination + "/");
            foreach (var file in files)
            {
                var filename = Path.GetFileName(file);
                var DestinationFile = Destination + "/" + filename;
                if (File.Exists(DestinationFile))
                {
                    File.Delete(DestinationFile);
                }
                //Copy File ** Not Include Directory
                File.Copy(file, DestinationFile);
            }
            var Directories = Directory.GetDirectories(Source);
            foreach (var DirectoryFile in Directories)
            {
                Copy(DirectoryFile, Destination + "/" + Path.GetFileName(DirectoryFile));
            }
        }

        public static void Copy(string Source, string Destination, string owner, string group, OSType ostype = OSType.Unix)
        {
            switch (ostype)
            {
                case OSType.Unix:
                    break;
                case OSType.Windows:
                    break;
            }
            Source = Source.Replace(@"\", "/");
            Destination = Destination.Replace(@"\", "/");
            var files = Directory.GetFiles(Source);
            CreateDirectory(Destination + "/");
            foreach (var file in files)
            {
                var filename = Path.GetFileName(file);
                var DestinationFile = Destination + "/" + filename;
                if (File.Exists(DestinationFile))
                {
                    File.Delete(DestinationFile);
                }
                //Copy File ** Not Include Directory
                File.Copy(file, DestinationFile);
            }
            var Directories = Directory.GetDirectories(Source);
            foreach (var DirectoryFile in Directories)
            {
                Copy(DirectoryFile, Destination + "/" + Path.GetFileName(DirectoryFile));
            }
        }
        public static bool IsFile(string path)
        {
            // get the file attributes for file or directory
            FileAttributes attr = File.GetAttributes(path);

            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                return false;
            else
                return true;
        }
        public static void CreateDirectory(string path)
        {
            path = path.Replace(@"\\", "/").Replace(@"\", "/");
            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                path = Path.GetDirectoryName(path);
                var NetworkPath = path.IndexOf("/");
                var filedirectory = path.Split('/');
                var directory = "";
                for (var x = 0; x < filedirectory.Length; x++)
                {
                    directory += filedirectory[x] + "/";

                    if (!Directory.Exists(directory) && filedirectory[x] != "")
                    {
                        if (!(NetworkPath == 0 && (x <= 2)))
                        {
                            Directory.CreateDirectory(directory);
                        }
                    }
                }
            }
        }

        public static long FreeSpace(string folderName)
        {
            if (string.IsNullOrEmpty(folderName))
            {
                throw new ArgumentNullException("folderName");
            }
            folderName = folderName.Replace("\\", "/");

            try
            {
                folderName = Path.GetPathRoot(folderName);
            }
            catch
            {
                if (!folderName.EndsWith("/"))
                {
                    folderName += '/';
                }
                folderName = Path.GetPathRoot(folderName);
            }


            long free = 0, dummy1 = 0, dummy2 = 0;

            if (GetDiskFreeSpaceEx(folderName, ref free, ref dummy1, ref dummy2))
            {
                return free;
            }
            else
            {
                return -1;
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage"), SuppressUnmanagedCodeSecurity]
        [DllImport("Kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetDiskFreeSpaceEx
        (
            string lpszPath,                    // Must name a folder, must end with '\'.
            ref long lpFreeBytesAvailable,
            ref long lpTotalNumberOfBytes,
            ref long lpTotalNumberOfFreeBytes
        );
    }
}
