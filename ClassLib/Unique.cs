﻿using System;
using System.Linq;

namespace ClassLib
{
    public static class Unique
    {
        public static string GuidPrefix = "yy";
        public static string PrimaryKey = "PrimaryKey";
        public static string NewGuid()
        {            
            return String.Concat((DateTime.Now.ToString(GuidPrefix) + Guid.NewGuid().ToString("N")).Select(c => (char)(c + 17)));
        }
    }
}