﻿
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace ClassLib
{
    
    public static class Access
    {
        public static string GetAccess(string roleId, string access)
        {
            Dictionary<string, List<string>> tableaccess = new Dictionary<string, List<string>>();
            tableaccess.Add(roleId, new List<string>() { access });
            return JsonConvert.SerializeObject(tableaccess);
        }
        public static string GetAccess(string roleId, params string[] access)
        {
            Dictionary<string, List<string>> fieldacces = new Dictionary<string, List<string>>();
            fieldacces.Add(roleId, access.ToList());
            return JsonConvert.SerializeObject(fieldacces);
        }

        public static string GetAccess(string roleId, List<string> access)
        {
            Dictionary<string, List<string>> fieldacces = new Dictionary<string, List<string>>();
            fieldacces.Add(roleId, access);
            return JsonConvert.SerializeObject(fieldacces);
        }

        public static string GetAccess(List<string> roleId, string access)
        {
            Dictionary<string, List<string>> fieldacces = new Dictionary<string, List<string>>();
            foreach (var id in roleId)
            {
                fieldacces.Add(id, new List<string>() { access });
            }
            return JsonConvert.SerializeObject(fieldacces);
        }
        public static string GetAccess(List<string> roleId,List<string> access)
        {
            Dictionary<string, List<string>> fieldacces = new Dictionary<string, List<string>>();
            foreach (var id in roleId)
            {
                fieldacces.Add(id, access);
            }
            return JsonConvert.SerializeObject(fieldacces);
        }
    }
}
