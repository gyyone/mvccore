using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class MenuProperty
{    
    public partial class Pages
    {    
        public partial class JsTable
        {    
            
            
            public struct FieldsId
            {
                public const string Button = "CAFsACHuDCIwwFEGBtJuFtAsAuAvtFBCAE";
                public const string AccessRight = "CAtDCtGJCGvuHtErwsIwssDEGwIJFHHFJv";
                public const string RowNo = "CAGsuIDHBJHDBuEFDIJDruFJsrsBtGuuGC";
                public const string DateVersion = "CABGCIEwAJuuJAECwDsvADAIJHIuBDFrrC";
                public const string ChangedBy = "CArrCwGIGuHvuuEtwDsAAtvCrAvwBHCuJA";
                public const string id = "id";
                public const string ParentId = "CAwIsFBvEtGIEGECrvJuwsEEGCJCJBEtIH";
                public const string Table = "CAADsHwDvuvwrEEEusJCvArGsGwswtIJwr";
                public const string Sort = "CAsvAtwEEsDsEwErHDrIIAEuBvACFHGAHC";
                public const string HardFilter = "CAGEDGBBBttEvGEBwIJIttvwCwBHrAGEww";
                public const string JstreeId = "CAEAJwssAFJDGtECDvsFvrIAvutrECGuIs";
                public const string JstreeParentId = "CAGuuDsIHHFIwHEwrArJrIGJJHtJBHGHuE";
                public const string JstreeDisplay = "CAtuuEIrvsBvEwEBurrEArHvAHGEAIHEBr";
                public const string JstreeIcon = "CAwvwvFAEFuHCDEIwvrHsrrCBCCFCFGwHH";
                public const string SoftFilter = "CAECCCtsCrAvFvEtBtJGIIICJDHFrEuvvA";
                public const string Visible = "CArJEFrrsFIvBHEJwCrArEuIEEGuvsJBwI";
                
            }
            
            
            public JsTable()
            {
                _Button = "";
                _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
                _RowNo = 0;
                _DateVersion = DateTime.Now;
                _ChangedBy = "";
                _id = "";
                _ParentId = "";
                _Table = "";
                _Sort = new Dictionary<string,List<string>>();
                _HardFilter = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{"""":[]}");
                _JstreeId = new List<string>();
                _JstreeParentId = new List<string>();
                _JstreeDisplay = new List<string>();
                _JstreeIcon = new List<string>();
                _SoftFilter = new Dictionary<string,List<string>>();
                _Visible = false;
                
            }
            
            
            private Dictionary<string,string> _Changed { set; get; }
            public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
            public const string ClassID  = "CAFuDCBrFtJADCEDutrFwwEGrAsIECrCHD";
            public string Class_ID  {get{return ClassID;}}
            
            private string _Button { set; get; }
            [JsonProperty(FieldsId.Button)] 
            public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
            
            private Dictionary<string, List<string>> _AccessRight { set; get; }
            [JsonProperty(FieldsId.AccessRight)] 
            public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
            
            private int _RowNo { set; get; }
            [JsonProperty(FieldsId.RowNo)] 
            public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
            
            private DateTime _DateVersion { set; get; }
            [JsonProperty(FieldsId.DateVersion)] 
            public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
            
            private string _ChangedBy { set; get; }
            [JsonProperty(FieldsId.ChangedBy)] 
            public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
            
            private string _id { set; get; }
            [JsonProperty(FieldsId.id)] 
            public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
            
            private string _ParentId { set; get; }
            [JsonProperty(FieldsId.ParentId)] 
            public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
            
            private string _Table { set; get; }
            [JsonProperty(FieldsId.Table)] 
            public string Table { set { Changed[FieldsId.Table] = "true"; _Table = value; } get { return _Table ; } }
            
            private Dictionary<string, List<string>> _Sort { set; get; }
            [JsonProperty(FieldsId.Sort)] 
            public Dictionary<string, List<string>> Sort { set { Changed[FieldsId.Sort] = "true"; _Sort = value; } get { return _Sort ; } }
            
            private Dictionary<string, List<string>> _HardFilter { set; get; }
            [JsonProperty(FieldsId.HardFilter)] 
            public Dictionary<string, List<string>> HardFilter { set { Changed[FieldsId.HardFilter] = "true"; _HardFilter = value; } get { return _HardFilter ; } }
            
            private List<string> _JstreeId { set; get; }
            [JsonProperty(FieldsId.JstreeId)] 
            public List<string> JstreeId { set { Changed[FieldsId.JstreeId] = "true"; _JstreeId = value; } get { return _JstreeId ; } }
            
            private List<string> _JstreeParentId { set; get; }
            [JsonProperty(FieldsId.JstreeParentId)] 
            public List<string> JstreeParentId { set { Changed[FieldsId.JstreeParentId] = "true"; _JstreeParentId = value; } get { return _JstreeParentId ; } }
            
            private List<string> _JstreeDisplay { set; get; }
            [JsonProperty(FieldsId.JstreeDisplay)] 
            public List<string> JstreeDisplay { set { Changed[FieldsId.JstreeDisplay] = "true"; _JstreeDisplay = value; } get { return _JstreeDisplay ; } }
            
            private List<string> _JstreeIcon { set; get; }
            [JsonProperty(FieldsId.JstreeIcon)] 
            public List<string> JstreeIcon { set { Changed[FieldsId.JstreeIcon] = "true"; _JstreeIcon = value; } get { return _JstreeIcon ; } }
            
            private Dictionary<string, List<string>> _SoftFilter { set; get; }
            [JsonProperty(FieldsId.SoftFilter)] 
            public Dictionary<string, List<string>> SoftFilter { set { Changed[FieldsId.SoftFilter] = "true"; _SoftFilter = value; } get { return _SoftFilter ; } }
            
            private bool _Visible { set; get; }
            [JsonProperty(FieldsId.Visible)] 
            public bool Visible { set { Changed[FieldsId.Visible] = "true"; _Visible = value; } get { return _Visible ; } }
            
        }
        
        
        public struct FieldsId
        {
            public const string Button = "BJAHtrDsIBHsBwEGJBIvDtGIvGvFrsBwFr";
            public const string AccessRight = "BJrItFJwAGDsFJErCJrDwwGDGuBHIFFuCC";
            public const string RowNo = "BJsruFHtussGrBEtCDItrrwBCFtCvABuwH";
            public const string DateVersion = "BJCsHBFFvvrHJIEAuJruBwItBvrAtFvDrH";
            public const string ChangedBy = "BJFAursuFDAvrAEDrvJvJArAIFFvDEIIDw";
            public const string id = "id";
            public const string ParentId = "BJsJIBBGJJHDCrEDvDJDDCsEsJFuJAAGFG";
            public const string PageType = "CAsvGCFvsEGCGGEvvFIEAEvFICHvCtIFtB";
            public const string TableId = "BJJrFGrsBJJIFtEGCAICtFwvwrFEHBGDCE";
            public const string Url = "CAsAsBJruCFsACEHtFsBtArvBHBEIGCrtF";
            public const string Sort = "CAuHADGAtGHHuGEuAHsHFHvCBCItsrsHJC";
            public const string HardFilter = "CABvuFGIJCAArJEHAwrBEFFrsFEAwJIIGt";
            public const string SoftFilter = "CAsEErDsCHsuttEwvJIuvICEuHtJwvuuCr";
            public const string Visible = "BJDuEIvDGIBrEvEDCuJEtrwDFEAtrsBJsL";
            
        }
        
        
        public Pages()
        {
            _Button = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _ChangedBy = "";
            _id = "";
            _ParentId = "";
            _PageType = @"Table";
            _TableId = "";
            _Url = "";
            _Sort = new Dictionary<string,List<string>>();
            _HardFilter = new Dictionary<string,List<string>>();
            _SoftFilter = new Dictionary<string,List<string>>();
            _Visible = false;
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJGEDIHDGsHGADEIADIAAHIHAGECGJswEw";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _PageType { set; get; }
        [JsonProperty(FieldsId.PageType)] 
        public string PageType { set { Changed[FieldsId.PageType] = "true"; _PageType = value; } get { return _PageType ; } }
        
        private string _TableId { set; get; }
        [JsonProperty(FieldsId.TableId)] 
        public string TableId { set { Changed[FieldsId.TableId] = "true"; _TableId = value; } get { return _TableId ; } }
        
        private string _Url { set; get; }
        [JsonProperty(FieldsId.Url)] 
        public string Url { set { Changed[FieldsId.Url] = "true"; _Url = value; } get { return _Url ; } }
        
        private Dictionary<string, List<string>> _Sort { set; get; }
        [JsonProperty(FieldsId.Sort)] 
        public Dictionary<string, List<string>> Sort { set { Changed[FieldsId.Sort] = "true"; _Sort = value; } get { return _Sort ; } }
        
        private Dictionary<string, List<string>> _HardFilter { set; get; }
        [JsonProperty(FieldsId.HardFilter)] 
        public Dictionary<string, List<string>> HardFilter { set { Changed[FieldsId.HardFilter] = "true"; _HardFilter = value; } get { return _HardFilter ; } }
        
        private Dictionary<string, List<string>> _SoftFilter { set; get; }
        [JsonProperty(FieldsId.SoftFilter)] 
        public Dictionary<string, List<string>> SoftFilter { set { Changed[FieldsId.SoftFilter] = "true"; _SoftFilter = value; } get { return _SoftFilter ; } }
        
        private bool _Visible { set; get; }
        [JsonProperty(FieldsId.Visible)] 
        public bool Visible { set { Changed[FieldsId.Visible] = "true"; _Visible = value; } get { return _Visible ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJCGBACFABEFHsEAJHsAGtHtDtFuBBEHwI";
        public const string RowNo = "BJsJtJDrrssGBDEsADrsAtArJsvCGrwIGB";
        public const string DateVersion = "BJsvuErrFutruGECFHrAwCurtDrurHsrrF";
        public const string id = "id";
        public const string ParentId = "BJsFBtEHHIwtFrEHBDIBuJGIJrGEssssts";
        public const string ChangedBy = "BJCwBDDtIBwAHHEvDtIwrwIJIvrIJEuvvA";
        public const string AccessRight = "BJEJBJDIvEFsvvEEHIJrvFEHJBwEHrEtEI";
        public const string Visible = "BJEHDsvFGIFAvFEEsHsswtwuCwAEDGsJru";
        public const string Name = "BJIvvuwtHHGvDFEtGGrCABwwGCFHEFHwBB";
        public const string Icon = "BJBGDBvGIAEJIHEtwwstssGsAFDHFCHFuB";
        public const string IsDefaultPage = "BJEHHEBsDGHrGCECsCrBCtDwrsJwvwIItD";
        public const string Color = "BJBsBHHEwJuJBGEHuGJEArJAJCDsBFCCtC";
        
    }
    
    
    public MenuProperty()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Visible = true;
        _Name = "";
        _Icon = "";
        _IsDefaultPage = false;
        _Color = @"";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJvrADDCJBrBBCEruAJDJuGDwIDIvCHJwJ";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private bool _Visible { set; get; }
    [JsonProperty(FieldsId.Visible)] 
    public bool Visible { set { Changed[FieldsId.Visible] = "true"; _Visible = value; } get { return _Visible ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private string _Icon { set; get; }
    [JsonProperty(FieldsId.Icon)] 
    public string Icon { set { Changed[FieldsId.Icon] = "true"; _Icon = value; } get { return _Icon ; } }
    
    private bool _IsDefaultPage { set; get; }
    [JsonProperty(FieldsId.IsDefaultPage)] 
    public bool IsDefaultPage { set { Changed[FieldsId.IsDefaultPage] = "true"; _IsDefaultPage = value; } get { return _IsDefaultPage ; } }
    
    private string _Color { set; get; }
    [JsonProperty(FieldsId.Color)] 
    public string Color { set { Changed[FieldsId.Color] = "true"; _Color = value; } get { return _Color ; } }
    
}
