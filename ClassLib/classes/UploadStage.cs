using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class UploadStage
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJAFCJsuBFrtFCEEuGsGHtDuBBwtwEttIG";
        public const string RowNo = "BJrCAAAAIvAEHvEHwFJHJuuwCADCHuJFCC";
        public const string DateVersion = "BJDArCErBJBIFHEFDIrwrAvBvsHIGIHsrD";
        public const string id = "id";
        public const string ParentId = "BJGFvCDwBBtuwHEICGJuCFwGsBBtGIHvvI";
        public const string ChangedBy = "BJIGFIEGwrEuEBEwuusEtJsHHDwvuEEtDs";
        public const string AccessRight = "BJuHJAvHuEBrErEAwJsIAEHEsttAIsusFH";
        public const string Name = "BJHBwvwDEAFBtvEswGJwFJtBIEsIEBvJHB";
        public const string FileLocation = "BJAuCEIHFIFwIsEIGurCtsrCHtFJCtDuIv";
        public const string Files = "BJvEArtAuJItssEttEJGEsAIrDAFBvvDrA";
        public const string UploadStatus = "BJsGGIvJHuFsDtEJsvsFBGtCtsGwGEGIJs";
        public const string Description = "BJErADuHGrGrGGErIFrsEtIEtDuAvJEDCs";
        
    }
    
    
    public UploadStage()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        _FileLocation = "";
        _Files = "";
        _UploadStatus = "";
        _Description = @"";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJEtDtJCGwEwBsEHCJJvIvJsIGACtAsDFt";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private string _FileLocation { set; get; }
    [JsonProperty(FieldsId.FileLocation)] 
    public string FileLocation { set { Changed[FieldsId.FileLocation] = "true"; _FileLocation = value; } get { return _FileLocation ; } }
    
    private string _Files { set; get; }
    [JsonProperty(FieldsId.Files)] 
    public string Files { set { Changed[FieldsId.Files] = "true"; _Files = value; } get { return _Files ; } }
    
    private string _UploadStatus { set; get; }
    [JsonProperty(FieldsId.UploadStatus)] 
    public string UploadStatus { set { Changed[FieldsId.UploadStatus] = "true"; _UploadStatus = value; } get { return _UploadStatus ; } }
    
    private string _Description { set; get; }
    [JsonProperty(FieldsId.Description)] 
    public string Description { set { Changed[FieldsId.Description] = "true"; _Description = value; } get { return _Description ; } }
    
}
