using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Template
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJDsEEEDBFsuJFEutCrICEGFAIGGDsFuJI";
        public const string RowNo = "BJHIAAFGsuGvJAEEBHJCHJCvABFIBvuHsE";
        public const string DateVersion = "BJDIuuwIABCABGEvtJsAEvBIDwEBACAGBr";
        public const string id = "id";
        public const string ParentId = "BJJrstJHrAuJsFEIJrIHDHHwBGtAurGFrH";
        public const string ChangedBy = "BJAHABDGrJGAJJEEvEJCsEEGGtHJAuABJs";
        public const string AccessRight = "BJFrAAHCvEuGDDEsGvrGutsFBrHAHDJuuE";
        public const string Name = "BJuCrtAtrGGEusEsHHrsIrJAHEDsrEtFrw";
        public const string GroupName = "BJBBFGGHEJItvtErHBIItHuuCAwIwrJDJA";
        public const string ForTable = "BJAHHFtussJvtGEtsBrFuAsAuwsvAJGIvs";
        public const string Filter = "CAuCsHAwGEsEEFEsGIrIBAuFDrDsHvHrur";
        public const string Code = "BJBJuHwCABIFDvEGwwrBrsFCrIAErJBuFt";
        
    }
    
    
    public Template()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        _GroupName = "";
        _ForTable = "";
        _Filter = new Dictionary<string,List<string>>();
        _Code = new CodeEditor("");
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJsIrHBJEvDssrEGuFIuDFrCBFvAIDuBFG";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private string _GroupName { set; get; }
    [JsonProperty(FieldsId.GroupName)] 
    public string GroupName { set { Changed[FieldsId.GroupName] = "true"; _GroupName = value; } get { return _GroupName ; } }
    
    private string _ForTable { set; get; }
    [JsonProperty(FieldsId.ForTable)] 
    public string ForTable { set { Changed[FieldsId.ForTable] = "true"; _ForTable = value; } get { return _ForTable ; } }
    
    private Dictionary<string, List<string>> _Filter { set; get; }
    [JsonProperty(FieldsId.Filter)] 
    public Dictionary<string, List<string>> Filter { set { Changed[FieldsId.Filter] = "true"; _Filter = value; } get { return _Filter ; } }
    
    private CodeEditor _Code { set; get; }
    [JsonProperty(FieldsId.Code)] 
    public CodeEditor Code { set { Changed[FieldsId.Code] = "true"; _Code = value; } get { return _Code ; } }
    
}
