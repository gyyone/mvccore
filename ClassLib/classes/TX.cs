using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class TX
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJrIBvtEFBtsCsEECuJEHwEFEIBDtswIws";
        public const string RowNo = "BJstsDHIvwGJEBEBBGJtrCuHCtBBHJDvJI";
        public const string DateVersion = "BJCIAvDwDItDBJEGIFIDFDstBJItBBFCFG";
        public const string id = "id";
        public const string ParentId = "BJtwJGsuGDuIAIEwFFrABrDEuruvBAvIGt";
        public const string ChangedBy = "BJIFFIGHrIBFAsEsEIJGHvAruEuEutHEDC";
        public const string AccessRight = "BJvIItGttBGwDrEGHHrvuFHtJFFvrHGBBA";
        public const string FieldId = "BJsCCAuEGFtuDwEBJBsJvFFtvCCBDvFsDC";
        public const string RecordId = "BJEwrFEvsHuEDCEErJIEIDFwHtCrDDvAtI";
        public const string Value = "BJruwsFvvwwHBDEsHtJCGJuvIuHvwuDIFA";
        
    }
    
    
    public TX()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""],""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Modify"",""Delete""],""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Modify"",""Delete""]}");
        _FieldId = "";
        _RecordId = "";
        _Value = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJEsAHsttDAEuBEuGIIIFGCAsrFItDADtw";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _FieldId { set; get; }
    [JsonProperty(FieldsId.FieldId)] 
    public string FieldId { set { Changed[FieldsId.FieldId] = "true"; _FieldId = value; } get { return _FieldId ; } }
    
    private string _RecordId { set; get; }
    [JsonProperty(FieldsId.RecordId)] 
    public string RecordId { set { Changed[FieldsId.RecordId] = "true"; _RecordId = value; } get { return _RecordId ; } }
    
    private string _Value { set; get; }
    [JsonProperty(FieldsId.Value)] 
    public string Value { set { Changed[FieldsId.Value] = "true"; _Value = value; } get { return _Value ; } }
    
}
