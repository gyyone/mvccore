using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Users
{    
    public partial class Contacts
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJwGswuAEvEHIDEDrGsFsHEwAIJsBCBBIH";
            public const string RowNo = "BJGJwDCCDIssBBEGDAsBusstICFFvACvtJ";
            public const string DateVersion = "BJwHFHEuuCDtvGEDDArJDvvDuCFBtBHGtH";
            public const string id = "id";
            public const string ParentId = "BJEuHHwJGEIwCuEsBCsIJFurGArFvBGrHI";
            public const string ChangedBy = "BJJJGEDIwrJAtuEvAJrsHIsFvABEGsturC";
            public const string AccessRight = "BJEHGwJHtBtAuJEJwFJuHvvFJJJDtHtADJ";
            public const string ContactNumber = "BJwtwAwJAAHCJvEDJCrEtDvHwrDwFIuDHt";
            
        }
        
        
        public Contacts()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _ContactNumber = "";
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJwutCtEtDuHwCEGtDrFvGCGCJFvDGvDwE";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _ContactNumber { set; get; }
        [JsonProperty(FieldsId.ContactNumber)] 
        public string ContactNumber { set { Changed[FieldsId.ContactNumber] = "true"; _ContactNumber = value; } get { return _ContactNumber ; } }
        
    }
    public partial class Emails
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJIuDwrvEHBFJHEAAArBvEuCCIGtCsusGD";
            public const string RowNo = "BJtsGrrJBvHHvFEFrwJJCrFAAusACEwFtG";
            public const string DateVersion = "BJwvtwFtGsCwIwEwJCIGwvustGGJCtEJCv";
            public const string id = "id";
            public const string ParentId = "BJArrBuuBHGtFsEvJDIFIFICFAFDIDHCDF";
            public const string ChangedBy = "BJAHDAEDvDDttuEBsGrAvwwBIGJCuDBIEI";
            public const string AccessRight = "BJswwIErFvEtrBEEBGrsGJBEJGJsIvGrJJ";
            public const string Email = "BJBGJHFJwGrsuwEwEDIEsrEFGAHvIsrICt";
            
        }
        
        
        public Emails()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _Email = "";
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJAvvvrBHvGtDEEuuCIGtuErAtEFFCFEDw";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _Email { set; get; }
        [JsonProperty(FieldsId.Email)] 
        public string Email { set { Changed[FieldsId.Email] = "true"; _Email = value; } get { return _Email ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJtCDHIvBBDEwuEBEBJtJGrrFAIrrIACAD";
        public const string RowNo = "BJHJuGIJHEDwrrEuuJIAEDDJAtwJvsDHtu";
        public const string DateVersion = "BJGrtJsuBIJwGGEGGCJCrFrrBJAsHJFtvI";
        public const string id = "id";
        public const string ParentId = "BJvCuAAssJJHvGEsAvrHIFCAJICGHFHEvu";
        public const string ChangedBy = "BJEJHGACuDAtIHEuGJsIHJJIBtJwGEFBBE";
        public const string UserGroups = "CAwJuCwtCrrDIsECAFsFCGIBvwvrEAAvJr";
        public const string AccessRight = "BJEEJsJCtusCsGEwEAsEFurAvvsFEwwIJv";
        public const string UserId = "BJJDFtsIAsEsBHEEvGIsrswFJuABFJtDAv";
        public const string UserName = "BJAvGBJJIHtAFrEwIHrHJvHvwErCwIrEIJ";
        public const string Password = "BJIsJHJDDuBGuwEJEFICFArCsJEHCvJvBr";
        public const string Language = "BJJFwCCIEwsCrDEIDtsvGJAtvuwCsAvssF";
        public const string DateFormat = "BJCuwtuCtwvttHEtvwrHDFAICrHvJwGFrJ";
        public const string Activated = "BJrIJsJBrtFDJEEEuJJrEBuwCAvDHCEHID";
        public const string Theme = "CAswHvvDGuBFutEtrtJFBsAJEBBvFtsCIC";
        public const string HostIP = "CAFAvvFuwHAGrBEEtDIwwuJBsDtErsHFuF";
        public const string EnableAPICall = "CAItuIGwCHtDsBEFrwrrAHwvuwGAHBrAGr";
        public const string QRTemplate = "CAGACFvGusuAvsEHrCrEvssBCEDHCGBtAr";
        
    }
    
    
    public Users()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _UserGroups = JsonConvert.DeserializeObject<List<string>>(@"[""BJFJwJsHuFBBEBEADDIGsBEsvvuvAstAvr""]");
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _UserId = "";
        _UserName = "";
        _Password = @"";
        _Language = "";
        _DateFormat = "yyyy-MM-dd HH:mm:ss";
        _Activated = false;
        _Theme = "";
        _HostIP = new List<string>();
        _EnableAPICall = false;
        _QRTemplate = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJFCswsHHCrvGtEvIGswHrIsHIvuJtsGwA";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private List<string> _UserGroups { set; get; }
    [JsonProperty(FieldsId.UserGroups)] 
    public List<string> UserGroups { set { Changed[FieldsId.UserGroups] = "true"; _UserGroups = value; } get { return _UserGroups ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _UserId { set; get; }
    [JsonProperty(FieldsId.UserId)] 
    public string UserId { set { Changed[FieldsId.UserId] = "true"; _UserId = value; } get { return _UserId ; } }
    
    private string _UserName { set; get; }
    [JsonProperty(FieldsId.UserName)] 
    public string UserName { set { Changed[FieldsId.UserName] = "true"; _UserName = value; } get { return _UserName ; } }
    
    private string _Password { set; get; }
    [JsonProperty(FieldsId.Password)] 
    public string Password { set { Changed[FieldsId.Password] = "true"; _Password = value; } get { return _Password ; } }
    
    private string _Language { set; get; }
    [JsonProperty(FieldsId.Language)] 
    public string Language { set { Changed[FieldsId.Language] = "true"; _Language = value; } get { return _Language ; } }
    
    private string _DateFormat { set; get; }
    [JsonProperty(FieldsId.DateFormat)] 
    public string DateFormat { set { Changed[FieldsId.DateFormat] = "true"; _DateFormat = value; } get { return _DateFormat ; } }
    
    private bool _Activated { set; get; }
    [JsonProperty(FieldsId.Activated)] 
    public bool Activated { set { Changed[FieldsId.Activated] = "true"; _Activated = value; } get { return _Activated ; } }
    
    private string _Theme { set; get; }
    [JsonProperty(FieldsId.Theme)] 
    public string Theme { set { Changed[FieldsId.Theme] = "true"; _Theme = value; } get { return _Theme ; } }
    
    private List<string> _HostIP { set; get; }
    [JsonProperty(FieldsId.HostIP)] 
    public List<string> HostIP { set { Changed[FieldsId.HostIP] = "true"; _HostIP = value; } get { return _HostIP ; } }
    
    private bool _EnableAPICall { set; get; }
    [JsonProperty(FieldsId.EnableAPICall)] 
    public bool EnableAPICall { set { Changed[FieldsId.EnableAPICall] = "true"; _EnableAPICall = value; } get { return _EnableAPICall ; } }
    
    private string _QRTemplate { set; get; }
    [JsonProperty(FieldsId.QRTemplate)] 
    public string QRTemplate { set { Changed[FieldsId.QRTemplate] = "true"; _QRTemplate = value; } get { return _QRTemplate ; } }
    
}
