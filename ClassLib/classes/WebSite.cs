using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Util;
using System.Linq;

using Newtonsoft.Json.Converters;
using ClassLib;
public partial class WebSite
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJEIEFuJIHBswEEuDtJBHIsIwDHFwtDDBu";
        public const string RowNo = "BJwHAsGtBGIruwECGwJGwssvHCJvrDGHHv";
        public const string DateVersion = "BJCCHwHBvvEwsEEDwuJCwEuwwssACwCuCD";
        public const string id = "id";
        public const string ParentId = "BJusrFICHEIHFtEurGIDJJIJCusEtsHEwH";
        public const string ChangedBy = "BJtBGIuAItHuHrEEBwJEwBFCGtAIFrssJE";
        public const string AccessRight = "BJCICAAFsABwuwEEEHJJHCuBuruBwFuvrw";
        public const string Url = "BJJvrJvsEttuuGEICIrDrJGJtFDBHvrFHC";
        public const string CompanyName = "BJCrHrHrvvCrDBEABuJwGwstJwuIHrFwAs";
        
    }
    
    
    public WebSite()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Url = "";
        _CompanyName = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJrBJIGrvuJurCEswwItDwwJCvEIuBEIIt";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed["BJEIEFuJIHBswEEuDtJBHIsIwDHFwtDDBu"] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed["BJwHAsGtBGIruwECGwJGwssvHCJvrDGHHv"] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed["BJCCHwHBvvEwsEEDwuJCwEuwwssACwCuCD"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed["BJusrFICHEIHFtEurGIDJJIJCusEtsHEwH"] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed["BJtBGIuAItHuHrEEBwJEwBFCGtAIFrssJE"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed["BJCICAAFsABwuwEEEHJJHCuBuruBwFuvrw"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Url { set; get; }
    [JsonProperty(FieldsId.Url)] 
    public string Url { set { Changed["BJJvrJvsEttuuGEICIrDrJGJtFDBHvrFHC"] = "true"; _Url = value; } get { return _Url ; } }
    
    private string _CompanyName { set; get; }
    [JsonProperty(FieldsId.CompanyName)] 
    public string CompanyName { set { Changed["BJCrHrHrvvCrDBEABuJwGwstJwuIHrFwAs"] = "true"; _CompanyName = value; } get { return _CompanyName ; } }
    
}
