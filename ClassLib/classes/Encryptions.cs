using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Encryptions
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJuBACABAsEJDrEEuIrCtHutvrwBCEEJCD";
        public const string RowNo = "BJGAFAwGHBEvGvEvtErBCsrJEHvDIrJAGs";
        public const string DateVersion = "BJrFFGGwBADtIHEBJuswAIJsAFrJvJHHus";
        public const string id = "id";
        public const string ParentId = "BJEIHBJBCuruIuEtJtJsssFuuAwGBrJGAB";
        public const string ChangedBy = "BJrrJIAHBFBsCAEtDErrDCCrtEIEJAsCwD";
        public const string AccessRight = "BJDGJwsEsGFFsJEJvvsrEGDDrCvEFrutGF";
        public const string EncryptionType = "BJtvvHvrFGJvuvEGvsIEtGEDrHusrHGIAI";
        public const string Key = "BJvurIHrFrAHBBEHsrJEHtFBErIEtvuuFE";
        public const string IV = "BJHrEDECJBDFIJEtCEJIIDGsCJHBHGDvCE";
        public const string Name = "BJuEEFIJAICEwsEDwusADGErJvruJFttvv";
        
    }
    
    
    public Encryptions()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _EncryptionType = "";
        _Key = "";
        _IV = "";
        _Name = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJGEuCGBtDCIBtECIuIJDwrvJwDICsGwEB";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _EncryptionType { set; get; }
    [JsonProperty(FieldsId.EncryptionType)] 
    public string EncryptionType { set { Changed[FieldsId.EncryptionType] = "true"; _EncryptionType = value; } get { return _EncryptionType ; } }
    
    private string _Key { set; get; }
    [JsonProperty(FieldsId.Key)] 
    public string Key { set { Changed[FieldsId.Key] = "true"; _Key = value; } get { return _Key ; } }
    
    private string _IV { set; get; }
    [JsonProperty(FieldsId.IV)] 
    public string IV { set { Changed[FieldsId.IV] = "true"; _IV = value; } get { return _IV ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
}
