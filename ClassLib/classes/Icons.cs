using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Icons
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJDwwBFAutBBrAEHBIJJsBEDHwBIGCCCEA";
        public const string RowNo = "BJJGBvJGvGEDsGEIEDJBFCHFAuHABDBuuw";
        public const string DateVersion = "BJIIsDErDCJHBJEuuGItJsBFvwFErBwBwD";
        public const string id = "id";
        public const string ParentId = "BJuHGvwvsDIDAFEvGAsGHHtAIvJJrruvtD";
        public const string ChangedBy = "BJsGtCBtAGvstHEwEArwBtvIsvBrurHEsu";
        public const string AccessRight = "BJCvHHEuEIsEIsEFsEIGEvIHvBHCFGIJDs";
        public const string ClassName = "BJvvwuvFAtuFuwEDGrJHIGDJFDvGGDBDJt";
        public const string Description = "BJFwGtsrttBtDrEADIIsHCHGBHuGvCEIvB";
        public const string Html = "BJBsvwswvGHsuCEvwsrJBsrvBAsstFrsrt";
        
    }
    
    
    public Icons()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _ClassName = "";
        _Description = "";
        _Html = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJDHCHIuBGGsCFEFHuJAwtCsrttAGwGIsu";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _ClassName { set; get; }
    [JsonProperty(FieldsId.ClassName)] 
    public string ClassName { set { Changed[FieldsId.ClassName] = "true"; _ClassName = value; } get { return _ClassName ; } }
    
    private string _Description { set; get; }
    [JsonProperty(FieldsId.Description)] 
    public string Description { set { Changed[FieldsId.Description] = "true"; _Description = value; } get { return _Description ; } }
    
    private string _Html { set; get; }
    [JsonProperty(FieldsId.Html)] 
    public string Html { set { Changed[FieldsId.Html] = "true"; _Html = value; } get { return _Html ; } }
    
}
