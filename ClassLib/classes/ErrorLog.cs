using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class ErrorLog
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJuvIBuBFFrEBCEICEsBHCrICrJFuEurBs";
        public const string RowNo = "BJFHHsEtGEwHHDEDJtrHuAIsBCCtAIwACE";
        public const string DateVersion = "BJHEsCvvCttFIFEsItsCuwrsAJIsvGtCGt";
        public const string id = "id";
        public const string ParentId = "BJCsAJHwDACJtvEDAuItuJsFIIvvIGtrCE";
        public const string ChangedBy = "BJrCuIAIEABIFAEGDIJsvtvIIGJEEEwFwB";
        public const string AccessRight = "BJrEGwrCsEAssBEBFHsJtBEHrsCHAIACIs";
        public const string ErrMsg = "BJFusJutGAGEuEEIBDJrurwuvtDCHrwAvw";
        public const string ObjDetail = "BJAJErDHtwFFBFEBEwIrBFsstvCtwwuHrv";
        
    }
    
    
    public ErrorLog()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _ErrMsg = "";
        _ObjDetail = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJFGBBswtGCuJBEsAssCJrFsBvurCBGwIu";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _ErrMsg { set; get; }
    [JsonProperty(FieldsId.ErrMsg)] 
    public string ErrMsg { set { Changed[FieldsId.ErrMsg] = "true"; _ErrMsg = value; } get { return _ErrMsg ; } }
    
    private string _ObjDetail { set; get; }
    [JsonProperty(FieldsId.ObjDetail)] 
    public string ObjDetail { set { Changed[FieldsId.ObjDetail] = "true"; _ObjDetail = value; } get { return _ObjDetail ; } }
    
}
