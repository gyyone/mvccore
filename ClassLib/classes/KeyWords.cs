using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class KeyWords
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJGHwGDtvGDtHGEJEvJGBDsIwIIJHGBvrG";
        public const string RowNo = "BJrrHHwtHuAIwsEIvIsrJIIsBuBwsrtsEB";
        public const string DateVersion = "BJAHEvCEwuIsFBEGHJIEDFtwrtEJIDIsvD";
        public const string id = "id";
        public const string ParentId = "BJCtHrGvvGDvuJEADvrutAHCsvAErsDHJI";
        public const string ChangedBy = "BJGJCuJtIAEAFJErBwsGHFrBrIvIwwwDtI";
        public const string AccessRight = "BJwvuvuJHtrvwrEEurIsAHEtutEvEHDAss";
        public const string KeyWord = "BJFtIItFuGDFsCEtEIJFABtHBJuFBHDJDC";
        public const string Chinese = "BJsBEtAItECrGuEvIIIrBrwICvCDrwCstI";
        public const string English = "BJFDrBGEAAHAAJEDDFsuAvwHBGvFCEJEJu";
        
    }
    
    
    public KeyWords()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _KeyWord = "";
        _Chinese = "";
        _English = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJAuAGvHDICCCJEGsFIAwItwCFADJAwJIu";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _KeyWord { set; get; }
    [JsonProperty(FieldsId.KeyWord)] 
    public string KeyWord { set { Changed[FieldsId.KeyWord] = "true"; _KeyWord = value; } get { return _KeyWord ; } }
    
    private string _Chinese { set; get; }
    [JsonProperty(FieldsId.Chinese)] 
    public string Chinese { set { Changed[FieldsId.Chinese] = "true"; _Chinese = value; } get { return _Chinese ; } }
    
    private string _English { set; get; }
    [JsonProperty(FieldsId.English)] 
    public string English { set { Changed[FieldsId.English] = "true"; _English = value; } get { return _English ; } }
    
}
