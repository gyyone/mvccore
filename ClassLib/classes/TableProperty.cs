using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class TableProperty
{    
    public partial class FieldProperty
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJIuHtGwvBHCDJEHIIsJuwHBHwJFsuGwIu";
            public const string RowNo = "BJAAFvrvvFuIwDEArGJEtJvruGEJBtFBHG";
            public const string DateVersion = "BJwEtCDAFuJEGHEGuBrJBJGuJDJutvrEuI";
            public const string id = "id";
            public const string ParentId = "BJutFBwItEtsFHECICsHEJrAsJvIBGHJuB";
            public const string ChangedBy = "BJuHsutJCtGsIuEuFCJJvDwstsEJAEDwCB";
            public const string AccessRight = "BJsrrFGrDBAIsvEHHFswJIGADrvtBtttwJ";
            public const string Name = "BJwBwttIwuEGrwEEBsJICCvuuBFEuuHDJs";
            public const string FormGroup = "CAHCGvEFvIIvFGEFsHrAtAJBuAvsCGFGEH";
            public const string Enable = "BJIDFrECBFFJBDEtrGJsEGIDIAHJvGCrtw";
            public const string Visible = "BJJDwBvtBJBtrrEswCIrFvGAICrBBvIEvH";
            public const string OnlyOneEnable = "CACADBwAsBGwEHEFEEsrvIrDtwuHHuCEAJ";
            public const string ControlType = "BJCIHDIwICBsIGEwAssuFDHvtACJJCDEJC";
            public const string UseUserDateFormat = "BJEBuFtGEsvrAIEwIEIDABHrwAIGFIJtsG";
            public const string MultipleValue = "BJJFIICsrJBEsJEFHvrAIwvBIIHEDDHBsB";
            public const string Template = "BJvBCJGAACFtttEFAGIIvJIuEAFIFvBBsF";
            public const string Encrytion = "BJIFGBBEEFBtwAEHHvIEvDIFHrHsJEJsuJ";
            public const string QueryType = "BJJDGsBvrtDrGAEFwGICArEwCDurCIJEGD";
            public const string DataType = "BJJArFCwDsCAtIEArtsJAGBuwtvGwDJwvr";
            public const string DependentField = "BJCFEHAtJsvDIIEBIHrDGDuGsIswEACwvt";
            public const string Source = "BJrFwJBwDrEHHwErJsIJsuECDsrEIBDEHv";
            public const string Options = "BJJvuvCrFJHHIwECsAIuGrCADuGFstEuGJ";
            public const string DefaultValues = "BJswEJIFsBrJIFEFFsJvEHtHGHGGCBCGEt";
            public const string Hint = "BJFGJtErCIGIAsEwGCrwHCrFDutrvwGutD";  
            public const string Max = "BJFDHHFGIGJAwBEuJwIrJwACJDHDvBwvrv";
            public const string Min = "BJBuuFHDCrsBGEEBGIsuvHDsFvCrJtFtJE";
            public const string Sum = "BJCEDCwtDEFFFIEEECrBwutGEwtBrvAsED";
            public const string Avg = "BJFEtttEJBGFJrEFDsJwsBAHEtIAAEGACt";
            
        }
        
        
        public FieldProperty()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = @"175b8bb8e76b8f4802acd02adb49450004";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Delete"",""Modify""],""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Delete"",""Modify""]}");
            _Name = "";
            _FormGroup = new List<string>();
            _Enable = false;
            _Visible = true;
            _OnlyOneEnable = false;
            _ControlType = "";
            _UseUserDateFormat = false;
            _MultipleValue = false;
            _Template = "";
            _Encrytion = "";
            _QueryType = @"RangeQuery";
            _DataType = @"text_general";
            _DependentField = new Dictionary<string,List<string>>();
            _Source = new DataSource();
            _Options = new CodeEditor("");
            _DefaultValues = "";
            _Hint = @"";           
            _Max = false;
            _Min = false;
            _Sum = false;
            _Avg = false;
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJEHvFAECFEDCtEFEvIDJFDuBIIEBIDvJJ";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _Name { set; get; }
        [JsonProperty(FieldsId.Name)] 
        public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
        
        private List<string> _FormGroup { set; get; }
        [JsonProperty(FieldsId.FormGroup)] 
        public List<string> FormGroup { set { Changed[FieldsId.FormGroup] = "true"; _FormGroup = value; } get { return _FormGroup ; } }
        
        private bool _Enable { set; get; }
        [JsonProperty(FieldsId.Enable)] 
        public bool Enable { set { Changed[FieldsId.Enable] = "true"; _Enable = value; } get { return _Enable ; } }
        
        private bool _Visible { set; get; }
        [JsonProperty(FieldsId.Visible)] 
        public bool Visible { set { Changed[FieldsId.Visible] = "true"; _Visible = value; } get { return _Visible ; } }
        
        private bool _OnlyOneEnable { set; get; }
        [JsonProperty(FieldsId.OnlyOneEnable)] 
        public bool OnlyOneEnable { set { Changed[FieldsId.OnlyOneEnable] = "true"; _OnlyOneEnable = value; } get { return _OnlyOneEnable ; } }
        
        private string _ControlType { set; get; }
        [JsonProperty(FieldsId.ControlType)] 
        public string ControlType { set { Changed[FieldsId.ControlType] = "true"; _ControlType = value; } get { return _ControlType ; } }
        
        private bool _UseUserDateFormat { set; get; }
        [JsonProperty(FieldsId.UseUserDateFormat)] 
        public bool UseUserDateFormat { set { Changed[FieldsId.UseUserDateFormat] = "true"; _UseUserDateFormat = value; } get { return _UseUserDateFormat ; } }
        
        private bool _MultipleValue { set; get; }
        [JsonProperty(FieldsId.MultipleValue)] 
        public bool MultipleValue { set { Changed[FieldsId.MultipleValue] = "true"; _MultipleValue = value; } get { return _MultipleValue ; } }
        
        private string _Template { set; get; }
        [JsonProperty(FieldsId.Template)] 
        public string Template { set { Changed[FieldsId.Template] = "true"; _Template = value; } get { return _Template ; } }
        
        private string _Encrytion { set; get; }
        [JsonProperty(FieldsId.Encrytion)] 
        public string Encrytion { set { Changed[FieldsId.Encrytion] = "true"; _Encrytion = value; } get { return _Encrytion ; } }
        
        private string _QueryType { set; get; }
        [JsonProperty(FieldsId.QueryType)] 
        public string QueryType { set { Changed[FieldsId.QueryType] = "true"; _QueryType = value; } get { return _QueryType ; } }
        
        private string _DataType { set; get; }
        [JsonProperty(FieldsId.DataType)] 
        public string DataType { set { Changed[FieldsId.DataType] = "true"; _DataType = value; } get { return _DataType ; } }
        
        private Dictionary<string, List<string>> _DependentField { set; get; }
        [JsonProperty(FieldsId.DependentField)] 
        public Dictionary<string, List<string>> DependentField { set { Changed[FieldsId.DependentField] = "true"; _DependentField = value; } get { return _DependentField ; } }
        
        private DataSource _Source { set; get; }
        [JsonProperty(FieldsId.Source)] 
        public DataSource Source { set { Changed[FieldsId.Source] = "true"; _Source = value; } get { return _Source ; } }
        
        private CodeEditor _Options { set; get; }
        [JsonProperty(FieldsId.Options)] 
        public CodeEditor Options { set { Changed[FieldsId.Options] = "true"; _Options = value; } get { return _Options ; } }
        
        private string _DefaultValues { set; get; }
        [JsonProperty(FieldsId.DefaultValues)] 
        public string DefaultValues { set { Changed[FieldsId.DefaultValues] = "true"; _DefaultValues = value; } get { return _DefaultValues ; } }
        
        private string _Hint { set; get; }
        [JsonProperty(FieldsId.Hint)] 
        public string Hint { set { Changed[FieldsId.Hint] = "true"; _Hint = value; } get { return _Hint ; } }
        
              
        private bool _Max { set; get; }
        [JsonProperty(FieldsId.Max)] 
        public bool Max { set { Changed[FieldsId.Max] = "true"; _Max = value; } get { return _Max ; } }
        
        private bool _Min { set; get; }
        [JsonProperty(FieldsId.Min)] 
        public bool Min { set { Changed[FieldsId.Min] = "true"; _Min = value; } get { return _Min ; } }
        
        private bool _Sum { set; get; }
        [JsonProperty(FieldsId.Sum)] 
        public bool Sum { set { Changed[FieldsId.Sum] = "true"; _Sum = value; } get { return _Sum ; } }
        
        private bool _Avg { set; get; }
        [JsonProperty(FieldsId.Avg)] 
        public bool Avg { set { Changed[FieldsId.Avg] = "true"; _Avg = value; } get { return _Avg ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJJJrvHvJvwrIsEwDCIwrDsrJuBBIFCrJD";
        public const string RowNo = "BJvIsuHDvJtEtAEErvJvwIruvBEGIICAut";
        public const string DateVersion = "BJJrsEsuBvAtuJErABsGGAAErtCGGJJArI";
        public const string id = "id";
        public const string ParentId = "BJvtAuvGHHvsAFEAsIsEHJGCGFEuwEJIuv";
        public const string ChangedBy = "BJtGHvJBDJwGDrECHDJIFuuuJvCvGJBwDt";
        public const string AccessRight = "BJvtGAIBFGCrwrEDuHIHBDJGvIIGvCuvrB";
        public const string Name = "BJuwwtCHEFBvwsEHIwssuDCrsGrwrEAuvt";
        public const string Enable = "BJHFEJJtBDAEABEAJFsFJBIswEJCAtHAIB";
        public const string Visible = "BJAuHGvwFFHFGtEIDtrvuttGGrturtGtAt";
        public const string TableType = "BJFFCIEtCBArEGEIEBJwDtGrtvAtwEDDwC";
        public const string Lock = "CAJAEuGGIvEtEsEBGAsDwEAwvuCrAArHvD";
        public const string RealTime = "BJFGHJuIrBwHHHEJGCrwsuBHFFCuCrAtJu";
        public const string LeftColumnsFixed = "BJAtEAtIBEADEvEEHHIEEAwFJFswGtsJvu";
        public const string RightColumnsFixed = "BJEvEBHCruAGBBEwGwJIJHsuBIEtGsDuIA";
        public const string UniquesList = "BJvwGIJAFGGAFEEIHHrsvuBCDrBFsFsGHG";
        public const string DependentField = "CAsBvGwuvFwvDGEFwtJGsHuwADwAAECGIH";
        public const string TextOnly = "BJCwAFDDIFGuCAEBGCrHwwHwGHAtAHtIIs";
        public const string SelfReference = "BJHGtvwuIDtEtDEwrFIFGuECvGIGsBvJAv";
        public const string BasicTable = "BJwuFwwDwABABAEErHrIwBGABFBABAtJIt";
        
    }
    
    
    public TableProperty()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = @"175b8bb8e76b8f4802acd02adb49450004";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""],""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Delete"",""Modify""],""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Delete"",""Modify""]}");
        _Name = "";
        _Enable = true;
        _Visible = true;
        _TableType = "";
        _Lock = false;
        _RealTime = false;
        _LeftColumnsFixed = 1;
        _RightColumnsFixed = 0;
        _UniquesList = new Dictionary<string,List<string>>();
        _DependentField = new Dictionary<string,List<string>>();
        _TextOnly = false;
        _SelfReference = false;
        _BasicTable = false;
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJCuCBrHFGFAJrEDDGIFtJAuHDCruBFBsB";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private bool _Enable { set; get; }
    [JsonProperty(FieldsId.Enable)] 
    public bool Enable { set { Changed[FieldsId.Enable] = "true"; _Enable = value; } get { return _Enable ; } }
    
    private bool _Visible { set; get; }
    [JsonProperty(FieldsId.Visible)] 
    public bool Visible { set { Changed[FieldsId.Visible] = "true"; _Visible = value; } get { return _Visible ; } }
    
    private string _TableType { set; get; }
    [JsonProperty(FieldsId.TableType)] 
    public string TableType { set { Changed[FieldsId.TableType] = "true"; _TableType = value; } get { return _TableType ; } }
    
    private bool _Lock { set; get; }
    [JsonProperty(FieldsId.Lock)] 
    public bool Lock { set { Changed[FieldsId.Lock] = "true"; _Lock = value; } get { return _Lock ; } }
    
    private bool _RealTime { set; get; }
    [JsonProperty(FieldsId.RealTime)] 
    public bool RealTime { set { Changed[FieldsId.RealTime] = "true"; _RealTime = value; } get { return _RealTime ; } }
    
    private int _LeftColumnsFixed { set; get; }
    [JsonProperty(FieldsId.LeftColumnsFixed)] 
    public int LeftColumnsFixed { set { Changed[FieldsId.LeftColumnsFixed] = "true"; _LeftColumnsFixed = value; } get { return _LeftColumnsFixed ; } }
    
    private int _RightColumnsFixed { set; get; }
    [JsonProperty(FieldsId.RightColumnsFixed)] 
    public int RightColumnsFixed { set { Changed[FieldsId.RightColumnsFixed] = "true"; _RightColumnsFixed = value; } get { return _RightColumnsFixed ; } }
    
    private Dictionary<string, List<string>> _UniquesList { set; get; }
    [JsonProperty(FieldsId.UniquesList)] 
    public Dictionary<string, List<string>> UniquesList { set { Changed[FieldsId.UniquesList] = "true"; _UniquesList = value; } get { return _UniquesList ; } }
    
    private Dictionary<string, List<string>> _DependentField { set; get; }
    [JsonProperty(FieldsId.DependentField)] 
    public Dictionary<string, List<string>> DependentField { set { Changed[FieldsId.DependentField] = "true"; _DependentField = value; } get { return _DependentField ; } }
    
    private bool _TextOnly { set; get; }
    [JsonProperty(FieldsId.TextOnly)] 
    public bool TextOnly { set { Changed[FieldsId.TextOnly] = "true"; _TextOnly = value; } get { return _TextOnly ; } }
    
    private bool _SelfReference { set; get; }
    [JsonProperty(FieldsId.SelfReference)] 
    public bool SelfReference { set { Changed[FieldsId.SelfReference] = "true"; _SelfReference = value; } get { return _SelfReference ; } }
    
    private bool _BasicTable { set; get; }
    [JsonProperty(FieldsId.BasicTable)] 
    public bool BasicTable { set { Changed[FieldsId.BasicTable] = "true"; _BasicTable = value; } get { return _BasicTable ; } }
    
}
