using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Scheduler
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJDstHtsJCwGvvEFrHIJIFBJCAtHHFCEAw";
        public const string RowNo = "BJBvvDrCwurAJIEBrCJurHEvBBCFAtvJGr";
        public const string DateVersion = "BJCuJJsAIJBrrFErIAIEAwBrtDCJFFuABF";
        public const string id = "id";
        public const string ParentId = "BJvuIHwDCsJwGAEwuBrtrACtJFBIJBBtBt";
        public const string ChangedBy = "BJGvIFwJIvuJGEEGtHJEuHCABGJwrvwDJr";
        public const string AccessRight = "BJFBusrJErsvHtEAEwJuusBAvrsGwHIuGG";
        public const string Name = "BJDGtsrDGGsBDvEwsusADGJtvsFuuDwvDF";
        public const string Enable = "BJBFutuEtrwutuEDJEsBJHwvIwsJwJustE";
        public const string ExecutionTime = "BJwJuIJBswtJIsEuCsIJsrIvwvvBwHDIDr";
        public const string Repeat = "BJtGIvHwvwBvGDEBIvstuFtvuIAIwCCCtI";
        public const string EndDate = "BJIvBvDrtGvAJAEGvwJHHtJsGtCBwJwADs";
        public const string StartDate = "BJGCvICGrtvCIDEvrwsAICtwwIDvurrErs";
        public const string Years = "BJCuFAFEDEBCttEJGEsJIvEBsDFIrBEurv";
        public const string Months = "BJCsAIurDAHFACEIFrJEsACsItJurBAJAG";
        public const string Days = "BJBCsFtADGDDwtEHFtIsrEHtDJBBHJFFGu";
        public const string Hours = "BJBGArJFBJBGDAEIsrssFBAAEHJFtJAAvv";
        public const string Minutes = "BJICCIBGGuDuswEBICsDJDtDFDAvstDCGB";
        public const string Seconds = "BJCstuvGstwDvvEIGGsHtDIvvIGJFFCAGr";
        public const string Miliseconds = "BJGEuIBFGrvwCuEErDIFIrvtJDHsCGFAJI";
        
    }
    
    
    public Scheduler()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        _Enable = false;
        _ExecutionTime = Convert.ToDateTime("2016-Dec-01 12: 00: 00 am");
        _Repeat = true;
        _EndDate = DateTime.Now;
        _StartDate = DateTime.Now;
        _Years = 0;
        _Months = 0;
        _Days = 0;
        _Hours = 0;
        _Minutes = 0;
        _Seconds = 10;
        _Miliseconds = 500;
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJACttGIJJFAutEwuwrHFAuuFArABvrHCH";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private bool _Enable { set; get; }
    [JsonProperty(FieldsId.Enable)] 
    public bool Enable { set { Changed[FieldsId.Enable] = "true"; _Enable = value; } get { return _Enable ; } }
    
    private DateTime _ExecutionTime { set; get; }
    [JsonProperty(FieldsId.ExecutionTime)] 
    public DateTime ExecutionTime { set { Changed[FieldsId.ExecutionTime] = "true"; _ExecutionTime = value; } get { return _ExecutionTime ; } }
    
    private bool _Repeat { set; get; }
    [JsonProperty(FieldsId.Repeat)] 
    public bool Repeat { set { Changed[FieldsId.Repeat] = "true"; _Repeat = value; } get { return _Repeat ; } }
    
    private DateTime _EndDate { set; get; }
    [JsonProperty(FieldsId.EndDate)] 
    public DateTime EndDate { set { Changed[FieldsId.EndDate] = "true"; _EndDate = value; } get { return _EndDate ; } }
    
    private DateTime _StartDate { set; get; }
    [JsonProperty(FieldsId.StartDate)] 
    public DateTime StartDate { set { Changed[FieldsId.StartDate] = "true"; _StartDate = value; } get { return _StartDate ; } }
    
    private int _Years { set; get; }
    [JsonProperty(FieldsId.Years)] 
    public int Years { set { Changed[FieldsId.Years] = "true"; _Years = value; } get { return _Years ; } }
    
    private int _Months { set; get; }
    [JsonProperty(FieldsId.Months)] 
    public int Months { set { Changed[FieldsId.Months] = "true"; _Months = value; } get { return _Months ; } }
    
    private int _Days { set; get; }
    [JsonProperty(FieldsId.Days)] 
    public int Days { set { Changed[FieldsId.Days] = "true"; _Days = value; } get { return _Days ; } }
    
    private int _Hours { set; get; }
    [JsonProperty(FieldsId.Hours)] 
    public int Hours { set { Changed[FieldsId.Hours] = "true"; _Hours = value; } get { return _Hours ; } }
    
    private int _Minutes { set; get; }
    [JsonProperty(FieldsId.Minutes)] 
    public int Minutes { set { Changed[FieldsId.Minutes] = "true"; _Minutes = value; } get { return _Minutes ; } }
    
    private int _Seconds { set; get; }
    [JsonProperty(FieldsId.Seconds)] 
    public int Seconds { set { Changed[FieldsId.Seconds] = "true"; _Seconds = value; } get { return _Seconds ; } }
    
    private int _Miliseconds { set; get; }
    [JsonProperty(FieldsId.Miliseconds)] 
    public int Miliseconds { set { Changed[FieldsId.Miliseconds] = "true"; _Miliseconds = value; } get { return _Miliseconds ; } }
    
}
