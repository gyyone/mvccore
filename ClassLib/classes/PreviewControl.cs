using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class PreviewControl
{    
    
    
    public struct FieldsId
    {
        public const string Button = "BJDuDAwwruwBwsEHsCIAsHEGtvuwBusAwB";
        public const string RowNo = "BJACruFrrCDsCAECHBsvBBFGwGFtuFDGBw";
        public const string TextString = "CAAItwtJFsABIAEJCDswvEJAvsGuIsrwDt";
        public const string DateVersion = "BJBuGurGwIvwDCEIEGrIsFuBwFswEsDGuD";
        public const string id = "id";
        public const string ParentId = "BJvHEHJJustDswEAsGJrIAwuwsGGGDHDvs";
        public const string ChangedBy = "BJIHDJGDFJsIArEvEtsFCJFswvsGwCBGCv";
        public const string TextGeneral = "BJJIBtuGBHvrBBEIBHsItuDCvwCuvCtAAJ";
        public const string AccessRight = "BJAEtGsvGwsIFwEEHwsAFAHGtBICFCBDEr";
        public const string CheckBox = "BJvrBJswrGvAICEDCwJADBGJJDtGHDBIFs";
        public const string SwitchButton = "BJAwBJsEsCHDHAEwsIrACttHAtEHJvEwCH";
        public const string ComboBox = "BJsHHutAvrEBGBEFABIDIEABJCCwFrGJCB";
        public const string ComboBoxMultiselect = "BJvtCwssItuCBAEtHIItFJtvFsGwICuAwE";
        public const string ComboBoxSingleKeyValue = "CAEDrEABJJtwItEGuwItJrtJvvrrAutCtr";
        public const string DefaultEditor = "BJBGsrrHDvsrsvEstrJurHEGAHDwuBswID";
        public const string ColorPicker = "BJIvrJAGHBACHIECursvtIIuwrtsuFIABB";
        public const string CodeEditor = "BJtuFGIGDIuCDJEBsuIwArwBEACHwrvuGs";
        public const string DatetimePicker = "BJGCDIHDAAtFJFECtDIGHtCDBCIBsIwGIH";
        public const string RadioButton = "BJtusvrsBwAJswEECJssvFFHHErIEJJBDB";
        public const string CheckListTable = "BJvstIAwCGEvIwErCwrsIvAJJBEGBEtsww";
        public const string CheckList = "BJCIIwvBBrGuuEEIEHIvIHsvGwAGJuAAHG";
        public const string InputMask = "BJGrwJEHAsItGsEBrIrtstvFAEvIIDwwED";
        public const string FileUploadMultiple = "CAFEHIFBvAwIwtEwJvsvwvIuEHtEFDEEtI";
        public const string FileUploadPUblic = "BJFIAIrJJBFErwEtAAJFEtGCrFHwDvwrFC";
        public const string SourceEditor = "BJsttwwtvtuDsDEuAHsvAJIwDFJEDsBHGu";
        public const string SortableComboBox = "BJEFBHAvHJHJArEAuJIAuEJtHEusttJJrr";
        public const string TestMask = "BJCFsFJvstJGvwEFEvsIuIHAGsFsIGrAuC";
        public const string EncryptText = "BJFBrvAsCFGwvwEFHwJEwGsJEwFAAEJFvs";
        
    }
    
    
    public PreviewControl()
    {
        _Button = "";
        _RowNo = 0;
        _TextString = "";
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _TextGeneral = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Modify"",""Delete""],""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Modify"",""Delete""],""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Delete"",""Modify""]}");
        _CheckBox = true;
        _SwitchButton = false;
        _ComboBox = @"17cf66df6ab13a47acaacb5cbf0bca5006";
        _ComboBoxMultiselect = JsonConvert.DeserializeObject<List<string>>(@"[""17cf66df6ab13a47acaacb5cbf0bca5006"",""17cf66df6ab13a47acaacb5cbf0bca0002""]");
        _ComboBoxSingleKeyValue = new List<string>();
        _DefaultEditor = @"";
        _ColorPicker = @"";
        _CodeEditor = new CodeEditor("");
        _DatetimePicker = DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
        _RadioButton = @"";
        _CheckListTable = new List<string>();
        _CheckList = new List<string>();
        _InputMask = "";
        _FileUploadMultiple = "";
        _FileUploadPUblic = "";
        _SourceEditor = @"";
        _SortableComboBox = new List<string>();
        _TestMask = "";
        _EncryptText = @"";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJFruwAJEIIBGsEBIssvADvtGGsAHAHBtG";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private string _TextString { set; get; }
    [JsonProperty(FieldsId.TextString)] 
    public string TextString { set { Changed[FieldsId.TextString] = "true"; _TextString = value; } get { return _TextString ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private string _TextGeneral { set; get; }
    [JsonProperty(FieldsId.TextGeneral)] 
    public string TextGeneral { set { Changed[FieldsId.TextGeneral] = "true"; _TextGeneral = value; } get { return _TextGeneral ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private bool _CheckBox { set; get; }
    [JsonProperty(FieldsId.CheckBox)] 
    public bool CheckBox { set { Changed[FieldsId.CheckBox] = "true"; _CheckBox = value; } get { return _CheckBox ; } }
    
    private bool _SwitchButton { set; get; }
    [JsonProperty(FieldsId.SwitchButton)] 
    public bool SwitchButton { set { Changed[FieldsId.SwitchButton] = "true"; _SwitchButton = value; } get { return _SwitchButton ; } }
    
    private string _ComboBox { set; get; }
    [JsonProperty(FieldsId.ComboBox)] 
    public string ComboBox { set { Changed[FieldsId.ComboBox] = "true"; _ComboBox = value; } get { return _ComboBox ; } }
    
    private List<string> _ComboBoxMultiselect { set; get; }
    [JsonProperty(FieldsId.ComboBoxMultiselect)] 
    public List<string> ComboBoxMultiselect { set { Changed[FieldsId.ComboBoxMultiselect] = "true"; _ComboBoxMultiselect = value; } get { return _ComboBoxMultiselect ; } }
    
    private List<string> _ComboBoxSingleKeyValue { set; get; }
    [JsonProperty(FieldsId.ComboBoxSingleKeyValue)] 
    public List<string> ComboBoxSingleKeyValue { set { Changed[FieldsId.ComboBoxSingleKeyValue] = "true"; _ComboBoxSingleKeyValue = value; } get { return _ComboBoxSingleKeyValue ; } }
    
    private string _DefaultEditor { set; get; }
    [JsonProperty(FieldsId.DefaultEditor)] 
    public string DefaultEditor { set { Changed[FieldsId.DefaultEditor] = "true"; _DefaultEditor = value; } get { return _DefaultEditor ; } }
    
    private string _ColorPicker { set; get; }
    [JsonProperty(FieldsId.ColorPicker)] 
    public string ColorPicker { set { Changed[FieldsId.ColorPicker] = "true"; _ColorPicker = value; } get { return _ColorPicker ; } }
    
    private CodeEditor _CodeEditor { set; get; }
    [JsonProperty(FieldsId.CodeEditor)] 
    public CodeEditor CodeEditor { set { Changed[FieldsId.CodeEditor] = "true"; _CodeEditor = value; } get { return _CodeEditor ; } }
    
    private DateTime _DatetimePicker { set; get; }
    [JsonProperty(FieldsId.DatetimePicker)] 
    public DateTime DatetimePicker { set { Changed[FieldsId.DatetimePicker] = "true"; _DatetimePicker = value; } get { return _DatetimePicker ; } }
    
    private string _RadioButton { set; get; }
    [JsonProperty(FieldsId.RadioButton)] 
    public string RadioButton { set { Changed[FieldsId.RadioButton] = "true"; _RadioButton = value; } get { return _RadioButton ; } }
    
    private List<string> _CheckListTable { set; get; }
    [JsonProperty(FieldsId.CheckListTable)] 
    public List<string> CheckListTable { set { Changed[FieldsId.CheckListTable] = "true"; _CheckListTable = value; } get { return _CheckListTable ; } }
    
    private List<string> _CheckList { set; get; }
    [JsonProperty(FieldsId.CheckList)] 
    public List<string> CheckList { set { Changed[FieldsId.CheckList] = "true"; _CheckList = value; } get { return _CheckList ; } }
    
    private string _InputMask { set; get; }
    [JsonProperty(FieldsId.InputMask)] 
    public string InputMask { set { Changed[FieldsId.InputMask] = "true"; _InputMask = value; } get { return _InputMask ; } }
    
    private string _FileUploadMultiple { set; get; }
    [JsonProperty(FieldsId.FileUploadMultiple)] 
    public string FileUploadMultiple { set { Changed[FieldsId.FileUploadMultiple] = "true"; _FileUploadMultiple = value; } get { return _FileUploadMultiple ; } }
    
    private string _FileUploadPUblic { set; get; }
    [JsonProperty(FieldsId.FileUploadPUblic)] 
    public string FileUploadPUblic { set { Changed[FieldsId.FileUploadPUblic] = "true"; _FileUploadPUblic = value; } get { return _FileUploadPUblic ; } }
    
    private string _SourceEditor { set; get; }
    [JsonProperty(FieldsId.SourceEditor)] 
    public string SourceEditor { set { Changed[FieldsId.SourceEditor] = "true"; _SourceEditor = value; } get { return _SourceEditor ; } }
    
    private List<string> _SortableComboBox { set; get; }
    [JsonProperty(FieldsId.SortableComboBox)] 
    public List<string> SortableComboBox { set { Changed[FieldsId.SortableComboBox] = "true"; _SortableComboBox = value; } get { return _SortableComboBox ; } }
    
    private string _TestMask { set; get; }
    [JsonProperty(FieldsId.TestMask)] 
    public string TestMask { set { Changed[FieldsId.TestMask] = "true"; _TestMask = value; } get { return _TestMask ; } }
    
    private string _EncryptText { set; get; }
    [JsonProperty(FieldsId.EncryptText)] 
    public string EncryptText { set { Changed[FieldsId.EncryptText] = "true"; _EncryptText = value; } get { return _EncryptText ; } }
    
}
