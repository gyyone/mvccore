using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class WebLibrary
{    
    
    
    public struct FieldsId
    {
        public const string Button = "CAttBBsJFvsHsIEwwwrHDHJBFrtGruEAvE";
        public const string RowNo = "CADHrrrwCIrADwEGsrsBEAEFJwtBwuJrJw";
        public const string DateVersion = "CAtrvFJuJCrDEIErCJJCIBwBvsAuwssvEF";
        public const string id = "id";
        public const string ParentId = "CAICJIGADJHuuCEwCErsJEsDHHDEwutuDw";
        public const string ChangedBy = "CAsJBurwsFuuGAEFEsJJrBJvEGvBGsEuJu";
        public const string Files = "CADwDrruEFAswJEFHHrIvtvErIvIsAJCsH";
        public const string AccessRight = "CAFIJrrJvGACJAEwCDsFvJuuDGtFFwuDrs";
        public const string Name = "CAwICIBuIJFBHvEGJrrrIsHHsBHtIuIuHw";
        public const string Group = "CAGrGHEErtCCuwEJwBrwCwvCHsCFvBHuDH";
        public const string Version = "CAvJwHCHvsFsvBEICAIDACtrFuuAIHJBDH";
        public const string CssReference = "CABuGHtCuEFCDFEwDEItwvDCJIIBFvIAAw";
        public const string ScriptReference = "CAIrusvsuEvsCIEJssJwCIEDEHACwvHFDF";
        
    }
    
    
    public WebLibrary()
    {
        _Button = "";
        _RowNo = 0;

    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "CAAvDEArtswuttECBDrsFBEvrAEFDttAtJ";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private List<string> _Files { set; get; }
    [JsonProperty(FieldsId.Files)] 
    public List<string> Files { set { Changed[FieldsId.Files] = "true"; _Files = value; } get { return _Files ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private List<string> _Group { set; get; }
    [JsonProperty(FieldsId.Group)] 
    public List<string> Group { set { Changed[FieldsId.Group] = "true"; _Group = value; } get { return _Group ; } }
    
    private string _Version { set; get; }
    [JsonProperty(FieldsId.Version)] 
    public string Version { set { Changed[FieldsId.Version] = "true"; _Version = value; } get { return _Version ; } }
    
    private List<string> _CssReference { set; get; }
    [JsonProperty(FieldsId.CssReference)] 
    public List<string> CssReference { set { Changed[FieldsId.CssReference] = "true"; _CssReference = value; } get { return _CssReference ; } }
    
    private List<string> _ScriptReference { set; get; }
    [JsonProperty(FieldsId.ScriptReference)] 
    public List<string> ScriptReference { set { Changed[FieldsId.ScriptReference] = "true"; _ScriptReference = value; } get { return _ScriptReference ; } }
    
}
