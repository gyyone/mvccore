using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Grouping
{    
    
    
    public struct FieldsId
    {
        public const string Button = "CAJAstAvJACvtrEEGrrEBtvrEGuDHvArHD";
        public const string RowNo = "CAtFHAvDBBAIrEECvDsuEGJtwHIIwAJHwA";
        public const string DateVersion = "CAAIwtBDHGvCsCEwtrJtGEDrwBDJsIsGsB";
        public const string id = "id";
        public const string ParentId = "CAFtvDCCAIEHvAEEGBIvtsCvFtvCHtCFCu";
        public const string ChangedBy = "CADvuBwGDvuwHsEurIsrIHGrEEJEJsrAEv";
        public const string AccessRight = "CAvHtCtAuEurGsEwBHrGtFDEEJHrFtBvrE";
        public const string GroupName = "CArEsBHvIAJGFsEEAIsJHGFGsEvGHCButC";
        public const string GroupLabel = "CAFFAHtIIsuuvwEGwDJDCrGEGHrJIJwBvF";
        public const string Icon = "CADsErIIDwuGAGEJsvItuJAvJwFrtwAtJC";
        public const string GroupDesc = "CADGACwtCvDGIwEttJrHrFBDCCuIHBvsDG";
        
    }
    
    
    public Grouping()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _GroupName = "";
        _GroupLabel = new List<string>();
        _Icon = @"acceaf57-4673-4891-81de-5f159f917526";
        _GroupDesc = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "CAGAErEsGvJrvsEJJwsuutCIvDtAJrIHFs";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _GroupName { set; get; }
    [JsonProperty(FieldsId.GroupName)] 
    public string GroupName { set { Changed[FieldsId.GroupName] = "true"; _GroupName = value; } get { return _GroupName ; } }
    
    private List<string> _GroupLabel { set; get; }
    [JsonProperty(FieldsId.GroupLabel)] 
    public List<string> GroupLabel { set { Changed[FieldsId.GroupLabel] = "true"; _GroupLabel = value; } get { return _GroupLabel ; } }
    
    private string _Icon { set; get; }
    [JsonProperty(FieldsId.Icon)] 
    public string Icon { set { Changed[FieldsId.Icon] = "true"; _Icon = value; } get { return _Icon ; } }
    
    private string _GroupDesc { set; get; }
    [JsonProperty(FieldsId.GroupDesc)] 
    public string GroupDesc { set { Changed[FieldsId.GroupDesc] = "true"; _GroupDesc = value; } get { return _GroupDesc ; } }
    
}
