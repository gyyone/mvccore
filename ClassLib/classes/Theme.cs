using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class Theme
{    
    
    
    public struct FieldsId
    {
        public const string Button = "CAvGACvIAIICICEFItJsHtIErvHAJsAFrI";
        public const string RowNo = "CAwAEDCJwuuEECEwDuswtusAHIJDBJDEsE";
        public const string DateVersion = "CACHJBIHArrCAEEEsurtHDsrHGvDsAvuwJ";
        public const string AccessRight = "CArrBHuDHHwvrAEHrtJJsFtCwBrBvuvFIr";
        public const string Name = "CAsBDIwDwGFwvJEBCuJGrwCDIDuDrswvwr";
        public const string Header = "CAutEIJstEHAIAEDGJsuArBGCsHuDvCBuv";
        public const string Sidebar = "CAGwAJtEsDwrJrECursJGHIwAvEIEDuIrJ";
        public const string ScriptBody = "CAtsEDrrGuvCsAEswDJJAFusJBEvCDAJHI";
        public const string CssBody = "CADHJCICIJECJGEBIHssItDIIvEtFHHABu";
        public const string MainContent = "CAvsDvGAICBGFGEHwEJAGIFsECsCJvAGCC";
        public const string Design = "CAFwACDsFuBFFtEBwrIuEuJwJIDAHDDvGF";
        public const string id = "id";
        public const string ParentId = "CAtvwwHCsCtFJEEJFIJrGrGtHrJvHGECGv";
        public const string ChangedBy = "CAICGwwtrAvuwCEGAtIJDJtCtFuuGvJwFJ";
        
    }
    
    
    public Theme()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        _Header = @"CAIrJJEFHuuDwvEArvJIrCrEstFtJrrBAs";
        _Sidebar = @"CAHtvJGBBwrFHCEGvrIuGAHAAIGBrtJGwA";
        _ScriptBody = @"CAAurvCrrADAuHEEJwsvrJrwGGIGBAwDuE";
        _CssBody = @"CAEEwtDHrEDDJFEABwIHDDJFJDEBBCuHBt";
        _MainContent = @"CAtHBsBHDBJvCAEIGJJJCJEDvrHvrJvHEI";
        _Design = @"CAFvtEIJsCwJIHEvBFIrsvHHFCsvFFvwrw";
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "CAHDDEDvuACBHtEIuGrDGCsFIIGtrFuBII";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
    private string _Header { set; get; }
    [JsonProperty(FieldsId.Header)] 
    public string Header { set { Changed[FieldsId.Header] = "true"; _Header = value; } get { return _Header ; } }
    
    private string _Sidebar { set; get; }
    [JsonProperty(FieldsId.Sidebar)] 
    public string Sidebar { set { Changed[FieldsId.Sidebar] = "true"; _Sidebar = value; } get { return _Sidebar ; } }
    
    private string _ScriptBody { set; get; }
    [JsonProperty(FieldsId.ScriptBody)] 
    public string ScriptBody { set { Changed[FieldsId.ScriptBody] = "true"; _ScriptBody = value; } get { return _ScriptBody ; } }
    
    private string _CssBody { set; get; }
    [JsonProperty(FieldsId.CssBody)] 
    public string CssBody { set { Changed[FieldsId.CssBody] = "true"; _CssBody = value; } get { return _CssBody ; } }
    
    private string _MainContent { set; get; }
    [JsonProperty(FieldsId.MainContent)] 
    public string MainContent { set { Changed[FieldsId.MainContent] = "true"; _MainContent = value; } get { return _MainContent ; } }
    
    private string _Design { set; get; }
    [JsonProperty(FieldsId.Design)] 
    public string Design { set { Changed[FieldsId.Design] = "true"; _Design = value; } get { return _Design ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
}
