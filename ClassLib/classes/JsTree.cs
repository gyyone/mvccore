using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class JsTree
{    
    
    
    public struct FieldsId
    {
        public const string Button = "CAJtJvDAwCBwvEEDFHrCGvsFHwwDDIHuwF";
        public const string DateVersion = "CADDrHIrHrrAtwECvCJCFADIvEsBvtEIGB";
        public const string id = "id";
        public const string ParentId = "CAGvvuAwrGrEGsEuDIJvIFvHIuuEJBuGJB";
        public const string ChangedBy = "CAFBDAHHIEGAAwEEDwrtFFrtsFtCFHvuBv";
        public const string NewField = "CAruvvrGJCrDIFECwCsvGDIBGFGwIItDIt";
        public const string AccessRight = "CAGvBJJrEIJDGuEsIFIuEFFJJwFErGDJuv";
        
    }
    
    
    public JsTree()
    {
        _Button = "";
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = @"175b8bb8e76b8f4802acd02adb49450004";
        _NewField = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Delete"",""Modify""],""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Delete"",""Modify""]}");
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "CAGBJHCsvutsGGEHFvsFADuvJCwJBDCJAw";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed["CAJtJvDAwCBwvEEDFHrCGvsFHwwDDIHuwF"] = "true"; _Button = value; } get { return _Button ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed["CADDrHIrHrrAtwECvCJCFADIvEsBvtEIGB"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed["CAGvvuAwrGrEGsEuDIJvIFvHIuuEJBuGJB"] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed["CAFBDAHHIEGAAwEEDwrtFFrtsFtCFHvuBv"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private string _NewField { set; get; }
    [JsonProperty(FieldsId.NewField)] 
    public string NewField { set { Changed["CAruvvrGJCrDIFECwCsvGDIBGFGwIItDIt"] = "true"; _NewField = value; } get { return _NewField ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed["CAGvBJJrEIJDGuEsIFIuEFFJJwFErGDJuv"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
}
