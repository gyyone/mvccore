using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Util;
using System.Linq;

using Newtonsoft.Json.Converters;
using ClassLib;
public partial class Sellers
{    
    public partial class Product
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJwCrtDIwFJJJEErsAIsCDtJFDBAwuJtws";
            public const string RowNo = "BJGGHDrtvIGGrHEwsrJEHBIFJwuuFGrAEt";
            public const string DateVersion = "BJFBJsDBCEvFwBEIIustArHIuEIuCCtDAG";
            public const string id = "id";
            public const string ParentId = "BJrvtHACCDwwuCEwvJJEDGBFBCCIvDHtHt";
            public const string ChangedBy = "BJDDJtwCItEuDvEvurJsBFCvAGAAwHrAsr";
            public const string ProductUrl = "BJFBEFFBEErFEvECAwrHvDsrJBCAvuvvwE";
            public const string Rate = "BJFCHrsEJEDHIwEwGCrAAEuutsAJDHtHFH";
            public const string Price = "BJuCBuBHsAAEHwEvGvrtJCFDBCDrrGssAs";
            public const string DeliveryFees = "BJwDHEuHsIJEIIEIrCsJsDuGJtBrHGvEsD";
            public const string Name = "BJswwEvCuCGFIFEuIssIAtJCHHEDEvJIrr";
            public const string Seller = "BJItsvCrEHsAtvEFGuIuCvBwIHFwsCJHuE";
            public const string UrlVendor = "BJDrHvEtHIAIAFECABrDJCsCBvrHwIABtG";
            
        }
        
        
        public Product()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _ProductUrl = @"";
            _Rate = 0;
            _Price = 0;
            _DeliveryFees = 0;
            _Name = "";
            _Seller = "";
            _UrlVendor = "";
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJEuIsrFDECIFrEJtsItwFtFwsBstuAtII";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed["BJwCrtDIwFJJJEErsAIsCDtJFDBAwuJtws"] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed["BJGGHDrtvIGGrHEwsrJEHBIFJwuuFGrAEt"] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed["BJFBJsDBCEvFwBEIIustArHIuEIuCCtDAG"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed["BJrvtHACCDwwuCEwvJJEDGBFBCCIvDHtHt"] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed["BJDDJtwCItEuDvEvurJsBFCvAGAAwHrAsr"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private string _ProductUrl { set; get; }
        [JsonProperty(FieldsId.ProductUrl)] 
        public string ProductUrl { set { Changed["BJFBEFFBEErFEvECAwrHvDsrJBCAvuvvwE"] = "true"; _ProductUrl = value; } get { return _ProductUrl ; } }
        
        private double _Rate { set; get; }
        [JsonProperty(FieldsId.Rate)] 
        public double Rate { set { Changed["BJFCHrsEJEDHIwEwGCrAAEuutsAJDHtHFH"] = "true"; _Rate = value; } get { return _Rate ; } }
        
        private double _Price { set; get; }
        [JsonProperty(FieldsId.Price)] 
        public double Price { set { Changed["BJuCBuBHsAAEHwEvGvrtJCFDBCDrrGssAs"] = "true"; _Price = value; } get { return _Price ; } }
        
        private double _DeliveryFees { set; get; }
        [JsonProperty(FieldsId.DeliveryFees)] 
        public double DeliveryFees { set { Changed["BJwDHEuHsIJEIIEIrCsJsDuGJtBrHGvEsD"] = "true"; _DeliveryFees = value; } get { return _DeliveryFees ; } }
        
        private string _Name { set; get; }
        [JsonProperty(FieldsId.Name)] 
        public string Name { set { Changed["BJswwEvCuCGFIFEuIssIAtJCHHEDEvJIrr"] = "true"; _Name = value; } get { return _Name ; } }
        
        private string _Seller { set; get; }
        [JsonProperty(FieldsId.Seller)] 
        public string Seller { set { Changed["BJItsvCrEHsAtvEFGuIuCvBwIHFwsCJHuE"] = "true"; _Seller = value; } get { return _Seller ; } }
        
        private string _UrlVendor { set; get; }
        [JsonProperty(FieldsId.UrlVendor)] 
        public string UrlVendor { set { Changed["BJDrHvEtHIAIAFECABrDJCsCBvrHwIABtG"] = "true"; _UrlVendor = value; } get { return _UrlVendor ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJGtAEGBssAHBAEJwvrIuGtGJHJutFwHIH";
        public const string RowNo = "BJvsAtJsvGvCAIEDDrrFtwFCvuCBBuvJus";
        public const string DateVersion = "BJDwuHCIGErswBEEIBJtBEutJuHGDAvtBI";
        public const string id = "id";
        public const string ParentId = "BJHwuHEvFDAEtBEtJsJFFrDtAsrCEtIDFG";
        public const string ChangedBy = "BJrFIAFuAGvHAGEJDtItCtIAEHIGuCrruI";
        public const string AccessRight = "BJuJDAJAFrHvwHEHuurwHHIIGEEFABABBE";
        public const string Name = "BJEswtsGJrvFrCEHHwrtuBwuJDGHJCHCGs";
        public const string WebSite = "BJAJIIJtEtAAJwEstIstAuABuBuAwDEEDr";
        
    }
    
    
    public Sellers()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        _WebSite = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJBCACHDErBusEEvDAICEuvHFIDwsDHrBD";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed["BJGtAEGBssAHBAEJwvrIuGtGJHJutFwHIH"] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed["BJvsAtJsvGvCAIEDDrrFtwFCvuCBBuvJus"] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed["BJDwuHCIGErswBEEIBJtBEutJuHGDAvtBI"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed["BJHwuHEvFDAEtBEtJsJFFrDtAsrCEtIDFG"] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed["BJrFIAFuAGvHAGEJDtItCtIAEHIGuCrruI"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed["BJuJDAJAFrHvwHEHuurwHHIIGEEFABABBE"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed["BJEswtsGJrvFrCEHHwrtuBwuJDGHJCHCGs"] = "true"; _Name = value; } get { return _Name ; } }
    
    private string _WebSite { set; get; }
    [JsonProperty(FieldsId.WebSite)] 
    public string WebSite { set { Changed["BJAJIIJtEtAAJwEstIstAuABuBuAwDEEDr"] = "true"; _WebSite = value; } get { return _WebSite ; } }
    
}
