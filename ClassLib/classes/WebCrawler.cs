using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Util;
using System.Linq;

using Newtonsoft.Json.Converters;
using ClassLib;
public partial class WebCrawler
{    
    public partial class Products
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJCtDGDAHswDBBEwvFJHCsvIAEuJtrwDDD";
            public const string RowNo = "BJHFBrrIsErIHvEEtCrFstswsHFIEJtHDt";
            public const string DateVersion = "BJtvEGHECAsECHECwAIrBBGvvGFJrCIGrv";
            public const string id = "id";
            public const string ParentId = "BJHIBJBGAAwuCvEtDsIDvBBGvDBuDEuBCG";
            public const string ChangedBy = "BJFuAFJtAwwHuIEwEIIrtBuHwGrDGBsFsA";
            public const string AccessRight = "BJsCIBwHFsCHHtEuHvsFuDGvCGuJDIGsGv";
            public const string Url = "BJFuwDwGHFICrvErwJsHDvFBtHBDBEvsGJ";
            public const string ProcessedOn = "BJErIDICwJGuutEBIwIBJFDtAIEtFwBwsC";
            
        }
        
        
        public Products()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _Url = "";
            _ProcessedOn = DateTime.Now;
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJEFrDHsAJtwrsEHCIJsFArwCGJGtHtrvu";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed["BJCtDGDAHswDBBEwvFJHCsvIAEuJtrwDDD"] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed["BJHFBrrIsErIHvEEtCrFstswsHFIEJtHDt"] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed["BJtvEGHECAsECHECwAIrBBGvvGFJrCIGrv"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed["BJHIBJBGAAwuCvEtDsIDvBBGvDBuDEuBCG"] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed["BJFuAFJtAwwHuIEwEIIrtBuHwGrDGBsFsA"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed["BJsCIBwHFsCHHtEuHvsFuDGvCGuJDIGsGv"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _Url { set; get; }
        [JsonProperty(FieldsId.Url)] 
        public string Url { set { Changed["BJFuwDwGHFICrvErwJsHDvFBtHBDBEvsGJ"] = "true"; _Url = value; } get { return _Url ; } }
        
        private DateTime _ProcessedOn { set; get; }
        [JsonProperty(FieldsId.ProcessedOn)] 
        public DateTime ProcessedOn { set { Changed["BJErIDICwJGuutEBIwIBJFDtAIEtFwBwsC"] = "true"; _ProcessedOn = value; } get { return _ProcessedOn ; } }
        
    }
    public partial class ElementHide
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJruJEHICBJAJFEHwCJHGwIrDJDHDIFtHE";
            public const string RowNo = "BJvBFIrHwItGHJEFCtrDsvGEwHIAsCFDtD";
            public const string DateVersion = "BJwtusAJDuEIwsEHGJsGuIAJGssAIGruDv";
            public const string id = "id";
            public const string ParentId = "BJwuFCrHEDFAGDEuuIItwrIDFvACwrAGuI";
            public const string ChangedBy = "BJGvFvJrrGsDIEEJHIJvAIHCEDJtsBIwwE";
            public const string AccessRight = "BJvAwHCCDwEFDFEBwJrBwsvAHuIIGGFrwr";
            public const string XPath = "BJJwBHEtIDHGEvEBsrsIDEtFBJFtwrHrJB";
            
        }
        
        
        public ElementHide()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _XPath = "";
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJBEFGFFDIGwDIEvEGIswGHCHDEGIAGCww";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed["BJruJEHICBJAJFEHwCJHGwIrDJDHDIFtHE"] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed["BJvBFIrHwItGHJEFCtrDsvGEwHIAsCFDtD"] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed["BJwtusAJDuEIwsEHGJsGuIAJGssAIGruDv"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed["BJwuFCrHEDFAGDEuuIItwrIDFvACwrAGuI"] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed["BJGvFvJrrGsDIEEJHIJvAIHCEDJtsBIwwE"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed["BJvAwHCCDwEFDFEBwJrBwsvAHuIIGGFrwr"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _XPath { set; get; }
        [JsonProperty(FieldsId.XPath)] 
        public string XPath { set { Changed["BJJwBHEtIDHGEvEBsrsIDEtFBJFtwrHrJB"] = "true"; _XPath = value; } get { return _XPath ; } }
        
    }
    public partial class Categories
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJItuHuHIvIwFAEuHBIJGJJsuvFHvvtruE";
            public const string RowNo = "BJFJrDvJsGHFuJEssIIGBrFDAFwvHwAvGu";
            public const string DateVersion = "BJsFAvsuwCBHCGEIIusDDItuwBHFBHrAtH";
            public const string id = "id";
            public const string ParentId = "BJwBwuwwwrrwEvECtFsCEJwvEtHHwHFABF";
            public const string ChangedBy = "BJGsJBBBAuHsCHEGIJJrHAHIFGuEHGrwHI";
            public const string AccessRight = "BJJtsDwDFGrsuvErwuJJBuBwAHJurHtIvv";
            public const string Url = "BJHvHrArHrGtswEEIHrGICDusBrHFEwCtH";
            public const string ProcessedOn = "BJFsEutCtvAvtsEGrFsHAsswICHtutuBuv";
            
        }
        
        
        public Categories()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _Url = "";
            _ProcessedOn = DateTime.Now;
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJHsrBBDAsGHCsEDFwrBJIDvDEtIBuuwFw";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed["BJItuHuHIvIwFAEuHBIJGJJsuvFHvvtruE"] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed["BJFJrDvJsGHFuJEssIIGBrFDAFwvHwAvGu"] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed["BJsFAvsuwCBHCGEIIusDDItuwBHFBHrAtH"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed["BJwBwuwwwrrwEvECtFsCEJwvEtHHwHFABF"] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed["BJGsJBBBAuHsCHEGIJJrHAHIFGuEHGrwHI"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed["BJJtsDwDFGrsuvErwuJJBuBwAHJurHtIvv"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _Url { set; get; }
        [JsonProperty(FieldsId.Url)] 
        public string Url { set { Changed["BJHvHrArHrGtswEEIHrGICDusBrHFEwCtH"] = "true"; _Url = value; } get { return _Url ; } }
        
        private DateTime _ProcessedOn { set; get; }
        [JsonProperty(FieldsId.ProcessedOn)] 
        public DateTime ProcessedOn { set { Changed["BJFsEutCtvAvtsEGrFsHAsswICHtutuBuv"] = "true"; _ProcessedOn = value; } get { return _ProcessedOn ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJArJEvJCFCCDtEGHDrFsIECFvGDCHuHAw";
        public const string RowNo = "BJFGADEGEHAJvvEAHtrrAAuBusBJstEsur";
        public const string DateVersion = "BJsrCCBuIsuIDsEFFDIJwwJArACHwEHIvB";
        public const string id = "id";
        public const string ParentId = "BJGDCJJErvtEBAErIAJBtEBFFuwBEAvHGA";
        public const string ChangedBy = "BJuuEFJGFBrwwHEsuvruArEEssBtBrtCJv";
        public const string AccessRight = "BJJIvCIAwwBvEBEBCurAAAGFDDvJtrEFHC";
        public const string WebCompany = "BJCJrBBFBCGBrFEFvwJBEHHFEFHFAsrHvt";
        public const string ProductCrawScheduler = "BJDEJBHFsBFswsEAtvJFuGGtHDFrGJCDuw";
        public const string XPathNextResult = "BJEIIJGrEHICFvEJuAIAHrJsIEIDrDDIuw";
        public const string XpathProductName = "BJDEEItrDrEtEGEHAwJtGwCHGGHtJvCvuw";
        public const string XpathRate = "BJFrHFIAuuHrwHEBIIJtGrEACuFCsCDIDv";
        public const string XPathDeliveryFees = "BJwrrvBrtJGwIDECAAsAwHABsIrDGrFFtE";
        public const string XPathSeller = "BJBAvrsDEHwrDIEJuwsJHrAHBFuHwwFAAE";
        public const string XPathPrice = "BJBvsCtDEvEHwDECrBrAGBCuJGHGHDHGFu";
        public const string XPathProductImage = "BJrsvrtvBtHCFGEEBEIrrDJGBEAvuuIFtt";
        public const string XPathProductImageNext = "BJFCsAEABtCJtGEuBvItJGFswECrBFFrDw";
        public const string XPathCategories = "BJDvsDttGCGtuEEHDIJFABvvAFFFCtursJ";
        public const string CategoryUrlCrawScheduler = "BJFDIrJBJvCACsECFFIuIHwJruswEEFIuw";
        public const string XPathProductUrl = "BJrAtHEFvHFHrGEtGIrEvvIAFArHCvIGuE";
        public const string ProductUrlCrawScheduler = "BJsIBursJEGDtBEGtuIsvICHrHEDAAJJwB";
        public const string Enable = "BJvBEEFsJEICHJEGsAJrGFGvFDGFHGsHCC";
        
    }
    
    
    public WebCrawler()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _WebCompany = "";
        _ProductCrawScheduler = "";
        _XPathNextResult = "";
        _XpathProductName = "";
        _XpathRate = "";
        _XPathDeliveryFees = "";
        _XPathSeller = "";
        _XPathPrice = "";
        _XPathProductImage = "";
        _XPathProductImageNext = "";
        _XPathCategories = "";
        _CategoryUrlCrawScheduler = "";
        _XPathProductUrl = "";
        _ProductUrlCrawScheduler = "";
        _Enable = false;
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJvDBAvwIDsvutEJrwIwEJEFJHuCHDIDAr";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed["BJArJEvJCFCCDtEGHDrFsIECFvGDCHuHAw"] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed["BJFGADEGEHAJvvEAHtrrAAuBusBJstEsur"] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed["BJsrCCBuIsuIDsEFFDIJwwJArACHwEHIvB"] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed["id"] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed["BJGDCJJErvtEBAErIAJBtEBFFuwBEAvHGA"] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed["BJuuEFJGFBrwwHEsuvruArEEssBtBrtCJv"] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed["BJJIvCIAwwBvEBEBCurAAAGFDDvJtrEFHC"] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _WebCompany { set; get; }
    [JsonProperty(FieldsId.WebCompany)] 
    public string WebCompany { set { Changed["BJCJrBBFBCGBrFEFvwJBEHHFEFHFAsrHvt"] = "true"; _WebCompany = value; } get { return _WebCompany ; } }
    
    private string _ProductCrawScheduler { set; get; }
    [JsonProperty(FieldsId.ProductCrawScheduler)] 
    public string ProductCrawScheduler { set { Changed["BJDEJBHFsBFswsEAtvJFuGGtHDFrGJCDuw"] = "true"; _ProductCrawScheduler = value; } get { return _ProductCrawScheduler ; } }
    
    private string _XPathNextResult { set; get; }
    [JsonProperty(FieldsId.XPathNextResult)] 
    public string XPathNextResult { set { Changed["BJEIIJGrEHICFvEJuAIAHrJsIEIDrDDIuw"] = "true"; _XPathNextResult = value; } get { return _XPathNextResult ; } }
    
    private string _XpathProductName { set; get; }
    [JsonProperty(FieldsId.XpathProductName)] 
    public string XpathProductName { set { Changed["BJDEEItrDrEtEGEHAwJtGwCHGGHtJvCvuw"] = "true"; _XpathProductName = value; } get { return _XpathProductName ; } }
    
    private string _XpathRate { set; get; }
    [JsonProperty(FieldsId.XpathRate)] 
    public string XpathRate { set { Changed["BJFrHFIAuuHrwHEBIIJtGrEACuFCsCDIDv"] = "true"; _XpathRate = value; } get { return _XpathRate ; } }
    
    private string _XPathDeliveryFees { set; get; }
    [JsonProperty(FieldsId.XPathDeliveryFees)] 
    public string XPathDeliveryFees { set { Changed["BJwrrvBrtJGwIDECAAsAwHABsIrDGrFFtE"] = "true"; _XPathDeliveryFees = value; } get { return _XPathDeliveryFees ; } }
    
    private string _XPathSeller { set; get; }
    [JsonProperty(FieldsId.XPathSeller)] 
    public string XPathSeller { set { Changed["BJBAvrsDEHwrDIEJuwsJHrAHBFuHwwFAAE"] = "true"; _XPathSeller = value; } get { return _XPathSeller ; } }
    
    private string _XPathPrice { set; get; }
    [JsonProperty(FieldsId.XPathPrice)] 
    public string XPathPrice { set { Changed["BJBvsCtDEvEHwDECrBrAGBCuJGHGHDHGFu"] = "true"; _XPathPrice = value; } get { return _XPathPrice ; } }
    
    private string _XPathProductImage { set; get; }
    [JsonProperty(FieldsId.XPathProductImage)] 
    public string XPathProductImage { set { Changed["BJrsvrtvBtHCFGEEBEIrrDJGBEAvuuIFtt"] = "true"; _XPathProductImage = value; } get { return _XPathProductImage ; } }
    
    private string _XPathProductImageNext { set; get; }
    [JsonProperty(FieldsId.XPathProductImageNext)] 
    public string XPathProductImageNext { set { Changed["BJFCsAEABtCJtGEuBvItJGFswECrBFFrDw"] = "true"; _XPathProductImageNext = value; } get { return _XPathProductImageNext ; } }
    
    private string _XPathCategories { set; get; }
    [JsonProperty(FieldsId.XPathCategories)] 
    public string XPathCategories { set { Changed["BJDvsDttGCGtuEEHDIJFABvvAFFFCtursJ"] = "true"; _XPathCategories = value; } get { return _XPathCategories ; } }
    
    private string _CategoryUrlCrawScheduler { set; get; }
    [JsonProperty(FieldsId.CategoryUrlCrawScheduler)] 
    public string CategoryUrlCrawScheduler { set { Changed["BJFDIrJBJvCACsECFFIuIHwJruswEEFIuw"] = "true"; _CategoryUrlCrawScheduler = value; } get { return _CategoryUrlCrawScheduler ; } }
    
    private string _XPathProductUrl { set; get; }
    [JsonProperty(FieldsId.XPathProductUrl)] 
    public string XPathProductUrl { set { Changed["BJrAtHEFvHFHrGEtGIrEvvIAFArHCvIGuE"] = "true"; _XPathProductUrl = value; } get { return _XPathProductUrl ; } }
    
    private string _ProductUrlCrawScheduler { set; get; }
    [JsonProperty(FieldsId.ProductUrlCrawScheduler)] 
    public string ProductUrlCrawScheduler { set { Changed["BJsIBursJEGDtBEGtuIsvICHrHEDAAJJwB"] = "true"; _ProductUrlCrawScheduler = value; } get { return _ProductUrlCrawScheduler ; } }
    
    private bool _Enable { set; get; }
    [JsonProperty(FieldsId.Enable)] 
    public bool Enable { set { Changed["BJvBEEFsJEICHJEGsAJrGFGvFDGFHGsHCC"] = "true"; _Enable = value; } get { return _Enable ; } }
    
}
