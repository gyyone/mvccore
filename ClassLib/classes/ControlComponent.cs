using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class ControlComponent
{    
    
    
    public struct FieldsId
    {
        public const string Button = "CAAGDJFEwwBtCCEFCCJrrwJuCFvvAtrGAD";
        public const string RowNo = "CAHIJHDIHEAuJEErBBIHCwrwFJFDAFFwAu";
        public const string DateVersion = "CAvCDAHsBBtBACEBDIrCHBBuvtEwADvrGG";
        public const string id = "id";
        public const string ParentId = "CArIBEFtFwuDvIEAssIFGIABtvDFIDuGGw";
        public const string ChangedBy = "CAwIHGECJuBrCwEvrJJAvsCAGDBIDEurIt";
        public const string Files = "CAuHtrEttHCEwCEsuHJIJAuBEvEJEwIIBA";
        public const string DependantOnLibary = "CAwuEGrItsEBusEJAvIBwEvvBvEwHIIrws";
        public const string ControlType = "CADrsuFuvvEtswEDsFJvuFErFtrArEusuI";
        public const string AccessRight = "CAwwutAIurtAJEEHHvJJBCJACGuHIIFBFA";
        public const string LanguageType = "CArEABvrCFIGIvEGFGIFGBwCBJwACsAsID";
        public const string InitializationFile = "CAAHuCCDwGIDDHEDItJsIwItHArruAHrHB";
        public const string BackEndFile = "CAArBrrrGJsAwvEwCrJJIDECwCEFwuDEJI";
        public const string InitializationScript = "CAFBIGEDvsFBBuEEBDrDIFHJvJGtuCwBJw";
        
    }
    
    
    public ControlComponent()
    {
        _Button = "";
        _RowNo = 0;

        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "CAFBJvIFBBwwFwEAIJrvDEBFvFEICrDDts";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private List<string> _Files { set; get; }
    [JsonProperty(FieldsId.Files)] 
    public List<string> Files { set { Changed[FieldsId.Files] = "true"; _Files = value; } get { return _Files ; } }
    
    private List<string> _DependantOnLibary { set; get; }
    [JsonProperty(FieldsId.DependantOnLibary)] 
    public List<string> DependantOnLibary { set { Changed[FieldsId.DependantOnLibary] = "true"; _DependantOnLibary = value; } get { return _DependantOnLibary ; } }
    
    private List<string> _ControlType { set; get; }
    [JsonProperty(FieldsId.ControlType)] 
    public List<string> ControlType { set { Changed[FieldsId.ControlType] = "true"; _ControlType = value; } get { return _ControlType ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _LanguageType { set; get; }
    [JsonProperty(FieldsId.LanguageType)] 
    public string LanguageType { set { Changed[FieldsId.LanguageType] = "true"; _LanguageType = value; } get { return _LanguageType ; } }
    
    private List<string> _InitializationFile { set; get; }
    [JsonProperty(FieldsId.InitializationFile)] 
    public List<string> InitializationFile { set { Changed[FieldsId.InitializationFile] = "true"; _InitializationFile = value; } get { return _InitializationFile ; } }
    
    private List<string> _BackEndFile { set; get; }
    [JsonProperty(FieldsId.BackEndFile)] 
    public List<string> BackEndFile { set { Changed[FieldsId.BackEndFile] = "true"; _BackEndFile = value; } get { return _BackEndFile ; } }
    
    private CodeEditor _InitializationScript { set; get; }
    [JsonProperty(FieldsId.InitializationScript)] 
    public CodeEditor InitializationScript { set { Changed[FieldsId.InitializationScript] = "true"; _InitializationScript = value; } get { return _InitializationScript ; } }
    
}
