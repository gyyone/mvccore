using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
using ClassLib;
public partial class LookupTable
{    
    public partial class Items
    {    
        
        
        public struct FieldsId
        {
            public const string Button = "BJrHGGwvBrrHEGEvssrBvuIuJEtDFswsIv";
            public const string RowNo = "BJwHAEsIHAtBGJEsFrJAHurwwDHAJBEEAv";
            public const string DateVersion = "BJBJAJJDDvDDFwEIJJIBIFJDIvvuutuBwF";
            public const string id = "id";
            public const string ParentId = "BJIvCGDHIvJCEvEBvssJFIwJtusHtwHJCD";
            public const string ChangedBy = "BJBsDGwFDJCsIBEHFBssvBvFDFCFvEvBFA";
            public const string AccessRight = "BJwHBwDHJwJADAEuFCrCvtwIsrrArtuArt";
            public const string Name = "BJsBrrCuuItIvGEtrrJvHvwHHvrCtusuIs";
            public const string Value = "BJvwGFDsFDwDGvEFwFJrHAJCGBsuwHwEtJ";
            public const string Icon = "BJACGDIwrHwBJEEJAArGIHuBsDJJwEBHBF";
            
        }
        
        
        public Items()
        {
            _Button = "";
            _RowNo = 0;
            _DateVersion = DateTime.Now;
            _id = "";
            _ParentId = "";
            _ChangedBy = "";
            _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
            _Name = "";
            _Value = @"";
            _Icon = @"ec028161-5207-44c5-bbb4-52ed485c3574";
            
        }
        
        
        private Dictionary<string,string> _Changed { set; get; }
        public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
        public const string ClassID  = "BJvDttCtAArFFrEuGAsAuGCJJDvJuswuBs";
        public string Class_ID  {get{return ClassID;}}
        
        private string _Button { set; get; }
        [JsonProperty(FieldsId.Button)] 
        public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
        
        private int _RowNo { set; get; }
        [JsonProperty(FieldsId.RowNo)] 
        public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
        
        private DateTime _DateVersion { set; get; }
        [JsonProperty(FieldsId.DateVersion)] 
        public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
        
        private string _id { set; get; }
        [JsonProperty(FieldsId.id)] 
        public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
        
        private string _ParentId { set; get; }
        [JsonProperty(FieldsId.ParentId)] 
        public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
        
        private string _ChangedBy { set; get; }
        [JsonProperty(FieldsId.ChangedBy)] 
        public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
        
        private Dictionary<string, List<string>> _AccessRight { set; get; }
        [JsonProperty(FieldsId.AccessRight)] 
        public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
        
        private string _Name { set; get; }
        [JsonProperty(FieldsId.Name)] 
        public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
        
        private string _Value { set; get; }
        [JsonProperty(FieldsId.Value)] 
        public string Value { set { Changed[FieldsId.Value] = "true"; _Value = value; } get { return _Value ; } }
        
        private string _Icon { set; get; }
        [JsonProperty(FieldsId.Icon)] 
        public string Icon { set { Changed[FieldsId.Icon] = "true"; _Icon = value; } get { return _Icon ; } }
        
    }
    
    
    public struct FieldsId
    {
        public const string Button = "BJvErCGHABCCFtEvsBsrrHFDGtsCFFHAvu";
        public const string RowNo = "BJDsFBJsGGFuvAEEvIIFFtsuJDECFFDvHD";
        public const string DateVersion = "BJHJwIvtFEtBIAEBGGsArDuEArGCCErGCI";
        public const string id = "id";
        public const string ParentId = "BJtJEIDswrttEGEAtvrHsAAGuJCvBJtIuG";
        public const string ChangedBy = "BJGGECIHHvGHDtErtFJCAuDEHuvHIAEsIv";
        public const string AccessRight = "BJEvJsJwEArIIAErHHIsAtwvCrrJBEHvDH";
        public const string Name = "BJJGJtAvssDwDtEBFsJvBtABusuABvtssu";
        
    }
    
    
    public LookupTable()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""]}");
        _Name = "";
        
    }
    
    
    private Dictionary<string,string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID  = "BJuIFCrwDGJvCvECCwrCBFCIFBvuAuuAHD";
    public string Class_ID  {get{return ClassID;}}
    
    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)] 
    public string Button { set { Changed[FieldsId.Button] = "true"; _Button = value; } get { return _Button ; } }
    
    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)] 
    public int RowNo { set { Changed[FieldsId.RowNo] = "true"; _RowNo = value; } get { return _RowNo ; } }
    
    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)] 
    public DateTime DateVersion { set { Changed[FieldsId.DateVersion] = "true"; _DateVersion = value; } get { return _DateVersion ; } }
    
    private string _id { set; get; }
    [JsonProperty(FieldsId.id)] 
    public string id { set { Changed[FieldsId.id] = "true"; _id = value; } get { return _id ; } }
    
    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)] 
    public string ParentId { set { Changed[FieldsId.ParentId] = "true"; _ParentId = value; } get { return _ParentId ; } }
    
    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)] 
    public string ChangedBy { set { Changed[FieldsId.ChangedBy] = "true"; _ChangedBy = value; } get { return _ChangedBy ; } }
    
    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)] 
    public Dictionary<string, List<string>> AccessRight { set { Changed[FieldsId.AccessRight] = "true"; _AccessRight = value; } get { return _AccessRight ; } }
    
    private string _Name { set; get; }
    [JsonProperty(FieldsId.Name)] 
    public string Name { set { Changed[FieldsId.Name] = "true"; _Name = value; } get { return _Name ; } }
    
}
