﻿using Newtonsoft.Json;

public partial class MenuProperty
{
    [JsonIgnore]
    private Icons m_Icon { set; get; }
    [JsonIgnore]
    public Icons GetIcon
    {
        get
        {
            if (m_Icon == null)
            {
                m_Icon = new Icons();
            }
            return m_Icon;
        }
    }

}
