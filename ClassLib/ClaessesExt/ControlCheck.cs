﻿namespace ClassLib
{
    public static class ControlCheck
    {
        public static bool Islookup(string controlType)
        {
            switch (controlType)
            {
                //CheckList
                case "BJCtCutvAIEFEFEtHCICBtFGFAFsJuDEHI":
                //Combobox
                case "BJBGtIFvvsusAGEtwJsttJADvHIsAFHBFF":
                //Sortable Combobox
                case "BJEEFAEsrwvsrsEIuvIwrswvIFBsvFtFrD":
                //KeyValue
                case "BJHttsEAAIAvBJEvCArtIutJsBvwJBwBJB":
                //radio button
                case "BJstHBIIDvCwICEDCrIvrBsDHrGJsrwuwu":
                    return true;
            }
            return false;
        }
    }


}
