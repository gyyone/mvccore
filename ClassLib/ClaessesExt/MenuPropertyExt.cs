﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLib
{
    public static class MenuPropertyExt
    {
        public static Dictionary<string, string> GetLiAttr(this MenuProperty field)
        {
            return new Dictionary<string, string>() { { "style", $"color:{field.Color};" } };
        }
        public static bool HasAccess(this MenuProperty menu, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
            return menu.AccessRight.Keys.Any(x => roles.Contains(x));
        }
        public static List<string> GetAccess(this MenuProperty menu, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
            List<string> accessmode = new List<string>();
            foreach (var key in menu.AccessRight.Keys.Where(x => roles.Contains(x)))
            {
                accessmode.AddRange(menu.AccessRight[key]);
            }
            return accessmode.Distinct().ToList();
        }
    }


}
