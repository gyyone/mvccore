﻿using System;
using System.Collections.Generic;
using Util;
using System.Linq;

namespace ClassLib
{
    public static class TablePropertyExt
    {
        public static string GetCleanName(this TableProperty table)
        {
            return StandardFormat.GetCleanName(table.Name);
        }

        public static bool HasAccess(this TableProperty table, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
      
            return roles.Contains(Id.UsersAccount.Developer) || table.AccessRight.Keys.Any(x => roles.Contains(x));
        }
        public static List<string> GetAccess(this TableProperty table, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
            if(roles.Contains(Id.UsersAccount.Developer))
            {
                return new List<string>() {"View", "Modify","Delete"};
            }
            List<string> accessmode = new List<string>();
            foreach (var key in table.AccessRight.Keys.Where(x => roles.Contains(x)))
            {
                accessmode.AddRange(table.AccessRight[key]);
            }
            return accessmode.Distinct().ToList();
        }
    }
}
