﻿using System.Collections.Generic;
using Util;
using System.Linq;

namespace ClassLib
{
    public static class FieldPropertyExt
    {
        public static string GetCleanName(this TableProperty.FieldProperty field)
        {
            return StandardFormat.GetCleanName(field.Name);
        }
        /// <summary>
        /// return id if Name is id. else return guid of the field
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string GetId(this TableProperty.FieldProperty field)
        {
            if (field.Name.Equals("id"))
            {
                return "id";
            }
            return field.id;
        }
        public static bool HasAccess(this TableProperty.FieldProperty field, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
            //Developer Can Access any field
           

            return roles.Contains(Id.UsersAccount.Developer) || field.AccessRight.Keys.Any(x => roles.Contains(x));
        }
        public static List<string> GetAccess(this TableProperty.FieldProperty field, List<string> rolesId, string id = null)
        {
            List<string> roles = new List<string>();
            roles.Add(Id.Role.Public);
            if (rolesId != null)
            {
                roles.AddRange(rolesId);
            }
            if (id != null)
            {
                roles.Add(id);
            }
            if (roles.Contains(Id.UsersAccount.Developer))
            {
                return new List<string>() {"View","Modify","Delete"};
            }
            List<string> accessmode = new List<string>();
            foreach (var key in field.AccessRight.Keys.Where(x => roles.Contains(x)))
            {
                accessmode.AddRange(field.AccessRight[key]);
            }
            return  accessmode.Distinct().ToList();
        }
    }


}
