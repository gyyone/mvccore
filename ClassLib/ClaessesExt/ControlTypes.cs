﻿namespace ClassLib
{
    public enum ControlTypes
    {
        None,
        Html,
        TextBox,
        CheckBox,
        SwitchButton,
        SummerNote,
        CodeEditor,
        ColorPicker,
        DatetimePicker,
        InputMask,
        FileUpload,
        RadioButton,
        ComboBox,
        CheckList,
        KeyValueBox,
        SourceEditor,
        SortableComboBox,
        EncrytText

    }


}
