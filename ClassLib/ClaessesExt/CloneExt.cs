﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace ClassLib
{

    //public static class CloneExt<T>
    //{
    //    public static T Clone(T obj)
    //    {
    //        var settings = new JsonSerializerSettings();
    //        settings.Converters.Add(new StringEnumConverter { });
    //        settings.TypeNameHandling = TypeNameHandling.Auto;
    //        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj, settings), settings);
    //    }    
    //}
    public static class CloneExt
    {
        public static T Clone<T>(this T obj) where T : new()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter { });
            settings.TypeNameHandling = TypeNameHandling.Auto;
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj, settings), settings);
        }
    }
}
