﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassLib
{
    public class KeyValueSource
    {
        public KeyValueSource()
        {
            Items = new Dictionary<string, string>();
            NewTag = false;
        }
        public KeyValueSource(Type enumType)
        {
            Items = Enum.GetNames(enumType).ToDictionary(x => x, y => y);
        }
        public Dictionary<string, string> Items { set; get; }
        public string ClassType => Const.KeyValueSource;
        public bool NewTag { set; get; }

    }
}
