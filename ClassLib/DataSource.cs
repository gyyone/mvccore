﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassLib
{
    public class DataSource
    {
        public DataSource()
        {
            KeySource = new List<SelectSource>();
            ValueSource = new List<SelectSource>();
        }
        public DataSource(ApiSource apisource)
        {
            KeySource = new List<SelectSource>();
            ValueSource = new List<SelectSource>();

            KeySource.Add(new SelectSource(apisource));
        }
        public DataSource(Dictionary<string, string> keyvalue)
        {
            KeySource = new List<SelectSource>();
            ValueSource = new List<SelectSource>();
            KeySource.Add(new SelectSource(new KeyValueSource() { Items = keyvalue }));
        }
        public DataSource(TableSource tbsource)
        {
            KeySource = new List<SelectSource>();
            ValueSource = new List<SelectSource>();

            KeySource.Add(new SelectSource(tbsource));
        }
        public DataSource(List<TableSource> tbsource)
        {
            KeySource = new List<SelectSource>();
            ValueSource = new List<SelectSource>();
            KeySource.AddRange(tbsource.Select(x => new SelectSource(x)).ToList());
        }
        //Use for Lookup Source
        public void AddKeySource(Type enumType)
        {
            KeySource.Add(new SelectSource(new KeyValueSource(enumType)));
        }
        public void AddKeySource(Dictionary<string, string> keyvalue)
        {
            KeySource.Add(new SelectSource(new KeyValueSource() { Items = keyvalue }));
        }
        public void AddKeySource(TableSource tbsource)
        {
            KeySource.Add(new SelectSource(tbsource));
        }
        public void AddKeySource(List<TableSource> tbsource)
        {
            KeySource.AddRange(tbsource.Select(x => new SelectSource(x)).ToList());
        }
        public void AddKeySource(ApiSource apiSource)
        {
            KeySource.Add(new SelectSource(apiSource));
        }

        //Use for Lookup Source
        public void AddValueSource(Dictionary<string, string> keyvalue)
        {
            ValueSource.Add(new SelectSource(new KeyValueSource() { Items = keyvalue }));
        }
        public void AddValueSource(TableSource tbsource)
        {
            ValueSource.Add(new SelectSource(tbsource));
        }
        public void AddValueSource(List<TableSource> tbsource)
        {
            ValueSource.AddRange(tbsource.Select(x => new SelectSource(x)).ToList());
        }
        public void AddValueSource(Type enumType)
        {
            ValueSource.Add(new SelectSource(new KeyValueSource(enumType)));
        }
        public void AddValueSource(ApiSource apiSource)
        {
            ValueSource.Add(new SelectSource(apiSource));
        }

        public List<SelectSource> KeySource { set; get; }
        //User for Key value together      
        public List<SelectSource> ValueSource { set; get; }
        public bool ValueSourceMultipleValue { set; get; }

    }
}
