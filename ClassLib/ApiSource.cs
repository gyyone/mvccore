﻿using System.Collections.Generic;

namespace ClassLib
{
    public class ApiSource
    {
        public ApiSource()
        {
            Url = "";
            DependentField = new List<string>();
        }
        public ApiSource(string url)
        {
            Url = url;
            DependentField = new List<string>();
        }
        public ApiSource(string url, List<string> dependentField)
        {
            Url = url;
            DependentField = dependentField;
        }

        public string Url { set; get; }
        public List<string> DependentField { set; get; }
    }
}
