﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Collections;
using System.Reflection;
using Newtonsoft.Json.Converters;

using System.Threading.Tasks;
using Util;



namespace ClassLib
{

    public class DBStructure
    {

        public DBStructure()
        {
            MenuPropertyDic = new Dictionary<string, MenuProperty>();
            TableNameDic = new Dictionary<string, TableProperty>();
            FieldNameDic = new Dictionary<string, TableProperty.FieldProperty>();
            TableToFieldDic = new Dictionary<string, Dictionary<string, TableProperty.FieldProperty>>();
            TableIdDic = new Dictionary<string, TableProperty>();
            FieldIdDic = new Dictionary<string, TableProperty.FieldProperty>();
            TableToFieldNameDic = new Dictionary<string, Dictionary<string, TableProperty.FieldProperty>>();
            MenuTableDic = new Dictionary<string, MenuProperty.Pages>();  
        }
        public void SetTablePropertyAndField(List<TableProperty> tablelist, List<TableProperty.FieldProperty> fieldlist)
        { 

            foreach (var x in tablelist)
            {
                TableIdDic[x.id] = x;
            }
            foreach (var x in fieldlist)
            {
                FieldIdDic[x.id] = x;
            }
            RebuidStructure();
        }
       
        public Dictionary<string, MenuProperty.Pages> MenuTableDic { set; get; }
        public Dictionary<string, MenuProperty> MenuPropertyDic { set; get; }
        //Provide table id get table property
        public Dictionary<string, TableProperty> TableIdDic { set; get; }
        //Provide data Id get  get data Property
        public Dictionary<string, TableProperty.FieldProperty> FieldIdDic { set; get; }

        //Provide Table Name Get Table Property Build from TableIdDic and FieldIdDIc

        public Dictionary<string, TableProperty> TableNameDic { get; }
        //Provide Field name get the Property
        public Dictionary<string, TableProperty.FieldProperty> FieldNameDic { get; }
        public Dictionary<string, Dictionary<string, TableProperty.FieldProperty>> TableToFieldDic { get; }
        public Dictionary<string, Dictionary<string, TableProperty.FieldProperty>> TableToFieldNameDic { get; }

        //Provide TableName, FieldName,UserId/ Role ID to check Access Right

        public string RootPath { set; get; }


        public void ClearCache()
        {
            FieldNameDic.Clear();
            TableNameDic.Clear();
            TableToFieldDic.Clear();
            TableToFieldNameDic.Clear();
        }


        public void RebuidStructure()
        {
            ClearCache();

            foreach (var item in TableIdDic.Values.Where(x => x.ParentId == "#").ToList())
            {
                BuildDic(item, StandardFormat.GetCleanName(item.Name)); 
            }
        }

        private void BuildDic(TableProperty tableparent, string tableName)
        {
            try
            {
                string parentId = tableparent.id;
                var childTable = TableIdDic.Values.Where(x => x.ParentId == parentId).ToList();
                var childField = FieldIdDic.Values.Where(x => x.ParentId == parentId).ToList();
                if (!TableToFieldNameDic.ContainsKey(parentId))
                {
                    TableToFieldNameDic[parentId] = new Dictionary<string, TableProperty.FieldProperty>();
                }
                if (!TableNameDic.ContainsKey(tableName))
                {
                    TableNameDic[tableName] = TableIdDic[tableparent.id];
                }
                if (!TableToFieldDic.ContainsKey(parentId))
                {
                    TableToFieldDic[parentId] = new Dictionary<string, TableProperty.FieldProperty>();
                }
                foreach (var field in childField)
                {
                    if (field.Name.Equals("id"))
                    {
                        TableToFieldDic[parentId][field.Name] = FieldIdDic[field.id];
                        TableToFieldNameDic[parentId][StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];
                        FieldNameDic[tableName + @"_" + StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];
                    }
                    else
                    {
                        TableToFieldDic[parentId][field.id] = FieldIdDic[field.id];
                        TableToFieldNameDic[parentId][StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];
                        FieldNameDic[tableName + @"_" + StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];
                    }

                }

                foreach (var baseProperty in childTable)
                {
                    BuildDic(baseProperty, tableName + @"_" + StandardFormat.GetCleanName(baseProperty.Name));
                }

            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
           
        }


        public string GetFieldIdByName(string tableId, string fieldName)
        {
            string fieldnameclean = StandardFormat.GetCleanName(fieldName);
            if (fieldnameclean == "id")
            {
                return "id";
            }
            return TableToFieldNameDic[tableId][fieldnameclean].id;
        }
        public string GetFullFieldNameClean(string tableId, string fieldName)
        {
            return StandardFormat.GetCleanName($"{GetFullTablename(tableId)}_{fieldName}");
        }

        public string GetFullTablenameClean(string tableId)
        {
            return StandardFormat.GetCleanName(GetFullTablename(tableId));
        }
        public string GetFullTablename(string tableId)
        {
            if (tableId == "#")
            {
                return "";
            }
            if (!string.IsNullOrWhiteSpace(tableId) && TableIdDic[tableId].ParentId != "#")
            {
                return GetFullTablename(TableIdDic[tableId].ParentId) + $"_{TableIdDic[tableId].Name}";
            }

            return TableIdDic[tableId].Name;
        }

        public const string SystemDateFormat = "yyyy.MM.dd.HHmmssfff";
        public const string SystemDateFormatUi = "yyyy.MM.dd.HHmmss000";
        public static string GetTimeVersionFuture()
        {
            return "9999.12.31.235959999";
        }
        public static string GetTimeVersionPass()
        {
            return "0000.0000.0000.000000000";
        }
        public static string GetNowTimeVersion()
        {
            return DateTime.Now.ToString(SystemDateFormat);
        }
      
     
    }
}

