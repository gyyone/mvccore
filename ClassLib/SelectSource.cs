﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClassLib
{
    public class SelectSource
    {
        public SelectSource()
        {
            LookupSource = null;
            ClassType = "";
        }
        public SelectSource(ApiSource apiSource)
        {
            LookupSource = apiSource;
            ClassType = Const.APISource;
        }
        public SelectSource(TableSource source)
        {
            LookupSource = source;
            ClassType = Const.TableSource;
        }
        public SelectSource(KeyValueSource source)
        {
            LookupSource = source;
            ClassType = Const.KeyValueSource;

        }
        public SelectSource(Dictionary<string, string> source)
        {
            LookupSource = new KeyValueSource() { Items = source };

            ClassType = Const.KeyValueSource;
        }

        public T GetSource<T>() where T: new()
        {

            switch (ClassType)
            {
                case Const.KeyValueSource:
                    if(typeof(T) == typeof(KeyValueSource))
                    {
                        return (T)LookupSource;
                    }
                    break;
                case Const.TableSource:
                    if (typeof(T) == typeof(TableSource))
                    {
                        return (T)LookupSource;
                    }
                    break;
                case Const.APISource:
                    if (typeof(T) == typeof(string) || typeof(T) == typeof(ApiSource))
                    {
                        return (T)LookupSource;
                    }            
                    break;

            }
            return default(T);
        }
        public object GetSource()
        {
            switch (ClassType)
            {
                case Const.KeyValueSource:
                    return LookupSource as KeyValueSource;
                case Const.TableSource:
                    return LookupSource as TableSource;
                case Const.APISource:
                    if(LookupSource == null)
                    {
                        LookupSource = new ApiSource("");                    
                    }
                    if (LookupSource.GetType() == typeof(string))
                    {
                        return new ApiSource(LookupSource as string); 
                    }
                    else
                    {
                        return LookupSource as ApiSource;
                    }
            }
            return null;
        }
   

        public string ClassType { set; get; }        
        public object LookupSource { set; get; }
    }
}
