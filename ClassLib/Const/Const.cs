public static partial class Const
{
	public static class BootstrapSizing
	{
		public static string colmd12 = "CAGtDJuEFtEEDBEDvwIrHIDDHsuFwIsttw";
		public static string colmd1 = "CArDIwwvIrBwIBEJtAsvDIDsJwuFJtGGAF";
		public static string colmd6 = "CAItCFGCvDGIDBEAADsJwDIuFswHruHtGG";
	}
	public static class SolrDatatype
	{
		public static string delimited_payloads_string = "CAAtGsuAtJBIAtEIvJrIEvDvsHDtHCwEFw";
		public static string delimited_payloads_float = "CAEAEBFJGwCCBAEDrDsBIFGttHECwJAwEv";
		public static string delimited_payloads_int = "CABJHuFtBtvGFJEuJEIBItEuFBCsCAuEvv";
		public static string descendent_path = "CABECJEuIFHIDGEFECrJttHsEuAAFuGAvD";
		public static string ancestor_path = "CAsuIwvGIBJIHEEICrJJtFsGtHsJAButBD";
		public static string location_rpt = "CAruruCrEJHuDuEtrrJBGBHJGHBtFJtIrs";
		public static string phonetic_en = "CAEsBAtwCGBCwrEBEsJsrvCtuuBvDCIwFE";
		public static string booleans = "CAGIHBuuuruBvEEJAwIBtEwrEswvJDGFAI";
		public static string location = "CADDDwBABDFJwBEEDCIvsuvGrGrAsIwEGE";
		public static string boolean = "CAEFEvAwEAGrwvEsJtIAGwuuGutuuBFwHI";
		public static string lowercase = "CAGFrIGsswAGBtECvuJHCuHDDHuACuJCvC";
		public static string pdoubles = "CAtrCvrvwJuEAHEGHusvIFFEwHtCuAtEwG";
		public static string text_en_splitting_tight = "CAGAvAvrAIsDwDEDAsIJAttJIFDIFAECBr";
		public static string text_en_splitting = "CAIswJDGFJstEHEIsurIECJAGvCsDvDHDu";
		public static string pdouble = "CADEuGAtHIwACFEFwFICIBDvIGusvvsrIA";
		public static string pfloats = "CAEFDJFHuGGJAHErrFIDCsBEsCIHDBJJtD";
		public static string strings = "CACItHDCHCJuvtEDFGsBFFvGrIJvBBHsts";
		public static string text_ar = "CAJCrvwJuCIJvAEutHIvCCDHIDCsECHHtE";
		public static string text_bg = "CAtJrsGJACFEuJEFswrGAJtBAtsIvIFAvI";
		public static string binary = "CAtIHAFuHssHGJEHGwJBsAvAvuEwCsJtJJ";
		public static string text_general_rev = "CAEuBFtAGCDIJvEDCIJtIJDFHBwEBsvvuu";
		public static string text_gen_sort = "CAGwBIJFtDBGvwEEFJrstBGFvIuICEHsBG";
		public static string text_cjk = "CAEwFHFwtGuFHFEFFGIDIuHHDCJFwwruDI";
		public static string pdates = "CABEGtJEFFsIrDEwIGJJvBEBHtAvvGIGBA";
		public static string pfloat = "CAAGJvFBEDGAwtEBwCsrHGGFHCuGwAswBI";
		public static string plongs = "CADtIEGAHtABBAEsAsJwAsuHIuJHEuGEIH";
		public static string random = "CAAswJHFJCJBCHEsEEIAJrFGwtCvtvtuvI";
		public static string pdate = "CArwwAJJCEBErIEDHtswrFBAAAIHAACCrI";
		public static string pint = "CADtBHvJDHJIDCErtIrDEBAEvCCGvttIsA";
		public static string text_general = "CAGBsBvIFBHrvEEwJIrFAIuFFsAGvJJJuD";
		public static string text_ca = "CAFvBvFJFDCHEwEJACrvvEtJDBtJuABuDB";
		public static string text_cz = "CABFHwJFCrAtAvEFsDJEJEJAHssHJACAGG";
		public static string text_da = "CAsAEuvFJwtwCDEHtCsuuDCruGtDrIuGtt";
		public static string text_de = "CAJJuswstJHJvsEEHsrCuBFHBwuIGGDJrv";
		public static string string_text = "CAuAJsBwDGtrDEEwFtJsJHIEIvCwIJtFGA";
		public static string pints = "CAswECvssDtwwsEHCvsGArwFCsCEEAAABE";
		public static string plong = "CAHIHCBAGFDIEBEGsFJuGCIEHJDDHAvEwJ";
		public static string point = "CAAsGJrCrJGHuvEJtvJDwstBsHDHHBIJCt";
		public static string text_en = "CAGJGIEtuvsrrsEEvFrHBFvtwDBwrIwuAu";
		public static string text_es = "CAEttDtIuAADErEGGusuAsIvHIIGrGuFGE";
		public static string text_eu = "CAvtJAGFuErCEFEDuBstGFEtJHrHJABEIr";
		public static string text_fa = "CAHrJFvrJJssCIEtFAruvCHurIBuCrAwwv";
		public static string text_fi = "CAsvIFGDvDDsIAEtuErsIGCrBuBrvGtwHB";
		public static string text_fr = "CAEAvtAwsEFGDJEFFBJEBuwDsCFJtwIJBF";
		public static string text_ga = "CAGJEtCuEACvsDEEBrIsDACtuFEvFrCHDw";
		public static string text_el = "CAJFDrFAAsGtJrEGFGrrBuvBuvArFEGGtF";
		public static string text_gl = "CAwGGBFBCAAuFrEHIFrDDBEtABFIAtAEFB";
		public static string text_hi = "CAHsEIBJECErAHErAFJsABwvwDCCAFsuHF";
		public static string text_hu = "CAIAJwFttsAwDsEwrBswGEBEEAGIsHDrFv";
		public static string text_id = "CAvAJBBsDEwrHvEEDGIHsvBBvGrHDABJtA";
		public static string text_it = "CABBvDvJCrFAsAEAFrJCDFBuwAGABAIABs";
		public static string text_ja = "CAADCIvrJGstEGEtADrIBwHJFwvtJvBADv";
		public static string text_lv = "CAICDsDCJtttvwEBrAJCJEGuHutJrGFJFF";
		public static string text_nl = "CACvHFJsuuAFwvEDJHIDrDrHBvtFtvJuFr";
		public static string text_no = "CAIEIBAvBCFFJFECrIsBrBrGBtGABCwFBB";
		public static string text_pt = "CAAsttwswIGvHAEuCusJCCswtHGAuDFGFJ";
		public static string text_ro = "CArwFttJEArsCCEBFFJCGvBsGAAGGEIACE";
		public static string text_ru = "CAFvvuBFEvuwFGEJFtJDGIrJEvCBuurAEt";
		public static string text_hy = "CAIHtDwJrAwCwtEuwArHuHHBErHEBvCEvH";
		public static string text_sv = "CABCGEDBGvutJwEGGCIvrtuHJCEFACFHBJ";
		public static string text_tr = "CAsuEtJFCHAJHuEIArrEHHwtuHDHtrJHAE";
		public static string text_ws = "CAGFAEJrCAvrvrEBvsJFJJwstwwGJCFrEB";
		public static string text_th = "CAtBrrJvDBFEDCEJJBsAsGttCwrDIHAFsJ";
	}
	public static class ControlTypes
	{
		public const string DatetimePicker = "BJGGusHAuCBssuEsIrJJErGErBtFCDsFsv";
		public const string SortableComboBox = "BJEEFAEsrwvsrsEIuvIwrswvIFBsvFtFrD";
		public const string SwitchButton = "BJBtBtruuAsGHvECtwrustAEsrsFDurCAt";
		public const string SourceEditor = "BJuFJBGFIBtCHDEGIwrvBsGuJHGDuuCBJr";
		public const string ColorPicker = "BJwAtJJssGwvuvEEvtJIvEIuGJHuEsFCIu";
		public const string TextBox = "BJAvvIsECCDwCJEJtGrJsAJIsIrtDstsGt";
		public const string None = "BJtFstuDGtvvvvEBsIJuFJDBwEIIvvBJBE";
		public const string RadioButton = "BJstHBIIDvCwICEDCrIvrBsDHrGJsrwuwu";
		public const string KeyValueBox = "BJHttsEAAIAvBJEvCArtIutJsBvwJBwBJB";
		public const string CodeEditor = "BJHFFAHBFrFDwAECDDrBrECFsEDDAGrCCE";
		public const string FileUpload = "BJCDFCtuDwAtrGEHrGsruDssFCwrrwtuDv";
		public const string InputMask = "BJCuHEJEJICBFJErwIsAIDrrwFEEtuHtJJ";
		public const string CheckBox = "BJBsBttEtEGwJCEEvHIvtGvsFCJtJIsuJA";
		public const string ComboBox = "BJBGtIFvvsusAGEtwJsttJADvHIsAFHBFF";
		public const string CheckList = "BJCtCutvAIEFEFEtHCICBtFGFAFsJuDEHI";
		public const string Html = "BJtEsHrFstDJEDEvvDIuJCsJwAEHsIEFuD";
		public const string SummerNote = "BJtCAEDBwEEwwIEIDBrJEJJJurDvBEJFsC";
		public const string EncrytText = "BJuGArrsGwBDtrEDBErGwrEuHsrwJJvHDF";
		public const string JsTree = "CAIGttwAIJGuFtErHuJBHIEwHJHGGrtFBt";
	}
	public static class TableAccess
	{
		public static string Modify = "BJHEtrtIFAEIsGEEFAItGsErwEvHAIuBGC";
		public static string View = "BJrruvwrBBustAEGEAswvvvCAFvCHvvrsr";
	}
	public static class FieldAccess
	{
		public static string Delete = "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv";
		public static string Modify = "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr";
		public static string View = "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt";
	}
	public static class TableTypes
	{
		public static string LoadOnScrollTable = "BJtFAGsCEEtvCCEAwAsCwIJCtsJHsItBJJ";
		public static string PaginationTable = "BJrCvDADEIFCCwEFEsJvvwErCCuFDBJAIA";
		public static string OfflineTable = "BJGBAIIFIwrBvHEvIIJEJCtuIFCJrEHBIE";
	}
	public static class Aggregate
	{
		public static string sum = "BJtDADAIIsrGsGECICJBwtvCHHtsstssGD";
		public static string avg = "BJsvCwuHGsCCCCEGJuJDuEEwItAvsBwGsJ";
		public static string min = "BJHAFwvHuBuAAAEuIBsEssHHCEHrtCDEII";
		public static string max = "BJJCCDIvAwAsEJEswrrwwFtDvGtrrFtFII";
	}
	public static class Options
	{
		public static string DateOptions = "BJAIuIEFCrGwwsEwBJJBEvvsGBJEHHDuvu";
	}
	public static class PageType
	{
		public static string Table = "CAECwDvJwCuGAIEsJBIDGAuDErtuIvvFus";
		public static string JsTree = "CAJwIABFvGBtBAEGrEsHtECwvtsCuEBusw";
		public static string OpenNew = "CAsArwuEJuBCDCEJuGIFrEJAEuvwGIFIAH";
		public static string Url = "CAAGsEDvIEwswuEvICrEHAHErIGDuwDvuv";
	}
	public static class Grouping
	{
		public static string JavaScriptLib = "CADvArDvEwuwrIEBIIsJJuFrvJwtDtsuuu";
		public static string Template = "CAswwHGsHEFAwHEDGssFJCvIsBBIvADuwC";
		public static string UserGroups = "CAuHwtCFEswDCvErHIJGBBGsrCtHIJBBBw";
		public static string ContextMenuGroup = "CADFvBsEDsvtDEEvFssHDwEDIvBJrwwAEA";
	}
	public static class ProgrammingLanguage
	{
		public static string C = "CAFFJCrtssHuGvEuFGIrtFsCAJFDDHJuHI";
	}
	public static class UserGroup
	{
		public static string Tester = "CAwHHuIIBDJJAGEvIrJGvsGGtJsIDBAJvJ";
		public static string Users = "175b8bb8e76b8f4802acd02adb49451003";
		public static string Public = "BJFJwJsHuFBBEBEADDIGsBEsvvuvAstAvr";
		public static string Developer = "17cf66df6ab13a47acaacb5cbf0bca1002";
		public static string Admin = "175b8bb8e76b8f4802acd02adb49451001";
	}
	public static class TemplateGroup
	{
		public static string GenerateClass = "CAurGJBHGAttFtEGEtIusuvBFrDEGuuGEw";
		public static string QRTemplate = "CAEEutJDBEvHIsEGCJsGDGGHvtsvAEvJtC";
		public static string Backup = "CADGDBADFFrFvGEvIJJvvAvtuHurvrssvJ";
		public static string MainContent = "CAuEJvtHAIHuvEEvrwIDvuBHsEwFtDtHEI";
		public static string DesignTheme = "CAFtDHBCJvBvBtEEFsIvCBGGwGDvCwArIr";
		public static string Script = "CAIDAEttwHJwssEsAGsADGvvtFGFwrEEsD";
		public static string API = "17c3288cf0b28749f89c16a36b06de1004";
		public static string SideBar = "CAEJItFBBrvwAHEBEtstGJGGEsvDFBtuBC";
		public static string Header = "CADBAEuwHvsFBHEJursFwssGGtAtIHrIws";
		public static string Footer = "CAHuuIGHDJHHHuEvIwssJsGEFwBEsJJCtC";
		public static string Css = "CArEtBGDrGIEuuEtCEruAsDJEuEAtFAJJt";
	}
	public static class WebLibaryGroup
	{
		public static string bootstrapdatetimepicker = "CAJIrvuJFFHIAsEvvuJwtDrFCGDCIArwwH";
		public static string bootstrapcolorpicker = "CABIHsDvuuBJBIECEuIAuJCwAItrGJAIJv";
		public static string jqueryblockUI = "CADwuJssCurEHHEtECssAsAwFvEIHAGsFt";
		public static string jqueryfileupload = "CAFAIrvJEssFwJECHwIvBEJtCJHIEJEwGs";
		public static string bootstrapswitch = "CAsBFtEItswFvvEuCuIEIuEFJJrCFvAuEt";
		public static string bootstrapmodal = "CAEtvsHvvJHJFBEGFIJCBtArCFsDGHrEJF";
		public static string jquerysortable = "CAHrADCwrtJuGJEwCHJCEtvGtHGCBDuAHA";
		public static string fontawesome = "CArEtuGwAtCIrJEwAJrvCGuCJDtuGvAEuG";
		public static string Select2 = "CAwCGJBvHtJDFBEAHFJIJEBsrGIEJGwAvw";
		public static string JsTree = "CADvsBDGEuuDErEuwrrsvCrvFEJvJrCGAB";
		public static string jquerymask = "CArBJAsBwHCvDtEAwusEIwvtFsArIDDHCv";
		public static string bootstrap = "CACDIJACBtwFvsEIIIIGDGErBsGuAtEJEF";
		public static string JqueryUI = "CAwCJErHHDuIvsEAvtJvrvDCFIrIFvAJGB";
		public static string Bootbox = "CAEvrFwruEtEvCEDJJIuABBIwGDHtDEHHu";
		public static string jQuery = "CArHstBAIuBEEHEDCDJsrtwsFwsIvJAtHA";
		public static string icheck = "CAAuAGHAHvFFssEsvuJHFGHCsBHvsFBFEC";
		public static string moment = "CArCrrJwvBGCwBEHuDswDBwwEsGGIFFEtv";
		public static string jqueryminicolors = "CADBEDvIDvtCuGECGwsrsrtuuIBFvrJJIr";
		public static string Summernote = "CACIDtAsGHArAJEGHFrrCGtrCFuADFDtvH";
		public static string BaseJs = "CAEDCBvwFsHusEEtJBrwuJwIAEurEADwHA";
	}
}
