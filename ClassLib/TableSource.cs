﻿using System.Collections.Generic;

namespace ClassLib
{
    public class TableSource
    {
        public TableSource()
        {
            Fields = new List<string>();
            //Default will return Id
            SelectedKeyField = "id";
            Filter = new Dictionary<string, List<string>>();      
            NewTag = false;
        }
        public string TableId { get; set; }
        public Dictionary<string, List<string>> Filter { set; get; }
        public string ViewResult { set; get; }
        public string SelectedKeyField { set; get; }
        public List<string> Fields { set; get; }
        public bool NewTag { set; get; }
        public bool Sortable { set; get; }
    }
}
