﻿using System.Collections.Generic;

namespace SimpleDatabase.Database
{
    public class QueryResult
    {
        public QueryResult()
        {
            docs = new List<Dictionary<string, object>>();
        }
        public int numFound { set; get; }
        public int start { set; get; }
        public List<Dictionary<string, object>> docs { set; get; }
    }

}
