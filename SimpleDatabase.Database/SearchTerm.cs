﻿using System.Collections.Generic;

namespace SimpleDatabase.Database
{
    public enum QueryType
    {
        RangeQuery,
        TextQuery,
        ExactMatch,
        CustomMatch,

    }
    public class SearchTerm
    {
        public string FieldId { set; get; }  
        public List<string> search { set; get; }
        public string Sort { set; get; }
        public QueryType queryType { set; get; }
    }
}
