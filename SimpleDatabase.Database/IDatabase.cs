﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace SimpleDatabase.Database
{
    public interface IDatabase
    {
        public void AddOrAlterField(string tableId, string fieldname, string datatype, object defaultvalue, bool multivalue, CancellationToken cancellationToken);        
        public void AddTable(string tableId, CancellationToken cancellationToken);
        public void DeleteField(string tableId, string fieldid, CancellationToken cancellationToken);
        public void DeleteTable(string tableId, CancellationToken cancellationToken);
        public void Insert(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken);
        public void Insert(string tableId, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken);
        //public void InsertOrUpdate(string tableId, Dictionary<string, object> searchTerm, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken);
        public QueryResult Query(string tableId, List<SearchTerm> searchTerm, List<string> showList,  CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue, bool orQuery = false); 
        public void Update(string tableId, List<Dictionary<string,object>> datalist, CancellationToken cancellationToken);
        public void Update(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken);


    }
}
