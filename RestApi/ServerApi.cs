﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RestApi
{
    public class ApiAuth
    {
        public ApiAuth()
        {
            UserName = "";
            Password = "";
        }
        public ApiAuth(string username,string password)
        {
            UserName = username;
            Password = password;
        }
        public string UserName { set; get; }
        public string Password { set; get; }
    }
    public static class ServerApi
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="apiPath"></param>
        /// <param name="obj"></param>
        /// <param name="auth"></param>
        /// <param name="timeout"></param>
        /// <param name="mediaType"></param>
        /// <param name="serverCertificateValidation"></param>
        /// <returns></returns>
        public static T Post<T>(string apiPath, object obj, ApiAuth auth = null, int timeout = 60, string mediaType = "application/json", bool serverCertificateValidation = false) where T : new()
        {
            RestApiResponse response = Post(apiPath, obj, auth, timeout, mediaType, serverCertificateValidation);
            if(string.IsNullOrEmpty(response.Error))
            {
                Task<string> result = response.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(result.Result);
            }
            throw new Exception(response.Error);
            
        }
        public static T Post<T>(string apiPath, object obj,CancellationToken cancellationToken, ApiAuth auth = null,  string mediaType = "application/json", bool serverCertificateValidation = false) where T : new()
        {
            RestApiResponse response = Post(apiPath, obj, cancellationToken, auth , mediaType, serverCertificateValidation);
            if (string.IsNullOrEmpty(response.Error))
            {
                Task<string> result = response.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(result.Result);
            }
            throw new Exception(response.Error);

        }
        /// <summary>
        /// Inconsistent return result use this method
        /// </summary>
        /// <param name="apiPath"></param>
        /// <param name="obj"></param>
        /// <param name="auth"></param>
        /// <param name="timeout"></param>
        /// <param name="mediaType"></param>
        /// <param name="serverCertificateValidation"></param>
        /// <returns></returns>
        public static RestApiResponse Post(string apiPath, object obj, ApiAuth auth = null, int timeout = 60, string mediaType = "application/json", bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();

                string content = string.Empty;
                switch (mediaType)
                {
                    case "text/xml":
                        content = obj.ToString();
                        break;
                    case "application/json":
                        content = JsonConvert.SerializeObject(obj);
                        break;
                    default:
                        content = JsonConvert.SerializeObject(obj);
                        break;
                }


                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                
                //var byteContent = new ByteArrayContent(buffer);
                //byteContent.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                }
                client.PostAsync(apiPath, new StringContent(content, Encoding.UTF8, mediaType)).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {

                            response.Result = t.Result;
                        }
                        else
                        {
                            Task<string> result = t.Result.Content.ReadAsStringAsync();                           
                            response.Error =$"StatusCode:{t.Result.StatusCode},Reason:{t.Result.ReasonPhrase}, Message:{result.Result}";

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {

                        response.Error = "Cancelled";

                    }
                    else
                    {
                        response.Error = t.Exception.ToString();
                    }

                }).Wait(timeout * 1000);
            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }
        public static RestApiResponse Post(string apiPath, object obj,CancellationToken cancellationToken, ApiAuth auth = null, string mediaType = "application/json", bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();

                string content = string.Empty;
                switch (mediaType)
                {
                    case "text/xml":
                        content = obj.ToString();
                        break;
                    case "application/json":
                        content = JsonConvert.SerializeObject(obj);
                        break;
                    default:
                        content = JsonConvert.SerializeObject(obj);
                        break;
                }


                var buffer = System.Text.Encoding.UTF8.GetBytes(content);

                //var byteContent = new ByteArrayContent(buffer);
                //byteContent.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                }
               
                client.PostAsync(apiPath, new StringContent(content, Encoding.UTF8, mediaType)).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {

                            response.Result = t.Result;
                        }
                        else
                        {
                            Task<string> result = t.Result.Content.ReadAsStringAsync();
                            response.Error = $"StatusCode:{t.Result.StatusCode},Reason:{t.Result.ReasonPhrase}, Message:{result.Result}";

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {

                        response.Error = "Cancelled";

                    }
                    else
                    {
                        response.Error = t.Exception.ToString();
                    }
                  

                },cancellationToken).Wait();             
    

            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }
        public static RestApiResponse Delete(string apiPath, int timeout = 60, ApiAuth auth = null, bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                }
                client.DeleteAsync(apiPath).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {
                            response.Result = t.Result;
                        }
                        else
                        {
                            response.Error = t.Result.ToString();

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {
                        response.Error = t.Result.ToString();
                    }
                    else
                    {
                        response.Error = t.Result.ToString();
                    }

                }).Wait(timeout * 1000);

            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }

        public static RestApiResponse Delete(string apiPath, object obj, int timeout = 60, ApiAuth auth = null, string mediaType = "application/json", bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Delete, apiPath);
                string content = string.Empty;
                switch (mediaType)
                {
                    case "text/xml":
                        content = obj.ToString();
                        break;
                    case "application/json":
                        content = JsonConvert.SerializeObject(obj);
                        break;
                    default:
                        content = JsonConvert.SerializeObject(obj);
                        break;
                }

                request.Content = new StringContent(content, Encoding.UTF8, mediaType);
                //client.SendAsync(request);
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                }
                client.SendAsync(request).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {
                            response.Result = t.Result;
                        }
                        else
                        {
                            response.Error = t.Result.ToString();

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {


                        response.Error = t.Result.ToString();

                    }
                    else
                    {
                        response.Error = t.Result.ToString();
                    }

                }).Wait(timeout * 1000);

            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }
        public static T Get<T>(string apiPath, CancellationToken cancellationToken, ApiAuth auth = null, bool serverCertificateValidation = false) where T : new()
        {
            RestApiResponse response = Get(apiPath, auth, 1, serverCertificateValidation);
            if (string.IsNullOrEmpty(response.Error))
            {
                Task<string> result = response.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(result.Result);
            }
            throw new Exception(response.Error);
        }
        public static T Get<T>(string apiPath, ApiAuth auth = null, int timeout = 60,  bool serverCertificateValidation = false) where T:new()
        {
            RestApiResponse response = Get(apiPath, auth, timeout, serverCertificateValidation);
            if (string.IsNullOrEmpty(response.Error))
            {
                Task<string> result = response.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(result.Result);
            }
            throw new Exception(response.Error);
        }

        public static RestApiResponse Get(string apiPath, CancellationToken cancellationToken, ApiAuth auth = null, bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                }

                client.GetAsync(apiPath).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {
                            response.Result = t.Result;
                        }
                        else
                        {
                            Task<string> result = t.Result.Content.ReadAsStringAsync();
                            response.Error = result.Result;

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {

                        response.Error = t.Result.ToString();

                    }
                    else
                    {
                        response.Error = t.Exception.ToString();
                    }

                }, cancellationToken).Wait();
       
            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }

        public static RestApiResponse Get(string apiPath, ApiAuth auth = null, int timeout = 60,  bool serverCertificateValidation = false)
        {
            RestApiResponse response = new RestApiResponse();
            try
            {
                if (!serverCertificateValidation)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                   delegate (object s, X509Certificate certificate,
                   X509Chain chain, SslPolicyErrors sslPolicyErrors)
                   { return true; };

                }
                HttpClient client = new HttpClient();
                if (auth != null)
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{auth.UserName}:{auth.Password}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                }
                client.GetAsync(apiPath).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        if (t.Result.IsSuccessStatusCode)
                        {
                            response.Result = t.Result;
                        }
                        else
                        {
                            Task<string> result = t.Result.Content.ReadAsStringAsync();
                            response.Error = result.Result;

                        }
                    }
                    else if (t.Status == TaskStatus.Canceled)
                    {

                        response.Error = t.Result.ToString();

                    }
                    else
                    {
                        response.Error = t.Exception.ToString();
                    }

                }).Wait(timeout * 1000);

            }
            catch (Exception ex)
            {
                response.Error = ex.ToString();
            }
            return response;
        }
    }


}
