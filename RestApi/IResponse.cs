﻿
using System.Net.Http;

namespace RestApi
{
    public interface IRestApiResponse
    {
        HttpResponseMessage Result { set; get; }
        string Error { set; get; }
    }
    public class RestApiResponse : IRestApiResponse
    {
        public HttpResponseMessage Result { set; get; }
        public string Error { set; get; }
    }
}
