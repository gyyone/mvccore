﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawlerService.ApiClasses
{
    public class PastResult
    {
        public string DrawDate { get; set; }
        public string DrawID { get; set; }
        public string DrawDay { get; set; }
        public string DrawDayZh { get; set; }
        public string FirstPrize { get; set; }
        public string SecondPrize { get; set; }
        public string ThirdPrize { get; set; }
        public string Special1 { get; set; }
        public string Special2 { get; set; }
        public string Special3 { get; set; }
        public string Special4 { get; set; }
        public string Special5 { get; set; }
        public string Special6 { get; set; }
        public string Special7 { get; set; }
        public string Special8 { get; set; }
        public string Special9 { get; set; }
        public string Special10 { get; set; }
        public string Console1 { get; set; }
        public string Console2 { get; set; }
        public string Console3 { get; set; }
        public string Console4 { get; set; }
        public string Console5 { get; set; }
        public string Console6 { get; set; }
        public string Console7 { get; set; }
        public string Console8 { get; set; }
        public string Console9 { get; set; }
        public string Console10 { get; set; }
        public string Powerball1 { get; set; }
        public string Powerball2 { get; set; }
        public string LifeNum1 { get; set; }
        public string LifeNum2 { get; set; }
        public string LifeNum3 { get; set; }
        public string LifeNum4 { get; set; }
        public string LifeNum5 { get; set; }
        public string LifeNum6 { get; set; }
        public string LifeNum7 { get; set; }
        public string LifeNum8 { get; set; }
        public string LifeBonusNum1 { get; set; }
        public string LifeBonusNum2 { get; set; }
        public string Jackpot1Amount { get; set; }
        public string Jackpot2Amount { get; set; }
        public string Jackpot1Winner { get; set; }
        public string Jackpot2Winner { get; set; }
        public string GoldJackpot1Amount { get; set; }
        public string GoldJackpot2Amount { get; set; }
        public string GoldJackpot1Winner { get; set; }
        public string GoldJackpot2Winner { get; set; }
        public string PowerballJackpot1Amount { get; set; }
        public string PowerballJackpot2Amount { get; set; }
        public string PowerballJackpot1aWinner { get; set; }
        public string PowerballJackpot1bWinner { get; set; }
        public string PowerballJackpot1cWinner { get; set; }
        public string PowerballJackpot2Winner { get; set; }
        public string LifePrize1Winner { get; set; }
        public string LifePrize2Winner { get; set; }
    }

    public class PastResultsRange
    {
        public List<PastResult> PastResults { get; set; }
    }

    public class MagnumResult
    {
        public PastResultsRange PastResultsRange { get; set; }
    }
}
