﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Converters;
using ClassExtension;
public partial class Draw4DGame
{


    public struct FieldsId
    {
        public const string Button = "CAJBIuBIvGEBvtECtAsCAwrHtGHFJwIDFB";
        public const string RowNo = "CABCuwrGCHICtuEJrvrCIsDHwABsDJsJIv";
        public const string DateVersion = "CADwAtErHHEAGGEJAvruBBDCCFFrvrDtDE";
        public const string id = "id";
        public const string ParentId = "CAEJAuvIuJrHFAEttJIwurrvwuDEIGsHwC";
        public const string ChangedBy = "CAHEAIFtsErEJrEEABICwsFHwuAHEIJJEB";
        public const string AccessRight = "CABwGAvBIJEtAAEJwArDCEAttDwHvDtEsu";
        public const string DrawDate = "CAAvvCsFwJBHwGEsvssEtvJGGFuHGIrDrI";
        public const string DrawType = "CAAEJCDuFsuGtIEHtCIICtIHFHGGIJCCBG";
        public const string FirstPrice = "CACsuAIEBtwEGIEAurJuDBIGsCCwJuGwwC";
        public const string SecondPrice = "CAtFDvuJAFEtCrEDsJIBtvAIHFsEGFuHBE";
        public const string ThirdPrice = "CAIArtDsssBHDsEDtCsBAAsFDGEtCAuBIr";
        public const string Consolation1 = "CAIDsJutBHGIFBEAwGsrCwCsHJEFrBCwrv";
        public const string Consolation2 = "CAuHrAFvAJIuJIECJGrAsEHvIvHHFBsJuD";
        public const string Consolation3 = "CAEusIEDJJBwBJEsruIBtwGEDEvDutAuJt";
        public const string Consolation4 = "CArGruvHsArIvFEJEFJvArvFIIwDuDFttw";
        public const string Consolation5 = "CArGGrtswBsHFBEEwEIvGEErGrHuHDwACv";
        public const string Consolation6 = "CACBACuFuuCAwHEurIJCstJusBIuItFwuD";
        public const string Consolation7 = "CAJuHAEFCFCCFJECDHJvrGvGDruFHJrFsE";
        public const string Consolation8 = "CAJuECtDFHHIvFEEJFrFJAAFuIwAsEswvE";
        public const string Consolation9 = "CAsruJBErtrtuvEwEusDEBIIvrFBDvGACH";
        public const string Consolation10 = "CArABCssGrGBFJEuCvIrrFuGIuGAtFIEru";
        public const string Special1 = "CAsHHvrHwJHtuHEJsJrGuDBBrJAwBGJAss";
        public const string Special2 = "CAEvJutvJCwuuAEHsCsHCrFtutvtHEAGCB";
        public const string Special3 = "CArrJIFIwtvvDwEwsCJEsFttDsvCCHtFGw";
        public const string Special4 = "CAAGDEIsBuGGAIEruIJDJJrICDBrCBGFAE";
        public const string Special5 = "CACCJvHHsCEEuuEtBtrtuGGEvIECJGBFFD";
        public const string Special6 = "CAwAGCtAAFEErsECGBIuDwsEADJDErCDDr";
        public const string Special7 = "CABBEtAJtJwCtsEtCrrwDJCtAJAJIuDwHG";
        public const string Special8 = "CAHtIuCtvrIvwHEAFvJGsBtuDBBBuGDADB";
        public const string Special9 = "CAvDAHJECsuwBAEDFHIGFvvAEvwwEDBsIr";
        public const string Special10 = "CAruGtIAIArrsvEswtIwIstBsuJAsJBtGt";

    }


    public Draw4DGame()
    {
        _Button = "";
        _RowNo = 0;
        _DateVersion = DateTime.Now;
        _id = "";
        _ParentId = "";
        _ChangedBy = "";
        _AccessRight = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(@"{""175b8bb8e76b8f4802acd02adb49450004"":[""View"",""Modify"",""Delete""],""175b8bb8e76b8f4802acd02adb49451001"":[""View"",""Delete"",""Modify""],""17cf66df6ab13a47acaacb5cbf0bca1002"":[""View"",""Delete"",""Modify""]}");
        _DrawDate = DateTime.Now;
        _DrawType = @"Magnum";
        _FirstPrice = "0000";
        _SecondPrice = "0000";
        _ThirdPrice = "0000";
        _Consolation1 = "0000";
        _Consolation2 = "0000";
        _Consolation3 = "0000";
        _Consolation4 = "0000";
        _Consolation5 = "0000";
        _Consolation6 = "0000";
        _Consolation7 = "0000";
        _Consolation8 = "0000";
        _Consolation9 = "0000";
        _Consolation10 = "0000";
        _Special1 = "0000";
        _Special2 = "0000";
        _Special3 = "0000";
        _Special4 = "0000";
        _Special5 = "0000";
        _Special6 = "0000";
        _Special7 = "0000";
        _Special8 = "0000";
        _Special9 = "0000";
        _Special10 = "0000";

    }


    private Dictionary<string, string> _Changed { set; get; }
    public Dictionary<string, string> Changed { set { _Changed = value; } get { return _Changed = _Changed ?? new Dictionary<string, string>(); } }
    public const string ClassID = "CAHIJvJuDsutGDEBtGIGrwHstuJGGDJJCI";
    public string Class_ID { get { return ClassID; } }

    private string _Button { set; get; }
    [JsonProperty(FieldsId.Button)]
    public string Button { set { Changed["CAJBIuBIvGEBvtECtAsCAwrHtGHFJwIDFB"] = "true"; _Button = value; } get { return _Button; } }

    private int _RowNo { set; get; }
    [JsonProperty(FieldsId.RowNo)]
    public int RowNo { set { Changed["CABCuwrGCHICtuEJrvrCIsDHwABsDJsJIv"] = "true"; _RowNo = value; } get { return _RowNo; } }

    private DateTime _DateVersion { set; get; }
    [JsonProperty(FieldsId.DateVersion)]
    public DateTime DateVersion { set { Changed["CADwAtErHHEAGGEJAvruBBDCCFFrvrDtDE"] = "true"; _DateVersion = value; } get { return _DateVersion; } }

    private string _id { set; get; }
    [JsonProperty(FieldsId.id)]
    public string id { set { Changed["id"] = "true"; _id = value; } get { return _id; } }

    private string _ParentId { set; get; }
    [JsonProperty(FieldsId.ParentId)]
    public string ParentId { set { Changed["CAEJAuvIuJrHFAEttJIwurrvwuDEIGsHwC"] = "true"; _ParentId = value; } get { return _ParentId; } }

    private string _ChangedBy { set; get; }
    [JsonProperty(FieldsId.ChangedBy)]
    public string ChangedBy { set { Changed["CAHEAIFtsErEJrEEABICwsFHwuAHEIJJEB"] = "true"; _ChangedBy = value; } get { return _ChangedBy; } }

    private Dictionary<string, List<string>> _AccessRight { set; get; }
    [JsonProperty(FieldsId.AccessRight)]
    public Dictionary<string, List<string>> AccessRight { set { Changed["CABwGAvBIJEtAAEJwArDCEAttDwHvDtEsu"] = "true"; _AccessRight = value; } get { return _AccessRight; } }

    private DateTime _DrawDate { set; get; }
    [JsonProperty(FieldsId.DrawDate)]
    public DateTime DrawDate { set { Changed["CAAvvCsFwJBHwGEsvssEtvJGGFuHGIrDrI"] = "true"; _DrawDate = value; } get { return _DrawDate; } }

    private string _DrawType { set; get; }
    [JsonProperty(FieldsId.DrawType)]
    public string DrawType { set { Changed["CAAEJCDuFsuGtIEHtCIICtIHFHGGIJCCBG"] = "true"; _DrawType = value; } get { return _DrawType; } }

    private string _FirstPrice { set; get; }
    [JsonProperty(FieldsId.FirstPrice)]
    public string FirstPrice { set { Changed["CACsuAIEBtwEGIEAurJuDBIGsCCwJuGwwC"] = "true"; _FirstPrice = value; } get { return _FirstPrice; } }

    private string _SecondPrice { set; get; }
    [JsonProperty(FieldsId.SecondPrice)]
    public string SecondPrice { set { Changed["CAtFDvuJAFEtCrEDsJIBtvAIHFsEGFuHBE"] = "true"; _SecondPrice = value; } get { return _SecondPrice; } }

    private string _ThirdPrice { set; get; }
    [JsonProperty(FieldsId.ThirdPrice)]
    public string ThirdPrice { set { Changed["CAIArtDsssBHDsEDtCsBAAsFDGEtCAuBIr"] = "true"; _ThirdPrice = value; } get { return _ThirdPrice; } }

    private string _Consolation1 { set; get; }
    [JsonProperty(FieldsId.Consolation1)]
    public string Consolation1 { set { Changed["CAIDsJutBHGIFBEAwGsrCwCsHJEFrBCwrv"] = "true"; _Consolation1 = value; } get { return _Consolation1; } }

    private string _Consolation2 { set; get; }
    [JsonProperty(FieldsId.Consolation2)]
    public string Consolation2 { set { Changed["CAuHrAFvAJIuJIECJGrAsEHvIvHHFBsJuD"] = "true"; _Consolation2 = value; } get { return _Consolation2; } }

    private string _Consolation3 { set; get; }
    [JsonProperty(FieldsId.Consolation3)]
    public string Consolation3 { set { Changed["CAEusIEDJJBwBJEsruIBtwGEDEvDutAuJt"] = "true"; _Consolation3 = value; } get { return _Consolation3; } }

    private string _Consolation4 { set; get; }
    [JsonProperty(FieldsId.Consolation4)]
    public string Consolation4 { set { Changed["CArGruvHsArIvFEJEFJvArvFIIwDuDFttw"] = "true"; _Consolation4 = value; } get { return _Consolation4; } }

    private string _Consolation5 { set; get; }
    [JsonProperty(FieldsId.Consolation5)]
    public string Consolation5 { set { Changed["CArGGrtswBsHFBEEwEIvGEErGrHuHDwACv"] = "true"; _Consolation5 = value; } get { return _Consolation5; } }

    private string _Consolation6 { set; get; }
    [JsonProperty(FieldsId.Consolation6)]
    public string Consolation6 { set { Changed["CACBACuFuuCAwHEurIJCstJusBIuItFwuD"] = "true"; _Consolation6 = value; } get { return _Consolation6; } }

    private string _Consolation7 { set; get; }
    [JsonProperty(FieldsId.Consolation7)]
    public string Consolation7 { set { Changed["CAJuHAEFCFCCFJECDHJvrGvGDruFHJrFsE"] = "true"; _Consolation7 = value; } get { return _Consolation7; } }

    private string _Consolation8 { set; get; }
    [JsonProperty(FieldsId.Consolation8)]
    public string Consolation8 { set { Changed["CAJuECtDFHHIvFEEJFrFJAAFuIwAsEswvE"] = "true"; _Consolation8 = value; } get { return _Consolation8; } }

    private string _Consolation9 { set; get; }
    [JsonProperty(FieldsId.Consolation9)]
    public string Consolation9 { set { Changed["CAsruJBErtrtuvEwEusDEBIIvrFBDvGACH"] = "true"; _Consolation9 = value; } get { return _Consolation9; } }

    private string _Consolation10 { set; get; }
    [JsonProperty(FieldsId.Consolation10)]
    public string Consolation10 { set { Changed["CArABCssGrGBFJEuCvIrrFuGIuGAtFIEru"] = "true"; _Consolation10 = value; } get { return _Consolation10; } }

    private string _Special1 { set; get; }
    [JsonProperty(FieldsId.Special1)]
    public string Special1 { set { Changed["CAsHHvrHwJHtuHEJsJrGuDBBrJAwBGJAss"] = "true"; _Special1 = value; } get { return _Special1; } }

    private string _Special2 { set; get; }
    [JsonProperty(FieldsId.Special2)]
    public string Special2 { set { Changed["CAEvJutvJCwuuAEHsCsHCrFtutvtHEAGCB"] = "true"; _Special2 = value; } get { return _Special2; } }

    private string _Special3 { set; get; }
    [JsonProperty(FieldsId.Special3)]
    public string Special3 { set { Changed["CArrJIFIwtvvDwEwsCJEsFttDsvCCHtFGw"] = "true"; _Special3 = value; } get { return _Special3; } }

    private string _Special4 { set; get; }
    [JsonProperty(FieldsId.Special4)]
    public string Special4 { set { Changed["CAAGDEIsBuGGAIEruIJDJJrICDBrCBGFAE"] = "true"; _Special4 = value; } get { return _Special4; } }

    private string _Special5 { set; get; }
    [JsonProperty(FieldsId.Special5)]
    public string Special5 { set { Changed["CACCJvHHsCEEuuEtBtrtuGGEvIECJGBFFD"] = "true"; _Special5 = value; } get { return _Special5; } }

    private string _Special6 { set; get; }
    [JsonProperty(FieldsId.Special6)]
    public string Special6 { set { Changed["CAwAGCtAAFEErsECGBIuDwsEADJDErCDDr"] = "true"; _Special6 = value; } get { return _Special6; } }

    private string _Special7 { set; get; }
    [JsonProperty(FieldsId.Special7)]
    public string Special7 { set { Changed["CABBEtAJtJwCtsEtCrrwDJCtAJAJIuDwHG"] = "true"; _Special7 = value; } get { return _Special7; } }

    private string _Special8 { set; get; }
    [JsonProperty(FieldsId.Special8)]
    public string Special8 { set { Changed["CAHtIuCtvrIvwHEAFvJGsBtuDBBBuGDADB"] = "true"; _Special8 = value; } get { return _Special8; } }

    private string _Special9 { set; get; }
    [JsonProperty(FieldsId.Special9)]
    public string Special9 { set { Changed["CAvDAHJECsuwBAEDFHIGFvvAEvwwEDBsIr"] = "true"; _Special9 = value; } get { return _Special9; } }

    private string _Special10 { set; get; }
    [JsonProperty(FieldsId.Special10)]
    public string Special10 { set { Changed["CAruGtIAIArrsvEswtIwIstBsuJAsJBtGt"] = "true"; _Special10 = value; } get { return _Special10; } }

}
