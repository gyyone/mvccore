﻿using CoreApi;
using RestApi;
using System;
using Topshelf;

namespace WebCrawlerService
{
    class Program
    {
        static void Main(string[] args)
        {

            var rc = HostFactory.Run(x =>
            {
                x.Service<CoreService>(s =>
                {

                    s.ConstructUsing(name => new CoreService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                x.StartAutomaticallyDelayed();

            });
        }
    }
}
