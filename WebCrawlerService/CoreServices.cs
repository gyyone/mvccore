using CoreApi;
using JsonHelper;
using LogHelper;
using RestApi;
using System;
using System.Collections.Generic;
using System.Reflection;
using ClassExtension;
using System.Linq;
using Newtonsoft.Json;
using System.Timers;
using System.Threading;
using WebCrawlerService.ApiClasses;
using SimpleDatabase.MariaDB;
using SimpleDatabase.Solr;
using SimpleDatabase.Database;

namespace WebCrawlerService
{
    public class Config
    {
        public Config()
        {
            ApiUrl = "https://localhost:5000/Api/";
            apiAuth = new ApiAuth("4DService", "123456");
        }
        public string ApiUrl { set; get; }
        public ApiAuth apiAuth { set; get; } 
    }
    public static class Settings
    {
        public static Config Cfg { set; get; }
    }
  

    public class CoreService
    {
        public bool Stopped { set; get; }
        public string configPath = AppDomain.CurrentDomain.BaseDirectory + "config.json";
        private System.Timers.Timer _tmrExecute { set; get; }
        List<Action> ActList { set; get; }
        public void Start()
        {
            //SimpleMariaDB maria = new SimpleMariaDB("SERVER=192.168.56.179;Database=test;UID=root;PASSWORD=123456;PORT=3306;");
            CancellationToken c = new CancellationToken();
            //SimpleDatabase.Database.QueryResult qr =  maria.Query("testtable", new List<SimpleDatabase.Database.SearchTerm>(), new List<string>() { "ABC", "Def" }, c, 2, 2);

            SimpleSolr solr = new SimpleSolr("http://localhost:8983", "solr", "SolrRocks", "solr", "solrGroup");
            SimpleDatabase.Database.QueryResult result = solr.Query("BJACttGIJJFAutEwuwrHFAuuFArABvrHCH", new List<SearchTerm>(), new List<string>() { "BJDGtsrDGGsBDvEwsusADGJtvsFuuDwvDF" }, c, 0, 4);
            //LogHandler.InitLog(CallBackLog);
            //Settings.Cfg = JsonIO.Load<Config>(configPath);
            //ActList = new List<Action>();
            //ActList.Add(InsertMagnumData);
            //_tmrExecute = new System.Timers.Timer();
            //_tmrExecute.Interval = 1000;
            //_tmrExecute.Elapsed += OnExecute;
            //_tmrExecute.Start();
            //_tmrExecute.Enabled = true;
            //Stopped = false;
        }
     
        private void OnExecute(object sender, ElapsedEventArgs e)
        {
            foreach(var act in ActList)
            {
                Thread th = new Thread(x => {
                    act();
                });
                th.Start();
            }
    
        }
        Dictionary<string, object> mutexDic = new Dictionary<string, object>();
        private void InsertMagnumData()
        {
            //if(Stopped)
            //{
            //    return;
            //}
            //if (!mutexDic.ContainsKey(magnumLock))
            //{
            //    mutexDic[magnumLock] = new object();
            //}
            //if (Monitor.TryEnter(mutexDic[magnumLock]))
            //{
            //    try
            //    {

            //        QueryItem qi = new QueryItem();
            //        qi.AddDateSearch(Draw4DGame.FieldsId.DrawDate, DateTime.Now);
            //        qi.AddSortDesc(Draw4DGame.FieldsId.DrawDate);
            //        QueryResult qr = ServerApi.Post<QueryResult>($"{Settings.Cfg.ApiUrl}Query/Select", new QueryItem() { TableId = Draw4DGame.ClassID, SearchTerm = new Dictionary<string, object>(), Columns = new List<string>() { "*" } }, Settings.Cfg.apiAuth);
            //        Draw4DGame drawgameResult = qr.Items.FirstOrDefault().CastToObject<Draw4DGame>();
            //        DateTime crawTime;
            //        DateTime crawEndTime = DateTime.Now;
            //        string dateformat = "dd-MM-yyyy";
            //        if (drawgameResult == null)
            //        {
            //            crawTime = new DateTime(1985, 04, 25);
            //        }
            //        else
            //        {
            //            crawTime = drawgameResult.DrawDate.AddDays(1);
            //        }
            //        int crawrecord = 9;
            //        while (crawEndTime > crawTime && !Stopped)
            //        {
            //            MagnumResult mgresult = ServerApi.Get<MagnumResult>($"https://webservices.magnum4d.my/results/past/between-dates/{crawTime.ToString(dateformat)}/{crawTime.AddDays(crawrecord).ToString(dateformat)}/{crawrecord}");
            //            crawTime = crawTime.AddDays(crawrecord);
            //            List<Dictionary<string, object>> updatelist = new List<Dictionary<string, object>>();
            //            foreach(PastResult result in mgresult.PastResultsRange.PastResults)
            //            {
            //                try
            //                {
            //                    Draw4DGame draw4d = new Draw4DGame();
            //                    draw4d.FirstPrice = result.FirstPrize;
            //                    draw4d.SecondPrice = result.SecondPrize;
            //                    draw4d.ThirdPrice = result.ThirdPrize;
            //                    draw4d.Consolation1 = result.Console1;
            //                    draw4d.Consolation2 = result.Console2;
            //                    draw4d.Consolation3 = result.Console3;
            //                    draw4d.Consolation4 = result.Console4;
            //                    draw4d.Consolation5 = result.Console5;
            //                    draw4d.Consolation6 = result.Console6;
            //                    draw4d.Consolation7 = result.Console7;
            //                    draw4d.Consolation8 = result.Console8;
            //                    draw4d.Consolation9 = result.Console9;
            //                    draw4d.Consolation10 = result.Console10;
            //                    draw4d.Special1 = result.Special1;
            //                    draw4d.Special2 = result.Special2;
            //                    draw4d.Special3 = result.Special3;
            //                    draw4d.Special4 = result.Special4;
            //                    draw4d.Special5 = result.Special5;
            //                    draw4d.Special6 = result.Special6;
            //                    draw4d.Special7 = result.Special7;
            //                    draw4d.Special8 = result.Special8;
            //                    draw4d.Special9 = result.Special9;
            //                    draw4d.Special10 = result.Special10;
            //                    draw4d.DrawType = "Magnum";
            //                    draw4d.DrawDate = result.DrawDate.ConvertToDate("dd/MM/yyyy");
            //                    updatelist.Add(draw4d.ToDictionary());
                                
            //                }
            //                catch(Exception ex)
            //                {
            //                    LogHandler.Fatal(ex);
            //                }
                            
            //            }
            //            QueryResult queryResult = ServerApi.Post<QueryResult>($"{Settings.Cfg.ApiUrl}Query/InsertOrUpdate", new InsertOrUpdateItem() { ParentId = "#", TableId = Draw4DGame.ClassID, updateItems = updatelist }, Settings.Cfg.apiAuth);
            //            if (queryResult.Error)
            //            {
            //                LogHandler.Fatal(queryResult.Message);
            //            }
            //            else
            //            {
            //                LogHandler.Debug($"Draw Date {crawTime.ToString(dateformat)}" + queryResult.Message);
            //            }
            //        }

                   



            //    }
            //    catch
            //    {

            //    }
            //    Monitor.Exit(mutexDic[magnumLock]);
            //}

        }
        const string magnumLock = "magnum";
        public void CallBackLog(object obj)
        {
           

        }
        
        public void Stop()
        {
            Stopped = true;
            LogHandler.Debug("Service Stopped.");
            foreach(var x in mutexDic)
            {
                Monitor.Enter(x.Value);
            }

            foreach (var x in mutexDic)
            {
                Monitor.Exit(x.Value);
            }

        }
    }
}