﻿using System.Collections.Generic;
using System.Linq;

namespace Util
{
    public static class TextMap
    {
        /// <summary>
        /// Source look at suitable Target
        /// </summary>
        /// <param name="Sources">Source will find a suitable Target for It. Most similar Pair will consider the source of Target</param>
        /// <returns>Target Mapped Default Minimum Maping is 0</returns>
        public static string FindMapping(string Source, string[] Targets)
        {
            return FindMapping(Source, Targets, 0);
        }
        /// <summary>
        /// Source look at suitable Target . Return Content of DatabaseTable.MapInfoTable
        /// </summary>
        /// <param name="Sources">Source will find a suitable Target for It. Most similar Pair will consider the source of Target</param>
        /// <param name="MinScore"></param>
        /// <returns>Target Mapped</returns>
        public static string FindMapping(string Source, string[] Targets, double MinScore)
        {
            double score = 0;
            return FindMapping(Source, Targets, MinScore, ref score);
        }
        public static string FindMapping(string Source, string[] Targets, ref double Score)
        {

            return FindMapping(Source, Targets, 0, ref Score);
        }
        /// <summary>
        /// Source look at suitable Target . Return Content of DatabaseTable.MapInfoTable
        /// </summary>
        /// <param name="Sources">Source will find a suitable Target for It. Most similar Pair will consider the source of Target</param>
        /// <param name="MinScore"></param>
        /// <returns>Target Mapped</returns>
        public static string FindMapping(string Source, string[] Targets, double MinScore, ref double Socre)
        {
            //Table Tb = new KeySearchTable(SourcePath, TableName);

            //Class Name Compare with SQID . Find the most suitable Pair Class name with SQID      
            Dictionary<string, double> _DicResult = new Dictionary<string, double>();

            foreach (string Target in Targets)
            {
                double posibility = 0.0f;
                posibility = FuzyLogic.LevenshteinEdit(Source, Target);

                //posibility = FuzyLogic.ContinuousSimilar(Source, Target);
                if (!_DicResult.ContainsKey(Target))
                {
                    _DicResult.Add(Target, posibility);
                }

            }
            if (!_DicResult.Any())
            {
                return string.Empty;
            }
            double Max = (from x in _DicResult select x.Value).Max();
            KeyValuePair<string, double> MaxValue = (from keypari in _DicResult where keypari.Value == Max select keypari).First();
            Socre = MaxValue.Value;

            return MaxValue.Value > MinScore ? MaxValue.Key : string.Empty;

        }
    }
}
