﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Util;

namespace Security
{
   

    public class SimpleAES
    {
        private const int BlockSize = 128;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">AES128 = 16 lenght,AES192 = 24 lenght,AES256=32 lenght</param>
        /// <param name="iv">Standard lenght 16 byte</param>       
        public SimpleAES(byte[] key,byte []iv)
        {
            this.Key = key;
            this.IV = iv;     
        }

        public byte[] Key { set; get; }
        public byte[] IV { set; get; }  


        public string Encrypt(byte[] plainText)
        {
            return Encoding.Unicode.GetString(Encrypt(Encoding.Unicode.GetString(plainText), Key, IV));
        }
        public string Encrypt(string plainText)
        {        
            return Encoding.Unicode.GetString(Encrypt(plainText, Key, IV));
        }

        public string Decrypt(byte[] chiperText)
        {
            return Decrypt(chiperText, Key, IV);
        }
        public string Decrypt(string chiperText)
        {
            return Decrypt(Encoding.Unicode.GetBytes(chiperText), Key, IV);
        }
        public byte[] EncryptToByte(string plainText)
        {
            return Encrypt(plainText, Key, IV);
        }

        public byte[] DecryptToByte(byte[] chipher)
        {
            return Encoding.Unicode.GetBytes(Decrypt(chipher, Key, IV)) ;
        }

        public static byte[] GenerateIV(string password)
        {        
            byte[] temp = Encoding.ASCII.GetBytes(password);
            byte[] keySize = new byte[16];
            for (int x = 0; x < keySize.Length; x++)
            {
                if (temp.Length > x)
                {
                    keySize[x] = temp[x];
                }
                else
                {
                    keySize[x] = (byte)(x % 256);
                }
            }
            return keySize;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="size">AES128 = 16,AES192 = 24,AES256=32</param>
        /// <returns></returns>
        public static byte[] GenerateKey(string password,int size)
        {
            // 32 byte of Key Size * 8 bit = 256 bit of BlockSize
            byte[] temp = Encoding.ASCII.GetBytes(password);
            byte[] keySize = new byte[size/8];
            for(int x =0; x < keySize.Length; x++)
            {
                if(temp.Length > x)
                {
                    keySize[x] = temp[x];
                }
                else
                {
                    keySize[x] =(byte) (x % 256);
                }           
            }
            return keySize;
        }

        public static string Encrypt(string plainText, string Key, int keylenght, string IV)
        {        
            byte []b = Encrypt(plainText, GenerateKey(Key, keylenght), GenerateIV(IV));
            return Convert.ToBase64String(b);
        }

        private static byte[] Encrypt(string plainText, byte[] Key, byte[] IV)
        {
            byte[] encrypted;
            // Create a new AesManaged.    
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {              
                // Create encryptor    
                ICryptoTransform encryptor = aes.CreateEncryptor(Key, IV);
               
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (StreamWriter sw = new StreamWriter(cs))
                            sw.Write(plainText);
                        encrypted = ms.ToArray();
                    }
                }
            }
            // Return encrypted data    
            return encrypted;
        }
        public static string Decrypt(string cipherText, string Key,int keyLengh, string IV)
        {
            return Decrypt(Convert.FromBase64String(cipherText), GenerateKey(Key,keyLengh),GenerateIV(IV));
        }
        private static string Decrypt(byte[] cipherText, byte[] Key, byte[] IV)
        {
            string plaintext = null;
            // Create AesManaged    
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                // Create a decryptor    
                ICryptoTransform decryptor = aes.CreateDecryptor(Key, IV);
                // Create the streams used for decryption.          
                using (MemoryStream ms = new MemoryStream(cipherText))
                {
                    // Create crypto stream    
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        // Read crypto stream    
                        using (StreamReader reader = new StreamReader(cs))
                            plaintext = reader.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

  
    }

}
