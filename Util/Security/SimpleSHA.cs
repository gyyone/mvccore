﻿using System.Security.Cryptography;
using System.Text;

namespace Security
{
    public static class SimpleSHA
    {
        public static string EncryptOneWay(string text)
        {
            if(string.IsNullOrWhiteSpace(text))
            {
                return text;
            }
            byte[] data = Encoding.ASCII.GetBytes(text);
            SHA256 shaM = new SHA256Managed();
            return Encoding.ASCII.GetString(shaM.ComputeHash(data));
        }
    }
}
