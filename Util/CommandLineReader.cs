﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Util
{
    public static class CommandLineReader
    {
        private static Dictionary<string, System.Collections.Generic.List<string>> MessageDic = new Dictionary<string, List<string>>();
        private static Dictionary<string, StringBuilder> ErrorDic = new Dictionary<string, StringBuilder>();
        public static List<string> Run(string filename, string args, out bool error)
        {
            List<string> output = new List<string>();
            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(filename, args);
                psi.UseShellExecute = false;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardInput = false;
                psi.RedirectStandardError = true;
                psi.CreateNoWindow = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                var proc = Process.Start(psi);
                error = false;
                while (!proc.StandardOutput.EndOfStream)
                {
                    output.Add(proc.StandardOutput.ReadLine());
                }
                while (!proc.StandardError.EndOfStream)
                {
                    output.Add(proc.StandardError.ReadLine());
                    error = true;
                }
                proc.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Command Error");
                error = true;
                output.Add(ex.ToString());
            }
            finally
            {

            }
            //Console.WriteLine("Output " + output.Count);
            return output;
        }
        public static List<string> Run(string filename, string input, string exitCommand, out bool error)
        {
            List<string> output = new List<string>();
            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(filename);
                psi.UseShellExecute = false;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardInput = true;
                psi.CreateNoWindow = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.RedirectStandardError = false;
                var proc = Process.Start(psi);
                proc.StandardInput.WriteLine(input);
                proc.StandardInput.WriteLine(exitCommand);
                error = false;
                while (!proc.StandardOutput.EndOfStream)
                {
                    output.Add(proc.StandardOutput.ReadLine());
                }
                while (!proc.StandardError.EndOfStream)
                {
                    output.Add(proc.StandardError.ReadLine());
                    error = true;
                }
                proc.Close();

            }
            catch (Exception ex)
            {
                error = true;
                output.Add(ex.ToString());
            }
            finally
            {

            }

            return output;
        }

        private static void ErrorReceived(string id, string data)
        {

            if (!string.IsNullOrEmpty(data))
            {
                if (!ErrorDic.ContainsKey(id))
                {
                    ErrorDic[id] = new StringBuilder();
                }
                ErrorDic[id].AppendLine(data);
            }


        }

        private static void Received(string id, string data)
        {

            if (!string.IsNullOrEmpty(data))
            {
                if (!MessageDic.ContainsKey(id))
                {
                    MessageDic[id] = new List<string>();
                }
                MessageDic[id].Add(data);
            }

        }

        private static void KillProcess(Process p)
        {
            try
            {
                p.Kill();
            }
            catch (Exception e)
            {

            }
        }
    }
}
