﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClassExtension;
namespace Util
{
    public static class AkkaHandler
    {
        public static string GetConfig(Dictionary<string, string> param)
        {
            if(!string.IsNullOrWhiteSpace(param["seed-nodes"]))
            {
                string cluster = "cluster = " + param["seed-nodes"];
            }
            return GetAkkaConf().ReplaceFormat(param);
        }
        public static Dictionary<string,string> GetParam(string hostname = "0.0.0.0",string remotehost= "WebCrawler.com", int port = 7000,int remoteport=7000)
        {
            return new Dictionary<string, string>() {
                { "serializer", "Util.CompatJsonSerializer, Util" },
                {"port",$"{port}" },
                { "hostname",hostname},
                { "public-hostname","WebCrawler.com"},
                { "enable-ssl", "false"},
                { "certpath", "ca.pfx"},
                { "certpassword", "P@ssw0rd"},
                { "certflag", "machine-key-set"},
                { "seed-nodes", "\"akka.tcp://clusterSystem@localhost@8081\""},
                { "remotehost", $"{remotehost}"},
                { "remoteport", $"{remoteport}"}
            };
        }
  
        public static string GetClusterSeed(string clusterName, int port, bool enableSSL,List<string> clusterNodes)
        {
            string tcp = "akka.tcp";
            if (enableSSL)
            {
                tcp = "akka.ssl.tcp";
            }

            return string.Join(",", clusterNodes.Select(x=> "\"" + tcp + "://" + clusterName + "@" + x + ":" + port + "\"")); ;
        }
        private static string GetAkkaConf()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "akka.txt";
            return File.ReadAllText(path);
        }

        public static string GetSelector(string systemname,string actorName,Dictionary<string,string> parameter)
        {
            string tcp = "akka.tcp";
            if (parameter["enable-ssl"].ToLower() == "true")
            {
                tcp = "akka.ssl.tcp";
            }
            return tcp + string.Format("://{0}@{1}:{2}/user/{3}", systemname, parameter["remotehost"], parameter["remoteport"], actorName);
        }

    }
}
