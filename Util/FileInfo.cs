﻿using System.Collections.Generic;
using System.IO;

namespace Util
{
    public class FileInfo
    {
        public FileInfo(string id, string fileName)
        {
            Id = id;
            FileName = fileName;
            //Default image able to access by everyone
            AccessRight = new List<string>() { "BJFJwJsHuFBBEBEADDIGsBEsvvuvAstAvr" };
        }
        public string FileName { set; get; }
        public string Id { set; get; }
        public List<string> AccessRight { set; get; }
        public string GetSavedFile()
        {
            return $"{Id}{Path.GetExtension(FileName)}";
        }

    }
}