﻿using CsvHelper;
using DirectoryHelper;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Text;

namespace Util
{
    public static class CSVExt
    {
        public static void WriteCSV(string pathFile,List<Dictionary<string,object>> items,List<string> header)
        {
            List<dynamic> dynamiclist = new List<dynamic>();
            foreach(var i in items)
            {
                var eoColl = (new ExpandoObject()) as IDictionary<string, object>;             
                foreach (var h in header)
                {
                    if(i.ContainsKey(h))
                    {
                        eoColl[h] = i[h];
                    }
                    else
                    {
                        eoColl[h] = "";
                    }
                  
                }
                dynamiclist.Add(eoColl);
            }
            DirHelper.CreateDirectory(pathFile);
            using (CsvWriter csv = new CsvWriter(new StreamWriter(pathFile,true)))
            {
                //csv.WriteDynamicHeader((dynamic)eoColl);
                csv.WriteRecords(dynamiclist);
            }
        }

        public static void WriteCSVHeader(string pathFile, Dictionary<string,string> header)
        {
            List<dynamic> dynamiclist = new List<dynamic>();
            var eoColl = (new ExpandoObject()) as IDictionary<string, object>;
            foreach (var h in header)
            {
                eoColl[h.Key] = h.Value;
            }
            dynamiclist.Add(eoColl);
            DirHelper.CreateDirectory(pathFile);
            using (CsvWriter csv = new CsvWriter(new StreamWriter(pathFile, true)))
            {
                //csv.WriteDynamicHeader((dynamic)eoColl);
                csv.WriteRecords(dynamiclist);
            }
        }
    }
}
