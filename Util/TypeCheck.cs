﻿using System;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Util
{
    public static class TypeCheck<T>
    {
        public static List<T> CastToList(object obj)
        {
            if (obj == null || (obj != null && string.IsNullOrEmpty(obj.ToString())))
            {
                return new List<T>();
            }
            else if (obj is Newtonsoft.Json.Linq.JArray)
            {
                return JsonConvert.DeserializeObject<List<T>>(obj.ToString());
            }
            else if (obj.GetType().IsGenericType || obj.GetType().IsArray)
            {
                List<T> temp = new List<T>();
                var objectlist = obj as IList;
                Type type = objectlist.GetType().GetGenericArguments()[0];
                var listType = typeof(List<>);         
                foreach (var x in objectlist)
                {
                    temp.Add((T)Convert.ChangeType(x, type));
                }
                

                return temp;
            }
            else
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<T>>(obj.ToString());
                }
                catch
                {

                }

            }

            return new List<T>() {(T)Convert.ChangeType(obj, typeof(T))  };
        }
        public static T TryCast(object obj)
        {
            try
            {
                if(obj == null)
                {
                    return (T)obj;
                }
                if(obj.GetType() == typeof(T))
                {
                    return (T)obj;
                }
                else if (!TypeCheck.IsReserveType(typeof(T)))
                {
                    return JsonConvert.DeserializeObject<T>(obj.ToString());
                }
                else if(TypeCheck.IsMultiValue(obj) && !TypeCheck.IsMultiValueType(typeof(T))  )
                {
                    object objtemp = TypeCheck.CastToList(obj).FirstOrDefault();
                    return (T)Convert.ChangeType(objtemp, typeof(T));                    
                }
                else if ((!TypeCheck.IsMultiValue(obj) && TypeCheck.IsMultiValueType(typeof(T))) ||(TypeCheck.IsMultiValue(obj) && TypeCheck.IsMultiValueType(typeof(T))))
                {
                      var temp = default(T);
                   Type type =  temp.GetType().GetGenericArguments()[0];
                    var listType = typeof(List<>);
                    var constructedListType = listType.MakeGenericType(type);

                    var instance =(IList) Activator.CreateInstance(constructedListType);
                    var listobj = TypeCheck.CastToList(obj);
                    if(listobj != null)
                    {
                        foreach (var x in listobj)
                        {
                            instance.Add(Convert.ChangeType(x, type));
                        }
                    }
                            
                    return (T)(instance);
                }
     
                return (T)Convert.ChangeType(obj, typeof(T));

            }
            catch(Exception ex)
            {
                
            }

            return default(T);
        }
    }

    public static  class TypeCheck
    {
        public static object GetObjectFromStr(string jsonstr,Type t)
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter { });
            settings.TypeNameHandling = TypeNameHandling.Auto;
            return JsonConvert.DeserializeObject(jsonstr,t, settings);
        }
        public static bool IsReserveType(object obj)
        {

            try
            {
                if (obj == null || obj is Newtonsoft.Json.Linq.JArray)
                {
                    return false;
                }
                Type p = obj.GetType();
                return IsReserveType(p);             
            }
            catch(Exception ex)
            {

            }
             return false;
        }
        public static bool IsReserveType(Type p)
        {

            if (p == typeof(Newtonsoft.Json.Linq.JArray))
            {
                return false;
            }
            if (p == typeof(DateTime) ||
                p == typeof(string) ||
                p == typeof(int) ||
                p == typeof(double) ||
                p == typeof(long) ||
                p == typeof(float) ||
                p == typeof(bool) ||
                p.IsEnum ||
                (p.IsArray && p.GetGenericTypeDefinition() == typeof(List<>) ) 
           
                )
            {
                return true;
         
            }
            return false;
        }

        public static bool IsMultiValue(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            Type t = obj.GetType();          

            return t.IsGenericType || t.IsArray || obj is Newtonsoft.Json.Linq.JArray;
        }
        public static bool IsMultiValueType(Type  t)
        {            
            return t.IsGenericType || t.IsArray || t == typeof(Newtonsoft.Json.Linq.JArray);
        }
        public static List<string> CastToList(object obj)
        {
            if(obj == null || (obj != null &&  string.IsNullOrEmpty(obj.ToString())))
            {
                return new List<string>();
            }
            else if(obj is Newtonsoft.Json.Linq.JArray)
            {
                return JsonConvert.DeserializeObject<List<string>>(obj.ToString());
            }
            else if(obj.GetType().IsGenericType || obj.GetType().IsArray)
            {
                List<string> temp = new List<string>();
                try
                {
                    var objectlist = obj as IList;
                    Type type = objectlist.GetType().GetGenericArguments().FirstOrDefault();
                
                    if(type == null)
                    {
                        return temp;
                    }                 
                    foreach (var x in objectlist)
                    {
                        if (x != null)
                        {
                            temp.Add(x.ToString());
                        }
                    }
                 
                }
                catch(Exception ex)
                {

                }
                return temp;
            }
            else 
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<string>>(obj.ToString());
                }
                catch(Exception ex)
                {
                   
                }
            
            }

            return new List<string>() { obj.ToString() };
        }
        public static Dictionary<string,object> CastToDic(string jsonObject)
        {
            Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string,object>>(jsonObject);
            return temp;

        }
        //multipleLevel object will cast to string
        /// <summary>
        /// Automatic cast multiple level to string , first level will base on th value
        /// Only accept First level with List, array,string
        /// Multilevel: object,key value pair
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static object CastToObject(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            else if (IsReserveType(obj))
            {
                return obj;
            }     
            else if (obj is Newtonsoft.Json.Linq.JArray)
            {
                List<object> objlist =  JsonConvert.DeserializeObject<List<object>>(obj.ToString());
                bool havesuboject = objlist.Any(x => x is Newtonsoft.Json.Linq.JArray || x is Newtonsoft.Json.Linq.JObject);
                if(havesuboject)
                {
                    return ((JObject)obj).ToString(Formatting.None);
                }
                else
                {
                    return JsonConvert.DeserializeObject<List<string>>(obj.ToString());
                }
            }
            else if (obj.GetType().IsGenericType && obj.GetType().IsArray)
            {
                return obj as List<string>;
            }    
            else
            {
                return ((JObject)obj).ToString(Formatting.None);

            }

           
        }


        public static bool CastToBool(object value)
        {
            bool x;
            if(value == null || string.IsNullOrWhiteSpace(value.ToString()))
            {
                return false;
            }
            else if(value is bool || value is Boolean)
            {
                return (bool)value;
            }
            if(bool.TryParse(value.ToString().ToLower(),out x))
            {
                return x;
            }
            return false;
       
        }
        public static bool IsKeyValue(string controlType)
        {
            switch (controlType)
            {         
                case "KeyValueBox":
                    return true;
            }
            return false;
        }

        public static bool IsLookUp(string controlType)
        {
            switch (controlType)
            {
                //CheckList
                case "BJCtCutvAIEFEFEtHCICBtFGFAFsJuDEHI":
                    //Combobox
                case "BJBGtIFvvsusAGEtwJsttJADvHIsAFHBFF":
                    //Sortable Combobox
                case "BJEEFAEsrwvsrsEIuvIwrswvIFBsvFtFrD":
                    //KeyValue
                case "BJHttsEAAIAvBJEvCArtIutJsBvwJBwBJB":
                    //radio button
                case "BJstHBIIDvCwICEDCrIvrBsDHrGJsrwuwu":
                    return true;             
            }
            return false;
        }
        public static string GetJsonPropertyName(object obj,string name)
        {
            Type t = obj.GetType();
            List<JsonPropertyAttribute> list  = t.GetProperties().Where(x=> x.Name.Equals(name))
                         .Select(p => p.GetCustomAttribute<JsonPropertyAttribute>()).ToList();
            if(list.Any())
            {
                return list.FirstOrDefault().PropertyName;
            }
            return "";
        }
    }

}
