﻿

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Completion;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Util
{

    public static class RuntimeCompliler
    {
        public static Func<string, object> EvalCallback { set; get; }
        public static Func<string,string, object> EvalCallbackCache { set; get; }
        public static Func<string,List<string>,List<PortableExecutableReference>, string, List<CompletionItem>> GetCompleterItem { set; get; }

       
    }
    public static class RuntimeCompliler<T>
    {
        private static object codeLock = new object();
        public static T Eval(string code)
        {
            lock(codeLock)
            {
                return (T)RuntimeCompliler.EvalCallback(code);
            }
           
        }
        public static T EvalWithCache(string code,string key)
        {
            lock (codeLock)
            {
                return (T)RuntimeCompliler.EvalCallbackCache(code,key);
            }

        }

        public static T InvokeMethod(string typeName, string methodName, params object[] args)
        {
            // Get the Type for the class
            Type calledType = Type.GetType(typeName);

            // Invoke the method itself. The string returned by the method winds up in s
            T s = (T)calledType.InvokeMember(
                            methodName,
                            BindingFlags.InvokeMethod | BindingFlags.Public |
                                BindingFlags.Static,
                            null,
                            null,
                            args);

            // Return the string that was returned by the called method.
            return s;
        }
    }

}
