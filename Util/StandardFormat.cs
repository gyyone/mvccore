﻿using System.Text.RegularExpressions;

namespace Util
{
    public static class StandardFormat
    {
        //If all date format save as standard, it will easily to convert to any format
    
  
        public static string NumberFormat = @"[^0-9\d]";

        public static string GetCleanName(string text)
        {
            return Regex.Replace(text, @"[^\w\d]", "");
        }
    }
}