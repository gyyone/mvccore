﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Util
{
    //********************************Comment Knowledge***********************
    //Levenshtein distance
    //Levenshtein Edit distance
    //Dynamic Programing
    //Fuzzy Logic Search Engine
    public static class FuzyLogic
    {



        /// <summary>
        /// Levenshtein Edit distance Logic. Calculate The Similarity of the two Compare Text
        /// Will Take considering of SHORT FORM when compare. Example HSTP with HOSTPITAL
        /// </summary>
        /// <param name="Text1">string</param>
        /// <param name="Text2">string</param>
        /// <returns> Return Posibility 0 -  1 seen to be same. 1 denoted Exactly Same</returns>
        public static double LevenshteinEdit(string Text1, string Text2)
        {
            Text1 = Text1.ToLower();
            Text2 = Text2.ToLower();
            double PosibleSame = (100 - Levenshtein.LDEdit(Text1, Text2)) / 100.000f;

            return PosibleSame;


        }
        /// <summary>
        /// Levenshtein distance Logic. Calculate The Similarity of the two Compare Text
        /// </summary>
        /// <param name="Text1"></param>
        /// <param name="Text2"></param>
        /// <returns>Return Posibility 0 -  1 seen to be same. 1 denoted Exactly Same</returns>
        public static double LevenshteinDistance(string Text1, string Text2)
        {
            Text1 = Text1.ToLower();
            Text2 = Text2.ToLower();

            double PosibleSame = (100 - Levenshtein.LD(Text1, Text2)) / 100.00f;
            return PosibleSame;
        }
        /// <summary>
        /// Check For Continuos Similarlity
        /// </summary>
        /// <param name="Text1"></param>
        /// <param name="Text2"></param>
        /// <returns>Return Posibility 0 -  1 seen to be same. 1 denoted Exactly Same</returns>
        public static double ContinuousSimilar(string Text1, string Text2)
        {
            Text1 = Text1.ToLower();
            Text2 = Text2.ToLower();

            double PosibleSame = Levenshtein.LDContinuousSimilar(Text1, Text2);
            return PosibleSame;
        }
        /// <summary>
        /// Return The Score of counting Continuos char map identity.
        /// Example: 'ABCD EA'  with 'ABCD FB' : Continuos equal is 4
        /// </summary>
        /// <param name="Count">Number of  Identity charactor continuos equal </param>
        /// <returns>The score given</returns>
        public static double GetContinuousScore(int Count)
        {
            return Levenshtein.GetArithmeticScore(Count);
        }

    }

    public class Levenshtein
    {
        private static string[] m_Vowels = { "a", "e", "i", "o", "u", " " };

        #region LD  Levenshtein Edit distance
        //http://www.codeproject.com/Articles/13525/Fast-memory-efficient-Levenshtein-algorithm
        ///*****************************
        /// Compute Levenshtein distance 
        /// Memory efficient version
        ///*****************************
        ///LDEdit Levenshtein Edit distance
        internal static double LDEdit(String sRow, String sCol)
        {
            int RowLen = sRow.Length;  // length of sRow
            int ColLen = sCol.Length;  // length of sCol
            int RowIdx;                // iterates through sRow
            int ColIdx;                // iterates through sCol
            char Row_i;                // ith character of sRow
            char Col_j;                // jth character of sCol
            double cost;                   // cost

            /// Test string length
            if (Math.Max(sRow.Length, sCol.Length) > Math.Pow(2, 31))
                throw (new Exception("\nMaximum string length in Levenshtein.iLD is " + Math.Pow(2, 31) + ".\nYours is " + Math.Max(sRow.Length, sCol.Length) + "."));

            // Step 1

            if (RowLen == 0)
            {
                return ColLen;
            }

            if (ColLen == 0)
            {
                return RowLen;
            }

            /// Create the two vectors
            double[] v0 = new double[RowLen + 1];
            double[] v1 = new double[RowLen + 1];
            double[] vTmp;



            /// Step 2
            /// Initialize the first vector
            for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
            {
                v0[RowIdx] = RowIdx;
            }

            // Step 3

            /// Fore each column
            for (ColIdx = 1; ColIdx <= ColLen; ColIdx++)
            {
                /// Set the 0'th element to the column number
                v1[0] = ColIdx;

                Col_j = sCol[ColIdx - 1];


                // Step 4

                /// Fore each row
                for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
                {
                    Row_i = sRow[RowIdx - 1];


                    // Step 5

                    if (Row_i == Col_j)
                    {
                        cost = 0;
                    }
                    else
                    {
                        if (m_Vowels.Contains(Row_i.ToString()))
                        {
                            //Example Hospital compare with HSTP minus less only take 0.2 of it
                            //o is search from HSTP
                            cost = 0.5;

                        }
                        else
                        {
                            cost = 1;
                        }

                    }

                    // Step 6

                    /// Find minimum
                    double m_min = v0[RowIdx] + 1;
                    double b = v1[RowIdx - 1] + 1;
                    double c = v0[RowIdx - 1] + cost;

                    if (b < m_min)
                    {
                        m_min = b;
                    }
                    if (c < m_min)
                    {
                        m_min = c;
                    }

                    v1[RowIdx] = m_min;
                }

                /// Swap the vectors
                vTmp = v0;
                v0 = v1;
                v1 = vTmp;

            }


            // Step 7

            /// Value between 0 - 100
            /// 0==perfect match 100==totaly different
            /// 
            /// The vectors where swaped one last time at the end of the last loop,
            /// /// that is why the result is now in v0 rather than in v1 
            int max = System.Math.Max(RowLen, ColLen);
            return ((100 * v0[RowLen]) / max);
        }
        #endregion


        ///*****************************
        /// Compute Levenshtein distance         
        ///*****************************
        #region LD Distance
        public static int LD(String sRow, String sCol)
        {
            int RowLen = sRow.Length;  // length of sRow
            int ColLen = sCol.Length;  // length of sCol
            int RowIdx;                // iterates through sRow
            int ColIdx;                // iterates through sCol
            char Row_i;                // ith character of sRow
            char Col_j;                // jth character of sCol
            int cost;                   // cost

            /// Test string length
            if (Math.Max(sRow.Length, sCol.Length) > Math.Pow(2, 31))
                throw (new Exception("\nMaximum string length in Levenshtein.iLD is " + Math.Pow(2, 31) + ".\nYours is " + Math.Max(sRow.Length, sCol.Length) + "."));

            // Step 1

            if (RowLen == 0)
            {
                return 100;
            }

            if (ColLen == 0)
            {

                return 100;
            }

            /// Create the two vectors
            int[] v0 = new int[RowLen + 1];
            int[] v1 = new int[RowLen + 1];
            int[] vTmp;



            /// Step 2
            /// Initialize the first vector
            for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
            {
                v0[RowIdx] = RowIdx;
            }

            // Step 3

            /// Fore each column
            for (ColIdx = 1; ColIdx <= ColLen; ColIdx++)
            {
                /// Set the 0'th element to the column number
                v1[0] = ColIdx;

                Col_j = sCol[ColIdx - 1];


                // Step 4

                /// Fore each row
                for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
                {
                    Row_i = sRow[RowIdx - 1];


                    // Step 5

                    if (Row_i == Col_j)
                    {
                        cost = 0;
                    }
                    else
                    {
                        cost = 1;
                    }

                    // Step 6

                    /// Find minimum
                    int m_min = v0[RowIdx] + 1;
                    int b = v1[RowIdx - 1] + 1;
                    int c = v0[RowIdx - 1] + cost;

                    if (b < m_min)
                    {
                        m_min = b;
                    }
                    if (c < m_min)
                    {
                        m_min = c;
                    }

                    v1[RowIdx] = m_min;
                }

                /// Swap the vectors
                vTmp = v0;
                v0 = v1;
                v1 = vTmp;

            }


            // Step 7

            /// Value between 0 - 100
            /// 0==perfect match 100==totaly different
            /// 
            /// The vectors where swaped one last time at the end of the last loop,
            /// /// that is why the result is now in v0 rather than in v1 
            int max = System.Math.Max(RowLen, ColLen);
            return ((100 * v0[RowLen]) / max);
        }

        #endregion

        #region LD Continuos Similar  Distance
        public static double LDContinuousSimilar(String sRow, String sCol)
        {
            int RowLen = sRow.Length;  // length of sRow
            int ColLen = sCol.Length;  // length of sCol
            int RowIdx;                // iterates through sRow
            int ColIdx;                // iterates through sCol
            char Row_i;                // ith character of sRow
            char Col_j;                // jth character of sCol
            double cost;                   // cost            
            #region Debug Use Only
            //using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"D:\Compare.csv", true))
            //{
            //    sw.Write(", ,");
            //    for (int x = 0; x < sRow.Length; x++)
            //    {
            //        sw.Write(sRow[x] + ",");
            //    }
            //    sw.WriteLine("");
            //}
            #endregion
            /// Test string length
            if (Math.Max(sRow.Length, sCol.Length) > Math.Pow(2, 31))
                throw (new Exception("\nMaximum string length in Levenshtein.iLD is " + Math.Pow(2, 31) + ".\nYours is " + Math.Max(sRow.Length, sCol.Length) + "."));

            // Step 1

            if (RowLen == 0 && ColLen > 0)
            {
                return 0;
            }

            if (ColLen == 0 && RowLen > 0)
            {

                return 0;
            }

            /// Create the two vectors
            double[] v0 = new double[RowLen + 1];
            double[] v1 = new double[RowLen + 1];
            double[] vTmp;
            //ABCD EF
            //ABCD DD
            // 0.1, 0.2, 0.3....


            /// Step 2
            /// Initialize the first vector
            for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
            {
                v0[RowIdx] = RowIdx;
            }

            // Step 3     
            bool[,] _Similar = new bool[RowLen + 1, ColLen + 1];
            double PreMinCost = System.Math.Max(RowLen, ColLen);
            /// Fore each column
            for (ColIdx = 1; ColIdx <= ColLen; ColIdx++)
            {
                /// Set the 0'th element to the column number
                v1[0] = ColIdx;

                Col_j = sCol[ColIdx - 1];

                // Step 4   
                /// Fore each row
                for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
                {
                    Row_i = sRow[RowIdx - 1];


                    // Step 5

                    if (Row_i == Col_j)
                    {
                        cost = 0;
                        _Similar[RowIdx, ColIdx] = true;
                    }
                    else
                    {
                        cost = 1;
                        _Similar[RowIdx, ColIdx] = false;

                    }

                    // Step 6

                    /// Find minimum
                    double m_min = v0[RowIdx] + 1;
                    double b = v1[RowIdx - 1] + 1;
                    double c = v0[RowIdx - 1] + cost;

                    if (b < m_min)
                    {
                        m_min = b;
                    }
                    if (c < m_min)
                    {
                        m_min = c;
                    }
                    v1[RowIdx] = m_min;

                }
                #region Debug Use only

                //using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"D:\Compare.csv", true))
                //{
                //    sw.Write(Col_j + ",");
                //    foreach (double value in v0)
                //    {
                //        sw.Write(value + ",");
                //    }
                //    sw.WriteLine("");
                //}
                //if (ColIdx == ColLen)
                //{
                //    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"D:\Compare.csv", true))
                //    {
                //        sw.Write(Col_j + ","); 
                //        foreach (double value in v1)
                //        {
                //            sw.Write(value + ",");
                //        }
                //        sw.WriteLine("");
                //    }
                //}

                #endregion

                /// Swap the vectors
                vTmp = v0;
                v0 = v1;
                v1 = vTmp;

            }
            int[,] CountRecord = new int[RowLen + 1, ColLen + 1];
            int AverageCount = 0;
            //Look For Similar Max Count
            for (ColIdx = 1; ColIdx <= ColLen; ColIdx++)
            {
                for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
                {
                    if (RowIdx == 1 || ColIdx == 1)
                    {
                        //Row
                        int Colmn = (ColLen - ColIdx);
                        int Row = (RowLen - RowIdx);
                        int MinLen = System.Math.Min(Colmn, Row);
                        int ContinuosCount = 0;
                        for (int x = 0; x <= MinLen; x++)
                        {
                            // _Similar[RowIdx, ColIdx] = false;
                            if (_Similar[RowIdx + x, ColIdx + x] == true)
                            {
                                ContinuosCount++;
                            }
                            else
                            {
                                ContinuosCount = 0;
                            }
                            if (CountRecord[RowIdx, ColIdx] < ContinuosCount)
                            {
                                CountRecord[RowIdx, ColIdx] = ContinuosCount;
                            }

                        }
                    }

                    if (_Similar[RowIdx, ColIdx] == true)
                    {
                        AverageCount++;
                    }
                }
            }

            IEnumerable<int> MaxCount = (from int x in CountRecord where x >= 3 select x);


            //Look For Similar Max Count

            // Step 7

            /// Value between 0 - 100
            /// 0==perfect match 100==totaly different
            /// 
            /// The vectors where swaped one last time at the end of the last loop,
            /// /// that is why the result is now in v0 rather than in v1 
            int max = System.Math.Max(RowLen, ColLen);
            int Min = System.Math.Min(RowLen, ColLen);
            // return (MaxContinuosCOst/ max) ;
            //if (v0[RowLen] - MaxContinuosCOst > 0)
            //{
            //    return ((100 * (v0[RowLen] - MaxContinuosCOst)) / max);
            //}
            //else
            //{
            //    return ((100 * (v0[RowLen])) / max);
            //}
            double LevensteinPosibility = (1.0f - (((v0[RowLen])) / max));
            LevensteinPosibility = (LevensteinPosibility >= 0.65) ? LevensteinPosibility * 5 : LevensteinPosibility;

            double ContinuousPosibility = 0;// (MaxCount.Sum()> 0 ? ((double)MaxCount.Max() / Min) * 0.5 : 0);
            foreach (int count in MaxCount)
            {
                ContinuousPosibility += GetArithmeticScore(count);

            }
            ContinuousPosibility -= Math.Abs(sRow.Length - sCol.Length) * 0.05;
            double SimilarPosibility = (AverageCount / (double)(RowLen * ColLen));
            return LevensteinPosibility + SimilarPosibility + ContinuousPosibility;
        }

        public static double GetArithmeticScore(int ContinuosCount)
        {
            return SumArithmetic(0.1f, 0.01f, ContinuosCount);
        }

        private static double SumArithmetic(double FirstTermValue, double CommonDifferent, int term)
        {
            //http://www.mathsisfun.com/algebra/sequences-sums-arithmetic.html
            //A(n) = a(1) + (n - 1 )d
            //S(n) = n(a(1) + a(n))/2
            double An = FirstTermValue + ((term - 1) * CommonDifferent);
            return term * (FirstTermValue + An) / 2;
        }
        #endregion
        ///*****************************
        /// Compute the min
        ///*****************************

        private static int Minimum(int a, int b, int c)
        {
            int mi = a;

            if (b < mi)
            {
                mi = b;
            }
            if (c < mi)
            {
                mi = c;
            }

            return mi;
        }


    }
}
