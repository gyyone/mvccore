﻿using System.Collections.Generic;

namespace Util
{
    public class FileStructure
    {
        public FileStructure()
        {
            Files = new List<FileInfo>();
        }
        public List<FileInfo> Files { set; get; }
    }
}