﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ClassExtension
{
    public static class ObjectExt
    {
     
        public static Dictionary<string, object> ToDictionary(this object obj, bool returnasId = true)
        {
            Dictionary<string, object> record = new Dictionary<string, object>();
            var fieldproperty = obj;
            PropertyInfo[] propertyInfos = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo p in propertyInfos)
            {
                if (p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                {
                    continue;
                }
                if (returnasId)
                {
                    string columnid = p.GetCustomAttribute<JsonPropertyAttribute>() != null && !string.IsNullOrWhiteSpace(p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName) ? p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName : "";
                    if (string.IsNullOrEmpty(columnid))
                    {
                        record[p.Name] = p.GetValue(obj);
                    }
                    else if(p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    {
                        record[columnid] = JsonConvert.SerializeObject(p.GetValue(obj));
                    }
                    else
                    {
                        record[columnid] = p.GetValue(obj);
                    }

                }
                else
                {
                    try
                    {
                        string name = p.Name;
                        record[name] = p.GetValue(obj);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                }

            }

            return record;
        }

        public static bool IsMultiValue(this object obj)
        {
            if (obj == null)
            {
                return false;
            }
            Type t = obj.GetType();

            return typeof(string[])== t ||  typeof(List<>) == t || t.IsGenericType || t.IsArray || obj is Newtonsoft.Json.Linq.JArray;
        }
        public static bool IsMultiValueType(this Type t)
        {
            return t.IsGenericType || t.IsArray || t == typeof(Newtonsoft.Json.Linq.JArray);
        }
        public static List<string> CastToList(this object obj)
        {
            if (obj == null || (obj != null && string.IsNullOrEmpty(obj.ToString())))
            {
                return new List<string>();
            }
            else if (obj is Newtonsoft.Json.Linq.JArray)
            {
                return JsonConvert.DeserializeObject<List<string>>(obj.ToString());
            }
            else if (obj.GetType().IsGenericType || obj.GetType().IsArray)
            {
                List<string> temp = new List<string>();
                try
                {
                    var objectlist = obj as IList;
                    Type type = objectlist.GetType().GetGenericArguments().FirstOrDefault();

                    if (type == null)
                    {
                        return temp;
                    }
                    foreach (var x in objectlist)
                    {
                        if (x != null)
                        {
                            temp.Add(x.ToString());
                        }
                    }

                }
                catch (Exception ex)
                {

                }
                return temp;
            }
            else
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<string>>(obj.ToString());
                }
                catch (Exception ex)
                {

                }

            }

            return new List<string>() { obj.ToString() };
        }
    }
}
