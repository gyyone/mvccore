﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace ClassExtension
{
    public static class StringExt
    {
        
        public static string ReplaceFormat(this string text, Dictionary<string, string> args)
        {
            string replaced = text;
            foreach (var x in args)
            {
                replaced = replaced.Replace("{" + x.Key + "}", x.Value);
            }
            return replaced;
        }

        public static string ReplaceFormat(this string text, string[] args)
        {
            string replaced = text;
            for (int x = 0; x < args.Length; x++)
            {
                replaced = replaced.Replace("{" + x + "}", args[x]);
            }
            return replaced;
        }
        public static List<Dictionary<string, string>> FindPatterns(this string text, string pattern)
        {
            List<Dictionary<string, string>> matchvalues = new List<Dictionary<string, string>>();

            Regex expression = new Regex(pattern);
            Match match = expression.Match(text);


            while (match.Success)
            {
                Dictionary<string, string> matchvalue = new Dictionary<string, string>();
                foreach (string g in expression.GetGroupNames())
                {
                    matchvalue[g] = match.Groups[g].Value;
                }
                matchvalues.Add(matchvalue);
                match = match.NextMatch();
            }
            return matchvalues;
        }
        public static Dictionary<string, string> FindPattern(this string text, string pattern)
        {
            Regex expression = new Regex(pattern);
            Match match = expression.Match(text);
            Dictionary<string, string> matchvalue = new Dictionary<string, string>();
            if (match.Success)
            {
                foreach (string g in expression.GetGroupNames())
                {
                    matchvalue[g] = match.Groups[g].Value;
                }
                return matchvalue;
            }
            else
            {
                foreach (string g in expression.GetGroupNames())
                {
                    matchvalue[g] = "";
                }
            }
            return matchvalue;
        }

        /// <summary>
        /// Deprecated. Return results finded. Use FindPattern instead
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static List<string> FindByPattern(this string text,string pattern)
        {
            //\p{P} all punctualtion
            //ref https://docs.microsoft.com/en-us/dotnet/standard/base-types/character-classes-in-regular-expressions
            List<string> matchedlist = new List<string>();
            Regex rg = new Regex(pattern);
            
            foreach(Match mc in rg.Matches(text))
            {
                matchedlist.Add(mc.Value);            
            }
            return matchedlist;
        }

        /// <summary>
        /// Deprecated. Use FindPatterns instead
        /// </summary>
        /// <param name="textlist"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static List<string> FindByPattern(this List<string> textlist, string pattern)
        {
            //\p{P} all punctualtion
            //ref https://docs.microsoft.com/en-us/dotnet/standard/base-types/character-classes-in-regular-expressions
            List<string> matchedlist = new List<string>();
            Regex rg = new Regex(pattern);
            foreach(var text in textlist)
            {
                foreach (Match mc in rg.Matches(text))
                {
                    matchedlist.Add(mc.Value);
                }
            }
      
            return matchedlist;
        }
        /// <summary>
        /// Return full path from string with \
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToBSlashFullPath(this string path)
        {
            return Path.GetFullPath(path).ToBSlashClean();
        }
        /// <summary>
        /// Return full path from string with /
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToFSlashFullPath(this string path)
        {
            return Path.GetFullPath(path).ToFSlashClean();
        }
        /// <summary>
        /// Convert / or \\ or // or \/ or /\ to  \ .Network path will be ignore
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToBSlashClean(this string path)
        {
            return path.ToCleanSlash(@"\");
        }
        /// <summary>
        /// Convert \ or \\ or // or \/ or /\ to / . Network path will be ignore
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToFSlashClean(this string path)
        {
            return path.ToCleanSlash("/");
        }
        public static string ToFSlashDirectory(this string path)
        {
            path = Path.GetDirectoryName(path);
            if (string.IsNullOrWhiteSpace(path))
            {
                return path;
            }
            return (path + "/").ToFSlashClean();
        }
        public static string ToCleanSlash(this string path , string replacement)
        {
            int networkIndex = path.IndexOf(@"\\");
            if (networkIndex == 0)
            {
                path = path.Substring(2, path.Length - 2);
            }
            Regex rg = new Regex(@"(/)|(\\)|(\\\\)|(//)|(\\/)|(/\\)");
            path = rg.Replace(path, replacement);
            if (networkIndex == 0)
            {
                path = @"\\" + path;
            }
            return path;
        }
    }
}
