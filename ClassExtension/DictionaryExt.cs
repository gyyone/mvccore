﻿using LogHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ClassExtension
{
    public static class DictionaryExt
    {
        public static Dictionary<string, object> Append(this Dictionary<string, object> dic, string key, string value)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic[key] = value;
            return dic;
        }
        public static T CastToObject<T>(this Dictionary<string, object> row) where T : new()
        {
            if (row == null)
            {
                return default(T);
            }
            T data = new T();
            try
            {

                var fieldproperty = data;
                PropertyInfo[] propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var columnid in row.Keys)
                {
                    try
                    {

                        PropertyInfo p = propertyInfos.Where(x => x.GetCustomAttribute<JsonPropertyAttribute>() != null && x.GetCustomAttribute<JsonPropertyAttribute>().PropertyName == columnid).FirstOrDefault();
              
                        if (columnid == "id")
                        {
                            p = propertyInfos.Where(x => x.Name == "id").FirstOrDefault();
                            p.SetValue(data, row[columnid]);
                        }
                        else if (p == null)
                        {
                            continue;
                        }
                        else if (p != null && p.CanWrite)
                        {
                            if (p.PropertyType == typeof(string) || p.PropertyType == typeof(int) ||
                                p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                                p.PropertyType == typeof(float) || p.PropertyType == typeof(bool))
                            {
                                try
                                {
                                    //Any Control Configure sort by numeric use this query
                                    string text;
                                    object obj = row[columnid];
                                    if (obj.IsMultiValue())
                                    {
                                        text = obj.CastToList().FirstOrDefault();
                                    }
                                    else
                                    {
                                        text = obj.ToString();
                                    }

                                    p.SetValue(data, Convert.ChangeType(text, p.PropertyType));
                                }
                                catch { 
                                    //Ignore for those invalid string format
                                }
                  
                            }       
                            else if (p.PropertyType == typeof(DateTime))
                            {
                                string text = row[columnid].ToString();
                                p.SetValue(data, text.ConvertToDate());
                            }
                            else if (p.PropertyType.IsEnum)
                            {
                                // int record = (int)Enum.Parse(p.PropertyType, row[column.ToString()].ToString());
                                p.SetValue(data, Enum.Parse(p.PropertyType, row[columnid].ToString()));
                            }
                            else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                            {
                                string text;
                                object obj = row[columnid];
                                if (obj.IsMultiValue())
                                {
                                    text = obj.CastToList().FirstOrDefault();
                                }
                                else
                                {
                                    text = obj.ToString();
                                }
                                if (string.IsNullOrEmpty(text))
                                {
                                    text = "{}";
                                }

                                p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                            }
                            else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                string text;
                                object obj = row[columnid];
                                if (obj.IsMultiValue())
                                {
                                    text = obj.ToString();
                                }
                                else
                                {                                    
                                    text = JsonConvert.SerializeObject(obj.CastToList());
                                }
                                if (string.IsNullOrEmpty(text))
                                {
                                    text = "[]";
                                }
                                p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                            }
                            //else if (p.PropertyType == typeof(CodeEditor))
                            //{
                            //    p.SetValue(data, new CodeEditor(row[columnid].ToString()));
                            //}
                            else 
                            {
                                try
                                {
                                    object obj = row[columnid];
                                    var settings = new JsonSerializerSettings();
                                    settings.Converters.Add(new StringEnumConverter { });
                                    settings.TypeNameHandling = TypeNameHandling.Auto;

                                    p.SetValue(data, JsonConvert.DeserializeObject(row[columnid].ToString(), p.PropertyType, settings));
                                }
                                catch(Exception ex)
                                {
                                    LogHandler.Error($"Error Handling column {columnid}:{p.Name}" +ex) ;
                                }
     
                            }
                        
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHandler.Error(ex);

                    }

                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
            }
            return data;
        }
    }
}
