﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ClassExtension
{
    public static class DateExtension
    {
        public const string SystemDateFormat = "yyyy.MM.dd.HHmmssfff";
        public const string SystemDateFormatStart = "yyyy.MM.dd.000000000";
        public const string SystemDateFormatEnd = "yyyy.MM.dd.235959000";
        public const string SystemDateFormatUi = "yyyy.MM.dd.HHmmss000";

        public static Dictionary<string, object> CastToDateOptionDic(string dateoption)
        {
            try
            {
                int lastidexofclose = dateoption.LastIndexOf("}");
                if(dateoption.LastIndexOf(";") > lastidexofclose)
                {
                    dateoption = dateoption.Substring(0, dateoption.LastIndexOf(";"));
                }
                return JsonConvert.DeserializeObject<Dictionary<string, object>>(dateoption);
            }
            catch
            {
                return new Dictionary<string, object>() {{ "format","yyyy-MM-dd" } };
            }

        }
        public static string GetDateFormat(string dateoption)
        {
            return CastToDateOptionDic(dateoption)["format"].ToString();
        }
        public static string ToDateVersion(this DateTime date)
        {
            return date.ToString(SystemDateFormat);
        }
        public static DateTime GetFutureTime(this DateTime date)
        {
            return ConvertToDate(GetFutureTimeVersion(), SystemDateFormat);
        }
        public static DateTime GetPassTime(this DateTime date)
        {
            return ConvertToDate(GetPassTimeVersion(), SystemDateFormat);
        }
        public static string GetPassTimeVersion()
        {
            return "0000.0000.0000.000000000";
        }
        public static string GetFutureTimeVersion()
        {
            return "9999.12.31.235959999";
        }
        public static string GetCurrentTimeVersion()
        {
            return DateTime.Now.ToString(SystemDateFormat);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="formatorigin"></param>
        /// <param name="formatdestination"></param>
        /// <param name="uppertime">use for 23:59:59</param>
        /// <returns></returns>
        public static string ConvertDateFormat(string date, string formatorigin, string formatdestination, bool uppertime = false)
        {
            try
            {
                return DateTime.ParseExact(date, formatorigin, CultureInfo.InvariantCulture).ToString(formatdestination);
            }
            catch (Exception)
            {
                DateTime dt = new DateTime();
                if (DateTime.TryParse(date, out dt))
                {
                    if (uppertime)
                    {
                        return dt.AddHours(23).AddMinutes(59).AddSeconds(59).ToString(formatdestination);
                    }
                    else
                    {
                        return dt.ToString(formatdestination);
                    }

                }

            }

            throw new Exception("Invalid Date format");
        }


        /// <summary>
        /// Standard "yyyy.MM.dd.HHmmssfff" or yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="date"></param>
        /// <param name="formatorigin"></param>
        /// <param name="formatdestination"></param>
        /// <param name="uppertime">use for 23:59:59</param>
        /// <returns></returns>
        public static DateTime ConvertToDate(this string date, string formatorigin = "yyyy.MM.dd.HHmmssfff")
        {
            try
            {
                return DateTime.ParseExact(date, formatorigin, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                DateTime dt = new DateTime();
                if (DateTime.TryParse(date, out dt))
                {
                    return dt;
                }
            }

            return DateTime.Now;
        }

        public static DateTime EndOfThisMonth()
        {
            return DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
        }
        public static DateTime FirstOfThisMonth()
        {
            return DateTime.Now.AddDays(-DateTime.Now.Day);
        }
        public static DateTime ThisMonth(int day)
        {
            return FirstOfThisMonth().AddDays(day);
        }
        //public static DateTime WeekOn(DayOfWeek dayofweek, int week = 1, DayOfWeek firstdayOfWeek = DayOfWeek.Sunday)
        //{
        //    int diffday = (int)DayOfWeek.Sunday - (int)firstdayOfWeek;
        //    DateTime dt = DateTime.Now;
        //    //firstdayofweek = Monday = 1
        //    // diffday  = 1
        //    //DayOfWeek =  Monday = 1 - diffday(1) = 0
        //    //week count =1 
        //    //Today is sunday = 0
        //    //(1 * 7) + 0 % 7 - 3 + 1
        //    int dayweek = ((int)dayofweek - diffday) % 7;
        //    return dt.AddDays( (week * 7) + ((int)dayofweek) - (int)dt.DayOfWeek + diffday);
        //}

    }
}
