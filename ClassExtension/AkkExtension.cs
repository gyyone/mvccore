﻿using Akka.Actor;
using System;
using System.Threading.Tasks;

namespace ClassExtension
{
    public static class AkkExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cantell"></param>
        /// <param name="enumobject"></param>
        /// <param name="timeOut">second</param>
        /// <returns></returns>
        public static T AskEnum<T>(this ICanTell cantell, object enumobject, int timeOut = 300)
        {
            if (!enumobject.GetType().IsEnum)
            {
                throw new Exception("Only accept enum type");
            }
            T temp = default(T);
            Task t = cantell.Ask<T>(new EnumObj() { obj = enumobject, type = enumobject.GetType() }).ContinueWith(r => {
                if (r.Status == TaskStatus.RanToCompletion)
                {
                    temp = r.Result;
                }
            });
            t.Wait(TimeSpan.FromSeconds(timeOut));
            return temp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cantell"></param>
        /// <param name="enumobject"></param>       
        /// <returns></returns>
        public static void TellEnum(this ICanTell cantell, object enumobject, IActorRef sender)
        {
            if (!enumobject.GetType().IsEnum)
            {
                throw new Exception("Only accept enum type");
            }

            cantell.Tell(new EnumObj() { obj = enumobject, type = enumobject.GetType() }, sender);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cantell"></param>
        /// <param name="objsend"></param>
        /// <param name="timeOut">Second</param>
        /// <returns></returns>
        public static T AskSync<T>(this ICanTell cantell, object objsend, int timeOut = 300)
        {
            T temp = default(T);       
            Task t = cantell.Ask<T>(objsend).ContinueWith(r =>
            {
                if (r.Status == TaskStatus.RanToCompletion)
                {
                    temp = r.Result;
                }
            
            });
            t.Wait(TimeSpan.FromSeconds(timeOut));
   
            return temp;
        }

     
        public static T ConfirmAsk<T>(this ICanTell cantell, object objsend, int interval = 10,int maxTry = 300)
        {
            int trysend = 0;
            bool received = false;
            while(trysend < maxTry)
            {
                T temp = default(T);
                Task t = cantell.Ask<T>(objsend).ContinueWith(r =>
                {
                    if (r.Status == TaskStatus.RanToCompletion)
                    {
                        temp = r.Result;
                        received = true;
                    }
                });
                t.Wait(TimeSpan.FromSeconds(interval));
                if(received)
                {
                    return temp;
                }
                trysend++;         

            }
            return default(T);       
           
        }
    }
}