﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ClassExtension
{
    public class CodeEditor
    {
        public CodeEditor()
        {
            option = "";
            Value = "";
        }
        public CodeEditor(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                option = "";
                Value = "";
            }
            else
            {
                string[] settings = text.Split(new[] { '\n' }, StringSplitOptions.None);
                if (settings.Length >= 2)
                {
                    try
                    {
                        option = settings[0];
                        //Try Deserialize, if it is object, then continue
                        JsonConvert.DeserializeObject<Dictionary<string, object>>(option);
                        Value = text.Replace(settings[0] + "\n", "");
                    }
                    catch
                    {
                        option = "";
                        Value = text;
                    }

                }
                else
                {
                    option = "";
                    Value = text;
                }
            }


        }
        public string option { set; get; }
        public string Value { set; get; }

    }
}
