﻿using System;

namespace ClassExtension
{
    public static class EnumExtension
    {
        public static T ToEnum<T>(this string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value);
            }
            catch(Exception ex)
            {
                return default(T);
            }
          
        }
    }
}