﻿using System.Collections.Generic;

namespace ApiClientLib
{

    public class FindItem
    {
        public string ClassId { set; get; }
        public int Start { set; get; }
        public int Rows { set; get; }
        public List<string> Sort { set; get; }
        public string Search { set; get; }
    }
   
}
