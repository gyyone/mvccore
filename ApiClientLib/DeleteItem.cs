﻿using System.Collections.Generic;

namespace ApiClientLib
{
    public class DeleteItem
    {
        public string ClassId { set; get; }
        public List<string> DeleteIds { set; get; }
    }
}
