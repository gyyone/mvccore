﻿namespace ApiClientLib
{
    public class DeleteItemCondition
    {
        public string ClassId { set; get; }
        public string Condition { set; get; }
    }
}
