﻿namespace ApiClientLib
{
    public static class Const
    {
        public const string TableSource = "TableSource";
        public const string KeyValueSource = "KeyValueSource";
        public const string APISource = "APISource";
        public const string Exception = "Exception";
    }
}
