﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Util;

namespace ApiClientLib
{
    public static partial class Data
    {
        public static string ApiHost { set; get; }
        public static ApiAuth Auth { set; get; }
        public static string InitDataBase()
        {
            RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/InitializeDB", null,Auth,600);
            if (!string.IsNullOrEmpty(msr.Error))
            {
                return msr.Error;
            }
            Task<string> result = msr.Result.Content.ReadAsStringAsync();
    
            Dictionary<string, string> temp = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Result);
            return temp["Status"];
        }
        public static class Table
        {
            public static List<Dictionary<string, object>> Find(string classid,Dictionary<string, object> obj, int start = 0, int rows = int.MaxValue, List<string> sort = null)
            {
                if(!obj.ContainsKey("ClassID"))
                {
                    obj["Changed"] = obj.ToDictionary(x => x.Key, y => y.Key);
                }
                string jsonobject = JsonConvert.SerializeObject(obj);
                FindItem finditem = new FindItem();
                finditem.ClassId = classid;
                finditem.Search = jsonobject;
                finditem.Sort = sort ?? new List<string>();
                finditem.Start = start;
                finditem.Rows = rows;

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Select", finditem, Auth);
                if(!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);
                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                List<Dictionary<string, object>> output = new List<Dictionary<string, object>>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if (record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                    output.Add(record);
                }
                return output;

            }
            public static List<Dictionary<string, object>> FindByList(string classid,List<Dictionary<string, object>> objs, int start = 0, int rows = int.MaxValue, List<string> sort = null)
            {
                List<FindItem> items = new List<FindItem>();
                foreach (Dictionary<string, object> obj in objs)
                {
                    FindItem f = new FindItem() { ClassId = classid, Rows = rows, Start = start, Sort = sort ?? new List<string>(), Search = JsonConvert.SerializeObject(obj) };
                    items.Add(f);
                }

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/SelectByList", items, Auth);
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();

                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                List<Dictionary<string, object>> output = new List<Dictionary<string, object>>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if (record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                    output.Add(record);
                }
                return output;
            }
            public static void Update(string classId,Dictionary<string,object> update)
            {
                UpdateItem updateItem = new UpdateItem();
                updateItem.ClassId = classId;
                updateItem.Update = JsonConvert.SerializeObject(update);

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Update", updateItem, Auth);
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
        
                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
                if (temp.ContainsKey(Const.Exception))
                {
                    throw new Exception(temp[Const.Exception].ToString());
                }
           
            }
            public static void Update(string classId, List<Dictionary<string, object>> updatelist)
            {
                List<UpdateItem> items = new List<UpdateItem>();
                foreach(var record in updatelist)
                {
                    UpdateItem updateItem = new UpdateItem();
                    updateItem.ClassId = classId;
                    updateItem.Update = JsonConvert.SerializeObject(record);
                    items.Add(updateItem);
                   
                }
                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/UpdateList", items, Auth);
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();

                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
                if (temp.ContainsKey(Const.Exception))
                {
                    throw new Exception(temp[Const.Exception].ToString());
                }
            }
            public static Dictionary<string, Type> GetFieldType(object obj)
            {
                Dictionary<string, Type> record = new Dictionary<string, Type>();
                var fieldproperty = obj;
                PropertyInfo[] propertyInfos = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo p in propertyInfos)
                {
                    if (p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                    {
                        continue;
                    }
                    record[p.Name] = p.PropertyType;
                }

                return record;
            }

            public static Dictionary<string, object> CastToDic(object obj,bool returnasId)
            {
                Dictionary<string, object> record = new Dictionary<string, object>();
                var fieldproperty = obj;
                PropertyInfo[] propertyInfos = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo p in propertyInfos)
                {
                    if(p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                    {
                        continue;
                    }
                    if (returnasId)
                    {
                        string columnid = p.GetCustomAttribute<JsonPropertyAttribute>() != null && !string.IsNullOrWhiteSpace(p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName) ? p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName : "";
                        if(string.IsNullOrEmpty(columnid))
                        {
                            record[p.Name] = p.GetValue(obj);
                        }
                        else
                        {
                            record[columnid] = p.GetValue(obj);
                        }
                        
                    }
                    else
                    {
                        try
                        {
                            string name = p.Name;
                            record[name] = p.GetValue(obj);
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                   
                    }

                }
     
                return record;
            }
            public static Dictionary<string, string> GetFieldNameToID(object obj)
            {
                Dictionary<string, string> record = new Dictionary<string, string>();
                var fieldproperty = obj;
                PropertyInfo[] propertyInfos = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo p in propertyInfos)
                {
                    if (p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                    {
                        continue;
                    }
                    string columnid = p.GetCustomAttribute<JsonPropertyAttribute>() != null && !string.IsNullOrWhiteSpace(p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName) ? p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName : "";
                    record[p.Name] = columnid;                  
                }

                return record;
            }
    
        }
        public static class Table<T> where T : new()
        {
            public static T Update(T obj)
            {
              
                UpdateItem updateItem = new UpdateItem();
                updateItem.ClassId = ((dynamic)obj).ClassID;
                updateItem.Update = JsonConvert.SerializeObject(obj);         

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Update", updateItem, Auth);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
                if(temp.ContainsKey(Const.Exception))
                {
                    throw new Exception(temp[Const.Exception].ToString());
                }
                return Cast(temp);         
            }

            public static List<T> Update(T obj, T condition)
            {             
                UpdateItemCondition updateItem = new UpdateItemCondition();
                updateItem.ClassId = ((dynamic)obj).ClassID;
                updateItem.Update = JsonConvert.SerializeObject(obj);
                updateItem.Condition = JsonConvert.SerializeObject(condition);
                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/UpdateCondition", updateItem, Auth);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
 
                List<T> output = new List<T>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if (record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                }
                return output;
            }

            public static T Delete(T obj)
            {

                DeleteItem updateItem = new DeleteItem();
                updateItem.ClassId = ((dynamic)obj).ClassID;
                updateItem.DeleteIds = new List<string>();
                updateItem.DeleteIds.Add(((dynamic)obj).id);
                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Delete", updateItem, Auth);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
                if (temp.ContainsKey(Const.Exception))
                {
                    throw new Exception(temp[Const.Exception].ToString());
                }
                return Cast(temp);
            }



            public static List<T> GetAll(int start = 0, int rows = int.MaxValue, List<string> sort = null)
            {

                string jsonobject = JsonConvert.SerializeObject(new T());
                FindItem finditem = new FindItem();
                finditem.ClassId = ((dynamic)new T()).ClassID;
                finditem.Search = jsonobject;
                finditem.Sort = sort ?? new List<string>();
                finditem.Start = start;
                finditem.Rows = rows;

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Select", finditem, Auth);
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                List<T> output = new List<T>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if (record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                    output.Add(Cast(record));

                }
                return output;

            }


            public static List<T> Find(T obj, int start = 0, int rows = int.MaxValue, List<string> sort = null)
            {

                string jsonobject = JsonConvert.SerializeObject(obj);
                FindItem finditem = new FindItem();
                finditem.ClassId = ((dynamic)obj).ClassID;
                finditem.Search = jsonobject;
                finditem.Sort = sort ?? new List<string>();
                finditem.Start = start;
                finditem.Rows = rows;

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/Select", finditem, Auth);
                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                List<T> output = new List<T>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if(record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                    output.Add(Cast(record));

                }
                return output;

            }
              

            public static List<T> FindByList(List<T> objs, int start = 0, int rows = int.MaxValue, List<string> sort = null)
            {
                List<FindItem> items = new List<FindItem>();
                foreach (dynamic obj in objs)
                {
                    FindItem f = new FindItem() { ClassId = obj.ClassID, Rows = rows, Start = start, Sort = sort ?? new List<string>(), Search = JsonConvert.SerializeObject(obj) };
                    items.Add(f);
                }

                // Call API Service here and get result
                RestApiResponse msr = ServerApi.Post(ApiHost + "/Data/SelectByList", items, Auth);
                if (!string.IsNullOrEmpty(msr.Error))
                {
                    throw new Exception(msr.Error);

                }
                Task<string> result = msr.Result.Content.ReadAsStringAsync();

                List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                List<T> output = new List<T>();
                foreach (Dictionary<string, object> record in temp)
                {
                    if (record.ContainsKey(Const.Exception))
                    {
                        throw new Exception(record[Const.Exception].ToString());
                    }
                    output.Add(Cast(record));
                }
                return output;
            }
           
            public static T Cast(Dictionary<string, object> row)
            {
                if(row == null)
                {
                    return default(T);
                }
                T data = new T();
                try
                {

                    var fieldproperty = data;
                    PropertyInfo[] propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var columnid in row.Keys)
                    {                        
                        try
                        {   
                            
                            PropertyInfo p = propertyInfos.Where(x => x.GetCustomAttribute<JsonPropertyAttribute>() != null && x.GetCustomAttribute<JsonPropertyAttribute>().PropertyName == columnid).FirstOrDefault();
                
                            if(columnid == "id")
                            {
                                p = propertyInfos.Where(x => x.Name == "id").FirstOrDefault();
                                p.SetValue(data, row[columnid]);
                            }
                            else if(p == null)
                            {
                                continue;
                            }
                            else if (p != null && p.CanWrite)
                            {
                                if (p.PropertyType == typeof(string) || p.PropertyType == typeof(int) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                                    p.PropertyType == typeof(float) || p.PropertyType == typeof(bool))
                                {
                                    //Any Control Configure sort by numeric use this query
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = TypeCheck.CastToList(obj).FirstOrDefault();
                                    }
                                    else
                                    {
                                        text = obj.ToString();
                                    }

                                    p.SetValue(data, Convert.ChangeType(text, p.PropertyType));
                                }
                                else if(p.PropertyType == typeof(CodeEditor))
                                {
                                    string text = row[columnid].ToString();
                                    if (string.IsNullOrEmpty(text))
                                    {
                                        text = "";
                                    }
                                    p.SetValue(data, new CodeEditor(text));
                                }
                                else if(p.PropertyType == typeof(DateTime))
                                {
                                    string text = row[columnid].ToString();
                                    p.SetValue(data, text.ConvertToDate());
                                }
                                else if (p.PropertyType.IsEnum)
                                {
                                    // int record = (int)Enum.Parse(p.PropertyType, row[column.ToString()].ToString());
                                    p.SetValue(data, Enum.Parse(p.PropertyType, row[columnid].ToString()));
                                }
                                else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                                {
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = TypeCheck.CastToList(obj).FirstOrDefault();
                                    }
                                    else
                                    {
                                        text = obj.ToString();
                                    }
                                    if (string.IsNullOrEmpty(text))
                                    {
                                        text = "{}";
                                    }

                                    p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                                }
                                else if (TypeCheck.IsMultiValueType(p.PropertyType) && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    string text;
                                    object obj = row[columnid];
                                    if (TypeCheck.IsMultiValue(obj))
                                    {
                                        text = obj.ToString();
                                    }
                                    else
                                    {
                                        text = $"[{obj.ToString()}";
                                    }
                                    if (string.IsNullOrEmpty(text))
                                    {
                                        text = "[]";
                                    }
                                    p.SetValue(data, JsonConvert.DeserializeObject(text, p.PropertyType));
                                }                     
                                else if (p.GetType().GetProperties().Any())
                                {
                                    object obj = row[columnid];
                                    var settings = new JsonSerializerSettings();
                                    settings.Converters.Add(new StringEnumConverter { });
                                    settings.TypeNameHandling = TypeNameHandling.Auto;
                                    p.SetValue(data, JsonConvert.DeserializeObject(row[columnid].ToString(), p.PropertyType, settings));
                                }
                                else
                                {
                                    p.SetValue(data, row[columnid]);
                                }
                            }
                        }
                        catch (Exception ex)
                        {


                        }

                    }
                }
                catch (Exception ex)
                {

                }
                return data;
            }
            public static void Commit(List<string> classIdlist)
            {
                 ServerApi.Post(ApiHost + "/Data/Commit", classIdlist, Auth);

            }
            public static Dictionary<string, object> CastObjectToDic(T data)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                PropertyInfo[] plist = data.GetType().GetProperties();
                foreach (var p in plist)
                {
                    if (p.GetCustomAttributes(false).OfType<JsonIgnoreAttribute>().Any())
                    {
                        continue;
                    }
                    string fieldid = p.GetCustomAttribute<JsonPropertyAttribute>() != null ? p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName : p.Name;
                    object objvalue = p.GetValue(data);
                    if (objvalue == null)
                    {
                        continue;
                    }
                    if (string.IsNullOrWhiteSpace(fieldid) || p.Name == "id")
                    {
                        fieldid = p.Name;
                    }
                    if (p.Name == "id")
                    {
                        param.Add(fieldid, objvalue);
                    }
                    else if (p.PropertyType == typeof(DateTime))
                    {
                        //Any Date version Control use this 
                        //_dataStructure.DbConnection.InsertOrUpdate();                        
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else if (p.PropertyType == typeof(string))
                    {
                        param.Add(fieldid, p.GetValue(data).ToString());
                    }
                    else if (p.PropertyType == typeof(int) ||
                             p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                             p.PropertyType == typeof(float))
                    {
                        //Any Control Configure sort by numeric use this query
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else if (p.PropertyType == typeof(bool))
                    {
                        //Unique must in varchar 50, and contain clean string
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
                        || p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    {
                        var settings = new JsonSerializerSettings();
                        settings.Converters.Add(new StringEnumConverter { });
                        settings.TypeNameHandling = TypeNameHandling.Auto;
                        param.Add(fieldid, JsonConvert.SerializeObject(objvalue, settings));

                    }
                    else if (p.PropertyType.IsEnum || TypeCheck.IsReserveType(p.PropertyType))
                    {
                        param.Add(fieldid, objvalue.ToString());
                    }
                    else
                    {
                        var settings = new JsonSerializerSettings();
                        settings.Converters.Add(new StringEnumConverter { });
                        settings.TypeNameHandling = TypeNameHandling.Auto;
                        param.Add(fieldid, JsonConvert.SerializeObject(objvalue, settings));

                    }

                }
                return param;
            }
            public static void Commit()
            {
                dynamic d = (dynamic)(new T());           
                Commit(new List<string>() { d.ClassID });
            }
        }

    }
}
