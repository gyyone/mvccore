﻿using MySql.Data.MySqlClient;
using SimpleDatabase.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;

namespace SimpleDatabase.MariaDB
{
    public class MariaConnection
    {
        public MariaConnection(string connectionstring)
        {
            ConnectionString = connectionstring;
        }
        public MariaConnection(string server, int port, string dbUser, string dbPassword)
        {
            ConnectionString = $"SERVER={server};Port={port};UID={dbUser};PASSWORD={dbPassword}";
        }
        public string ConnectionString { set; get; }
        /// <summary>
        /// 
        /// </summary> 
        /// <param name="query">Example : select field1, field2, field3 from tableA where field= 1;</param>
        /// <param name="Countquery">Example : select Count(1) from tablea where field= 1;</param>
        /// <param name="cancellationToken"></param>
        /// <param name="start"></param>
        /// <param name="rows"></param>
        /// <param name="orQuery"></param>
        /// <returns></returns>
        public QueryResult Select(string query, string Countquery, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue, bool orQuery = false)
        {
            DataSet dataset = new DataSet();
            int maxrowsAccept = Math.Min(int.MaxValue - start, rows);
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                AutoResetEvent waitEvent = new AutoResetEvent(false);
                MySqlCommand cmd = new MySqlCommand(query + Countquery, conn);
                //FOUND_ROWS()
                Thread th = new Thread(c =>
                {
                    MySqlCommand command = (MySqlCommand)c;

                    MySqlDataAdapter adap = new MySqlDataAdapter(command);
                    adap.Fill(dataset);
                    waitEvent.Set();

                });
                th.Start(cmd);
                while (!waitEvent.WaitOne(5000))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        cmd.Cancel();
                        break;
                    }
                }
                conn.Close();
            }
            return dataset.GetResult();
        }
        public QueryResult Select(string tableId, List<SearchTerm> searchTerm, List<string> showList, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue, bool orQuery = false)
        {
            DataSet dataset = new DataSet();
            int maxrowsAccept = Math.Min(int.MaxValue - start, rows);
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                AutoResetEvent waitEvent = new AutoResetEvent(false);
                string condition = searchTerm.ToQueryString();
                string sort = searchTerm.ToSortString();
                string selectquery = $"select {string.Join(",", showList)} FROM {tableId} {condition} {sort} limit {start},{maxrowsAccept}; ";
                string foundrowquery = $" SELECT COUNT(1) as Total FROM {tableId} {condition};";

                MySqlCommand cmd = new MySqlCommand(selectquery + foundrowquery, conn);
                //FOUND_ROWS()
                Thread th = new Thread(c =>
                {
                    MySqlCommand command = (MySqlCommand)c;
                    foreach (var keypair in searchTerm)
                    {
                        command.Parameters.Add("@" + keypair.search);
                    }
                    MySqlDataAdapter adap = new MySqlDataAdapter(command);
                    adap.Fill(dataset);
                    waitEvent.Set();

                });
                th.Start(cmd);
                while (!waitEvent.WaitOne(5000))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        cmd.Cancel();
                        break;
                    }
                }
                conn.Close();
            }
            return dataset.GetResult();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="columnName">Name of the column</param>
        /// <param name="datatype">Example: varchar(50) not null</param>
        /// <param name="defaultvalue"> Example : default 'abcd' </param>
        /// <param name="multiple"></param>
        /// <param name="cancellationToken"></param>
        public  void AddColumn(string tableName, string columnName, string datatype, object defaultvalue, bool multiple, CancellationToken cancellationToken)
        {   //expected default value : default 'abcd'
            //datatype
            if (multiple)
            {
                ExecQuery($"alter table {tableName} add {columnName} text {defaultvalue};", cancellationToken);
            }
            else
            {
                ExecQuery($"alter table {tableName} add {columnName} {datatype} {defaultvalue};", cancellationToken);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="columnName">Name of the column</param>
        /// <param name="datatype">Example: varchar(50) not null</param>
        /// <param name="defaultvalue"> Example : default 'abcd' </param>
        /// <param name="multiple"></param>
        /// <param name="cancellationToken"></param>
        public void AlterColumn(string tableName, string columnName, string datatype, object defaultvalue, bool multiple, CancellationToken cancellationToken)
        {   //expected default value : default 'abcd'
            //datatype
            if (IsColumnExist(tableName, columnName, cancellationToken))
            {
                if (multiple)
                {
                    ExecQuery($"alter table {tableName} alter {columnName} text {defaultvalue};", cancellationToken);
                }
                else
                {
                    ExecQuery($"alter table {tableName} alter {columnName} {datatype} {defaultvalue};", cancellationToken);
                }
            }
            else
            {
                AddColumn(tableName, columnName, datatype, defaultvalue, multiple, cancellationToken);
            }

        }
        public void DropColumn(string tableName, string columnName, CancellationToken cancellationToken)
        {
            ExecQuery($"alter table {tableName} drop {columnName};", cancellationToken);
        }

        public void DropTable(string tableName, CancellationToken cancellationToken)
        {
            ExecQuery($"Drop table if  exists  {tableName};", cancellationToken);
        }
        public void CreateTable(string tableName, CancellationToken cancellationToken)
        {
            ExecQuery($"CREATE TABLE IF NOT EXISTS {tableName} (id varchar(36) NOT NULL DEFAULT uuid(),PRIMARY KEY(id));", cancellationToken);
        }
        /// <summary>
        /// UPDATE, INSERT, or DELETE statements.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns>-1: cancelled or invalid query.</returns>
        public int ExecQuery(string query, Dictionary<string, object> parameters, CancellationToken cancellationToken)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    AutoResetEvent waitEvent = new AutoResetEvent(false);
                    int rowAffected = -1;
                    Thread th = new Thread(obj => {
                        foreach (var keypair in parameters)
                        {
                            command.Parameters.AddWithValue("@" + keypair.Key, keypair.Value);
                        }
                        rowAffected = command.ExecuteNonQuery();
                        waitEvent.Set();
                    });
                    th.Start();
                    while (!waitEvent.WaitOne(5000))
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            command.Cancel();
                        }
                    }
                    return rowAffected;
                }
            }
        }
        /// <summary>
        /// <para>UPDATE, INSERT, or DELETE statements.</para> 
        /// <para>Create, Alter Table</para>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public int ExecQuery(string query, CancellationToken cancellationToken)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    AutoResetEvent waitEvent = new AutoResetEvent(false);
                    int rowAffected = -1;
                    Thread th = new Thread(obj => {
                        rowAffected = command.ExecuteNonQuery();
                        waitEvent.Set();
                    });
                    th.Start();
                    while (!waitEvent.WaitOne(5000))
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            command.Cancel();
                        }
                    }
                    return rowAffected;
                }
            }
        }

        public bool IsColumnExist(string tableId, string fieldname, CancellationToken cancellationToken)
        {

            QueryResult qr = Select($"SHOW COLUMNS FROM {tableId} where field = '{fieldname}';", "", cancellationToken);
            return qr.docs.Count > 0;

        }

        public void Insert(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken)
        {           
            List<string> insertedvalue = new List<string>();
            List<string> insertedfield = new List<string>();
            foreach (var r in data)
            {
                insertedfield.Add(r.Key);
                insertedvalue.Add($"@{r.Key}");
            }  
            string qudatequery = $"insert into {tableId} ({string.Join(",",insertedfield)}) values({string.Join(",",insertedvalue)});";
            ExecQuery(qudatequery, data, cancellationToken);
        }


        public void Update(string tableId,Dictionary<string, object> data, CancellationToken cancellationToken)
        {
            string updatefield = string.Join(",", data.Where(x => !x.Key.Equals("id")).Select(x => $"{x.Key}=@{x.Key}").ToList());
            string qudatequery = $"update {tableId} {updatefield} where id=@id;";
            ExecQuery(qudatequery, data, cancellationToken);
        }
    }
}
