﻿using SimpleDatabase.Database;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SimpleDatabase.MariaDB
{
    public static class MariaDBExtension
    {       

        public static string ToQueryString(this List<SearchTerm> searchTerm, bool OrField = false)
        {
            List<string> queryList = new List<string>();
            foreach (var s in searchTerm)
            {
                s.search = s.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                if (!s.search.Any())
                {
                    continue;
                }
                switch (s.queryType)
                {
                    case QueryType.ExactMatch:
                        {
                            List<string> q = s.search.Select(x => $"{s.FieldId}=@{s.FieldId}").ToList();
                            queryList.Add("(" + string.Join(" OR ", q) + ")");
                        }
     
                        break;
                    case QueryType.RangeQuery:
                        while (s.search.Count <= 2)
                        {
                            s.search.Add("");
                        }
                        if(!string.IsNullOrWhiteSpace(s.search[0]))
                        {
                            queryList.Add($"{s.FieldId}>=@{s.FieldId}");
                        }
                        if (!string.IsNullOrWhiteSpace(s.search[1]))
                        {
                            queryList.Add($"{s.FieldId}<=@{s.FieldId}");
                        }
                        break;
                    case QueryType.TextQuery:
                        {
                            List<string> q = s.search.Select(x => $"{s.FieldId} Like @{s.FieldId}").ToList();
                            queryList.Add("(" + string.Join(" OR ", q) + ")");
                        }     
                        break;
                    case QueryType.CustomMatch:
                        {
                            List<string> q = s.search.Select(x => $"{s.FieldId}=@{s.FieldId}").ToList();
                            queryList.Add("(" + string.Join(" OR ", q) + ")");
                        }
                        break;
                }
            }

            if(queryList.Count > 0)
            {
                return " where " + string.Join(" AND ", queryList);
            }

            if (OrField)
            {
                return string.Join(" OR ", queryList);
            }
            return "";
      
        }
        public static string ToSortString(this List<SearchTerm> searchTerm)
        {
            List<string> queryList = new List<string>();
            foreach (var s in searchTerm)
            {
                if (!string.IsNullOrEmpty(s.Sort))
                {
                    queryList.Add(string.Format("{0} {1}", s.FieldId, s.Sort));
                }

            }
            if (queryList.Any())
            {
                return "order by" + string.Join(",", queryList);
            }
            return "";
        }
        public static QueryResult GetResult(this DataSet ds)
        {
            QueryResult q = new QueryResult();   
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Dictionary<string, object> item = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    item[col.ColumnName] = dr[col.ColumnName];
                }
                q.docs.Add(item);
            }
            if(ds.Tables.Count == 1)
            {
                q.numFound = q.docs.Count;
            }
            else
            {
                q.numFound = int.Parse(ds.Tables[1].Rows[0]["total"].ToString());
            }
          
            return q;
        }
    }
}
