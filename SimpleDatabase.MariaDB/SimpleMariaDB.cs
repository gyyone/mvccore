﻿
using ClassExtension;
using SimpleDatabase.Database;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;

namespace SimpleDatabase.MariaDB
{
    public class SimpleMariaDB :IDatabase
    {
        MariaConnection Connection { set; get; }
        /// <summary>
        /// SetConnection
        /// </summary>
        /// <param name="args">only accept arg[0]: Full connectionstring</param>    
        /// <remarks>
        /// Examples
        /// <para>connectionStr :  Server=serverAddress1, serverAddress2, serverAddress3;Database=myDataBase;Port=3306;Uid=myUsername;Pwd=myPassword;</para>
        /// <para><a href="https://www.connectionstrings.com/mysql/" >Refer to Connection String</a> </para>
        /// </remarks>
        public SimpleMariaDB(string connectionStr)
        {
            Connection = new MariaConnection(connectionStr);
        }
        /// <summary>
        /// SimpleMariaDB
        /// </summary>
        /// <param name="server"></param>
        /// <param name="port"></param>
        /// <param name="dbUser"></param>
        /// <param name="dbPassword"></param>
        public SimpleMariaDB(string server, int port, string dbUser, string dbPassword)
        {
            Connection = new MariaConnection(server, port, dbUser, dbPassword);
        }

        public void AddOrAlterField(string tableId, string fieldname, string datatype, object defaultvalue, bool multivalue, CancellationToken cancellationToken)
        {
            Connection.AlterColumn(tableId, fieldname, datatype,defaultvalue,multivalue ,cancellationToken);
        }

        public void AddTable(string tableId, CancellationToken cancellationToken)
        {
            Connection.CreateTable(tableId, cancellationToken);
        }

        public void DeleteField(string tableId, string fieldid, CancellationToken cancellationToken)
        {
            Connection.DropColumn(tableId, fieldid, cancellationToken);
        }

        public void DeleteTable(string tableId, CancellationToken cancellationToken)
        {
            Connection.DropTable(tableId, cancellationToken);
        }

        public void Insert(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken)
        {
            Connection.Insert(tableId, data, cancellationToken);
        }

        public void Insert(string tableId, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken)
        {
            foreach(var row in datalist)
            {
                Connection.Insert(tableId, row, cancellationToken);
            }      
        }

        public QueryResult Query(string tableId, List<SearchTerm> searchTerm, List<string> showList, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue, bool orQuery = false)
        {
            return Connection.Select(tableId, searchTerm, showList, cancellationToken, start, rows, orQuery);
        }

        public void Update(string tableId, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken)
        {
            foreach (var row in datalist)
            {
                Connection.Update(tableId, row, cancellationToken);
            }
        }

        public void Update(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken)
        {
            Connection.Update(tableId, data, cancellationToken);
        }
    }
}
