﻿using log4net;
using log4net.Core;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace LogHelper
{
    public static class LogHandler
    {
        private static log4net.ILog Log { set; get; }
        private static Action<object> CallBack { set; get; }
        public static void InitLog(Action<object> callback)
        {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(),
                       typeof(log4net.Repository.Hierarchy.Hierarchy));        
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
            Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CallBack = callback;
        }

        public static void Debug(object obj)
        {          
            Log.Debug(JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented));
            CallBackIfExist(obj);
        }
        public static void Error(object obj)
        {
         
            Log.Error(obj);
            CallBackIfExist(obj);
        }
        public static void Fatal(object obj)
        {
            Log.Fatal(obj);
            CallBackIfExist(obj);
        }
        private static void CallBackIfExist(object obj)
        {
            CallBack?.Invoke(obj);
        }

  
    }


}
