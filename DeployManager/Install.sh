﻿servicename=deploymanager.service
installdir="/opt/DeployManager"
#mkdir -p -- "$installdir"

cp "$installdir/$servicename" "/etc/init.d/$servicename"
chmod 0744 "/etc/init.d/$servicename"
chown root: "/etc/init.d/$servicename"
systemctl enable $servicename
systemctl start $servicename
firewall-cmd --zone=public --add-port=9989/tcp --permanent
firewall-cmd --reload
firewall-cmd --zone=dmz --add-port=9989/tcp --permanent