﻿using System.Reflection;
using System.IO;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis.Host.Mef;
using Microsoft.CodeAnalysis.Text;
using Microsoft.CodeAnalysis.Completion;
using System.Threading.Tasks;
using System.Threading;
using Util;
using LogHelper;

namespace DeployManager
{
    public class EvalItem
    {
        public EvalItem(SimpleUnloadableAssemblyLoadContext context, Assembly assembly,string code)
        {
            Context = context;
            Assembly = assembly;
            Code = code;
        }
        public SimpleUnloadableAssemblyLoadContext Context { set; get; }
        public Assembly Assembly { set; get; }
        public string Code { set; get; }
    }
    public static class Compiler<T>
    {
        private static object evallockobj = new object();
        private static Dictionary<string, EvalItem> assemblydic = new Dictionary<string, EvalItem>();
           
        
        public static T Eval(string code)
        {
            lock (evallockobj)
            {
                try
                {
                    LogHandler.Debug($"Eval {code}");
                    return Eval(code, new string[] { });
                }
                catch(Exception ex)
                {
                    LogHandler.Error(ex);
                    return default(T);
                }
       
            }
        }
        
        public static T EvalWithCache(string code,string key)
        {
            lock(evallockobj)
            {
                LogHandler.Debug($"Eval {code} Key :{key}");
                try
                {
                    if (assemblydic.ContainsKey(key))
                    {
                        if (assemblydic[key].Code.GetHashCode() == code.GetHashCode())
                        {
                            return (T)assemblydic[key].Assembly.GetType(key).GetMethod("Eval").Invoke(null, null);
                        }
                        assemblydic[key].Context.Unload();
                    }
                    return Eval(code, key, new string[] { });
                }
                catch(Exception ex)
                {
                    LogHandler.Error(ex);
                }
                return default(T);
            }          
        }
        private static T Eval(string code,string key, string[] includeNamespaces = null)
        {
            StringBuilder namespaces = null;
            if (includeNamespaces != null && includeNamespaces.Any())
            {
                foreach (string _namespace in includeNamespaces)
                {
                    namespaces = new StringBuilder();
                    namespaces.Append(string.Format("using {0};\n", _namespace));
                }
            }
            Type outType = typeof(T);
            var compilation = CSharpCompilation.Create(key).WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
     .AddReferences(
         MetadataReference.CreateFromFile(typeof(Enumerable).GetTypeInfo().Assembly.Location),
         MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location))
     .AddSyntaxTrees(CSharpSyntaxTree.ParseText(
       string.Format(
                    @"{1}  
                using System.Linq;
                using System;  
                    public static class "+ key + @"{{  
                        public static {2} Eval(){{  
                            {3} {0};  
                        }}  
                    }}  
                ",
                    code,
                    namespaces != null ? namespaces.ToString() : "",
                    outType != null ? outType.FullName : "void",
                    outType != null ? "return" : string.Empty)));


            //var fileName = "a.dll";

            //compilation.Emit(fileName);
            using (var memoryStream = new MemoryStream())
            {
                var emitResult = compilation.Emit(memoryStream);
                if (emitResult.Success)
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    SimpleUnloadableAssemblyLoadContext context = new SimpleUnloadableAssemblyLoadContext();

                    var assembly = context.LoadFromStream(memoryStream);

                    //assembly.GetType("ClassName").GetMethod("MethodName").Invoke(null, null);
                    object result = assembly.GetType(key).GetMethod("Eval").Invoke(null, null);
                    assemblydic[key] = new EvalItem(context, assembly,code);
                    return (T)result;

                }
            }
            return default(T);



        }
        private static T Eval(string code, string[] includeNamespaces = null)
        {
            StringBuilder namespaces = null;
            if (includeNamespaces != null && includeNamespaces.Any())
            {
                foreach (string _namespace in includeNamespaces)
                {
                    namespaces = new StringBuilder();
                    namespaces.Append(string.Format("using {0};\n", _namespace));
                }
            }
            Type outType = typeof(T);
      
            var compilation = CSharpCompilation.Create("CSharpCode").WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
     .AddReferences(
                MetadataReference.CreateFromFile(typeof(Enumerable).GetTypeInfo().Assembly.Location),
         MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location))
     .AddSyntaxTrees(CSharpSyntaxTree.ParseText(
       string.Format(
                    @"{1}  
                using System;         
                using System.Linq;
                    public static class CSharpCode{{  
                        public static {2} Eval(){{  
                            {3} {0};  
                        }}  
                    }}  
                ",
                    code,
                    namespaces != null ? namespaces.ToString() : "",
                    outType != null ? outType.FullName : "void",
                    outType != null ? "return" : string.Empty)));  


            //var fileName = "a.dll";

            //compilation.Emit(fileName);
            using (var memoryStream = new MemoryStream())
            {
                var emitResult = compilation.Emit(memoryStream);
                if (emitResult.Success)
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    SimpleUnloadableAssemblyLoadContext context = new SimpleUnloadableAssemblyLoadContext();

                    var assembly = context.LoadFromStream(memoryStream);
                    try
                    {
                        LogHandler.Debug("CSharpCode.Eval");
                        object result = assembly.GetType("CSharpCode").GetMethod("Eval").Invoke(null, null);
                        return (T)result;
                    }
                    catch(Exception ex)
                    {

                    }
                    finally
                    {
                        LogHandler.Debug("Unload");
                        context.Unload();
                    }                       

                }
            }
            return default(T);



        }

        public static List<CompletionItem> GetAutoComplete(string scriptCode,List<string> usingsname , List<PortableExecutableReference> refexecutable,string language= LanguageNames.CSharp)
        {
            List<CompletionItem> completeitem = new List<CompletionItem>();
            try
            {
                var host = MefHostServices.Create(MefHostServices.DefaultAssemblies);

                var workspace = new AdhocWorkspace(host);
                var compilationOptions = new CSharpCompilationOptions(
                   OutputKind.DynamicallyLinkedLibrary,
                   usings: usingsname.ToArray());
                var scriptProjectInfo = ProjectInfo.Create(ProjectId.CreateNewId(), VersionStamp.Create(), "Script", "Script", language, isSubmission: true)
                   .WithMetadataReferences(refexecutable.ToArray())
                   .WithCompilationOptions(compilationOptions);

                var scriptProject = workspace.AddProject(scriptProjectInfo);
                var scriptDocumentInfo = DocumentInfo.Create(
                    DocumentId.CreateNewId(scriptProject.Id), "Script",
                    sourceCodeKind: SourceCodeKind.Script,
                    loader: TextLoader.From(TextAndVersion.Create(SourceText.From(scriptCode), VersionStamp.Create())));
                var scriptDocument = workspace.AddDocument(scriptDocumentInfo);

                // cursor position is at the end
                var position = scriptCode.Length;

                var completionService = CompletionService.GetService(scriptDocument);
                var tokenSource2 = new CancellationTokenSource();
                CancellationToken ct = tokenSource2.Token;
                Task<CompletionList> tasklist = completionService.GetCompletionsAsync(scriptDocument, position, cancellationToken: ct);
                tasklist.Wait();

               
                try
                {
                    CompletionList clist = tasklist.Result;
                    if (clist != null)
                    {
                        foreach (var i in clist.Items)
                        {
                            completeitem.Add(i);
                            //LogHandler.Debug($"{i.DisplayText}");
                        }
                    }
                }
                catch
                {

                }
            }
            catch(Exception ex)
            {

            }


            //tokenSource2.Cancel();
            return completeitem;

        }
    }
}