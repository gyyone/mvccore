﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using PeterKottas.DotNetCore.WindowsService;
using Core;
using Core.Akka;
using Newtonsoft;
using Security;
using Util;
using Microsoft.CodeAnalysis.Completion;
using LogHelper;
using JsonHelper;
using Newtonsoft.Json;

namespace DeployManager
{
    internal class Program
    {
        private static void Main(string[] args)
        {
         

            RuntimeCompliler.EvalCallback = Compiler<object>.Eval;
            RuntimeCompliler.EvalCallbackCache = Compiler<object>.EvalWithCache;
            RuntimeCompliler.GetCompleterItem = Compiler<List<CompletionItem>>.GetAutoComplete;
       
            LogHandler.InitLog(null);
            JsonIO.SetDefaultSerializer();
            Console.Title = ActorTypes.DeployManager.ToString();
            //byte[] unicode = System.Text.Encoding.Unicode.GetBytes("A");
            //byte[] unicode2 = System.Text.Encoding.Unicode.GetBytes("哈");
            //byte[] ascii = System.Text.Encoding.ASCII.GetBytes("A");
            //byte[] ascii2 = System.Text.Encoding.ASCII.GetBytes("哈");
            //byte[] iv = Encryption.GenerateIV("123456789012");
            //byte[] key = Encryption.GenerateKey("M@ster5am", 16);
            //byte[] encrypt = Encryption.Encrypt("Hello Pleas procced to counter. @Computer is Fun %?!!.",key,iv);
            //string decrypt = Encryption.Decrypt(encrypt, key, iv);
            //Compiler<object>.Test("DateTime.");
            //Compiler<object>.Test("DateTime.Now");
            //StandardFormat.GetCleanName()
           //string text = RuntimeCompliler<string>.InvokeMethod("Util.StandardFormat", "GetCleanName", new object[] { "Hello Word" });
            
            ServiceRunner<CoreServices>.Run(config =>
            {
                var name = config.GetDefaultName();
                config.Service(serviceConfig =>
                {
                    serviceConfig.ServiceFactory((service,extraArguments) =>
                    {
                        return new CoreServices();
                    });
                    serviceConfig.OnStart((service, extraArguments) =>
                    {
                        Console.WriteLine("Service {0} started", name);
                        service.Start();
                    });

                    serviceConfig.OnStop(service =>
                    {
                        Console.WriteLine("Service {0} stopped", name);
                        service.Stop();
                    });

                    serviceConfig.OnInstall(service =>
                    {
                        Console.WriteLine("Service {0} installed", name);
                    });

                    serviceConfig.OnUnInstall(service =>
                    {
                        Console.WriteLine("Service {0} uninstalled", name);
                    });

                    serviceConfig.OnPause(service =>
                    {
                        Console.WriteLine("Service {0} paused", name);
                    });

                    serviceConfig.OnContinue(service =>
                    {
                        Console.WriteLine("Service {0} continued", name);
                    });

                    serviceConfig.OnShutdown(service =>
                    {
                        Console.WriteLine("Service {0} shutdown", name);
                    });

                    serviceConfig.OnError(e =>
                    {
                        Console.WriteLine("Service {0} errored with exception : {1}", name, e.Message);
                    });
                });
            });
        }

        public static async Task<IActorRef> CreateActorIfNotExistsAsync(ActorSystem actorSystem,
            ActorMetadata actorPath, Props creationProps)
        {
            IActorRef actor = null;
            try
            {
                actor = await actorSystem.ActorSelection(actorPath.Path)
                    .ResolveOne(TimeSpan.FromSeconds(1));
            }
            catch (ActorNotFoundException)
            {
                actor = actorSystem.ActorOf(creationProps, actorPath.Name);
            }
            return actor;
        }

    }

    public class ActorMetadata
    {
        public string Name { get; private set; }
        public string Path { get; private set; }

        public ActorMetadata Parent { get; private set; }
        public ActorMetadata(string name, ActorMetadata parent = null)
        {
            Name = name;
            var parentPath = parent != null ? parent.Path : "/user";
            Path = $"{parentPath}/{Name}";
        }
    }
}