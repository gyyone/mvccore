using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Actor;
using Core.Actor.ComObject;
using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Routing;
using PeterKottas.DotNetCore.WindowsService.Base;
using PeterKottas.DotNetCore.WindowsService.Interfaces;
using Core;
using System;
using System.Text;
using Core.Actor.Security;
using Util;
using LogHelper;
using JsonHelper;

namespace DeployManager
{
    internal class CoreServices : MicroService, IMicroService
    {

        private ActorSystem MainSystem { set; get; }
        private ActorSystem ClusterNode { set; get; }
        private Dictionary<IActorRef,bool> ProcessTerminated = new Dictionary<IActorRef, bool>();
        private string configPath => AppDomain.CurrentDomain.BaseDirectory + "config.json";
        public CoreServices()
        {
           
            DataHandlerMasterConfig dhConfig = JsonIO.Load<DataHandlerMasterConfig>(configPath);
           
            JsonIO.Save(configPath,dhConfig);
    
            dhConfig.DataHandlerMasterAkkaParam["seed-nodes"] = AkkaHandler.GetClusterSeed(R.SystemName.MainSystem, int.Parse(dhConfig.DataHandlerMasterAkkaParam["port"]), dhConfig.DataHandlerMasterAkkaParam["enable-ssl"].ToLower() == "true", dhConfig.ClusterHostList); ;
            LogHandler.Debug(dhConfig.DataHandlerMasterAkkaParam["seed-nodes"]);
            MainSystem = ActorSystem.Create(R.SystemName.MainSystem, AkkaHandler.GetConfig(dhConfig.DataHandlerMasterAkkaParam));
            var mediator = DistributedPubSub.Get(MainSystem).Mediator;          

     
            var props = Props.Create(() => new DataHandlerMaster(mediator, dhConfig));
            IActorRef datahandlerMaster = MainSystem.ActorOf(props, R.ActorName.DataHandlerMasterActor);

            KeyWordsDic.LanguageActor = datahandlerMaster;

            dhConfig.DataHandlerAkkaParam["seed-nodes"] = AkkaHandler.GetClusterSeed(R.SystemName.MainSystem, int.Parse(dhConfig.DataHandlerAkkaParam["remoteport"]), dhConfig.DataHandlerAkkaParam["enable-ssl"].ToLower() == "true", dhConfig.ClusterHostList);
            ActorSystem clusterNode = ActorSystem.Create(R.SystemName.MainSystem, AkkaHandler.GetConfig(dhConfig.DataHandlerAkkaParam));
            LogHandler.Debug("Creating Import Workder");
            IActorRef dbhandlerActor = clusterNode.ActorOf(Props.Create(() => new DataHandler(dhConfig, datahandlerMaster)), R.ActorName.DataHandlerActor);


            //ProcessTerminated.Add(centerlock,false);
            //ProcessTerminated.Add(datahandler, false);
            //ProcessTerminated.Add(importWorker, false);
            JsonIO.Save(configPath, dhConfig);
            LogHandler.Debug("Actor Started");


        }

        
        public void Start()
        {
       
        }

        public void Stop()
        {
            ManualResetEvent resetEvent = new ManualResetEvent(false);
            int totalTerminateCount = ProcessTerminated.Count;
            List<Task> tasks = new List<Task>();
            foreach (var actor in ProcessTerminated.Keys.ToList())
            {
                if (!ProcessTerminated[actor])
                {
                    Task task = actor.Ask<TerminateConfirm>(new TerminateRequest()).ContinueWith(t =>
                    {
                        ProcessTerminated[t.Result.Actor] = true;
                      
                    });
                    tasks.Add(task);
                }
            }
            Task.WaitAll(tasks.ToArray());
            MainSystem.Terminate();
     
        }

    }
}