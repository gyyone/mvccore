using LogHelper;
using Newtonsoft.Json;
using System;
using Util;

namespace Core
{
    public class Performace
    {
        DateTime OldTime  { set; get; }
        DateTime NewTime { set; get; }

        public void Start()
        {
            OldTime = DateTime.Now;
        }

        public void End()
        {
            NewTime = DateTime.Now;
            TimeSpan span = NewTime.Subtract(OldTime);
            LogHandler.Debug(string.Format("{0} days, {1} hours, {2} minutes, {3} seconds",
                span.Days, span.Hours, span.Minutes, span.Seconds));
        }

    }
  
}