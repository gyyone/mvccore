﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using Newtonsoft.Json;

//namespace Util
//{

//    public enum ConfigKey
//    {
//        FilesUploadUrl=-9999,
//        FilesDownloadUrl,
//        FileDownloadPath,
//        FileUploadPath,
//        ConfigFile,
//        DeployManagerSelector = -1,//Not configuratble in the global config file, only able to configure view App Config
//        SolrPath,     
//        DeployManagerHost,
//        DeployManagerPort,
//        ClusterPort,
//        FilesUrl,
//        FilePath,
//        MaxIndexPerTransaction,
//        EnableOptimizations,
//        WebAPi,
//        CertificateValidation,
//        SolrAPI
//    }
//    public class GlobalConfig
//    {
//        public Dictionary<ConfigKey, string> ConfigDic = new Dictionary<ConfigKey, string>();
//        //Change on config file at data manager
  
//        public string LogicalDrive = AppDomain.CurrentDomain.BaseDirectory;
//        Mutex mutex { set; get; }

//        public GlobalConfig()
//        {
//            mutex = new Mutex(false, "GlobalConfig");
//        }
//        public string Get(ConfigKey key)
//        {        
       
//            if (!ConfigDic.ContainsKey(key))
//            {
//                AddDefault(key);
//            }
        
//            return ConfigDic[key];
//        }

//        private void Create()
//        {
//            mutex.WaitOne();
//            try
//            {
//                string datapath = Get(ConfigKey.ConfigFile);
//                foreach (ConfigKey value in Enum.GetValues(typeof (ConfigKey)))
//                {
//                    Get(value);
//                }
//                using (StreamWriter sw = new StreamWriter(datapath, false))
//                {
//                    Dictionary<ConfigKey, string> configuration =
//                        ConfigDic.Where(x => x.Key >= 0).ToDictionary(x => x.Key, y => y.Value);
//                    sw.Write(JsonConvert.SerializeObject(configuration));
//                }
//            }
//            catch (Exception)
//            {


//            }
//            finally
//            {
//                mutex.ReleaseMutex();
//            }

        
//        }

//        public void Load()
//        {
//            mutex.WaitOne();
//            try
//            {
//                string datapath = Get(ConfigKey.ConfigFile);
//                if (!File.Exists(datapath))
//                {
//                    // If the file can be opened for exclusive access it means that the file
//                    // is no longer locked by another process.
//                    DirectoryHelper.CreateDirectory(datapath);
//                    Create();
//                }
//                ConfigDic = JsonConvert.DeserializeObject<Dictionary<ConfigKey, string>>(File.ReadAllText(datapath));
//            }
//            finally
//            {
//                mutex.ReleaseMutex();
//            }
    
         
//        }

       
//        private void AddDefault(ConfigKey key)
//        {
//            //system.actorSelection("/user/*") ! msg

//            switch (key)
//            {
//                case ConfigKey.SolrPath:
//                    ConfigDic.Add(key, LogicalDrive + "Solr/" );
//                    break;
//                case ConfigKey.FilePath:
//                    ConfigDic.Add(key, LogicalDrive + "Files/");
//                    break;
//                case ConfigKey.FileUploadPath:
//                    ConfigDic.Add(key, Get(ConfigKey.FilePath)+  "Upload/");
//                    break;
//                case ConfigKey.FileDownloadPath:
//                    ConfigDic.Add(key, Get(ConfigKey.FilePath) + "Download/");
//                    break;
//                case ConfigKey.DeployManagerSelector:
//                    ConfigDic.Add(key, AkkaConfig.GetSelector(R.SystemName.MainSystem, Get(ConfigKey.DeployManagerHost), int.Parse(Get(ConfigKey.DeployManagerPort)), R.ActorName.DataHandlerMasterActor,true));
//                    break;
//                case ConfigKey.ConfigFile:
//                    ConfigDic.Add(key, LogicalDrive + "config.json");
//                    break;
//                case ConfigKey.DeployManagerHost:
//                    ConfigDic.Add(key, "localhost");
//                    break;
//                case ConfigKey.DeployManagerPort:
//                    ConfigDic.Add(key, "7000");
//                    break;
//                case ConfigKey.ClusterPort:
//                    ConfigDic.Add(key, "7001");
//                    break;
//                case ConfigKey.FilesUrl:
//                    ConfigDic.Add(key, "https://localhost/Files/");
//                    break;
//                case ConfigKey.FilesUploadUrl:
//                    ConfigDic.Add(key, Get(ConfigKey.FilesUrl) +"Upload/");
//                    break;
//                case ConfigKey.FilesDownloadUrl:
//                    ConfigDic.Add(key, Get(ConfigKey.FilesUrl)+ "Download/");
//                    break;
//                case ConfigKey.MaxIndexPerTransaction:
//                    ConfigDic.Add(key, "500");
//                    break;
//                case ConfigKey.EnableOptimizations:
//                    ConfigDic.Add(key, "false");
//                    break;
//                case ConfigKey.WebAPi:
//                    ConfigDic.Add(key, "https://localhost:44327");
//                    break;
//                case ConfigKey.CertificateValidation:
//                    ConfigDic.Add(key, "false");
//                    break;
//                case ConfigKey.SolrAPI:
//                    ConfigDic.Add(key, "http://localhost:8983");
//                    break;
//                default:
//                    ConfigDic.Add(key, "");
//                    break;
//            }
//        }
//    }
//}
