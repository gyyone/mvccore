﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;

using System.IO;
using System.Linq;
using System.Text;

namespace Core
{
    public static class ExcelIO
    {
        public static List<string> SheetList(string filepath)
        {
            List<string> sheetlist = new List<string>();
            var extention = Path.GetExtension(filepath);
            switch (extention)
            { 
                case ".xlsx":              
                    using (var fs = File.OpenRead(filepath))
                    {
                        var wb = new XSSFWorkbook(fs);
                        for(int x =0; x < wb.NumberOfSheets; x++)
                        {
                            sheetlist.Add(wb.GetSheetName(x));
                        }                  
          
                    }
                    break;
                case ".xls":
                    using (var fs = File.OpenRead(filepath))
                    {
                        var wb = new XSSFWorkbook(fs);
                        for (int x = 0; x < wb.NumberOfSheets; x++)
                        {
                            sheetlist.Add(wb.GetSheetName(x));
                        }

                    }
                    break;
            }
            return sheetlist;
        }
        public static List<string> ImportHeader(string filepath)
        {
            List<string> header = new List<string>();
            var extention = Path.GetExtension(filepath);
            switch (extention)
            { 
                case ".xlsx":              
                    foreach(var row in ImportXlsx(filepath,true))
                    {
                        for(int x = 0; x < row.Table.Columns.Count; x++)
                        {
                            header.Add(row[x].ToString());
                        }
                    }
                    break;
                case ".xls":
                    foreach (var row in ImportXls(filepath, true))
                    {
                        for (int x = 0; x < row.Table.Columns.Count; x++)
                        {
                            header.Add(row[x].ToString());
                        }
                    }
                    break;
            }
            return header;
        }
        public static IEnumerable<DataRow> ImportXlsx(string filePath,bool headerOnly = false)
        {
            DataTable dt = new DataTable();
            using (var fs = File.OpenRead(filePath))
            {
                var wb = new HSSFWorkbook(fs);
                var sheet = wb.GetSheetAt(0);          
                for (int row =0; row <= sheet.LastRowNum; row++)
                {                 
                    DataRow dataRow = dt.NewRow();
                    var record = sheet.GetRow(row);
                    dataRow.ItemArray = record.Cells.Select(c => c.ToString()).ToArray();
                    yield return dataRow;                  
                    if(headerOnly)
                    {
                        break;
                    }
                }             
            }

        }
        public static IEnumerable<DataRow> ImportXls(string filePath, bool headerOnly = false)
        {
            DataTable dt = new DataTable();
            using (var fs = File.OpenRead(filePath))
            {
                var wb = new HSSFWorkbook(fs);
                var sheet = wb.GetSheetAt(0);
                for (int row = 0; row <= sheet.LastRowNum; row++)
                {
                    DataRow dataRow = dt.NewRow();
                    var record = sheet.GetRow(row);
                    dataRow.ItemArray = record.Cells.Select(c => c.ToString()).ToArray();
                    yield return dataRow;
                    if (headerOnly)
                    {
                        break;
                    }
                }
            }

        }



    }
}