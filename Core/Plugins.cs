using System;
using System.Collections.Generic;
using FieldProperty = TableProperty.FieldProperty;

namespace Core
{
    public class Plugins
    {

        public string TableName { set; get; }
        public string TableText { set; get; }
        public List<TableProperty> TableList { set; get; }
        public List<FieldProperty> FieldList { set; get; }
    }
}