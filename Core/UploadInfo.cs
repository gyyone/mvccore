namespace Core
{
    public class UploadInfo
    {
        public string FileName { set; get; }
        public string FileType { set; get; }
        public string FileSize { set; get; }
        public string Data { set; get; }

    }
}