﻿using ClassLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using SolrNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Util;
using FieldProperty = TableProperty.FieldProperty;
namespace Core
{
    public enum Aggregate
    {
        sum,
        avg,
        min,
        max,
    }
    public enum SortType
    {
        String,
        Numeric,

    }


    public enum TableTypes
    {
        OfflineTable,
        PaginationTable,
        LoadOnScrollTable
    }

    public enum BaseTypes
    {
        None,
        Table,
        Field,
        HtmlField
    }
    public enum TableAccess
    {
        View,
        Modify
  
    }
    public enum FieldAccess
    {
        View,
        Modify,
        Delete,        
        
    }
    public enum MenuAccess
    {
        View,     
        None
    }
    public class Structure
    {
        public List<TableProperty> Table { set; get; }
        public List<FieldProperty> Fields { set; get; }
    }

    public class ControlSize
    {
        public ControlSize()
        {
            ControlLabelClass = "col-4";
            ControlValueClass = "col-4";
            ControlClass = "col-6";
        }
        public string ControlLabelClass { set; get; }
        public string ControlClass { set; get; }
        public string ControlValueClass { set; get; }
    }

}