using System;
using System.Text.RegularExpressions;
using Util;

namespace Core
{
    public static class Validate
    {
        public static bool IsGuid(string guid)
        { 
            if (string.IsNullOrWhiteSpace(guid))
            {
                return false;
            }
            else
            {
                return true;
            }
            //Guid result;

            //return guid.Length < 33
            //    ? false
            //    : Guid.TryParse(guid.Substring(Unique.GuidPrefix.Length - 1, guid.Length - Unique.GuidPrefix.Length),
            //        out result);
        }

        public static bool IsDate(string date)
        {
            DateTime result;
            return DateTime.TryParse(date, out result);
        }

        public static bool IsNumeric(string str, out double number)
        {
            var qvalue = Regex.Replace(str, StandardFormat.NumberFormat, "");
            return double.TryParse(qvalue, out number);
        }
    }

    public enum LuceneOccur:int
    {
        MUST = 0,
        SHOULD = 1,
        MUST_NOT = 2
    }
    public enum LuceneQuery
    {
        PhraseQuery, //Term Search , exactly match condition
        QueryParse, //Search for Like %XXX% 
        FuzzyParse, // Prosiblility Search
        RangeQuery,
    }
}