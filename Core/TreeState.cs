﻿using System.Collections.Generic;

namespace Core
{
    public static class TreeState
    {    
        public static Dictionary<string,bool> GetTreeState(bool?loaded = null,bool? opened = null,bool? selected = null, bool? disabled = null )
        {
            Dictionary<string, bool> item = new Dictionary<string, bool>();
            if(loaded != null)
            {
                item["loaded"] = loaded.Value;
            }
            if (opened != null)
            {
                item["opened"] = opened.Value;
            }
            if (selected != null)
            {
                item["selected"] = selected.Value;
            }
            if (disabled != null)
            {
                item["disabled"] = disabled.Value;
            }
            return item;
        }

    }


}