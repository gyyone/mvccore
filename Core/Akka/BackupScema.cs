﻿using ClassLib;
using System.Collections.Generic;
using FieldProperty = TableProperty.FieldProperty;
namespace Core.Akka
{
    public class MessageBackupScema : IMessage
    {
        public string UserIdentity { set; get; }
        public List<string> SchemaIds { set; get; }
    }

    public class BackupResponse:IReturnMessage
    {
        public bool Error { set; get; }
        public byte[] Data { set; get; }
        public string Message { get; set; }
        public bool ValidToken { set; get; }
        public bool TimeOut { set; get; }
        public Dictionary<string,string> TableIdToNameDic { set; get; }
        public Dictionary<string, List<string>> TableToFieldNameDic { set; get; }
    }

    public class SchemaBackup
    {
        public List<TableProperty> TablePropertyList { set; get; }
        public List<FieldProperty> FieldPropertyList { set; get; }    
    
    }
}