using Akka.Actor;
using Akka.Remote;

namespace Core.Akka
{
    public class MessageReleaseLock
    {
        public MessageReleaseLock(IActorRef sender,string lockName)
        {
            LockName = lockName;
            Sender = sender;
        }
        public string LockName { set; get; }
        public IActorRef Sender { set; get; }
    }
}