using Akka.Actor;
using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageDownloadCSVReturn : IReturnMessage
    {
        public string DownloadFileUrl { set; get; }
        public string FileName { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
    }

    public class MessageDownloadCSV : IMessage
    {
        public string UserIdentity { set; get; }
        public List<string> FieldList { set; get; }       
        public Dictionary<string, object> SearchTerm { set; get; }
        public Dictionary<string, string> SortTerm { set; get; }  
        public string HostUrl { set; get; }
        public IActorRef StatusSender { set; get; }
    }
}