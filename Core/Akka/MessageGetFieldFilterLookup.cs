﻿namespace Core.Akka
{
    public class MessageGetFieldFilterLookup:IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public string HostUrl { set; get; }
        public string[] SeachName { set; get; }
        public bool Searchbykey { set; get; }
    }
    public class MessageGetFieldFilterLookupValue: MessageGetFieldFilterLookup
    {
        public string SelectedFieldId { set; get; }
    }
}