﻿namespace Core.Akka
{
    public class GetRecordAccessRight
    {
        public string TableId { set; get; }
        public string RecordId { set; get; }
    }
}