﻿namespace Core.Akka
{
    public class UpdateProgressStatus
    {
        public UpdateProgressStatus(string id,int total,int current,string message)
        {
            Id = id;
            Total = total;
            Current = current;
            Message = message;
        }
        public string Id { set; get; }
        public string Message { set; get; }
        public int Total { set; get; }
        public int Current { set; get; }
    }
}