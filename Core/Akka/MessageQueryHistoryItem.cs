using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageQueryHistoryItem: IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public Dictionary<string, object> SearchTerm { set; get; }
        public Dictionary<string, string> SortTerm { set; get; }
        public int PageSize { get; set; }
        public int StartIndex { get; set; }
        public LuceneQuery QueryType { set; get; }
        public LuceneOccur QueryOccur { set; get; }
        public bool Trash { set; get; }
        public string HostUrl { set; get; }
    }
}