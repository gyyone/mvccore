using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageQueryForm
    {
        public string TableId { set; get; }
        public string Id { set; get; }
        public string UserIdentity { set; get; }
        public Dictionary<string,object> SearchTerm { get; set; }
    }
}