using Akka.Actor;

namespace Core.Akka
{
    public class MessageLock 
    {
        public MessageLock()
        {
            LockName = "GlobalLock";
        }
        public MessageLock(IActorRef sender, string lockName)
        {
            LockName = lockName;
            Sender = sender;
        } 
        public string LockName { set; get; }
        public IActorRef Sender { set; get; }
    }
}