namespace Core.Akka
{
    public class MessageFeedBack : IReturnMessage
    {
        public MessageFeedBack()
        {
        }

        public MessageFeedBack(string token, string message)
        {
            Token = token;
            Message = message;
        }

        public string Token { set; get; }
        public string Message { set; get; }
        public bool Error { set; get; }
        public bool ValidToken { set; get; }
        public bool TimeOut { get; set; }

    }

}