using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageReturnAvailablePlugins : IReturnMessage
    {
        public MessageReturnAvailablePlugins ()
        {
            PluginList = new Dictionary<string, string>();
        }
        public Dictionary<string,string> PluginList { set; get; } 
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
    }
}