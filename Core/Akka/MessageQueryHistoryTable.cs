namespace Core.Akka
{
    public class MessageQueryHistoryTable:IMessage
    {
        public string TableId { set; get; }
        public string FieldId { set; get; }
        public string Id { set; get; }
        public string UserIdentity { set; get; }
    }
}