﻿using Akka.Actor;

namespace Core.Akka
{
    public class ClearData : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class RebuildSchema : IMessage
    {
        public string UserIdentity { set; get; }
        public string[] TableIdList { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class LockTable:IMessage
    {        
        public string LockTableId { set; get; }
        public bool IsLocked { set; get; }
        public string UserIdentity { set; get; }
    }
}