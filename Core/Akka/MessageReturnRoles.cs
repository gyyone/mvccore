using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageReturnRoles : IReturnMessage
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
        public Dictionary<string, string> RoleDic { set; get; }
        public string RoleLabelName { set; get; }
    }
}