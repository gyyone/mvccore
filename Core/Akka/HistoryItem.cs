﻿using System;

namespace Core.Akka
{
    public class HistoryItem
    {
        public string FieldName { set; get; }
        public string Value { set; get; }
        public string ChangedBy { set; get; }
        public string Date { set; get; }
    }
}