using System.Collections.Generic;

namespace Core.Akka
{
    public class GetUser : IMessage
    {
        public string UserIdentity { get; set; }   
    }
    public class MessageLogin:IMessage
    {
        public Dictionary<string, string> SearchTerm { set; get; }
        public string UserIdentity { get; set; }
        public string Password { set; get; }
        public bool ExternalLogin { set; get; }
    }
    public class RequestLogin : MessageLogin
    {   
    }
    public class MessageGetAutoComple
    {
        public string Search { set; get; }
    }
}