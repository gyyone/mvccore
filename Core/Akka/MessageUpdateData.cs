using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageInsertOrUpdate:IMessage
    {
        public MessageInsertOrUpdate()
        {
            DataList = new List<Dictionary<string, object>>();
            UpdateOrderOnly = false;
        }
        public string UserIdentity { set; get; }       
        public List<Dictionary<string, object>> DataList { set; get; }
        public string TableId { get; set; }
        public string ParentId { set; get; }
        public bool UpdateOrderOnly { set; get; }
    }
    public class MessageUpdateOnly : IMessage
    {
        public MessageUpdateOnly()
        {
            SearchTerm = new Dictionary<string, object>();
            SortTerm = new Dictionary<string, string>();
            Start = 0;
            Limit = int.MaxValue - 1;
            UpdateOrderOnly = false;
        }
        public string UserIdentity { set; get; }
        public Dictionary<string,object> SearchTerm { set; get; }
        public Dictionary<string, string> SortTerm { set; get; }
        public int Start { set; get; }
        public int Limit { set; get; }
        public Dictionary<string, object> Data { set; get; }
        public string TableId { get; set; }      
        public bool UpdateOrderOnly { set; get; }
    }
    public class MessageUpdateEncryptedField : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { get; set; }
        public string FieldId { set; get; }
        public string RecordId { set; get; }
        public string FieldValue { set; get; }
    }

    public class MessageViewEncryptedField : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { get; set; }
        public string FieldId { set; get; }
        public string RecordId { set; get; }
    }
    public class ReturnEncryptedField : IReturnMessage
    {
        public bool Error { set; get; }
        public string Message { set; get; }
        public bool ValidToken { set; get; }
        public bool TimeOut { set; get; }
    }
}