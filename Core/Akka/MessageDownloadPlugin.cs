using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageDownloadPlugin: IMessage
    {
        public string UserIdentity { set; get; }
        public List<string> TableNames { set; get; } 
    }
}