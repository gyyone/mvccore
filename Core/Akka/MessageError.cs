using System;

namespace Core.Akka
{
    public class MessageError : IMessage
    {
        public MessageError()
        {
        }

        public MessageError(Exception ex)
        {
            Exception = ex.ToString();
            Message = ex.Message;
            InnerException = ex.InnerException == null ? "" : ex.InnerException.ToString();
        }

        public string Exception { set; get; }
        public string UserIdentity { set; get; }
        public string InnerException { set; get; }
        public string Message { set; get; }
    }
}