using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageCreateStructure : IMessage
    {
        public string TableName { set; get; }
        public List<string> FieldNames { set; get; }
        public string UserIdentity { get; set; }
        public string IdUser { get; set; }
    }
}