
using ClassLib;
using SolrNetCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Akka
{
    public class SubTableProperty
    {
        public string TableId { set; get; } 
        public string Name { set; get; }
        public Dictionary<string,object> SearchTerm { set; get; }
    }
    public class ControlProperty
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string ControlType { get; set; }
        public QueryType QueryType { get; set; }
        public List<string> AccessMode { set; get; }
        public bool ReadOnly => !AccessMode.Contains("Modify");
        public bool Visible { set; get; }
        //Key:save value, Value:Display Text   
        public DataSource Source { set; get; }     
        public int SeqNo { get; set; }
        public int HistoryCount { get; set; }
        public object Value { get; set; }  
        public bool MultipleValue { set; get; }
        public string Options { get; set; }
        public string Hint { get; set; }
        public string GroupFrom { set; get; }
        public bool SetDefault { get;  set; }
    }
}