using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageQueryReturn : IReturnMessage
    {     
        public string TableType { set; get; }
        public int StartIndex { set; get; }
        public int Total { set; get; }       
        public int PageSize { set; get; }
        public bool ValidToken { set; get; }
        public bool TimeOut { get; set; }
        public bool Error { set; get; }
        public string Message { set; get; }
        public List<Dictionary<string, object>> Data { set; get; }    
        public Dictionary<string, SubTableProperty> SubTableList { get; set; }
    }
}