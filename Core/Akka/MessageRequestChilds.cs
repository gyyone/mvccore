using ClassLib;
using System.Collections.Generic;

namespace Core.Akka
{

    public class MessageReturnRequestChilds:IReturnMessage
    {
        public MessageReturnRequestChilds()
        {
            Childs = new Dictionary<string, string>();
            FieldIdDic = new Dictionary<string, TableProperty.FieldProperty>();
        }
        public Dictionary<string,string> Childs { set; get; }
        public Dictionary<string, TableProperty.FieldProperty> FieldIdDic { set; get; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
    }
}