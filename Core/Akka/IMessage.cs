﻿
using Akka.Actor;

namespace Core.Akka
{
    public interface IReturnMessage
    {
        bool Error { set; get; }
        string Message { set; get; }
        bool ValidToken { set; get; }
        bool TimeOut { set; get; }
    }
    public class ReturnMessage:IReturnMessage
    {
        public bool Error { set; get; }
        public string Message { set; get; }
        public bool ValidToken { set; get; }
        public bool TimeOut { set; get; }
    }
    public interface IMessage
    {
        string UserIdentity { set; get; }
    }
    public class ThreadMessage : IMessage
    {
        public ThreadMessage(IActorRef sender, IActorRef self, object obj)
        {
            this.Self = self;
            this.Sender = sender;
            this.Obj = obj;
        }
        public string UserIdentity { set; get; }
        public IActorRef Sender { set; get; }
        public IActorRef Self { set; get; }
        public object Obj { set; get; }
    }


}