﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Akka.Query
{
    public class TableMenuItem
    {
        public string id { set; get; }
        public string ParentId { set; get; }        
        public string TableId { set; get; }
        public string PageType { set; get; }
        public string Url { set; get; }
        public Dictionary<string,List<string>> SoftFilter { set; get; }

    }
}
