﻿using Akka.Actor;
using ClassLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FieldProperty = TableProperty.FieldProperty;
namespace Core.Akka.Query
{
    public class LookupQuery:IMessage
    {
        public List<TableSource> Sources { set; get; }
        public string UserIdentity { get; set; }
        public List<string> Searchlist { set; get; }
        public bool SearchbyId { set; get; }
        public LuceneQuery QueryType { set; get; }
        public LuceneOccur Occur { set; get; }
    }
    public class AutoCompleteQuery
    {
        public string UserIdentity { set; get; }
        public string TableName { set; get; }
        public string FieldName { set; get; }
        public ulong Rows { set; get; }
        public string Text { set; get; }
    }
    public class UpdateStructure
    {
        public UpdateStructure()
        {
            TableList = new List<TableProperty>();
            FieldList = new List<FieldProperty>();
        }
        public List<TableProperty> TableList { set; get; }
        public List<TableProperty.FieldProperty> FieldList { set; get; }
        public IActorRef ActorStatus { set; get; }
        public string Id { set; get; }

    }
    public class AddMenu : IMessage
    {
        public string UserIdentity { set; get; }
        public bool AddAsSubMenu { set; get; }
        public MenuProperty SelectedMenu { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class DeleteMenu : IMessage
    {
        public string UserIdentity { set; get; }
        public MenuProperty Menu { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class CopyMenuTo : IMessage
    {
        public string UserIdentity { set; get; }
        public MenuProperty TargetCopyMenu { set; get; }
        public MenuProperty ToMenu { set; get; }
        public bool CopyAsSubMenu { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class AddTable : IMessage
    {
        public string UserIdentity { set; get; }
        public TableProperty SelectedTable { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class CopyTableTo : IMessage
    {
        public string UserIdentity { set; get; }
        public string TargetCopyTable { set; get; }
        public string ToTable { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class AddSubTable : IMessage
    {
        public string UserIdentity { set; get; }
        public TableProperty ParentTable { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class CopyFieldTo : IMessage
    {
        public string UserIdentity { set; get; }
        public string TargetCopyField { set; get; }
        public string ToId { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class AddTableField : IMessage
    {
        public string UserIdentity { set; get; }
        public BaseProperty Selected { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class DeleteField : IMessage
    {
        public string UserIdentity { set; get; }
        public FieldProperty Field { set; get; }
        public bool DeleteData { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class DeleteTable : IMessage
    {
        public string UserIdentity { set; get; }
        public TableProperty Table { set; get; }
        public bool DeleteData { set; get; }
        public IActorRef StatusSender { set; get; }
    }
    public class QuerySchema : IMessage
    {
        public string UserIdentity { set; get; }
        public Dictionary<string,string> SearchValue { set; get; }    
    }
    public class QueryMenu: IMessage
    {
        public string UserIdentity { set; get; }
        public Dictionary<string, string> SearchValue { set; get; }
    }
    public class QueryTables: IMessage
    {
        public string UserIdentity { set; get; }
        public string SearchName { set; get; }
    }
    public class UpdateSchema:IMessage
    {
        public string UserIdentity { set; get; }    
        public string TableId { set; get; }
        public bool UpdateOrderOnly { set; get; }
        public List<string> RecordId { set; get; }

    }

    public class QueryFields : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }  
        public string SeachName { set; get; }
        public bool Unique { set; get; }
       
    }
    public class QueryUniqueListFields : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public string []SeachName { set; get; }
        public bool SearchByKey { set; get; }
    }
    public class QueryPropertyFields : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableName { set; get; }
        public string SeachName { set; get; }
        public string FieldId { set; get; }
        public bool SearchByKey { set; get; }
    }
    public class QueryFieldValue : IMessage
    {
        public string UserIdentity { set; get; }
        public string HostUrl { set; get; }
        public string TableId { set; get; }
        public string FieldId { set; get; }
        public string[] SeachName { set; get; }
        public bool Searchbykey { set; get; }

    }
}
