using System.Collections.Generic;


using FieldProperty = TableProperty.FieldProperty;
namespace Core.Akka
{
    public class MessageImportStructure : IMessage
    {
        public string UserIdentity { get; set; }
        List<TableProperty> Tables { set; get; }
        List<FieldProperty> Fields { set; get; }
    }
}