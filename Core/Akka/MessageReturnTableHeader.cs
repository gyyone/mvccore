using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Akka
{
    public class TableViewField
    {
        public string Id { set; get; }
        public int RowNo { set; get; }
        public string FieldName { set; get; }
        public string FieldId { set; get; }
        public bool Enable { set; get; }
        public bool AllowView { set; get; }
    }
    public class MessageReturnTableHeader:IReturnMessage
    {    
        public Dictionary<string, ControlProperty> SearchDic { set; get; } 
        public List<string> NotSortableList { set; get; }
        public List<TableViewField> Header { set; get; }
        public int LeftColumnFix { set; get; }
        public int RightColumnFix { set; get; }
        public Dictionary<string, SubTableProperty> SubTableList { set; get; }
        public string TableNameDisplay { set; get; }
        public bool HasFooter { set; get; }
        public string TableType { set; get; } 
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
        public Dictionary<string,object> SearchTerm { set; get; }
    }
}