﻿namespace Core.Akka
{
    public enum Request
    {
   
        SideBar,
        LanguageDic,
        UpdateLanguage,
        UpdateKeyWords,   
        DataConfig
    }

    public enum ActorTypes
    {
        DeployManager,
        DeployWorker,
        ImportWorker,
        Client,
        DataWriter
    }
    public class MessageRequest 
    {
        public Request RequestType { set; get; }
        public string Message { set; get; }
        public string UserIdentity { set; get; }
    }
}