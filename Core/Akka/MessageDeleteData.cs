using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageDeleteData : IMessage
    {
        public string UserIdentity { set; get; }
        public List<string> IdList { set; get; }
        public string TableId { get; set; }

    }
}