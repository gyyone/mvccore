namespace Core.Akka
{
    public class MessageReturnDownloadPlugin : IReturnMessage
    {
        public string FilePath { set; get; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
    }
}