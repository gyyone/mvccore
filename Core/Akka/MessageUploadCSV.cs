﻿using Akka.Actor;

namespace Core.Akka
{
    /// <summary>
    ///     Search result by term, strictly map with charactor.
    /// </summary>
    public class MessageUploadCSV
    {
        public string UserIdentity { set; get; }
        public string UploadId { set; get; }     
        public string ParentId { set; get; }
        public IActorRef StatusSender { set; get; }

    }
}