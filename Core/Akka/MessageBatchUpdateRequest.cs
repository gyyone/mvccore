using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageBatchUpdateRequest : IMessage
    {
        public string UserIdentity { set; get; }
        public List<Dictionary<string,object>> Data { set; get; }
    }

    public class MesssageBatchDeleteRequest : IMessage
    {
        public string UserIdentity { set; get; }
        public List<Dictionary<string, object>> Data { set; get; }
    }

    public class MessageDeleteRequest : IMessage
    {
        public string UserIdentity { set; get; }
    }
}