using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageTableHeader
    {
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public bool Trash { get; set; }
        public Dictionary<string,object> SearchTerm { set; get; }
    }
}