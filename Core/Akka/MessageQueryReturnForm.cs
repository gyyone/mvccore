﻿using System.Collections.Generic;

namespace Core.Akka
{
    public class MessageQueryReturnForm:IReturnMessage
    {
        public string TableName { set; get; }

        public string Id { set; get; }
        public List<ControlProperty> Fields { set; get; }
        public Dictionary<string, SubTableProperty> SubTableList { set; get; }
        public bool ValidToken { set; get; }
        public bool TimeOut { get; set; }

        public bool Error { get; set; }
        public string Message { set; get; }
        public Dictionary<string,object> SearchTerm { get; set; }
        public string TableNameDisplay { get; set; }
        public Dictionary<string,Dictionary<string,List<string>>> Dependancy { set; get; }
    }
}