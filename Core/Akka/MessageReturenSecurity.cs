using System.Collections.Generic;
using FieldProperty = TableProperty.FieldProperty;
namespace Core.Akka
{
    public class MessageReturenSecurity : IReturnMessage
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ValidToken { get; set; }
        public bool TimeOut { get; set; }
        //Key Id: value Display RoleName and Description
        public Dictionary<string,string> FieldAccessDic { set; get; } 
        public Dictionary<string, Dictionary<string, FieldProperty>> TableToFieldProteryDic { set; get; }
    }
}