using System.Collections.Generic;

namespace Core.Akka
{
    /// <summary>
    ///     Search result by term, strictly map with charactor.
    /// </summary>
    public class MessageQuery 
    {        
        public string UserIdentity { set; get; }
        public string TableId { set; get; }
        public Dictionary<string, object> SearchTerm { set; get; }
        public Dictionary<string, string> SortTerm { set; get; }
        //Set Pagesize = 0 to view all record
        public int PageSize { get; set; }
        public int StartIndex { get; set; }
        public LuceneQuery QueryType { set; get; }
        public LuceneOccur QueryOccur { set; get; }
        public string HostUrl { set; get; }
        public List<string> Columns { set; get; }
        public bool RawData { set; get; }
    }
}