using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Akka.Actor;

namespace Core.Akka
{
    public static class Message<T>
    {
        public static T Query(ActorSelection remotehandle, object message, int timeout = 30)
        {
            T objectReceive = default(T);
            Task task =
                remotehandle.Ask<T>(
                    message,(TimeSpan.FromMilliseconds(GetMaxTimeOutSecond(timeout)))).ContinueWith(t =>
                    {
                        objectReceive = t.Result;

                    }, TaskContinuationOptions.OnlyOnRanToCompletion);

            task.Wait(GetMaxTimeOutSecond(timeout));
            return objectReceive;
        }
 
        public static int GetMaxTimeOutSecond(int timeout)
        {
            //Min is 30 second wait, max is8640
            return Math.Max(Math.Min(timeout , 8640) * 1000, 30000);
        }
        public static async Task<T> QueryTask(ActorSelection remotehandle,object message,int timeout = 30)
        {
            T objectReceive = default(T);
           
            await
                remotehandle.Ask<T>(
                    message, (TimeSpan.FromMilliseconds(GetMaxTimeOutSecond(timeout)))).ContinueWith(t =>
                    {
                        if (t.Status == TaskStatus.RanToCompletion)
                        {
                            objectReceive = t.Result;
                        }
                        else
                        {
                            objectReceive =(T) Activator.CreateInstance(typeof(T));
                            ((IReturnMessage)objectReceive).ValidToken = false;
                            ((IReturnMessage)objectReceive).Error = true;
                            ((IReturnMessage)objectReceive).Message = "Server Not Response";
                            ((IReturnMessage) objectReceive).TimeOut = true;
                        }

                    });
            return objectReceive;
        }
        public static async Task<T> QueryTask(IActorRef remotehandle, object message, int timeout = 30)
        {
            T objectReceive = default(T);

            await
                remotehandle.Ask<T>(
                    message, (TimeSpan.FromMilliseconds(GetMaxTimeOutSecond(timeout)))).ContinueWith(t =>
                    {
                        if (t.Status == TaskStatus.RanToCompletion)
                        {
                            objectReceive = t.Result;
                        }
                        else
                        {
                            objectReceive = (T)Activator.CreateInstance(typeof(T));
                            ((IReturnMessage)objectReceive).ValidToken = false;
                            ((IReturnMessage)objectReceive).Error = true;
                            ((IReturnMessage)objectReceive).Message = "Server Not Response";
                            ((IReturnMessage)objectReceive).TimeOut = true;
                        }

                    });
            return objectReceive;
        }
    }
}