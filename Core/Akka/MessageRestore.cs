﻿using Akka.Actor;
namespace Core.Akka
{
    public class MessageRestore : IMessage
    {
        public string UserIdentity { set; get; }
        public string TableID { set; get; }
        public string RecordId { set; get; }
        public IActorRef StatusSender { set; get; }
    }
}