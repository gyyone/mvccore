using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Akka.Remote;

namespace Core
{
    public enum MinViews
    {
        Minute = 0,
        Hour,
        Day,
        Month,
        Year
    }

    public class Option
    {
        public Option()
        {
            OptionValue = "";
        }
        public Option(string value)
        {
            OptionValue = value;
        }
        //Optional Value
        public string OptionValue { set; get; }
    }

    public class DateTimeOption: Option
    {
        public string UserDateFormat { set; get; }
        public DateTimeOption()
        {
            AutoClose = true;
            ShowSecond = false;
            TodayButton = true;
            Meridian = false;
            TodayHighlight = true;
            MinuteStep = 5;
            AutoClose = true;
        }

        public DateTimeOption(MinViews minview)
        {
            MinView = (int) minview;
        }
        //dd MM yyyy - hh:ii
        public DateTimeOption(MinViews minview,string dateformat):this()
        {
            MinView = (int)minview;
            Format = dateformat;
            UserDateFormat = dateformat;
            switch (minview)
            {
                case MinViews.Minute:
                    Format = dateformat;
                    break;
                case MinViews.Hour:
                    Regex hour = new Regex("[^yMdHhmt.:-]");
                    Format = hour.Replace(dateformat, "").Trim(new char[] { '-', '.', ':' });
                    break;
                case MinViews.Day:
                    Regex day = new Regex("[^yMd.-]");
                    Format = day.Replace(dateformat, "").Trim(new char[] {'-','.',':'});
                    break;
                case MinViews.Month:
                    Regex month = new Regex("[^yM-]");
                    Format = month.Replace(dateformat, "").Trim(new char[] { '-', '.', ':' });
                    break;
                case MinViews.Year:
                    Regex year = new Regex("[^y.:-]");
                    Format = year.Replace(dateformat, "").Trim(new char[] { '-', '.', ':' });
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(minview), minview, null);
            }
            Meridian = Format.IndexOf("h",0,StringComparison.InvariantCulture) >= 0;

        }


        //ISO 8601 also supported
        //http://www.malot.fr/bootstrap-datetimepicker/
        public string Format { set; get; }
        public bool AutoClose { set; get; }
        public bool ShowSecond { set; get; }
        public bool TodayButton { set; get; }
        public string StartDate { set; get; }
        //The minuteStep property can be used to specify the gap between each preset on the hour view.
        public int MinuteStep { set; get; }
        //pickerPosition: "bottom-left"
        //False: 24 hours format
        public bool Meridian { set; get; }
        public bool TodayHighlight { set; get; }
        //0: Minute
        //1: Hour
        //2: Month
        //3: year
        //4 Decade
        public int MinView { set; get; }
        private string selectedDate { set; get; }
        public string InitialDate
        {
            set { selectedDate = value; }
            get
            {
                if (string.IsNullOrWhiteSpace(selectedDate) || string.IsNullOrWhiteSpace(UserDateFormat))
                {
                    return "";
                }
                return DateTime.ParseExact(selectedDate, UserDateFormat, CultureInfo.InvariantCulture).ToString(Format);
            }

        }
    }
}