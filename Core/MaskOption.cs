using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Core
{
    public class MaskOption:Option
    {
        public MaskOption(string pattern, string option)
        {
            Pattern = pattern;
            Params = option;
        }
        public string Pattern { set; get; }
        public string Params { set; get; }

    }

}