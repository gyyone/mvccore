﻿using Akka.Actor;

namespace Core
{
    public static class R
    {
        public static class SystemSettings
        {
            public static int PageSize = 50000;
        }
        public static class SystemName
        {
            public const string MainSystem = "MainSystem";
            public const string WebServices = "WebServices";        
        }
        public static class ActorName
        {
            public const string DataHandlerMasterActor = "DataHandlerMasterActor";

            public const string DataHandlerActor = "DataHandlerActor";

            public const string ImportWorkerActor = "ImportWorkerActor";

            public static string WebServerHandlerActor = "WebServerHandlerActor";
        }

        public static class ClassName
        {
            public const string TableProperty = "TableProperty";
            public const string FieldProperty = "FieldProperty";
            public const string MenuProperty = "MenuProperty";

        }
      
        public enum Event
        {
            WebServerConnect,
        }
    }
}
