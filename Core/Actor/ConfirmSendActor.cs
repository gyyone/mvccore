﻿using Core.Actor.ComObject;
using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Core;
using Util;
using ClassExtension;
namespace Core.Actor
{
    /// <summary>
    /// Confirm Object to be delivery
    /// </summary>
    public class SendAndConfirmObject
    {
        public IActorRef Sender { set; get; }
        public object obj { set; get; }
        public Type type { set; get; }
    }

    /// <summary>
    /// Same Event Name will be forfeit and replace
    /// </summary>
    public class SendAndConfirmEvent
    {
        public string DestinationResourceId { set; get; }
        public IActorRef Sender { set; get; }
        public object obj { set; get; }
    }
    public abstract class ConfirmSendActor : TypedActor,
        IHandle<SendAndConfirmObject>,
        IHandle<EnumObj>,
        IHandle<Core.Actor.ConfirmSendActor.ConnectEvent>,
        IHandle<string>
    {
        public class ConnectEvent
        {
            public string ResourceId { set; get; }
        }
        protected string ResourceId { set; get; }
        Dictionary<string, SendAndConfirmEvent> eventDic = new Dictionary<string, SendAndConfirmEvent>();
        Queue<KeyValuePair<string, SendAndConfirmObject>> SendQueueItem = new Queue<KeyValuePair<string, SendAndConfirmObject>>();
        object mutexItemSend = new object();
        object mutexevent = new object();
        object mutexResource = new object();
        public Timer tmr = new Timer(1000);
        public IActorRef SelfActor { set; get; }
        private Dictionary<string, ICanTell> RemoteDic = new Dictionary<string, ICanTell>();
        public ConfirmSendActor(string resourceId)
        {
            tmr.Elapsed += ConfirmSendHandler;
            tmr.Enabled = true;
            tmr.AutoReset = true;
            tmr.Stop();
            if (string.IsNullOrWhiteSpace(resourceId))
            {
                throw new Exception("Resource Id Could not be null for ConfirmSendActor");
            }
            ResourceId = resourceId;
        }
        public void UpdateResourceId(string remoteResourceId)
        {

        }
        public string Connect(ActorSelection actorDes, string senderResourceId)
        {
            string destinationResourceid = actorDes.AskSync<string>(new ConnectEvent() { ResourceId = senderResourceId }, 10);
            if (string.IsNullOrWhiteSpace(destinationResourceid))
            {
                return destinationResourceid;
            }
            RemoteDic[destinationResourceid] = actorDes;
            return destinationResourceid;
        }
        public void Handle(ConnectEvent message)
        {
            System.Threading.Monitor.Enter(mutexResource);
            try
            {
                RemoteDic[message.ResourceId] = Sender;
                Sender.Tell(ResourceId);
            }
            finally
            {
                System.Threading.Monitor.Exit(mutexResource);
            }
       
        }

        public ICanTell GetSender(string resourceId)
        {
            System.Threading.Monitor.Enter(mutexResource);
            ICanTell act = ActorRefs.NoSender;
            if (RemoteDic.ContainsKey(resourceId))
            {
                act = RemoteDic[resourceId];
            }

            System.Threading.Monitor.Exit(mutexResource);
            return act;
        }

        public void Handle(SendAndConfirmObject message)
        {
            Console.WriteLine("Confirm Received ");
            Sender.Tell(new ConfirmReceived());
            Self.Tell(Cast(message.obj, message.type), message.Sender);
        }
        public void Handle(EnumObj message)
        {
            Self.Tell(Cast(message.obj, message.type), Sender);
        }

        public object Cast(object source, Type type)
        {
            if (type.IsEnum)
            {
                return Enum.ToObject(type, Enum.Parse(type,source.ToString()));
            }
            return source;
        }

        public bool CanSend(ICanTell destination, IActorRef sender, object objectsend, int timeout = 60000)
        {
            bool sent = false;

            if (destination is ActorSelection)
            {
                return CanSend((ActorSelection)destination, sender, objectsend, timeout);

            }
            else if (destination is IActorRef)
            {
                return CanSend((IActorRef)destination, sender, objectsend, timeout);
            }


            return sent;
        }
        public bool CanSend(IActorRef destination, IActorRef sender, object objectsent, int timeout = 60000)
        {
            Console.WriteLine("IActorRef Sending " + objectsent.ToString());
            bool sent = false;

            destination.Ask<ConfirmReceived>(new SendAndConfirmObject() { obj = objectsent, Sender = sender, type = objectsent.GetType() }).ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    sent = true;
                    Console.WriteLine("Sent Completed");
                }
                else if (t.Status == TaskStatus.Canceled)
                {
                    Console.WriteLine("Sent Cancelled");
                    sent = false;
                }
            }).Wait(timeout);
            
            return sent;
        }
        public bool CanSend(ActorSelection destination, IActorRef sender, object objectsent, int timeout = 60000)
        {
            bool sent = false;     
          // Console.WriteLine("ActorSelection Sending " + objectsent.ToString());
           destination.Ask<ConfirmReceived>(new SendAndConfirmObject() { obj = objectsent, Sender = sender, type = objectsent.GetType() }).ContinueWith(t =>
           {
               if (t.Status == TaskStatus.RanToCompletion)
               {
                   sent = true;
                   Console.WriteLine("Sent Completed");
               }
               else if (t.Status == TaskStatus.Canceled)
               {
                   sent = false;
               }
           }).Wait(timeout);

            

            return sent;
        }
        private void ConfirmSendHandler(object sender, ElapsedEventArgs e)
        {
            if (System.Threading.Monitor.TryEnter(mutexItemSend))
            {
                try
                {
                    if (SendQueueItem.Count > 0)
                    {
                        var item = SendQueueItem.Dequeue();
                        try
                        {
                            System.Threading.Monitor.Enter(mutexResource);
                            if (!CanSend(RemoteDic[item.Key], item.Value.Sender, item.Value.obj))
                            {
                                SendQueueItem.Enqueue(item);
                            }
                            else
                            {
                                Console.WriteLine("ConfirmSend Fail");
                            }
                        

                        }
                        finally
                        {
                            System.Threading.Monitor.Exit(mutexResource);
                        }

                    }
                }
                finally
                {
                    System.Threading.Monitor.Exit(mutexItemSend);
                }


            }


            if (System.Threading.Monitor.TryEnter(mutexevent))
            {
                try
                {
                    if (eventDic.Count > 0)
                    {

                        foreach (var key in eventDic.Keys.ToList())
                        {
                            var item = eventDic[key];
                            try
                            {
                                System.Threading.Monitor.Enter(mutexResource);
                                if (CanSend(RemoteDic[item.DestinationResourceId], item.Sender, item.obj))
                                {
                                    eventDic.Remove(key);
                                }
                                else
                                {
                                    Console.WriteLine("ConfirmFiring Event Fail");
                                }
                             
                            }
                            finally
                            {
                                System.Threading.Monitor.Exit(mutexResource);
                            }


                        }
                    }
                }
                finally
                {
                    System.Threading.Monitor.Exit(mutexevent);
                }
            }

            if (eventDic.Count == 0 && eventDic.Count == 0)
            {
                tmr.Stop();
            }

        }
        //Make sure it have reply at the end of calling
        /// <summary>
        /// Do not use this feature for all message send. It will increase the load of checking.
        /// More processing need. Use it when it is required or imporatant.
        ///Make sure the remote server reply "ConfirmReceived" message
        /// </summary>
        /// <param name="obj">object to send to</param>
        /// <param name="sender">Actor of Remote server to send</param>
        public void ConfirmSendAsyn(string destResourceId, IActorRef sender, object objectsend)
        {
            System.Threading.Monitor.Enter(mutexItemSend);

            if (sender == null)
            {
                throw new Exception("Sender cannot be null");
            }
            if (string.IsNullOrWhiteSpace(destResourceId))
            {
                throw new Exception("Destination resource id cannot be null");
            }

            SendQueueItem.Enqueue(new KeyValuePair<string, SendAndConfirmObject>(destResourceId, new SendAndConfirmObject() { obj = objectsend, Sender = sender }));
            tmr.Start();
            System.Threading.Monitor.Exit(mutexItemSend);
        }
        /// <summary>
        ///If the object with Same Event Id haven't send yet, would overwrite the object and send
        ///Single 
        /// </summary>
        /// <param name="destination"></param>
        /// <param name=""></param>
        /// <param name="eventId"></param>
        /// <param name="objectsend"></param>
        public void ConfirmFireEvent(string destResourceId, IActorRef sender, string eventId, object objectsend)
        {
            System.Threading.Monitor.Enter(mutexevent);
            if (sender == null)
            {
                throw new Exception("Sender cannot be null");
            }
            if (string.IsNullOrWhiteSpace(destResourceId))
            {
                throw new Exception("Destination resource id cannot be null");
            }

            eventDic[eventId] = new SendAndConfirmEvent() { DestinationResourceId = destResourceId, obj = objectsend, Sender = sender };
            tmr.Start();
            System.Threading.Monitor.Exit(mutexevent);
        }

        public void Handle(string message)
        {
            Console.WriteLine(message);
        }

    }

}