﻿using Core.Actor.ComObject;
using Akka.Actor;


namespace Core.Actor
{
    public class BaseActor : TypedActor,
        IHandle<TerminateRequest>
    {
        public bool Terminate { set; get; }
        public IActorRef MasterActor { set; get; }
        public int TerminateCount { set; get; }

        public virtual void Handle(TerminateRequest message)
        {
            Terminate = true;
            MasterActor = Sender;
            MasterActor.Tell(new TerminateConfirm() { Actor = this.Self });
            Context.Stop(this.Self);
        }

        public void Stop(IActorRef actor)
        {
            MasterActor.Tell(new TerminateConfirm() { Actor = actor });
            Context.Stop(actor);
        }
    }
}