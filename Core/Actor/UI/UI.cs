﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


public static class UI
{
    public enum UIFile
    {
        SwitchON,
        SwitchOFF
    }
    public static string basePath = "Actor/UI/";
    public static string Check(bool isChecked)
    {
        if(isChecked)
        {
            return "<span class='fas fa-check'></span> Checked";
        }
        return "<span class='fas fa-times'></span> Checked";
    }
    public static string CheckBox(bool isChecked,string checkColor, string uncheckColor)
    {
        string filepath = AppDomain.CurrentDomain.BaseDirectory + (isChecked ? $"{basePath}CheckedBox.html" : $"{basePath}UnCheckedBox.html");
        return File.ReadAllText(filepath);  
    }
    public static string SwitchButton(bool isChecked, string checkColor,string uncheckColor)
    {
        string filepath = AppDomain.CurrentDomain.BaseDirectory + (isChecked ? $"{basePath}SwitchButtonON.html" : $"{basePath}SwitchButtonOFF.html");
        return File.ReadAllText(filepath);
    }
    public static string GetDetailButtonHtml(string tableId, bool visible = true)
    {
        string text = visible ? "visible" : "invisible";
        return $"<div class='flex-fill btn p-0 text-primary {text}'> <i name=\"" + tableId.Replace(@"\", "") + "\" class='details-control blue fas fa-plus' data-tablename='" + tableId + "' ></i></div>";
    }

    public static string GetEditButtonHtml(string tableId,string recordid, bool visible = true)
    {
        string text = visible ? "visible" : "invisible";
        return $"<div class='flex-fill btn p-1 text-success {text}' data-id='" + recordid + "'> <i onclick=\"EditTableRecord(this,false);\" data-tablename='" + tableId + "' class='fas fa-edit'></i></div>";
        //return "<button type='button' class='btn btn-sm green buttonMargin' onclick='EditTableRecord(this,false);' data-tablename='" + tableName + "' style='visibility:" + (visible ? "visible" : "hidden") + ";'><i class='fas fa-edit fa-lg'></i></button>";
    }

    public static string GetCheckBoxHtml(string tableId, string eventchange = "")
    {
        return "<div class='flex-fill btn p-1'> <input name='" + tableId + "'  type='checkbox' class='ICheckTable' data-onchanged=\"" + eventchange + "\" /></div>";
    }

    public static string GetDeleteButtonHtml(bool trash,string recordid, bool visible = true)
    {
        string text = visible ? "visible" : "invisible";
        return $"<div class='flex-fill btn p-1 text-danger {text}' data-id='" + recordid + "' > <i onclick=\"DeleteRecord(this," + trash.ToString().ToLower() + ",false);\"  class='fas fa-trash'></i></div>  ";
    }

    public static string GetTableButtonRows(string tableId,string recordid,bool trash, bool visibleDetail, bool visibleEdit, bool visibleDelete)
    {
        return $"<div class='flex-row'>{GetDetailButtonHtml(tableId,  visibleDetail)}{GetEditButtonHtml(tableId,recordid,visibleEdit)}{GetDeleteButtonHtml(trash,recordid, visibleDelete)}</div>";
    }
}

