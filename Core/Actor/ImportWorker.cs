using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Akka.Actor;
using Akka.Cluster;
using Akka.Cluster.Tools.PublishSubscribe;
using Core.Actor;
using Core;
using Core.Akka;
using Timer = System.Timers.Timer;
using ComObject;
using Core.Actor.ComObject;
using DBHandler;
using Util;
using ClassLib;
using LogHelper;

namespace Core.Actor
{
    public class ImportWorker : BaseActor,
        IHandle<TerminateImport>,
        IHandle<MessageConfig>,
        IHandle<OnProcess>,
        IHandle<AddNewLanguageKey>
    {

        private static readonly SemaphoreSlim semaphoreImport = new SemaphoreSlim(1, 1);

        private const int maxCount = 1;
        private static readonly SemaphoreSlim semaphoreMaxTempIndex = new SemaphoreSlim(maxCount, maxCount);
        private readonly double intervalCheck = 5000d;
        private readonly IActorRef MainSystem;
        private Queue<string> indexQueue = new Queue<string>();
        private Queue<string> indexdeleteQueue = new Queue<string>();
        private KeyWordsDic _keyWords { set; get; }
        private DataHandlerMasterConfig _dhmasterConfig { set; get; }
   
        private object lockWork = new object();
        public readonly Cluster Cluster = Cluster.Get(Context.System);
        public readonly IActorRef Mediator = DistributedPubSub.Get(Context.System).Mediator;
 
        public ImportWorker(DataHandlerMasterConfig dhMasterConfig, IActorRef mainSystem)
        {
            _dhmasterConfig = dhMasterConfig;
            MainSystem = mainSystem;
            KeyWordsDic.LanguageActor = mainSystem;

            _Timer = new Timer(intervalCheck);
            _Timer.Elapsed += OnTimedEvent;
            _Timer.Elapsed += OnTimedEvent;           
            _Timer.Enabled = true;
            //Should Trigger By Event
            _Timer.Stop();
            _CurrentActor = Self;

            Mediator.Tell(new Subscribe(Topic.ConfigChange.ToString(), Self));
            Mediator.Tell(new Subscribe(Topic.OnProcess.ToString(), Self));
            _UpdateLanguageCompleted = false;
            _UpdateStructureCompleted = false;
            Task task = MainSystem.Ask<MessageConfig>(new MessageRequest() { RequestType = Request.DataConfig }).ContinueWith(t =>
            {
                _dataStructure = t.Result.DataStructure;
                _keyWords = t.Result.KeyWords;
                _UpdateLanguageCompleted = true;
                _UpdateStructureCompleted = true;
            });
            task.Wait();
        }

        private bool _UpdateStructureCompleted = false;
        private bool _UpdateLanguageCompleted = false;
        private Timer _Timer { get; }
        private IActorRef _CurrentActor { get; }
        private string Token { set; get; }
        private DataStructure _dataStructure { set; get; }

        public void Handle(MessageConfig config)
        {
            LogHandler.Debug("Import Worker Message Config");
            semaphoreImport.Wait();
        
            for (int x = 0; x < maxCount; x++)
            {
                semaphoreMaxTempIndex.Wait();
            }
            _keyWords = config.KeyWords;
            _dataStructure = config.DataStructure;     
       
            _UpdateLanguageCompleted = true;
            _UpdateStructureCompleted = true;
            for (int x = 0; x < maxCount; x++)
            {
                semaphoreMaxTempIndex.Release();
            }
    
            semaphoreImport.Release();
        }



        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {

            if (semaphoreImport.Wait(100))
            {
                if (Terminate)
                {
                    Terminate = false;
                    _CurrentActor.Tell(new TerminateImport());
                }
                semaphoreImport.Release();
            }

            if (_UpdateLanguageCompleted && _UpdateStructureCompleted)
            {        
                #region Process Import from Excel

                if (semaphoreImport.Wait(100))
                {
  
                    try
                    {
                
                    }
                    catch (Exception ex)
                    {
                        MainSystem.Tell(new MessageError(ex));
                        LogHandler.Error(ex);
                    }
                    finally
                    {
                        semaphoreImport.Release();
                    }
                }

                #endregion

            }
        

        }

      


   
        private void UpdateUpload(Users user, string id, string processstatus, int total, int totalProcessed, double progress, string errorMessage)
        {
            string tableName = "Upload";
            var fields = new Dictionary<string, object>();
            fields.Add(FieldNames.id.ToString(), id);
            fields.Add("TotalProcessed", totalProcessed.ToString());
            fields.Add("Total", total.ToString());
            fields.Add("Progress", progress.ToString("0.##%"));
            fields.Add("ProcessStatus", processstatus);
            _dataStructure.Save(user, tableName, new List<Dictionary<string, object>> { fields });

            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                tableName = @"Upload_ErrorMessage";
                fields.Clear();
                fields.Add(FieldNames.id.ToString(), Unique.NewGuid());
                fields.Add(FieldNames.ParentId.ToString(), id);
                fields.Add(FieldNames.ChangedBy.ToString(), user.id);
                fields.Add("Message", errorMessage);
                _dataStructure.Save(user, tableName, new List<Dictionary<string, object>> { fields });
            }
        }


        private List<Dictionary<string, object>> GetPendingUpload()
        {
            var resultlist = new List<Dictionary<string, object>>();       
            var searchTerm = new Dictionary<string, object>();
            searchTerm.Add("ProcessStatus", "Processing");

            var records = _dataStructure.Query("Upload", searchTerm,start:0,limit:1000,strict:true).docs;

            foreach (var record in records)
            {
                if (record != null)
                {
                    resultlist.Add(record);
                }
            }
            return resultlist;
        }

        public void Handle(TerminateRequest message)
        {
            Terminate = true;
            MasterActor = Sender;
        }
        public void Handle(TerminateImport message)
        {
            LogHandler.Debug("Terminated");
            Stop(Self);
         
        }

        public void Handle(OnProcess message)
        {
            if (message.OnHold)
            {
                _Timer.Stop();
            }
            else
            {
               _Timer.Start();
            }
        }

        public void Handle(AddNewLanguageKey message)
        {

            foreach (var addedkeyword in message.Language)
            {
                if (!_keyWords.Languages.ContainsKey(addedkeyword.Key))
                {
                    _keyWords.Languages.Add(addedkeyword.Key, new Dictionary<string, string>());
                }
                foreach (var word in addedkeyword.Value)
                {
                    _keyWords.Languages[addedkeyword.Key][word.Key] = word.Value;
                }
            }
        }
    }

    public class TerminateImport
    {
        
    }
}