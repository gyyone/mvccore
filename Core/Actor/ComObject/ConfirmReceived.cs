﻿using Akka.Actor;

namespace Core.Actor.ComObject
{
    public class ConfirmReceived
    {
        public IActorRef Sender { set; get; }
    }
    public class Resource
    {
        public string resourceId { set; get; }
    }
    public class LogMessage
    {
        public string text { set; get; }
    }
    public class Stop
    {

    }
}
