using Akka.Actor;

namespace Core.Actor.ComObject
{
    public class TerminateRequest
    {
    }

    public class TerminateConfirm
    {
        public IActorRef Actor { set; get; }
    }
}