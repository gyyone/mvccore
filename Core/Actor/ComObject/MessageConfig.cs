using DBHandler;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Core;
using System.Collections.Generic;
using ClassLib;
namespace ComObject
{
    public class MessageConfig: ICloneable
    {
        public MessageConfig(DataStructure ds, KeyWordsDic keyword)
        {
            DataStructure = ds.Clone();
            KeyWords = keyword.Clone();
        }
        public DataStructure DataStructure { set; get; }
        public KeyWordsDic KeyWords { set; get; }
        public virtual dynamic Clone()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter { });
            settings.TypeNameHandling = TypeNameHandling.Auto;
            string temp = JsonConvert.SerializeObject(this, settings);
            return JsonConvert.DeserializeObject<MessageConfig>(temp, settings);
        }
    }
}