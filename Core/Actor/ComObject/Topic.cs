﻿namespace Core.Actor
{
    public enum Topic
    {
        DataHandler,
        ConfigChange,
        OnProcess,
    }
}