using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Core.Actor;
using Newtonsoft.Json;
using Core;
using Core.Akka;
using ComObject;
using DBHandler;
using SolrNetCore;
using Core.Actor.ComObject;
using System.Collections;
using Newtonsoft.Json.Converters;
using Util;
using FieldProperty = TableProperty.FieldProperty;
using Pages= MenuProperty.Pages;
using JsTable = MenuProperty.Pages.JsTable;
using ClassExtension;
using ClassLib;
using LogHelper;

namespace Core.Actor
{
    public partial class DataHandlerMaster : ConfirmSendActor,
        IHandle<MessageRequest>,
        IHandle<SubscribeAck>,
        IHandle<MessageConfig>,    
        IHandle<AddNewLanguageKey>,
        IHandle<string>,  
        IHandle<R.Event>,
        IHandle<Terminated>,
        IHandle<object>

    {

        public IActorRef Mediator { set; get; }
        public DataHandlerMasterConfig _dhmasterConfig { set; get; }
        private DataStructure _dataStructure { set; get; }
        private KeyWordsDic _keyWords { set; get; }
        private string ConnectionStr { set; get; }
        private string ConnectionStrTx { set; get; }
        private Mutex mutex = new Mutex(false,"Master");
        private List<IActorRef> webServerList { set; get; }
        public object AssemblyInfo { get; private set; }
        private List<string> tableListUpdate = new List<string>() { FieldProperty.ClassID, TableProperty.ClassID, MenuProperty.ClassID, Pages.ClassID };
        public DataHandlerMaster(IActorRef mediator,DataHandlerMasterConfig dhConfig):base(dhConfig.ResourceId)
        {   
            Mediator = mediator;
            _dhmasterConfig = dhConfig;
            _keyWords = new KeyWordsDic();
            mutex.WaitOne();
            webServerList = new List<IActorRef>();
            mutex.ReleaseMutex();
            Init(null);
            Mediator.Tell(new Put(Self));
            Mediator.Tell(new Subscribe(Topic.DataHandler.ToString(), Self));
            Mediator.Tell(new Subscribe(Topic.ConfigChange.ToString(), Self));

        }
        private void Init(object obj)
        {
            mutex.WaitOne();
            
            _dataStructure = GetDbStructure();            
            _keyWords.Languages = LoadLanguagesDic();
     
            // SaveMenuStructure("", _menuStructure);
            _dataStructure.InitializeCompleted();


            mutex.ReleaseMutex();
        }
        #region Base Method

        private DataStructure GetDbStructure()
        {
            //DataStructure _temp2 = LoadDataStructure(_globalConfig.Get(ConfigKey.SolrAPI));
            //_temp2.solrApi.Core.ClearAllCore();
            while (!SolrApi.IsSorUp(_dhmasterConfig.SolrAuthentication))
            {
                LogHandler.Debug("Waiting solr up.");
                System.Threading.Thread.Sleep(3000);
            }
            foreach(var core in tableListUpdate)
            {
                while(!SolrApi.IsCoreUp(_dhmasterConfig.SolrAuthentication, core))
                {
                    LogHandler.Debug($"Waiting Core {core} up.");
                    System.Threading.Thread.Sleep(3000);
                }
            }
            DataStructure _temp = new DataStructure(_dhmasterConfig.SolrAuthentication);
            _temp.UploadedFolders = _dhmasterConfig.UploadFolders;
            _temp.MinSpaceRequired = _dhmasterConfig.MinSpaceRequired;
            List<Dictionary<string, object>> tableproperty = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> fieldproperty = new List<Dictionary<string, object>>();
            try
            {
         
                tableproperty = _temp.solrApi.DocQuery.CustomQuery(TableProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs;
                fieldproperty = _temp.solrApi.DocQuery.CustomQuery(FieldProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs;
                _temp.SetTablePropertyAndField(tableproperty.Select(x => x.CastToObject<TableProperty>()).ToList(), fieldproperty.Select(x => x.CastToObject<FieldProperty>()).ToList());

                List<FieldProperty> flist =  _temp.FieldIdDic.Where(x => x.Value.Source.KeySource.Any() && x.Value.Source.KeySource.Where(y => y.GetSource() == null).Any()).Select(x => x.Value).ToList();
                
                List<MenuProperty> menulist=  _temp.solrApi.DocQuery.CustomQuery(MenuProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs.Select(x => x.CastToObject<MenuProperty>()).ToList();
                _temp.MenuIdDic = _temp.solrApi.DocQuery.CustomQuery(MenuProperty.ClassID, "*:*", new List<string>() { "*" }, "").docs.Select(x => x.CastToObject<MenuProperty>()).ToList().ToDictionary(x => x.id, y => y);
                _temp.PagesIdDic = _temp.solrApi.DocQuery.CustomQuery(Pages.ClassID, "*:*", new List<string>() { "*" }, "").docs.Select(x => x.CastToObject<Pages>()).ToList().ToDictionary(x => x.id, y => y);
                _temp.JsTableIdDic = _temp.solrApi.DocQuery.CustomQuery(JsTable.ClassID, "*:*", new List<string>() { "*" }, "").docs.Select(x => x.CastToObject<JsTable>()).ToList().ToDictionary(x => x.id, y => y);
                //_temp.Save(new Users() { id = Id.UsersAccount.Developer , UserId ="Developer"},FieldProperty.ClassID, _temp.FieldIdDic.Select(x => x.Value).ToList());


                _temp.FilesPath = _dhmasterConfig.FilePath;
                _temp.FilesUploadUrl = _dhmasterConfig.FilesUploadUrl;
                _temp.FilesDownloadUrl = _dhmasterConfig.FilesDownloadUrl;





                if (_temp.FieldIdDic.Any() || _temp.TableIdDic.Any())
                {
                    return _temp;
                }
            }
            catch(Exception ex)
            {

            }

            return _temp;

            
        }

        private Dictionary<string, Dictionary<string, string>> LoadLanguagesDic()
        {
            //LanguageId-> Keywords->Display
            var languageDic = new Dictionary<string, Dictionary<string, string>>();
            //Get Chinese and English...
            var results = _dataStructure.Query(Language.ClassID, null, new List<string>() { FieldNames.id }, start: 0,limit: R.SystemSettings.PageSize,strict: false);
            List<string> languageslist = new List<string>();
            foreach (var column in results.docs)
            {
                languageslist.Add(column[FieldNames.id].ToString());
            }
          
            List<string> keywordsListShow = new List<string>() {  KeyWords.FieldsId.KeyWord };
            keywordsListShow.AddRange(languageslist);
            var keywordlist= _dataStructure.Query(KeyWords.ClassID, showResult: keywordsListShow, start: 0,limit: R.SystemSettings.PageSize,strict:false);

            foreach (var row in keywordlist.docs)
            {
                if (row.ContainsKey(KeyWords.FieldsId.KeyWord))
                {
                    foreach (var language in languageslist)
                    {
                        if (!languageDic.ContainsKey(language))
                        {
                            languageDic[language] = new Dictionary<string, string>();
                        }
                        if(row.ContainsKey(language))
                        {
                            languageDic[language][row[KeyWords.FieldsId.KeyWord].ToString()] = row[language].ToString();
                        }
                

                    }
                }              

            }

            return languageDic;
        }

  
       

   

        private static object CreateInstance(string tableName)
        {
            object temp = null;
            switch (tableName)
            {
                case R.ClassName.TableProperty:
                    temp = new TableProperty();
                    break;
                case R.ClassName.TableProperty + "_" + R.ClassName.FieldProperty:
                    temp = new FieldProperty();
                    break;
                case R.ClassName.MenuProperty:
                    temp = new MenuProperty();
                    break;

            }

            return temp;
        }

        private void UpdteNewFieldLanguage(DataStructure ds, Users user,List<BaseProperty> changeproperty )
        {
        
            List<Dictionary<string, object>> updatedList = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> languageRecords = ds.Query(Language.ClassID,start:0,limit:R.SystemSettings.PageSize,strict:false).docs;
            if (languageRecords.Count == 0)
            {
                languageRecords.Add(new Dictionary<string, object>() { { "Id", KeyWords.FieldsId.English }, { "Code", "English" }, { "Description", "English" }, { "AccessRight", Id.Role.Admin } });
                languageRecords.Add(new Dictionary<string, object>() { { "Id", KeyWords.FieldsId.Chinese }, { "Code", "Chinese" }, { "Description", "Chinese" }, { "AccessRight", Id.Role.Admin } });

            }

            Dictionary<string,string> languageList = languageRecords.Select(x => new KeyValuePair<string,string>(x["Code"].ToString(), x[FieldNames.id.ToString()].ToString())).ToDictionary(x=> x.Key, y=> y.Value );
            foreach (var tablerecord in changeproperty)
            {
                var tablelanguages = new Dictionary<string, object>();
                tablelanguages.Add(FieldNames.id.ToString(), "");
                tablelanguages.Add(FieldNames.ParentId.ToString(), "#");
                tablelanguages.Add(FieldNames.DateVersion.ToString(), "");
                tablelanguages.Add(FieldNames.ChangedBy.ToString(), user.id);
                tablelanguages.Add(FieldNames.AccessRight.ToString(),Access.GetAccess(Id.Role.Admin,new List<string>() { "View","Modify","Delete" }));
                tablelanguages.Add("KeyWord", tablerecord.Name);
                bool newRecord = false;
                foreach (var language in languageList)
                {
                    string displayText = tablerecord.Name;
                    if (!_keyWords.Languages.ContainsKey(language.Value))
                    {
                        _keyWords.Languages.Add(language.Value, new Dictionary<string, string>());
                    }
                    if (!_keyWords.Languages[language.Value].ContainsKey(tablerecord.Name))
                    {
                        _keyWords.Languages[language.Value].Add(tablerecord.Name, displayText);
                        newRecord = true;
                        tablelanguages.Add(language.Key, displayText);
                    }
                    else
                    {
                        tablelanguages.Add(language.Key, _keyWords.Languages[language.Value][tablerecord.Name]);
                    }
                }

                if (newRecord)
                {                    
                    updatedList.Add(tablelanguages);
                }
            }
            ds.Save(user, KeyWords.ClassID, updatedList);

        }



        #endregion

        private void PublishConfigChange()
        {

            Mediator.Tell(new Publish(Topic.ConfigChange.ToString(),new MessageConfig(_dataStructure, _keyWords)));
        }
        private void Publish(Topic topic ,object message)
        {
            Mediator.Tell(new Publish(topic.ToString(), message));
        }

        public void Handle(MessageRequest request)
        {
            try
            {
                mutex.WaitOne();
                switch (request.RequestType)
                {
                    case Request.DataConfig:
                        Sender.Tell(new MessageConfig(_dataStructure, _keyWords) );
                        break;                
                    case Request.LanguageDic:
                        {
                            Sender.Tell(_keyWords.Languages);
                        }
                        break;        
                    case Request.UpdateLanguage:
                        {
                            Users user = new Users() {id = Id.UsersAccount.System,UserId = "System" ,UserName = "System"};

                            //UpdateLanguage(_dataStructure, user);

                        }
                        break;
                    case Request.UpdateKeyWords:
                        {
                            _keyWords.Languages = LoadLanguagesDic();
                            Sender.Tell(new MessageConfig(_dataStructure, _keyWords) );
                            foreach(IActorRef act in webServerList)
                            {
                                act.Tell(_keyWords.Languages);
                            }                            
                          
                        }
                        break;
                    default:
                        Mediator.Tell(new Send("/user/" + R.ActorName.DataHandlerActor, request), Sender);
                        break;
                }
            }
            catch (Exception ex)
            {

                Sender.Tell(new MessageFeedBack() { ValidToken = true, Error = true, Message = ex.ToString(), TimeOut = false });
                LogHandler.Error(ex);
            }
            finally
            {
                mutex.ReleaseMutex();
            }

        }


        public void Handle(SubscribeAck ack)
        {
            Console.WriteLine("Actor [{0}] has subscribed to topic [{1}]", ack.Subscribe.Ref, ack.Subscribe.Topic);
        }

        public void Handle(MessageConfig message)
        {
            mutex.WaitOne();
            if (!Sender.Equals(Self))
            {
                LogHandler.Debug("DataHandlerMaster Message Config Received");
                _dataStructure = message.DataStructure;
            
                LogHandler.Debug("DataHandlerMaster Rebuild Config");
                _keyWords = message.KeyWords;    
                foreach(var webactor in webServerList)
                {
                    webactor.Tell(message);
                }
            }
            mutex.ReleaseMutex();
        }

        public void Handle(object message)
        {
            LogHandler.Debug("Forward to Datahandler");
            //Distributed
            Mediator.Tell(new Send("/user/" + R.ActorName.DataHandlerActor, message), Sender);

            //Signal all
            //Mediator.Tell(new  Publish("/user/" + R.ActorName.DataHandlerActor, message), Sender);

        }        
      

        public void Handle(AddNewLanguageKey message)
        {
            mutex.WaitOne();
            try
            {
                List<Dictionary<string, object>> languageRecords = _dataStructure.Query(Language.ClassID, new Dictionary<string, object>(), null, null, start:0,limit: R.SystemSettings.PageSize, strict: false).docs;
                if (languageRecords.Count == 0)
                {
                    languageRecords.Add(new Dictionary<string, object>() { {FieldNames.id, KeyWords.FieldsId.English }, { Language.FieldsId.Code, "English" }, { Language.FieldsId.Description, "English" }, { Language.FieldsId.AccessRight, Id.Role.Admin } });
                    languageRecords.Add(new Dictionary<string, object>() { { FieldNames.id, KeyWords.FieldsId.Chinese }, { Language.FieldsId.Code, "Chinese" }, { Language.FieldsId.Description, "Chinese" }, { Language.FieldsId.AccessRight, Id.Role.Admin } });

                }
                Dictionary<string, object> languageList = languageRecords.ToDictionary(x => x[Language.FieldsId.Code].ToString(), y => y[FieldNames.id]);
                Dictionary<string, Dictionary<string, string>> addedDic = new Dictionary<string, Dictionary<string, string>>();
                //Fill In other key language
                List<string> keywordslist = new List<string>();
                foreach (var language in languageList)
                {
                    if (!message.Language.ContainsKey(language.Value.ToString()))
                    {
                        continue;
                    }
                    foreach (var keyword in message.Language[language.Value.ToString()])
                    {
                        if (!_keyWords.Languages.ContainsKey(language.Value.ToString()))
                        {
                            _keyWords.Languages.Add(language.Value.ToString(), new Dictionary<string, string>());

                        }
                        string key = KeyWordsDic.GetKey(keyword.Key);
                        if (!_keyWords.Languages[language.Value.ToString()].ContainsKey(key))
                        {
                            if (!addedDic.ContainsKey(language.Key))
                            {
                                addedDic.Add(language.Value.ToString(), new Dictionary<string, string>());
                            }
                            _keyWords.Languages[language.Value.ToString()].Add(key, keyword.Value);
                            addedDic[language.Value.ToString()].Add(key, keyword.Value);
                            keywordslist.Add(key);
                        }
                    }

                }
                keywordslist = keywordslist.Distinct().ToList();
                List<Dictionary<string, object>> recordlist = new List<Dictionary<string, object>>();
                foreach (var keyword in keywordslist)
                {
                    Dictionary<string, object> updatevalue = new Dictionary<string, object>();


                    updatevalue[KeyWords.FieldsId.KeyWord] = keyword;
                    updatevalue[KeyWords.FieldsId.AccessRight] = Access.GetAccess(new List<string>() { Id.Role.Admin,Id.Role.Developer }, "Modify");

                    foreach (var language in languageList)
                    {

                        if (addedDic.ContainsKey(language.Value.ToString()) && addedDic[language.Value.ToString()].ContainsKey(keyword))
                        {
                            updatevalue[language.Value.ToString()] = addedDic[language.Value.ToString()][keyword];
                        }
                        else
                        {
                            updatevalue[language.Value.ToString()] = keyword;
                        }

                    }
                    if(!updatevalue.ContainsKey("id"))
                    {
                        updatevalue["id"] = Unique.NewGuid();
                    }
                    recordlist.Add(updatevalue);

                }
                //if (addedDic.ContainsKey("English"))
                //{
                //    throw new Exception("addedDic Contain English");
                //}
                if (addedDic.Any() )
                {
                    _dataStructure.Save(_dataStructure.GetUserById(Id.UsersAccount.System), KeyWords.ClassID, recordlist);
                    var newchange = new AddNewLanguageKey() { Language = addedDic };
                    foreach(var x  in addedDic)
                    {
                        foreach(var y in x.Value)
                        {
                            if(y.Value.Length > 30)
                            {
                                throw new Exception("Keyword lenght More than 30");
                            }
                        }
                    }
                    Mediator.Tell(new Publish(Topic.ConfigChange.ToString(), newchange));
                    foreach (IActorRef act in webServerList)
                    {
                        act.Tell(newchange);
                    }
                }
            }
            catch(Exception ex)
            {
                LogHandler.Debug(ex.ToString());
            }
            
            mutex.ReleaseMutex();
           
        }

        public void Handle(string message)
        {
            LogHandler.Debug(message);
        }
   
        public void Handle(R.Event message)
        {
            mutex.WaitOne();
            switch(message)
            {
                case R.Event.WebServerConnect:
                    webServerList.Add(Sender);
                    Sender.Tell(new MessageConfig(_dataStructure, _keyWords) );
                    //Context.Watch(Sender);
                    break;
            }
         
            mutex.ReleaseMutex();
       
        }

        public void Handle(Terminated message)
        {
            mutex.WaitOne();
            long uid = message.ActorRef.Path.Uid;
            var removelist = webServerList.Where(x => x.Path.Uid.Equals(uid)).ToList();
            foreach(var x in removelist)
            {
                webServerList.Remove(x);
            }
            mutex.ReleaseMutex();

        }
    }
}