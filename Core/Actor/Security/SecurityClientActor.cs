﻿using Akka.Actor;
using Akka.Event;
using System.Text;
using System.Timers;
using Core;
using Security;
using System.Threading.Tasks;
using Core.Actor;
using System.Threading;
using System;
using Util;
using ClassExtension;
namespace Core.Actor.Security
{
    public enum SecurityRequest
    {
        ConnectVerify,
        Register
    }
    public class SecurityClientActor: ConfirmSendActor,
        IHandle<DeadLetter>

    {
        private ActorSelection Actserver { set; get; }
        private string serverResourceId { set; get; }
        private IActorRef SelfActor { set; get; }
        private System.Timers.Timer tmrConnect = new System.Timers.Timer(4000);
        public static bool ConnectedServer { set; get; }
        public SecurityClientActor(ActorSelection serverActor,string resourceId):base(resourceId)
        {
            ConnectedServer = false;
            SelfActor = Self;
            Actserver = serverActor;
            Context.System.EventStream.Subscribe(Self, typeof(DeadLetter));
            tmrConnect.Elapsed += OnConnect;
            tmrConnect.Enabled = true;
            tmrConnect.Start();
            //Actserver.Tell(new EnumObj() {obj=Request.ConnectVerify, type = Request.ConnectVerify.GetType() },SelfActor);
           
        }

        private void OnConnect(object sender, ElapsedEventArgs e)
        {
            if(Monitor.TryEnter(this))
            {

                if (string.IsNullOrWhiteSpace(serverResourceId))
                {
                    serverResourceId = Connect(Actserver, ResourceId);
            

                }
                if(!string.IsNullOrWhiteSpace(serverResourceId))
                {            
                    Actserver.TellEnum(SecurityRequest.Register, SelfActor);
                    
                    tmrConnect.Stop();
                }
                Monitor.Exit(this);
            }

        }


        public void Handle(DeadLetter message)
        {
            tmrConnect.Start();
        }


    }

    public class SenderInfo
    {
        public IActorRef Sender { set; get; }   
    }
}
