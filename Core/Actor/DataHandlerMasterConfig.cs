﻿using Newtonsoft.Json;
using SolrNetCore;
using System;
using System.Collections.Generic;
using Core;
using Util;

namespace Core.Actor
{
    public class DataHandlerMasterConfig
    {
       

        public DataHandlerMasterConfig()
        {
            ResourceId = Guid.NewGuid().ToString();
            FilePath = AppDomain.CurrentDomain.BaseDirectory + "Files/";
            SolrPath = AppDomain.CurrentDomain.BaseDirectory + "Solr/";
  
            ClusterHostList = new List<string>();
            CertificateValidation = false;
            SolrAuthentication = new SolrAuth();  
                 
            DataHandlerMasterAkkaParam = AkkaHandler.GetParam(port: 7000, hostname: "0.0.0.0",remotehost:"",remoteport:7000);
            DataHandlerAkkaParam = AkkaHandler.GetParam(port: 0, hostname: "0.0.0.0", remotehost: "localhost", remoteport: 7000);

            UploadFolders = new List<string>();
            MinSpaceRequired = 10000000000;
        }
       
        public string ResourceId { set; get; }
        public string SolrPath { set; get; }      
        public List<string> ClusterHostList { set; get; }        
        public string FilesUrl { set; get; }
        public string FilePath { set; get; }
        public bool CertificateValidation { set; get; }
        public SolrAuth SolrAuthentication { set; get; }
        public Dictionary<string,string> DataHandlerMasterAkkaParam { set; get; }
        public Dictionary<string, string> DataHandlerAkkaParam { set; get; }
        public List<string> ClusterList { set; get; }
        [JsonIgnore]
        public string DeployManagerSelector { get { return AkkaHandler.GetSelector(R.SystemName.MainSystem,R.ActorName.DataHandlerMasterActor, DataHandlerMasterAkkaParam); } }
        [JsonIgnore]
        public string FilesUploadUrl { get { return ""; } }
        [JsonIgnore]
        public string FilesDownloadUrl { get { return ""; } }
        [JsonIgnore]
        public string FileDownloadPath { get { return ""; } }
        [JsonIgnore]
        public string FileUploadPath { get { return ""; } }
        [JsonIgnore]
        public string ConfigFile { get { return ""; } }

        public List<string> UploadFolders { get;  set; }
        public long MinSpaceRequired { get;  set; }
    }
}