using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Core.Actor.ComObject;
using Akka.Actor;
using Akka.Cluster;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Util.Internal;
using ComObject;
using DBHandler;
using Newtonsoft.Json;
using RestApi;
using SolrNetCore;
using Core.Akka;
using Core.Akka.Query;
using Util;
using ClassLib;
using Access = ClassLib.Access;
using FieldProperty = TableProperty.FieldProperty;
using Pages = MenuProperty.Pages;
using System.IO.Compression;
using CsvHelper;

using Security;
using Microsoft.CodeAnalysis.Completion;
using Microsoft.CodeAnalysis;
using LogHelper;
using JsonHelper;
using DirectoryHelper;
using ClassExtension;
using NPOI.HSSF.Record;

namespace Core.Actor
{
    public class DataHandler : BaseActor,
        IHandle<MessageRequest>,
        IHandle<Terminated>, 
        IHandle<MessageUpdateOnly>,
        IHandle<MessageInsertOrUpdate>,  
        IHandle<MessageDeleteData>,
        IHandle<ClearData>,
        IHandle<MessageQuery>,       
        IHandle<MessageLogin>,
        IHandle<GetUser>,
        IHandle<MessageQueryForm>,
        IHandle<MessageQueryHistoryTable>,
        IHandle<MessageQueryHistoryItem>,
        IHandle<MessageTableHeader>,
        IHandle<MessageSecurity>,
        IHandle<MessageConfig>,
        IHandle<LookupQuery>,
        IHandle<AddNewLanguageKey>,
        IHandle<CommitTable>,
        IHandle<AutoCompleteQuery>,
        IHandle<QuerySchema>,
        IHandle<QueryMenu>,
        IHandle<QueryTables>,
        IHandle<QueryFields>,
        IHandle<QueryFieldValue>,
        IHandle<QueryPropertyFields>,
        IHandle<AddTable>,
        IHandle<AddSubTable>,
        IHandle<CopyTableTo>,
        IHandle<QueryUniqueListFields>,
        IHandle<AddTableField>,
        IHandle<CopyMenuTo>,
        IHandle<CopyFieldTo>,
        IHandle<DeleteTable>,
        IHandle<DeleteField>,     
        IHandle<AddMenu>,
        IHandle<DeleteMenu>,
        IHandle<MessageGetFieldFilterLookup>,
        IHandle<MessageGetFieldFilterLookupValue>,
        IHandle<MessageBackupScema>,
        IHandle<MessageRestore>,
        IHandle<MessageUpdateEncryptedField>,
        IHandle<MessageViewEncryptedField>,
        IHandle<MessageDownloadCSV>,
        IHandle<MessageUploadCSV>,
        IHandle<RebuildSchema>,
        IHandle<MessageGetAutoComple>,
        IHandle<GetRecordAccessRight>,
        IHandle<LockTable>
    {
        public void Handle(string message)
        {
      
            //CMD.WriteLine(message + " " +Self.Path.Address.ToString());
        }
        private List<string> BaseSchemaList = new List<string>() { FieldProperty.ClassID, TableProperty.ClassID, MenuProperty.ClassID, Pages.ClassID, KeyWords.ClassID };
        //Key(Country Code) : value(Key(Field Name): value( Text)  )
        public readonly Cluster Cluster = Cluster.Get(Context.System);
        public readonly IActorRef Mediator = DistributedPubSub.Get(Context.System).Mediator;
        public IActorRef MainSystem { set; get; }
        private DataHandlerMasterConfig _dhmasterConfig { set; get; }
        private KeyWordsDic _keyWords { set; get; }
        private DBHandler.DataStructure _dataStructure { set; get; }

        public DataHandler(DataHandlerMasterConfig dhMasterConfig,IActorRef mainSystem)
        {
            _dhmasterConfig = dhMasterConfig;

            MainSystem = mainSystem;
            KeyWordsDic.LanguageActor = mainSystem;
             Task task = MainSystem.Ask<MessageConfig>(new MessageRequest() {RequestType = Request.DataConfig}).ContinueWith(t =>
            {
                _dataStructure = t.Result.DataStructure;     
                _keyWords = t.Result.KeyWords;
                _dataStructure.RebuidStructure();
            });
            task.Wait();
        
            Mediator.Tell(new Put(Self));
            Mediator.Tell(new Subscribe(Topic.DataHandler.ToString(), Self));
            Mediator.Tell(new Subscribe(Topic.ConfigChange.ToString(), Self));

           
        }
        public void Handle(MessageRequest request)
        {
            try
            {
                Users user = _dataStructure.GetUserById(request.UserIdentity);
                switch (request.RequestType)
                {
                    case Request.DataConfig:
                        Sender.Tell(new MessageConfig(null, _keyWords));
                        break;             
                      
                    case Request.LanguageDic:
                        {
                            Sender.Tell(_keyWords.Languages);
                        }
                        break;
      
    
                }
            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
                Sender.Tell(new MessageFeedBack() { ValidToken = true, Error = true, Message = ex.ToString(), TimeOut = false });
      
            }
            finally
            {
             
            }

        }


        public void Handle(TerminateRequest message)
        {
            MasterActor = Sender;
            Terminate = true;
             Stop(this.Self);
  
        }

        public void Handle(MessageUpdateOnly message)
        {
            Thread th = new Thread(itemth =>
            {
                ThreadMessage thitem = (ThreadMessage)itemth;
                MessageUpdateOnly item = (MessageUpdateOnly)thitem.Obj;
                try
                {
                    //LogHandler.Debug(item);
                    var user = new Users();
                    if (ValidUser(ref user, item.UserIdentity))
                    {
                        List<Dictionary<string, object>> updateList = new List<Dictionary<string, object>>();
                        var searchTerm = item.SearchTerm.Where(x => !string.IsNullOrWhiteSpace(x.Value.ToString()) && _dataStructure.TableToFieldDic[item.TableId].ContainsKey(x.Key)).ToDictionary(x => x.Key, y => (object)y.Value);

                        string parentid = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.ParentId);
          
                        foreach (var search in searchTerm.Keys.ToArray())
                        {
                            var field = _dataStructure.TableToFieldDic[item.TableId][search];
                            switch (field.ControlType)
                            {
                                case Const.ControlTypes.DatetimePicker:
                                    var datevalue = TypeCheck.CastToList(searchTerm[search]);
                                    if (datevalue.Any(x => !string.IsNullOrWhiteSpace(x)))
                                    {
                                        try
                                        {
                                            // From User Interface, Date selected does not include fff milisecond
                                            //use SystemDateFormatUi instead
                                            datevalue[0] = (!string.IsNullOrWhiteSpace(datevalue[0])) ? DateExtension.ConvertDateFormat(datevalue[0], field.UseUserDateFormat ? user.DateFormat : DateExtension.GetDateFormat(field.Options.Value), DateExtension.SystemDateFormat) : DateExtension.GetPassTimeVersion();

                                            datevalue[1] = (!string.IsNullOrWhiteSpace(datevalue[1])) ? DateExtension.ConvertDateFormat(datevalue[1], field.UseUserDateFormat ? user.DateFormat : DateExtension.GetDateFormat(field.Options.Value), DateExtension.SystemDateFormat, true) : DateExtension.GetFutureTimeVersion();
                                            searchTerm[search] = datevalue;
                                        }
                                        catch (Exception)
                                        {
                                            //Skip for invalid date format search
                                            searchTerm.Remove(search);
                                            continue;

                                        }

                                    }
                                    else
                                    {
                                        searchTerm.Remove(search);
                                    }
                                    break;

                                default:
                                    searchTerm[search] = TypeCheck.CastToList(searchTerm[search]);
                                    break;

                            }

                        }

                        var records = _dataStructure.Query(item.TableId, searchTerm, sortTerm: item.SortTerm, start: item.Start, limit: item.Limit, strict: false).docs;
                        string accessrightId = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.AccessRight);
                        foreach (var record in records)
                        {
                            Dictionary<string, object> update = new Dictionary<string, object>();
                            List<string> accessrow = _dataStructure.GetAccessRight(record[accessrightId].ToString(), user.id, user.UserGroups);
                            switch (item.TableId)
                            {
                                case TableProperty.ClassID:
                                case FieldProperty.ClassID:
                                    
                                    if (update.ContainsKey(parentid) && _dataStructure.TableIdDic.ContainsKey(update[parentid].ToString()) && _dataStructure.IsTableLock(update[parentid].ToString()))
                                    {
                                        continue;
                                    }
                                    else if(record.ContainsKey(parentid) && _dataStructure.TableIdDic.ContainsKey(record[parentid].ToString()) && _dataStructure.IsTableLock(record[parentid].ToString()))
                                    {
                                        continue;
                                    }
                                    break;
                            }
                            //Table Row Access Check
                            if (!_dataStructure.TableIdDic[item.TableId].GetAccess(user.UserGroups, user.id).Contains("Modify") || !accessrow.Contains("Modify") && user.id != Id.UsersAccount.Developer)
                            {
                                continue;
                            }
       
                            var columns = item.Data.Keys.Where(x => !x.Equals(FieldNames.id) && _dataStructure.GetAccessMode(item.TableId, user, x).Contains("Modify")).ToList();
                            foreach (var columnupdate in columns)
                            {
                                switch (_dataStructure.FieldIdDic[columnupdate].ControlType)
                                {
                                    case Const.ControlTypes.DatetimePicker:
                                        if (_dataStructure.TableToFieldDic[item.TableId][columnupdate].UseUserDateFormat)
                                        {
                                            update[columnupdate] = DateExtension.ConvertDateFormat(item.Data[columnupdate].ToString(), user.DateFormat, DateExtension.SystemDateFormat);
                                        }
                                        else
                                        {
                                            update[columnupdate] = DateExtension.ConvertDateFormat(item.Data[columnupdate].ToString(), DateExtension.GetDateFormat(_dataStructure.TableToFieldDic[item.TableId][columnupdate].Options.Value)
                                                , DateExtension.SystemDateFormat);
                                        }
                                        break;
                                    case Const.ControlTypes.EncrytText:
                                        EncryptText(user, message.TableId, columnupdate, record["id"].ToString(), item.Data[columnupdate].ToString());
                                        break;
                                    default:
                                        if (TypeCheck.IsMultiValue(item.Data[columnupdate]))
                                        {
                                            update[columnupdate] = TypeCheck.CastToList(item.Data[columnupdate]);
                                        }
                                        else
                                        {
                                            update[columnupdate] = item.Data[columnupdate];
                                        }
                                        break;

                                }

                            }
                            if (item.Data.ContainsKey(TableProperty.FieldProperty.FieldsId.DefaultValues))
                            {
                                if (!(item.Data[TableProperty.FieldProperty.FieldsId.DefaultValues] is string))
                                {
                                    update[TableProperty.FieldProperty.FieldsId.DefaultValues] = JsonConvert.SerializeObject(item.Data[TableProperty.FieldProperty.FieldsId.DefaultValues]);
                                }
                            }
                            if (item.Data.ContainsKey("id"))
                            {
                                update["id"] = item.Data["id"];
                            }
                            else
                            {
                                update["id"] = record["id"];
                            }

                            if (!update.ContainsKey(parentid))
                            {
                                update[parentid] = record[parentid];
                            }

                            updateList.Add(update);

                            //message.Data
                            //CMD.WriteLine(message);
                        }
                        LogHandler.Debug($"Update  {updateList.Count}/{records.Count}");
                        int updatedcnt = _dataStructure.Save(user, item.TableId, updateList);
                        int acceessDenial = updateList.Count - updatedcnt;

                        CheckAndUpdateSchema(message.TableId, updateList.Select(x => x[FieldNames.id].ToString()).ToList(), user, message.UpdateOrderOnly);
                        thitem.Sender.Tell(new MessageFeedBack { Error = false, ValidToken = true, Message = $"Updated   {updatedcnt}/{records.Count}. Skipped : {records.Count - updatedcnt} due to permission. Rebuild Index required for any changes on DataType and Multi Value." });
                    }
                    else
                    {
                        thitem.Sender.Tell(new MessageFeedBack { ValidToken = false });
                    }
                }
                catch (Exception ex)
                {
                    LogHandler.Error(ex);
                    thitem.Sender.Tell(new MessageFeedBack { ValidToken = true, Error = true, Message = ex.ToString() });
                }
            });
            ThreadMessage thm = new ThreadMessage(this.Sender, this.Self, message);
            th.Start(thm);


        }

        public void Handle(MessageInsertOrUpdate message)
        {
            try
            {
                //LogHandler.Debug(message);
                var user = new Users();
               
                if (ValidUser(ref user, message.UserIdentity))
                {
                    List<FieldProperty> onlyoneFields = _dataStructure.TableToFieldDic[message.TableId].Where(x => x.Value.OnlyOneEnable && (x.Value.ControlType == Const.ControlTypes.CheckBox.ToString() || x.Value.ControlType == Const.ControlTypes.SwitchButton.ToString())).Select(x => x.Value).ToList();
                    if (onlyoneFields.Any())
                    {
                        //_dataStructure.Save(user, message.TableId,)

                        foreach (var field in onlyoneFields)
                        {
                            List<Dictionary<string, object>> replaceddata = _dataStructure.Query(message.TableId, new Dictionary<string, object>(), new List<string>() { "id", field.id }, sortTerm: new Dictionary<string, string>() { { field.id, "desc" } }, start: 0, strict: false).docs;
                            //Cannot Perform update field Together, due to sorting is different.
                            List<Dictionary<string, object>> updatedonlyFiled = new List<Dictionary<string, object>>();
                            foreach (var row in replaceddata)
                            {
                                row[field.id] = false;
                                updatedonlyFiled.Add(row);
                            }
                            _dataStructure.Save(user, message.TableId, updatedonlyFiled);

                        }
                    }

                    List<Dictionary<string, object>> updateList = new List<Dictionary<string, object>>();
                    List<string> columnRemoved = new List<string>();
                    int tableLockCnt = 0;
                    if (!_dataStructure.TableIdDic[message.TableId].GetAccess(user.UserGroups, user.id).Contains("Modify"))
                    {
                        Sender.Tell(new MessageFeedBack { Error = true, ValidToken = true, Message = $"Updated Fail. you does not have permission to edit record in table {_dataStructure.TableIdDic[message.TableId].Name}" });
                        return;
                    }
                    foreach (var record in message.DataList.ToList())
                    {

                        var columns = record.Keys.Where(x => !x.Equals(FieldNames.id) && !x.Equals(_dataStructure.GetFieldIdByName(message.TableId, FieldNames.ParentId))).ToList();
                        foreach (var columnupdate in columns)
                        {
                            if(!_dataStructure.TableToFieldDic.ContainsKey(message.TableId) || !_dataStructure.TableToFieldDic[message.TableId].ContainsKey(columnupdate))
                            {
                                continue;
                            }
                            List<string> access = _dataStructure.GetAccessMode(message.TableId, user, columnupdate);
                            if (!access.Contains("Modify"))
                            {
                                //Avoid Hacker modify the permission, Just double check again.
                                record.Remove(columnupdate);
                                columnRemoved.Add(_dataStructure.TableToFieldDic[message.TableId][columnupdate].Name);
                            }
                            else
                            {
                                if (_dataStructure.TableToFieldDic[message.TableId][columnupdate].ControlType == Const.ControlTypes.DatetimePicker.ToString())
                                {
                                    if (string.IsNullOrWhiteSpace(record[columnupdate].ToString()))
                                    {
                                        continue;
                                    }
                                    if (
                                        _dataStructure.TableToFieldDic[message.TableId][columnupdate]
                                            .UseUserDateFormat)
                                    {
                                        record[columnupdate] = DateExtension.ConvertDateFormat(record[columnupdate].ToString(), user.DateFormat, DateExtension.SystemDateFormat);
                                    }
                                    else
                                    {
                                        record[columnupdate] = DateExtension.ConvertDateFormat(record[columnupdate].ToString(), DateExtension.GetDateFormat(_dataStructure.TableToFieldDic[message.TableId][columnupdate].Options.Value)
                                            , DateExtension.SystemDateFormat);
                                    }
                                }
                                else
                                {
                                    if (record.ContainsKey(columnupdate) && TypeCheck.IsMultiValue(record[columnupdate]))
                                    {
                                        record[columnupdate] = TypeCheck.CastToList(record[columnupdate]);
                                    }
                                }
                            }
                        }

                        if (record.ContainsKey(TableProperty.FieldProperty.FieldsId.DefaultValues))
                        {
                            if (!(record[TableProperty.FieldProperty.FieldsId.DefaultValues] is string))
                            {
                                record[TableProperty.FieldProperty.FieldsId.DefaultValues] = JsonConvert.SerializeObject(record[TableProperty.FieldProperty.FieldsId.DefaultValues]);
                            }
                        }
                        string parentId = _dataStructure.GetFieldIdByName(message.TableId, FieldNames.ParentId);
                        if (!string.IsNullOrWhiteSpace(message.ParentId))
                        {
                            record[parentId] = message.ParentId;
                        }
                        if (!record.ContainsKey(FieldNames.id) || string.IsNullOrWhiteSpace(record[FieldNames.id].ToString()))
                        {
                            record[FieldNames.id] = Unique.NewGuid();
                        }

                        switch (message.TableId)
                        {
                            case TableProperty.ClassID: 
                            case FieldProperty.ClassID:
                                if (record.ContainsKey(parentId) && _dataStructure.TableIdDic.ContainsKey(record[parentId].ToString()) &&  _dataStructure.IsTableLock(record[parentId].ToString()))
                                {
                                    tableLockCnt++;
                                    continue;
                                }
                                break;
                        }

                        updateList.Add(record);

                        //message.Data
                        //CMD.WriteLine(message);
                    }

                    int updated=  _dataStructure.Save(user, message.TableId, updateList);
                    int denialUpdate = updateList.Count - updated;
                    if(message.DataList.Count == 1)
                    {
                        if(tableLockCnt >= 1)
                        {
                            Sender.Tell(new MessageFeedBack { Error = true, ValidToken = true, Message = $"Update Fail. Table is locked." });
                        }
                        else if(updated == 0)
                        {
                            Sender.Tell(new MessageFeedBack { Error = true, ValidToken = true, Message = $"Update Fail. You do not have permission." });
                        }
                        else
                        {
                            Sender.Tell(new MessageFeedBack { Error = false, ValidToken = true, Message = $"Update Completed. " });
                            CheckAndUpdateSchema(message.TableId, updateList.Select(x => x[FieldNames.id].ToString()).ToList(), user, message.UpdateOrderOnly);
                        }
                     
                    }
                    else
                    {
                        Sender.Tell(new MessageFeedBack { Error = false, ValidToken = true, Message = $"Update Completed. Total Update {updated}/{message.DataList.Count()}. Denial {denialUpdate} records. Table Locked {tableLockCnt} " });
                        CheckAndUpdateSchema(message.TableId, updateList.Select(x => x[FieldNames.id].ToString()).ToList(), user, message.UpdateOrderOnly);
                    }
           
                }
                else
                {
                    Sender.Tell(new MessageFeedBack { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
                Sender.Tell(new MessageFeedBack { ValidToken = true, Error = true, Message = ex.ToString() });
            }
        }
        private void CheckAndUpdateSchema(string tableid,List<string> idlist, Users user, bool updateorderonly)
        {
            if (BaseSchemaList.Contains(tableid) || updateorderonly)
            {
                UpdateSchemaChange(new UpdateSchema() { RecordId = idlist, TableId = tableid, UserIdentity = user.id, UpdateOrderOnly = updateorderonly });
            }
            
        }
        public void Handle(MessageDeleteData message)
        {        
            try
            {
                var user = new Users();
                if (ValidUser(ref user, message.UserIdentity))
                {
                    string accessrightId = _dataStructure.GetFieldIdByName(message.TableId, FieldNames.AccessRight);
                    List<string> deletedId = new List<string>();
                    foreach(var id in message.IdList)
                    {
                        Dictionary<string,object> doc =   _dataStructure.QueryById(message.TableId, id).docs.FirstOrDefault();
                        List<string> accessrow = _dataStructure.GetAccessRight(doc[accessrightId].ToString(), user.id, user.UserGroups);
                        if (!accessrow.Contains("Delete") && user.id != Id.UsersAccount.Developer)
                        {
                            continue;
                        }
                        deletedId.Add(id);
                    }
                    _dataStructure.Delete(message.TableId, deletedId, user.UserId);
                    Sender.Tell(new MessageFeedBack { Error = false, ValidToken = true, Message = $"Deleted {deletedId.Count}/{message.IdList.Count}. Skip {message.IdList.Count - deletedId.Count} due to permission." });
                    CheckAndUpdateSchema(message.TableId, deletedId, user, false);
                }
                else
                {
                    Sender.Tell(new MessageFeedBack { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
                Sender.Tell(new MessageFeedBack { ValidToken = true,Error =true,Message = ex.ToString() });
            }

            
        }

        public void Handle(MessageLogin message)
        {         
            try
            {
               

                Users users = _dataStructure.GetUserByUserId(message.UserIdentity);
                if (users != null)
                {            
                    if(message.SearchTerm[Users.FieldsId.Password] == null)
                    {
                        message.SearchTerm[Users.FieldsId.Password] = "";
                    }
                    //External Login already verify the password. We dont save the password for external login
                    if (users.Password.Equals(SimpleSHA.EncryptOneWay(message.SearchTerm[Users.FieldsId.Password])) || message.ExternalLogin)
                    {
                        InsertLoginSession(users, message);
                        Sender.Tell(users);
                        LogHandler.Debug("Login Success " + users.UserId);
                    }         
                    else
                    {
                        users = _dataStructure.Query(Users.ClassID, new Dictionary<string, object>() { { Users.FieldsId.UserId, Id.UsersAccount.PublisUser } }).docs.FirstOrDefault().CastToObject<Users>();
                        Sender.Tell(users);
                    }
                }
                else
                {
                    LogHandler.Debug("Invalid Password");
                    //CMD.WriteLine(us);
                    Sender.Tell(new Users() {id =Id.UsersAccount.PublisUser });
                }
                //Retrive user id from session                   
                var userrecord =
                    _dataStructure.Query(Users.ClassID,
                        new Dictionary<string, object> { { Users.FieldsId.UserId, message.SearchTerm[Users.FieldsId.UserId] }},start:0,limit:1).docs.FirstOrDefault();
                LogHandler.Debug("Login User");


            }
            catch (Exception ex)
            {
                LogHandler.Debug("Login Exception" + ex);
                LogHandler.Error(ex);
                Sender.Tell(new Users());
            }
            
        }

        public void Handle(GetUser message)
        {
            try
            {
                Users user = _dataStructure.GetUserByUserId(message.UserIdentity);
                if(user == null || string.IsNullOrEmpty(message.UserIdentity))
                {
                    user = _dataStructure.GetUserByUserId(Id.UsersAccount.PublisUser);
                }
                Sender.Tell(user);
            }
            catch (Exception ex)
            {           
                LogHandler.Error(ex);               
            }
        }
        private void InsertLoginSession(Users us, MessageLogin message)
        {
            var loginSession = new Dictionary<string, object>();
            loginSession.Add(FieldNames.id, message.UserIdentity);
            loginSession.Add(LoginSession.FieldsId.SessionTime, DateTime.Now.ToString(DateExtension.SystemDateFormat));
            loginSession.Add(LoginSession.FieldsId.UserId, us.id);
            loginSession.Add(LoginSession.FieldsId.ChangedBy, us.id);
            loginSession.Add(LoginSession.FieldsId.AccessRight,JsonConvert.SerializeObject(new Dictionary<string, List<FieldAccess>>() {  { us.id,new List<FieldAccess>() { FieldAccess.View } }, { Id.Role.Admin, new List<FieldAccess>() { FieldAccess.Modify } } }));
            loginSession.Add(LoginSession.FieldsId.ParentId, "#");
            string TableName = LoginSession.ClassID;
            _dataStructure.Save(us, TableName,
                    new List<Dictionary<string, object>> { loginSession });
            _dataStructure.solrApi.Commit(TableName);

        }
        public void Handle(MessageQuery message)
        {
            Thread th = new Thread(itemth=> {
                ThreadMessage thitem = (ThreadMessage)itemth;
                MessageQuery item =(MessageQuery) thitem.Obj;
                try
                {
                  
                    var user = new Users();       
                    if (ValidUser(ref user, item.UserIdentity))
                    {                         
                        if (_dataStructure.TableToFieldDic.ContainsKey(item.TableId))
                        {
                            var footerDic = _dataStructure.GetFacet(item.TableId);
                            var searchTerm = item.SearchTerm.Where(x => !string.IsNullOrWhiteSpace(x.Value.ToString()) && _dataStructure.TableToFieldDic[item.TableId].ContainsKey(x.Key)).ToDictionary(x => x.Key, y => (object)y.Value);
                            bool strict = item.QueryType == LuceneQuery.PhraseQuery;
                            string accessRightId = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.AccessRight);
                            string parentid = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.ParentId);

                            List<string> roleid = new List<string>();
                            if(user.id != Id.UsersAccount.Developer)
                            {
                                roleid.AddRange(user.UserGroups);
                                roleid.Add(user.id);
                                roleid.Add(Id.Role.Public);
                                searchTerm[accessRightId] = roleid.Select(x => $"*{x}*\\[*(View) OR (Modify)*\\]*").ToList();
                            }


                       
                            foreach (var search in searchTerm.Keys.ToArray())
                            {
                                var field = _dataStructure.TableToFieldDic[item.TableId][search];
                                if (field.ControlType == Const.ControlTypes.DatetimePicker.ToString())
                                {
                                    List<string> datevalue = new List<string>();
                                    if(searchTerm[search] is List<DateTime>)
                                    {
                                        datevalue= ((searchTerm[search] as List<DateTime>)?? new List<DateTime>()).Select(x=> x.ToString(DateExtension.SystemDateFormat)).ToList();
                                        if(!datevalue.Any())
                                        {
                                            searchTerm.Remove(search);
                                        }
                                    }
                                    if (searchTerm[search] is DateTime)
                                    {
                                        string startdate = ((DateTime)searchTerm[search]).ToString(DateExtension.SystemDateFormatStart);
                                        string enddate = ((DateTime)searchTerm[search]).ToString(DateExtension.SystemDateFormatEnd);
                                        datevalue.Add(startdate);
                                        datevalue.Add(enddate);
                                        if (!datevalue.Any())
                                        {
                                            searchTerm.Remove(search);
                                        }
                                    }
                                    else if(searchTerm[search] is List<string>)
                                    {
                                        datevalue = searchTerm[search] as List<string>;
                                        if (datevalue.Any(x => !string.IsNullOrWhiteSpace(x)))
                                        {
                                            try
                                            {
                                                // From User Interface, Date selected does not include fff milisecond
                                                //use SystemDateFormatUi instead
                                                datevalue[0] = (!string.IsNullOrWhiteSpace(datevalue[0])) ? DateExtension.ConvertDateFormat(datevalue[0], field.UseUserDateFormat ? user.DateFormat : DateExtension.GetDateFormat(field.Options.Value), DateExtension.SystemDateFormat) : DateExtension.GetPassTimeVersion();

                                                datevalue[1] = (!string.IsNullOrWhiteSpace(datevalue[1])) ? DateExtension.ConvertDateFormat(datevalue[1], field.UseUserDateFormat ? user.DateFormat : DateExtension.GetDateFormat(field.Options.Value), DateExtension.SystemDateFormat, true) : DateExtension.GetFutureTimeVersion();
                                                searchTerm[search] = datevalue;
                                            }
                                            catch (Exception)
                                            {
                                                //Skip for invalid date format search
                                                searchTerm.Remove(search);
                                                continue;

                                            }

                                        }
                                        else
                                        {
                                            searchTerm.Remove(search);
                                        }
                                    }
                                    
                                }
                                
                            }
                            string Sortspecify = "";
                            //For SortSpecify but only for few field. Could not be many
                            //foreach (var fieldid in item.SortTerm.Keys.ToList())
                            //    {
                            //        if (!_dataStructure.FieldIdDic.ContainsKey(fieldid))
                            //        {
                            //            continue;
                            //        }
                            //        FieldProperty field = _dataStructure.FieldIdDic[fieldid];
                            //        if (TypeCheck.IsLookUp(field.ControlType))
                            //        {
                            //            switch (field.ControlType.ToEnum<ControlTypes>())
                            //            {
                            //                case Const.ControlTypes.CheckList:
                            //                case Const.ControlTypes.ComboBox:
                            //                case Const.ControlTypes.SortableComboBox:
                            //                    Dictionary<string, string> lookupitem = _dataStructure.GetLookupItems(field, "*");

                            //                    //Reference, Order by specify id
                            //                    //q=id:5+OR+id:3+OR+id:10+OR+id:6&bq=id:5^4+id:3^3+id:10^2
                            //                    //https://stackoverflow.com/questions/19813548/is-it-possible-in-solr-to-specify-an-ordering-of-documents
                            //                    List<string> sortboostlist = new List<string>();
                            //                    if (item.SortTerm[fieldid].ToLower().Equals("asc"))
                            //                    {
                            //                        //Index of (item) is reverse order
                            //                        lookupitem = lookupitem.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, y => y.Value);
                            //                    }
                            //                    else
                            //                    {
                            //                        lookupitem = lookupitem.OrderBy(x => x.Value).ToDictionary(x => x.Key, y => y.Value);
                            //                    }
                            //                    List<string> keys = lookupitem.Keys.ToList();
                            //                    for (int x = lookupitem.Keys.Count; x > 0; x--)
                            //                    {
                            //                        sortboostlist.Add($"{fieldid}:{keys[x - 1]}^={x}");
                            //                    }

                            //                    Sortspecify = string.Join("+", sortboostlist);
                            //                    item.SortTerm.Remove(fieldid);
                            //                    LogHandler.WriteLine("Sort : " + Sortspecify);
                            //                    break;
                            //            }
                            //        }
                            //    }

                            List<TableViewField> columnFields = _dataStructure.GetTableViewFields(user, item.TableId);
                            var columnOrder = columnFields.Where(x => ((message.Columns != null && message.Columns.Any() && (message.Columns.Contains(x.FieldId) || message.Columns.Contains("*")) && x.AllowView) 
                            || (x.AllowView && x.Enable)) 
                            || x.FieldName == FieldNames.id ).Select(x => x.FieldId).ToList();

                            List<string> dependencyFields = new List<string>();
                            foreach (var x in _dataStructure.TableToFieldDic[item.TableId])
                            {
                                if (columnOrder.Contains(x.Key) && x.Value != null && x.Value.DependentField != null && x.Value.DependentField.Any())
                                {
                                    dependencyFields.AddRange(x.Value.DependentField.Keys);
                                }
                            }
                            dependencyFields.AddRange(columnOrder);
                            dependencyFields = dependencyFields.Distinct().ToList();

                            var result = _dataStructure.Query(
                                item.TableId,
                                searchTerm,
                                dependencyFields,
                                sortTerm: item.SortTerm,    
                                sortspecify: Sortspecify,
                                start:item.StartIndex,
                                limit:item.PageSize,
                                facet:JsonConvert.SerializeObject(_dataStructure.GetFacet(item.TableId)),
                                strict:strict);
                            List<string> columnLookupColumn = _dataStructure.TableToFieldDic[item.TableId].Where(x => ControlCheck.Islookup(x.Value.ControlType) && columnOrder.Contains(x.Value.id)).Select(x => x.Value.id).ToList();
                            Dictionary<bool, Dictionary<string, Dictionary<string, string>>> lookupTextDic = _dataStructure.GetLookupTextDic(item.TableId, columnLookupColumn, result.docs, item.HostUrl);
                            var records = new List<Dictionary<string, object>>();
                            string buttonId = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.Button);
                            string rowNoId = _dataStructure.GetFieldIdByName(item.TableId, FieldNames.RowNo);

                            int rowcount = 0;
                            Dictionary<string, SubTableProperty> subtable = new Dictionary<string, SubTableProperty>();// _dataStructure.GetSubTablesId(message.TableId,);
                            if(item.RawData)
                            {
                                records = result.docs;
                            }
                            else
                            {
                                foreach (var row in result.docs)
                                {
                                    rowcount++;
                                    Dictionary<string, object> data = new Dictionary<string, object>();

                                    foreach (var col in columnOrder)
                                    {
                                        var dValue = row.ContainsKey(col) ? row[col] : "";
                                        try
                                        {

                                            if (TypeCheck.IsMultiValue(dValue))
                                            {
                                                List<string> values = JsonConvert.DeserializeObject<List<string>>(dValue.ToString());
                                                DataSource ds = _dataStructure.TableToFieldDic[item.TableId][col].Source;
                                                if (ds.KeySource.Any() && ControlCheck.Islookup(_dataStructure.TableToFieldDic[item.TableId][col].ControlType))
                                                {
                                                    switch (_dataStructure.TableToFieldDic[item.TableId][col].ControlType)
                                                    {
                                                        case Const.ControlTypes.RadioButton:
                                                        case Const.ControlTypes.ComboBox:
                                                        case Const.ControlTypes.SortableComboBox:
                                                        case Const.ControlTypes.CheckList:
                                                            {
                                                                data[col] = GetLookupHtml(item.TableId, col, values, lookupTextDic, false);
                                                            }
                                                            break;
                                                        default:
                                                            data[col] = "";
                                                            break;
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                string value = dValue.ToString();
                                                switch (_dataStructure.TableToFieldDic[item.TableId][col].ControlType)
                                                {
                                                    case Const.ControlTypes.Html:
                                                        List<string> templateid = TypeCheck.CastToList(_dataStructure.TableToFieldDic[item.TableId][col].Template);
                                                        if (templateid.Any())
                                                        {
                                                            Template temp = new Template();
                                                            ResponseDetail rd = _dataStructure.QueryById(Template.ClassID, templateid.FirstOrDefault());
                                                            temp = rd.docs.FirstOrDefault().CastToObject<Template>();
                                                            data[col] = temp.GetTemplateText(user,_dataStructure, row["id"].ToString());
                                                            //data[col] = data[col].ToString().Replace("{{tableid}}", item.TableId);
                                                            //data[col] = data[col].ToString().Replace("{{recordid}}", row["id"].ToString());
                                                        }
                                                        else
                                                        {
                                                            data[rowNoId] = (rowcount + item.StartIndex).ToString();
                                                            data[buttonId] = UI.GetTableButtonRows(item.TableId, row["id"].ToString(), false, subtable.Any(), true, true);
                                                        }

                                                        break;
                                                    case Const.ControlTypes.DatetimePicker:
                                                        if (string.IsNullOrWhiteSpace(value))
                                                        {
                                                            data[col] = "";
                                                            continue;
                                                        }
                                                        if (
                                                    _dataStructure.TableToFieldDic[item.TableId][col]
                                                        .UseUserDateFormat)
                                                        {
                                                            data[col] = DateExtension.ConvertDateFormat(value, DateExtension.SystemDateFormat, user.DateFormat);
                                                        }
                                                        else
                                                        {
                                                            data[col] = DateExtension.ConvertDateFormat(value, DateExtension.SystemDateFormat, DateExtension.GetDateFormat(_dataStructure.TableToFieldDic[item.TableId][col].Options.Value));

                                                        }

                                                        break;
                                                    case Const.ControlTypes.ColorPicker:
                                                        data[col] = GetColorHtml(value);
                                                        break;
                                                    case Const.ControlTypes.KeyValueBox:
                                                        {
                                                            
                                                            Dictionary<string, List<string>> obj = value.CastJsonToObject<Dictionary<string, List<string>>>();
                                                            if(obj == null)
                                                            {
                                                                obj = new Dictionary<string, List<string>>();
                                                            }
                                                            Dictionary<string, List<string>> objtemp = new Dictionary<string, List<string>>();
                                                            Dictionary<string, string> keytoKeyText = new Dictionary<string, string>();
                                                            foreach (var x  in obj)
                                                            {
                                                                string keyText = GetLookupText(col, x.Key, lookupTextDic, false);
                                                                objtemp.Add(x.Key, new List<string>());
                                                                keytoKeyText[x.Key] = keyText;
                                                                foreach (var y in x.Value)
                                                                {
                                                                    string valueText = GetLookupText(col, y, lookupTextDic, true);
                                                                    objtemp[x.Key].Add(valueText);
                                                                }
                                                                objtemp[x.Key].Sort();
                                                            }
                                                            obj = objtemp;
                                                            if (obj == null)
                                                            {
                                                                data[col] = "";
                                                                continue;
                                                            }
                                                            List<string> lookupvalue = new List<string>();
                                                            
                                                            foreach (var x in obj)
                                                            {
                                                                //Dictionary<string, List<string>> dependencyValue = new Dictionary<string, List<string>>();
                                                                //foreach (var d in _dataStructure.FieldIdDic[col].DependentField)
                                                                //{
                                                                //    if (row.ContainsKey(d.Key))
                                                                //    {
                                                                //        dependencyValue[d.Key] = row[d.Key].CastToList();
                                                                //    }
                                                                //    else
                                                                //    {
                                                                //        dependencyValue[d.Key] = d.Value;
                                                                //    }
                                                                //}

                                                                lookupvalue.Add("<div class='d-flex flex-row bd-highlight justify-content-between mb-0 border  border-primary'>");
                                                               
                                                                
                                                                if (_dataStructure.FieldIdDic[col].Source.KeySource.Any(x=> x.GetSource() is TableSource) || _dataStructure.FieldIdDic[col].Source.KeySource.Any(x => x.GetSource() is KeyValueSource))
                                                                {
                                                                    //lookupvalue.Add($"<div class='lookupsource' data-iskeysource='true' data-value='{x.Key}' data-fieldid='{col}' ></div>");
                                                                    lookupvalue.Add($"<div >{keytoKeyText[x.Key]}</div>");
                                                                    
                                                                }
                                                                else if (_dataStructure.FieldIdDic[col].Source.KeySource.Any(x => x.GetSource() is ApiSource))
                                                                {
                                                                    ApiSource api = _dataStructure.FieldIdDic[col].Source.KeySource.FirstOrDefault().GetSource() as ApiSource;
                                                                    string url = api != null ? api.Url : "";
                                                                    //lookupvalue.Add($"<div class='apisource' data-apiurl='{url}' data-dependencyvalue='{JsonConvert.SerializeObject(dependencyValue)}' data-iskeysource='true' data-value='{x.Key}' data-fieldid='{col}' ></div>");
                                                                    lookupvalue.Add($"<div  >{keytoKeyText[x.Key]}</div>");
                                                                }

                                                                foreach (var y in x.Value)
                                                                {
                                                                    if (_dataStructure.FieldIdDic[col].Source.ValueSource.Any(x => x.GetSource() is TableSource) || _dataStructure.FieldIdDic[col].Source.KeySource.Any(x => x.GetSource() is KeyValueSource))
                                                                    {
                                                                        //lookupvalue.Add($"<div class='lookupsource p-0 bd-highlig ht' data-iskeysource='false' data-value='{y}' data-fieldid='{col}' ></div>");
                                                                        lookupvalue.Add($"<div class='p-0 bd-highlig ht'>{y}</div>");
                                                                    }
                                                                    else if(_dataStructure.FieldIdDic[col].Source.ValueSource.Any(x => x.GetSource() is ApiSource))
                                                                    {
                                                                        //ApiSource api = _dataStructure.FieldIdDic[col].Source.ValueSource.FirstOrDefault().GetSource() as ApiSource;
                                                                        //string url = api != null ? api.Url : "";
                                                                        //dependencyValue["key"] = new List<string>() { x.Key };
                                                                        //lookupvalue.Add($"<div class='p-0 bd-highlight' data-apiurl='{url}' data-dependencyvalue='{JsonConvert.SerializeObject(dependencyValue)}' data-iskeysource='false' data-value='{y}' data-fieldid='{col}'  >{lookupTextDic[true][col][y]}</div>");
                                                                        lookupvalue.Add($"<div class='p-0 bd-highlight' >{y}</div>");
                                                                    }
                                                                   
                                                                }
                                                                lookupvalue.Add("</div>");
                                                            }
                                                  
                                                            data[col] = string.Join("", lookupvalue);
                                                        }
                                                        break;
                                                    case Const.ControlTypes.SourceEditor:

                                                        string modelid = Unique.NewGuid();
                                                        string modal = @"<div class='modal fade' id='" + modelid + @"' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
  <div class='modal-dialog modal-lg' role='document'>
    <div class='modal-content'>
      <div class='modal-header'>
        <h5 class='modal-title' ></h5>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>
      <div class='modal-body' >
        <textarea style='width:600px;height:600px;' >" + value + @"</textarea>
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
      </div>
    </div>
  </div>
</div>";
                                                        data[col] = $"<button class='btn btn-primary' data-toggle='modal' data-target='#{modelid}' >View</button>{modal}";
                                                        break;
                                                    case Const.ControlTypes.SwitchButton:
                                                        {
                                                            bool ischeck;
                                                            bool.TryParse(value, out ischeck);
                                                            data[col] = UI.SwitchButton(ischeck, "primary", "danger");
                                                        }
                                                        break;
                                                    case Const.ControlTypes.CheckBox:
                                                        {
                                                            bool ischeck;
                                                            bool.TryParse(value, out ischeck);
                                                            data[col] = UI.CheckBox(ischeck, "primary", "danger");
                                                        }
                                                        break;
                                                    case Const.ControlTypes.SummerNote:

                                                        data[col] = ReplaceFormat(value, row);

                                                        data[col] = data[col].ToString().Replace("{{tableid}}", item.TableId);
                                                        data[col] = data[col].ToString().Replace("{{recordid}}", data["id"].ToString());
                                                        break;
                                                    case Const.ControlTypes.EncrytText:
                                                        //tableid, fieldid, recordid
                                                        data[col] = "<i class='far fa-eye' onclick=\"ViewEncryptedField('" + item.TableId + "', '" + col + "', '" + row["id"].ToString() + "');\" style='cursor:pointer;' ></i>";
                                                        break;
                                                    default:
                                                        if (ControlCheck.Islookup(_dataStructure.TableToFieldDic[item.TableId][col].ControlType))
                                                        {
                                                            data[col] = GetLookupHtml(item.TableId, col, new List<string>() { value }, lookupTextDic, false);
                                                        }
                                                        else
                                                        {
                                                            data[col] = value;

                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogHandler.Debug(ex.ToString());
                                            if (!data.ContainsKey((string)col))
                                            {
                                                data.Add((string)col, "");
                                            }

                                        }

                                    }
                                    records.Add(data);
                                }
                            }
                         
                            thitem.Sender.Tell(new MessageQueryReturn
                            {
                                Data = records,
                                StartIndex = item.StartIndex,
                                PageSize = item.PageSize,
                                Total = result.numFound,
                                ValidToken = true,
                                TableType = _dataStructure.TableIdDic[item.TableId].TableType,
                                SubTableList = subtable,

                            }, ActorRefs.Nobody);
                        }

                    }
                    else
                    {
                        thitem.Sender.Tell(new MessageQueryReturn { ValidToken = false });

                    }
                }
                catch (Exception ex)
                {
                    LogHandler.Error(ex);
                    thitem.Sender.Tell(new MessageQueryReturn { ValidToken = true, Error = true, Message = ex.ToString() });
                }
            });
            ThreadMessage thmessage = new ThreadMessage(this.Sender,this.Self,message);
            th.Start(thmessage);
        }

        private bool ValidUser(ref Users user, string userIdentity)
        {
            user = _dataStructure.GetUserById(userIdentity);
            if(user == null || string.IsNullOrEmpty(user.id ))
            {
                return false;
            }
            return true;
        }

        private string ReplaceFormat(string text, Dictionary<string,object> data)
        {
            //string text = "<p><button class=\"btn btn-primary\"><span data-id=\"BJJIBtuGBHvrBBEIBHsItuDCvwCuvCtAAJ\" class=\"m-1 p-1\" style=\"color: blue; border: thick solid blue;\">Text</span></button></p>";
            string pattern = "<span data-id[a-zA-Z\\w=\\\":\\s-;<>]+</span>";
            Regex rgx = new Regex(pattern);

            foreach (Match match in rgx.Matches(text))
            {
                string foundvalue = match.Value;

                foreach(var key in data.Keys)
                {
                    if(foundvalue.Contains("data-id=\""+key+ "\""))
                    {
                        text = text.Replace(foundvalue, data[key].ToString());
                    }
                }
            }
            return text;
        }
        private string GetColorHtml(string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return "";
            }
            return $"<div  style = 'background-color:{value};border-style: solid;border-color:#000000;width:100%;' ><h>{value}</h></div>";
        }
        private string GetLookupText(string field,string value, Dictionary<bool, Dictionary<string, Dictionary<string, string>>> lookupDic, bool isvalueSource = false)
        {
            if(lookupDic.ContainsKey(isvalueSource) 
                && lookupDic[isvalueSource].ContainsKey(field)
               && lookupDic[isvalueSource][field].ContainsKey(value) )
            {
                return lookupDic[isvalueSource][field][value];
            }
            return "";
        }

        private string GetLookupHtml(string tableName,string field, List<string> values, Dictionary<bool, Dictionary<string, Dictionary<string, string>>> lookupDic,bool isvalueSource = false)
        {
            List<string> lookupvalue = new List<string>();
            lookupvalue.Add("<div class='itembox'>");
            foreach (var val in values)
            {
                
                if(lookupDic.ContainsKey(isvalueSource) && lookupDic[isvalueSource].ContainsKey(field) 
                    && lookupDic[isvalueSource][field].ContainsKey(val))
                {
                    lookupvalue.Add("<div>");
                    string v = lookupDic[isvalueSource][field][val];
                    lookupvalue.Add(string.IsNullOrEmpty(v) ? val : v);
                    lookupvalue.Add("</div>");
                }
           
            }
            lookupvalue.Add("</div>");
            return string.Join("", lookupvalue);
        }

      
        public void Handle(MessageQueryForm message)
        {
            var user = new Users();
            var returnForm = new MessageQueryReturnForm();
          
            try
            {
                returnForm.Dependancy = _dataStructure.TableToFieldDic[message.TableId].ToDictionary(x => x.Key, y => y.Value.DependentField);

                returnForm.ValidToken = true;
                if (ValidUser(ref user, message.UserIdentity))
                {
               
                    returnForm.TableNameDisplay = _keyWords.GetWord(user.Language,_dataStructure.TableIdDic[message.TableId].Name);
          
                    var fieldList = _dataStructure.GetAccessColumns(user,message.TableId).Where(x=> (x.Enable && !x.Name.Equals(FieldNames.Button) && !x.Name.Equals(FieldNames.RowNo))).ToList();
          
                    Dictionary<string, object> data = new Dictionary<string, object>();
                    returnForm.SearchTerm = message.SearchTerm;
                    returnForm.Id = Unique.NewGuid();
                    string parentId = _dataStructure.GetFieldIdByName(message.TableId, FieldNames.ParentId);
                    if (!string.IsNullOrEmpty(message.Id) && Validate.IsGuid(message.Id))
                    {
                        returnForm.Id = message.Id;
                        data = _dataStructure.QueryById(message.TableId, message.Id).docs.FirstOrDefault();
                        if (data != null && data.Any() && data.Count > 1)
                        {
                            returnForm.SearchTerm = new Dictionary<string, object>() { { parentId, data.ContainsKey(parentId) ?  data[parentId].ToString(): "#" } } ;
                        }   
                        else
                        {
                            data = new Dictionary<string, object>();
                        }
                    }
  

                    var _tempField = new Dictionary<string, ControlProperty>();
                    returnForm.TableName = message.TableId;
                    returnForm.Fields = new List<ControlProperty>();                
                    if(!fieldList.Any(x=> x.Name.Equals(FieldNames.id)))
                    {
                        FieldProperty f = _dataStructure.TableToFieldDic[message.TableId][FieldNames.id].Clone();
                        f.Visible = false;
                        fieldList.Add(f);
                    }

                    foreach (var field in fieldList)
                    {
                        bool setDefaultValue = false;
                        string columnId = field.Name.Equals(FieldNames.id) ? FieldNames.id : field.id;
                        object valueField = "";
            
                        if(data.ContainsKey(columnId))
                        {
                            if (TypeCheck.IsMultiValue(data[columnId]))
                            {
                                valueField = TypeCheck.CastToList(data[columnId]);
                            }                          
                            else 
                            {
                                valueField = data[columnId].ToString();
                            }
                        } 
                        
                  
                        if (!data.Any() || (!data.ContainsKey(columnId) && _dataStructure.TableToFieldDic[message.TableId].ContainsKey(columnId)))
                        {
                      
                            string value = field.DefaultValues;
                            if(TypeCheck.IsLookUp(_dataStructure.TableToFieldDic[message.TableId][columnId].ControlType) &&
                              !TypeCheck.IsKeyValue(_dataStructure.TableToFieldDic[message.TableId][columnId].ControlType))
                            {
                                if (_dataStructure.TableToFieldDic[message.TableId][columnId].MultipleValue)
                                {
                                    valueField = TypeCheck.CastToList(value);
                                }
                                else
                                {
                                    valueField = TypeCheck.CastToList(value).FirstOrDefault();
                                }
                                if(TypeCheck.CastToList(value).Any())
                                {
                                    setDefaultValue = true;
                                }
                            
                            }
                            else
                            {
                                switch (field.ControlType)
                                {
                                    case Const.ControlTypes.DatetimePicker:
                                        try
                                        {
                                            if(!string.IsNullOrWhiteSpace(value))
                                            {
                                                DateTime dt =  RuntimeCompliler<DateTime>.Eval(new CodeEditor(value).Value);
                                                if(field.UseUserDateFormat)
                                                {
                                                    valueField = dt.ToString(user.DateFormat);
                                                }
                                                else
                                                {
                                                    valueField = dt.ToString(DateExtension.GetDateFormat(field.Options.Value));
                                                }

                                                setDefaultValue = true;
                                            }
                                          
                                        }
                                        catch(Exception ex)
                                        {

                                        }
                                    
                                        break;
                                    default:
                                        valueField = value;
                                        if (TypeCheck.IsMultiValue(valueField))
                                        {
                                            if (_dataStructure.TableToFieldDic[message.TableId][columnId].MultipleValue)
                                            {
                                                valueField = TypeCheck.CastToList(value);
                                            }
                                            else
                                            {
                                                valueField = TypeCheck.CastToList(value).FirstOrDefault();
                                            }
                                            if (TypeCheck.CastToList(value).Any())
                                            {
                                                setDefaultValue = true;
                                            }
                                        }
                                        break;
                                }                            
                            }
                        }
                        if(_dataStructure.FieldIdDic[field.id].Name.Equals(FieldNames.id))
                        {
                            valueField= data.ContainsKey("id") ?  data["id"].ToString(): Unique.NewGuid();
                        }
                        List<string> accessTable = _dataStructure.TableIdDic[message.TableId].GetAccess(user.UserGroups, user.id);
                        List<string> accessField = field.GetAccess(user.UserGroups, user.id);
                        List<string> accessmode = accessTable.Intersect(accessField).ToList();

                        if (columnId.Equals(TableProperty.FieldProperty.FieldsId.DefaultValues))
                        {
                            //Default Value Control depend on the Control Type Selected

                            if(_dataStructure.FieldIdDic[data["id"].ToString()].MultipleValue)
                            {
                                valueField = TypeCheck.CastToList(valueField);
                            }
                            
                            switch(_dataStructure.FieldIdDic[data["id"].ToString()].ControlType)
                            {
                                case Const.ControlTypes.DatetimePicker:
                                    _tempField.Add(columnId, new ControlProperty
                                    {
                                        Name = columnId,
                                        Text = _keyWords.GetWord(user.Language, field.Name),
                                        ControlType = Const.ControlTypes.CodeEditor,
                                        SeqNo = fieldList.IndexOf(field),
                                        HistoryCount = 0,
                                        Value = valueField,
                                        QueryType = _dataStructure.FieldIdDic[data["id"].ToString()].QueryType.ToEnum<QueryType>(),
                                        Source = _dataStructure.FieldIdDic[data["id"].ToString()].Source,
                                        MultipleValue = _dataStructure.FieldIdDic[data["id"].ToString()].MultipleValue,
                                        Options = "datetimevalueeditor",
                                        AccessMode = accessmode,
                                        Visible = field.Visible,
                                        GroupFrom = string.Join(" ",  field.FormGroup),
                                        Hint = field.Hint,
                                        SetDefault = setDefaultValue
                                    });
                                    break;

                                default:
                                    _tempField.Add(columnId, new ControlProperty
                                    {
                                        Name = columnId,
                                        Text = _keyWords.GetWord(user.Language, field.Name),
                                        ControlType = _dataStructure.FieldIdDic[data["id"].ToString()].ControlType,
                                        SeqNo = fieldList.IndexOf(field),
                                        HistoryCount = 0,
                                        Value = valueField,
                                        QueryType = _dataStructure.FieldIdDic[data["id"].ToString()].QueryType.ToEnum<QueryType>(),
                                        Source = _dataStructure.FieldIdDic[data["id"].ToString()].Source,
                                        MultipleValue = _dataStructure.FieldIdDic[data["id"].ToString()].MultipleValue,
                                        Options = JsonConvert.SerializeObject(_dataStructure.FieldIdDic[data["id"].ToString()]),
                                        AccessMode = accessmode,
                                        Visible = field.Visible,
                                        GroupFrom = string.Join(" " ,field.FormGroup),
                                        Hint = field.Hint,
                                        SetDefault = setDefaultValue
                                    });
                                    break;

                               
                            }
          
                        }
                        else
                        {         
                            _tempField.Add(columnId, new ControlProperty
                            {
                                Name = columnId,
                                Text = _keyWords.GetWord(user.Language, field.Name),
                                ControlType = field.ControlType,
                                SeqNo = fieldList.IndexOf(field),
                                HistoryCount = 0,
                                Value = valueField,
                                Source = field.Source,
                                MultipleValue = field.MultipleValue,
                                Options = JsonConvert.SerializeObject(field.Options),
                                AccessMode = accessmode,
                                Visible = field.Visible,
                                GroupFrom = string.Join(" ", field.FormGroup),
                                Hint = field.Hint,
                                SetDefault = setDefaultValue

                            });
                        }
             
                    }

                    if (data != null && data.ContainsKey(FieldNames.id))
                    {
                        foreach(var field in fieldList.Where(x=> x.ControlType != Const.ControlTypes.Html.ToString() && !x.Name.Equals(FieldNames.id)))
                        {
                            var historyrecords = _dataStructure.GetTxCount(message.TableId, field.id, data[FieldNames.id].ToString());
                            _tempField[field.id].HistoryCount = historyrecords;
                            if (data.ContainsKey(field.id) && data[field.id] != null && !data[field.id].Equals("null"))
                            {
                                _tempField[field.id].Value = (field.ControlType == Const.ControlTypes.DatetimePicker.ToString() && !string.IsNullOrWhiteSpace(data[field.id].ToString())) ? DateExtension.ConvertDateFormat(data[field.id].ToString(), DateExtension.SystemDateFormat, field.UseUserDateFormat ? user.DateFormat : DateExtension.GetDateFormat(field.Options.Value)) : _tempField[field.id].Value;
                            }
                        }
                       
                    }                  
                    
                    returnForm.SubTableList = _dataStructure.GetSubTablesId(message.TableId,returnForm.Id);
                    foreach (var table in returnForm.SubTableList)
                    {
                        if(_dataStructure.TableIdDic.ContainsKey(table.Value.TableId) && _dataStructure.TableIdDic[table.Value.TableId].DependentField !=null)
                        {
                            returnForm.Dependancy[table.Key] = _dataStructure.TableIdDic[table.Value.TableId].DependentField;
                        }
                       
                     
                    }

                    returnForm.Fields.AddRange(_tempField.Values);
                    Sender.Tell(returnForm);
                }
                else
                {
                    returnForm.ValidToken = false;
                    Sender.Tell(returnForm);
                }
            }
            catch (Exception ex)
            {
                returnForm.Error = true;
                returnForm.Message = ex.ToString();
              
                LogHandler.Error(ex);
                Sender.Tell(returnForm);
            }
            
        }



        public void Handle(MessageQueryHistoryTable message)
        {
     
            try
            {
                var user = new Users();
                //var json = JsonConvert.SerializeObject(message);
                //CMD.WriteLine(json);
                if (ValidUser(ref user, message.UserIdentity))
                {
                    var mrh = new MessageReturnTableHeader();
                    mrh.HasFooter = _dataStructure.GetFacet(message.TableId).Any();
                    mrh.Header = new List<TableViewField>();
                    string[] columnIds = {TX.FieldsId.FieldId, TX.FieldsId.DateVersion, TX.FieldsId.Value, TX.FieldsId.ChangedBy};
                    foreach (var columnId in columnIds)
                    {                        
                        mrh.Header.Add(new TableViewField() { Id= columnId,AllowView=true, FieldId=columnId, Enable=true, FieldName= _keyWords.GetWord(user.Language, _dataStructure.FieldIdDic[columnId].Name)  });
                    }
                    var fieldDic = new Dictionary<string, string>();
                    foreach (var column in _dataStructure.TableToFieldDic[message.TableId])
                    {
                        if(column.Value.ControlType == Const.ControlTypes.KeyValueBox.ToString() 
                            || column.Value.ControlType == Const.ControlTypes.SourceEditor.ToString())
                        {
                            continue;
                        }
                        fieldDic.Add(column.Key + ";" + message.Id, _keyWords.GetWord(user.Language, column.Value.Name));
                    }
                    mrh.SearchDic = new Dictionary<string, ControlProperty>();                 
                    mrh.SearchDic.Add(TX.FieldsId.FieldId, new ControlProperty
                    {
                        AccessMode = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete },
                        ControlType = Const.ControlTypes.ComboBox,
                        HistoryCount = 0,                       
                        Source = new DataSource(fieldDic),
                        MultipleValue = true,
                        Name = "FieldName",
                        Value = message.FieldId + ";" + message.Id,
                        Text = _keyWords.GetWord(user.Language, "FieldName"),
                        SeqNo = 0
              

                    });
                    mrh.SearchDic.Add(TX.FieldsId.DateVersion, new ControlProperty
                    {
                        AccessMode = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete },
                        ControlType = Const.ControlTypes.DatetimePicker,
                        HistoryCount = 0,              
                        Source = new DataSource(),
                        MultipleValue = false,
                        Name = "DateVersion",
                        Options = "",
                        Text = _keyWords.GetWord(user.Language, "DateVersion") ,
                        SeqNo = 1
    
                    });
                    mrh.SearchDic.Add(TX.FieldsId.Value, new ControlProperty
                    {
                        AccessMode = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete },
                        ControlType = Const.ControlTypes.TextBox,
                        HistoryCount = 0,                     
                        Source = new DataSource(),
                        MultipleValue = false,
                        Name = "Value",
                        Text = _keyWords.GetWord(user.Language, "Value") ,
                        SeqNo = 2
                    });
                    mrh.SearchDic.Add(TX.FieldsId.ChangedBy, new ControlProperty
                    {
                        AccessMode = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete },
                        ControlType = Const.ControlTypes.TextBox,
                        HistoryCount = 0,                 
                        Source = new DataSource(),
                        MultipleValue = false,
                        Name = FieldNames.ChangedBy,
                        Text = _keyWords.GetWord(user.Language, FieldNames.ChangedBy),
                        SeqNo = 3
                    });
                    mrh.NotSortableList = new List<string>();

                    mrh.SubTableList = new Dictionary<string, SubTableProperty>();
                    mrh.TableType = TableTypes.LoadOnScrollTable.ToString();
                    mrh.ValidToken = true;
              
                    Sender.Tell(mrh, ActorRefs.Nobody);
                }
                else
                {
                    Sender.Tell(new MessageReturnTableHeader {ValidToken = false});
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
                
            }
        
        }

        public void Handle(MessageQueryHistoryItem message)
        {
            try
            {
                LogHandler.Debug("MessageQueryHistoryItem");
                LogHandler.Debug(message);
                var user = new Users();            
                if (ValidUser(ref user, message.UserIdentity))
                {
                    var mrh = new MessageReturnHistoryItem();
                    Dictionary<string, string> FieldSelected = new Dictionary<string, string>();
                    Dictionary<string, object> param = new Dictionary<string, object>();

                    foreach (var term in message.SearchTerm)
                    {
                        List<string> values = TypeCheck.CastToList(term.Value);
                        if (values == null)
                        {
                            continue;
                        }
                        if (term.Key == TX.FieldsId.DateVersion)
                        {
                            if (values.Count >= 1 && !string.IsNullOrWhiteSpace(values[0]))
                            {
                                values[0] = DateExtension.ConvertDateFormat(values[0], user.DateFormat, DateExtension.SystemDateFormat);

                            }
                            if (values.Count >= 2 && !string.IsNullOrWhiteSpace(values[1]))
                            {
                                values[1] = DateExtension.ConvertDateFormat(values[1], user.DateFormat, DateExtension.SystemDateFormat, true);

                            }
                           
                            param.Add(term.Key, values);
                        }
                        else if (term.Key == TX.FieldsId.FieldId)
                        {
                            if (values.Count > 0)
                            {
                                var value = values.FirstOrDefault();
                                string[] filter = value.Split(';');                                                                                 
                                param.Add(TX.FieldsId.RecordId, filter[1]);

                            }
                            foreach (var value in values)
                            {
                                string[] filter = value.Split(';');
                                FieldSelected.Add(_dataStructure.FieldIdDic[filter[0]].Name, filter[0]);
                            }

                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(values[0]))
                            {
                                param.Add(term.Key, values[0]);
                            }
                        }
                    }

                    string[] columnIds = { FieldNames.id, TX.FieldsId.FieldId, TX.FieldsId.DateVersion, TX.FieldsId.Value, TX.FieldsId.ChangedBy };
                    mrh.ControlDic = new Dictionary<string, ControlProperty>();
                    foreach (var columnId in columnIds)
                    {
                        string name = columnId == FieldNames.id ? columnId: _dataStructure.FieldIdDic[columnId].Name;
                        mrh.ControlDic.Add(columnId, new ControlProperty
                        {
                            AccessMode = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete },
                            ControlType = Const.ControlTypes.Html,
                            HistoryCount = 0,
                            Source = new DataSource(),
                            MultipleValue = false,
                            Name = name,
                            Text = _keyWords.GetWord(user.Language, name),
                            SeqNo = 0
                        });
                    }
                    Dictionary<string, string> sortdic = new Dictionary<string, string>();
                    List<string> FieldSelectedId = new List<string>();
                    FieldSelectedId = FieldSelected.OrderByDescending(x => x.Key).Select(x => x.Value).ToList();
                    if (message.SortTerm.Any())
                    {
                        foreach (var sort in message.SortTerm.ToDictionary(x => x.Key, y => y.Value))
                        {
                            if (sort.Key == TX.FieldsId.FieldId)
                            {
                                if (sort.Value.ToLower() == "desc")
                                {
                                    FieldSelectedId = FieldSelected.OrderByDescending(x => x.Key).Select(x => x.Value).ToList();
                                }
                                else
                                {
                                    FieldSelectedId = FieldSelected.OrderBy(x => x.Key).Select(x => x.Value).ToList();
                                }
                                message.SortTerm.Remove(sort.Key);
                            }
                            else
                            {
                                sortdic.Add(sort.Key, sort.Value);
                            }
                        }
                    }
                    else
                    {
                        sortdic.Add(TX.FieldsId.DateVersion, " desc");
                    }
                    message.SortTerm = new Dictionary<string, string>();
                    foreach (var sort in sortdic)
                    {
                        message.SortTerm[sort.Key] = sort.Value;
                    }

                    mrh.Data = new List<Dictionary<string, object>>();
                    foreach (var fieldId in FieldSelectedId)
                    {
                        param[TX.FieldsId.FieldId] = fieldId;
                        ResponseDetail dt = _dataStructure.Query(TX.ClassID, param, null, message.SortTerm, start:0,limit :R.SystemSettings.PageSize,strict: false);
                        List<Dictionary<string, object>> lookupData = new List<Dictionary<string, object>>();
                        foreach (var data in dt.docs)
                        {
                            lookupData.Add(new Dictionary<string, object>() { { fieldId, data[TX.FieldsId.Value] } });
                        }
                        Dictionary<bool, Dictionary<string, Dictionary<string, string>>> lookupTextDic = _dataStructure.GetLookupTextDic(message.TableId, new List<string>() { fieldId }, lookupData,message.HostUrl);
                        foreach (var dr in dt.docs)
                        {
                            Dictionary<string, object> temp = new Dictionary<string, object>();

                            foreach (var columnId in columnIds)
                            {
                                switch(columnId)
                                {
                                    case FieldNames.id:
                                        temp.Add(FieldNames.id, dr[columnId].ToString());
                                        break;
                                    case TX.FieldsId.FieldId:
                                        temp.Add(columnId,_dataStructure.FieldIdDic[dr[columnId].ToString()].Name);
                                        break;
                                    case TX.FieldsId.DateVersion:
                                        temp.Add(columnId, DateExtension.ConvertDateFormat(dr[columnId].ToString(), DateExtension.SystemDateFormat, user.DateFormat));
                                        break;
                                    case TX.FieldsId.Value:
                                        switch (_dataStructure.TableToFieldDic[message.TableId][fieldId].ControlType)
                                        {
                                            case Const.ControlTypes.Html:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.EncrytText:
                                            case Const.ControlTypes.TextBox:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.SortableComboBox:
                                            case Const.ControlTypes.ComboBox:

                                                try
                                                {
                                                    string lookup = string.Empty;
                                                    List<string> guidlist = TypeCheck.CastToList(dr[columnId]);
                                                    lookup = GetLookupHtml(message.TableId, fieldId, guidlist, lookupTextDic, false);
                                                    //foreach (var id in guidlist)
                                                    //{
                                                    //    lookup += _dataStructure.GetLookupValue(message.TableName, field, id)+ ", ";
                                                    //}

                                                    temp.Add(columnId, lookup);
                                                }
                                                catch (Exception)
                                                {
                                                    if (dr[columnId].ToString().IndexOf("null", StringComparison.OrdinalIgnoreCase) >= 0)
                                                    {
                                                        temp.Add(columnId, "");
                                                    }
                                                    else
                                                    {
                                                        temp.Add(columnId, dr[columnId].ToString());
                                                    }
                                                }

                                                break;
                                            case Const.ControlTypes.CheckBox:
                                                {
                                                    bool ischecked;
                                                    bool.TryParse(dr[columnId].ToString(), out ischecked);
                                                    temp.Add(columnId, UI.CheckBox(ischecked, "primary", "danger"));
                                                }

                                                break;
                                            case Const.ControlTypes.SwitchButton:
                                                {
                                                    bool ischecked;
                                                    bool.TryParse(dr[columnId].ToString(), out ischecked);
                                                    temp.Add(columnId, UI.SwitchButton(ischecked, "primary", "danger"));
                                                }
                                                break;
                                            case Const.ControlTypes.SummerNote:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.CodeEditor:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.ColorPicker:

                                                temp.Add(columnId, GetColorHtml(dr[columnId].ToString()));

                                                break;
                                            case Const.ControlTypes.DatetimePicker:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.RadioButton:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.InputMask:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.CheckList:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            case Const.ControlTypes.FileUpload:
                                                temp.Add(columnId, dr[columnId].ToString());
                                                break;
                                            default:
                                                throw new ArgumentOutOfRangeException();
                                        }
                                        break;
                                    case TX.FieldsId.ChangedBy:
                                        temp.Add(columnId, _dataStructure.GetTextById(Users.ClassID, Users.FieldsId.UserId, dr[columnId].ToString()));
                                        break;
                                    default:
                                        temp.Add(columnId, dr[columnId].ToString());
                                        break;
                                }                                          
                       
                            }

                            mrh.Data.Add(temp);
                        }
                        mrh.Total = dt.numFound;
                    }

                    mrh.TableType = TableTypes.LoadOnScrollTable.ToString();
                    mrh.ValidToken = true;
                    Sender.Tell(mrh, ActorRefs.Nobody);
                }
                else
                {
                    Sender.Tell(new MessageReturnTableHeader { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
                Sender.Tell(new MessageReturnTableHeader { ValidToken = true,Error=true,Message=ex.ToString() });
            }

        }



        public void Handle(MessageSecurity message)
        {
            try
            {
                var user = new Users();
                var json = JsonConvert.SerializeObject(message);

                if (ValidUser(ref user, message.UserIdentity))
                {
                    var sec = new MessageReturenSecurity();
                    sec.FieldAccessDic = new Dictionary<string, string>();
                    sec.TableToFieldProteryDic = new Dictionary<string, Dictionary<string, FieldProperty>>();
                    if (!string.IsNullOrWhiteSpace(message.roleId))
                    {
                        foreach (var value in Enum.GetValues(typeof(FieldAccess)))
                        {
                            var s = Enum.GetName(typeof(FieldAccess), value);
                            sec.FieldAccessDic.Add(value.ToString(), _keyWords.GetWord(user.Language,s));
                        }
                        foreach (var table in _dataStructure.TableToFieldDic)
                        {
                            foreach (var fieldProperty in table.Value)
                            {
                                fieldProperty.Value.Name = _keyWords.GetWord(user.Language,fieldProperty.Value.Name);
                            }
                        }
                        sec.TableToFieldProteryDic = _dataStructure.TableToFieldDic;
                    }
                    sec.ValidToken = true;
                    Sender.Tell(sec);
                }

                else
                {
                    LogHandler.Debug(message);
                    Sender.Tell(new MessageReturnRequestChilds { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
            }
            
        }


        public void Handle(MessageTableHeader message)
        {
            try
            {
                var user = new Users();
                var json = JsonConvert.SerializeObject(message);
                LogHandler.Debug(json);
                if (ValidUser(ref user, message.UserIdentity))
                {
                    var mrh = new MessageReturnTableHeader();
                    if (!string.IsNullOrWhiteSpace(message.TableId) && _dataStructure.TableToFieldDic.ContainsKey(message.TableId) )
                    {
                        mrh.Header = _dataStructure.GetTableViewFields(user,message.TableId);                     
                        mrh.HasFooter = _dataStructure.GetFacet(message.TableId).Any();                        
                        mrh.NotSortableList = mrh.Header.Where(x => _dataStructure.TableToFieldDic[message.TableId][x.FieldId].ControlType == Const.ControlTypes.Html.ToString()).Select(x=> x.FieldId).ToList();
                        mrh.SubTableList = new Dictionary<string, SubTableProperty>(); // _dataStructure.GetSubTablesId(message.TableId,"" );
                        mrh.TableNameDisplay = _keyWords.GetWord(user.Language, _dataStructure.TableIdDic[message.TableId].Name);
                        mrh.TableType = _dataStructure.TableIdDic[message.TableId].TableType;
                        mrh.LeftColumnFix = _dataStructure.TableIdDic[message.TableId].LeftColumnsFixed;
                        mrh.RightColumnFix = _dataStructure.TableIdDic[message.TableId].RightColumnsFixed;
                        string parentKey = _dataStructure.GetFieldIdByName(message.TableId, FieldNames.ParentId);
                        mrh.SearchTerm = new Dictionary<string, object>(){};     
                        foreach(var s in message.SearchTerm)
                        {
                            mrh.SearchTerm[s.Key] = s.Value.CastToList();
                        }
           
         
                   var _tempField = new Dictionary<string, ControlProperty>();
                        foreach (var header in mrh.Header)
                        {
                            var field = _dataStructure.TableToFieldDic[message.TableId][header.FieldId];
                            _tempField.Add(header.FieldId, new ControlProperty
                            {
                                Name = header.FieldName,
                                Text = header.FieldName,
                                QueryType = field.QueryType == null ? QueryType.TextQuery : field.QueryType.ToEnum<QueryType>(),
                                ControlType = field.ControlType,
                                HistoryCount = 0,
                                Value = "",
                                Source = field.Source ?? new DataSource(),
                                MultipleValue = field.MultipleValue,
                                Options = JsonConvert.SerializeObject(field.Options),
                                AccessMode = field.GetAccess(user.UserGroups, user.id)
                            });

                        }
                        mrh.SearchDic = _tempField.Where(x => x.Value.ControlType != Const.ControlTypes.Html).ToDictionary(x => x.Key, y => y.Value);
                       
                        mrh.ValidToken = true;                        
                        Sender.Tell(mrh);
                    }
                    else
                    {

                        Sender.Tell(new MessageReturnTableHeader { ValidToken = true, Error = true, Message = $"Invalid Table Name {message.TableId}" });
                    }

                }
                else
                {
                   Sender.Tell(new MessageReturnTableHeader { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                //CMD.WriteLine("Login Exception" + ex);
                Sender.Tell(new MessageReturnTableHeader { ValidToken = true,Error=true,Message = ex.ToString() });
            }
            
        }



 

        private void ReportException(Exception ex,Dictionary<string,string> searchTerm)
        {
        
            Sender.Tell(new MessageQueryReturn
            {
                Message = ex.ToString(),
                ValidToken = true,
                Error = true,
            }, ActorRefs.Nobody);
            LogHandler.Error(ex);
        }
        //Return Key1 : FieldName, (Key2: AllRecords function FieldName,Value: process value)


  

        public string DataPath { get; set; }
                

        //Use when upload file,Usually new records or bulk upload
      

        public void Handle(Terminated message)
        {
          
                //CMD.WriteLine("Removed :" + message.ActorRef.Path.Uid);
                //_DataWorkerList.Remove(message.ActorRef.Path.Uid);
                //CMD.WriteLine(_DataWorkerList);
            

        }

    
        private void Save(string filepath,object obj)
        {
            Directory.CreateDirectory(filepath);
            using (StreamWriter sw = new StreamWriter(filepath, false))
            {
                sw.Write(JsonConvert.SerializeObject(obj));
            }
        }

        public void Handle(MessageError message)
        {
            LogHandler.Debug(message.Exception);
        }
        
      
        private void RemapParentIdPlugin(Plugins items, TableProperty node,string replaceId)
        {
            string nodeid = node.id;
            node.id = replaceId;
            foreach (var field in items.FieldList.Where(x => x.ParentId == nodeid))
            {
                field.ParentId = replaceId;
            }
            foreach (TableProperty table in items.TableList.Where(x => x.ParentId == nodeid))
            {
                table.ParentId = replaceId;
                TableProperty orginaltable = _dataStructure.TableIdDic.Where(x => x.Value.ParentId == replaceId && x.Value.Name == table.Name).Select(x => x.Value).FirstOrDefault();
                if (orginaltable != null)
                {
                    RemapParentIdPlugin(items, table, orginaltable.id);
                }
            }
        }

   

        private void SaveFile(string pathFile, string imageString)
        {
            //string image = imageString.Substring(imageString.IndexOf("base64,") + 7);
            var base64Data = Regex.Match(imageString, @"data:(?<application>.+?)/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var binData = Convert.FromBase64String(base64Data);
            DirHelper.CreateDirectory(pathFile);
            File.WriteAllBytes(pathFile, binData);
        }

        public void Handle(MessageConfig message)
        {
            LogHandler.Debug("DataHandler Configuration Received ");
            
            _keyWords = message.KeyWords;       
            _dataStructure = message.DataStructure;
            _dataStructure.RebuidStructure();
            LogHandler.Debug("DataHandler Configuration Completed ");
        }

        public void Handle(LookupQuery message)
        {
            Sender.Tell(_dataStructure.GetLookupItems(message));
        }

        public void Handle(AddNewLanguageKey message)
        {
            
            foreach (var addedkeyword in message.Language)
            {
                if (!_keyWords.Languages.ContainsKey(addedkeyword.Key))
                {
                    _keyWords.Languages.Add(addedkeyword.Key, new Dictionary<string, string>());
                }
                foreach (var word in addedkeyword.Value)
                {
                    _keyWords.Languages[addedkeyword.Key][word.Key] = word.Value;
                }
            }
        }

  

        public void Handle(CommitTable message)
        {
            _dataStructure.solrApi.Commit(message.TableName);
            Sender.Tell(new MessageFeedBack { ValidToken = true, Error = false, Message = "Sync Completed" });
        }

        public void Handle(AutoCompleteQuery message)
        {
            List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
            try
            {
                records = _dataStructure.solrApi.DocQuery.SelectDistinct(message.TableName, message.FieldName, message.Text, message.Rows).docs;
            }
            catch (Exception ex)
            {
                records.Add(new Dictionary<string, object>() { { message.FieldName, ex.ToString()} });
            }
            Sender.Tell(records);
        }

        public void Handle(QuerySchema message)
        {
            Users user = new Users();
    
            try
            {
                if(ValidUser(ref user,message.UserIdentity))
                {
                    bool tableonly = false;
                    if (message.SearchValue.ContainsKey("TableOnly"))
                    {
                        tableonly = message.SearchValue["TableOnly"].ToLower() == "true";
                    }
                    var root = new List<TreeNode>(); root = new List<TreeNode>();
                    if (user == null)
                    {
                        Sender.Tell(root);
                    }
                    else if (message.SearchValue.ContainsKey("ParentId") && message.SearchValue["ParentId"] == "*")
                    {

                        List<TableProperty> tbparentlist = _dataStructure.GetAccessableTableProperty("#", user.UserGroups, user.id).ToList();
                        List<FieldProperty> fplist = new List<FieldProperty>();
                        foreach (var x in tbparentlist.ToList())
                        {
                            fplist.AddRange(_dataStructure.GetAccessableFieldProperty(x.id, user.UserGroups, user.id));
                        }

                        root.AddRange(
                        from fieldpair in
                            tbparentlist
                        let fvalue = fieldpair
                        select new TreeNode
                        {
                            id = fvalue.id,
                            text = fvalue.Name,
                            parent = fvalue.ParentId,
                            type = "tablename",
                            icon = Icon.tableicon,
                        //state = new TreeState() { opened = false, loaded = false, disbled = false, selected = false },
                        //children = true

                    });

                        if (!tableonly)
                        {
                            root.AddRange(
                            from fieldpair in
                             fplist
                            let fvalue = fieldpair
                            select new TreeNode
                            {
                                id = fvalue.id,
                                text = fvalue.Name,
                                parent = fvalue.ParentId,
                                type = "fieldname",
                                icon = Icon.fieldicon,
                          //state = new TreeState() { opened = false, loaded = true, disbled = false, selected = false },
                          //children = false
                      });
                        }


                    }
                    else if (!message.SearchValue.Any() || (message.SearchValue.Where(x => string.IsNullOrEmpty(x.Value)).Any()))
                    {
                        root.AddRange(
                            from fieldpair in
                                _dataStructure.TableIdDic.Where(x => x.Value.Enable && x.Value.ParentId == "#" && x.Value.GetAccess(user.UserGroups, user.id).Any()).OrderBy(x => x.Value.RowNo)
                            let fvalue = fieldpair.Value
                            select new TreeNode
                            {
                                id = fvalue.id,
                                text = fvalue.Name,
                                parent = fvalue.ParentId,
                                type = "tablename",
                                icon = Icon.tableicon,
                                NodeObject = new Dictionary<string, string>() { { "TableId", TableProperty.ClassID }, { "id", fvalue.id }, { "ParentId", fvalue.ParentId },{"IsLock", fvalue.Lock.ToString().ToLower() } },
                            //state = new TreeState() { opened = false, loaded = false },
                            children = true

                            });
                    }
                    else if (message.SearchValue.ContainsKey("ParentId"))
                    {
                        root.AddRange(
                        from fieldpair in
                       _dataStructure.TableIdDic.Where(x => x.Value.Enable && (x.Value.ParentId == message.SearchValue["ParentId"]) && x.Value.HasAccess(user.UserGroups, user.id)).OrderBy(x => x.Value.RowNo)
                        let fvalue = fieldpair.Value
                        select new TreeNode
                        {
                            id = fvalue.id,
                            text = fvalue.Name,
                            parent = fvalue.ParentId,
                            type = "tablename",
                            icon = Icon.tableicon,
                            NodeObject = new Dictionary<string, string>() { { "TableId", TableProperty.ClassID }, { "id", fvalue.id }, { "ParentId", fvalue.ParentId }, { "IsLock", fvalue.Lock.ToString().ToLower() } },
                            state = TreeState.GetTreeState(opened:false, loaded :true, disabled: false, selected :false ),
                            children = true

                        });
                        if (!tableonly)
                        {
                            root.AddRange(
                      from fieldpair in
                     _dataStructure.FieldIdDic.Where(x => x.Value.Enable && x.Value.ParentId == message.SearchValue["ParentId"] && x.Value.HasAccess(user.UserGroups, user.id)).OrderBy(x => x.Value.RowNo)
                      let fvalue = fieldpair.Value
                      select new TreeNode
                      {
                          id = fvalue.id,
                          text = fvalue.Name,
                          parent = fvalue.ParentId,
                          type = "fieldname",
                          icon = Icon.fieldicon,
                          NodeObject = new Dictionary<string, string>() { { "TableId", TableProperty.FieldProperty.ClassID }, { "id", fvalue.id }, { "ParentId", fvalue.ParentId },{ "IsLock",_dataStructure.TableIdDic[ fvalue.ParentId].Lock.ToString().ToLower() } },
                          state = TreeState.GetTreeState(opened :false, loaded:true),
                          children = false

                      });
                        }

                    }
                    else if (message.SearchValue.ContainsKey("str"))
                    {
                        List<string> idlist = _dataStructure.GetFullLevelIdByName(message.SearchValue["str"], user).Distinct().ToList();
                        List<string> idlistRoot = idlist.ToList();
                        idlistRoot = idlistRoot.Distinct().ToList();
                        foreach (string id in idlistRoot)
                        {
                            
                            if (_dataStructure.TableIdDic.ContainsKey(id) && !idlistRoot.Contains(_dataStructure.TableIdDic[id].ParentId) && _dataStructure.TableIdDic[id].ParentId != "#")
                            {//Remove it for no Parent Id
                                idlist.Remove(id);
                                continue;
                            }
                            if (_dataStructure.FieldIdDic.ContainsKey(id) && !idlistRoot.Contains(_dataStructure.FieldIdDic[id].ParentId) && _dataStructure.FieldIdDic[id].ParentId != "#")
                            {
                                //Remove it for no Parent Id
                                idlist.Remove(id);
                                continue;
                            }
                        }

                        root.AddRange(idlist.Where(x => _dataStructure.TableIdDic.ContainsKey(x)).Select(x =>
                         new TreeNode
                         {
                             id = _dataStructure.TableIdDic[x].id,
                             text = _dataStructure.TableIdDic[x].Name,
                             parent = _dataStructure.TableIdDic[x].ParentId,
                             type = "tablename",
                             icon = Icon.tableicon,
                             NodeObject = new Dictionary<string, string>() { { "TableId", TableProperty.ClassID }, { "id", _dataStructure.TableIdDic[x].id }, { "ParentId", _dataStructure.TableIdDic[x].ParentId }, { "IsLock", _dataStructure.TableIdDic[x].Lock.ToString().ToLower() } },
                             state = TreeState.GetTreeState(opened: true, loaded: true)
                         }));

                        if (!tableonly)
                        {
                            root.AddRange(idlist.Where(x => _dataStructure.FieldIdDic.ContainsKey(x)).Select(x =>
                                  new TreeNode
                                  {
                                      id = _dataStructure.FieldIdDic[x].id,
                                      text = _dataStructure.FieldIdDic[x].Name,
                                      parent = _dataStructure.FieldIdDic[x].ParentId,
                                      type = "fieldname",
                                      icon = Icon.fieldicon,
                                      NodeObject = new Dictionary<string, string>() { { "TableId", TableProperty.FieldProperty.ClassID }, { "id", _dataStructure.FieldIdDic[x].id }, { "ParentId", _dataStructure.FieldIdDic[x].ParentId },{ "IsLock", _dataStructure.TableIdDic[_dataStructure.FieldIdDic[x].ParentId].Lock.ToString().ToLower() } },
                                      state = TreeState.GetTreeState(opened: false, loaded: true)
                                  }));
                        }


                    }
                    Sender.Tell(root);
                }
                else
                {
                    Sender.Tell(new List<TreeNode>());
                }
               
             
            }
            catch(Exception ex)
            {
                LogHandler.Debug(ex.ToString());
                Sender.Tell(new List<TreeNode>());
            }
        }

        //Use By Source Editor
        public void Handle(QueryFields message)
        {        
            try
            {          
                if (!string.IsNullOrWhiteSpace(message.SeachName) && message.SeachName.ToLower().Equals(FieldNames.id))
                {
                    Sender.Tell(new Dictionary<string, string>() { { FieldNames.id, FieldNames.id } });
                }         
                else if (!string.IsNullOrWhiteSpace(message.SeachName) && _dataStructure.FieldIdDic.ContainsKey(message.SeachName))
                {
                    Sender.Tell(new Dictionary<string, string>() { { _dataStructure.FieldIdDic[message.SeachName].id, _dataStructure.FieldIdDic[message.SeachName].Name } });
                }        
                else if (!string.IsNullOrWhiteSpace(message.TableId) &&_dataStructure.TableToFieldDic.ContainsKey(message.TableId))
                {
                    if (string.IsNullOrWhiteSpace(message.SeachName))
                    {
                        message.SeachName = "";
                    }

                    if (message.Unique)
                    {  //Only allow those field have unique so that it can reference
                        Sender.Tell(_dataStructure.TableToFieldDic[message.TableId]
                            .Where(x=>
                                 x.Key == FieldNames.id ||
                                ( x.Value.Name.Contains(message.SeachName)
                               &&
                                (_dataStructure.TableIdDic[message.TableId].UniquesList.Where(z => z.Value.Count == 1 && z.Value.Contains(x.Value.id)).Any()
                              || _dataStructure.TableIdDic[message.TableId].UniquesList.Where(z => z.Value.Count == 2 && z.Value.Contains(_dataStructure.GetFieldIdByName(message.TableId,FieldNames.ParentId)) && z.Value.Contains(x.Value.id)).Any()
                                ))
                            ).OrderBy(x => x.Value.RowNo).ToDictionary(x => x.Value.GetId(), y => y.Value.Name));
                    }
                    else
                    {
                        Sender.Tell(_dataStructure.TableToFieldDic[message.TableId]
                                          .Where(x => x.Value.Name.ToLower().Contains(message.SeachName.ToLower())).ToDictionary(x => x.Value.GetId(), y => y.Value.Name));
                    }
                }
                else
                {
                    Sender.Tell(new Dictionary<string, string>());
                }
            
           
            }
            catch(Exception ex)
            {
                LogHandler.Debug(ex.ToString());
                Sender.Tell(new Dictionary<string, string>() { { "", ex.ToString() } });
            }
        }

        public void Handle(QueryTables message)
        {
            try
            {
                if(_dataStructure.TableIdDic.ContainsKey(message.SearchName))
                {
                    Sender.Tell(new Dictionary<string, string>() { { message.SearchName, _dataStructure.TableIdDic[message.SearchName].Name } });
                }
                else
                {
                    Sender.Tell(_dataStructure.TableIdDic.Where(x => x.Value.Name.ToLower().Contains(message.SearchName.ToLower())).ToDictionary(x => x.Key, y => _dataStructure.GetFullTablename(y.Key)));
                }
               

            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
                Sender.Tell(new Dictionary<string, string>() { { "Error:",ex.ToString()} });
            }
        }

        public void Handle(QueryFieldValue message)
        {
            try
            {
                Users user = _dataStructure.GetUserByUserId(message.UserIdentity);
                Dictionary<string, string> dropdown = new Dictionary<string, string>();
                if (string.IsNullOrWhiteSpace(message.TableId) || !_dataStructure.TableIdDic.ContainsKey(message.TableId))
                {
                    message.TableId = _dataStructure.FieldIdDic[message.FieldId].ParentId;
                }
                if (!_dataStructure.TableIdDic.ContainsKey(message.TableId))
                {
                    Sender.Tell(dropdown);
                    return;
                }
                string tableId = message.TableId; 
                switch(_dataStructure.TableToFieldDic[tableId][message.FieldId].ControlType)
                {
                    case Const.ControlTypes.SwitchButton:
                    case Const.ControlTypes.CheckBox:
                        if(message.SeachName.Contains("true") || message.SeachName.Contains("false"))
                        {
                            foreach(var s in message.SeachName)
                            {
                                dropdown.Add(s, UI.Check(s.ToLower().Equals("true")));
                            }                          
                        }
                        else
                        {
                            dropdown.Add("true", UI.Check(true));
                            dropdown.Add("false", UI.Check(false));
                        }
                 
                        break;
                    case Const.ControlTypes.DatetimePicker:
                        dropdown.Add("0", "Now");
                        for(var x = -30; x <=30; x++)
                        {
                            dropdown.Add(x.ToString(), $"{x} Days");
                        }
                        break;
                    case Const.ControlTypes.CheckList:
                    case Const.ControlTypes.SortableComboBox:
                    case Const.ControlTypes.RadioButton:
                    case Const.ControlTypes.ComboBox:
                       List<SelectSource> datasource = _dataStructure.TableToFieldDic[tableId][message.FieldId].Source.KeySource;
                        if(datasource.Any(x=> x.GetSource() is TableSource))
                        {
                            List<TableSource> tableSource = datasource.Select(x =>x.GetSource() as TableSource).ToList();
                            Self.Tell(new LookupQuery() { UserIdentity = message.UserIdentity, Sources = tableSource, SearchbyId = message.Searchbykey, Searchlist = message.SeachName.ToList(), Occur = LuceneOccur.SHOULD, QueryType = LuceneQuery.PhraseQuery },Sender);
                            return;
                        }
                        else if(datasource.Any(x => x.GetSource() is KeyValueSource))
                        {
                            List<KeyValueSource> keyvaluesource = datasource.Select(x => x.GetSource() as KeyValueSource).ToList();
                            if(message.Searchbykey)
                            {
                                var items = keyvaluesource.FirstOrDefault().Items;
                                foreach(var s in message.SeachName)
                                {
                                    if (items.ContainsKey(s))
                                    {
                                        dropdown = new Dictionary<string, string>() { { s, items[s] } };
                                    }
                                }
                           
                            }
                            else
                            {
                                dropdown = keyvaluesource.FirstOrDefault().Items;
                            }
                                                   
                        }
                        else if(datasource.Any(x => x.GetSource() is string) || datasource.Any(x => x.GetSource() is ApiSource))
                        {
                           object source = datasource.Where(x => x != null && !string.IsNullOrEmpty(x.GetSource().ToString())).FirstOrDefault().GetSource();
                            string Url = "";
                            if(source is ApiSource)
                            {
                                ApiSource apisource = (ApiSource)source;
                                Url = apisource.Url;
                            }
                            else
                            {
                                Url = source.ToString();
                            }
                            if(!string.IsNullOrWhiteSpace(Url))
                            {
                                string host = Url;
                                if (!Url.ToLower().Contains("http"))
                                {
                                    host = message.HostUrl + Url;
                                }
                            

                                RestApiResponse msr = ServerApi.Post(host,new ApiSelectArgument() { search = message.SeachName, searchbykey = message.Searchbykey , dependent="{}"});
                                Task<string> result = msr.Result.Content.ReadAsStringAsync();
                                List<Dictionary<string,string>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result.Result);
                                dropdown = temp.ToDictionary(x=> x["id"], y=> y["text"]);

                            }
                        }               
                       
                        break;
                    case Const.ControlTypes.KeyValueBox:

                        break;
                    case Const.ControlTypes.InputMask:
                    case Const.ControlTypes.TextBox:
                    case Const.ControlTypes.ColorPicker:
                        {
                            List<Dictionary<string, object>> items = _dataStructure.Query(tableId, new Dictionary<string, object>() { { message.Searchbykey? "id" : message.FieldId, message.SeachName } }, new List<string>() { "id", message.FieldId }).docs;
                            foreach (Dictionary<string, object> i in items)
                            {
                                if (i.ContainsKey(message.FieldId) && i.ContainsKey("id"))
                                {
                                    dropdown[i["id"].ToString()] = i[message.FieldId].ToString();
                                }
                            }
                        }      
                        break;
                    default:
                        //dropdown.Add("NA", "N/A");
                        break;
                }

                Sender.Tell(dropdown);
            }
            catch (Exception ex)
            {
                Sender.Tell(new Dictionary<string,string>());
            }
        }

        public void Handle(QueryPropertyFields message)
        {
            try
            {
                //LogHandler.Debug(message);
                Users user = _dataStructure.GetUserByUserId(message.UserIdentity);
                if (string.IsNullOrWhiteSpace(message.TableName) && (string.IsNullOrWhiteSpace(message.FieldId) ||  !_dataStructure.FieldIdDic.ContainsKey(message.FieldId)))
                {                    
                    Sender.Tell(new Dictionary<string,string>());
                    return;
                }
                else if(string.IsNullOrWhiteSpace(message.TableName) || !_dataStructure.TableIdDic.ContainsKey(message.TableName))
                {
                    message.TableName = _dataStructure.FieldIdDic[message.FieldId].ParentId;
                }
                string tableId = message.TableName;
              
                if(message.SearchByKey && _dataStructure.TableToFieldDic[message.TableName].ContainsKey(message.SeachName))
                {
                    Sender.Tell(new Dictionary<string, string>() { { _dataStructure.TableToFieldDic[message.TableName][message.SeachName].GetId(), _dataStructure.TableToFieldDic[message.TableName][message.SeachName].Name } });
                }
                else  if (_dataStructure.TableToFieldDic.ContainsKey(tableId))
                {
                    Sender.Tell(_dataStructure.TableToFieldDic[tableId].Where(x => _keyWords.GetWord(user.Language, x.Value.Name).ToLower().Contains(message.SeachName.ToLower())).ToDictionary(x => x.Value.GetId(), y => y.Value.Name));
                }

            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
            }
        }

        public void Handle(AddTable message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
                    if (_dataStructure.IsTableLock(message.SelectedTable.ParentId))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail add table. The table is lock.", ValidToken = true });
                        return;
                    }
                    TableProperty table = _dataStructure.CreateTableProperty(Unique.NewGuid(), "New Table", message.SelectedTable.ParentId, true, 0,  TableTypes.PaginationTable.ToString());
                    table.UniquesList = new Dictionary<string, List<string>>();
                    table.UniquesList.Add("Primary_Map", new List<string>() { TableProperty.FieldsId.id });
                    table.DateVersion = DateTime.Now;
                    table.ChangedBy = user.id;
                    table.AccessRight[user.id] = new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete };
                    _dataStructure.TableIdDic[table.id] = table;              
                    _dataStructure.AddBaseFields(table.id);
                    _dataStructure.RebuidStructure();
                    List<TableProperty.FieldProperty> updatedField = new List<TableProperty.FieldProperty>();
                    
                    // Add Field to table
                    foreach (var fieldarchive in _dataStructure.TableToFieldDic[table.id])
                    {
                        updatedField.Add(fieldarchive.Value);
                    }
 
                    List<TableProperty> updatedTable = new List<TableProperty>();
                    updatedTable.Add(table);
           

                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });

                    Thread th = new Thread(x=> {
                        UpdateStructure t = (UpdateStructure)x;
                        _dataStructure.SaveDataStructure(t);
                        _dataStructure.UpdateDatabase(t);
                        _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                        _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);
                    });
                    UpdateStructure update = new UpdateStructure();
                    update.FieldList = updatedField;
                    update.TableList = updatedTable;
                    update.ActorStatus = message.StatusSender;
                    update.Id = message.SelectedTable.id;
                    th.Start(update);


                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }  
        }

        public void Handle(AddSubTable message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
                    if (_dataStructure.IsTableLock(message.ParentTable.id))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail add table. The table is lock.", ValidToken = true });
                        return;
                    }
                    TableProperty table = _dataStructure.CreateTableProperty(Unique.NewGuid(), "NewTable", message.ParentTable.id, true,  0,  TableTypes.LoadOnScrollTable.ToString());
                    table.UniquesList.Add("Primary_Map", new List<string>() { "id" });
                    table.DateVersion = DateTime.Now;
                    table.ChangedBy = user.id;
                    table.AccessRight[Id.Role.Admin] = new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" };
                    _dataStructure.TableIdDic[table.id] = table;
                
                    _dataStructure.AddBaseFields(table.id);
                    _dataStructure.RebuidStructure();
                    List<TableProperty> updatedTable = new List<TableProperty>();
                    updatedTable.Add(table);
                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });  
                    Thread th = new Thread(x => {
                        UpdateStructure t = (UpdateStructure)x;
                        _dataStructure.SaveDataStructure(t);
                        _dataStructure.UpdateDatabase(t);
                        _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                        _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);
                    });
                    UpdateStructure update = new UpdateStructure();
                    update.FieldList = _dataStructure.TableToFieldDic[table.id].Select(x => x.Value).ToList();
                    update.TableList = new List<TableProperty>() { table };
                    update.ActorStatus = message.StatusSender;
                    update.Id = message.ParentTable.id;
                    th.Start(update);
                }                                         
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }
        public void Handle(CopyFieldTo message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
      
                    List<FieldProperty> updatedField = new List<FieldProperty>();
                    FieldProperty  fp = _dataStructure.FieldIdDic[message.TargetCopyField].Clone();
                    fp.id = Unique.NewGuid();
                    string parentid;
                    if(_dataStructure.TableIdDic.ContainsKey(message.ToId))
                    {
                        parentid = message.ToId;
                    }
                    else 
                    {
                        parentid = _dataStructure.FieldIdDic[message.ToId].ParentId;
                        fp.RowNo = _dataStructure.FieldIdDic[message.ToId].RowNo;
                    }
                    if (_dataStructure.IsTableLock(parentid))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail to Copy. The table is lock.", ValidToken = true });
                        return;
                    }
                    fp.ParentId = parentid;
                    
                    updatedField.Add(fp);
                    _dataStructure.FieldIdDic[fp.id] = fp;
                    _dataStructure.RebuidStructure();                 

                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });
                    Thread th = new Thread(x => {
                        UpdateStructure t = (UpdateStructure)x;
                        _dataStructure.SaveDataStructure(t);
                        _dataStructure.UpdateDatabase(t);
                        _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                        _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);
                    });
                    UpdateStructure update = new UpdateStructure();
                    update.FieldList = updatedField;
                    update.TableList = new List<TableProperty>();
                    update.ActorStatus = message.StatusSender;
                    update.Id = parentid;
                    th.Start(update);
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }
        public void Handle(CopyTableTo message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
         
                    List<TableProperty> updatedTable = new List<TableProperty>();
                    List<FieldProperty> updatedField = new List<FieldProperty>();
                    TableProperty targetTable = _dataStructure.TableIdDic[message.TargetCopyTable].Clone();
                    TableProperty destTable = null;
                    //Expected result, paste on a table where target is Table or Field
                    if(_dataStructure.TableIdDic.ContainsKey(message.ToTable) && _dataStructure.TableIdDic.ContainsKey(_dataStructure.TableIdDic[message.ToTable].ParentId))
                    {                     
                        //Paste on Table Level
                        destTable = _dataStructure.TableIdDic[_dataStructure.TableIdDic[message.ToTable].ParentId];         
                        targetTable.RowNo = _dataStructure.TableIdDic[message.ToTable].RowNo;
                    }
                    else
                    {
                        //It paste on Field level message.ToTable-> Field Id
                        destTable = _dataStructure.FieldIdDic.ContainsKey(message.ToTable) ? _dataStructure.TableIdDic[_dataStructure.FieldIdDic[message.ToTable].ParentId] : null;
             

                    }
                    if (destTable != null &&  _dataStructure.IsTableLock(destTable.id))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail paste on table. The table is lock.", ValidToken = true });
                        return;
                    }
                    if (destTable == null && _dataStructure.TableIdDic.ContainsKey(message.ToTable))
                    {
                        targetTable.RowNo = _dataStructure.TableIdDic[message.ToTable].RowNo;
                    }
                    _dataStructure.Copy(targetTable, destTable, ref updatedTable, ref updatedField);
                    foreach(var x in updatedTable)
                    {
                        _dataStructure.TableIdDic[x.id] = x;
                    }
                    foreach(var x in updatedField)
                    {
                        _dataStructure.FieldIdDic[x.id] = x;
                    }
                    _dataStructure.RebuidStructure();
                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });
                    Thread th = new Thread(x => {
                        UpdateStructure t = (UpdateStructure)x;
                        _dataStructure.SaveDataStructure(t);
                        _dataStructure.UpdateDatabase(t);
                        _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                        _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);
                    });
                    UpdateStructure update = new UpdateStructure();
                    update.FieldList = updatedField;
                    update.TableList = updatedTable;
                    update.ActorStatus = message.StatusSender;
                    update.Id = message.ToTable;
                    th.Start(update);
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }
        public void Handle(QueryUniqueListFields message)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(message.TableId) || !_dataStructure.TableIdDic.ContainsKey(message.TableId))
                {
                    Sender.Tell(new Dictionary<string, string>());
                    return;
                }
                Users user = _dataStructure.GetUserByUserId(message.UserIdentity);
                string tableId = message.TableId;
                if(message.SearchByKey)
                {
                    Sender.Tell(_dataStructure.TableToFieldDic[tableId].Where(x => message.SeachName.Any(y=> y.ToLower().Equals(x.Key.ToLower()))).ToDictionary(x => x.Value.id, y => _keyWords.GetWord(user.Language, y.Value.Name)));
                }
                else if (_dataStructure.TableToFieldDic.ContainsKey(tableId))
                {

                    Sender.Tell(_dataStructure.TableToFieldDic[tableId].Where(x => 
                    (x.Value.ControlType == Const.ControlTypes.TextBox.ToString() || x.Value.ControlType == Const.ControlTypes.ColorPicker.ToString()
                    || x.Value.ControlType == Const.ControlTypes.ComboBox.ToString() || x.Value.ControlType == Const.ControlTypes.InputMask.ToString()) &&
                    !x.Value.MultipleValue  && 
                    (message.SeachName.Any(y=> _keyWords.GetWord(user.Language, x.Value.Name).ToLower().Contains(y.ToLower())) || !message.SeachName.Any())).ToDictionary(x => x.Value.id, y => _keyWords.GetWord(user.Language, y.Value.Name)));
                }
                else
                {
                    Sender.Tell(new Dictionary<string,string>());
                }
            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
            }
        }

        public void Handle(AddTableField message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {

                    if ( _dataStructure.IsTableLock(message.Selected.ParentId))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail to add Field. The table is lock.", ValidToken = true });
                        return;
                    }


                    FieldProperty field = _dataStructure.AddField(Unique.NewGuid(),"New Field", message.Selected.ParentId, Const.ControlTypes.TextBox,new DataSource(), false);
                    field.DateVersion = DateTime.Now;
                    field.ChangedBy = user.id;
                    if(_dataStructure.FieldIdDic.ContainsKey(message.Selected.id))
                    {
                        field.RowNo = _dataStructure.FieldIdDic[message.Selected.id].RowNo;
                    }
                    _dataStructure.RebuidStructure();
                    List<TableProperty> updatedTable = new List<TableProperty>();
                    List<FieldProperty> updatedField = new List<FieldProperty>();
                    updatedField.Add(field);             
                    //Update Table
                    updatedTable.Add(_dataStructure.TableIdDic[message.Selected.ParentId]);
                
         
                    Thread th = new Thread(x => {
                        UpdateStructure t = (UpdateStructure)x;
                        _dataStructure.SaveDataStructure(t);
                        _dataStructure.UpdateDatabase(t);
                        _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                        _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);
                    });
                    UpdateStructure update = new UpdateStructure();
                    update.FieldList = updatedField;
                    update.TableList = updatedTable;
                    update.ActorStatus = message.StatusSender;
                    update.Id = message.Selected.ParentId;
                    th.Start(update);

                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }

        public void Handle(DeleteTable message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
                    if (_dataStructure.IsTableLock(message.Table.id))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail to delete table. The table is lock.", ValidToken = true });
                        return;
                    }

                    string tablename = TableProperty.ClassID;
                    List<string> idToDelete = _dataStructure.GetDeleleList(_dataStructure.TableIdDic[message.Table.id]);
                    foreach(var id in idToDelete)
                    {
                        _dataStructure.TableIdDic.Remove(id);
                    }
                    _dataStructure.Delete(tablename, idToDelete, user.UserId);
                    _dataStructure.RebuidStructure();
                    _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                    _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);

                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }

        public void Handle(DeleteField message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
                    if (_dataStructure.IsTableLock(message.Field.ParentId))
                    {
                        Sender.Tell(new ReturnMessage() { Error = true, Message = "Fail to delete field. The table is lock.", ValidToken = true });
                        return;
                    }
                    string tableId = TableProperty.FieldProperty.ClassID;
                    _dataStructure.Delete(tableId, message.Field.id, user.UserId);
                    _dataStructure.FieldIdDic.Remove(message.Field.id);
                    _dataStructure.RebuidStructure();
                    _dataStructure.CommitIfNeeded(TableProperty.ClassID);
                    _dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);

                    Sender.Tell(new ReturnMessage() { Error = false, Message = "", ValidToken = true });
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }

      

        private void UpdateSchemaChange(UpdateSchema message)
        {
            try
            {
                foreach (var id in message.RecordId)
                {
                    _dataStructure.UpdatePropertyFromDb(message.TableId, id);
       
                }
                _dataStructure.RebuidStructure();              
                PublishConfigChange();
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
            }
        }
        private void PublishConfigChange()
        {
            MessageConfig config =  new MessageConfig(_dataStructure, _keyWords);
            Mediator.Tell(new Publish(Topic.ConfigChange.ToString(), config.Clone()));
        }

        public void Handle(QueryMenu message)
        {
            Users user = _dataStructure.GetUserById(message.UserIdentity);
            try
            {
                var root = new List<TreeNode>(); root = new List<TreeNode>();
                if (user == null)
                {
                    Sender.Tell(root);
                }
                else if (!message.SearchValue.Any() || !message.SearchValue.Where(x => !string.IsNullOrEmpty(x.Value)).Any())
                {
                    root.AddRange(
                        from menupair in
                            _dataStructure.MenuIdDic.Where(x => x.Value.ParentId == "#" && x.Value.HasAccess(user.UserGroups,user.id)).OrderBy(x => x.Value.RowNo)
                        let fvalue = menupair.Value
                        select new TreeNode
                        {
                            id = fvalue.id,
                            text = fvalue.Name,
                            parent = fvalue.ParentId,
                            type = fvalue.ParentId == "#" ? "toplevel" : "sublevel",                            
                            icon = string.IsNullOrEmpty(fvalue.Icon) ? "" : _dataStructure.GetTextById(Icons.ClassID, new List<string>() { Icons.FieldsId.ClassName }, fvalue.Icon),
                            li_attr = fvalue.GetLiAttr(),
                            NodeObject = new Dictionary<string, string>() { { "TableId", MenuProperty.ClassID }, { "id", fvalue.id }, { FieldNames.ParentId, fvalue.ParentId} },
                            //state = new TreeState() { opened = false, loaded = false },
                            children = true

                        });
                }
                else if (message.SearchValue.ContainsKey(FieldNames.ParentId))
                {
                    root.AddRange(
                    from menupair in
                        _dataStructure.MenuIdDic.Where(x => x.Value.ParentId == message.SearchValue[FieldNames.ParentId] && x.Value.HasAccess(user.UserGroups,user.id) ).OrderBy(x => x.Value.RowNo)
                    let fvalue = menupair.Value
                    select new TreeNode
                    {
                        id = fvalue.id,
                        text = fvalue.Name,
                        parent = fvalue.ParentId,
                        type = fvalue.ParentId == "#" ? "toplevel" : "sublevel",
                        state = TreeState.GetTreeState(opened: false, loaded: true, disabled: false, selected : false) ,
                        icon = string.IsNullOrEmpty(fvalue.Icon) ? "" : _dataStructure.GetTextById(Icons.ClassID, new List<string>() { Icons.FieldsId.ClassName }, fvalue.Icon),
                        li_attr = fvalue.GetLiAttr(),
                        NodeObject = new Dictionary<string, string>() { { "TableId", MenuProperty.ClassID }, { "id", fvalue.id },{ FieldNames.ParentId, fvalue.ParentId } },
                        children = _dataStructure.MenuIdDic.Values.Any(x=> x.ParentId.Equals(fvalue.id))

                    });
                   
                }
                else if (message.SearchValue.ContainsKey("str"))
                {
                    List<string> idlist = _dataStructure.GetFullLevelMenuIdByName(message.SearchValue["str"], user).Distinct().ToList();
            
                    root.AddRange(idlist.Where(x => _dataStructure.MenuIdDic.ContainsKey(x)).Select(x =>
                     new TreeNode
                     {
                         id = _dataStructure.MenuIdDic[x].id,
                         text = _dataStructure.MenuIdDic[x].Name,
                         parent = _dataStructure.MenuIdDic[x].ParentId,
                         type = _dataStructure.MenuIdDic[x].ParentId == "#" ? "toplevel" :"sublevel",
                         icon = string.IsNullOrEmpty(_dataStructure.MenuIdDic[x].Icon) ? "" : _dataStructure.GetTextById(Icons.ClassID, new List<string>() { Icons.FieldsId.ClassName }, _dataStructure.MenuIdDic[x].Icon),
                         li_attr = _dataStructure.MenuIdDic[x].GetLiAttr(),
                         NodeObject = new Dictionary<string, string>() { { "TableId", MenuProperty.ClassID }, { "id", _dataStructure.MenuIdDic[x].id }, { "ParentId", _dataStructure.MenuIdDic[x].ParentId } },
                         state = TreeState.GetTreeState(opened :true, loaded:true)
                     }));          

                }
                Sender.Tell(root);
            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
                Sender.Tell(new List<TreeNode>());
            }
        }

        public void Handle(AddMenu message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
             
                    var menu = new MenuProperty
                    {
                        id = Unique.NewGuid(),
                        Name = "New Menu",                        
                        ParentId = message.AddAsSubMenu ? message.SelectedMenu.id : message.SelectedMenu.ParentId,           
                        Visible = true,                                      
                        DateVersion = DateTime.Now,
                        ChangedBy = user.id,
                        AccessRight = new Dictionary<string, List<string>>() { {user.id, new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete } } }
                    };
                    _dataStructure.MenuIdDic[menu.id] = menu;
                    _dataStructure.Save(user, MenuProperty.ClassID,new List<object>() { menu });
                   
                    
                    Sender.Tell(new ReturnMessage() { Error = false, Message = $"", ValidToken = true });
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                    LogHandler.Debug(ex);
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }
        public void Handle(CopyMenuTo message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
    
                    List<MenuProperty> menulist = new List<MenuProperty>();
                    List<Pages> tablemenulist = new List<Pages>();                
                    _dataStructure.Copy(_dataStructure.MenuIdDic[message.TargetCopyMenu.id], _dataStructure.MenuIdDic[message.ToMenu.id],message.CopyAsSubMenu,ref menulist,ref tablemenulist);
                 
                    foreach(var x in menulist)
                    {
                        _dataStructure.MenuIdDic[x.id] = x;
                    }
                    foreach (var x in tablemenulist)
                    {
                        _dataStructure.PagesIdDic[x.id] = x;
                    }   
                    _dataStructure.Save(user, MenuProperty.ClassID, menulist.ToList<object>());
                    _dataStructure.Save(user, MenuProperty.Pages.ClassID, tablemenulist.ToList<object>());


                    Sender.Tell(new ReturnMessage() { Error = false, Message = $"", ValidToken = true });
                }
                catch (Exception ex)
                {
                    Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString(), ValidToken = true });
                    LogHandler.Error(ex);
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }
        public void Handle(DeleteMenu message)
        {
            ReturnMessage returnmessage = new ReturnMessage();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                List<string> idtoDelete = _dataStructure.GetDeleleList(_dataStructure.MenuIdDic[message.Menu.id]);
              
                foreach (var id in idtoDelete)
                {                
                    _dataStructure.MenuIdDic.Remove(id);
                }
        
                _dataStructure.Delete(MenuProperty.ClassID, idtoDelete, user.id);
                Sender.Tell(new ReturnMessage() { Error = false, Message = $"", ValidToken = true });
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
           
        }

        public void Handle(MessageGetFieldFilterLookup message)
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                try
                {
                    List<string> search = message.SeachName.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    if (_dataStructure.TableIdDic.ContainsKey(message.TableId))
                    {
                        if(message.Searchbykey)
                        {
                            List<string> tableIdList = new List<string>() { message.TableId, _dataStructure.TableIdDic[message.TableId].ParentId };
                            foreach (var table in tableIdList)
                            {
                                if(!_dataStructure.TableIdDic.ContainsKey(table))
                                {
                                    continue;
                                }
                                string tableName = _dataStructure.TableIdDic[table].Name;
                                List<TableProperty.FieldProperty> fieldlist = _dataStructure.TableToFieldDic[table].Where(x => (search.Contains(x.Value.id)) && !x.Value.Name.Equals(FieldNames.Button) && !x.Value.Name.Equals(FieldNames.RowNo)).Select(x => x.Value).ToList();
                                foreach (var field in fieldlist)
                                {
                                    items[field.id] = $"{tableName} - {field.Name}";
                                }
                            }
                        }
                        else
                        {
                            List<string> tableIdList = new List<string>() { message.TableId, _dataStructure.TableIdDic[message.TableId].ParentId };
                            foreach (var table in tableIdList)
                            {
                                if(_dataStructure.TableIdDic.ContainsKey(table))
                                {
                                    string tableName = _dataStructure.TableIdDic[table].Name;
                                    List<TableProperty.FieldProperty> fieldlist = _dataStructure.TableToFieldDic[table].Where(x => !search.Any() || (search.Any(y => x.Value.Name.ToLower().IndexOf(y.ToLower()) >= 0) && !x.Value.Name.Equals(FieldNames.Button) && !x.Value.Name.Equals(FieldNames.RowNo) && !x.Value.MultipleValue)).Select(x => x.Value).ToList();
                                    foreach (var field in fieldlist)
                                    {
                                        items[field.id] = $"{tableName} - {field.Name}";
                                    }
                                }
                     
                            }

                        }

                    }
         
               
                }
                catch (Exception ex)
                {
                    items["Error"] = ex.ToString();
                    //Sender.Tell(new Dictionary<string, string>() { { "Error", ex.ToString() } });
                    LogHandler.Debug(ex);
                }
            }
            Sender.Tell(items);
        }

        public void Handle(MessageGetFieldFilterLookupValue message)
        {
            try
            {
                Users user = _dataStructure.GetUserByUserId(message.UserIdentity);
                Dictionary<string, string> dropdown = new Dictionary<string, string>();
                List<string> search = message.SeachName.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                string tableId = _dataStructure.FieldIdDic[message.SelectedFieldId].ParentId;
                if(message.Searchbykey && search.Any())
                {
                    List<Dictionary<string, object>> items = _dataStructure.solrApi.DocQuery.CustomQuery(tableId, $"{message.SelectedFieldId}:{message.SeachName.FirstOrDefault()}", new List<string>() { message.SelectedFieldId }).docs;
                    if (TypeCheck.IsLookUp(_dataStructure.FieldIdDic[message.SelectedFieldId].ControlType))
                    {
                        dropdown = items.Select(x => x[message.SelectedFieldId]).Distinct().ToDictionary(x => x.ToString(), y => _dataStructure.GetLookupText(tableId, message.SelectedFieldId, y.ToString()));
                    }
                    else
                    {
                        dropdown = items.Select(x => x[message.SelectedFieldId]).Distinct().ToDictionary(x => x.ToString(), y => y.ToString());
                    }
                }
                else
                {
                    if(!search.Any())
                    {
                        search.Add("");
                    }
                    foreach(var s in search)
                    {
                        List<Dictionary<string, object>> items = _dataStructure.solrApi.DocQuery.CustomQuery(tableId, $"{message.SelectedFieldId}:*{message.SeachName.FirstOrDefault()}*", new List<string>() { message.SelectedFieldId }).docs;
                        if (TypeCheck.IsLookUp(_dataStructure.FieldIdDic[message.SelectedFieldId].ControlType))
                        {
                            dropdown = items.Select(x => x[message.SelectedFieldId]).Distinct().ToDictionary(x => x.ToString(), y => _dataStructure.GetLookupText(tableId, message.SelectedFieldId, y.ToString()));
                        }
                        else
                        {
                            dropdown = items.Select(x => x[message.SelectedFieldId]).Distinct().ToDictionary(x => x.ToString(), y => y.ToString());
                        }
                    }
                }
                Sender.Tell(dropdown);
            }
            catch (Exception ex)
            {
                Sender.Tell(new Dictionary<string, string>() { { "Error", ex.ToString() } });
            }
        }

        public void Handle(MessageBackupScema message)
        {
            BackupResponse backupdata = new BackupResponse();
           
            try
            {
                string templocation = AppDomain.CurrentDomain.BaseDirectory + $"temp/{DateTime.Now.ToString("yyyyMMddHHmmss")}/";

                List<TableProperty> TablePropertyList = _dataStructure.TableIdDic.Where(x => message.SchemaIds.Contains(x.Key)).Select(x => x.Value).ToList();
                List<FieldProperty> FieldPropertyList = _dataStructure.FieldIdDic.Where(x => message.SchemaIds.Contains(x.Value.ParentId)).Select(x => x.Value).ToList();
                SchemaBackup backup = new SchemaBackup();
                backup.TablePropertyList = TablePropertyList.ToList();
                backup.FieldPropertyList = FieldPropertyList.ToList();
                foreach (var table in TablePropertyList)
                {
                    string bakupPath = $"{_dataStructure.solrApi.SolrAuthentication.SolrHome}/{table.id}";
                    if(Directory.Exists(bakupPath))
                    {
                        DirHelper.Copy(bakupPath, $"{templocation}/{table.id}/"); 
                    }
                                
                }
                foreach (var x in backup.TablePropertyList)
                {
                    x.Changed.Clear();
                }
                foreach (var x in backup.FieldPropertyList)
                {
                    x.Changed.Clear();
                }
                JsonIO.Save($"{templocation}/schema.json", backup);
                string ziptemplocation = $"{AppDomain.CurrentDomain.BaseDirectory}tempzip/{DateTime.Now.ToString("yyyyMMddHHmmss")}.zip";
                DirHelper.CreateDirectory(ziptemplocation);
                ZipFile.CreateFromDirectory(templocation, $"{ziptemplocation}");

                backupdata.Data = File.ReadAllBytes(ziptemplocation);
                File.Delete(ziptemplocation);
                Directory.Delete(templocation,true);

                backupdata.TableIdToNameDic = _dataStructure.TableIdDic.Select(x => x.Value).ToDictionary(x => x.id, y =>_dataStructure.GetFullTablename(y.id));
                backupdata.TableToFieldNameDic = new Dictionary<string, List<string>>();
                foreach(var table in backupdata.TableIdToNameDic)
                {
                    backupdata.TableToFieldNameDic[table.Key] = _dataStructure.FieldIdDic.Where(x=> x.Value.ParentId == table.Key).Select(x => x.Value.Name).ToList();
                }

                Sender.Tell(backupdata);
            }
            catch(Exception ex)
            {
                backupdata.Error = true;
                backupdata.Message = ex.ToString();
                Sender.Tell(backupdata);
            }
                       
        
        }

        public void Handle(MessageRestore message)
        {
            Thread th = new Thread(x=> {
                ThreadMessage mt = (ThreadMessage)x;
                MessageRestore m = (MessageRestore)mt.Obj;
                Users user = new Users();
                if (ValidUser(ref user, m.UserIdentity))
                {                 

                    Dictionary<string, object> records = _dataStructure.QueryById(m.TableID, m.RecordId).docs.FirstOrDefault();
                    Backup backup = records.CastToObject<Backup>();
                   bool hasAccess =  _dataStructure.FieldIdDic["BJvsuDAwAJstJEEBuuIIFtuIwrJGJsuBwr"].HasAccess(user.UserGroups, user.id);
                    if(!hasAccess)
                    {
                        mt.Sender.Tell(new ReturnMessage { Error = true, ValidToken = true, Message = "Restoration Fial. Access Denial" });
                        return;
                    }

                    string templocation = AppDomain.CurrentDomain.BaseDirectory + $"temp/{Path.GetFileNameWithoutExtension(backup.BackupLocation)}/";
                    if (Directory.Exists(templocation))
                    {
                        Directory.Delete(templocation, true);
                    }
                    ZipFile.ExtractToDirectory(backup.BackupLocation, templocation);
                    SchemaBackup schemabackup = JsonIO.Load<SchemaBackup>($"{templocation}/schema.json");
                    int count = 0;
                    foreach (TableProperty table in schemabackup.TablePropertyList)
                    {
                        if (table.id == Backup.ClassID)
                        {
                            //Never Restore this
                            continue;
                        }
                        LogHandler.Debug($"Deleting Core {table.id}");
                        _dataStructure.solrApi.Core.DeleteCore(table.id);
                        if (Directory.Exists($"{_dataStructure.solrApi.SolrAuthentication.SolrHome}/{table.id}"))
                        {
                            Directory.Delete($"{_dataStructure.solrApi.SolrAuthentication.SolrHome}/{table.id}", true);
                        }

                    }
                    foreach (TableProperty table in schemabackup.TablePropertyList)
                    {
                        if (table.id == Backup.ClassID)
                        {
                            //Never Restore this
                            continue;
                        }
                        LogHandler.Debug($"##############Copy Core {table.Name} {table.id}##############");
                        DirHelper.Copy($"{templocation}/{table.id}", $"{_dataStructure.solrApi.SolrAuthentication.SolrHome}/{table.id}");

                    }

                    foreach (TableProperty table in schemabackup.TablePropertyList)
                    {
                        if (table.id == Backup.ClassID)
                        {
                            //Never Restore this
                            continue;
                        }
                        LogHandler.Debug($"##############Create Core {table.Name} {table.id}##############");
                        _dataStructure.solrApi.Core.Create(table.id);
                        LogHandler.Debug($"#######################################################");
                        Directory.Delete($"{templocation}/{table.id}", true);
                    }
                    LogHandler.Debug($"Restoring Completed. ");
                    LogHandler.Debug($"Performing cleaning.");
                    Directory.Delete(templocation, true);
                    LogHandler.Debug($"Clean Completed.");
                    mt.Sender.Tell(new ReturnMessage { Error = false, ValidToken = true, Message = "Restore Completed" });

                }
                else
                {
                    mt.Sender.Tell(new ReturnMessage { ValidToken = false });
                }
            });

            th.Start(new ThreadMessage(Sender,Self, message));
     
        }

        public void Handle(MessageUpdateEncryptedField message)
        {
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                EncryptText(user,message.TableId,message.FieldId,message.RecordId,message.FieldValue);
                Sender.Tell(new ReturnEncryptedField { ValidToken = true, Message = "Save Completed." });
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }

        private void EncryptText(Users user,string tableId, string fieldId, string recordId, string value)
        {
            Dictionary<string, object> record = _dataStructure.QueryById(tableId, recordId).docs.FirstOrDefault();
            if (_dataStructure.TableToFieldDic[tableId][fieldId].HasAccess(user.UserGroups, user.id))
            {
                FieldProperty field = _dataStructure.TableToFieldDic[tableId].FirstOrDefault(x => x.Value.GetCleanName().Equals("AccessRight")).Value;
                if (record == null)
                {
                    record = new Dictionary<string, object>();
                    record[field.id] = Access.GetAccess(user.id, new List<string>() { Const.FieldAccess.View, Const.FieldAccess.Modify, Const.FieldAccess.Delete });
                }
                Dictionary<string, List<string>> accessRight = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(record[field.id].ToString());
                if (accessRight.Keys.Where(x => (user.UserGroups.Contains(x) || user.id.Equals(x)) && accessRight[x].Contains("Modify")).Any())
                {
                    string encryptionid = _dataStructure.TableToFieldDic[tableId][fieldId].Encrytion;


                    Encryptions enc = new Encryptions();
                    Dictionary<string, object> items = _dataStructure.QueryById(Encryptions.ClassID, encryptionid).docs.FirstOrDefault();
                    enc = items.CastToObject<Encryptions>();
                    //int key = _dataStructure.TableToFieldDic[tableId][columnupdate];
                    //Encryption ID
                    string ecnrypted;
                    if (enc.EncryptionType == "0")
                    {
                        ecnrypted = SimpleSHA.EncryptOneWay(value);
                    }
                    else
                    {
                        ecnrypted = SimpleAES.Encrypt(value, enc.Key, int.Parse(enc.EncryptionType), enc.IV);
                    }


                    var newrecord = new Dictionary<string, object>();
                    newrecord[fieldId] = ecnrypted;
                    newrecord["id"] = recordId;

                    _dataStructure.Save(user, tableId, new List<Dictionary<string, object>>() { newrecord });
                    //   Dictionary<string,object> encrtypobj = _dataStructure.QueryById( _dataStructure.TableToFieldDic[message.TableId][columnupdate].Encrytion
                    
                }

            }
        }

        public void Handle(MessageViewEncryptedField message)
        {
            Users user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                Dictionary<string, object> record = _dataStructure.QueryById(message.TableId, message.RecordId).docs.FirstOrDefault();
                if (_dataStructure.TableToFieldDic[message.TableId][message.FieldId].HasAccess(user.UserGroups, user.id))
                {
                    try
                    {
                        if (record == null)
                        {
                            Sender.Tell(new ReturnEncryptedField { ValidToken = true, Message = "" });
                        }
                        else
                        {
                            FieldProperty field = _dataStructure.TableToFieldDic[message.TableId].FirstOrDefault(x => x.Value.GetCleanName().Equals("AccessRight")).Value;

                            Dictionary<string, List<string>> accessRight = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(record[field.id].ToString());
                            if (accessRight.Keys.Where(x => (user.UserGroups.Contains(x) || user.id.Equals(x)) && accessRight[x].Contains("Modify")).Any())
                            {

                                string valuerecord = record.ContainsKey(message.FieldId) ? TypeCheck.CastToList(record[message.FieldId]).FirstOrDefault().ToString() : "";
                                Encryptions enc = new Encryptions();
                                string encryptionid = _dataStructure.TableToFieldDic[message.TableId][message.FieldId].Encrytion;
                                enc = _dataStructure.QueryById(Encryptions.ClassID, encryptionid).docs.FirstOrDefault().CastToObject<Encryptions>();
                                //int key = _dataStructure.TableToFieldDic[message.TableId][columnupdate];
                                //Encryption ID


                                string decrypted;
                                if (enc.EncryptionType == "0")
                                {
                                    decrypted = valuerecord;
                                }
                                else
                                {
                                    decrypted = SimpleAES.Decrypt(valuerecord, enc.Key, int.Parse(enc.EncryptionType), enc.IV);
                                }

                                Sender.Tell(new ReturnEncryptedField { ValidToken = true, Message = decrypted });

                            }
                        }

                    }
                    catch(Exception ex)
                    {
                        LogHandler.Error(ex);
                        Sender.Tell(new ReturnEncryptedField { ValidToken = true,Error= true, Message = ex.ToString() });

                    }
                    
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage { ValidToken = false });
            }
        }

        public void Handle(MessageDownloadCSV message)
        {
            try
            {
                var user = new Users();
                if (ValidUser(ref user, message.UserIdentity))
                {
                    Thread t = new Thread(obj =>
                    {
                        ThreadMessage thobject = (ThreadMessage)obj;
                        try
                        {
                            string templocation = AppDomain.CurrentDomain.BaseDirectory + $"temp/{DateTime.Now.ToString("yyyyMMddHHmmss")}/";
                            
                            MessageDownloadCSV mdcsv = (MessageDownloadCSV)thobject.Obj;
                  
                            List<string> tableList = _dataStructure.FieldIdDic.Where(x => mdcsv.FieldList.Contains(x.Key)).Select(x => x.Value.ParentId).Distinct().ToList();
                            foreach(var table in tableList)
                            {
                                string csvfile = $"{templocation}{_dataStructure.GetFullTablename(table)}.csv";
                                List<string> fieldlist = new List<string>();
                                List<string> outputFieldList = new List<string>();
                                foreach(var f in mdcsv.FieldList)
                                {
                                    if(_dataStructure.TableToFieldDic[table].ContainsKey(f) && _dataStructure.TableToFieldDic[table][f].HasAccess(user.UserGroups, user.id))
                                    {
                                        fieldlist.Add(f);
                                        outputFieldList.Add(f);
                                        if(TypeCheck.IsLookUp(_dataStructure.TableToFieldDic[table][f].ControlType))
                                        {
                                            outputFieldList.Add("View-" + f );
                                        }
                                    }
                                }
                                string parentId  = _dataStructure.GetFieldIdByName(table, "ParentId");
                                if(!fieldlist.Contains(parentId))
                                {
                                    fieldlist.Add(parentId);
                                    outputFieldList.Add(parentId);
                                }
                                if(!fieldlist.Contains("id"))
                                {
                                    fieldlist.Add("id");
                                    outputFieldList.Add("id");
                                }
                                fieldlist = fieldlist.Distinct().ToList();
                                Dictionary<string, object> searchterm = mdcsv.SearchTerm.Where(x => fieldlist.Contains(x.Key)).ToDictionary(x=> x.Key, y=> y.Value);
                                Dictionary<string,string> sorterm = mdcsv.SortTerm.Where(x => fieldlist.Contains(x.Key)).ToDictionary(x => x.Key, y => y.Value);
                                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                                ResponseDetail rd = _dataStructure.Query(table, searchterm, fieldlist, sorterm, start:0, limit:_dataStructure.MaxRecordPerQuery,strict:false);
                                int count = 0;
                                int found = rd.numFound;
                                Download download = new Download();
                                download.id = Unique.NewGuid();
                                download.ParentId = "#";
                                int indexfolder = 0;
                                string uploadlocation = _dataStructure.GetUploadLocation(ref indexfolder);
                                download.FileLocation = $"{uploadlocation}/{Download.FieldsId.Files}/{download.id}/".ToFSlashClean();                   
                                download.DateVersion = DateTime.Now;  
                                string tablename = _dataStructure.TableIdDic.Where(x => tableList.Contains(x.Key) && !tableList.Contains(x.Value.ParentId)).FirstOrDefault().Value.GetCleanName();

                                StringBuilder sb = new StringBuilder();
                                sb.Append("<div class='row' ><ul>");
                                foreach (var tb in _dataStructure.TableIdDic.Where(x => tableList.Contains(x.Key)).Select(x => x.Value))
                                {
                                    sb.AppendLine($"<li>{tb.Name}</li>");
                                }
                                sb.Append("</ul></div>");
                                download.Description = sb.ToString();                               
                                _dataStructure.Save(user, Download.ClassID, download);
                                DirHelper.CreateDirectory(download.FileLocation);
                                Dictionary<string, object> header = fieldlist.ToDictionary(x => x, y => (object)(_dataStructure.FieldIdDic.ContainsKey(y) ? _dataStructure.FieldIdDic[y].Name : y));
                                List<Dictionary<string, object>> outputresult = new List<Dictionary<string, object>>();
                                outputresult.Add(header);                               
                                CSVExt.WriteCSV($"{download.FileLocation}{_dataStructure.TableIdDic[table].Name}.csv", outputresult,  outputFieldList);

                                while (rd.docs.Count > 0)
                                {                              
                                    data.AddRange(rd.docs);
                                    Dictionary<bool,Dictionary<string,Dictionary<string,string>>> dicLoopkup = _dataStructure.GetLookupTextDic(table, fieldlist.Where(x => _dataStructure.FieldIdDic.ContainsKey(x) && TypeCheck.IsLookUp(_dataStructure.FieldIdDic[x].ControlType)).ToList(), data, "");
                                    foreach (var field in fieldlist.Where(x=> _dataStructure.FieldIdDic.ContainsKey(x) && TypeCheck.IsLookUp(_dataStructure.FieldIdDic[x].ControlType) ))
                                    {  
                                        switch (_dataStructure.FieldIdDic[field].ControlType)
                                        {
                                            case "CheckList":
                                            case "ComboBox":
                                            case "SortableComboBox":
                                                {
                                                    //List<string> idlist = new List<string>();
                                                    //foreach (var d in data)
                                                    //{
                                                    //    if(!d.ContainsKey(field))
                                                    //    {
                                                    //        continue;
                                                    //    }
                                                    //    idlist.AddRange(TypeCheck.CastToList(d[field]));
                                                    //    idlist = idlist.Distinct().ToList();
                                                    //}                                                    
                                
                                                    foreach (var d in data)
                                                    {
                                                        if (!d.ContainsKey(field))
                                                        {
                                                            d[field] = "";
                                                        }
                                                        d[field] = JsonConvert.SerializeObject(TypeCheck.CastToList(d[field]).Select(x => x));
                                                        d["View-" + field] = JsonConvert.SerializeObject(TypeCheck.CastToList(d[field]).Select(x => dicLoopkup[false][field].ContainsKey(x) ? dicLoopkup[false][field][x] : x));

                                                    }

                                                }
                                                break;
                                            case "KeyValueBox":
                                                {

                                                    foreach (var d in data)
                                                    {
                                                        if (!d.ContainsKey(field))
                                                        {
                                                            d[field] = "";
                                                        }
                                                        try
                                                        {
                                                            Dictionary<string, List<string>> output = new Dictionary<string, List<string>>();
                                                            Dictionary<string, List<string>> temp = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(d[field].ToString());
                                                            foreach (var te in temp)
                                                            {
                                                                if (dicLoopkup[false][field].ContainsKey(te.Key))
                                                                {
                                                                    output[dicLoopkup[false][field][te.Key]] = te.Value.Select(x => dicLoopkup[false][field].ContainsKey(x) ? dicLoopkup[false][field][x] : x).ToList();
                                                                }

                                                            }
                                                            d[field] = JsonConvert.SerializeObject(d[field]);
                                                            d["View-" + field] = JsonConvert.SerializeObject(output);
                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }
                                                    }
                                                }                                                

                                                break;
                                            default:
                                                foreach (var d in data)
                                                {
                                                    d[field] = d[field].ToString();
                                                }

                                                break;
                                        }
                                        
                                    }
                                    outputresult.Clear();
                                    outputresult.AddRange(data);

                                    CSVExt.WriteCSV($"{download.FileLocation}{_dataStructure.TableIdDic[table].Name}.csv", outputresult, outputFieldList);
                                    count += _dataStructure.MaxRecordPerQuery;
                                    rd = _dataStructure.Query(table, searchterm, fieldlist, sorterm,start: count,limit: _dataStructure.MaxRecordPerQuery, strict:false);
                                }           
                            }  

                        }
                        catch (Exception ex)
                        {
                            thobject.Sender.Tell(new ReturnMessage() { Error = true, Message = ex.ToString() });
                            //ReportException(ex, sender, remoteServerActor, new MessageDownloadCSVReturn {Error = true, Message = ex.ToString()});
                            LogHandler.Error(ex);

                        }
                    });
                    ThreadMessage tm = new ThreadMessage(this.Sender,this.Sender,message);

                    t.Start(tm);
                    Sender.Tell(new ReturnMessage() { ValidToken = true,Message="Visit your download for status.", Error = false, TimeOut = false });
                }
                else
                {
                    Sender.Tell(new ReturnMessage() { ValidToken = false });
                }
            }
            catch (Exception ex)
            {
                Sender.Tell(new ReturnMessage() { ValidToken = true,Error = true, Message= ex.ToString() });
                LogHandler.Error(ex);
            }
        }

        public void Handle(MessageUploadCSV message)
        {
            Thread th = new Thread(itemth => {
                ThreadMessage thitem = (ThreadMessage)itemth;
                MessageUploadCSV item = (MessageUploadCSV)thitem.Obj;
                try
                {
                    var user = new Users();
                    if (ValidUser(ref user, item.UserIdentity))
                    {
                        thitem.Sender.Tell(new ReturnMessage { ValidToken = true });
                        UploadStage _upload = new UploadStage();
                        _upload = _dataStructure.QueryById(UploadStage.ClassID, item.UploadId).docs.FirstOrDefault().CastToObject<UploadStage>();

                        if(_upload == null || string.IsNullOrEmpty(_upload.id))
                        {
                            _upload = new UploadStage() {id = item.UploadId };
                        }
                        _upload.UploadStatus = "Processing";
                        _upload.DateVersion = DateTime.Now;
                        _dataStructure.Save(user, UploadStage.ClassID, _upload);
                        int indexfolder = 0;
                        string uploadlocation = $"{_dataStructure.GetUploadLocation(ref indexfolder)}/{UploadStage.FieldsId.Files}/{_upload.id}/";
                        string[] files = Directory.GetFiles(uploadlocation, "*.csv", SearchOption.AllDirectories);
                        foreach (var file in files)
                        {
                            using (StreamReader sr = new StreamReader(file))
                            {
                                List<Dictionary<string, object>> output = new List<Dictionary<string, object>>();
                                using (CsvReader csv = new CsvReader(sr))
                                {

                                    using (var dr = new CsvDataReader(csv))
                                    {
                                        var dt = new DataTable();
                                        dt.Load(dr);
                                        foreach (DataRow row in dt.Rows)
                                        {
                                            Dictionary<string, object> temp = new Dictionary<string, object>();
                                            foreach (var col in dt.Columns)
                                            {
                                                if (_dataStructure.FieldIdDic.ContainsKey(col.ToString()))
                                                {
                                                    switch (_dataStructure.FieldIdDic[col.ToString()].ControlType)
                                                    {
                                                        case "CheckList":
                                                        case "ComboBox":
                                                        case "SortableComboBox":
                                                            temp[col.ToString()] = TypeCheck.CastToList(row[col.ToString()]);
                                                            break;
                                                        default:
                                                            temp[col.ToString()] = row[col.ToString()];
                                                            break;
                                                    }

                                                }
                                                else
                                                {
                                                    temp[col.ToString()] = row[col.ToString()];
                                                }

                                            }
                                            output.Add(temp);
                                        }
                                    }
                                    //First Display Header Remove it
                                    output.RemoveAt(0);
                                    List<string> tablelist = output.FirstOrDefault().Keys.ToList().Where(x => _dataStructure.FieldIdDic.ContainsKey(x)).Select(x => _dataStructure.FieldIdDic[x].ParentId).Distinct().ToList();
                                    foreach (var table in tablelist)
                                    {
                                        foreach (var record in output)
                                        {
                                            try
                                            {
                                                _dataStructure.Save(user, table, new List<Dictionary<string, object>>() { record });
                                            }
                                            catch (Exception ex)
                                            {
                                                _upload.Description += "Error Processing Record " + JsonConvert.SerializeObject(record) + Environment.NewLine;
                                                _upload.Description += ex.ToString() + Environment.NewLine;
                                            }

                                        }

                                    }
                                }
                            }
                        }
                       


                        _upload.UploadStatus = "Completed";
                        _dataStructure.Save(user, UploadStage.ClassID, _upload);
                    }
                    else
                    {
                        thitem.Sender.Tell(new ReturnMessage { ValidToken = false });
                    }
                }
                catch (Exception ex)
                {
                    LogHandler.Error(ex);
                    thitem.Sender.Tell(new ReturnMessage { ValidToken = true, Error = true, Message = ex.ToString() });
                }
            });       
            th.Start(new ThreadMessage(Sender, Self, message));
        }

        public void Handle(ClearData message)
        {
            var user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {
                if(user.UserGroups.Contains(Id.Role.Developer) || user.id.Contains(Id.UsersAccount.Developer))
                {
                    _dataStructure.solrApi.Document.Clear(message.TableId);
                    _dataStructure.solrApi.Commit(message.TableId);
                    Sender.Tell(new ReturnMessage() { ValidToken = true, Error = false, Message = "Clear Completed" });
                }
                else
                {
                    Sender.Tell(new ReturnMessage() { ValidToken = true, Error = true, Message = "Permission denial. Only developer are allow."});
                }
            }
            else
            {
                Sender.Tell(new ReturnMessage() { ValidToken = false});
            }
               
        }

        public void Handle(RebuildSchema message)
        {
            Thread th = new Thread(itemth => {
                ThreadMessage thitem = (ThreadMessage)itemth;
                RebuildSchema item = (RebuildSchema)thitem.Obj;
                try
                {
                    var user = new Users();
                    if (ValidUser(ref user, item.UserIdentity))
                    {
                        foreach(var tablid in item.TableIdList)
                        {
                            item.StatusSender.Tell(new UpdateProgressStatus(tablid, 1, 1, $"Repairing Index {_dataStructure.TableIdDic[tablid].Name}."));
                            List<string> uniqueList = (_dataStructure.TableIdDic[tablid].UniquesList.Count == 0)
                         ? new List<string>() : _dataStructure.TableIdDic[tablid].UniquesList.Values.SelectMany(x => x).Distinct().ToList();
                            string newcoreid = _dataStructure.DuplicateCore(tablid, uniqueList, _dataStructure.TableToFieldDic[tablid].Select(x => x.Value).ToList());

                            _dataStructure.solrApi.Core.DeleteCore(tablid);
                            _dataStructure.solrApi.Core.Rename(newcoreid, tablid);

                            item.StatusSender.Tell(new UpdateProgressStatus(tablid, 1, 1, $"Repair Index {_dataStructure.TableIdDic[tablid].Name} Completed"));
                        }
                        thitem.Sender.Tell(new ReturnMessage { ValidToken = true, Message = $"Index is repaired." });

                    }
                    else
                    {
                        thitem.Sender.Tell(new ReturnMessage { ValidToken = false });
                    }
                }
                catch (Exception ex)
                {
                    LogHandler.Error(ex);
                    thitem.Sender.Tell(new ReturnMessage { ValidToken = true, Error = true, Message = ex.ToString() });
                }
            });
            th.Start(new ThreadMessage(Sender, Self, message));
        }

        public void Handle(MessageGetAutoComple message)
        {
            Thread th = new Thread(itemth => {
                ThreadMessage thitem = (ThreadMessage)itemth;
                MessageGetAutoComple i = (MessageGetAutoComple)thitem.Obj;
                List<Dictionary<string, string>> resultlist = new List<Dictionary<string, string>>();
                try
                {
                    
                    List<CompletionItem> items = RuntimeCompliler.GetCompleterItem(i.Search,
                        new List<string>() { "System", "System.Linq" },
                        new List<Microsoft.CodeAnalysis.PortableExecutableReference>() {
                    MetadataReference.CreateFromFile(typeof(Util.StandardFormat).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(typeof(Enumerable).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location)
                        }, LanguageNames.CSharp);
                    foreach (CompletionItem item in items)
                    {
                       
                        StringBuilder sb_property = new StringBuilder();
                        foreach (var x in item.Tags)
                        {
                            sb_property.AppendLine($"{x}");
                        }
                        sb_property.AppendLine(item.InlineDescription);
                        //LogHandler.Debug($"{i.Search} : {item.DisplayText}");
                        string posfix = i.Search;
                        string prefix = "";
                        if (i.Search.LastIndexOf(".") >= 0)
                        {
                            posfix = i.Search.Substring(i.Search.LastIndexOf(".")).Replace(".","");
                            prefix = i.Search.Substring(0, i.Search.LastIndexOf(".")) + ".";
                        }
                       
                        
                        if(item.DisplayText.ToLower().Contains(posfix.ToLower()))
                        {
                            Dictionary<string, string> result = new Dictionary<string, string>();
                            result["caption"] = prefix + item.DisplayText;
                            result["value"] = prefix + item.DisplayText;
                            result["desc"] = sb_property.ToString();
                            result["type"] = "snippet";
                            resultlist.Add(result);
                        }
             
                    }
                    
                }
                catch(Exception ex)
                {
                    LogHandler.Error(ex);
                }
                thitem.Sender.Tell(resultlist);
            });
            th.Start(new ThreadMessage(Sender, Self, message));

            
        }

        public void Handle(GetRecordAccessRight message)
        {

            string accessrightId = _dataStructure.TableToFieldDic[message.TableId].FirstOrDefault(x => x.Value.Name == FieldNames.AccessRight).Value.GetId();

            ResponseDetail rd = _dataStructure.Query(message.TableId, new Dictionary<string, object>() { { FieldNames.id, message.RecordId } }, new List<string>() { accessrightId }, strict: true, start: 0, limit: 1);
            
            if(rd.docs.Count > 0)
            {
                Sender.Tell(JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(rd.docs[0][accessrightId].ToString()));
            }
            
            Sender.Tell(new Dictionary<string,List<string>>());
        }

        public void Handle(LockTable message)
        {
            var user = new Users();
            if (ValidUser(ref user, message.UserIdentity))
            {

                if (_dataStructure.TableIdDic.ContainsKey(message.LockTableId) && _dataStructure.TableIdDic[message.LockTableId].HasAccess(user.UserGroups,user.id))
                {
                    _dataStructure.TableIdDic[message.LockTableId].Lock = message.IsLocked;
                    Handle(new MessageUpdateOnly() { });
                    //_dataStructure.InsertOrUpdate(message.LockTableId,new Dictionary<string, object>() { {FieldNames.id, _dataStructure.TableIdDic[message.LockTableId].id },{ } }, new List<string>());
                    // UpdateStructure t = new UpdateStructure();               
                    // t.TableList.Add(_dataStructure.TableIdDic[message.LockTableId]);
                    //_dataStructure.SaveDataStructure(t);
                    //_dataStructure.UpdateDatabase(t);
                    //_dataStructure.CommitIfNeeded(TableProperty.ClassID);
                    //_dataStructure.CommitIfNeeded(TableProperty.FieldProperty.ClassID);

                    Sender.Tell(new ReturnMessage() { Error = false, Message = _keyWords.GetWord(user.Language, message.IsLocked ? "LockTableSuccess" : "UnLockTableSuccess") });
                }
                else
                {
                    Sender.Tell(new ReturnMessage() { Error = false, Message = _keyWords.GetWord(user.Language, "LockTableFail") });
                }

            }
        }
    }
}