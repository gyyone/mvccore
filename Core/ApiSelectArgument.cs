﻿namespace Core
{
    public class ApiSelectArgument
    {
        public ApiSelectArgument()
        {
            limit = 20;
        }
        public string[] search { set; get; }
        public string fieldId { set; get; }   
        public string dependent { set; get; }
        public bool searchbykey { set; get; }
        public int limit { set; get; }
    }
}