﻿namespace Core
{
    public static class Icon
    {
        public const string fieldicon = "fas fa-align-justify text-warning";
        public const string tableicon = "fas fa-table text-success";
    }
}