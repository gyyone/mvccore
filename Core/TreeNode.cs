﻿using System.Collections.Generic;

namespace Core
{
    public class TreeNode
    {
        public TreeNode()
        {
            li_attr = new Dictionary<string, string>();
            li_attr["style"] = "color:black";
        }

        public TreeNode(string text):base()
        {
            this.text = text;
          
        }

        public TreeNode(string text, string icon) : base()
        {
            this.text = text;
            this.icon = icon;
        }


        public string id { set; get; }
        public string parent { set; get; }
        public bool children { set; get; }
        public string text { get; set; }
        public string icon { get; set; }
        public Dictionary<string,bool> state { set; get; }
        public string type { set; get; }
        public string style { set; get; }
        public string title { set; get; }
        public Dictionary<string,string> li_attr { set; get; }
        public object NodeObject { set; get; }
    }
}