﻿
using ClassLib;
using DBHandler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;
using FieldProperty = TableProperty.FieldProperty;
using ClassExtension;
namespace Core
{
    public static class TemplateExtension
    {
        public static string GetTemplateText(this Template targetTemplate, Users user, DataStructure ds,string recordId)
        {
            if (targetTemplate == null)
            {
                return "";
            }
            Dictionary<string,object> obj = ds.QueryById(targetTemplate.ForTable, recordId).docs.FirstOrDefault();

            return targetTemplate.GetTemplateText(user, ds, obj);
        }
        public static string GetTemplateText(this Template targetTemplate, Users user, DataStructure ds, object obj)
        {
            if(targetTemplate == null)
            {
                return "";
            }
            return targetTemplate.GetTemplateText(user, ds, obj.ToDictionary());
        }

        public static string GetTemplateText(this Template targetTemplate,Users user, DataStructure ds, Dictionary<string, object> keyvalue)
        {
            if (targetTemplate == null)
            {
                return "";
            }

            foreach (var field in keyvalue.ToDictionary(x=> x.Key, y=> y.Value))
            {
                if(ds.TableToFieldDic.ContainsKey(targetTemplate.ForTable) && ds.TableToFieldDic[targetTemplate.ForTable].ContainsKey(field.Key))
                {
                    FieldProperty f = ds.TableToFieldDic[targetTemplate.ForTable][field.Key];
                    if (!ds.TableToFieldDic[targetTemplate.ForTable][field.Key].HasAccess(user.UserGroups, user.id))
                    {
                        keyvalue[field.Key] = $"<div>Access Denial {ds.TableToFieldDic[targetTemplate.ForTable][field.Key].Name}<i class='fas fa-ban' title='Access Denial'></i></div>";
                    }
                    if (f.ControlType == Const.ControlTypes.DatetimePicker.ToString())
                    {
                        string value = field.Value.ToString();
                        if (string.IsNullOrWhiteSpace(value))
                        {
                            keyvalue[field.Key] = "";
                            continue;
                        }
                        if (f.UseUserDateFormat)
                        {
                            keyvalue[field.Key] = DateExtension.ConvertDateFormat(value, DateExtension.SystemDateFormat, user.DateFormat);
                        }
                        else
                        {
                            keyvalue[field.Key] = DateExtension.ConvertDateFormat(value, DateExtension.SystemDateFormat, DateExtension.GetDateFormat(f.Options.Value));

                        }

                       
                    }
                }
    
            }

            if (targetTemplate.Filter != null && targetTemplate.Filter.Any())
            {
                foreach(var fieldid in targetTemplate.Filter)
                {
                    if(keyvalue.ContainsKey(fieldid.Key) && fieldid.Value.Contains(keyvalue[fieldid.Key]))
                    {
                        continue;
                    }
                    return "";
                }
            }
    
            string templatepattern = "\\{\\[([a-zA-Z:;=\\-!@#$%&*_+~`?\\s\\d]+)\\]\\}";
            List<string> findlist = targetTemplate.Code.Value.FindByPattern("[^\\n\\r]+").FindByPattern($"[\\t\\s]*{templatepattern}");

            string templatecode = targetTemplate.Code.Value;
            foreach (var str in findlist)
            {
                string replace = str.FindByPattern(templatepattern).FirstOrDefault();
                string spaceprefix = str.Replace(replace, "");
                string[] code = replace.Split(";");
                if (code.Length >= 2)
                {
                    string id = code[1].Split("=")[0].Trim(new char[] { '[', '{', ']', '}' });

                    string[] commands = code[0].Split(".").Select(x => x.Trim(new char[] { '[', '{', ']', '}', ' ' })).ToArray();
                    string tempcodeReplace = replace;
                    if(!keyvalue.ContainsKey(id))
                    {
                        if(ds.TableToFieldDic.ContainsKey(targetTemplate.ForTable) && ds.TableToFieldDic[targetTemplate.ForTable].ContainsKey(id))
                        {
                            keyvalue[id] = $"<div>Access Denial {ds.TableToFieldDic[targetTemplate.ForTable][id].Name}<i class='fas fa-ban' title='Access Denial'></i></div>";
                        }
                        else
                        {
                            keyvalue[id] = $"<div><i class='fas fa-ban' title='Access Denial'></i></div>";
                        }
                
                    }
                    foreach (var cmd in commands)
                    {
                        try
                        {
                            switch (cmd)
                            {
                                case "clean":
                                    {

                                        string fieldid = id;
                                        templatecode = templatecode.Replace(tempcodeReplace, StandardFormat.GetCleanName(keyvalue[fieldid].ToString()));
                                        tempcodeReplace = StandardFormat.GetCleanName(keyvalue[fieldid].ToString());
                                    }
                                    break;
                                case "getDefaultValue":
                                    {
                                        FieldProperty field = keyvalue.CastToObject<FieldProperty>();
                                        string defaultvalue = GetDefaultValue(field);
                                        templatecode = templatecode.Replace(tempcodeReplace, defaultvalue);
                                        tempcodeReplace = defaultvalue;
                                    }
                                    break;
                                case "getDataType":
                                    {
                                        FieldProperty field = keyvalue.CastToObject<FieldProperty>();
                                        string datatype = GetDataType(field);
                                        templatecode = templatecode.Replace(tempcodeReplace, datatype);
                                        tempcodeReplace = datatype;
                                    }
                                    break;
                                case "template:child":
                                    {
                                        string templateid = id;
                                        Template subtemplate = ds.GetClass<Template>(user, templateid);
                                        string orderId = ds.GetFieldIdByName(subtemplate.ForTable, FieldNames.RowNo);
                                        string subtableParentid = ds.GetFieldIdByName(subtemplate.ForTable, FieldNames.ParentId);
                                        List<Dictionary<string, object>> subdatalist = ds.Query(subtemplate.ForTable, new Dictionary<string, object>() { { subtableParentid, keyvalue[FieldNames.id].ToString() } }, sortTerm: new Dictionary<string, string>() { { orderId, "asc" } }).docs;
                                        StringBuilder replacecode = new StringBuilder();
                                        foreach (var subdata in subdatalist)
                                        {
                                            string substr = subtemplate.GetTemplateText(user, ds, subdata);
                                            replacecode.Append(substr.Replace("\r\n", $"\r\n{spaceprefix}"));//.TrimStart(new char[] { '\n', '\r'}));
                                        }
                                        templatecode = templatecode.Replace(replace, replacecode.ToString());
                                        tempcodeReplace = replacecode.ToString();


                                    }

                                    break;
                                case "template":
                                    {
                                        string templateid = id;
                                        Template nexttemplate = ds.GetClass<Template>(user, templateid);
                                        StringBuilder replacecode = new StringBuilder();
                                        string substr = nexttemplate.GetTemplateText(user, ds, keyvalue);
                                        templatecode = templatecode.Replace(replace, substr).Replace("\r\n", $"\r\n{spaceprefix}");
                                        tempcodeReplace = substr;
                                    }

                                    break;
                                default:
                                    {
                                        string fieldid = id;
                                        if(ds.FieldIdDic.ContainsKey(fieldid) && ds.FieldIdDic[fieldid].Source.KeySource.Where(x=> x.GetSource<TableSource>() != null && x.GetSource<TableSource>().TableId == Template.ClassID).Any() &&
                                            (ds.FieldIdDic[fieldid].ControlType == Const.ControlTypes.ComboBox.ToString()
                                            ||  ds.FieldIdDic[fieldid].ControlType == Const.ControlTypes.SortableComboBox.ToString()
                                            || ds.FieldIdDic[fieldid].ControlType == Const.ControlTypes.RadioButton.ToString()
                                            ))
                                        {
                                           
                                           List<Template> templatelist =  ds.GetClass<Template>(user, TypeCheck.CastToList(keyvalue[fieldid]));
                                            StringBuilder sb = new StringBuilder();
                                            if (templatelist.Any())
                                            {
                                               
                                                foreach (Template t in templatelist)
                                                {
                                                    sb.Append(t.GetTemplateText(user, ds, user));
                                                }
                                                templatecode = templatecode.Replace(replace, sb.ToString());
                                            }
                                            else
                                            {
                                                templatecode = templatecode.Replace(replace, keyvalue[fieldid].ToString());
                                            }                                          
                                            
                                        }
                                        else if (ds.FieldIdDic.ContainsKey(keyvalue[fieldid].ToString()) && ds.FieldIdDic[keyvalue[fieldid].ToString()].Name.Equals("id"))
                                        {
                                            //For Field property Class Generated Use. Replace Guid of id to "id"
                                            templatecode = templatecode.Replace(replace, "id");
                                        }
                                        else
                                        {
                                            templatecode = templatecode.Replace(replace, keyvalue[fieldid].ToString());
                                        }

                                        tempcodeReplace = keyvalue[fieldid].ToString();
                                    }
                                    break;
                            }
                        }
                        catch(Exception ex)
                        {
                            tempcodeReplace = ex.ToString();
                        }
                   
                    }
                }

            }

            return templatecode;
        }

        private static string GetDefaultValue(FieldProperty field)
        {
            string defaultvalue = "";
            try
            {
                if (field.id == TableProperty.FieldProperty.FieldsId.Source && field.ParentId == TableProperty.FieldProperty.ClassID)
                {
                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "new DataSource()" : $"JsonConvert.DeserializeObject<DataSource>({JsonConvert.SerializeObject(field.DefaultValues)})";
                }
                //else if (field.id == Const.TableProperty.FieldProperty.ControlType && field.ParentId == Const.TableProperty.FieldProperty.ClassID)
                //{
                //    type = "ControlTypes";
                //}
                else if (field.Name == "RowNo")
                {
                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "0" : field.DefaultValues;
                }
                else
                {
                    switch (field.ControlType)
                    {
                        case Const.ControlTypes.DatetimePicker:
                            try
                            {
                                if (string.IsNullOrWhiteSpace(field.DefaultValues))
                                {
                                    defaultvalue = "DateTime.Now";
                                }
                                else
                                {
                                    string[] settings = field.DefaultValues.Split(new[] { "\n" }, StringSplitOptions.None);
                                    if (settings.Length >= 2)
                                    {
                                        defaultvalue = field.DefaultValues.Replace(settings[0] + "\n", "");
                                    }
                                    else
                                    {
                                        defaultvalue = field.DefaultValues;
                                    }

                                }

                            }
                            catch
                            {
                                defaultvalue = "DateTime.Now";
                            }
                          
                            break;
                        case Const.ControlTypes.SortableComboBox:
                        case Const.ControlTypes.CheckList:
                            try
                            {
                                defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "new List<string>()" : $"JsonConvert.DeserializeObject<List<string>>(@{JsonConvert.SerializeObject(field.DefaultValues).Replace("\\\"", "\"\"").Replace("\\\\", "\\")})";
                            }
                            catch
                            {
                                defaultvalue = "new List<string>();";
                            }
                            
                            break;
                        case Const.ControlTypes.ComboBox:
                            try
                            {
                                if (field.MultipleValue)
                                {
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "new List<string>()" : $"JsonConvert.DeserializeObject<List<string>>(@{JsonConvert.SerializeObject(field.DefaultValues).Replace("\\\"", "\"\"").Replace("\\\\", "\\")})";
                                }
                                else
                                {
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "\"\"" : $"@{JsonConvert.SerializeObject(TypeCheck.CastToList(field.DefaultValues).FirstOrDefault())}";
                                }
                            }
                            catch
                            {
                                defaultvalue = "new List<string>();";
                            }
                  
                            break;
                        case Const.ControlTypes.SwitchButton:
                        case Const.ControlTypes.CheckBox:
                            defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "false" : field.DefaultValues;
                            break;
                        case Const.ControlTypes.FileUpload:
                            if(field.MultipleValue)
                            {
                                defaultvalue = "new List<string>();";
                            }
                            else
                            {
                                defaultvalue = "\"\";";
                            }
                         
                            break;
                        case Const.ControlTypes.RadioButton:
                        case Const.ControlTypes.ColorPicker:
                        case Const.ControlTypes.EncrytText:
                        case Const.ControlTypes.SummerNote:
                        case Const.ControlTypes.SourceEditor:
                            defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "@\"\"" : $"@{JsonConvert.SerializeObject(field.DefaultValues).Replace("\\\"", "\"\"").Replace("\\\\", "\\")})";
                            break;
                        case Const.ControlTypes.KeyValueBox:
                            defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "new Dictionary<string,List<string>>()" : $"JsonConvert.DeserializeObject<Dictionary<string,List<string>>>(@{JsonConvert.SerializeObject(field.DefaultValues).Replace("\\\"", "\"\"").Replace("\\\\", "\\")})";
                            break;
                        case Const.ControlTypes.CodeEditor:
                            if (string.IsNullOrWhiteSpace(field.DefaultValues))
                            {
                                defaultvalue = "new CodeEditor(\"\")";
                            }
                            else
                            {
                                defaultvalue = "new CodeEditor(@" + JsonConvert.SerializeObject(field.DefaultValues).Replace("\\\"", "\"\"").Replace("\\\\", "\\") + ")";
                            }
                            break;
                        //Default value is base on datatype
                        //case Const.ControlTypes.InputMask:
                        //case Const.ControlTypes.TextBox:
                        //case Const.ControlTypes.Html:
                        default:
                            switch (field.DataType)
                            {
                                case "boolean":
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "false" : field.DefaultValues;
                                    break;
                                case "pdouble":
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "0" : field.DefaultValues;
                                    break;
                                case "plong":
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "0" : field.DefaultValues;
                                    break;
                                case "pint":
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "0" : field.DefaultValues;
                                    break;
                                default:
                                    defaultvalue = string.IsNullOrWhiteSpace(field.DefaultValues) ? "\"\"" : $"\"{field.DefaultValues}\"";
                                    break;
                            }
                            break;

                    }
                }
            }
            catch
            {
                defaultvalue = "\"\"";
            }
            return defaultvalue;
        }
        private static string GetDataType(FieldProperty field)
        {
            string datatype = "string";
            try
            {
                switch (field.ControlType)
                {
                    case Const.ControlTypes.KeyValueBox:
                        datatype = "Dictionary<string, List<string>>";
                        break;
                    case Const.ControlTypes.DatetimePicker:
                        datatype = "DateTime";
                        break;
                    case Const.ControlTypes.CodeEditor:
                        datatype = "CodeEditor";
                        break;
                    default:
                        string type = "string";
                        switch (field.DataType)
                        {
                            case "boolean":
                                type = "bool";
                                break;
                            case "pdouble":
                                type = "double";
                                break;
                            case "plong":
                                type = "long";
                                break;
                            case "pint":
                                type = "int";
                                break;
                            default:
                                type = "string";
                                break;
                        }
                        if (field.id == TableProperty.FieldProperty.FieldsId.Source && field.ParentId == TableProperty.FieldProperty.ClassID)
                        {
                            type = "DataSource";
                        }
                        //else if (field.id == Const.TableProperty.FieldProperty.ControlType && field.ParentId == Const.TableProperty.FieldProperty.ClassID)
                        //{
                        //    type = "ControlTypes";
                        //}
                        else if (field.Name == "RowNo")
                        {
                            type = "int";
                        }
                        if (field.MultipleValue)
                        {
                            datatype = $"List<{type}>";
                        }
                        else
                        {
                            datatype = type;
                        }
                        break;

                }

            }
            catch (Exception ex)
            {
                datatype = "string";
            }
            return datatype;
        }
    }
}