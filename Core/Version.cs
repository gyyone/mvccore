﻿using System.Collections.Generic;


namespace Core
{
    public static class CodeVersion
    {
        public static List<ushort> VersionNo = new List<ushort>() { 0, 0, 0, 1 };
    }
    /// <summary>
    /// Important, The version number will define whether to update database or not
    /// </summary>
    public  class Version
    {
        public List<ushort> Current { set; get; }
        public Version()
        {
            Current = CodeVersion.VersionNo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="versionNo"></param>
        /// <param name="versionNo2"></param>
        /// <returns>true if version no > version no2, else false</returns>
        public static bool IsLargerVersion(List<ushort> versionNo, List<ushort> versionNo2)
        {
           
            for (int x = 0; x < versionNo.Count; x++)
            {
                if (versionNo[x] > versionNo2[x])
                {
                    return true;
                }
                else if (versionNo[x] < versionNo2[x])
                {
                    return false;
                }
                else
                {
                    // equal
                    continue;
                }
            }
            //equal
            return false;
        }
    }
}