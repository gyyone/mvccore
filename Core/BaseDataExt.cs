﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using ClassLib;
using Core.Akka;

namespace Core
{

    public static class BaseDataExt
    {

        public static Dictionary<string, object> BaseColumn(bool ignoreDisplay=true)
        {
            var _temp = new Dictionary<string, object>();
            if (!ignoreDisplay)
            {
                _temp.Add(FieldNames.Button, FieldNames.Button);
                _temp.Add(FieldNames.RowNo, FieldNames.RowNo);
            }
            _temp.Add(FieldNames.DateVersion, FieldNames.DateVersion);
            _temp.Add(FieldNames.id, FieldNames.id);
            _temp.Add(FieldNames.ParentId,FieldNames.ParentId);
            
            _temp.Add(FieldNames.ChangedBy,FieldNames.ChangedBy);
            _temp.Add(FieldNames.AccessRight,FieldNames.AccessRight); 
            return _temp;
        }



        public static string FormatName(string name)
        {
           return  Regex.Replace(name, "[^0-9a-zA-Z\\d]", "");
        }
    }


}