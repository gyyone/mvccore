﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using Core;
using SolrNetCore;
using System.Collections;
using System.Reflection;
using Newtonsoft.Json.Converters;
using Akka;
using Akka.Actor;
using System.Threading.Tasks;
using RestApi;
using Util;
using ClassLib;
using Pages = MenuProperty.Pages;
using JsTable = MenuProperty.Pages.JsTable;
using Id = ClassLib.Id;
using Access = ClassLib.Access;
using FieldProperty = TableProperty.FieldProperty;
using Core.Akka.Query;
using Core.Akka;
using LogHelper;
using JsonHelper;
using DirectoryHelper;
using ClassExtension;

namespace DBHandler
{

    public  class DataStructure
    {     

        public SolrApi solrApi { set; get; }
        private bool Initialize = false;
        private string backupDir = $"{AppDomain.CurrentDomain.BaseDirectory}/backup/";
        public List<string> UploadedFolders { set; get; }
        public long MinSpaceRequired { set; get; }
        public Dictionary<string, MenuProperty> MenuIdDic { set; get; }
        public Dictionary<string, Pages> PagesIdDic { set; get; }
        public Dictionary<string, JsTable> JsTableIdDic { set; get; }
        //Provide table id get table property
        public Dictionary<string, TableProperty> TableIdDic { set; get; }
        //Provide data Id get  get data Property
        public Dictionary<string, FieldProperty> FieldIdDic { set; get; }
        public FieldProperty GetFieldProperty(string id)
        {
            if(FieldIdDic.ContainsKey(id))
            {
                return FieldIdDic[id];
            }
            return new FieldProperty();
        }




        //Provide Table Name Get Table Property Build from TableIdDic and FieldIdDIc

        public Dictionary<string, TableProperty> TableNameDic { get; }

        public Dictionary<string, Dictionary<string, FieldProperty>> TableToFieldDic { get; }
        public Dictionary<string, Dictionary<string, FieldProperty>> TableToFieldNameDic { get; }

        //Provide TableName, FieldName,UserId/ Role ID to check Access Right

        public string RootPath { set; get; }
        public int MaxNumberVer { set; get; }
        public string FilesPath { get; set; }
        public string FilesUpload => FilesPath + "Upload\\";
        public string FilesDownload => FilesPath + "Download\\";
        public string FilesUploadUrl { get; set; }
        public string FilesDownloadUrl { get; set; }
        public int MaxRecordPerQuery { get; set; }

        public string GetUploadLocation(ref int indexFolder)
        {        
            foreach (string folder in UploadedFolders)
            {
                long freespace = DirHelper.FreeSpace(folder);
                if (freespace > MinSpaceRequired)
                {
                    return Path.GetFullPath(folder).Replace("\\","/").Replace("//","/");
                }
                indexFolder++;

            }
            return null;
        }
        public DataStructure()
        {
            UploadedFolders = new List<string>();
            MinSpaceRequired = 10000000000;
             MaxNumberVer = 50;
            MenuIdDic = new Dictionary<string,MenuProperty>();
            PagesIdDic = new Dictionary<string, Pages>();       
 

            TableToFieldDic = new Dictionary<string, Dictionary<string, FieldProperty>>();
            TableIdDic = new Dictionary<string, TableProperty>();
            FieldIdDic = new Dictionary<string, FieldProperty>();
            TableToFieldNameDic = new Dictionary<string, Dictionary<string, FieldProperty>>();
            MaxNumberVer = 50;
            MaxRecordPerQuery = 524288;
        }
     
        public void SetTablePropertyAndField(List<TableProperty> tablelist, List<FieldProperty> fieldlist)
        {
            foreach (var x in tablelist)
            {
                TableIdDic[x.id] = x;
            }
            foreach (var x in fieldlist)
            {
                if(x.Source == null)
                {
                    x.Source = new DataSource();
                }
                if (x.Source.KeySource != null )
                {
                    if (x.Source.KeySource.Where(x => x.GetSource() == null).Any())
                    {
                        x.Source.KeySource.Clear();
                    }
                }
                else
                {
                    x.Source.KeySource = new List<SelectSource>();
                }
                if (x.Source.ValueSource != null)
                {
                    if (x.Source.ValueSource.Where(x => x.GetSource() == null).Any())
                    {
                        x.Source.ValueSource.Clear();
                    }
                }
                else
                {
                    x.Source.ValueSource = new List<SelectSource>();
                }

                FieldIdDic[x.id] = x;    
            }
            RebuidStructure();
        }
        public void RebuidStructure()
        {
            ClearCache();

            foreach (var item in TableIdDic.Values.Where(x => x.ParentId == "#").ToList())
            {
                BuildDic(item, StandardFormat.GetCleanName(item.Name));
            }
        }
        private void BuildDic(TableProperty tableparent, string tableName)
        {
            try
            {
                string parentId = tableparent.id;
                var childTable = TableIdDic.Values.Where(x => x.ParentId == parentId).ToList();
                var childField = FieldIdDic.Values.Where(x => x.ParentId == parentId).ToList();
                if (!TableToFieldNameDic.ContainsKey(parentId))
                {
                    TableToFieldNameDic[parentId] = new Dictionary<string, FieldProperty>();
                }
 
                if (!TableToFieldDic.ContainsKey(parentId))
                {
                    TableToFieldDic[parentId] = new Dictionary<string, FieldProperty>();
                }
                foreach (var field in childField)
                {
                    if (field.Name.Equals("id"))
                    {
                        TableToFieldDic[parentId][field.Name] = FieldIdDic[field.id];
                        TableToFieldNameDic[parentId][StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];
                     
                    }
                    else
                    {
                        TableToFieldDic[parentId][field.id] = FieldIdDic[field.id];
                        TableToFieldNameDic[parentId][StandardFormat.GetCleanName(field.Name)] = FieldIdDic[field.id];                     
                    }                   

                }

                foreach (var baseProperty in childTable)
                {
                    BuildDic(baseProperty, tableName + @"_" + StandardFormat.GetCleanName(baseProperty.Name));
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

        }
        public T GetClass<T>(Users user, string id) where T : new()
        {
            if(string.IsNullOrEmpty(id))
            {
                return new T();
            }
            T temp = new T();            
            Dictionary<string, object> t = temp.ToDictionary();
            string classId = t["Class_ID"].ToString();
            return Query(classId, new Dictionary<string, object>() { { "id", id } }, new List<string>() { "*" }).docs.FirstOrDefault().CastToObject<T>();            
        }
        public List<T> GetClass<T>(Users user, List<string> id) where T : new()
        {
            id = id.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            if (!id.Any())
            {
                return new List<T>();
            }
            T temp = new T();
            Dictionary<string, object> t = temp.ToDictionary();
            string classId = t["Class_ID"].ToString();
            return Query(classId, new Dictionary<string, object>() { { "id", id } }, new List<string>() { "*" }).docs.Select(x=> x.CastToObject<T>()).ToList();
        }
        public T GetLookupClass<T> (string useridentity,string fieldid, string id, bool valueSource = false) where T:new()
        {    
            T temp = new T();
            Dictionary<string, object> t = temp.ToDictionary();
            string classId = t["Class_ID"].ToString();
            return GetLookupClassById(useridentity, FieldIdDic[fieldid], classId, new List<string>() { id.ToString() }, valueSource).FirstOrDefault().CastToObject<T>(); 
        }
        public List<T> GetLookupClass<T>(string useridentity,string fieldid, List<string> ids, bool valueSource = false) where T : new()
        {
            T temp = new T();
            Dictionary<string, object> t = temp.ToDictionary();
            string classId = t["Class_ID"].ToString();
            return GetLookupClassById(useridentity, FieldIdDic[fieldid], classId, ids, valueSource).Select(x=> x.CastToObject<T>()).ToList() ;
        }
        public List<Dictionary<string, object>> GetLookupClassById(string useridentity,FieldProperty fieldproperty,string classid, List<string> searchlist, bool valueSource = false)
        {
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
           
            try
            {
                Users user = GetUserById(useridentity);
                foreach (var selectSource in valueSource ? fieldproperty.Source.ValueSource : fieldproperty.Source.KeySource)
                {
                    searchlist = searchlist.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    if (!searchlist.Any())
                    {
                        continue;
                    }

                    if (selectSource.GetSource() is TableSource)
                    {
                        TableSource dataSource = selectSource.GetSource() as TableSource;
                        string accessrightId = GetFieldIdByName(dataSource.TableId, ClassLib.FieldNames.AccessRight);
                        //string accessrightfilter = $"{accessrightId}:(" + user.Roles.Select(x => $"/.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Modify.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.View.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Delete.ToString()}\\\"{regexclose}.*/").Join(" OR ") + ")";
                        string accessrightfilter = "";
                        string query = accessrightfilter;
                        if (dataSource.TableId.Equals(classid) && TableToFieldDic.ContainsKey(dataSource.TableId))
                        {
                            
                            string fieldparentId = TableToFieldNameDic[dataSource.TableId][StandardFormat.GetCleanName(ClassLib.FieldNames.ParentId)].id;
                            Dictionary<string, object> searchTerm = new Dictionary<string, object>();

                            StringBuilder sbFilterParent = new StringBuilder();
                            List<string> filterlist = new List<string>();
                            List<string> filterParentList = new List<string>();
                            string parentId = TableIdDic[dataSource.TableId].ParentId;
                            foreach (var filter in dataSource.Filter)
                            {

                                if (TableToFieldDic.ContainsKey(parentId) && TableToFieldDic[parentId].ContainsKey(filter.Key))
                                {
                                    //Filter for Parent Table                                   

                                    if (filter.Value != null && filter.Value.Any())
                                    {
                                        filterParentList.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");
                                    }
                                }
                                else if (TableToFieldDic.ContainsKey(dataSource.TableId))
                                {
                                    //Filter for it own Field
                                    if (TableToFieldDic[dataSource.TableId].ContainsKey(filter.Key))
                                    {
                                        filterlist.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");

                                    }
                                }

                            }
                            List<string> parentidFilter = new List<string>();
                            if (filterParentList.Any())
                            {
                                string queryParent = string.Join(" OR ", filterParentList);
                                List<Dictionary<string, object>> parentrecords = solrApi.DocQuery.CustomQuery(parentId, queryParent, new List<string>() { ClassLib.FieldNames.id }).docs;
                                parentidFilter = parentrecords.Select(x => x[FieldNames.id].ToString()).ToList<string>();
                            }

                            if (parentidFilter.Any())
                            {
                                query += $"{fieldparentId}:({string.Join(" OR ", parentidFilter)}) AND ";
                            }
                            if (filterlist.Any())
                            {
                                query += $"({string.Join(" AND ", filterlist)}) AND ";
                            }
                  
                            query = string.Join(" OR ", searchlist.Select(search => $"{dataSource.SelectedKeyField}:{search}"));


                            List<string> displaylist = GetAccessColumns(user, dataSource.TableId).Select(x => x.Name.Equals(FieldNames.id) ? FieldNames.id : x.id).ToList();
                                                  
                            if (!displaylist.Contains(dataSource.SelectedKeyField))
                            {
                                displaylist.Add(dataSource.SelectedKeyField);
                            }
                            if (!displaylist.Contains(fieldparentId))
                            {
                                displaylist.Add(fieldparentId);
                            }

                            items = solrApi.DocQuery.CustomQuery(dataSource.TableId, query, displaylist).docs;   
                        }

                    }
                    else if (selectSource.GetSource() is KeyValueSource)
                    {
                        KeyValueSource dataSource = selectSource.GetSource() as KeyValueSource;
                        items.Add(dataSource.Items.ToDictionary(x=> x.Key,y=> (object)y.Value));
                    }
                }
            }
            catch (Exception ex)
            {
                LogHandler.Error(ex);
            }
            return items;
        }

        internal Dictionary<string, string> GetLookupItems(FieldProperty fieldproperty,string search, int start = 0, int limit = int.MaxValue - 1, bool valueSource= false)
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            try
            {
                foreach (var selectSource in valueSource ? fieldproperty.Source.ValueSource :fieldproperty.Source.KeySource )
                {
                    if(selectSource.GetSource() is TableSource)
                    {
                        TableSource dataSource = selectSource.GetSource() as TableSource;
                        string accessrightId = GetFieldIdByName(dataSource.TableId, ClassLib.FieldNames.AccessRight);
                        //string accessrightfilter = $"{accessrightId}:(" + user.Roles.Select(x => $"/.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Modify.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.View.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Delete.ToString()}\\\"{regexclose}.*/").Join(" OR ") + ")";
                        string accessrightfilter = "";
                        string query = accessrightfilter;
                        if (TableToFieldDic.ContainsKey(dataSource.TableId))
                        {
                            List<string> fieldlist = dataSource.Fields.Where(x => TableToFieldDic[dataSource.TableId].ContainsKey(x)).ToList();
                            string fieldparentId = TableToFieldNameDic[dataSource.TableId][StandardFormat.GetCleanName(ClassLib.FieldNames.ParentId)].id;
                            Dictionary<string, object> searchTerm = new Dictionary<string, object>();

                            StringBuilder sbFilterParent = new StringBuilder();
                            List<string> filterlist = new List<string>();
                            List<string> filterParentList = new List<string>();
                            string parentId = TableIdDic[dataSource.TableId].ParentId;
                            foreach (var filter in dataSource.Filter)
                            {

                                if (TableToFieldDic.ContainsKey(parentId) && TableToFieldDic[parentId].ContainsKey(filter.Key))
                                {
                                    //Filter for Parent Table                                   

                                    if (filter.Value != null && filter.Value.Any())
                                    {
                                        filterParentList.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");
                                    }
                                }
                                else if (TableToFieldDic.ContainsKey(dataSource.TableId))
                                {
                                    //Filter for it own Field
                                    if (TableToFieldDic[dataSource.TableId].ContainsKey(filter.Key))
                                    {
                                        filterlist.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");

                                    }
                                }

                            }
                            List<string> parentidFilter = new List<string>();
                            if (filterParentList.Any())
                            {
                                string queryParent = string.Join(" OR ", filterParentList);
                                List<Dictionary<string, object>> parentrecords = solrApi.DocQuery.CustomQuery(parentId, queryParent, new List<string>() { ClassLib.FieldNames.id }).docs;
                                parentidFilter = parentrecords.Select(x => x[FieldNames.id].ToString()).ToList<string>();
                            }

                            if (parentidFilter.Any())
                            {
                                query += $"{fieldparentId}:({string.Join(" OR ", parentidFilter)}) AND ";
                            }
                            if (filterlist.Any())
                            {
                                query += $"({string.Join(" AND ", filterlist)}) AND ";
                            }
                            query += " (" + string.Join(" OR ", fieldlist.Select(x => $"{x}:*{search.Trim(new char[] { '*' })}*")) + ")";

                            List<string> displaylist = new List<string>();
                            displaylist.AddRange(fieldlist);
                            if (!displaylist.Contains(dataSource.SelectedKeyField))
                            {
                                displaylist.Add(dataSource.SelectedKeyField);
                            }
                            if (!displaylist.Contains(fieldparentId))
                            {
                                displaylist.Add(fieldparentId);
                            }

                            List<Dictionary<string, object>> results = solrApi.DocQuery.CustomQuery(dataSource.TableId, query, displaylist,start:start, rows: limit).docs;
                            Dictionary<string, string> idtoStringDic = new Dictionary<string, string>();

                            if (TableIdDic[dataSource.TableId].SelfReference)
                            {

                                List<string> parentlist = results.Where(x => x[fieldparentId].ToString() != "#").Select(x => x[fieldparentId].ToString()).ToList();
                                List<Dictionary<string, object>> parentsults = parentlist.Any() ?
                                    solrApi.DocQuery.CustomQuery(dataSource.TableId, $"id:({string.Join(" OR ", parentlist.ToArray())})", displaylist, rows: 20).docs
                                    : new List<Dictionary<string, object>>();

                                foreach (var x in parentsults)
                                {
                                    if (!idtoStringDic.ContainsKey(x[dataSource.SelectedKeyField].ToString()))
                                    {
                                        idtoStringDic[x[dataSource.SelectedKeyField].ToString()] = "";
                                        foreach (var field in dataSource.Fields)
                                        {
                                            idtoStringDic[x[dataSource.SelectedKeyField].ToString()] += x.ContainsKey(field) ? x[field].ToString() : "";
                                        }
                                    }
                                }
                            }
                            foreach (var result in results)
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (var field in dataSource.Fields)
                                {

                                    if (TableIdDic[dataSource.TableId].SelfReference && result[fieldparentId].ToString() != "#")
                                    {
                                        sb.Append(idtoStringDic[result[fieldparentId].ToString()] + "-");
                                    }
                                    if (fieldlist.Any(x => x.Equals(field, StringComparison.OrdinalIgnoreCase)))
                                    {
                                        if (!result.ContainsKey(field))
                                        {
                                            continue;
                                        }
                                        if (TypeCheck.IsLookUp(FieldIdDic[field].ControlType))
                                        {
                                            string text = GetLookupText(FieldIdDic[field].ParentId, field, result[field].ToString(), false);
                                            if (!string.IsNullOrWhiteSpace(text))
                                            {
                                                sb.Append(text);
                                            }
                                        }
                                        else
                                        {
                                            sb.Append(result[field]);
                                        }

                                    }
                                    else
                                    {
                                        sb.Append(field);
                                    }
                                }
                                items[result[dataSource.SelectedKeyField].ToString()] = sb.ToString();
                            }
                        }                 

                    }
                    else if (selectSource.GetSource() is KeyValueSource)
                    {
                        KeyValueSource dataSource = selectSource.GetSource() as KeyValueSource;
                        items = dataSource.Items;
                    }
                }
            }
            catch(Exception ex)
            {
                return new Dictionary<string, string>(){ { "Error",ex.ToString()} };
            }
            return items;
        }
        
        internal Dictionary<string,string> GetLookupItems(LookupQuery message)
        {
            try
            {
                Users user = GetUserByUserId(message.UserIdentity);
                Dictionary<string, string> findValue = new Dictionary<string, string>();
                if (!message.Searchlist.Any() || message.Searchlist.Any(x => string.IsNullOrEmpty(x)) || message.Searchlist.Any(x => x.ToString() == "null"))
                {
                    if (message.SearchbyId)
                    {
                        return new Dictionary<string, string>();                     
                    }
                    message.Searchlist = message.Searchlist.Select(x => (string.IsNullOrEmpty(x) || x.ToString() == "null") ? "*" : x).ToList();
                    if(!message.Searchlist.Any())
                    {
                        message.Searchlist = new List<string>() { "*" };
                    }
                }
                bool moreSources = message.Sources.Count >= 2;
                user.UserGroups.Add(user.id);
                user.UserGroups = user.UserGroups.Distinct().ToList();

                foreach (var search in message.Searchlist)
                {
                    foreach (var dataSource in message.Sources)
                    {
                        string accessrightId = GetFieldIdByName(dataSource.TableId, ClassLib.FieldNames.AccessRight);
                        //string accessrightfilter = $"{accessrightId}:(" + user.Roles.Select(x => $"/.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Modify.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.View.ToString()}\\\"{regexclose}.*/ OR /.*\\\"{x}\\\":{regexopen}\\\"{FieldAccess.Delete.ToString()}\\\"{regexclose}.*/").Join(" OR ") + ")";
                        string accessrightfilter = "";
                        string query = accessrightfilter;

                        if (TableToFieldDic.ContainsKey(dataSource.TableId))
                        {
                            List<string> fieldlist = dataSource.Fields.Where(x => TableToFieldDic[dataSource.TableId].ContainsKey(x)).ToList();
                            string fieldparentId = TableToFieldNameDic[dataSource.TableId][StandardFormat.GetCleanName(ClassLib.FieldNames.ParentId)].id;
                            Dictionary<string, object> searchTerm = new Dictionary<string, object>();



                            StringBuilder sbFilterParent = new StringBuilder();
                            List<string> filterlist = new List<string>();
                            List<string> filterParentList = new List<string>();
                            string parentId = TableIdDic[dataSource.TableId].ParentId;
                            foreach (var filter in dataSource.Filter)
                            {

                                if (TableToFieldDic.ContainsKey(parentId) && TableToFieldDic[parentId].ContainsKey(filter.Key))
                                {
                                    //Filter for Parent Table                                   

                                    if (filter.Value != null && filter.Value.Any())
                                    {
                                        filterParentList.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");
                                    }
                                }
                                else if (TableToFieldDic.ContainsKey(dataSource.TableId))
                                {
                                    //Filter for it own Field
                                    if (TableToFieldDic[dataSource.TableId].ContainsKey(filter.Key))
                                    {
                                        filterlist.Add("(" + string.Join(" OR ", filter.Value.Select(y => string.IsNullOrEmpty(y) ? $"{filter.Key}:**" : $"{filter.Key}:{y}")) + ")");

                                    }
                                }

                            }
                            List<string> parentidFilter = new List<string>();
                            if (filterParentList.Any())
                            {
                                string queryParent = string.Join(" OR ", filterParentList);
                                List<Dictionary<string, object>> parentrecords = solrApi.DocQuery.CustomQuery(parentId, queryParent, new List<string>() { ClassLib.FieldNames.id }).docs;
                                parentidFilter = parentrecords.Select(x => x[FieldNames.id].ToString()).ToList<string>();
                            }

                            if (parentidFilter.Any())
                            {
                                query += $"{fieldparentId}:({string.Join(" OR ", parentidFilter)}) AND ";
                            }
                            if (filterlist.Any())
                            {
                                query += $"({string.Join(" AND ", filterlist)}) AND ";
                            }
                            if (message.SearchbyId)
                            {
                                query = $"{dataSource.SelectedKeyField}:\"{search}\"";
                            }
                            else
                            {
                                query += " (" + string.Join(" OR ", fieldlist.Select(x => $"{x}:*{search.Trim(new char[] { '*' })}*")) + ")";
                            }
                            List<string> displaylist = new List<string>();
                            displaylist.AddRange(fieldlist);
                            if (!displaylist.Contains(dataSource.SelectedKeyField))
                            {
                                displaylist.Add(dataSource.SelectedKeyField);
                            }
                            if (!displaylist.Contains(fieldparentId))
                            {
                                displaylist.Add(fieldparentId);
                            }
                            Dictionary<string, string> lookup = new Dictionary<string, string>(); 
                            foreach (var field in dataSource.Fields)
                            {
                                if (TypeCheck.IsLookUp(FieldIdDic[field].ControlType))
                                {
                                    lookup  = GetLookupItems(new LookupQuery() { Sources = FieldIdDic[field].Source.KeySource.Select(x=> x.GetSource<TableSource>()).ToList(), Searchlist = message.Searchlist, Occur= message.Occur, QueryType = message.QueryType, SearchbyId = message.SearchbyId, UserIdentity = user.id });
                                    if (lookup.Any())
                                    {
                                        query = $"{query} OR ({string.Join(" OR " ,lookup.Keys.Select(x=> $"{field}:{x}"))})";
                                    }
                                }
                            }
                       
                            List<Dictionary<string, object>> results = solrApi.DocQuery.CustomQuery(dataSource.TableId, query, displaylist, rows: 20).docs;
                            Dictionary<string, string> idtoStringDic = new Dictionary<string, string>();

                            if (TableIdDic[dataSource.TableId].SelfReference)
                            {

                                List<string> parentlist = results.Where(x => x[fieldparentId].ToString() != "#").Select(x => x[fieldparentId].ToString()).ToList();
                                List<Dictionary<string, object>> parentsults = parentlist.Any() ?
                                    solrApi.DocQuery.CustomQuery(dataSource.TableId, $"id:({string.Join(" OR ", parentlist.ToArray())})", displaylist, rows: 20).docs
                                    : new List<Dictionary<string, object>>();

                                foreach (var x in parentsults)
                                {
                                    if (!idtoStringDic.ContainsKey(x[dataSource.SelectedKeyField].ToString()))
                                    {
                                        idtoStringDic[x[dataSource.SelectedKeyField].ToString()] = "";
                                        foreach (var field in dataSource.Fields)
                                        {
                                            idtoStringDic[x[dataSource.SelectedKeyField].ToString()] += x.ContainsKey(field) ? x[field].ToString() : "";
                                        }
                                    }
                                }
                            }
                            foreach (var result in results)
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (var field in dataSource.Fields)
                                {

                                    if (TableIdDic[dataSource.TableId].SelfReference && result[fieldparentId].ToString() != "#")
                                    {
                                        sb.Append(idtoStringDic[result[fieldparentId].ToString()] + "-");
                                    }
                                    if (fieldlist.Any(x => x.Equals(field, StringComparison.OrdinalIgnoreCase)))
                                    {
                                        if (!result.ContainsKey(field))
                                        {
                                            continue;
                                        }
                                        if (TypeCheck.IsLookUp(FieldIdDic[field].ControlType))
                                        {
                                            foreach(var value in result[field].CastToList())
                                            {
                                                string text = GetLookupText(FieldIdDic[field].ParentId, field, value, false);
                                                if (!string.IsNullOrWhiteSpace(text))
                                                {
                                                    sb.Append(text);
                                                }
                                            }
                                  
                                        }
                                        else
                                        {
                                            sb.Append(result[field]);
                                        }

                                    }
                                    else
                                    {
                                        sb.Append(field);
                                    }
                                }
                                findValue[result[dataSource.SelectedKeyField].ToString()] = sb.ToString();
                            }
                        }

                    }
                }
                string newtag = message.Searchlist.FirstOrDefault();
                if (message.Sources.Any(x => x.NewTag) && !string.IsNullOrWhiteSpace(newtag))
                {
                    findValue[newtag] = newtag;
                }

                return findValue;
            }
            catch (Exception ex)
            {
                return new Dictionary<string, string>() { { "ERR", ex.ToString() } };
            }
        }
        internal Users GetUserByUserId(string userId)
        {
            Users user = Query(Users.ClassID, new Dictionary<string, object>() { { Users.FieldsId.UserId, userId } }, strict: true).docs.FirstOrDefault().CastToObject<Users>();
            return user ?? Query(Users.ClassID, new Dictionary<string, object>() { { Users.FieldsId.id, Id.UsersAccount.PublisUser } }).docs.FirstOrDefault().CastToObject<Users>(); 
        }
        internal Users GetUserById(string id)
        {
            Users user = Query(Users.ClassID, new Dictionary<string, object>() { { Users.FieldsId.id, id } }, strict: true).docs.FirstOrDefault().CastToObject<Users>();
            return user ?? Query(Users.ClassID, new Dictionary<string, object>() { { Users.FieldsId.id, Id.UsersAccount.PublisUser } }).docs.FirstOrDefault().CastToObject<Users>();
        }
        public DataStructure(SolrAuth solrAuth):this()
        {
            solrApi = new SolrApi(solrAuth);
           
        }
        public void SetInitialize()
        {
            Initialize = true;
        }
        public void InitializeCompleted()
        {
            Initialize = false;
        }
        public DataStructure(SolrAuth solrAuth, string rootPath, string filePath, string fileuploadUrl,string fileDownloadUrl)
            : this(solrAuth)
        {
            RootPath = rootPath;
            FilesPath = filePath;
            FilesUploadUrl = fileuploadUrl;
            FilesDownloadUrl = fileDownloadUrl;
            solrApi = new SolrApi(solrAuth);
     
        }

   

        public void ClearCache()
        {
            TableToFieldDic.Clear();
            TableToFieldNameDic.Clear();
        }


        public string GetPluginsPath()
        {
            return RootPath + "Plugins\\";
        }
        public List<string> GetSubTableId(string tableId)
        {
            List<string> tableIds = TableIdDic.Where(x => x.Value.ParentId == tableId).Select(x=> x.Key).ToList();
            foreach(var table in tableIds.ToList())
            {
                tableIds.AddRange(GetSubTableId(table));
         
            }
            return tableIds;
        }

        public Dictionary<string, SubTableProperty> GetSubTablesId(string tableId,string recordId)
        {
            return TableIdDic.Where(x => x.Value.ParentId == tableId)
                                  .ToDictionary(
                                    x => x.Key,
                                  y => new SubTableProperty()
                                  {
                                      Name = y.Value.Name,
                                      TableId = y.Value.id,
                                      SearchTerm = new Dictionary<string, object>() { { GetFieldIdByName(y.Value.id, ClassLib.FieldNames.ParentId), recordId } }
                                  });
        }

        public Dictionary<string,string> GetFacet(string tableName)
        {
            //Footer
            //
            Dictionary<string, string> aggregatelist = new Dictionary<string, string>();
            foreach (var x in TableToFieldDic[tableName])
            {
                if(x.Value.Sum)
                {
                    aggregatelist.Add(Aggregate.sum.ToString() + ":" + x.Key, string.Format("{0}({1})", Aggregate.sum.ToString(),x.Key));               
                }
                else if(x.Value.Avg)
                {
                    aggregatelist.Add(Aggregate.avg.ToString() + ":" + x.Key, string.Format("{0}({1})", Aggregate.sum.ToString(), x.Key));
                }
                else if (x.Value.Min)
                {
                    aggregatelist.Add(Aggregate.min.ToString() + ":" + x.Key, string.Format("{0}({1})", Aggregate.sum.ToString(), x.Key));
                }
                else if (x.Value.Max)
                {
                    aggregatelist.Add(Aggregate.max.ToString() + ":" + x.Key, string.Format("{0}({1})", Aggregate.sum.ToString(), x.Key));
                }
            }      
            return aggregatelist;
        }




   
        public void Save(Users user,string tableId,List<object> obj)
        {
            List<Dictionary<string, object>> recordlist = new List<Dictionary<string, object>>();
            foreach(var x in obj)
            {
                recordlist.Add(x.ToDictionary(true));
            }
            Save(user, tableId, recordlist);
        }
        public Dictionary<string,object> ConvertToDic(string tableId,object obj)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            List<PropertyInfo> propertylist = obj.GetType().GetProperties().ToList();
            foreach (PropertyInfo p in propertylist)
            {
                PropertyInfo tableinfo = obj.GetType().GetProperty(p.Name, BindingFlags.Public | BindingFlags.Instance);
                if(p.GetCustomAttribute<JsonIgnoreAttribute>() !=null)
                {
                    continue;
                }
                JsonPropertyAttribute jpa = p.GetCustomAttribute<JsonPropertyAttribute>();
                if (p.Name == ClassLib.FieldNames.id)
                {
                    param.Add(p.Name, tableinfo.GetValue(obj));
                }
                else
                {
                    if (jpa == null)
                    {
                        continue;
                    }
                    string fieldid = jpa.PropertyName;
                    if (tableinfo != null)
                    {

                        if (p.PropertyType == typeof(DateTime))
                        {
                            //Any Date version Control use this 
                            //_dataStructure.DbConnection.InsertOrUpdate();                        
                            param.Add(fieldid, tableinfo.GetValue(obj).ToString());
                        }
                        else if (p.PropertyType == typeof(string))
                        {
                            param.Add(fieldid, tableinfo.GetValue(obj));
                        }
                        else if (p.PropertyType == typeof(int) ||
                                 p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                                 p.PropertyType == typeof(float))
                        {
                            //Any Control Configure sort by numeric use this query
                            param.Add(fieldid, tableinfo.GetValue(obj));
                        }
                        else if (p.PropertyType == typeof(bool))
                        {
                            //Unique must in varchar 50, and contain clean string
                            param.Add(fieldid, tableinfo.GetValue(obj));
                        }
                        else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
                            || p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                        {
                            param.Add(fieldid, JsonConvert.SerializeObject(tableinfo.GetValue(obj)));
                        }
                        else if (p.PropertyType.IsEnum || TypeCheck.IsReserveType(p.PropertyType))
                        {
                            param.Add(fieldid, tableinfo.GetValue(obj));
                        }
                        else
                        {
                            var settings = new JsonSerializerSettings();
                            settings.Converters.Add(new StringEnumConverter { });
                            settings.TypeNameHandling = TypeNameHandling.Auto;
                            param.Add(fieldid, JsonConvert.SerializeObject(tableinfo.GetValue(obj), settings));
                        }

                    }
                    else
                    {
                        Console.WriteLine("Skip Insert ");
                    }
                }
                
            }
            return param;
        }
        public void Save(Users user, string tableId, object obj)
        {
            Save(user, tableId, new List<Dictionary<string,object>>() { obj.ToDictionary() });
        }
        /// <summary>
        ///     Use when only have correct structure for retrive data
        /// </summary>
        /// <param name="datastructure"></param>
        /// <param name="tableId"></param>
        /// <param name="flatTable"></param>
        /// <returns></returns>
        /// 
        public int Save(Users user, string tableId, List<Dictionary<string, object>> dataList)
        {
            int UpdatedCnt = 0;
            var columnsMap = TableIdDic[tableId].UniquesList.Any()? TableIdDic[tableId].UniquesList.FirstOrDefault().Value : new List<string>();
            foreach (var record in dataList)
            {
                string id = ClassLib.FieldNames.id;
                string changeById = GetFieldIdByName(tableId, FieldNames.ChangedBy);
                string dateVersionId = GetFieldIdByName(tableId, FieldNames.DateVersion);
                string accessRightId = GetFieldIdByName(tableId, FieldNames.AccessRight);
                string parentIdid = GetFieldIdByName(tableId, FieldNames.ParentId);
                List<string> excludekeylist = record.Keys.Where(x => (FieldIdDic.ContainsKey(x) && FieldIdDic[x].ControlType == "Html") || !FieldIdDic.ContainsKey(x) && !x.Equals("id") ).ToList();
                if(excludekeylist.Any())
                {
                    foreach(var k in excludekeylist)
                    {
                        record.Remove(k);
                    }
                }
                if (!record.ContainsKey(changeById) || record[changeById] == null ||
                  (record[changeById] != null &&  (string.IsNullOrWhiteSpace(record[changeById].ToString()))
                  || record[changeById].ToString() == Id.UsersAccount.CurrentUser))
                {
                    record[changeById] = user.id;
                }
                else if(record.ContainsKey(changeById))
                {
                    record[changeById] = TypeCheck.CastToList(record[changeById]).FirstOrDefault();
                }
                if (!record.ContainsKey(dateVersionId) ||
                    (record[dateVersionId] != null && string.IsNullOrWhiteSpace(record[dateVersionId].ToString())))
                {
                    record[dateVersionId] = DateExtension.GetCurrentTimeVersion();
                }               


                if (record.ContainsKey(id) && Validate.IsGuid(record[id].ToString()))
                {
                    var dt = solrApi.DocQuery.SelectById(tableId, new List<string>() { record[id].ToString() });
                    if (dt.numFound == 1 )
                    {
                        var row = dt.docs.FirstOrDefault();
                        if(row.ContainsKey(accessRightId))
                        {
                            List<string> accessrow = GetAccessRight(row[accessRightId].ToString(), user.id, user.UserGroups);
                            if(!accessrow.Contains("Modify") && user.id != Id.UsersAccount.Developer)
                            {
                                continue;
                            }
                            if (!dt.docs.FirstOrDefault().ContainsKey(parentIdid))
                            {
                                record[parentIdid] = "#";
                            }
                            InsertOrUpdate(tableId, record, new List<string> { id });
                            UpdatedCnt++;
                        }
         

                    }
                    else if (dt.numFound == 0)
                    {
                        //Populate Default Value
                        foreach (var field in TableToFieldDic[tableId])
                        {
                            if (string.IsNullOrWhiteSpace(field.Value.DefaultValues))
                            {
                                continue;
                            }
                            if (!record.ContainsKey(field.Key))
                            {
                                if (field.Value.ControlType == Const.ControlTypes.DatetimePicker.ToString())
                                {
                                    try
                                    {                               
                                        DateTime date = RuntimeCompliler<DateTime>.EvalWithCache(field.Value.DefaultValues, field.Key);
                                        record[field.Key] = date.ToString(DateExtension.SystemDateFormat);
                                    }
                                    catch
                                    {
                                        record[field.Key] = DateTime.Now.ToString(DateExtension.SystemDateFormat);
                                    }
                        
                                }
                                else
                                {
                                    if (field.Value.MultipleValue)
                                    {
                                        record[field.Key] = TypeCheck.CastToList(field.Value.DefaultValues);
                                    }
                                    else
                                    {
                                        record[field.Key] = FieldIdDic[field.Key].DefaultValues;
                                    }
                                }

                            }
                        }
                        //Populate Access right
                        Dictionary<string, List<string>> accessRight = new Dictionary<string, List<string>>();
                        if(record.ContainsKey(accessRightId) && !string.IsNullOrWhiteSpace(record[accessRightId].ToString()))
                        {
                            try
                            {
                                accessRight = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>((record[accessRightId].ToString()));
                            }
                            catch
                            {

                            }
                        }
                        if (accessRight == null || !accessRight.Any() ||
                         //Assign Access Right if empty string
                            (accessRight.Count > 0 && !accessRight.Any(x => !string.IsNullOrWhiteSpace(x.Key))))
                        {
                            record[accessRightId] =Access.GetAccess(user.id,new List<string>() {"View","Modify" ,"Delete"});
                        }
                        else if (accessRight.ContainsKey(Id.UsersAccount.CurrentUser))
                        {
                            accessRight[user.id] = accessRight[Id.UsersAccount.CurrentUser];                    
                            accessRight.Remove(Id.UsersAccount.CurrentUser);
                            record[accessRightId] = JsonConvert.SerializeObject(accessRight);
                        }
                        if (!record.ContainsKey(parentIdid))
                        {
                            record[parentIdid] = "#";
                        }
                        //No records found just insert it with the mapping
                        InsertOrUpdate(tableId, record, columnsMap);
                        UpdatedCnt++;
                    }
                    else
                    {
                        LogHandler.Debug(string.Format("Duplicated Id Found {0} at Table {1}, Please Contact Administrator ",
                                record[id], tableId));
                    }
                }
                else
                {
                    LogHandler.Error("Fail to Insert Record. Invalid Id Format");
                }

            }
            CommitIfNeeded(tableId);
            CommitIfNeeded(TX.ClassID);
            return UpdatedCnt;
        }
        public ResponseDetail QueryById(string tableName,string id)
        {
            return Query(tableName, new Dictionary<string, object>() { { FieldNames.id, id } },start: 0, limit:1,strict: true);
        }
        public List<Dictionary<string, object>> QueryAll(string tableId)
        {
            return Query(tableId,limit:int.MaxValue,showResult: TableToFieldDic[tableId].Select(x=> x.Key).ToList()).docs;
        }
        public ResponseDetail Query(string tableName, Dictionary<string, object> searchTerms = null, List<string> showResult = null,Dictionary<string,string> sortTerm = null,string sortspecify= "", int start = 0,int limit = int.MaxValue - 1, string facet ="", bool strict = true,bool orQuery= false)
        {
            if(searchTerms == null)
            {
                searchTerms = new Dictionary<string, object>();
            }
            if(showResult == null)
            {
                showResult = new List<string>() {"*"};
            }
            if(sortTerm == null)
            {
                sortTerm = new Dictionary<string, string>();
            }
            searchTerms = searchTerms ?? new Dictionary<string, object>();
            //Search Term with Sort Term Together
            foreach(var sort in sortTerm)
            {
                if(!searchTerms.ContainsKey(sort.Key))
                {
                    searchTerms[sort.Key] = "";
                }
             
            }
            List<SearchTerm> st = new List<SearchTerm>();
            if (searchTerms.Any())
            {
                foreach (var s in searchTerms)
                {                   
                    bool multivalue = TypeCheck.IsMultiValue(s.Value);                   
                  
                    List<string> search = multivalue? (s.Value as IEnumerable).Cast<string>().ToList<string>() : new List<string>() { (s.Value ?? "").ToString()};
                    search = search.Select(x=> string.IsNullOrWhiteSpace(x)? "": x).ToList();
                    st.Add(new SearchTerm()
                    {
                        FieldId = s.Key,
                        search = search.Select(x=> x.ToString()).ToList(),
                        Sort = sortTerm.ContainsKey(s.Key) ? sortTerm[s.Key] : "",
                       
                        queryType = strict ? QueryType.ExactMatch:
                        (TableToFieldDic.ContainsKey(tableName) && TableToFieldDic[tableName].ContainsKey(s.Key) ? (QueryType)Enum.Parse(typeof(QueryType),TableToFieldDic[tableName][s.Key].QueryType) : QueryType.TextQuery)
                    });
                }
            }

            if (strict)
            {
                #region Strict              
                showResult.AddRange(st.Select(x => x.FieldId).ToList());
                ResponseDetail r = solrApi.DocQuery.Select(tableName, st, showResult, facet, limit, start,orQuery,bq:sortspecify);
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

                searchTerms = searchTerms.Where(x => TypeCheck.IsMultiValue(x.Value) ?
                (x.Value as IEnumerable).Cast<string>().ToList<string>().Any(y=> !string.IsNullOrWhiteSpace(y)) : 
                !string.IsNullOrWhiteSpace((x.Value?? "").ToString())).ToDictionary(z=> z.Key,y=> y.Value);

                foreach (var record in r.docs)
                {
                    bool exactMap = true;
                    foreach (var key in searchTerms.Keys)
                    {
                        var multivalueSearch = TypeCheck.IsMultiValue(searchTerms[key]);
                        var multivalueRecord = TypeCheck.IsMultiValue(record[key]);
                        var typesearch = searchTerms[key].GetType();
                        var typerecord = record.ContainsKey(key) ? record[key].GetType() : null;
                        if (typerecord == null)
                        {
                            exactMap = false;
                            break;
                        }

                        if (multivalueSearch && multivalueRecord)
                        {
                            var rowsearch = searchTerms[key] as List<string>;
                            var row = record[key] as List<string>;

                            if (rowsearch.Except(row).Any() || row.Except(rowsearch).Any())
                            {
                                exactMap = false;
                                break;
                            }
                        }
                        else if(multivalueSearch && !multivalueRecord)
                        {
                            var rowsearch = searchTerms[key] as List<string>;
                            var row = record[key] as string;                           
                            if (!rowsearch.Where(x=> x.Equals(row)).Any())
                            {
                                exactMap = false;
                                break;
                            }
                        }
                        else if(!multivalueSearch && multivalueRecord)
                        {
                            var row = record[key] as List<string>;
                            var rowsearch = searchTerms[key] as string;
                            if (!row.Where(x=> x.Equals(rowsearch)).Any())
                            {
                                exactMap = false;
                                break;
                            }
                        }
                        else if (!multivalueSearch && !multivalueRecord)
                        {
                            var row = record[key] as string;
                            var rowsearch = searchTerms[key] as string;
                            if (!rowsearch.Equals(row, StringComparison.OrdinalIgnoreCase))
                            {
                                exactMap = false;
                                break;
                            }
                        }
               
                    }
                    if (exactMap)
                    {
                        rows.Add(record);
                    }
                }
                r.docs = rows;
                return r;
                #endregion
            }
            else
            {
                ResponseDetail r = solrApi.DocQuery.Select(tableName, st, showResult, facet, limit, start, orQuery,bq:sortspecify);    
                return r;
            }

        }

        public string GetLookupKey(string tableName, string fieldname, string text,bool valueSource = false)
        {
            List<string> lookuopkeys = new List<string>();
            if (!string.IsNullOrWhiteSpace(text))
            {
                string[] textArray = text.Split(';');
                foreach (var s in textArray)
                {
                   
                    if (TableToFieldDic[tableName][fieldname].Source.KeySource.Any())
                    {
                        List<SelectSource> sources;
                        if (valueSource)
                        {
                            sources = TableToFieldDic[tableName][fieldname].Source.KeySource;
                        }
                        else
                        {
                            sources = TableToFieldDic[tableName][fieldname].Source.ValueSource;
                        }


                        foreach (var source in sources)
                        {
                            if(source.GetSource() is TableSource)
                            {
                                TableSource dataSource = source.GetSource() as TableSource;
                                List<Dictionary<string, object>> record = Query(dataSource.TableId, new Dictionary<string, object>() { { "", s } }).docs;
                                if (record.Count > 0)
                                {
                                    lookuopkeys.Add(record[0]["Id"].ToString());

                                }
                                
                            }
                            else if(source.GetSource() is KeyValueSource)
                            {
                                KeyValueSource dataSource = source.GetSource() as KeyValueSource;
                                var searchid = dataSource.Items.FirstOrDefault(x => x.Value.Equals(s));
                                if (!string.IsNullOrWhiteSpace(searchid.Key))
                                {
                                    lookuopkeys.Add(searchid.Key);
                                }
                            }
                            
                        }
                    }
         

                }

            }
            return JsonConvert.SerializeObject(lookuopkeys);
        }



        public void Delete(string tableName, string id, string userId)
        {
            Delete(tableName, new List<string> {id}, userId);
        }

        public List<string> Delete(string tableId, List<string> ids, string userId)
        {   
              var idsToDelete = ids;
            var path = string.Empty;
            var indexpath = string.Empty;
            if(idsToDelete.Count == 0)
            {
                return idsToDelete;
            }
            List<string> FilesFieldid = TableToFieldDic[tableId].Where(x => x.Value.ControlType == "FileUpload").Select(x => x.Value.id).ToList();
            foreach(var fieldId in FilesFieldid)
            {
                //Clear the Upload files tool after delete
                foreach(var folder in  UploadedFolders)
                {             
                    foreach (var deleteid in idsToDelete)
                    {
                        string filelocation = $"{folder}/{fieldId}/{deleteid}/";
                        if(Directory.Exists(filelocation))
                        {                 

                            foreach (var file in Directory.GetFiles(filelocation))
                            {                      
                                if (File.Exists(file))
                                {
                                    File.Delete(file);                               
                                }
                            }                    
                        }                  
                    }       
                }
            }
            if (TableIdDic.Where(x => x.Value.ParentId == tableId).Any())
            {
                foreach (var table in TableIdDic.Where(x => x.Value.ParentId == tableId))
                {
                    string childtableId = table.Value.id;
                    var rs = solrApi.DocQuery.Select(table.Value.id,new SearchTerm() { FieldId = GetFieldIdByName(childtableId, ClassLib.FieldNames.ParentId), search = idsToDelete, queryType= QueryType.ExactMatch },new List<string>() {"*"} );
                    if (rs != null || rs.docs.Count > 0)
                    {
                        Delete(childtableId, rs.docs.Select(x => x[FieldNames.id].ToString()).ToList(), userId);
                    }
                }
            }
            solrApi.Document.Delete(tableId, idsToDelete);
            foreach (var id in idsToDelete)
            {
                InsertTx(tableId, "id", id, userId, DateExtension.GetCurrentTimeVersion(), $"Deleted {id}");
            }
            CommitIfNeeded(tableId);
            return idsToDelete;


        }
        public void CommitIfNeeded(string tableId)
        {
            if(!Initialize)
            {
                if (TableIdDic.ContainsKey(tableId) && TableIdDic[tableId].RealTime)
                {
                    solrApi.Commit(tableId);
                }
            }
      
        }
        public void InsertOrUpdate(string tableId, Dictionary<string, object> updatevalue, List<string> mapList)
        {
            Dictionary<string,object> searchTerm = new Dictionary<string, object>();
            Dictionary<string, object> changedvalue = new Dictionary<string, object>();
            string accessrightid = GetFieldIdByName(tableId, FieldNames.AccessRight);
            string ChangedByid = GetFieldIdByName(tableId, FieldNames.ChangedBy);
           
            string DateVersionid = GetFieldIdByName(tableId, FieldNames.DateVersion);
            foreach (var map in mapList)
            {
                if(updatevalue.ContainsKey(map))
                {                  
                    if(TypeCheck.IsMultiValue(updatevalue[map]))
                    {
                        
                        searchTerm.Add(map, (updatevalue[map] as IEnumerable).Cast<string>().ToList<string>());
                    }
                    else
                    {
                        searchTerm.Add(map, updatevalue[map].ToString());
                    }                  
                }
            }
            ResponseDetail rd = Query(tableId, searchTerm, new List<string>() {"*"},new Dictionary<string, string>(),start:0,limit:1,strict:true);

            if (rd.docs.Count == 0)
            {
                if (!updatevalue.ContainsKey(accessrightid) && updatevalue.ContainsKey(ChangedByid))
                {
                    if(updatevalue[ChangedByid].ToString()== Id.UsersAccount.System)
                    {
                        updatevalue[accessrightid] = new Dictionary<string, List<FieldAccess>>() { { Id.UsersAccount.Developer, new List<FieldAccess>() { FieldAccess.Modify } },{ Id.UsersAccount.Admin, new List<FieldAccess>() { FieldAccess.Modify,FieldAccess.Delete } } };
                    }            
                    else
                    {
                        updatevalue[accessrightid] =Access.GetAccess(updatevalue[ChangedByid].ToString(), new List<string>() { "View", "Modify", "Delete" }) ;
                    }
                   
                }
                LogHandler.Debug($"#####Insert into {tableId}#######");
                LogHandler.Debug(updatevalue);
           
                solrApi.Document.Insert(tableId, updatevalue);
                //No required Tx for Initialize
                InsertTx(tableId, updatevalue);
          
                //CommitIfNeeded(tableId);
            }
            else if(rd.docs.Count == 1)
            {
                //Update
       
                //If return more than one row, will take the first row to update
                //Usually by upload.
                //Normal case row count == 1
          
                var dr = rd.docs[0];               

                foreach (var key in updatevalue.Keys.ToList())
                {
                    if(!dr.ContainsKey(key))
                    {
                        dr.Add(key, new object());
                    }           
                    
                    if (updatevalue.ContainsKey(key) && TypeCheck.IsMultiValue(updatevalue[key]))
                    {
                        var row = TypeCheck.CastToList(dr[key]);
                        var updatekey = (updatevalue[key] as IEnumerable).Cast<string>().ToList<string>();
                        //Data type is different then must update this field
                        if (TypeCheck.IsMultiValue(dr[key]))
                        {
                            //Check the value is match
                            //Remove it if does not match
                            if ((updatekey.Except(row).Any()|| row.Except(updatekey).Any()))
                            {
                                changedvalue.Add(key, updatekey);
                            }              
                        }
                        else
                        {
                            //Data Type Differen,just overwrite it
                            changedvalue.Add(key, updatevalue[key]);
                        }
                    }
                    else
                    {
                        var row = dr[key].ToString();
                        //Do not update those value have no changes
                        if (updatevalue[key] != null &&  !row.Equals(updatevalue[key].ToString(), StringComparison.CurrentCulture))
                        {
                            changedvalue.Add(key, updatevalue[key]);
                        }      
                    }


                }
                if (changedvalue.Any() )
                {
                    changedvalue[DateVersionid] = updatevalue[DateVersionid];
                    changedvalue[ChangedByid] = updatevalue[ChangedByid];            
                    changedvalue[ClassLib.FieldNames.id] = dr[ClassLib.FieldNames.id];
                    InsertTx(tableId, changedvalue);
                    LogHandler.Debug("Update Change Value");
                    foreach(var x in changedvalue)
                    {
                        LogHandler.Debug($"{x.Key}:{x.Value.ToString()}");
                    }
                    LogHandler.Debug($"#####Update into {tableId}#######");
                    LogHandler.Debug(updatevalue);
                    solrApi.Document.Update(tableId,updatevalue);

                }
                //CommitIfNeeded(tableId);
            }
            else
            {
                LogHandler.Debug("Could not update or insert . InsertOrUpdate Count > 1. SearchTerm :" + JsonConvert.SerializeObject(searchTerm));
            }
        }
        public int GetTxCount(string tableId, string fieldId, string id)
        {
            try
            {                    
                return Query(TX.ClassID, new Dictionary<string, object>() { {TX.FieldsId.RecordId, id },{TX.FieldsId.FieldId, fieldId } }, start:0,limit: 1,strict:true).numFound;
            }
            catch(Exception ex)
            {
                LogHandler.Error(ex);
            }
            return 0;
            
        }
        public bool IsRecordUnique(string tableId,Dictionary<string,object> record)
        {
            foreach(var x in TableIdDic[tableId].UniquesList.Keys)
            {
                string query = string.Join(" AND " ,TableIdDic[tableId].UniquesList[x].Select(y=>
                $"( {y}:{ string.Join( " AND ", TypeCheck.CastToList(record[y]))}) " 
                ) );
            }
            return true;
        }
        private void InsertTx(string tableid, string fieldId, string id,  string changedby, string dateVersion, object value)
        {

            if (TableIdDic.ContainsKey(tableid))
            {
                Dictionary<string, object> tx = new Dictionary<string, object>();
                tx.Add(FieldNames.id, Unique.NewGuid());
                tx.Add(TX.FieldsId.ParentId, "#");
                tx.Add(TX.FieldsId.ChangedBy, changedby);
                tx.Add(TX.FieldsId.DateVersion, dateVersion);
                tx.Add(TX.FieldsId.RecordId, id);
                tx.Add(TX.FieldsId.FieldId, fieldId);
                
                tx.Add(TX.FieldsId.AccessRight, FieldIdDic[TX.FieldsId.AccessRight].DefaultValues);
                if (TypeCheck.IsMultiValue(value))
                {
                    tx.Add(TX.FieldsId.Value, JsonConvert.SerializeObject(value));
                }              
                else
                {
                    tx.Add(TX.FieldsId.Value,value);
                }

                solrApi.Document.Insert(TX.ClassID, tx);

            }

        }

        public void InsertTx(string tableid, Dictionary<string, object> keyvalue)
        {     
            if (!Initialize)
            {
                foreach (var key in keyvalue.Keys.Where(x => !x.Equals(ClassLib.FieldNames.id)))
                {
                    InsertTx(tableid, key, keyvalue[ClassLib.FieldNames.id].ToString(), keyvalue[GetFieldIdByName(tableid, ClassLib.FieldNames.ChangedBy)].ToString(), keyvalue[GetFieldIdByName(tableid, ClassLib.FieldNames.DateVersion)].ToString(), keyvalue[key]);
                }
                
            }
        }

       
        public List<string> GetAccessMode(string tableName, Users user, string fieldName)
        {
            if (!TableToFieldDic.ContainsKey(tableName) || !TableToFieldDic[tableName].ContainsKey(fieldName))
            {
                return new List<string>();
            }
            if(user.UserGroups.Contains(Id.Role.Developer))
            {
                return new List<string>() { "Modify", "Delete" } ;
            }
            return TableToFieldDic[tableName][fieldName].GetAccess(user.UserGroups,user.id);
        }
        public Dictionary<bool,Dictionary<string, Dictionary<string, string>>> GetLookupTextDic(string tableId,List<string> fields,List<Dictionary<string,object>> docs,string hostUrl)
        {
            Dictionary<bool,Dictionary<string, Dictionary<string, string>>> lookupDic = new Dictionary<bool, Dictionary<string, Dictionary<string, string>>>();
            if(!docs.Any())
            {
                return lookupDic;
            }
            lookupDic[true] = new Dictionary<string, Dictionary<string, string>>();
            lookupDic[false] = new Dictionary<string, Dictionary<string, string>>();
            foreach (var f in fields)
            {
                Dictionary<bool,List<string>> IsvalueSourcesSearch = new Dictionary<bool,List<string>>();
                IsvalueSourcesSearch[false] = new List<string>();
             
                lookupDic[true][f] = new Dictionary<string, string>();         
                lookupDic[false][f] = new Dictionary<string, string>();
                
                switch (TableToFieldDic[tableId][f].ControlType)
                {
                    case Const.ControlTypes.KeyValueBox:
                        IsvalueSourcesSearch[false] = new List<string>();
                        IsvalueSourcesSearch[true] = new List<string>();
                        List<Dictionary<string, object>> keyvaluelist= docs.Where(x=> x.ContainsKey(f) && !string.IsNullOrWhiteSpace(x[f].ToString())).Select(x => (Dictionary<string, object>)TypeCheck.GetObjectFromStr(x[f].ToString(), typeof(Dictionary<string, object>))).ToList();
                        foreach(var x in keyvaluelist)
                        {
                            IsvalueSourcesSearch[false].AddRange(x.Keys);
                        }
                        IsvalueSourcesSearch[false] = IsvalueSourcesSearch[false].Distinct().ToList();
                        foreach (var x in keyvaluelist)
                        {
                            foreach(var y in x)
                            {
                                IsvalueSourcesSearch[true].AddRange(TypeCheck.CastToList(y.Value));
                            }                         
                        }
                        IsvalueSourcesSearch[true] = IsvalueSourcesSearch[true].Distinct().ToList();
                        break;
                    default:
                        foreach (var r in docs)
                        {
                            if (r.ContainsKey(f) && !string.IsNullOrWhiteSpace(r[f].ToString()))
                            {
                                IsvalueSourcesSearch[false].AddRange(TypeCheck.CastToList(r[f]));
                            }
                          
                        }
                        IsvalueSourcesSearch[false] = IsvalueSourcesSearch[false].Distinct().ToList();
                        break;
                }
               
                foreach (var isvalueSource in IsvalueSourcesSearch)
                {
                  
                    List<SelectSource> sources;
                    if (isvalueSource.Key)
                    {
                        sources = TableToFieldDic[tableId][f].Source.ValueSource;
                    }
                    else
                    {
                        sources = TableToFieldDic[tableId][f].Source.KeySource;
                    }
                 
                    foreach (var source in sources)
                    {
                        if (source.GetSource() is TableSource)
                        {
                            TableSource dataSource = source.GetSource() as TableSource;
                            List<Dictionary<string, object>> recorddic;
                           
                            List<string> showList = dataSource.Fields.Where(x => TableToFieldDic[dataSource.TableId].ContainsKey(x)).ToList();
                            if(!showList.Any() || !isvalueSource.Value.Any())
                            {
                                continue;
                            }
                            if (!showList.Contains(dataSource.SelectedKeyField))
                            {
                                showList.Add(dataSource.SelectedKeyField);
                            }
                            recorddic = solrApi.DocQuery.Select(dataSource.TableId, new SearchTerm() { FieldId = dataSource.SelectedKeyField, search = isvalueSource.Value, queryType = QueryType.ExactMatch }, showList).docs;

                            //var record =  Query(dataSource.TableName, new Dictionary<string, object>() { {BaseField.id.ToString(),id } }, new List<string>() { "*" }, new Dictionary<string, string>(), 0, 10000, "", true).docs.FirstOrDefault();
                            if (recorddic != null)
                            {

                                foreach (Dictionary<string, object> x in recorddic)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    foreach (var field in dataSource.Fields)
                                    {
                                        if(!x.ContainsKey(field))
                                        {
                                            continue;
                                        }
                                        if (TypeCheck.IsLookUp(FieldIdDic[field].ControlType))
                                        {
                                            string text = GetLookupText(FieldIdDic[field].ParentId, field, x[field].ToString(), false);
                                            if (!string.IsNullOrWhiteSpace(text))
                                            {
                                                sb.Append(text);
                                            }
                                        }
                                        else
                                        {
                                            sb.Append(x[field].ToString());
                                        }
                                    }
                                    lookupDic[isvalueSource.Key][f][x[dataSource.SelectedKeyField].ToString()] =  sb.ToString();
                                }
                            }
                        }
                        else if (source.GetSource() is KeyValueSource)
                        {
                            KeyValueSource dataSource = source.GetSource() as KeyValueSource;
                            lookupDic[isvalueSource.Key][f] = dataSource.Items;
                        }
                        else if(source.GetSource() is string)
                        {
                            string Url = source.GetSource().ToString();
                            if (!string.IsNullOrWhiteSpace(Url))
                            {
                                string host = hostUrl;
                                if (!Url.ToLower().Contains("http"))
                                {
                                    if(hostUrl.LastIndexOf("/") == hostUrl.Length - 1 && Url.IndexOf("/") == 0)
                                    {
                                        host = hostUrl.Remove(hostUrl.LastIndexOf("/"), 1) + Url;
                                    }
                                    else
                                    {
                                        host = hostUrl + Url;
                                    }
                                  
                                }
                                lookupDic[isvalueSource.Key][f] = new Dictionary<string, string>();
                                foreach (var search in isvalueSource.Value)
                                {
                                    RestApiResponse msr = ServerApi.Post(host, new ApiSelectArgument() { search = new string[] { search }, searchbykey = true, dependent = "{}" });
                                    if(!string.IsNullOrEmpty(msr.Error))
                                    {
                                        LogHandler.Error(msr.Error);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            Task<string> result = msr.Result.Content.ReadAsStringAsync();
                                            List<Dictionary<string, object>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.Result);
                                            List<Dictionary<string, string>> obj = (temp != null && !temp.Any()) ? new List<Dictionary<string, string>>() : (from x in temp select x.ToDictionary(y => y.Key, z => z.Value.ToString())).ToList();
                                            if (obj != null && obj.FirstOrDefault() != null && obj.FirstOrDefault().ContainsKey("id") && obj.FirstOrDefault().ContainsKey("text"))
                                            {
                                                //Console.WriteLine($"Id {obj.FirstOrDefault()[FieldNames.id]}  Text : {obj.FirstOrDefault()["text"]}");
                                                lookupDic[isvalueSource.Key][f][obj.FirstOrDefault()[ClassLib.FieldNames.id]] = obj.FirstOrDefault()["text"];
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            LogHandler.Error(ex);
                                        }
                            
                                    }
                

                                }                         
                              

                            }
                        }
                     
                    }

                }
               
            }

    

            return lookupDic;           
        }
        public string GetLookupText(string tableName,string fieldname, string value,bool valueSource = false)
        {
            List<SelectSource> sources;
            if(!TableToFieldDic[tableName].ContainsKey(fieldname))
            {
                return "";
            }
            if(valueSource)
            {
                sources = TableToFieldDic[tableName][fieldname].Source.ValueSource;
            }
            else
            {
                sources = TableToFieldDic[tableName][fieldname].Source.KeySource;
            }
            bool multipleSource = sources.Count() >= 2;
            foreach (var source in sources)
            {
                if(source.GetSource() is TableSource)
                {
                    TableSource dataSource = source.GetSource() as TableSource;
                    Dictionary<string,object> record;
                    if (dataSource.SelectedKeyField == FieldNames.id)
                    {
                         record = solrApi.DocQuery.SelectById(dataSource.TableId, value, dataSource.Fields).docs.FirstOrDefault();
                    }
                    else
                    {
                        record = solrApi.DocQuery.Select(dataSource.TableId, new SearchTerm() { FieldId = dataSource.SelectedKeyField, search = new List<string>() { value }, queryType = QueryType.TextQuery }, null,orQuery:true).docs.FirstOrDefault();
                    }
                   
                    //var record =  Query(dataSource.TableName, new Dictionary<string, object>() { {BaseField.id.ToString(),id } }, new List<string>() { "*" }, new Dictionary<string, string>(), 0, 10000, "", true).docs.FirstOrDefault();
                    if (record != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var field in dataSource.Fields)
                        {
                            if (TypeCheck.IsLookUp(FieldIdDic[field].ControlType))
                            {
                                string text = GetLookupText(FieldIdDic[field].ParentId, field, record[field].ToString(), false);
                                if (!string.IsNullOrWhiteSpace(text))
                                {
                                    sb.Append(text);
                                }
                            }
                            else
                            {
                                sb.Append(record[field]);
                            }
                        }

                        return sb.ToString();
                    }
                }
                else if(source.GetSource() is KeyValueSource)
                {
                    KeyValueSource dataSource = source.GetSource() as KeyValueSource;
                    if(dataSource.Items.ContainsKey(value))
                    {
                        return  dataSource.Items[value];
                    }
                }
            }
          

            return value;
        }

        public object GetTextById(string tableId,string fieldname,string id)
        {
            Dictionary<string, object> record = Query(tableId, new Dictionary<string, object>() { { FieldNames.id, id } }, new List<string>() { "*" }, new Dictionary<string, string>(),start: 0,limit: 1, strict: true).docs.FirstOrDefault();
            return record != null ? record[fieldname] : "";
        }
        public string GetTextById(string tableName,List<string>fieldsname, string id)
        {
            Dictionary<string, object> record = Query(tableName, new Dictionary<string, object>() { { FieldNames.id, id } }, new List<string>() { "*" }, new Dictionary<string, string>(), start:0,limit: 1,strict: true).docs.FirstOrDefault();
            if(record == null)
            {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            foreach (var field in fieldsname)
            {
                if (record.ContainsKey(field))
                {
                    sb.Append(record[field]);
                }
                else
                {
                    sb.Append(field);
                }
              
            }
            return record != null ? sb.ToString() : "";
        }


        private static object CreateInstance(string tableName)
        {
            object temp = null;
            switch (tableName)
            {
                case TableProperty.ClassID:
                    temp = new TableProperty();
                    break;
                case TableProperty.FieldProperty.ClassID:
                    temp = new FieldProperty();
                    break;
                case MenuProperty.ClassID:
                    temp = new MenuProperty();
                    break;
                case Pages.ClassID:
                    temp = new Pages();
                    break;

            }

            return temp;
        }


        public void GenerateDefaultPermission(ref FieldProperty fieldProperty)
        {
            List<string> baseColumnDeveloper = new List<string>() { ClassLib.FieldNames.id, ClassLib.FieldNames.ParentId, ClassLib.FieldNames.ChangedBy};     
            fieldProperty.AccessRight = new Dictionary<string, List<string>>();
            //Developer Can Do Anythings
            fieldProperty.AccessRight.Add(Id.Role.Developer, new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" });
            if (!baseColumnDeveloper.Contains(fieldProperty.Name))
            {                
                if (fieldProperty.Name.Equals(FieldNames.DateVersion))
                {
                    fieldProperty.AccessRight.Add(Id.Role.Admin, new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" });
                }
                else if (fieldProperty.Name.Equals(FieldNames.AccessRight))
                {
                    fieldProperty.AccessRight.Add(Id.Role.Users, new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" });
                }  
                else
                {
                    fieldProperty.AccessRight.Add(Id.Role.Admin, new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" });
                }
            }         
        }
        public TableProperty CreateTableProperty(string id, string name, string parentId, bool enable, int rowNo, string tableType, bool realtime = true, bool visible = true)
        {
            return new TableProperty
            {
                id = id,
                ParentId = parentId,
                Enable = enable,
                RowNo = rowNo,
                Name = name,
                TableType = tableType,
                RealTime = realtime,
                Visible = visible,
                AccessRight = new Dictionary<string, List<string>>()
                
            };
        }
        public FieldProperty AddField(string name, string parentId, string controlType = Const.ControlTypes.TextBox, DataSource dataSource = null, bool multipleValue = false, Dictionary<string, List<string>> accessRight = null)
        {          
            string id = Unique.NewGuid();
           return AddField(id,name, parentId, controlType , dataSource , multipleValue,  accessRight);           
      
        }
        public FieldProperty AddField(string id, string name, string parentId, string controlType = Const.ControlTypes.TextBox, DataSource dataSource = null, bool multipleValue = false, Dictionary<string, List<string>> accessRight = null)
        {

            var rowNo = FieldIdDic.Values.Any(x => x.ParentId == parentId) ? (FieldIdDic.Values.Where(x => x.ParentId == parentId).Max(y => y.RowNo) + 1) : 1;
            string cleanname = StandardFormat.GetCleanName(name);
            FieldProperty fieldprop;
            if (cleanname.Equals(FieldNames.Button))
            {
                fieldprop = new FieldProperty()
                {
                    id = id,
                    ParentId = parentId,
                    Name = name,
                    RowNo = rowNo,
                    ControlType = Const.ControlTypes.Html.ToString(),
                    Source = dataSource ?? new DataSource(),
                    MultipleValue = false,
                    DataType = "text_general",
                    Enable = true,
                    QueryType = QueryType.TextQuery.ToString(),
                    DefaultValues = ""
                };               
            }
            else
            {
                switch (StandardFormat.GetCleanName(name))
                {
                    case"RowNo":
                        fieldprop = new FieldProperty()
                        {
                            id = id,
                            ParentId = parentId,
                            ControlType = controlType,
                            Name = name,
                            RowNo = rowNo,
                            DataType = "pint",
                            QueryType = GetQueryTypeByControlType(controlType).ToString(),
                            Source = dataSource ?? new DataSource(),
                            MultipleValue = multipleValue,
                            UseUserDateFormat = true,
                            Enable = true,
                            DefaultValues = "0",
                            AccessRight = accessRight
                        };
                        break;
                    case "AccessRight":                   

                        fieldprop = new FieldProperty()
                        {
                            id = id,
                            ParentId = parentId,
                            ControlType = controlType.ToString(),
                            Name = name,
                            RowNo = rowNo,
                            DataType = "string",
                            Source = dataSource??new DataSource(),
                            MultipleValue = multipleValue,
                            UseUserDateFormat = true,
                            DefaultValues = Access.GetAccess(Id.UsersAccount.CurrentUser, new List<string>() { "BJABCFsIBCEEBGEEsGstsrrAuBsvtuItvt", "BJBruAJsFBCAJIEArGsJJJtsuBAGrCrstr", "BJwtIBCDBEFEHGEHtDJrurDtJDIBEICEBv" }),
                            Enable = true,
                            AccessRight = accessRight,
                            QueryType = QueryType.CustomMatch.ToString(),                 

                        };                 
                        break;

                    case "ParentId":
                        fieldprop = new FieldProperty()
                        {
                            id = id,
                            ParentId = parentId,
                            ControlType = controlType.ToString(),
                            Name = name,
                            RowNo = rowNo,
                            DataType = "text_general",
                            QueryType = GetQueryTypeByControlType(controlType).ToString(),
                            Source = dataSource ?? new DataSource(),
                            MultipleValue = multipleValue,
                            UseUserDateFormat = true,
                            Enable = true,
                            DefaultValues = "",
                            AccessRight = accessRight
                        };
                        break;
                    case "DateVersion":
                        fieldprop = new FieldProperty()
                        {
                            id = id,
                            ParentId = parentId,
                            ControlType = controlType.ToString(),
                            Name = name,
                            RowNo = rowNo,
                            DataType = "text_general",
                            QueryType = GetQueryTypeByControlType(controlType).ToString(),
                            Source = dataSource ?? new DataSource(),
                            MultipleValue = multipleValue,
                            UseUserDateFormat = true,
                            Enable = true,
                            DefaultValues = "",
                            Options = new CodeEditor(""),
                            AccessRight = accessRight
                        };
                        break;
                    default:
                        fieldprop = new FieldProperty()
                        {
                            id = id,
                            ParentId = parentId,              
                            ControlType = controlType.ToString(),
                            Name = name,
                            RowNo = rowNo,
                            DataType = "text_general",
                            QueryType = GetQueryTypeByControlType(controlType).ToString(),
                            Source = dataSource?? new DataSource(),
                            MultipleValue = multipleValue,
                            UseUserDateFormat = true,
                            Enable = true,
                            DefaultValues = "",
                            AccessRight = accessRight
                        };                  
                        break;

                }
            }
 
            if (accessRight == null 
                || name == ClassLib.FieldNames.ChangedBy || name == ClassLib.FieldNames.id || name == ClassLib.FieldNames.ParentId
                )
            {
                GenerateDefaultPermission(ref fieldprop);
            }
            else
            {
                fieldprop.AccessRight = accessRight;
            }
            FieldIdDic.Add(fieldprop.id, fieldprop);  
   
            return fieldprop;
        }
        public QueryType GetQueryTypeByControlType(string control)
        {
            QueryType querytype = QueryType.TextQuery;
            switch (control)
            {
                case Const.ControlTypes.Html:
                    break;
                case Const.ControlTypes.TextBox:
                    break;
                case Const.ControlTypes.CheckBox:
                case Const.ControlTypes.SwitchButton: 
                    querytype = QueryType.ExactMatch;
                    break;
                case Const.ControlTypes.SummerNote:
                    break;
                case Const.ControlTypes.CodeEditor:
                    break;
                case Const.ControlTypes.ColorPicker:
                    querytype = QueryType.ExactMatch;
                    break;
                case Const.ControlTypes.DatetimePicker:
                    querytype = QueryType.RangeQuery;
                    break;
                case Const.ControlTypes.InputMask:                    
                    break;
                case Const.ControlTypes.RadioButton:
                    break;
                case Const.ControlTypes.ComboBox:
                    querytype = QueryType.ExactMatch;
                    break;
                case Const.ControlTypes.CheckList:
                    querytype = QueryType.ExactMatch;
                    break;
                case Const.ControlTypes.SortableComboBox:
                    querytype = QueryType.ExactMatch;
                    break;
                case Const.ControlTypes.FileUpload:
                    break;        
                case Const.ControlTypes.KeyValueBox:
                    querytype = QueryType.CustomMatch;
                    break;
                default:
                    querytype = QueryType.ExactMatch;
                    break;
            };

            return querytype;
        }
     

        public List<string> GetAccessRight(string accessString, string userid,List<string> roles)
        {
            if(userid == Id.UsersAccount.Developer)
            {
                return new List<string>() { "View", "Modify", "Delete" };

            }
            List<string> idlist = new List<string>();

            idlist.AddRange(roles);
            idlist.Add(userid);
            idlist.Add(Id.Role.Public);
            List<string> access = new List<string>();
            Dictionary<string, List<string>> accessright = string.IsNullOrWhiteSpace(accessString) ? new Dictionary<string, List<string>>() :
                JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(accessString);
            foreach(var x  in accessright)
            {
                if(idlist.Contains(x.Key))
                {
                    access.AddRange(x.Value);
                }
            }         

            return access.Distinct().ToList();
        }
        public string GetFieldIdByName(string tableId, string fieldName)
        {
            string fieldnameclean = StandardFormat.GetCleanName(fieldName);
            if(fieldnameclean == ClassLib.FieldNames.id)
            {
                return ClassLib.FieldNames.id;
            }
            return TableToFieldNameDic[tableId][fieldnameclean].id;   
        }
        public string GetFullFieldNameClean(string tableId, string fieldName)
        {
            return StandardFormat.GetCleanName($"{GetFullTablename(tableId)}_{fieldName}");
        }

        public string GetFullTablenameClean(string tableId)
        {
            return StandardFormat.GetCleanName(GetFullTablename(tableId));
        }
        public string GetFullTablename(string tableId)
        {
            if(tableId == "#")
            {
                return "";
            }
            if (!TableIdDic.ContainsKey(tableId))
            {
                throw new Exception($"Could not find tableid {tableId}");
            }
            if (!string.IsNullOrWhiteSpace(tableId) && TableIdDic[tableId].ParentId != "#")
            {
                return GetFullTablename(TableIdDic[tableId].ParentId) + $"_{TableIdDic[tableId].Name}";
            }
          
            return TableIdDic[tableId].Name;
        }

        public FieldProperty AddFieldTX(string name, string parentId ,string controlType = Const.ControlTypes.TextBox,bool multipleValue =false)
        {
            string id = Unique.NewGuid();

            FieldProperty fieldprop = new FieldProperty()
            {
                id = id,
                ParentId = parentId,              
                ControlType = controlType.ToString(),
                Name = name,              
                DataType = "text_general",
                MultipleValue = multipleValue,
                UseUserDateFormat = true,
                AccessRight = new Dictionary<string, List<string>>(),
                Enable = true,
                DefaultValues = Access.GetAccess(Id.UsersAccount.CurrentUser, "Modify")
            };
          
            fieldprop.AccessRight.Add(Id.Role.Developer, new List<string>() { "View", "Modify", "Delete" });
            fieldprop.AccessRight.Add(Id.UsersAccount.Developer, new List<string>() { "View", "Modify", "Delete" });
            FieldIdDic.Add(fieldprop.id, fieldprop);   

            return fieldprop;
        }

        public void AddBaseFields( string parentId,Dictionary<string,List<string>> accessright= null)
        {
            foreach (var valColumn in BaseDataExt.BaseColumn(false))
            {
                string targetcloneId = "";
                switch (valColumn.Key) 
                {
                    case ClassLib.FieldNames.id:
                        targetcloneId = TableToFieldDic[TableProperty.FieldProperty.ClassID][FieldProperty.FieldsId.id].id;
                        break;
                    case ClassLib.FieldNames.AccessRight:                      
                            targetcloneId = FieldProperty.FieldsId.AccessRight;    
                        break;
                    case ClassLib.FieldNames.ChangedBy:
                        {
                            targetcloneId = FieldProperty.FieldsId.ChangedBy;
                        }
                        break;
                    case ClassLib.FieldNames.ParentId:
                        targetcloneId = FieldProperty.FieldsId.ParentId;
                        break;
                    case ClassLib.FieldNames.DateVersion:
                        targetcloneId = FieldProperty.FieldsId.DateVersion;
                        break;
                    case ClassLib.FieldNames.Button:
                        targetcloneId = FieldProperty.FieldsId.Button;
                        break; 
                    default:
                        continue;                     
                   
                }
                FieldProperty f = FieldIdDic[targetcloneId].Clone<FieldProperty>();
                f.id = Unique.NewGuid();
                f.ParentId = parentId;
                FieldIdDic[f.id] = f;
            }

        }

        public List<FieldProperty> GetAccessColumns(Users user,string tableName)
        {
         
            return TableToFieldDic[tableName].Where(x => x.Value.HasAccess(user.UserGroups,user.id)).Select(x => x.Value).OrderBy(x => x.RowNo).ToList();
        }
        public List<string> GetFullLevelMenuIdByName(string searchMenu, Users user, string parentId = null)
        {
            List<string> idlist = MenuIdDic.Where(x => x.Value.Name.ToLower().Contains(searchMenu.ToLower())).Select(x => x.Value.id).ToList();

            List<string> fullmenuidlist = GetFullLevelMenuIdByIdList(idlist, user);
            return fullmenuidlist;
        }
        public List<string> GetFullLevelMenuIdByIdList(List<string> childId, Users user)
        {
            List<string> idFound = new List<string>();

            foreach (var id in childId)
            {
                string parentId = MenuIdDic[id].ParentId;
                if (parentId != "#" && !string.IsNullOrWhiteSpace(parentId) && !idFound.Contains(parentId))
                {
                    idFound.Add(MenuIdDic[id].ParentId);
                }
            }
            if (idFound.Any())
            {
                List<string> accessableId = idFound.Where(x => MenuIdDic.ContainsKey(x) && MenuIdDic[x].HasAccess(user.UserGroups, user.id)).ToList();
                idFound = GetFullLevelMenuIdByIdList(accessableId, user);
                childId.AddRange(idFound);
                return childId.Distinct().ToList();
            }
            else
            {
                List<string> accessableId = childId.Where(x => MenuIdDic.ContainsKey(x) && MenuIdDic[x].HasAccess(user.UserGroups, user.id)).ToList();

                return accessableId;
            }
        }
        public List<string> GetFullLevelIdByName(string propertyName, Users user)
        {
           
            List<string> FieldIdFound = FieldIdDic.Where(x => x.Value.Name.ToLower().Contains(propertyName.ToLower())).Select(x => x.Value.id).ToList();
            List<string> TableIdFound = TableIdDic.Where(x =>( x.Value.Name.ToLower().Contains(propertyName.ToLower())) || FieldIdFound.Any(y=> FieldIdDic[y].ParentId == x.Value.id) ).Select(x => x.Value.id).ToList();
            List<string> fulltableList = GetFullLevelTableIdByIdList(TableIdFound, user);
            FieldIdFound.AddRange(fulltableList);
            return FieldIdFound;
            //return GetFullLevelTableIdByIdList(TableIdFound, user); 
            //return idFound.Where(x => (FieldIdDic.ContainsKey(x) && tableIdFound.Contains(FieldIdDic[x].ParentId)) || (TableIdDic.ContainsKey(x) && tableIdFound.Contains(TableIdDic[x].ParentId))).ToList();

        }
        public List<string> GetFullLevelTableIdByIdList(List<string> childId, Users user)
        {
            List<string> idFound = new List<string>();

            foreach(var id in childId)
            {
                string parentId = TableIdDic[id].ParentId;
                if (parentId != "#" && !string.IsNullOrWhiteSpace(parentId) && !idFound.Contains(parentId))
                {
                    idFound.Add(TableIdDic[id].ParentId);
                }          
            }
            if(idFound.Any())
            {
                List<string> accessableId = idFound.Where(x => TableIdDic.ContainsKey(x) && TableIdDic[x].HasAccess(user.UserGroups, user.id)).ToList();
                idFound = GetFullLevelTableIdByIdList(accessableId, user);
                childId.AddRange(idFound);
                return childId.Distinct().ToList();
            }
            else
            {
                List<string> accessableId = childId.Where(x => TableIdDic.ContainsKey(x) && TableIdDic[x].HasAccess(user.UserGroups, user.id)).ToList();
          
                return accessableId;
            }
        }

        public void UpdateDatabase(UpdateStructure update)
        {
            int total = update.TableList.Count() + update.FieldList.Count();
            int current = 0;
            foreach (var tableProperty in update.TableList)
            {
                var tableId = tableProperty.id;
                if (!string.IsNullOrWhiteSpace(tableId))
                {
                    solrApi.Core.Create(tableId);                    
                }
                current++;
                if(update.ActorStatus != null)
                {
                    update.ActorStatus.Tell(new UpdateProgressStatus(update.Id, total, current, $"Add DB Table : {tableProperty.Name}"));
                }
            

            }
            foreach (var field in update.FieldList)
            {
                var tableId = field.ParentId;
                if (!string.IsNullOrWhiteSpace(tableId))
                {

                    StringBuilder sbInsertTx = new StringBuilder();
                    StringBuilder sbUpdateTx = new StringBuilder();

                    string columnquery = string.Empty;
                    if (field.ControlType != Const.ControlTypes.Html.ToString() && field.ControlType != Const.ControlTypes.None.ToString())
                    {
                        string fieldtype = "text_general";
                        if (FieldNames.id == field.Name)
                        {
                            fieldtype = "string";
                        }
                        else
                        {
                            fieldtype = field.DataType;
                        }            
                        solrApi.Field.AddField(tableId, field.id, fieldtype, true, field.DefaultValues, field.MultipleValue);                       
                        current++;
                        if(update.ActorStatus != null)
                        {
                            update.ActorStatus.Tell(new UpdateProgressStatus(update.Id, total, current, $"Add DB Field : {field.Name}"));
                        }

                    }

                }
            }

        }

        public void SaveDataStructure(UpdateStructure updated)
        {

            TableProperty tableproperty = CreateInstance(TableProperty.ClassID) as TableProperty;
            List<PropertyInfo> tablePropertylist = tableproperty.GetType().GetProperties().ToList();
            int total = updated.TableList.Count() + updated.FieldList.Count();
            int current = 0;
            foreach (var table in updated.TableList)
            {
                LogHandler.Debug($"Create Table {GetFullTablename(table.id) }-{table.Name}");
             
                InsertData(TableProperty.ClassID, tablePropertylist, table);
                current++;
                if(updated.ActorStatus != null)
                {
                    updated.ActorStatus.Tell(new UpdateProgressStatus(updated.Id, total, current, $"Create Table: {table.Name}"));
                }
                
            }

            FieldProperty fieldproperty = CreateInstance(TableProperty.FieldProperty.ClassID) as FieldProperty;

            List<PropertyInfo> fieldProperty = fieldproperty.GetType().GetProperties().ToList();
            foreach (var field in updated.FieldList)
            {
                LogHandler.Debug($"Create Field {GetFullTablename(field.ParentId) }-{field.Name}");
                InsertData(TableProperty.FieldProperty.ClassID, fieldProperty, field);
                current++;
                if(updated.ActorStatus != null)
                {
                    updated.ActorStatus.Tell(new UpdateProgressStatus(updated.Id, total, current, $"Create Field: {field.Name}"));
                }
      
            }
        }


        private void InsertData(string tableId, List<PropertyInfo> tableProperty, object data)
        {
            Console.WriteLine("Count :  " + tableProperty.Count);

            Dictionary<string, object> param = new Dictionary<string, object>();
            foreach (PropertyInfo p in tableProperty)
            {
                PropertyInfo tableinfo = data.GetType().GetProperty(p.Name, BindingFlags.Public | BindingFlags.Instance);
                if (tableinfo != null)
                {
                    if(!TableToFieldNameDic[tableId].ContainsKey(p.Name))
                    {
                        continue;
                    }
                    if(p.Name == ClassLib.FieldNames.id)
                    {
                        param.Add(p.Name, tableinfo.GetValue(data));
                    }
                    else if (p.PropertyType == typeof(DateTime))
                    {
                        //Any Date version Control use this 
                        //_dataStructure.DbConnection.InsertOrUpdate();  
                        DateTime dt = (DateTime)tableinfo.GetValue(data);
                        param.Add(GetFieldIdByName(tableId, p.Name), dt.ToDateVersion());
                    }
                    //else if(p.PropertyType == typeof(CodeEditor))
                    //{
                    //    CodeEditor c = tableinfo.GetValue(data) as CodeEditor;
                    //    param.Add(GetFieldIdByName(tableId, p.Name), c.ToString());
                    //}
                    else if (p.PropertyType == typeof(string))
                    {
                        param.Add(GetFieldIdByName(tableId, p.Name), tableinfo.GetValue(data));
                    }
                    else if (p.PropertyType == typeof(int) ||
                             p.PropertyType == typeof(double) || p.PropertyType == typeof(long) ||
                             p.PropertyType == typeof(float))
                    {
                        //Any Control Configure sort by numeric use this query
                        param.Add(GetFieldIdByName(tableId, p.Name), tableinfo.GetValue(data));
                    }
                    else if (p.PropertyType == typeof(bool))
                    {
                        //Unique must in varchar 50, and contain clean string
                        param.Add(GetFieldIdByName(tableId, p.Name), tableinfo.GetValue(data));
                    }
                    else if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
                        || p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    {
                        param.Add(GetFieldIdByName(tableId, p.Name), JsonConvert.SerializeObject(tableinfo.GetValue(data)));
                    }
                    else if (p.PropertyType.IsEnum || TypeCheck.IsReserveType(p.PropertyType))
                    {
                        param.Add(GetFieldIdByName(tableId, p.Name), tableinfo.GetValue(data));
                    }
                    else
                    {
                        var settings = new JsonSerializerSettings();
                        settings.Converters.Add(new StringEnumConverter { });
                        settings.TypeNameHandling = TypeNameHandling.Auto;
                        param.Add(GetFieldIdByName(tableId, p.Name), JsonConvert.SerializeObject(tableinfo.GetValue(data), settings));
                    }

                }
                else
                {
                    Console.WriteLine("Skip Insert ");
                }
            }
            try
            {
                if (param[GetFieldIdByName(tableId, ClassLib.FieldNames.ChangedBy)] == null || string.IsNullOrWhiteSpace(param[GetFieldIdByName(tableId, ClassLib.FieldNames.ChangedBy)].ToString()))
                {
                    param[GetFieldIdByName(tableId, ClassLib.FieldNames.ChangedBy)] = Id.UsersAccount.System;
                }
                LogHandler.Debug("Inserted Count :  " + param.Count);

                InsertOrUpdate(tableId, param, new List<string>() { ClassLib.FieldNames.id});
            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());

            }

        }
    
        public void UpdatePropertyFromDb(string tableId,string id)
        {
            try
            {
                ResponseDetail dt = Query(tableId, new Dictionary<string, object>() { { FieldNames.id, id} },  start:0,limit: R.SystemSettings.PageSize, strict: false);
                object temp = CreateInstance(tableId);
                var fieldproperty = temp;
                PropertyInfo[] propertyInfos = fieldproperty.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (Dictionary<string, object> row in dt.docs)
                {
                    //LogHandler.Debug("Schema Change :" + JsonConvert.SerializeObject(row));
                    if (tableId == TableProperty.ClassID)
                    {
                        TableProperty tb = row.CastToObject<TableProperty>();
                        TableIdDic[tb.id] = tb;
                    }
                    else if (tableId == TableProperty.FieldProperty.ClassID)
                    {
                        //foreach(var x  in row)
                        //{
                        //    LogHandler.Debug($"{x.Key} : {x.Value}");
                        //}
                        FieldProperty fp = row.CastToObject<FieldProperty>();
                        FieldIdDic[fp.id] = fp;                        
                    }
                    else if (tableId == MenuProperty.ClassID)
                    {
                        MenuProperty fp = row.CastToObject<MenuProperty>();
                        MenuIdDic[fp.id] = fp;                 
                    }
                    else if(tableId == Pages.ClassID)
                    {
                        Pages fp = row.CastToObject<Pages>();
                        PagesIdDic[fp.id] = fp;
                    }
                }
                if(!dt.docs.Any())
                {
                    //Removing
                    if (tableId == TableProperty.ClassID)
                    {

                        TableIdDic.Remove(id);
                    }
                    else if (tableId == TableProperty.FieldProperty.ClassID)
                    {                       
                        FieldIdDic.Remove(id);
                    }
                    else if (tableId == MenuProperty.ClassID)
                    {
                       
                        MenuIdDic.Remove(id);
                    }
                    else if(tableId == Pages.ClassID)
                    {
                        PagesIdDic.Remove(id);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHandler.Debug(ex.ToString());
            }

        }



        public List<TableViewField> GetTableViewFields(Users user,string tableId)
        {
            List<Dictionary<string, object>> tableviewRecords = Query(TableViewSetting.ClassID, new Dictionary<string, object>() { { TableViewSetting.FieldsId.UserId, user.id }, {TableViewSetting.FieldsId.TableId,tableId } }, new List<string>() { "*" }, new Dictionary<string, string>() { { TableViewSetting.FieldsId.RowNo, "asc" } }).docs;
            List<FieldProperty> fieldList = GetAccessColumns(user, tableId).Where(x => x.Visible && x.Enable).ToList();
            
            if (!tableviewRecords.Any() || tableviewRecords.Count != TableToFieldDic[tableId].Count)
            {
                int first10 = 10;
                foreach (var field in TableToFieldDic[tableId].Where(x => !tableviewRecords.Any(y => y[TableViewSetting.FieldsId.Columns].ToString() == x.Key)))
                {
                    Dictionary<string, object> record = new Dictionary<string, object>();
                    record[ClassLib.FieldNames.id] = Unique.NewGuid();
                    record[TableViewSetting.FieldsId.ParentId] = "#";
                    record[TableViewSetting.FieldsId.Columns] = field.Key;
                    record[TableViewSetting.FieldsId.RowNo] = field.Value.RowNo;
                    record[TableViewSetting.FieldsId.UserId] = user.id;
                    record[TableViewSetting.FieldsId.TableId] = tableId;
                    record[TableViewSetting.FieldsId.Enable] = first10 > 0;
                    if (fieldList.Where(x => x.id.Equals(field.Key)).Any())
                    {
                        first10--;
                    }
                    tableviewRecords.Add(record);
                }
                Save(user, TableViewSetting.ClassID, tableviewRecords);
            }
            List<TableViewField> fieldlist = new List<TableViewField>();
            foreach (var viewsetting in tableviewRecords)
            {
                string fieldid = TypeCheck.CastToList(viewsetting[TableViewSetting.FieldsId.Columns]).First();
                if (!FieldIdDic.ContainsKey(fieldid) && fieldid != ClassLib.FieldNames.id)
                {
                    Delete(TableViewSetting.ClassID, viewsetting[ClassLib.FieldNames.id].ToString(), user.id);
                    continue;
                }

                fieldlist.Add(new TableViewField()
                {
                    Id = viewsetting[FieldNames.id].ToString(),
                    Enable = TypeCheck.CastToBool(viewsetting[TableViewSetting.FieldsId.Enable]),
                    FieldId = fieldid,
                    RowNo = int.Parse(TypeCheck.CastToList(viewsetting[TableViewSetting.FieldsId.RowNo]).First()),
                    AllowView = fieldList.Where(x => x.id.Equals(fieldid) || (x.Name == FieldNames.id) ).Any(),
                    FieldName = fieldid == FieldNames.id? FieldNames.id: FieldIdDic[fieldid].Name
                });
            }
            return fieldlist;
        }


 

        public void Copy(TableProperty fromtable, TableProperty Totable,ref List<TableProperty> tp,ref List<FieldProperty> fplist)
        {
            TableProperty fromtablclone = fromtable.Clone();
            fromtablclone.id = Unique.NewGuid();
            fromtablclone.ParentId = Totable == null ? "#" : Totable.id;
            tp.Add(fromtablclone);
            foreach (var field in FieldIdDic.Where(x => x.Value.ParentId == fromtable.id))
            {
                FieldProperty fp = field.Value.Clone();
                fp.id = Unique.NewGuid();
                fp.ParentId = fromtablclone.id;
                fplist.Add(fp);
            }
            foreach (var t in TableIdDic.Where(x=>x.Value.ParentId == fromtable.id).Select(x=> x.Value))
            {
                Copy(t, fromtablclone, ref tp, ref fplist);         
            }
    
      
        }
        public void Copy(MenuProperty fromMenu, MenuProperty ToMenu,bool pasteAsChild, ref List<MenuProperty> mplist, ref List<Pages> tbmnlist)
        {
            MenuProperty fromMenuClone = fromMenu.Clone();
            fromMenuClone.id = Unique.NewGuid();
            fromMenuClone.ParentId = ToMenu == null ? "#" : (pasteAsChild?ToMenu.id : ToMenu.ParentId);
            mplist.Add(fromMenuClone);
            foreach (Pages tm in PagesIdDic.Where(x => x.Value.ParentId == fromMenu.id).Select(x => x.Value))
            {
                Pages tmclone = tm.Clone();
                tmclone.id = Unique.NewGuid();
                tmclone.ParentId = fromMenuClone.id;
                tbmnlist.Add(tmclone);
            }
            foreach (MenuProperty menu in MenuIdDic.Where(x => x.Value.ParentId == fromMenu.id).Select(x=> x.Value))
            {
                Copy(menu, fromMenuClone, true,ref mplist,ref tbmnlist);
            }
        }
        public List<string> GetDeleleList(MenuProperty menu)
        {
            List<string> idlist = new List<string>();
            idlist.Add(menu.id);
            foreach (var m in MenuIdDic.Where(x=> x.Value.ParentId == menu.id))
            {
               idlist.AddRange(GetDeleleList(m.Value));
            }
            return idlist;
        }
        public List<string> GetDeleleList(TableProperty menu)
        {
            List<string> idlist = new List<string>();
            idlist.Add(menu.id);
            foreach (var m in TableIdDic.Where(x => x.Value.ParentId == menu.id))
            {
                idlist.AddRange(GetDeleleList(m.Value));
            }
            return idlist;
        }
        public List<TableProperty> GetAccessableTableProperty(string parentid,List<string> roles,string userid)
        {
            List<TableProperty> tablelist = new List<TableProperty>(); ;

            foreach(var table in TableIdDic.Where(x => x.Value.Enable && x.Value.ParentId == parentid && x.Value.GetAccess(roles, userid).Any()).OrderBy(x => x.Value.RowNo).Select(x => x.Value))
            {
                tablelist.Add(table);
                if(TableIdDic.Where(x=> x.Value.ParentId == table.id).Any())
                {
                    tablelist.AddRange(GetAccessableTableProperty(table.id, roles, userid));
                }
               
            }
            return tablelist;
        }
        public List<FieldProperty> GetAccessableFieldProperty(string parentid, List<string> roles, string userid)
        {
            return FieldIdDic.Where(x => x.Value.Enable && x.Value.ParentId == parentid && x.Value.GetAccess(roles, userid).Any()).OrderBy(x => x.Value.RowNo).Select(x => x.Value).ToList();

        }
        public string DuplicateCore(string coreId, List<string> uniqueList, List<FieldProperty> fieldslist)
        {
            string newCoreId = coreId + "Temp";
            FieldProperty idproperty = fieldslist.FirstOrDefault(x => x.Name.Equals("id")).Clone();
            idproperty.id = "id";            
            fieldslist = fieldslist.Where(x => !x.Name.Equals("id")).ToList();
            fieldslist.Add(idproperty);
            CreateCore(newCoreId, uniqueList, fieldslist);

            ResponseDetail rd = Query(coreId,showResult: fieldslist.Select(x=> x.id).ToList(),start: 0,limit: MaxRecordPerQuery);
        
            int count = 0;
            int found = rd.numFound;
            while (rd.docs.Count > 0)
            {
                solrApi.Document.Insert(newCoreId, rd.docs);
                count += MaxRecordPerQuery;
                rd = rd = Query(coreId, showResult: fieldslist.Select(x => x.id).ToList(),start: count,limit: MaxRecordPerQuery);
            }
            solrApi.Commit(newCoreId);
            return newCoreId;
        }
        public void CreateCore(string coreId,List<string> uniqueList,List<FieldProperty> fieldslist)
        {
            solrApi.Core.Create(coreId);
            LogHandler.Debug($"#########Create Core {coreId}############");
            foreach (var field in fieldslist)
            {
                string fieldtype = "text_general";
                object defaultvalue;

                if (field.ControlType != "None")
                {                    
                    if (ClassLib.FieldNames.id == field.Name)
                    {
                        fieldtype = "string";
                        defaultvalue = field.DefaultValues;
                        defaultvalue = defaultvalue ?? "";
                    }  
                    else
                    {
                        fieldtype = field.DataType;
                        defaultvalue = field.DefaultValues;
                        defaultvalue = defaultvalue ?? "";
                    }
                    //ds.CreateFieldTx(table.Key, field.Key, fieldtype, defaultvalue,field.Value.MultipleValue);
                    LogHandler.Debug($"Create Field {field.id} {fieldtype}");
                    RestApi.RestApiResponse res = solrApi.Field.AddField(coreId, field.id, fieldtype, true, defaultvalue, field.MultipleValue);
                    //ds.solrApi.Field.AddField(ds.GetArchivedTable(table.Key), field.Key, fieldtype, true, defaultvalue, field.Value.MultipleValue);
                }
            }
            LogHandler.Debug($"#########Create Core {coreId} ENd############");
        }
        public List<string> GetChildTableId(string tableid, bool recursive)
        {
            List<string> idlist = new List<string>();
            foreach(var t in TableIdDic.Where(x=> x.Value.ParentId == tableid))
            {
                idlist.Add(t.Value.id);
                if(recursive)
                {
                    idlist.AddRange(GetChildTableId(t.Value.id, recursive));
                }
            }
            return idlist;
        }
        public bool IsTableLock(string tableId)
        {
            if(TableIdDic.ContainsKey(tableId))
            {
                if(TableIdDic[tableId].Lock)
                {
                    return true;
                }
                if(TableIdDic.ContainsKey(TableIdDic[tableId].ParentId))
                {
                    return IsTableLock(TableIdDic[tableId].ParentId);
                }                
            }            
            return false;
        }
  
    }
}