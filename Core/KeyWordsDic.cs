﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Akka.Actor;
using Core.Akka;

namespace Core
{
    public class KeyWordsDic
    {
        private  Dictionary<string, Dictionary<string, string>> _language { set; get; }
        public Dictionary<string, Dictionary<string, string>> Languages
        {
            set {
                lock(mutex)
                {
                    _language = value;
                }
            }
            get
            {
                lock(mutex)
                {
                    return _language;
                }
            }
        }
        public KeyWordsDic()
        {
            Languages = new Dictionary<string, Dictionary<string, string>>(); 
        }
        public static Regex keyRegex = new Regex("[^a-zA-Z0-9]+");
   
        public static ICanTell LanguageActor { set; get; }
        public static object mutex = new object();
        public static string GetKey(string word)
        {
            return keyRegex.Replace(word,"");
        }
        public string GetWord(string language, string word,bool noregexclean=false)
        {        
            string keyWord = noregexclean? word: GetKey(word);
            if(string.IsNullOrWhiteSpace(keyWord))
            {
                return word;
            }
            if (!Languages.ContainsKey(language))
            {
                lock(mutex)
                {
                    Languages.Add(language, new Dictionary<string, string>());
                }
               
            }
            if (!Languages[language].ContainsKey(keyWord))
            {
                lock (mutex)
                {               //Languages[language].Add(keyWord,word);
                    Dictionary<string, Dictionary<string, string>> dic = new Dictionary<string, Dictionary<string, string>>();
                    dic.Add(language, new Dictionary<string, string>());
                    dic[language].Add(keyWord, word);
                    if (LanguageActor != null)
                    {
                        if(keyWord.Length >= 40)
                        {
                            throw new Exception("Key Word Too Long");
                        }
                        if (!Languages.ContainsKey(language))
                        {
                            throw new Exception("Not valid Language");
                        }
                        LanguageActor.Tell(new AddNewLanguageKey() { Language = dic },ActorRefs.NoSender);
                        //Do update to Database here
                    }

                }

     
                return word;
            }
            return _language[language][keyWord];
        }
    }
}