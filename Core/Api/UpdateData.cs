﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Api
{
    public class UpdateData
    {
        public List<Dictionary<string,object>> updateRecords { set; get; }
        public Dictionary<string,object> UpdateFilter { set; get; }

    }
}
