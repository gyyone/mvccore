﻿using System;
using Topshelf;

namespace CoreService
{
    class Program
    {
        static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>                                   //1
            {
                x.Service<CoreServer>(s =>                                   //2
                {
                    s.ConstructUsing(name => new CoreServer());                //3
                    s.WhenStarted(tc => tc.Start());                         //4
                    s.WhenStopped(tc => tc.Stop());                          //5
                });
                x.RunAsLocalSystem();                                       //6

                x.SetDescription("Data Management");                   //7
                x.SetDisplayName("DB Management");                                  //8
                x.SetServiceName("DB Management");                                  //9
            });                                                             //10

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());  //11
            Environment.ExitCode = exitCode;
        }
    }
}
