﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CoreService
{
    public class CoreServer
    {
        string basepath = AppDomain.CurrentDomain.BaseDirectory;
        List<Process> ProcessList = new List<Process>();
        public CoreServer()
        {     
           
        }
        public void Start() {
            
            //ProcessStartInfo solr = new ProcessStartInfo();
            //solr.WorkingDirectory = $"{basepath}/solr/bin";
            //solr.FileName = $"{basepath}/solr/bin/solr.cmd";
            //solr.Arguments = "start ";
            //Process p = Process.Start(solr);
            //ProcessList.Add(p);
            //p.WaitForExit();

            ProcessStartInfo deploymanager = new ProcessStartInfo();
            deploymanager.WorkingDirectory = $"{basepath}DeployManager/";
            deploymanager.FileName = "dotnet";
            deploymanager.Arguments = "DeployManager.dll";
            ProcessList.Add(Process.Start(deploymanager));

            ProcessStartInfo mvcCore = new ProcessStartInfo();
            mvcCore.WorkingDirectory = $"{basepath}MVCCore/";
            mvcCore.FileName = "dotnet";
            mvcCore.Arguments = "MVCCore.dll";
            ProcessList.Add(Process.Start(mvcCore));




        }
        public void Stop() {
            foreach(Process p in ProcessList)
            {
                if(!p.HasExited)
                {
                    p.Kill();
                    p.WaitForExit();
                }
            }

            ProcessStartInfo solr = new ProcessStartInfo();
            solr.WorkingDirectory = $"{basepath}solr/bin";
            solr.FileName = $"{basepath}/solr/bin/solr.cmd";
            solr.Arguments = "stop -all";
            Process.Start(solr);
        }
    }
}
