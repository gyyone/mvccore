﻿namespace SimpleDatabase.Solr
{
    public class ResponseHeader
    {
        public int status { set; get; }
        public int QTime { set; get; }
    }
}
