﻿using System.Collections.Generic;

namespace SimpleDatabase.Solr
{
    public class FieldCollection
    {
        public List<SolrField> fields { set; get; }

    }
}
