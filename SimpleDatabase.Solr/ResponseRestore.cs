﻿namespace SimpleDatabase.Solr
{
    public class ResponseRestore
    {
        public ResponseHeader responseHeader { set; get; }
        public RestoreStatus restorestatus { set; get; }
    }
}
