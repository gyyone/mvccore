﻿using Newtonsoft.Json;

namespace SimpleDatabase.Solr
{
    public class QueryItem
    {
        public string q { set; get; }
        //public string filter { set; get; }
        public int start { set; get; }
        public int rows { set; get; }
        public string sort { set; get; }
        public bool facet { set; get; }
        [JsonProperty("json.facet")]
        public string facetJson { set; get; }
        public string fl { set; get; }
        public string defType { get; set; }
        public string bq { set; get; }


    }
}
