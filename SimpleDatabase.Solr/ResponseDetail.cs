﻿using System.Collections.Generic;

namespace SimpleDatabase.Solr
{
    public class ResponseDetail
    {
        public ResponseDetail()
        {
            docs = new List<Dictionary<string, object>>();
        }
        public int numFound { set; get; }
        public int start { set; get; }
        public double maxScore { set; get; }
        public List<Dictionary<string, object>> docs { set; get; }
    }
}
