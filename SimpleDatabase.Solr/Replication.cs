﻿using Newtonsoft.Json;

namespace SimpleDatabase.Solr
{
    public class Replication
    {
        [JsonProperty("details")]
        public ReplicationDetail details { set; get; }
    }
}
