﻿using RestApi;
using SimpleDatabase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SimpleDatabase.Solr
{
    public class SimpleSolr : IDatabase
    {
        /// <summary>
        /// SimpleSolr
        /// </summary>
        /// <param name="solrUrl">Eg : http://localhost:8983/ </param>
        /// <param name="username">The Api User to access solr database.  Eg : solr </param>
        /// <param name="password">The Api password to access solr database. Eg : SolrRocks</param>
        /// <param name="serviceAccount">The account to run Solr Service.  Eg : solr </param>
        /// <param name="servicegroup">The group of the account  to run Solr Service Eg : solrgroup</param>
        public SimpleSolr(string solrUrl, string username, string password,string serviceAccount,string servicegroup)
        {
            Connection = new SolrConnection(solrUrl,new ApiAuth() {UserName = username,  Password= password },serviceAccount,servicegroup);
        }
        private SolrConnection Connection { set; get; }

        public void AddOrAlterField(string tableId, string fieldId, string datatype, object defaultvalue, bool multivalue, CancellationToken cancellationToken)
        {
            Connection.AddOrAlterField(tableId, fieldId, datatype, defaultvalue, multivalue, cancellationToken);
        }

        public void AddTable(string tableId ,CancellationToken cancellationToken)
        {
            Connection.CreateCore(tableId, cancellationToken);
        }

        public void DeleteField(string tableId, string fieldid, CancellationToken cancellationToken)
        {
            Connection.DeleteField(tableId, fieldid, cancellationToken);
        }

        public void DeleteTable(string tableId, CancellationToken cancellationToken)
        {
            Connection.DeleteCore(tableId, cancellationToken);
        }

        public void Insert(string tableId, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken)
        {
            Connection.InsertDoc(tableId, datalist, cancellationToken);
        }

        public void Insert(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken)
        {
            Connection.InsertDoc(tableId, data, cancellationToken);
        }

        public void InsertOrUpdate(string tableId, Dictionary<string, object> searchTerm, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
            //Connection.up
        }

        public QueryResult Query(string tableId, string query,string sort, List<string> showResult, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue - 1 )
        {
            return Connection.QueryDoc(tableId, query, sort, showResult, cancellationToken, start, rows);           

        }

        public QueryResult Query(string tableId, List<SearchTerm> searchTerm, List<string> showResult, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue, bool orQuery = false)
        {
            return Connection.QueryDoc(tableId, searchTerm, showResult, cancellationToken, start, rows,false);
        }

        public void Update(string tableId, List<Dictionary<string, object>> datalist, CancellationToken cancellationToken)
        {
            foreach(var row in datalist)
            {
                Connection.UpdateDoc(tableId, row, cancellationToken);
            }
         
        }

        public void Update(string tableId, Dictionary<string, object> data, CancellationToken cancellationToken)
        {
            Connection.UpdateDoc(tableId, data, cancellationToken);
        }
    }
}
