﻿using System.Collections.Generic;

namespace SimpleDatabase.Solr
{
    public class ListCollection
    {
        public List<string> collections { set; get; }
    }
}
