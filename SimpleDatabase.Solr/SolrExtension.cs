﻿using RestApi;
using SimpleDatabase.Database;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using System.IO;
using DirectoryHelper;
using System.Threading;
using System.Runtime.InteropServices;
using Util;

namespace SimpleDatabase.Solr
{
    public static class SolrExtension
    {

        public static QueryResult ToQueryResult(this RestApiResponse msr)
        {
            if (!string.IsNullOrEmpty(msr.Error))
            {
                throw new System.Exception(msr.Error);
            }
            else
            {
                var t = msr.Result.Content.ReadAsStringAsync();
                ResponseDoc temp = JsonConvert.DeserializeObject<ResponseDoc>(t.Result);
                return new QueryResult() { docs = temp.response.docs, numFound = temp.response.numFound, start = temp.response.start };
            }
        }

        public static string ToSortString(this List<SearchTerm> searchTerm)
        {
            List<string> queryList = new List<string>();
            foreach (var s in searchTerm)
            {
                if (!string.IsNullOrEmpty(s.Sort))
                {
                    queryList.Add(string.Format("{0} {1}", s.FieldId, s.Sort));
                }

            }
            return string.Join(",", queryList);
        }
        public static string ToQueryString(this List<SearchTerm> searchTerm, bool OrField = false)
        {
            List<string> queryList = new List<string>();
            foreach (var s in searchTerm)
            {
                s.search = s.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                if (!s.search.Any())
                {
                    continue;
                }
                switch (s.queryType)
                {
                    case QueryType.ExactMatch:
                        List<string> q_exact = s.search.Select(x => string.Format("{0}:\"{1}\"", s.FieldId, x)).ToList();
                        queryList.Add("(" + string.Join(" OR ", q_exact) + ")");
                        break;
                    case QueryType.RangeQuery:
                        while (s.search.Count <= 2)
                        {
                            s.search.Add("*");
                        }
                        queryList.Add(string.Format("{0}:[{1} TO {2}]", s.FieldId, s.search[0] == "*" || string.IsNullOrWhiteSpace(s.search[0]) ? "*" : $"\"{s.search[0]}\"", s.search[1] == "*" || string.IsNullOrWhiteSpace(s.search[1]) ? "*" : $"\"{s.search[1]}\""));
                        break;
                    case QueryType.TextQuery:
                        List<string> q = s.search.Select(x => $"{s.FieldId}:{(x.Contains("*") ? x : $"*{x}*")}").ToList();
                        queryList.Add("(" + string.Join(" OR ", q) + ")");
                        break;
                    case QueryType.CustomMatch:
                        List<string> qregex = s.search.Select(x => string.Format("{0}:{1}", s.FieldId, x)).ToList();
                        queryList.Add("(" + string.Join(" OR ", qregex) + ")");
                        break;
                }
            }
            if (queryList.Count == 0)
            {
                queryList.Add("*:*");
            }
            if (OrField)
            {
                return string.Join(" OR ", queryList);
            }

            return string.Join(" AND ", queryList);
        }

        public static void HandleResponse(this RestApiResponse r)
        {
            if (!string.IsNullOrEmpty(r.Error))
            {
                throw new System.Exception(r.Error);
            }
            Task<string> result = r.Result.Content.ReadAsStringAsync();
            Dictionary<string, object> reply = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Result);
            if (reply.ContainsKey("exception"))
            {
                throw new Exception(reply["exception"].ToString());
            }
        }
        public static void HandleResponseDoc(this ResponseDoc r)
        {
            if (r == null || r.responseHeader.status != 0 )
            {
                throw new System.Exception( JsonConvert.SerializeObject(r.responseHeader.paramDic));
            }      
        }
        public static ResponseDoc GetResponseDoc(this RestApiResponse r)
        {

            if (!string.IsNullOrEmpty(r.Error))
            {
                throw new System.Exception(r.Error);
            }
            Task<string> t = r.Result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ResponseDoc>(t.Result.ToString());
        }
    }
}
