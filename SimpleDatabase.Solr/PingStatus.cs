﻿using Newtonsoft.Json;

namespace SimpleDatabase.Solr
{
    public class PingStatus
    {
        [JsonProperty("status")]
        public string Status { set; get; }
        [JsonProperty("QTime")]
        public int QTime { set; get; }
    }
}
