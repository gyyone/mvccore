﻿namespace SimpleDatabase.Solr
{
    public class ResponseDoc
    {
        public QueryResponseHeader responseHeader { set; get; }
        public ResponseDetail response { set; get; }
    }
}
