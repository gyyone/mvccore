﻿namespace SimpleDatabase.Solr
{
    public class RestoreStatus
    {
        public string snapshotName { set; get; }
        public string status { set; get; }
    }
}
