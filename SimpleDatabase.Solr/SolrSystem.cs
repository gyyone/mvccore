﻿using Newtonsoft.Json;

namespace SimpleDatabase.Solr
{
    public class SolrSystem
    {
        [JsonProperty("mode")]
        public string Mode { set; get; }
        [JsonProperty("solr_home")]
        public string SolrHome { set; get; }
    }
}
