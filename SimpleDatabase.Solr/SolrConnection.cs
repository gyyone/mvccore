﻿using DirectoryHelper;
using Newtonsoft.Json;
using RestApi;
using SimpleDatabase.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Util;

namespace SimpleDatabase.Solr
{
    public class SolrConnection
    {
        public SolrConnection()
        {

        }
        public SolrConnection(string hosturl,ApiAuth auth, string solrUser,string solrGroup)
        {
            HostUrl = hosturl;
            Authentication = auth;
            SolrUser = solrUser;
            SolrGroup = solrGroup;
        }
        public string HostUrl { set; get; }
        public ApiAuth Authentication { set; get; }
        public string SolrUser { set; get; }
        public string SolrGroup { set; get; }

        #region Core
        private static List<string> filechecking = new List<string>() {
        "managed-schema",
        "params.json",
        "protwords.txt",
        "solrconfig.xml",
        "stopwords.txt",
        "synonyms.txt"
        };
        public void DeleteCore(string corename, CancellationToken cancelationToken)
        {
            if (!ListCollectionCore().Contains(corename))
            {
                return;
            }
            SolrSystem systeminfo = GetSystemInfo(cancelationToken);
            //string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}", _SolrAuth.HostUrl, "UNLOAD", corename);
            string postapi = string.Format("{0}/solr/admin/cores?action={1}&core={2}&deleteIndex=true&deleteDataDir=true&deleteInstanceDir=true", HostUrl, "UNLOAD", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, cancelationToken, Authentication);

            string solrpath = systeminfo.SolrHome + "/" + corename;
            if (Directory.Exists(solrpath))
            {
                Directory.Delete(solrpath, true);
            }
            msr.HandleResponse();

        }
        public void CreateCore(string corename, CancellationToken cancelationToken)
        {

            if (ListCollectionCore().Contains(corename))
            {
                return;
            }

            bool copyfile = false;
            SolrSystem systeminfo = GetSystemInfo(cancelationToken);
            foreach (var file in filechecking)
            {
                string path = systeminfo.SolrHome + "/" + corename + "/conf/" + file;
                if (!File.Exists(path))
                {
                    copyfile = true;
                }
            }

            if (copyfile)
            {
                string config = AppDomain.CurrentDomain.BaseDirectory + "Files/conf";
                string solrpath = systeminfo.SolrHome + "/" + corename;
                DirHelper.Copy(config, $"{solrpath}/conf");

                if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    bool error;
                    List<string> output = CommandLineReader.Run($"/bin/chown", $" -R {SolrUser}:{SolrGroup} {solrpath}", out error);
                    if (error)
                    {
                        Console.WriteLine(string.Join(Environment.NewLine, output));
                    }
                }

            }


            string coreproperty = systeminfo.SolrHome + "/" + corename + "/core.properties";
            if (File.Exists(coreproperty))
            {
                File.Delete(coreproperty);
            }

            string postapi = string.Format("{0}/solr/admin/cores?action={1}&name={2}", HostUrl, "CREATE", corename);
            RestApiResponse msr = ServerApi.Post(postapi, null, Authentication);
            msr.HandleResponse();
            DirHelper.CreateDirectory(coreproperty);
            using (StreamWriter sw = new StreamWriter(coreproperty, false))
            {
                sw.WriteLine($"name={corename}");
            }

        }

        public List<string> ListCollectionCore()
        {
            string postapi = string.Format("{0}/solr/admin/cores", HostUrl);
            RestApiResponse msr = ServerApi.Post(postapi, null, Authentication);
            Task<string> result = msr.Result.Content.ReadAsStringAsync();
            ListCore temp = JsonConvert.DeserializeObject<ListCore>(result.Result);
            return temp.status.Keys.ToList();
        }
        #endregion

        #region Field
        public void AddOrAlterField(string corename, string fieldname, string type, object defaultvalue, bool multivalue, CancellationToken cancelationToken, bool stored = true)
        {
            defaultvalue = defaultvalue ?? "";
            defaultvalue = defaultvalue.ToString();
            if (!GetFields(corename, cancelationToken).Where(x => x.name.Equals(fieldname, StringComparison.OrdinalIgnoreCase)).Any())
            {
                string postapi = string.Format("{0}/solr/{1}/schema", HostUrl, corename);
                RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, AddField>() { { "add-field", new AddField() { name = fieldname, type = type, stored = stored, default_value = defaultvalue.ToString(), multiValued = multivalue } } }, cancelationToken, Authentication);
                msr.HandleResponse();
            }
            else
            {
                ReplaceField(corename, fieldname, type, stored, defaultvalue, multivalue, cancelationToken);
            }

        }
        public void DeleteField( string corename, string fieldname, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/schema", HostUrl, corename);
            RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, DelField>() { { "delete-field", new DelField() { name = fieldname } } }, cancelationToken, Authentication);
            msr.HandleResponse();
        }
        public void ReplaceField(string corename, string fieldname, string type, bool stored, object defaultValue, bool multivalue, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/schema", HostUrl, corename);
            List<SolrField> fields = GetFields(corename, cancelationToken);
            SolrField field = fields.Where(x => x.name == fieldname).FirstOrDefault();
            if (field == null)
            {
                throw new Exception($"Invalid Field {fieldname}");
            }
            if (!field.type.Equals(type) || field.stored != stored || (!field.default_value.Equals(defaultValue) && defaultValue != null && !string.IsNullOrWhiteSpace(defaultValue.ToString())) || field.multiValued != multivalue)
            {
                RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, ReplaceField>() { { "replace-field", new ReplaceField() { name = fieldname, type = type, stored = stored, default_value = defaultValue, multiValued = multivalue } } }, Authentication);
                msr.HandleResponse();
            }
        }

        public  List<SolrField> GetFields(string corename, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/schema/fields?wt=json", HostUrl, corename);

            RestApiResponse msr = ServerApi.Get(postapi, cancelationToken, auth: Authentication);

            if (string.IsNullOrEmpty(msr.Error))
            {
                Task<string> t = msr.Result.Content.ReadAsStringAsync();
                FieldCollection collectionfield = JsonConvert.DeserializeObject<FieldCollection>(t.Result.ToString());
                return collectionfield.fields;
            }
            return new List<SolrField>();
        }

        #endregion

        #region System Infomation and Solr Users Management
        public SolrSystem GetSystemInfo(CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/admin/info/system", HostUrl);
            RestApiResponse msr = ServerApi.Post(postapi, null, cancelationToken, Authentication);
            Task<string> result = msr.Result.Content.ReadAsStringAsync();
            SolrSystem temp = JsonConvert.DeserializeObject<SolrSystem>(result.Result);
            return temp;
        }
        public void SetUser(string username, string password, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", HostUrl);
            Dictionary<string, Dictionary<string, string>> param = new Dictionary<string, Dictionary<string, string>>();
            param["set-user"] = new Dictionary<string, string>();
            //Set default password
            param["set-user"][username] = password;
            RestApiResponse msr = ServerApi.Post(postapi, param, cancelationToken, Authentication);
        }
        public void DeleteUser(string username, string password, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", HostUrl);
            Dictionary<string, List<string>> param = new Dictionary<string, List<string>>();
            param["delete-user"] = new List<string>();
            //Set default password
            param["delete-user"].Add(username);
            RestApiResponse msr = ServerApi.Post(postapi, param, cancelationToken, Authentication);
        }
        public void SetUserRole(string username, List<string> roles, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", HostUrl);
            Dictionary<string, Dictionary<string, List<string>>> param = new Dictionary<string, Dictionary<string, List<string>>>();
            param["set-user-role"] = new Dictionary<string, List<string>>();
            //Set default password
            param["set-user-role"].Add(username, roles);
            RestApiResponse msr = ServerApi.Post(postapi, param, cancelationToken, Authentication);
        }

        public void SetPermission(string username, string permissions, string role, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/admin/authentication", HostUrl);

            Dictionary<string, Dictionary<string, string>> param = new Dictionary<string, Dictionary<string, string>>();
            param["set-permission"] = new Dictionary<string, string>();
            //Set default password
            param["set-permission"].Add("name", permissions);
            param["set-permission"].Add("role", role);
            RestApiResponse msr = ServerApi.Post(postapi, param, cancelationToken, Authentication);
        }

        #endregion

        #region Handle Document query or update
        public QueryResult QueryDoc(string tableId, List<SearchTerm> searchTerm, List<string> showList, CancellationToken canceltoken, int start = 0, int rows = int.MaxValue, bool orQuery = false)
        {
            string postapi = string.Format("{0}/solr/{1}/select", HostUrl, tableId);
            QueryItem qitem = new QueryItem();
            qitem.defType = "edismax";
            qitem.sort = searchTerm.ToSortString();
            qitem.q = searchTerm.ToQueryString(orQuery);

            int maxrowsAccept = int.MaxValue - start;
            qitem.rows = Math.Min(rows, maxrowsAccept);
            qitem.start = start;
            if (showList != null && showList.Count > 0)
            {
                qitem.fl = string.Join(",", showList.Select(x => $"{x}"));
            }

            RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, QueryItem>() { { "params", qitem } }, canceltoken, Authentication);
            return msr.ToQueryResult();
        }

        public QueryResult QueryDoc(string coreName, string query, string sort, List<string> showResult, CancellationToken cancellationToken, int start = 0, int rows = int.MaxValue - 1)
        {
            string postapi = string.Format("{0}/solr/{1}/select", HostUrl, coreName);
            QueryItem qitem = new QueryItem();
            qitem.q = query;
            qitem.sort = sort;
            //Console.WriteLine($"query : {qitem.q} Table :{coreName}");     
            int maxrowsAccept = int.MaxValue - start;
            qitem.rows = Math.Min(rows, maxrowsAccept);
            qitem.start = start;
            if (showResult != null && showResult.Count > 0)
            {
                qitem.fl = string.Join(",", showResult);
            }
            RestApiResponse msr = ServerApi.Post(postapi, new Dictionary<string, QueryItem>() { { "params", qitem } }, cancellationToken, Authentication);
            return msr.ToQueryResult();

        }
        public  void DocCommit(string coreName, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/update/json/docs?commit=true", HostUrl, coreName);

            RestApiResponse msr = ServerApi.Post(postapi, null, cancelationToken, Authentication);
            msr.HandleResponse();
            ResponseDoc rc = msr.GetResponseDoc();
            rc.HandleResponseDoc();
        }
        public void InsertDoc( string coreName, List<Dictionary<string, object>> updatevalue, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/update/json/docs", HostUrl, coreName);
            RestApiResponse msr = ServerApi.Post(postapi, updatevalue, cancelationToken, Authentication);
            msr.HandleResponse();

        }
        public void InsertDoc(string coreName, Dictionary<string, object> updatevalue, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/update/json/docs", HostUrl, coreName);
            RestApiResponse msr = ServerApi.Post(postapi, updatevalue, cancelationToken, Authentication);
            msr.HandleResponse();
        }
        public void UpdateDoc(string coreName, Dictionary<string, object> updatevalue, CancellationToken cancelationToken)
        {

            Dictionary<string, object> setvalue = new Dictionary<string, object>();
            foreach (var key in updatevalue)
            {
                if (key.Key.Equals("id"))
                {
                    setvalue.Add(key.Key, key.Value.ToString());
                }
                else
                {
                    setvalue.Add(key.Key, new Dictionary<string, object>() { { "set", key.Value } });
                }
            }

            string postapi = string.Format("{0}/solr/{1}/update?wt=json", HostUrl, coreName);
            RestApiResponse msr = ServerApi.Post(postapi, new List<Dictionary<string, object>>() { setvalue }, cancelationToken, Authentication);
            msr.HandleResponse();

        }

        public void ClearDoc(string coreName, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/update", HostUrl, coreName);
            //string deletedidstr = "<add><delete>" + string.Join("OR ", deleteid.Select(x => "<query>id:" + x + "</query>")) + "</delete></add>";
            //XmlDocument xml = new XmlDocument();
            //xml.LoadXml(deletedidstr);
            Dictionary<string, Dictionary<string, string>> deleteDic = new Dictionary<string, Dictionary<string, string>>();
            deleteDic["delete"] = new Dictionary<string, string>();
            deleteDic["delete"]["query"] = "*:*";

            RestApiResponse msr = ServerApi.Post(postapi, deleteDic, cancelationToken, Authentication);
            msr.HandleResponse();

        }
        public void DeleteDoc(string coreName, List<string> deleteid, CancellationToken cancelationToken)
        {
            string postapi = string.Format("{0}/solr/{1}/update", HostUrl, coreName);
            //string deletedidstr = "<add><delete>" + string.Join("OR ", deleteid.Select(x => "<query>id:" + x + "</query>")) + "</delete></add>";
            //XmlDocument xml = new XmlDocument();
            //xml.LoadXml(deletedidstr);
            Dictionary<string, Dictionary<string, string>> deleteDic = new Dictionary<string, Dictionary<string, string>>();
            deleteDic["delete"] = new Dictionary<string, string>();
            deleteDic["delete"]["query"] = "id:(" + string.Join(" OR ", deleteid) + ")";

            RestApiResponse msr = ServerApi.Post(postapi, deleteDic, cancelationToken, Authentication);
            msr.HandleResponse();

        }

        public void DeleteDoc( string coreName, string deleteid, CancellationToken cancelationToken)
        {
            DeleteDoc(coreName, new List<string>() { deleteid }, cancelationToken);
        }


        #endregion
    }

}
