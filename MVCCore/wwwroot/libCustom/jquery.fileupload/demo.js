/*
 * jQuery File Upload Demo
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $ */

$(function () {
    'use strict';
    var element = $("#fileupload");
    var obj = {
        "maxFileSize": 1000000000,
        "disableImageResize": false,
        "imageForceResize": true,
        "imageMaxWidth": 1080,
        "imageMaxHeight": 1080,
        "url": "/Upload/UploadFiles",
        "canvas": false
     
    };

    var uploadelement = $(element).fileupload(obj);
    $(element).addClass("fileupload-processing");
    var formData = $(element).serializeArray();
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: "/Upload/GetUploadedFiles",
        dataType: "json",
        data: formData,
        type: "Post",
        context: uploadelement
    })
        .always(function () {
            $(element).removeClass("fileupload-processing");
        })
        .done(function (result) {
            $(element)
                .fileupload("option", "done")
                // eslint-disable-next-line new-cap
                .call(element, $.Event("done"), {
                    result: {
                        files: result
                    }
                });
   
        });


  
});
