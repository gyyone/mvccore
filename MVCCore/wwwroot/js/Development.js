﻿

function InitDevelopment() {
    var url = UrlAction("GetSchema", "Schema");
    $.ajax({
        url: url,
        type: "Post",
        data: { "ParentId": "*" ,"TableOnly":"true"},
        async: true,
        success: function (e) {
           
            $("#development").on('state_ready.jstree', function () {

            }).jstree({
                "core": {
                    "themes": {
                        "responsive": true
                    },
                    "data": e
                },
                "search": {
                    "case_insensitive": true,
                    "show_only_matches": true,
                    "show_only_matches_children": true,      
                },
                "plugins": ["search", "contextmenu", "unique", "checkbox"],

            });
            var searching = false;
            $('#developmentsearch').keyup(function () {

                if (!searching) {
                    searching = true;
                    setTimeout(function () {
                        searching = false;
                        var v = $('#developmentsearch').val();
                        $("#schemabackup").jstree(true).search(v);
                        $("#ControlInput").html("<button></button>");
                    }, 1000);
                }

            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
    LoadBackupTable();
    

}
function RebuildSchema()
{
    var selectedId = $('#development').jstree('get_selected');
    bootbox.confirm(GetWords("Do you want to repair index?"), function (result) {
        if (result) {       
            setTimeout(function () {
                ShowBusy();
                setTimeout(function () {
                    var request = AjaxRequest(UrlAction("RebuildSchema", "Schema"), { "tableidlist": selectedId });
                    if (request.Code == "Success") {
                        bootbox.alert("Repair Index Completed");    
                      
                    }
                    else {
                        CaptureError(request.Message);                                            
                    }
                    ShowReady();
                }, 2000);
       
              
            }, 2000);
    
            
        }
    });
}
function DownloadClasses() {  
  
    var selectedId = $('#development').jstree('get_selected');
    ShowBusy();
    $.ajax({
        url: "/Schema/DownloadClasses",
        type: "Post",
        data: {
            "SchemaIdlist": selectedId
        }, 
        success: function (data) {
            ShowReady();
            var url = window.URL.createObjectURL(new Blob([new Uint8Array(data.Obj.File)], { type: "octet/steam" }));
            var a = document.createElement('a');  
            a.href = url;
            a.download = data.Obj.FileName;
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
      
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
         
        }
    });


}
function convertBase64ToBinary(base64) {

    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for (var i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
}
function str2bytes(str) {
    var bytes = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        bytes[i] = str.charCodeAt(i);
    }
    return bytes;
}
function stringToBytes(str) {
    var re = [];
    for (var i = 0; i < str.length; i++) {
       
        re.push(str.charCodeAt(i))
    }
    // return an array of bytes
    return re;
}    


