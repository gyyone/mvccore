﻿

function InitSideBarSetting() {
    var url = UrlAction("GetMenu", "Menu");
    var urlForm = UrlAction("GetHtmlForm", "Control");
    var updateUrl = UrlAction("InsertOrUpdate", "Data");
    var deleteUrl = UrlAction("Delete", "Data");
    var urlsubtable = UrlAction("GetHtmlTabTable", "Data");

    $("#Menu").on('state_ready.jstree', function () {

        $("#Menu").on('changed.jstree', function (e, data) {
            if (data && data.event && data.event.button == 0) {
                var obj = AjaxRequest(urlForm, { "tableName": data.node.original.NodeObject.TableId, "guid": data.node.original.NodeObject.id, parentId: data.node.original.NodeObject["Parent Id"], "isNew": false, "enableSubTable": true, "updateUrl": updateUrl, "deleteUrl": deleteUrl, "subtableUrl": urlsubtable })
                $("#MenuInput").html(obj.Obj);
                InitDependancy();
                InitControl();
            }
            //console.log(e);
            //console.log(data);

        });

    }).jstree({
        "core": {
            "themes": {
                "responsive": true
            },
            "data": {
                "url": url,
                "type": "post",
                "data": function (node) {
                    return { "Parent Id": node.id };
                }
            },
            "check_callback": function (operation, node, parent, position, more) {
                if (operation == "move_node") {

                    if (node.original.NodeObject["Parent Id"] == parent.id) {
                        console.log(node.text + " Move to " + position);
                        //console.log(operation);
                        //console.log(node);
                        //console.log(parent);
                        return true;
                    }
                }
                return false;


            }
        },
        "search": {
            "case_insensitive": true,
            "show_only_matches": true,
            "show_only_matches_children": true,
            ajax: {
                "url": url,
                "type": "post",
                "data": function (str) {
                    return { "str": str };

                }
            }
        },
        "plugins": ["state", "search", "contextmenu", "unique"],
        contextmenu: { items: ContextMenuSideBar }
    });
    var searching = false;
    $('#menusearch').keyup(function () {

        if (!searching) {
            searching = true;
            setTimeout(function () {
                searching = false;
                var v = $('#menusearch').val();
                $("#Menu").jstree(true).search(v);

            }, 1000);
        }

    });
}
var copyMenuNode;
function ContextMenuSideBar(selectednode) {
    console.log(node);
    var items = {};
    var node = selectednode.original;
    items["reorder"] =
        {
            "label": GetWords("Reorder"),
            "action": function () {

                var request = AjaxRequest(UrlAction("Reorder", "Schema"), { "tableId": node.NodeObject.TableId, "parentId": node.NodeObject["Parent Id"] });
                if (request.Code == "Success") {

                    ShowModal("Sort View", request.Obj);

                    $("#SortView").sortable({
                        start: function (event, ui) {
                            var start_pos = ui.item.index();
                            ui.item.data('start_pos', start_pos);
                        },
                        change: function (event, ui) {

                        },
                        update: function (event, ui) {

                        }
                    });

                }
                else {
                    ShowModal(request.Code, request.Message);
                }


            }

        };
    items["newmenu"] =
        {
            "label": GetWords("New Menu"),
            "action": function () {
                AjaxRequestAsync(UrlAction("AddMenu", "Menu"), { "selectedMenu": node.NodeObject, "addAsSub": false },
                    function (data, nodeobject) {

                        if (data.Code == "Success") {
                            $('#Menu').jstree(true).refresh({ skip_loading: false, forget_state: false });      


                        }
                        else {
                            ShowModal(request.Code, request.Message);
                        }

                    },
                    node.NodeObject, function () {
                        // Error Call Back Here

                    });

            }
        }; 
    items["newSubmenu"] =
        {
            "label": GetWords("New Sub Menu"),
        "action": function () {
           
            AjaxRequestAsync(UrlAction("AddMenu", "Menu"), { "selectedMenu": node.NodeObject,"addAsSub": true},
                    function (data, nodeobject) {

                        if (data.Code == "Success") {
                            $('#Menu').jstree(true).refresh({ skip_loading: false, forget_state: false });


                        }
                        else {
                            ShowModal(request.Code, request.Message);
                        }

                    },
                    node.NodeObject, function () {
                        // Error Call Back Here

                    });

            }
        }; 
    items["CopyMenu"] =
        {
            "label": GetWords("Copy"),
            "action": function () {
                copyMenuNode = node;

            }
        }; 

    if (copyMenuNode) {
        items["Paste"] =
            {
                "label": GetWords("Paste"),
                "action": function () {
                    AjaxRequestAsync(UrlAction("CopyMenu", "Menu"), { "targetCopyMenu": copyMenuNode.id, "targetDestMenu": node.id, "copyAsSub": false },
                        function (data, nodeobject) {

                            if (data.Code == "Success") {
                                $('#Menu').jstree(true).refresh({ skip_loading: false, forget_state: false });
                                //$("#schema").jstree(true).search($('#datasearch').val());
                                //$("#" + nodeobject.id).knob();
                            }
                            else {
                                ShowModal(request.Code, request.Message);
                            }

                        },
                        node.NodeObject, function () {
                            // Error Call Back Here

                        });
                }

            };

        items["PasteAsSub"] =
            {
                "label": GetWords("Paste as Sub Menu"),
                "action": function () {
                    AjaxRequestAsync(UrlAction("CopyMenu", "Menu"), { "targetCopyMenu": copyMenuNode.id, "targetDestMenu": node.id, "copyAsSub": true },
                        function (data, nodeobject) {

                            if (data.Code == "Success") {
                                $('#Menu').jstree(true).refresh({ skip_loading: false, forget_state: false });
                                //$("#schema").jstree(true).search($('#datasearch').val());
                                //$("#" + nodeobject.id).knob();
                            }
                            else {
                                ShowModal(request.Code, request.Message);
                            }

                        },
                        node.NodeObject, function () {
                            // Error Call Back Here

                        });
                }

            };
    }

    items["deletemenu"] =
        {
            "label": GetWords("Delete Menu"),
            "action": function () {
             AjaxRequestAsync(UrlAction("DeleteMenu", "Menu"), node.NodeObject,
                    function (data, nodeobject) {

                        if (data.Code == "Success") {                          
                            $('#Menu').jstree(true).refresh({ skip_loading: false, forget_state: false });                            
                        }
                        else {
                            ShowModal(request.Code, request.Message);
                        }

                    },
                    node.NodeObject, function () {
                        // Error Call Back Here

                    });

            }
        }; 

    return items;
}



