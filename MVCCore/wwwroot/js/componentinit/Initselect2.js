﻿var select2width = "100%";
// see https://select2.github.io/examples.html#data-ajax

function GetSelect2OptionBySource(source, targetelement, multiple, placeholder) {
    if (typeof(source) == "string") {
        //console.log(source + " " + tablename );
        return GetSelect2OptionUrl({"Url":source}, multiple, placeholder, targetelement);
    }
    else if (typeof (source) != "string" &&  "Url" in source)
    {
        return GetSelect2OptionUrl(source, multiple, placeholder, targetelement);
    }
    else if (typeof (source) != "string" && source.length > 0 && "TableId" in source[0]) {
        return GetSelect2Ajax(source, multiple, placeholder, targetelement);
    }
    else {
        return GetSelect2Option(source, multiple, placeholder, targetelement);
    }
}
function GetSelect2OptionUrl(apiSource, multiple, placeholder, targetelement) {

    var dropdownparent = targetelement;
    if ($(targetelement).closest(".ikeyvalue, .input-group, .form-group, SearchDiv, .portlet-body, .modal").length > 0) {
        dropdownparent = $(targetelement).closest(".ikeyvalue, .input-group .form-group, SearchDiv, .portlet-body, .modal");
    }  
 
    return {
        dropdownCssClass: "select2drop",
        dropdownParent: dropdownparent,
        keytotext: function (text, dependent) {
            var textdisplay = "";
            var fieldId = $(targetelement).closest(".groupdiv").attr("name");
            var items = {};
            if (text == null || text == "") {
                return items;
            }
            if (!dependent) {
                dependent = $(targetelement).closest(".groupdiv").data("dependentvalue");
            }
            if (!dependent) {
                dependent = "{}";
            }
         
     
            //console.log("text " + text + " dependent " + dependent);
            $.ajax({
                //url: apiSource.Url,
                url: apiSource.Url,
                dataType: "json",
                type: "Post",
                async: false,
                contentType: "application/json",
                data: JSON.stringify({
                    "search": [text],            
                    "fieldId": fieldId,
                    "dependent": dependent,
                    "searchbykey": true,
                }),
                success: function (data) {
                    switch (data.Code) {
                        case "Success":
                            items = data.Obj;
                            break;
                        case "Fail":
                            alert(data.Message);
                            break;
                        case "SessionExpired":
                            window.location.href = data.Obj;
                            break;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    CaptureError(thrownError + xhr.responseText);
                }

            });
            $.each(items, function (index, value) {
                textdisplay += "<div class='boxcontent align-items-center'>" + items[index].text + "</div>";
            });
            return textdisplay;
        },
        placeholder: {
            id: "",
            placeholder: placeholder
        },
        ajax: {
            url: apiSource.Url,
            dataType: "json",
            type: "Post",
            async: true,
            contentType: "application/json",
            delay: 500,
            data: function (params) {
                //console.log("Table " + tablename + " Url " + url );
                var dependent = $(targetelement).closest(".groupdiv").data("dependentvalue");   
                var fieldId = $(targetelement).closest(".groupdiv").attr("name"); 
                return JSON.stringify({
                    "search": [params.term],
                    "fieldId": fieldId,
                    "dependent": dependent,
                    "searchbykey": false
                });
            },
            success: function (data) {

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if (thrownError != "abort") {
                    CaptureError(thrownError + xhr.responseText);
                }

            },
            processResults: function (data, page) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                return {
                    results: data.Obj
                };
            },
            cache: true
        },
        allowClear: true,
        containerCssClass: "select2-containermodal",
        width: "100%",
        dropdownAutoWidth: true,
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection,
        multiple: multiple
    };
}
function GetSelect2Option(data, multiple, placeholder, targetelement) {
    var dropdownparent = targetelement;
    if ($(targetelement).closest(".form-group, SearchDiv, .portlet-body, .modal").length > 0) {
        dropdownparent = $(targetelement).closest(".form-group, SearchDiv, .portlet-body, .modal");
    }
    return {

        dropdownCssClass: "select2drop",
        dropdownParent: dropdownparent,
        keytotext: function (text, dependent) {
            var textdisplay = "";
            var items = data;
            $.each(items, function (index, value) {
                if (items[index].id == text) {
                    textdisplay += "<div class='boxcontent'>" + items[index].text + "</div>";
                }

            });
            return textdisplay;
        },
        data: data,
        containerCssClass: "select2-containermodal",
        width: "100%",
        dropdownAutoWidth: true,
        allowClear: true,
        placeholder: {
            id: "",
            placeholder: placeholder
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        templateResult: formatRepo,
        templateSelection: formatRepoSelection,
        containerCss: function (element) {
            var style = $(element)[0].style;
            return {
                display: style.display
            };
        },
        multiple: multiple
    };
}

function GetSelect2Ajax(source, multiple, placeholder, targetelement) {
    var url = "/Data/Select";
    var dropdownparent = targetelement;
    if ($(targetelement).closest(".form-group, SearchDiv, .portlet-body, .modal").length > 0) {
        dropdownparent = $(targetelement).closest(".form-group, SearchDiv, .portlet-body, .modal");
    }

    return {
        dropdownCssClass: "select2drop",
        dropdownParent: dropdownparent,
        keytotext: function (text, dependent) {
            var textdisplay = "";
            var items = GetSelect2ItemById(url, source, [text]);
            $.each(items, function (index, value) {
                textdisplay += "<div class='boxcontent'>" + items[index].text + "</div>";
            });
            return textdisplay;
        },
        placeholder: {
            id: "",
            placeholder: placeholder
        },
        ajax: {
            url: url,
            dataType: "json",
            type: "Post",
            delay: 500,
            data: function (params) {
                var dependent = $(targetelement).data("dependentvalue");
                return {
                    "searchlist": [params.term],
                    "source": source,
                    "searchById": false,
                    "dependent": dependent
                };
            },
            success: function (data) {

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if (thrownError != "abort") {
                    CaptureError(thrownError + xhr.responseText);
                }

            },
            processResults: function (data, page) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                return {
                    results: data.Obj
                };
            },
            cache: true
        },
        allowClear: true,
        containerCssClass: "select2-containermodal",
        width: "100%",
        dropdownAutoWidth: true,
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection,
        multiple: multiple
    };
}

function formatRepo(repo) {
    if (repo.loading) return repo.text;
    if ($(repo.element).data("html"))
    {
        return $(repo.element).data("html");
    }
    return repo.text;
}

function formatRepoSelection(repo) {
    if ($(repo.element).data("html")) {
        return $(repo.element).data("html");
    }
    return  repo.text;
}
function GetSelect2UrlItemById(apiSource,selectedvalues,targetElement) {
    var dependent = $(targetElement).closest(".groupdiv").data("dependentvalue");
    var fieldId = $(targetElement).closest(".groupdiv").attr("name");
    var result =[];

    $.ajax({
        url: apiSource.Url,
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify({
            "search": selectedvalues,
            "fieldId": fieldId,
            "dependent": dependent,
            "searchbykey": true
        }),
        async: false,
        success: function (data) {
            result = data;  
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
    return result;
}
function GetSelect2ItemById(url, source, selectedvalues) {
   var items = {};
    if (selectedvalues == null || selectedvalues[0] == null)
    {
        return items;
    }    
    

    $.ajax({
        url: url,
        type: "Post",
        async: false,
        data: {"searchlist": selectedvalues, "source": source, "searchById": true },
        success: function(data) {
            switch (data.Code) {
            case "Success":
                items = data.Obj;
                break;
            case "Fail":
                alert(data.Message);
                break;
            case "SessionExpired":
                window.location.href = data.Obj;
                break;
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            CaptureError(thrownError+ xhr.responseText);
        }

    });
    return items;
}

function InitSelect2() {

    try {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // see https://github.com/select2/select2/issues/2927
        //$.fn.select2.defaults.set("theme", "bootstrap3");

        var placeholder = "Select a State";
        var destroyElement = $("select2-container--open");

        var e_select = $("select.select2, select.select2-multiple");
        jQuery.each(e_select, function (i, val) {
            if (!$(val).hasClass('select2-hidden-accessible')) {
                var multiple = $(val).attr("multiple");          
                var source = $(val).data("source");
                var readonly = parseBool($(val).data("readonly"));
                var placeholder = val.getAttribute("data-placeholder-text");
                //var tablename = $(val).data("tablename");
                //var recordid = $(val).data("recordid");
                //var newtag = $(val).data("newtag");            
             
                var selectoption = GetSelect2Option(source, multiple, placeholder, val);
                $(val).select2(selectoption);
                if (readonly) {
                    $(val).prop("disabled", true);
                }
            }

        });

        var e_selectclear = $(".select2-allow-clear");
        jQuery.each(e_selectclear, function (i, val) {
            if (!$(val).hasClass('select2-hidden-accessible')) {
                var placeholder = val.getAttribute("data-placeholder-text");
                var readonly = parseBool($(val).data("readonly"));
                $(e_selectclear[i]).select2({
                    allowClear: true,
                    width: select2width,
                    placeholder: {
                        id: "",
                        placeholder: placeholder
                    },
                    width: null
                });

                if (readonly) {
                    $(e_selectclear[i]).prop("disabled", true);
                }
            }

        });

        var eAjax = $("select.js-data-ajax, select.js-data-ajax-multiple");
        jQuery.each(eAjax, function (i, val) {
            if (!$(val).hasClass('select2-hidden-accessible')) {
                var url = $(val).data("url");
                var source = $(val).data("source");
                var selectedvalues = $(val).data("selected");
                var tablename = $(val).data("tablename");
                var recordid = $(val).data("recordid");
                var readonly = parseBool($(val).data("readonly"));             
                var multiple = $(val).attr("multiple");   
                var placeholder = val.getAttribute("data-placeholder-text");  
                if (typeof (source) == "string" || "Url" in source) {

                    if (typeof (source) == "string") {
                        source = { "Url": source };
                    }
                    var select2urloption = GetSelect2OptionUrl(source, multiple, placeholder, val);
                    setTimeout(function () {
                        var data = GetSelect2UrlItemById(source, selectedvalues, val);
                        select2urloption["data"] = data.Obj;
                        $(val).select2(select2urloption);

                        if (readonly) {
                            $(elementSelect).prop("disabled", true);
                        }
                    }, 3000);
         


                }
                else {
                    
                    var selectajaxoption = GetSelect2Ajax(source, multiple, placeholder, val);
                    var items = GetSelect2ItemById(url, source, selectedvalues);
                    $.each(items, function (x, val) {
                        items[x]["selected"] = true;
                    });
                    selectajaxoption["data"] = items;                   
                    var elementSelect = $(eAjax[i]).select2(selectajaxoption);
                    $(elementSelect).closest(".select2basecontainer").find(".select2-selection__rendered").sortable({
                        containment: 'parent',
                        start: function () { $(elementSelect).select2("onSortStart"); },
                        update: function () { $(elementSelect).select2("onSortEnd"); }
                    });   
                }


            }
        });


        eAjax = $(".select2sortable");
        jQuery.each(eAjax, function (i, val) {
            if (!$(val).hasClass('select2-hidden-accessible')) {
                var element = eAjax[i];   
                var url = $(val).data("url");
                var source = $(val).data("source");
                var readonly = parseBool($(val).data("readonly"));
                var selectedvalues = $(val).data("selected");       
                var items = GetSelect2ItemById(url, source, selectedvalues);
                var select2option = GetSelect2OptionBySource(source, element, true, placeholder)
                select2option["data"] = items;
                var elementSelect = $(element).sortableselect2(select2option);
                if (readonly) {
                    $(elementSelect).prop("disabled", true);
                }
                //var keyvalueItem = {};
                //$.each(items, function (index, value) {
                //    keyvalueItem[items[index].id] = items[index].text;
                //});
                //var sortableItem = [];
                //$.each(selectedvalues, function (index, value) {
                //    sortableItem.push({ id: value, text: keyvalueItem[value] });
                //});
            
           

            }

        });
    } catch (e) {
        console.error(e);
    }

    
}


function DestroySelect2()
{
    //var e_select = $("select.select2, select.select2-multiple");
    //jQuery.each(e_select, function (i, val) {
    //    if ($(val).hasClass('select2-hidden-accessible')) {
    //        $(val).select2('destroy');
    //    }
    //});
     
}

(function ($) {
    //Select 2 Sortable Plugin
    $.fn.Select2Ext = function (options) {
        var settings = $.extend({
        }, options);
        var this_e = this;
        var div = document.createElement("div");
        this.append(div);
        var select = document.createElement("select");
        div.appendChild(select);
        $(select).select2(settings);
        settings.SelectedValues = settings.SelectedValues || [];
        $(select).on("change", function () {
            if ($(select).val() != null && $(select).val() != "") {
                var newitem = CreateDiv("col-1 p1-1", CreateIcon("fas fa-trash-alt", "color:Crimson;"));
                div.appendChild(newitem);
                var data = $(select).select2('data')
                settings.selectedvalues[data.id] = data.text;
                $(newitem).click({ id: data.id }, function (event) {
                    $(newitem).remove();
                    $(this_e).trigger("change");
                    delete settings.selectedvalues[event.data.id];
                });
                $(select).val(null).trigger('change');
                $(this_e).val(JSON.stringify(settings.selectedvalues));
            }
        });

    };
}(jQuery));