/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */


'use strict';
function InitFileUpload() {
    var upload_e = $(".fileuploader");
    jQuery.each(upload_e, function (i, val) {

        try {     
            var option = ParseToObj($(val).data("options"));       
            if (!option || !option.init) {
                option = GetDefaultUploadOption();               
            }
            option = GetDefaultUploadOption();
            option.init(val);  
           
        } catch (e) {            

        }


    });
 
   
}


function GetDefaultUploadOption() {
   return {
       init: function (element) {
       /*Do not remove these comment:InitFileUpload*/
           var multiple = parseBool($(element).data("multiple"));
           var readonly = parseBool($(element).data("readonly"));
           var obj = {
               "maxFileSize": 20000000000,
               "disableImageResize": false,
               "imageForceResize": true,
               "imageMaxWidth": 1080,
               "imageMaxHeight": 1080,
               "url": "/Upload/UploadFiles",
               "dropZone": element
           };
           if (!multiple) {
               obj["maxNumberOfFiles"] = 1;
           }
            var uploadelement = $(element).fileupload(obj);
            $(element).addClass("fileupload-processing");
            var formData = $(element).serializeArray();
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: "/Upload/GetUploadedFiles",
                dataType: "json",
                data: formData,
                type: "Post",
                context: uploadelement
            })
                .always(function () {
                    $(element).removeClass("fileupload-processing");
                })
                .done(function (result) {
                    $(element)
                        .fileupload("option", "done")
                        // eslint-disable-next-line new-cap
                        .call(element, $.Event("done"), {
                            result: {
                                files: result
                            }
                        });
                    InitSelect2();
                });

        }
    };
}

