//https://ourcodeworld.com/articles/read/309/top-5-best-code-editor-plugins-written-in-javascript

//Custom Auto Completed Sample
var langTools = ace.require("ace/ext/language_tools");
//var date = new Date();
//var today = date.getDay() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
//var endofmonth = "31/" + (date.getMonth() + 1) + " / " + date.getFullYear();
//var firstofmonth = "1/" + (date.getMonth() + 1) + " / " + date.getFullYear();
//var dateofmonth = "24/" + (date.getMonth() + 1) + " / " + date.getFullYear();
//var myList = [
//    { caption: "Today" + today, desc: today, value: "Datetime.Now" },
//    { caption: "End of month", desc: endofmonth, value: "DateExtension.EndOfThisMonth()" },
//    { caption: "First of month", desc: firstofmonth, value: "DateExtension.FirstOfThisMonth()" },
//    { caption: "Date of month.", desc: "DateExtension.ThisMonth(24) ->" + dateofmonth, value: "DateExtension.ThisMonth(24)" },

//    { caption: "DateTime.Now.AddYears(1)", desc: "AddYears(int)", value: "DateTime.Now.AddYears(1)" },
//    { caption: "DateTime.Now.AddMonths(1)", desc: "AddMonths(int)", value: "DateTime.Now.AddMonths(1)" },
//    { caption: "DateTime.Now.AddDays(1)", desc: "AddDays(int)", value: "DateTime.Now.AddDays(1)" },
//    { caption: "DateTime.Now.AddHours(1)", desc: "AddHours(int)", value: "DateTime.Now.AddHours(1)" },
//    { caption: "DateTime.Now.AddMinutes(1)", desc: "AddMinutes(int)", value: "DateTime.Now.AddMinutes(1)" },
//    { caption: "DateTime.Now.AddSeconds(1)", snippet: "AddSeconds(int)", value: "DateTime.Now.AddSeconds(1)" },
//];

var completer = {
    identifierRegexps: [/[^\s]+/],
    getCompletions: function (editor, session, pos, prefix, callback) {
        try {
            var element = editor.container;
            var opt = $(element).data("options");
            var option = {};
            if (typeof (opt) == "string" && opt == "datetimevalueeditor") {
                option = GetDefaultCodeEditorOption();
            }
            else if (typeof (opt) == "string") {
                var temp = opt.split('\n');
                if (temp.length > 1) {
                    try {
                        option = ParseToObj(temp[0]);
                        if (option != null) {
                            opt = opt.replace(temp[0], "");
                            option = ParseToObj(opt);
                        }
                        else {
                            option = ParseToObj(opt);
                        }
                    }
                    catch (e) {

                    }

                }
            }

            if (option && option.autocompleteUrl) {

                var param = {};
                var tag = false;
                if (typeof option.getparam === "function") {
                    param = option.getparam(element);
                }
                if (option.tags) {
                    tag = option.tags;
                }
                try {
                    // signarRServer.server.signalChanged(userName, updateRecord);
                    
                    $.ajax({
                        "dataType": "json",
                        "contentType": "application/json",
                        "type": "POST",
                        "url": option.autocompleteUrl,
                        "data": JSON.stringify({ name: prefix, dependent: param, tags: tag }),
                        "async": true,
                        "success":
                            function (data) {                                
                                switch (data.Code) {
                                    case "Success":
                                        // $("#" + id + " .status").html("<span style='color:red;'>Record Saved</span>");
                                        if (data && data.Obj) {
                                            callback(null, data.Obj);
                                        }
                                        break;                             
                                }
                            },
                        "error": function (jqXHR, exceptionC) {

                            var msg = "";
                            if (jqXHR.status === 0) {
                                msg = "Not connect.\n Verify Network.";
                            } else if (jqXHR.status == 404) {
                                msg = "Requested page not found. [404]";
                            } else if (jqXHR.status == 500) {
                                msg = "Internal Server Error [500].";
                            } else if (exception === "parsererror") {
                                msg = "Requested JSON parse failed.";
                            } else if (exception === "timeout") {
                                msg = "Time out error.";
                            } else if (exception === "abort") {
                                msg = "Ajax request aborted.";
                            } else {
                                msg = "Uncaught Error.\n";
                            }
                            console.log(msg + " " + jqXHR.responseText);         
                        }
                    });



                }
                catch (err) {
                }
            }
            
           
        }
        catch (e) {
            console.error(e);
        }
    },
    getDocTooltip: function (item) {
        if (item.type == "snippet") {
            item.docHTML = ["<b>",
                item.value,
                "</b>",
                "<hr></hr>",
                item.desc].join("");
        }
    }

};
langTools.addCompleter(completer);

function InitCodeEditor()
{     
    var element = $(".codeEditor");
    jQuery.each(element, function (i, val) {           
        try {
            
            if ($(val).hasClass("ace_editor")) {
                return;
            }
            //var temp = $(val).data("options");
         
            //var opt = {};
            //if (typeof (temp) == "string") {
            //    var tempobj = ParseToObj(temp);
            //    opt.mode = tempobj.mode;
            //    opt.value = tempobj.theme;
            //}
            //else {
            //    var tempobj = ParseToObj(temp.option);
            //    opt.mode = tempobj.mode;
            //    opt.theme = tempobj.theme;             
            //}  
            var buildDom = require("ace/lib/dom").buildDom;
            var editor = ace.edit(val);
            var code = $(val).data("value");
            if (!code) {
                code = {};
                code.Value = "";
                code.option = "";
            }
            var value = code.Value;
            var options = ParseToObj(code.option);
            if (!options) {
                options = {};
            }
            var opt = {};
            opt.mode = options.mode || "ace/mode/javascript";
            opt.theme = options.theme || "ace/theme/monokai";

            editor.setTheme(opt.theme);
            editor.session.setUseWrapMode(true);
            editor.session.setNewLineMode("windows");
            editor.session.setMode(opt.mode);
            editor.setOptions({
                useWrapMode: false,

                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: true,

            });
            editor.setWrapBehavioursEnabled(false);
            var refs = {};
            var toolbar = $(val).closest(".editorContainer").find(".editortoolbar");

            buildDom(["div", { class: "toolbar" },
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Cancel change",
                    ref: "cancelButton",
                    onclick: function () {
                        editor.setReadOnly(false);
                        $(refs.commitButton).hide();
                        $(refs.formatbutton).hide();
                        $(refs.minifybutton).hide();
                        $(refs.editButton).show();
                        $(refs.cancelButton).hide();
                        if (typeof (value) == "string") {
                            editor.setValue(value);
                        } else {
                            editor.setValue(JSON.stringify(value));
                        }
                    }
                }, ["i", {
                    class: "fas fa-times text-danger"
                }, ""]],
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Edit",
                    ref: "editButton",
                    onclick: function () {
                        editor.setReadOnly(false);
                        $(refs.commitButton).show();
                        $(refs.cancelButton).show();
                        $(refs.formatbutton).show();
                        $(refs.minifybutton).show();
                        $(refs.editButton).hide();
                    }
                }, ["i", {
                    class: "fas text-primary"
                }, "pen-alt"]],
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Commit change",
                    ref: "commitButton",
                    onclick: function () {
                        editor.setReadOnly(true);
                        $(refs.commitButton).hide();
                        $(refs.editButton).show();
                        $(refs.cancelButton).hide();
                        $(refs.formatbutton).hide();
                        $(refs.minifybutton).hide();
                        $(val).trigger("commitchange");
                    }
                }, ["i", {
                    class: "fas fa-check text-success"
                }, ""]],
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Full screen",
                    ref: "extendbutton",
                    onclick: function () {
                        editor.container.webkitRequestFullscreen();
                    }
                }, ["i", {
                    class: "fas fa-expand-arrows-alt text-dark"
                }, ""]],
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Beutify",
                    ref: "formatbutton",
                    onclick: function () {
                        var beautify = ace.require("ace/ext/beautify");
                        beautify.beautify(editor.session);

                    }
                }, ["i", {
                    class: "fas fa-code text-dark"
                }, ""]],
                ["button", {
                    class: "mr-2",
                    style: "cursor:pointer;",
                    title: "Minify",
                    ref: "minifybutton",
                    onclick: function () {
                        var value = editor.getValue().replace(/[\s]+/g, " ");
                        editor.setValue(value.replace(/[\s]+/g, " "));
                        editor.show
                    }
                }, ["i", {
                    class: "far fa-file-archive text-dark"
                }, ""]],

            ], toolbar[0], refs);
            editor.setReadOnly(true);
            $(refs.commitButton).hide();
            $(refs.cancelButton).hide();
            $(refs.formatbutton).hide();
            $(refs.minifybutton).hide();

            if (typeof (value) == "string") {
                editor.setValue(value);
            } else {
                editor.setValue(JSON.stringify(value));
            }

        } catch (e) {
            console.error(e);
        }
       
        

    });

}
function OffCodeEditorChange(editor) {
    $(editor).off("change");
}
var editorTmrChange;
function OnCodeEditorChange(editor, element) {
      
    var elementEditor = element;
    editor.on("change", function (e) {
        var value = editor.getValue();
        if (!value && value == "") {            
            if (editorTmrChange) {
                clearTimeout(editorTmrChange);
            }
            editorTmrChange = setTimeout(function () {
                $(elementEditor).trigger("change");
                console.log("change");
            }, 2000);
        }

    });
}
function GetDefaultCodeEditorOption() {
    return {
       /*Do not remove this comment:InitCodeEditor*/
       getparam: function (element) {
          return $(element).data("dependentvalue");
       },
       tags: false,
       autocompleteUrl:"/AceEditorCompleter/GetCSharpCompleter"
    };
}


