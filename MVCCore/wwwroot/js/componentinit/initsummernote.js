
function InitSummerNote() {
    var element = $(".Summernote");
    $.ajax({
        url: 'https://api.github.com/emojis',
        async: false
    }).then(function (data) {
        window.emojis = Object.keys(data);
        window.emojiUrls = data;
    });
    jQuery.each(element, function (i, val) {

        try {
            //$(val).summernote({ height: 200 });
            var option = ParseToObj($(val).data("options"));
            if (!option || option != null) {
                option = GetDefaultSummerNoteOption();
            }         
            var readonly = parseBool($(val).data("readonly"));
            if (readonly) {
                $(val).summernote("disable");
            }
            else {
                $(val).summernote(option);
            }
            
            if (option) {
        
//                $(val).summernote({
//                    height: 200,
//                    hint: [
//                        {
//                            match: /:([\-+\w]+)$/,
//                            search: function (keyword, callback) {
//                                callback($.grep(emojis, function (item) {
//                                    return item.indexOf(keyword) === 0;
//                                }));
//                            },
//                            template: function (item) {
//                                var content = emojiUrls[item];
//                                return '<img src="' + content + '" width="20" /> :' + item + ':';
//                            },
//                            content: function (item) {
//                                var url = emojiUrls[item];
//                                if (url) {
//                                    return $('<img />').attr('src', url).css('width', 20)[0];
//                                }
//                                return '';
//                            }
//                        }

//                        ,
//                        {
//                            match: /\B@text(\w*)$/,
//                            search: function (keyword, callback) {
//                                var tableName = $("div[name='BJAHHFtussJvtGEtsBrFuAsAuwsvAJGIvs']").find("select").val();
//                                AjaxRequestAsync('/Schema/SelectFields',
//                                    { search: keyword, tablename: tableName, recordid: "", searchbykey: false },
//                                    function (data) {
//                                        callback($.each(data.Obj, function (i, val) {
//                                            return val;
//                                        }));
//                                    }
//                                );

//                            },
//                            template: function (item) {                      
//                                return "<span data-id='" + item.id + "'>" + item.text + "</span>";
//                            },
//                            content: function (item) {
//                                var span = document.createElement("span");
//                                $(span).attr("data-id", item.id);
//                                $(span).attr("class", "m-1 p-1");
//                                span.style.color = "blue";
//                                span.style.border = "thick solid blue";
//                                $(span).text(item.text);
//                                return span;
//                            }
//                        },
//                        {
//                            match: /\B@button(\w*)$/,
//                            search: function (keyword, callback) {
//                                var tableName = $("div[name='BJAHHFtussJvtGEtsBrFuAsAuwsvAJGIvs']").find("select").val();
//                                AjaxRequestAsync('/Schema/SelectFields',
//                                    { search: keyword, tablename: tableName, recordid: "", searchbykey: false },
//                                    function (data) {
//                                        var newTag = { id: keyword, text: keyword,newtag:true };
//                                        data.Obj.splice(0, 0, newTag);
//                                        callback($.each(data.Obj, function (i, val) {
//                                            return val;
//                                        }));
//                                    }
//                                );

//                            },
//                            template: function (item) {
               
//                                return "<span data-id='" + item.id + "'>" + item.text + "</span>";
//                            },
//                            content: function (item) {
//                                var btn = document.createElement("button");
//                                $(btn).attr("class", "btn btn-primary");
//                                $(btn).attr("onclick", "TemplateClick('/Data/example','{{tableid}}','{{recordid}}',{},false)");
//                                if (!item.newtag) {
//                                    var span = document.createElement("span");
//                                    $(span).attr("data-id", item.id);
//                                    $(span).attr("class", "m-1 p-1");
//                                    span.style.color = "blue";
//                                    span.style.border = "thick solid blue";
//                                    $(span).text(item.text);
//                                    $(btn).append(span);
//                                }
//                                else {
//                                    $(btn).text(item.text);
//                                }
                  
//                                return btn;
//                            }
//                        }

//                    ]
//                }
//);
            }
        }
        catch (e) {
            console.error(e);
        }

      
       

    });
    
    //$(".Summernote").summernote({ height: 200 });
           //API:
    //var sHTML = $('#summernote_1').code(); // get code
    //$('#summernote_1').destroy(); // destroy
}

function GetDefaultSummerNoteOption() {
    return {
        /*Do not remove this comment:InitSummernote*/
        height: 200,
        hint: [{
            match: /:([\-+\w]+)$/,
            search: function (keyword, callback) {
                callback($.grep(emojis, function (item) {
                    return item.indexOf(keyword) === 0;
                }));
            },
            template: function (item) {
                var content = emojiUrls[item];
                return "<img src=\"" + content + "\" width=\"20\" /> :" + item + ":";
            },
            content: function (item) {
                var url = emojiUrls[item];
                if (url) {
                    return $("<img />").attr("src", url).css("width", 20)[0];
                }
                return "";
            }
        }
        ]

    };
}