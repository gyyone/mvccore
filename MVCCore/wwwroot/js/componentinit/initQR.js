﻿function InitQR() {
    var element = $(".qrcodegen");
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initialize");

        if (!initialize) {
            $(val).attr("initialize", "true");
            var text = $(val).data("text");
            var mode = $(val).data("mode");
            var msize = $(val).data("imagesize");
            var imageurl = $(val).data("imageurl");
            var qrsize = $(val).data("qrsize");
            var imageelement = document.createElement("img");
            imageelement.onload = function () {
                if (!msize) {
                    msize = 0.2;
                }
                if (!mode) {
                    mode = 4;
                }
                if (!text) {
                    text = "NA";
                }
                if (!qrsize) {
                    qrsize = 400;
                }
                $(val).qrcode({
                    size: qrsize,
                    text: text,
                    mode: mode,
                    image: imageelement,
                    mSize: msize
                });
            }
            imageelement.src = imageurl;
           
        }
    });
}
