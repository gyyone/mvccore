﻿var fixedColumnList = [];
var firstColumnWidth = 150;
/**
 * By default DataTables only uses the sAjaxSource variable at initialisation
 * time, however it can be useful to re-read an Ajax source and have the table
 * update. Typically you would need to use the `fnClearTable()` and
 * `fnAddData()` functions, however this wraps it all up in a single function
 * call.
 *
 * DataTables 1.10 provides the `dt-api ajax.url()` and `dt-api ajax.reload()`
 * methods, built-in, to give the same functionality as this plug-in. As such
 * this method is marked deprecated, but is available for use with legacy
 * version of DataTables. Please use the new API if you are used DataTables 1.10
 * or newer.
 *
 *  @name fnReloadAjax
 *  @summary Reload the table's data from the Ajax source
 *  @author [Allan Jardine](http://sprymedia.co.uk)
 *  @deprecated
 *
 *  @param {string} [sNewSource] URL to get the data from. If not give, the
 *    previously used URL is used.
 *  @param {function} [fnCallback] Callback that is executed when the table has
 *    redrawn with the new data
 *  @param {boolean} [bStandingRedraw=false] Standing redraw (don't changing the
 *      paging)
 *
 *  @example
 *    var table = $('#example').dataTable();
 *    
 *    // Example call to load a new file
 *    table.fnReloadAjax( 'media/examples_support/json_source2.txt' );
 *
 *    // Example call to reload from original file
 *    table.fnReloadAjax();
 */



function InitDataTable() {
    try {
        // Add event listener for opening and closing details
        var extensions = {
            "sFilterInput": "form-control",
            "sLengthSelect": "form-control"
        }
        // Used when bJQueryUI is false
        $.extend($.fn.dataTableExt.oStdClasses, extensions);
        // Used when bJQueryUI is true
        $.extend($.fn.dataTableExt.oJUIClasses, extensions);
        var ecolumnHeader = $(".columnheadertable");
        jQuery.each(ecolumnHeader, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {

                var dtProperty =
                {
                    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                    "language": {
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        },
                        "emptyTable": "No data available in table",
                        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                        "infoEmpty": "No entries found",
                        "infoFiltered": "(filtered1 from _MAX_ total entries)",
                        "lengthMenu": "_MENU_ entries",
                        "search": "Search:",
                        "zeroRecords": "No matching records found"
                    },
                    scrollY: "45vh",
                    scrollX: true,
                    paging: false,
                    rowReorder: true,
                    "columnDefs": [
                        { "width": "20%", "targets": 0 },
                        { "width": "60%", "targets": 1 },
                        { "width": "20%", "targets": 2 }
                    ]
                };
                $(val).DataTable(dtProperty);

            }
        });

        var eAjax = $(".modalhistoryItem");
        jQuery.each(eAjax, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {
                var urllink = $(val).data("url");
                var tableName = $(val).data("table");
                var keyvalue = $(val).data("header");
                var trash = $(val).data("trash");
                var allinputs = $(val).find(".fieldsearch");
                var pageLength = $(val).data("pagelength");
                var searchterm = $(val).data("searchterm");
                var notsortable = $(val).data("notsortable");
                var sort = $(val).data("sort");
                var columns = [];
                var order = [];
                var columnDefs = [];
                var targetNotSortable = [];
                PupulateFilter(allinputs, searchterm);
                for (var x in keyvalue) {

                    if (notsortable.indexOf(keyvalue[x]) >= 0) {
                        targetNotSortable.push(parseInt(x));
                    }
                    else {
                        if (keyvalue[x] == "Date") {
                            order.push([x, 'desc']);
                        }

                    }
                    columns.push({ "data": x.toString() });
                }

                columnDefs.push({ "targets": targetNotSortable, "orderable": false });
                //columnDefs.push({ "targets": 2, "visible": false });
                pageLength = pageLength == 0 ? 20 : pageLength;
                var dtProperty =
                {
                    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                    "language": {
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        },
                        "emptyTable": "No data available in table",
                        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                        "infoEmpty": "No entries found",
                        "infoFiltered": "(filtered1 from _MAX_ total entries)",
                        "lengthMenu": "_MENU_ entries",
                        "search": "Search:",
                        "zeroRecords": "No matching records found"
                    },

                    colReorder: true,
                    deferRender: true,
                    scrollCollapse: false,
                    scrollY: "45vh",
                    scrollX: true,
                    scroller: {
                        displayBuffer: 1
                    },
                    // Or you can use remote translation file
                    //"language": {
                    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                    //},
                    buttons: [
                        //{ extend: 'print', className: 'btn default' },
                        //{ extend: 'copy', className: 'btn default' },
                        //{ extend: 'pdf', className: 'btn default' },
                        //{ extend: 'excel', className: 'btn default' },

                        { extend: 'colvis', className: 'btn default' },
                        { extend: "csv", className: "btn default" }

                        //{
                        //    text: 'Reload',
                        //    className: 'btn default',
                        //    action: function(e, dt, node, config) {
                        //        //dt.ajax.reload();
                        //        alert('Custom Button');
                        //    }
                        //}
                    ],
                    //Default Sort Column
                    "order": order,
                    "columns": columns,
                    "columnDefs": columnDefs,
                    "dom": "<'row'<'col-sm-6'i><'col-sm-6'>><'row'<'col-sm-12't>><'row'<'col-sm-5'><'col-sm-7'>>",

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                    // So when dropdowns used the scrollable div should be removed. 
                    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                    "iDisplayLength": 20,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": urllink,
                    "bJQueryUI": true,
                    "bDeferRender": true,
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        PopulateServerParam(aoData, tableName, allinputs, keyvalue, searchterm,sort, trash);
                        aoData.push({ "name": "iTabletype", "value": "LoadOnScrollTable" });
                    },
                    "initComplete": onInitCompleted,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        var api = this.api();
                        $.ajax({
                            "dataType": "json",
                            "contentType": "application/x-www-form-urlencoded; charset=UTF-8",
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (data) {

                                    switch (data.Code) {
                                        case "Success":
                                            HandlerDataCallback(fnCallback, data, api);
                                            break;
                                        case "Fail":
                                            CaptureError(data.Message);
                                            break;
                                        case "SessionExpired":
                                            window.location.href = data.Obj;
                                            break;
                                    }

                                },
                            "error": function (jqXHR, exceptionC) {
                                var msg = "";
                                if (jqXHR.status === 0) {
                                    msg = "Not connect.\n Verify Network.";
                                } else if (jqXHR.status == 404) {
                                    msg = "Requested page not found. [404]";
                                } else if (jqXHR.status == 500) {
                                    msg = "Internal Server Error [500].";
                                } else if (exception === "parsererror") {
                                    msg = "Requested JSON parse failed.";
                                } else if (exception === "timeout") {
                                    msg = "Time out error.";
                                } else if (exception === "abort") {
                                    msg = "Ajax request aborted.";
                                } else {
                                    msg = "Uncaught Error.\n";
                                }
                                CaptureError(msg + jqXHR.responseText);
                            }
                        });
                    }

                };

                InitTableFeature(val, tableName, dtProperty, allinputs, keyvalue, false);

            }
        });

        eAjax = $(".Normaldatatable");
        jQuery.each(eAjax, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {
                var allinputs = $(val).find(".fieldsearch");
                var pageLength = $(val).data("pagelength");
                var searchterm = $(val).data("searchterm");
                var keyvalue = $(val).data("header");
                var leftcolumnfix = $(val).data("leftcolumnfixed");
                var rightcolumnfix = $(val).data("rightcolumnfixed");
                pageLength = pageLength == 0 ? 20 : pageLength;
                var sort = $(val).data("sort");
                PupulateFilter(allinputs, searchterm);
                var dtProperty = {
                    colReorder: true,
                    scrollCollapse: false,
                    scrollY: "50vh",
                    scrollX: true,
                    paging: false,
                    "order": [[0, "asc"]],
                    buttons: [
                        //{ extend: 'print', className: 'btn default' },
                        //{ extend: 'copy', className: 'btn default' },
                        //{ extend: 'pdf', className: 'btn default' },
                        //{ extend: 'excel', className: 'btn default' },
                        { extend: "csv", className: "btn default" }

                        //{
                        //    text: 'Reload',
                        //    className: 'btn default',
                        //    action: function(e, dt, node, config) {
                        //        //dt.ajax.reload();
                        //        alert('Custom Button');
                        //    }
                        //}
                    ],
                    "initComplete": onInitCompleted,
                    // set the initial value
                    fixedColumns: {
                        leftColumns: leftcolumnfix,
                        rightColumns: rightcolumnfix
                    },
                    "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                };

                var table = $(val).DataTable(dtProperty);
                table.columns().every(function () {
                    var that = this;
                    if (that[0][0] in keyvalue) {
                        allinputs.each(function (index) {
                            if (allinputs[index].name == keyvalue[that[0][0]]) {
                                $(allinputs[index]).on("change", function () {
                                    if (allinputs[index].className == "") {

                                    }
                                    else {
                                        var valcontrol = $(this).val();
                                        var searchstr = this.value;
                                        if ($.isArray(valcontrol)) {
                                            searchstr = "";
                                            for (var x in valcontrol) {
                                                searchstr += valcontrol[x] + "|"
                                            }
                                            searchstr = searchstr.slice(0, -1)

                                        }
                                        if (that.search() !== searchstr) {
                                            that.search(searchstr, true, false).draw();
                                        }

                                    }


                                });
                            }

                        });
                    }
                });

                //table.columns().every(function () {
                //    var that = this;
                //    $(allinputs).on('change', function () {
                //        if (that.search() !== this.value) {
                //            that.search(this.value)
                //                .draw();
                //        }
                //    });

                //});
            }


            //table.search(search);
        });

        //standardTable, No ajax. Normal Table init
        eAjax = $(".OfflineTable");
        jQuery.each(eAjax, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {
                var urllink = $(val).data("url");
                var tableName = $(val).data("table");
                var keyvalue = $(val).data("header");
                var allinputs = $(val).find("input");
                var trash = $(val).data("trash");
                var pageLength = $(val).data("pagelength");
                var searchterm = $(val).data("searchterm");
                var columns = [];
                var order = [];
                var leftcolumnfix = $(val).data("leftcolumnfixed");
                var rightcolumnfix = $(val).data("rightcolumnfixed");
                var sort = $(val).data("sort");
                PupulateFilter(allinputs, searchterm);
                for (var x in keyvalue) {
                    columns.push({ "data": x.toString() });
                    if (keyvalue[x] == "Date") {
                        order.push([x, 'desc']);
                    }

                }
                pageLength = pageLength == 0 ? 20 : pageLength;
                var param = { "iTableName": tableName, "iTtabletype": "OfflineTable", "iTrash": trash, "sSearch": "", "iDisplayLength": "-1", "iDisplayStart": 0, "sEcho": 0, "iColumns": allinputs.length, "iSortingCols": 0 };
                allinputs.each(function (x) {
                    param[allinputs[x].name] = allinputs[x].value;
                });
                for (var x in keyvalue) {
                    param[x] = keyvalue[x];
                }
                param["searchterm"] = searchterm;
                var jsondata = AjaxRequest(urllink, param);
                var dtProperty =
                {
                    fixedColumns: {
                        leftColumns: leftcolumnfix,
                        rightColumns: rightcolumnfix
                    },
                    data: jsondata.Obj["aaData"].aaData,
                    "columns": columns,
                    "initComplete": onInitCompleted,
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api();
                        for (var x in jsondata.Obj["Footer"]) {
                            var column = api.column(x);
                            $(column.footer()).html(jsondata.Obj["Footer"][x]);
                        }
                    }

                };
                $(val).DataTable({ "columns": columns, data: jsondata.Obj["aaData"].aaData });
                InitTableControl();
                //InitTableFeature(val,tableName, dtProperty, allinputs, keyvalue,true);

            }
        }
        );

        //None Pagination
        eAjax = $(".LoadOnScrollTable");
        jQuery.each(eAjax, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {
                var urllink = $(val).data("url");
                var tableName = $(val).data("table");
                var keyvalue = $(val).data("header");
                var trash = $(val).data("trash");
                var allinputs = $(val).find(".fieldsearch");
                var pageLength = $(val).data("pagelength");
                var searchterm = $(val).data("searchterm");
                var notsortable = $(val).data("notsortable");
                var leftcolumnfix = $(val).data("leftcolumnfixed");
                var rightcolumnfix = $(val).data("rightcolumnfixed");
                var sort = $(val).data("sort");
                var columns = [];
                var order = [];
                var columnDefs = [];
                var targetNotSortable = [];
                PupulateFilter(allinputs, searchterm);
                for (var x in keyvalue) {

                    if (notsortable.indexOf(keyvalue[x]) >= 0) {
                        targetNotSortable.push(parseInt(x));
                    }
                    else {
                        if (keyvalue[x] == "Date") {
                            order.push([x, 'desc']);
                        }

                    }
                    columns.push({ "data": keyvalue[x] });
                }

                columnDefs.push({ "targets": targetNotSortable, "orderable": false });
                //columnDefs.push({ "targets": 2, "visible": false });
                pageLength = pageLength == 0 ? 20 : pageLength;
                var dtProperty =
                {
                    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                    "language": {
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        },
                        "emptyTable": "No data available in table",
                        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                        "infoEmpty": "No entries found",
                        "infoFiltered": "(filtered1 from _MAX_ total entries)",
                        "lengthMenu": "_MENU_ entries",
                        "search": "Search:",
                        "zeroRecords": "No matching records found"
                    },

                    colReorder: true,
                    deferRender: true,
                    scrollCollapse: false,
                    scrollY: calcDataTableHeight(),
                    scrollX: true,
                    fixedColumns: {
                        leftColumns: leftcolumnfix,
                        rightColumns: rightcolumnfix
                    },
                    scroller: {
                        displayBuffer: 1
                    },
                    // Or you can use remote translation file
                    //"language": {
                    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                    //},
                    buttons: [
                        //{ extend: 'print', className: 'btn default' },
                        //{ extend: 'copy', className: 'btn default' },
                        //{ extend: 'pdf', className: 'btn default' },
                        //{ extend: 'excel', className: 'btn default' },

                        { extend: 'colvis', className: 'btn default' },
                        { extend: "csv", className: "btn default" }

                        //{
                        //    text: 'Reload',
                        //    className: 'btn default',
                        //    action: function(e, dt, node, config) {
                        //        //dt.ajax.reload();
                        //        alert('Custom Button');
                        //    }
                        //}
                    ],
                    //Default Sort Column
                    "order": order,
                    "columns": columns,
                    "columnDefs": columnDefs,
                    "dom": "<'row'<'col-sm-6'i><'col-sm-6'>><'row'<'col-sm-12't>><'row'<'col-sm-5'><'col-sm-7'>>",

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                    // So when dropdowns used the scrollable div should be removed. 
                    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                    "iDisplayLength": 20,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": urllink,
                    "bJQueryUI": true,
                    "bDeferRender": true,
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        PopulateServerParam(aoData, tableName, allinputs, keyvalue, searchterm,sort, trash);
                        aoData.push({ "name": "iTabletype", "value": "LoadOnScrollTable" });
                    },
                    "initComplete": onInitCompleted,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        var api = this.api();
                        $.ajax({
                            "dataType": "json",
                            "contentType": "application/x-www-form-urlencoded; charset=UTF-8",
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (data) {

                                    switch (data.Code) {
                                        case "Success":
                                            HandlerDataCallback(fnCallback, data, api);
                                            break;
                                        case "Fail":
                                            CaptureError(data.Message);
                                            break;
                                        case "SessionExpired":
                                            window.location.href = data.Obj;
                                            break;
                                    }

                                },
                            "error": function (jqXHR, exception) {
                                var msg = "";
                                if (jqXHR.status === 0) {
                                    msg = "Not connect.\n Verify Network.";
                                } else if (jqXHR.status == 404) {
                                    msg = "Requested page not found. [404]";
                                } else if (jqXHR.status == 500) {
                                    msg = "Internal Server Error [500].";
                                } else if (exception === "parsererror") {
                                    msg = "Requested JSON parse failed.";
                                } else if (exception === "timeout") {
                                    msg = "Time out error.";
                                } else if (exception === "abort") {
                                    msg = "Ajax request aborted.";
                                } else {
                                    msg = "Uncaught Error.\n";
                                }
                                CaptureError(msg + jqXHR.responseText);
                            }
                        });
                    }

                };

                InitTableFeature(val, tableName, dtProperty, allinputs, keyvalue, false);

            }
        }
        );

        //Pagination
        eAjax = $(".PaginationTable");
        jQuery.each(eAjax, function (i, val) {
            if (!$.fn.DataTable.isDataTable(val)) {
                var urllink = $(val).data("url");
                var tableName = $(val).data("table");
                var keyvalue = $(val).data("header");
                var allinputs = $(val).find(".fieldsearch");
                var sort = $(val).data("sort");
                var trash = $(val).data("trash");
                var pageLength = $(val).data("pagelength");
                var searchterm = $(val).data("searchterm");
                var notsortable = $(val).data("notsortable");
                var leftcolumnfix = $(val).data("leftcolumnfixed");
                var rightcolumnfix = $(val).data("rightcolumnfixed");
                var columns = [];
                var order = [];
                var columnDefs = [];
                var targetNotSortable = [];
                PupulateFilter(allinputs, searchterm);
                for (var x in keyvalue) {

                    if (notsortable.indexOf(keyvalue[x]) >= 0) {
                        targetNotSortable.push(parseInt(x));
                    }
                    else {
                        if (keyvalue[x] == "Date") {
                            order.push([x, 'desc']);
                        }

                    }
                    columns.push({ "data": keyvalue[x] });
                }

                columnDefs.push({ "targets": targetNotSortable, "orderable": false });

                pageLength = pageLength == 0 ? 20 : pageLength;
                var dtProperty =
                {
                    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                    "language": {
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        },
                        "emptyTable": "No data available in table",
                        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                        "infoEmpty": "No entries found",
                        "infoFiltered": "(filtered1 from _MAX_ total entries)",
                        "lengthMenu": "_MENU_",
                        "search": "Search:",
                        "zeroRecords": "No matching records found"
                    },
                    colReorder: true,
                    scrollCollapse: false,
                    scrollY: "60vh",
                    scrollX: true,
                    deferRender: true,
                    paging: true,
                    fixedColumns: {
                        leftColumns: leftcolumnfix,
                        rightColumns: rightcolumnfix
                    },
                    // Or you can use remote translation file
                    //"language": {
                    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                    //},

                    buttons: [
                        //{ extend: 'print', className: 'btn default' },
                        //{ extend: 'copy', className: 'btn default' },
                        //{ extend: 'pdf', className: 'btn default' },
                        //{ extend: 'excel', className: 'btn default' },
                        { extend: "csv", className: "btn default" }
                    ],
                    //Default Sort Column       
                    "order": order,
                    "columns": columns,
                    "columnDefs": columnDefs,
                    "lengthMenu": [
                        [10, 20, 50, 100, 200],
                        [10, 20, 50, 100, 200]
                    ], // change per page values here,
                    //"deferLoading": 3,
                    // set the initial value
                    "pageLength": pageLength,
                    "pagingType": "full_numbers",
                    "iPage": pageLength,
                    "dom": '<"top" <"row" <"nowrap" lp>> >rt<"bottom" i>',
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": urllink,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bDeferRender": true,
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        PopulateServerParam(aoData, tableName, allinputs, keyvalue, searchterm,sort, trash);
                        aoData.push({ "name": "tabletype", "value": "PaginationTable" });
                    },
                    "initComplete": onInitCompleted,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        var api = this.api();
                        $.ajax({
                            "dataType": "json",
                            "contentType": "application/x-www-form-urlencoded; charset=UTF-8",
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (data) {

                                    switch (data.Code) {
                                        case "Success":
                                            //fnCallback(data.Obj["aaData"]);
                                            HandlerDataCallback(fnCallback, data, api);
                                            break;
                                        case "Fail":
                                            CaptureError(data.Message);
                                            break;
                                        case "SessionExpired":
                                            window.location.href = data.Obj;
                                            break;
                                    }
                                },
                            "error": function (jqXHR, exceptionC) {
                                var msg = "";
                                if (jqXHR.status === 0) {
                                    msg = "Not connect.\n Verify Network.";
                                } else if (jqXHR.status == 404) {
                                    msg = "Requested page not found. [404]";
                                } else if (jqXHR.status == 500) {
                                    msg = "Internal Server Error [500].";
                                } else if (exception === "parsererror") {
                                    msg = "Requested JSON parse failed.";
                                } else if (exception === "timeout") {
                                    msg = "Time out error.";
                                } else if (exception === "abort") {
                                    msg = "Ajax request aborted.";
                                } else {
                                    msg = "Uncaught Error.\n";
                                }
                                CaptureError(msg + jqXHR.responseText);
                            }
                        });
                    }

                };
                InitTableFeature(val, tableName, dtProperty, allinputs, keyvalue, false);

            }
        }
        );
    } catch (e) {
        console.error(e);
    }

}
function onInitCompleted(settings, json) {
    //var allbulkinput = $(".bulkupdate");
    //jQuery.each(allbulkinput, function (i, val) {
    //    $(val).on("change", function () {
    //        $(val).data("ischanged", true);
    //        $(val).closest("th").addClass("border border-success");
    //    });
       
    //});  
    InitTableControl();
}
function HandlerDataCallback(fnCallback, data, api) {
    jQuery.when(fnCallback(data.Obj["aaData"])).then(function () {

        for (var x in data.Obj["Footer"]) {
            var column = api.column(x);
            $(column.footer()).html(data.Obj.Footer[x]);
        }

    }).then(function () {
        InitLookupValue();
    }).then(function () {
       // ReAdjust();
    });

}
function PupulateFilter(allinputs, searchterm) {
    if (searchterm == null || !searchterm) {
        return;
    }
    allinputs.each(function (x) {
        var element = allinputs[x];
        if (($(element).hasClass("select2-multiple") || $(element).hasClass("js-data-ajax-multiple"))) {
            if (element.name in searchterm) {
                var input = $(element).val();
                $.each(searchterm[element.name], function (x, val) {
                    if ($.inArray(val, input) == -1) {
                        input.push(val);
                    }
                });
                $(element).val(input);
            }
        }
        else
        {
            if (element.name in searchterm) {
                var input = $(element).val();
                $(element).val(searchterm[element.name]);       
               
            }
        }
        //else {
        //    if (!searchdata[element.name]) {
        //        searchdata[element.name] = [];
        //    }
        //    searchdata[element.name].push($(element).val());
        //}
    });


}
function GetSearchParam(tableid) {

}
function PopulateServerParam(aoData, tableName, allinputs, keyvalue, searchterm,sort, trash) {
    aoData.push({ "name": "iTableName", "value": tableName });
    aoData.push({ "name": "searchterm", "value": JSON.stringify(searchterm) });
    var searchdata = {};

    allinputs.each(function (x) {
        var element = allinputs[x];

        if (($(element).val() != "" && $(element).val() != null) || $(element).hasClass("datetime_picker")) {
            if (($(element).hasClass("select2-multiple") || $(element).hasClass("js-data-ajax-multiple"))) {                
                searchdata[element.name] = $(element).val();
            }
            else if ($(element).hasClass("datetime_picker")) {
                if (!searchdata[element.name]) {
                    searchdata[element.name] = [];
                }
                searchdata[element.name].push($(element).val());
            }
            else {
                if (!searchdata[element.name]) {
                    searchdata[element.name] = [];
                }
                searchdata[element.name].push($(element).val());
            }
        }
    });

    for (var name in searchdata) {
        aoData.push({ "name": name, "value": JSON.stringify(searchdata[name]) });
    }
   


    for (var x in keyvalue) {

        aoData.push({ "name": x, "value": keyvalue[x] });
        if (sort !=null && keyvalue[x] in sort) {
            aoData.push({ "name": "sort" + x, "value": keyvalue[x] });
            aoData.push({ "name": "sortdir" + x, "value": sort[keyvalue[x]] });
        }
   
    }

    
    aoData.push({ "name": "iTrash", "value": trash });

}
function InitTableFeature(val, tableName, dtProperty, allinputs, keyvalue, offline) {
    try {
        dtProperty = $.extend({ select: true }, dtProperty);
        var datatable = $(val).DataTable(dtProperty);

        var subtables = $(val).data("subtable");

        datatable.on('click', "button[name='" + tableName.replace(/\\/g, "") + "']", function () {

            var tr = $(this).closest('tr');
            var row = datatable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $(this).children("svg")[0].classList.remove('fa-minus-circle');
                $(this).children("svg")[0].classList.add('fa-plus-circle');
                //$(this).children("svg")[0].removeClass('fas fa-minus-circle red');
                //$(this).children("svg")[0].addClass('fas fa-plus-circle blue');

            }
            else {
                var id = $(this).closest("tr").attr("id");
                var url = $(this).closest("table").data("urlsubtable");

                var param = { "tableName": tableName, "parentId": id, "tabDic": subtables }
                var data = AjaxRequest(url, param);
                // Open this row
                row.child(data.Obj).show();
                tr.addClass('shown');
                $(this).children("svg")[0].classList.add('fa-minus-circle');
                $(this).children("svg")[0].classList.remove('fa-plus-circle');
                //$(this).children("svg")[0].removeClass('fas fa-plus blue');
                //$(this).children("svg")[0].addClass('fas fa-minus red');
            }
        });


        datatable.on("column-reorder", function (e, settings, details) {

            var temp = keyvalue[details.from];
            keyvalue[details.from] = keyvalue[details.to];
            keyvalue[details.to] = temp;
            InitTableControl();


        });

        if (offline) {
            datatable.columns().every(function () {
                var that = this;
                if (that[0][0] in keyvalue) {
                    allinputs.each(function (index) {
                        if (allinputs[index].name == keyvalue[that[0][0]]) {
                            $(allinputs[index]).on("change", function () {
                                if (that.search() !== this.value) {
                                    that.search(this.value).draw();
                                }
                            });
                        }

                    });
                }

            });
        }
        else {
            allinputs.on("change", function () {
                datatable.draw();
            });
            //datatable.draw();
        }
    }
    catch (e) {

    }

  
 
}
function InitTableControl() {
 
    InitSelect2();
    InitSummerNote();
    InitColorPicker();
    InitCodeEditor();
    InitDateTimePicker();
    InitMask();
    InitFileUpload();
    InitICheck();
    InitReadOnly();
    InitSwtich();   
    InitSourceEditor();
    InitSummerNote();
    adjustdatatable();
    InitOnChangedEvent();
    InitKeyValue();
    InitQR();
    
}

var calcDataTableHeight = function () {
    return $(window).height() * 70 / 100;
};
function ReAdjust()
{
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
}
$(window).resize(function () {
    ReAdjust();
});
function adjustdatatable() {
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust().draw();
}
function Redraw() {
    setTimeout(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();

    }, 1000);
}
function onchecklistchange(element, data) { 
    $(element).closest("th").addClass("border border-primary");   
    $(element).closest('.bulkupdate').data("ischanged", true);
    $(element).closest("th").find(".cancelchange").show();   
}
function onchangebulkEditField(element, data) {    
    $(element).data("ischanged", true);
    $(element).closest("th").addClass("border border-primary");
    $(element).closest("th").find(".cancelchange").show();   
}
function CancelBulkChange(element) {
    $(element).closest("th").find(".cancelchange").hide();   
    $(element).closest("th").find(".bulkupdate").data("ischanged", false);
    $(element).closest("th").removeClass("border border-primary");
}
function SaveTableRecord(e) {
    var formsvalue = {};
    var formelements = $(".bulkupdate");
    var tableName = $(e).data("tablename");
    var searchterm = $(e).data("searchterm");
    var searchdata = {};
    var allinputs = $(".fieldsearch");    
    allinputs.each(function (x) {
        var element = allinputs[x];
        if (($(element).val() != "" && $(element).val() != null) || $(element).hasClass("datetime_picker")) {
            if (($(element).hasClass("select2-multiple") || $(element).hasClass("js-data-ajax-multiple"))) {
                searchdata[element.name] = $(element).val();
            }
            else if ($(element).hasClass("datetime_picker")) {
                if (!searchdata[element.name]) {
                    searchdata[element.name] = [];
                }
                searchdata[element.name].push($(element).val());
            }
            else {
                if (!searchdata[element.name]) {
                    searchdata[element.name] = [];
                }
                searchdata[element.name].push($(element).val());
            }
        }
    });
    $.each(searchterm, function (key, val) {

        if (!(key in searchdata)) {
            searchdata[key] = searchterm[key];
        }
    });

    //console.log(searchdata);
    jQuery.each(formelements, function (i, val) {
        if (!$(formelements[i]).data("ischanged")) {
            return;
        }
        var name = $(formelements[i]).attr("name");
        var element = val;
        if ($(element).hasClass("md-checkbox-list")) {
            var checkelements = $(element).find(".md-checkbox");    
            var checklist = $(checkelements).find("input[name='" + name  + "']")
            var checked = [];
            jQuery.each(checklist, function (i, valcheck) {
                if (valcheck.checked) {
                    checked.push(valcheck.value);
                }
            });
            formsvalue[name] = checked;

        }
        else if ($(element).hasClass("ikeyvalue")) {
            formsvalue[name] = $(element).val();
        }
        else if ($(element).is(":checkbox")) {
            formsvalue[name] = element.checked;

        }
        else if ($(element).hasClass("Summernote")) {
            formsvalue[name] = $(element).summernote("code");
        }
        else if ($(element).hasClass("sortablelist")) {

            var listitem = $(element).find("li.liitem");
            var selectedid = [];
            $.each(listitem, function (index, value) {
                selectedid.push($(value).data("key"));
            });
            formsvalue[name] = selectedid;
        }
        else if ($(element).hasClass("select2-multiple") || $(element).hasClass("ajax-sortable") || $(element).hasClass("sortable") || $(element).hasClass("js-data-ajax-multiple")) {
            formsvalue[name] = $(element).val();
        }
        else if ($(element).hasClass("select2") || $(element).hasClass("js-data-ajax") || $(element).hasClass("js-data-ajax")) {
            formsvalue[name] = [$(element).val()];
        }
        else if ($(element).hasClass("ICheckRadio")) {
            formsvalue[name] = $(element).find("input[name=" + name + "]:checked").val();
        }
        else if ($(element).hasClass("Mask")) {
            formsvalue[name] = $(element).cleanVal();
        }
        else if ($(element).hasClass("date")) {
            formsvalue[name] = $(element).find("input").val();
        }
        else if ($(element).hasClass("codeEditor")) {
            var editor = ace.edit(element);
            formsvalue[name] = editor.getValue();
        }
        else if (element.type == "file") {
            try {

                var uploadid = element.id;
                var images = $("#Uploaded_" + uploadid).find(".uploadfile");

                var filesuploaded = [];
                jQuery.each(images, function (i, val) {
                    if ($(val).data("path")) {
                        filesuploaded.push({ Data: $(val).data("path"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                    }
                    else {
                        if ($(val).attr("src")) {
                            filesuploaded.push({ Data: $(val).attr("src"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                        }
                        else {
                            filesuploaded.push({ Data: $(val).data("data"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                        }

                    }

                });
                formsvalue[name] = JSON.stringify(filesuploaded);
                jQuery.each(element.files, function (i, val) {
                    if (val.type.indexOf("image") >= 0) {
                        // console.log(val);
                        //Save Quality
                        canvasResize(val, {
                            width: 1080,
                            height: 0,
                            crop: false,
                            quality: 80,
                            //rotate: 90,
                            callback: function (imagebyte, width, height) {
                                filesuploaded.push({ Data: imagebyte, FileName: val.name, FileSize: val.size, FileType: val.type });
                                formsvalue[name] = JSON.stringify(filesuploaded);
                                $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><img  class='uploadfile' style='width:100px;' src='" + imagebyte + "' data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  /><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                                if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                                    DisplayLast("Uploaded_" + uploadid);
                                }

                            }
                        });
                    }
                    else {
                        var reader = new FileReader();
                        reader.onloadend = function () {
                            filesuploaded.push({ Data: reader.result, FileName: val.name, FileSize: val.size, FileType: val.type });
                            formsvalue[name] = JSON.stringify(filesuploaded);
                            $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><p data-data='" + reader.result + "' class='uploadfile' style='width:100px;'  data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  />" + val.name + " </p><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                            if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                                DisplayLast("Uploaded_" + uploadid);
                            }
                        }
                        reader.readAsDataURL(val);

                    }

                });

            }
            catch (ex) {
                ShowModal(ex);
            }


            //console.log(element.files);
            //var file = val.files[0];

        }
        else if ($(element).hasClass("sourceEditor")) {
            formsvalue[name] = $(element).val();
        }
        else {
            formsvalue[name] = $(element).val();
        }
    });
    //console.log(formsvalue);

    ShowBusy();
    try {
        // signarRServer.server.signalChanged(userName, updateRecord);

        var data = { "tableName": tableName, "searchterm": searchdata, "updateRecord": formsvalue };
        $.ajax({
            "dataType": "json",
            "contentType": "application/json",
            "type": "POST",
            "url": "/Data/UpdateOnly",
            "data": JSON.stringify(data),
            "async": true,
            "success":
                function (data) {
                    ShowReady();
                    switch (data.Code) {
                        case "Success":
                            // $("#" + id + " .status").html("<span style='color:red;'>Record Saved</span>");

                            bootbox.alert(data.Obj);
                            ClearArray(formChangeRecord);
                            delete formChangeRecord;

                            break;
                        case "Fail":
                            alert(data.Message);
                            break;
                        case "SessionExpired":
                            window.location.href = data.Obj;
                            break;
                    }

                },
            "error": function (jqXHR, exceptionC) {

                var msg = "";
                if (jqXHR.status === 0) {
                    msg = "Not connect.\n Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg = "Time out error.";
                } else if (exception === "abort") {
                    msg = "Ajax request aborted.";
                } else {
                    msg = "Uncaught Error.\n";
                }
                CaptureError(msg + " " + jqXHR.responseText);

            }
        });



    }
    catch (err) {
        CaptureError(err);
        ShowReady();
    }
  
}