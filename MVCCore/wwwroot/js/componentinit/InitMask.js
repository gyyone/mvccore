﻿//http://igorescobar.github.io/jQuery-Mask-Plugin/

function addThousandSeparator(s) {
    return s.replace(/(\d)(?:(?=\d+(?=[^\d.]))(?=(?:\d{3})+\b)|(?=\d+(?=\.))(?=(?:\d{3})+(?=\.)))/gm, '$1,');
}

function InitMask() {

    var element = $(".Mask");
    jQuery.each(element, function (index, val) {

        //var custom_options = {
        //    byPassKeys: [8, 9, 37, 38, 39, 40],
        //    translation: {
        //        '0': { pattern: /\d/ },
        //        '9': { pattern: /\d/, optional: true },
        //        '#': { pattern: "/[a-z]/", recursive: true },
        //        'A': { pattern: /[a-zA-Z0-9]/ },
        //        'S': { pattern: /[a-zA-Z]/ }
        //    }
        //};
        //$(val).mask("###", custom_options);
        try {
            var option = ParseToObj($(val).data("options"));
            if (!option || !option.init) {
                option = GetDefaultMaskOption();
            }
            option.init(val);
        }
        catch (e) {
            console.error(e);
        }

    });
}
function GetDefaultMaskOption() {
    return {
        init: function (element) {
            /*Do not remove these comment:InitMask*/
            var obj =
            {
                "Pattern": "###",
                "translation": {
                    "0": {
                        "pattern": /\d/
                    },
                    "9": {
                        "pattern": /\d/,
                        "optional": true
                    },
                    "#": {
                        "pattern": /[a-zA-Z0-9]/,
                        "recursive": true
                    },
                    "A": {
                        "pattern": /[a-zA-Z0-9]/
                    },
                    "S": {
                        "pattern": /[a-zA-Z]/
                    }
                }
            };
            var readonly = parseBool($(element).data("readonly"));
            if (obj.maxlength) {
                $(element).attr("maxlength", obj.maxlength);
            }
            $(element).mask(obj.Pattern, obj);
            if (readonly) {
                $(element).attr("readonly", true);
            }
        }
    };
}