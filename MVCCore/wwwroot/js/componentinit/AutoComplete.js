﻿function InitAutoComplete() {
    $(document).ready(function () {
        $(".autocomplete").each(function (index) {
            var table = $(this).data("table");
            var field = $(this).data("field");
           $(this).autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "/Data/AutoComplete",
                        type: "Post",
                        data: { "search": request.term, "table": table, "field": field, "rows":10 },
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    value: item,
                                    label: item
                                }
                            }));
                          
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
      
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        });
       
    });
}


