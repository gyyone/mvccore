$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    icons: {
        time: 'far fa-clock',
        date: 'far fa-calendar-alt',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'far fa-calendar-check',
        clear: 'far fa-trash-alt',
        close: 'fas fa-times'
    }

});

function InitDateTimePicker()
{
            //need convert Microsoft Datetime format to Smallot Datetime Format
/*
 malot format
 String. Default: 'mm/dd/yyyy'

The date format, combination of p, P, h, hh, i, ii, s, ss, d, dd, m, mm, M, MM, yy, yyyy.
p : meridian in lower case ('am' or 'pm') - according to locale file
P : meridian in upper case ('AM' or 'PM') - according to locale file
s : seconds without leading zeros
ss : seconds, 2 digits with leading zeros
i : minutes without leading zeros
ii : minutes, 2 digits with leading zeros
h : hour without leading zeros - 24-hour format
hh : hour, 2 digits with leading zeros - 24-hour format
H : hour without leading zeros - 12-hour format
HH : hour, 2 digits with leading zeros - 12-hour format
d : day of the month without leading zeros
dd : day of the month, 2 digits with leading zeros
m : numeric representation of month without leading zeros
mm : numeric representation of the month, 2 digits with leading zeros
M : short textual representation of a month, three letters
MM : full textual representation of a month, such as January or March
yy : two digit representation of a year
yyyy : full numeric representation of a year, 4 digits
 *
 * */
/*
 * https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings
 */
    var element = $(".datetime_picker");
    jQuery.each(element, function (i, val) {
        try {
            var option = ParseToObj($(val).data("options"));
            if (!option || !option.init) {
                option = GetDefaultDateTimeOption();
            }

            option.format = option.format.replace("tt", "a");
            //Not Support
            option.format = option.format.replace(/t/g, "");
            option.format = option.format.replace("TT", "A");
            option.format = option.format.replace(/T/g, "");
            option.format = option.format.replace(/y/g, "Y");
            option.format = option.format.replace(/d/g, "D");
            $(element).datetimepicker(option);  
        }
        catch (e) {
            console.error(e);
        }
      
    });
 

}
function GetDefaultDateTimeOption() {

    return {
        /*Do not remove these comment:InitDateTime*/
        "format": "yyyy-MMM-dd hh:mm:ss tt",
        "viewMode": "days",
        "showTodayButton": true,
        "buttons": {
            "toolbarPlacement": "bottom",
            "showClose": true
        },
        "todayBtn": true    
    };
}