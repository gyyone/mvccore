﻿

function CreateIcon(classname, style) {
    var icon = document.createElement("i")
    icon.className = classname;
    icon.style = style;
    return icon;
}

function CreateDiv(classname) {
    var div = document.createElement("div");
    div.className = classname;
    return div;
}

function CreateDivChild(classname, child) {
    var div = document.createElement("div");
    div.className = classname;
    if (child) {
        div.appendChild(child);
    }
    return div;
}
function CreateDivText(classname, text) {
    var div = document.createElement("div");
    div.className = classname;
    $(div).html(text);
    return div;
}
function CreateRadio(groupname, value, text, checked) {
    var div = document.createElement("div");
    div.className = "radio-inline ICheckRadio col-md-4";
    var e = document.createElement("input");
    e.className = "icheckbox_flat-blue";
    e.name = groupname;  
    e.value = value;
    e.type = "radio";
    e.checked = checked;
    var lbl = document.createElement("label");
    var span = document.createElement("span");
    span.appendChild(document.createTextNode(text));
    span.className = "p-2";
    lbl.appendChild(span);
    lbl.appendChild(e);
    div.appendChild(lbl);

    return div;
}
function CreateCheckBox(inputref, text, checked) {
    var div = document.createElement("div");
    div.className = "radio-inline col-md-4";
    inputref.className = "";  
    inputref.type = "checkbox";
    inputref.checked = checked;
    var lbl = document.createElement("label");
    var span = document.createElement("span");
    span.appendChild(document.createTextNode(text));
    span.className = "p-2";
    lbl.appendChild(span);
    lbl.appendChild(inputref);
    div.appendChild(lbl);
    $(inputref).iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue',
    });
    return div;
}



