﻿(function ($) {
      

    $.fn.sortableselect2 = function (methodOrOptions) {

        var selectedValue = [];
        var methods = {
            init: function (options) {
                if ($(this).data("sortableselect2")) {
                    return this;
                }
                else {
                    $(this).data("sortableselect2", true);
                }
                // This is the easiest way to have default options.
                var settings = $.extend({
                }, options);
                var this_e = this;
                settings.SelectedKeyValue = settings.SelectedKeyValue || [];
                var select2e = $(this_e).find("select")[0];
                select2e.className = "form-control ";
                var div = this_e;
                $(select2e).select2(settings);
                var ul = document.createElement("ul");
                div.append(ul);
                ul.className = "list-group";
               
                for (var x in settings.SelectedKeyValue) {
                    if (settings.SelectedKeyValue[x].id != "" && settings.SelectedKeyValue[x].id != null) {
                        AddLi(settings.SelectedKeyValue[x]);
                    }
                 
                }
                SortableUl();
                $(this_e).val(selectedValue);
                $(select2e).on("change", function () {
                    if ($(select2e).val() != null && $(select2e).val() != "" && $(select2e).val() != []) {
                        var data = $(select2e).select2("data");
                        for (var x in data) {
                            AddLi(data[x]);
                        }
                        SortableUl();

                        $(select2e).val(null).trigger('change');


                    }

                });
                if (options.ReadOnly) {
                    $(select2e).prop("disabled", true);                   
                }
                function SortableUl() {
                    $(ul).sortable({
                        start: function (event, ui) {
                            var start_pos = ui.item.index();
                            ui.item.data('start_pos', start_pos);
                        },
                        change: function (event, ui) {

                        },
                        update: function (event, ui) {
                            var valueselected = $(this_e).val();
                           
                            var start_pos = ui.item.data('start_pos');
                            var valuemove = valueselected[start_pos];
                            var index = ui.item.index();
                            //console.log(start_pos + "->" + index);
                            valueselected.splice(start_pos, 1);
                            //console.log(selectedValue);
                            valueselected.splice(index, 0, valuemove);
                            //console.log(valueselected);
                            $(this_e).val(valueselected);
                            $(this_e).trigger("change");
                        }
                    });
                }
                function AddLi(data) {
                    var li = document.createElement("li");
                    var div = document.createElement("div");
                    div.style = "cursor:move;";
                    li.className = "list-group-item d-flex justify-content-between align-items-center p-1";
                    var divremove = CreateDivChild("badge badge-pill", CreateIcon("fas fa-trash-alt fa-lg", "color:Crimson;cursor:pointer;"));
                    selectedValue = $(this_e).val() == "" ? [] : $(this_e).val() ;
                    selectedValue.push(data.id);
                    $(divremove).click({}, function (event) {
                        var thisRemove = this;
                        var index = $(thisRemove.parentElement).index();
                        selectedValue = $(this_e).val() == "" ? [] : $(this_e).val();
                        selectedValue.splice(index, 1);
                        $(thisRemove).closest("li").remove();
                        //console.log(selectedValue);
                        $(this_e).val(selectedValue);
                        $(this_e).trigger("change");
                    });

                    var text = document.createTextNode(data.text);
                    div.appendChild(text);
                    li.appendChild(div);
                    li.appendChild(divremove);
                    ul.appendChild(li);
                    //console.log(selectedValue);
                    $(this_e).val(selectedValue);
                }

            },
            append: function (inputvalue) {
                var this_e = this;
                var ul = $(this_e).find("ul")[0];
                $(ul).empty();
               
                selectedValue = [];
                for (var x in inputvalue) {
                    AddLi(inputvalue[x]);
                }
                
                function AddLi(data) {
                    var li = document.createElement("li");
                    var div = document.createElement("div");
                    div.style = "cursor:move;";
                    li.className = "list-group-item d-flex justify-content-between align-items-center p-1";
                    var divremove = CreateDivChild("badge badge-pill", CreateIcon("fas fa-trash-alt fa-lg", "color:Crimson;cursor:pointer;"));
        
                    selectedValue.push(data.id);
                    $(divremove).click({}, function (event) {
                        var thisRemove = this;
                        var index = $(thisRemove.parentElement).index();
                        selectedValue = $(this_e).val() == "" ? [] : $(this_e).val();
                        selectedValue.splice(index, 1);
                        $(thisRemove).closest("li").remove();
                        //console.log(selectedValue);
                        $(this_e).val(selectedValue);
                        $(this_e).trigger("change");
                    });

                    var text = document.createTextNode(data.text);
                    div.appendChild(text);
                    li.appendChild(div);
                    li.appendChild(divremove);
                    ul.appendChild(li);
                    //console.log(selectedValue);
                    $(this_e).val(selectedValue);
                }
            },
            clear: function (content) {
                $(this).find("ul").empty();             
              
            }
        };

        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            // Default to "init"
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + options + ' does not exist on jQuery.sortableselect2');
        }    

    

    };

}(jQuery));