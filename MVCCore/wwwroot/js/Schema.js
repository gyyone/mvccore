﻿function InitSchema() { 
    var url = UrlAction("GetSchema", "Schema");
    var urlForm = UrlAction("GetHtmlForm", "Control");
    var updateUrl = UrlAction("InsertOrUpdate","Data");
    var deleteUrl = UrlAction("Delete", "Data");
    var urlsubtable = UrlAction("GetHtmlTabTable", "Data");
    DeletedNode = {};
    UpdatedNode = {};

    $("#schema").on('state_ready.jstree', function () {

        $("#schema").on('changed.jstree', function (e, data) {
            if (data && data.event && data.event.button == 0) {
                var obj = AjaxRequest(urlForm, { "tableName": data.node.original.NodeObject.TableId, "guid": data.node.original.NodeObject.id, parentId: data.node.original.NodeObject.ParentId, "isNew": false, "enableSubTable": false, "updateUrl": updateUrl, "deleteUrl": deleteUrl, "subtableUrl": urlsubtable })
                $("#ControlInput").html(obj.Obj);
                InitDependancy();
                InitControl();             
            }
            //console.log(e);
            //console.log(data);

        });

    }).jstree({
        "core": {
            "themes": {
                "responsive": true
            },
            "data": {
                "url": url,
                "type":"post",
                "data": function (node) {
                    return { "ParentId": node.id };
                }
            },
            "check_callback": function (operation, node, parent, position, more) {
                if (operation == "move_node") {
       
                    if (node.original.NodeObject.ParentId == parent.id) {
                        console.log(node.text + " Move to " + position);
                        //console.log(operation);
                        //console.log(node);
                        //console.log(parent);
                        return true;
                    }
                }
                return false;
              
                
            }
        },
        "search": {            
            "case_insensitive": true,
            "show_only_matches": true,
            "show_only_matches_children": true,
            ajax: {
                "url": url,
                "type": "post",
                "data": function (str) {
                    return { "str": str };

                }        
            }
        }, 
        "plugins": ["search","state", "contextmenu", "unique"],
        contextmenu: { items: ContextMenuSchema }
    });
    var searching = false;
    $('#datasearch').keyup(function () {

        if (!searching) {
            searching = true;
            setTimeout(function () {
                searching = false;
                var v = $('#datasearch').val();
                $("#schema").jstree(true).search(v);         
   
            }, 1000);
        }

    });

}
var copyNode;
function ContextMenuSchema(selectednode) {
    console.log(node);
    var items = {};
    var node = selectednode.original;
       items["reorder"] =
            {
            "label": GetWords("Reorder"),
            "action": function () {
                var request = AjaxRequest(UrlAction("Reorder", "Schema"), { "tableId": node.NodeObject.TableId, "parentId": node.NodeObject.ParentId }, function (data) { bootbox.alert(data.Message); });
                    if (request.Code == "Success") {                        
                        ShowModal("Sort View", request.Obj);                     
                        $("#SortView" ).sortable({
                            start: function (event, ui) {
                                var start_pos = ui.item.index();
                                ui.item.data('start_pos', start_pos);
                            },
                            change: function (event, ui) {

                            },
                            update: function (event, ui) {
             
                            }
                        });
                  
                       }              

                    }
                
        };
        items["Copy"] =
        {
            "label": GetWords("Copy"),
            "action": function () {
                copyNode = node;
            }

        };

    if (copyNode) {
        items["Paste"] =
            {
                "label": GetWords("Paste"),
                "action": function () {
                    if (copyNode.type == "tablename") {

                        AjaxRequestAsync(UrlAction("CopyTable", "Schema"), { "targetCopyId": copyNode.id, "targetDestId": node.id },
                            function (data, nodeobject) {
                                //Success
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                                    //$("#schema").jstree(true).search($('#datasearch').val());
                                    //$("#" + nodeobject.id).knob();

                            },
                            node.NodeObject, function (request) {
                                // Error Call Back Here
                                bootbox.alert(request.Message);  
                            });
                    }
                    else if (copyNode.type == "fieldname") {

                        AjaxRequestAsync(UrlAction("CopyField", "Schema"), { "targetCopyFieldId": copyNode.id, "targetDestId": node.id },
                            function (data, nodeobject) {
                                //success
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });                        

                            },
                            node.NodeObject, function (request) {
                                // Error Call Back Here
                                bootbox.alert(request.Message);    
                            });
                    }

                }

            };
    }

    if (node.type == "tablename") {
        if (node.NodeObject.IsLock == "true") {
            items["UnlockTable"] =
            {
                "label": GetWords("Unlock Table"),
                "action": function () {
                    var request = AjaxRequest(UrlAction("RequestLock", "Schema"), { "tableId": node.id , "islocked": false }, function (data) { bootbox.alert(data.Message); });
                    if (request.Code == "Success") {
                        $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                    }
                }
            };
        }
        else {
            items["newtable"] =
            {
                "label": GetWords("Add Table"),
                "action": function () {

                    AjaxRequestAsync(UrlAction("AddTable", "Schema"), node.NodeObject,
                        function (data, nodeobject) {

                            if (data.Code == "Success") {
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                                //$("#schema").jstree(true).search($('#datasearch').val());
                                //$("#" + nodeobject.id).knob();
                            }
                        },
                        node.NodeObject, function (request) {
                            // Error Call Back Here
                            bootbox.alert(request.Message);
                        });

                }
            };

            items["Locktable"] =
            {
                "label": GetWords("Lock Table"),
                "action": function () {
                    bootbox.confirm(GetWords("LockTableWarning."), function (result) {
                        if (result) {
                            var request = AjaxRequest(UrlAction("RequestLock", "Schema"), { "tableId": node.id , "islocked": true }, function (data) { bootbox.alert(data.Message); });
                            if (request.Code == "Success") {
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                            }

                        }

                    });
                }
            };  

            items["deletetable"] =
            {
                "label": GetWords("Delete Table"),
                "action": function () {
                    bootbox.confirm(GetWords("Do you want to delete data?"), function (result) {
                        if (result) {
                            var request = AjaxRequest(UrlAction("DeleteTable", "Schema"), { "selectedTable": node.NodeObject, "deletedata": result }, function (data) { bootbox.alert(data.Message); });
                            if (request.Code == "Success") {
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                            }

                        }

                    });
                }
            };

            items["cleardata"] =
            {
                "label": GetWords("Clear Data"),
                "action": function () {
                    bootbox.confirm(GetWords("Do you want to clear all data?"), function (result) {
                        if (result) {
                            var request = AjaxRequest(UrlAction("ClearData", "Schema"), { "tableid": node.id }, function (data) { bootbox.alert(data.Message); });
                            if (request.Code == "Success") {
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                            }
                        }

                    });
                }
            };

           
        }
       

    }
    if (node.type == "fieldname") {
        if (node.NodeObject.IsLock == "false") {
            items["createfield"] =
            {
                "label": GetWords("Add Field"),
                "action": function () {
                    var request = AjaxRequest(UrlAction("AddTableField", "Schema"), node.NodeObject, function (data) { bootbox.alert(data.Message); });
                    if (request.Code == "Success") {
                        $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                        //$("#schema").jstree(true).search($('#datasearch').val());
                    }

                }
            };

            items["deletefield"] =
            {
                "label": GetWords("Delete Field"),
                "action": function () {
                    bootbox.confirm("Do you want to delete data?", function (result) {
                        if (result) {
                            var request = AjaxRequest(UrlAction("DeleteField", "Schema"), { "selectedfield": node.NodeObject, "deletedata": result }, function (data) { bootbox.alert(data.Message); });
                            if (request.Code == "Success") {
                                $('#schema').jstree(true).refresh({ skip_loading: false, forget_state: false });
                            }
                        }

                    });
                }
            };
        }

    }


    return items;
}

var toastDic = {};
function ShowStatus(id, total, current, message )
{
    if (id in toastDic) {
        toastDic[id].update({
            heading: "Processing Request",
            text: "<div class='progress'><div class='progress-bar progress-bar-striped text-dark' role='progressbar' style='width:" + Math.round(current / total * 100).toString() + "%' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100'>" + message + " " + current + " / " + total+ "</div></div>",
            hideAfter: false,
            icon: 'info'
        });
    }
    else {
        toastDic[id] = $.toast({
            heading: "Processing Request",
            text: "<div class='progress'><div class='progress-bar progress-bar-striped text-dark' role='progressbar' style='width:" + Math.round(current / total * 100).toString() + "%' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100'>" + message + " " + current + " / " + total + "</div></div>",
            hideAfter: false,
            icon: 'info',
            position: { right: 10, top: 40 }
        });
    }

}
function RemoveStatus(id) {
    toastDic[id].reset();
}
document.addEventListener('readystatechange', function (evt) {
    switch (evt.target.readyState) {
        case "loading":
            // The document is still loading.
            break;
        case "interactive":
            // The document has finished loading. We can now access the DOM elements.
            break;
        case "complete":
            {
                var connection = new signalR.HubConnectionBuilder().withUrl("/serverHub").build();

  
                connection.on("UpdateProgressStatus", function (id, currentCount, Total, message) {
                    ShowStatus(id, Total, currentCount, message);
                    if (Total == currentCount) {
                        setTimeout(function () { RemoveStatus(id); }, 3000);                    
                    }
                });
                connection.start()
                    .then(function () {
                        connection.invoke("RegisterHub", "");
                    })
                    .catch(function (err) {
                        return console.error(err.toString());
                    });
            }
            break;
    }
}, false);





