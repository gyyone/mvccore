﻿

var IsMobile = false;
(function () {
    //var ori = HTMLCanvasElement.prototype.cloneNode;
    //HTMLCanvasElement.prototype.cloneNode = function () {
    //    var copy = ori.apply(this, arguments);
    //    // don't do it for web-gl canvas
    //    if (this.getContext('2d')) {
    //        copy.getContext('2d').drawImage(this, 0, 0);
    //    }
    //    return copy;
    //};

    var ori = $.clone;
    $.clone = function () {
        var oricanvas = $(this).find("canvas");
        var copy = ori.apply(this, arguments);      
        var clonecanvas = $(copy).find("canvas");
        var canvasori = [];
        var canvasclone = [];
        $.each(oricanvas, function (x, val) {
            canvasori.push(val);
        });
        $.each(clonecanvas, function (x, val) {
            canvasclone.push(val);
        });
        if (canvasclone.length == canvasori.length) {
            for (var x = 0; x < canvasclone.length; x++) {
                if (canvasori[x].getContext('2d')) {
                    canvasclone[x].getContext('2d').drawImage(canvasori[x], 0, 0);
                }
            }
        }
       

        return copy;
    };
})();


(function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) IsMobile = true; })(navigator.userAgent || navigator.vendor || window.opera);

var userConfig;
var CustomPage = false;
var urlList;
var ctrlKeyPressed;
var signarRServer;
var formChangeRecord = {};
var disableChangeEvent = false;



function ParseToObj(obj) {
    var tempobj;
    if (typeof (obj) == "string") {
        try {
            eval("tempobj = " + obj);
            return (tempobj);
        } catch (e) {
            return null;
        }
    
    }
    return obj;
}
$(document).keydown(function (event) {
    if (event.which == "17") {
        ctrlKeyPressed = true;
    }

});

function SaveTableView(urlUpdate,tableId) {
 
    var elements = $("[name='TableView_" + tableId + "']");
    var fieldorder = {};
    $.each(elements, function (i, val) {        
        var rowid = $(val).closest("tr").data("id");
        fieldorder[rowid] = val.checked ;       
    });

    var result = AjaxRequest(urlUpdate, { "tableId": tableId, "orderFields": fieldorder });
    switch (result.Code) {
        case "Success":
            // $("#" + id + " .status").html("<span style='color:red;'>Record Saved</span>");

            bootbox.alert("Saved Successfull");
            ClearArray(formChangeRecord);
            delete formChangeRecord;

            break;
        case "Fail":
            alert(result.Message);
            break;
        case "SessionExpired":
            window.location.href = data.Obj;
            break;
    }
}
$(document).keyup(function () {
    if (event.which == "17") {
        ctrlKeyPressed = false;
    }

});

function InitControl() {
    //Switch box need initialize first before init change

    try {
        InitDataTable();
        InitSwtich();
        $(".Resizable").resizable({
            handles: 'e'
        });
        InitFileUpload();
        InitSelect2();
        InitICheck();
        InitSummerNote();
        InitColorPicker();
        InitCodeEditor();
        InitDateTimePicker();
        InitMask();

        InitAutoComplete();

        //// general knob
        $(".knob").knob({
            'dynamicDraw': true,
            'thickness': 0.2,
            'tickColorizeValues': true,
            'skin': 'tron'
        });
        InitKeyValue();
        InitSourceEditor();
        InitDependancy();
        InitLookupValue();
        InitOnChangedEvent();
        InitQR();
   

    } catch (e) {
        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    }

  
}
function InitLookupValue() {
    try {
        var lookupsources = $(".lookupsource");
        var lookupdic = {};
        $.each(lookupsources, function (index, e) {
            var fieldid = $(e).data("fieldid");
            var iskeySource = $(e).data("iskeysource");

            var value = $(e).data("value");
            if (!(fieldid in lookupdic)) {
                lookupdic[fieldid] = {};
                lookupdic[fieldid][true] = { "value": [], "element": [] };
                lookupdic[fieldid][false] = { "value": [], "element": [] };
            }
            if (!lookupdic[fieldid][iskeySource]["value"].includes(value)) {
                lookupdic[fieldid][iskeySource]["value"].push(value);
            }
            lookupdic[fieldid][iskeySource]["element"].push(e);

        });
        $.each(Object.keys(lookupdic), function (i, key) {
            $.each(Object.keys(lookupdic[key]), function (j, isKeyvalue) {

                AjaxRequestAsync("/Data/GetSourceText", {
                    "fieldId": key,
                    "KeySource": isKeyvalue,
                    "idlist": lookupdic[key][isKeyvalue]["value"]
                }, OnLookupText, {
                    "fieldid": key,
                    "iskeySource": isKeyvalue,
                    "value": lookupdic[key][isKeyvalue]["value"],
                    "element": lookupdic[key][isKeyvalue]["element"]
                });
            });

        });


        var apisource = $(".apisource");
        var apilookupdic = {};
        $.each(apisource, function (index, e) {
            var fieldid = $(e).data("fieldid");
            var iskeySource = $(e).data("iskeysource");
            var value = $(e).data("value");
            var url = $(e).data("apiurl");
            var dependencyvalue = $(e).data("dependencyvalue");
            if (!(fieldid in apilookupdic)) {
                apilookupdic[fieldid] = {};
                apilookupdic[fieldid][true] = {};
                apilookupdic[fieldid][false] = {};

            }
            //Assume all same value will contain same dependency value
            if (!(value in apilookupdic[fieldid][iskeySource])) {
                apilookupdic[fieldid][iskeySource][value] = {
                    "dependent": dependencyvalue,
                    "Url": url,
                    "element": []
                };

            }
            apilookupdic[fieldid][iskeySource][value]["element"].push(e);

        });
        $.each(Object.keys(apilookupdic), function (i, key) {
            $.each(Object.keys(apilookupdic[key]), function (j, isKeyvalue) {

                $.each(Object.keys(apilookupdic[key][isKeyvalue]), function (k, val) {
                    $.ajax({
                        url: apilookupdic[key][isKeyvalue][val]["Url"],
                        contentType: "application/json",
                        type: "Post",
                        data: JSON.stringify({
                            "fieldId": key,
                            "KeySource": isKeyvalue,
                            "id": val,
                            "dependent": JSON.stringify(apilookupdic[key][isKeyvalue][val]["dependent"]),
                            "searchByKey": true
                        }),
                        async: true,
                        success: function (data) {
                            switch (data.Code) {
                                case "Success":
                                    OnLookupText(data, {
                                        "fieldid": key,
                                        "iskeySource": isKeyvalue,
                                        "value": [val],
                                        "element": apilookupdic[key][isKeyvalue][val]["element"]
                                    });

                                    break;
                                case "Fail":
                                    if (errCallback) {
                                        errCallback(data);
                                    }
                                    else {
                                        CaptureError(data.Message);
                                    }

                                    break;
                                case "SessionExpired":
                                    window.location.href = data.Obj;
                                    break;
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            ShowReady();
                            CaptureError(thrownError + xhr.responseText);
                        }
                    });


                });

            });

        });
    } catch (e) {
        console.error(e);
    }

    
}
function OnLookupText(data, param) {
    var dic = {};
    $.each(data.Obj, function (i, val) {
        dic[val.id] = val.text;
    });

    $.each(param["element"], function (i, e) {
        var value = $(e).data("value");
        if (value in dic) {
            $(e).html(dic[value]);
        }
     
    });

}
function InitDependancy() {
    //return;
    $.each($(".formbody"), function (i, val) {

        var dependency = $(val).data("dependancy");
        var dependencyscript = $(val).data("dependancyscript");
        var formid = $(val).data("formid");
        for (var fieldName in dependency) {

            if (dependency[fieldName] && dependency[fieldName] != null && Object.keys(dependency[fieldName]).length > 0) {
                var element = $(val).find("div[name='" + fieldName + "'], li[name='" + fieldName + "']");
                if (element.length > 0) {

                    var valueselecteddependancy = {};
                    var valselected = "";             

                    for (var targetfield in dependency[fieldName]) {

                        var dependentvalue = GetDependancyValue(formid, targetfield);
                        dependentvalue = dependentvalue.filter(function (element) {

                            return element !== undefined && element != "" && element != null;
                        });
                        for (var i in dependency[fieldName][targetfield]) {
                            if (dependentvalue.length > 0 && dependentvalue.includes(dependency[fieldName][targetfield][i])) {
                                valueselecteddependancy[targetfield] = dependentvalue;
                                break;
                            }

                            if (dependentvalue.length > 0 && dependency[fieldName][targetfield][i] == "All") {

                                valueselecteddependancy[targetfield] = dependentvalue;
                                break;
                            }
                        }
                    }
                    var needshow = true;
                    for (var targetfield in dependency[fieldName]) {
                        if (!(targetfield in valueselecteddependancy)) {
                            needshow = false;
                            break;
                        }
                    }
                    var elementdepent = $(element).data("dependentvalue");
                    if (elementdepent) {
                        var temp = ParseToObj(elementdepent);
                        //console.log("Element Depent :" + elementdepent);
                        for (var i in valueselecteddependancy) {
                            temp[i] = valueselecteddependancy[i];
                        }
                        $(element).data("dependentvalue", JSON.stringify(temp));
                    }
                    else {
                        $(element).data("dependentvalue", JSON.stringify(valueselecteddependancy));
                    }
                
                    if (needshow) {
                        $(element).show();
                        var sourceeditor = $(element).find(".sourceEditor");
                        var name = $(element).attr("name")
                        if (name == "BJrFwJBwDrEHHwErJsIJsuECDsrEIBDEHv") {
                          
                            switch (valueselecteddependancy["BJCIHDIwICBsIGEwAssuFDHvtACJJCDEJC"][0]) {
                                case "BJstHBIIDvCwICEDCrIvrBsDHrGJsrwuwu"://Radio Button
                                case "BJBGtIFvvsusAGEtwJsttJADvHIsAFHBFF"://Combo Box
                                case "BJCtCutvAIEFEFEtHCICBtFGFAFsJuDEHI"://Check List
                                case "BJEEFAEsrwvsrsEIuvIwrswvIFBsvFtFrD"://Sortable Combobox
                                    $(sourceeditor).data("showvaluesource", false);
                                    break;
                                case "BJHttsEAAIAvBJEvCArtIutJsBvwJBwBJB": //KeyValueBox
                                    $(sourceeditor).data("showvaluesource", true);
                                    break;
                            }
                            var value = $(sourceeditor).val();
                            if (value == "") {
                                value = $(sourceeditor).data("value");
                            }

                            value.ShowValueSource = $(sourceeditor).data("showvaluesource");
                            $(sourceeditor).iSourceEditor(value);
                        }

                        //Only change on options ID = BJJvuvCrFJHHIwECsAIuGrCADuGFstEuGJ   
                        var name = $(element).attr("name");
                        var defaultfieldId = 'BJCIHDIwICBsIGEwAssuFDHvtACJJCDEJC';
                        if (name == "BJJvuvCrFJHHIwECsAIuGrCADuGFstEuGJ" && ( defaultfieldId in valueselecteddependancy)) {
                            if ((valueselecteddependancy[defaultfieldId] == "CodeEditor" || valueselecteddependancy[defaultfieldId] == "SummerNote" || valueselecteddependancy[defaultfieldId] == "DatetimePicker" || valueselecteddependancy[defaultfieldId] == "InputMask" || valueselecteddependancy[defaultfieldId]  == "FileUpload")) {
                                try {
                                    var codeeditor = $(element).find(".codeEditor");

                                    jQuery.each(codeeditor, function (i, val) {
                                        //if (!$(val).hasClass("ace_editor")) {
                                        //    return;
                                        //}
                                        //var editor = ace.edit(val);
                                        ////$(val).off("change");
                                        //var value = editor.getValue();
                                        //var valueset = "";
                                        //if (valueselecteddependancy[defaultfieldId] == "DatetimePicker" && !value.includes("InitDateTime")) {
                                        //    /*Do not remove these comment:InitDateTime*/
                                        //    valueset = SubDefautOptionObj(GetDefaultDateTimeOption.toString());
                                        //        //'{init:function(element){ /*Do not remove these comment:InitDateTime*/var obj={"format":"yyyy-MMM-dd hh:mm:ss tt","viewMode":"days","showTodayButton":true,"buttons":{"toolbarPlacement":"bottom","showClose":true}};obj.format=obj.format.replace("tt","a");obj.format=obj.format.replace(/t/g,"");obj.format=obj.format.replace("TT","A");obj.format=obj.format.replace(/T/g,"");obj.format=obj.format.replace(/y/g,"Y");obj.format=obj.format.replace(/d/g,"D");$(element).datetimepicker(obj);}};';
                                        //}
                                        //else if (valueselecteddependancy[defaultfieldId] == "InputMask" && !value.includes("InitMask")) {
                                        //    /*Do not remove these comment:InitMask*/
                                        //    valueset = SubDefautOptionObj(GetDefaultMaskOption.toString());
                                        //        //'{init:function(element){/*Do not remove these comment:InitMask*/var obj={"Pattern":"###","translation":{"0":{"pattern":/\\d/},"9":{"pattern":/\\d/,"optional":true},"#":{"pattern":/[a-zA-Z0-9]/,"recursive":true},"A":{"pattern":/[a-zA-Z0-9]/},"S":{"pattern":/[a-zA-Z]/}}};if(obj.maxlength){$(element).attr("maxlength",obj.maxlength);}$(element).mask(obj.Pattern,obj);}};';
                                        //}
                                        //else if (valueselecteddependancy[defaultfieldId] == "FileUpload" && !value.includes("InitFileUpload")) {
                                        //    /*Do not remove these comment:InitFileUpload*/
                                        //    valueset = SubDefautOptionObj(GetDefaultUploadOption.toString());
                                        //    //'{init:function(element){/*Do not remove these comment:InitFileUpload*/var obj={"maxFileSize":1000000000,"disableImageResize":false,"imageForceResize":true,"imageMaxWidth":1080,"imageMaxHeight":1080,"url":urllink};var uploadelement=$(element).fileupload(obj);$(element).addClass("fileupload-processing");var formData=$(element).serializeArray();$.ajax({url:"/Upload/GetUploadedFiles",dataType:"json",data:formData,type:"Post",context:uploadelement}).always(function(){$(element).removeClass("fileupload-processing");}).done(function(result){$(element).fileupload("option","done").call(element,$.Event("done"),{result:{files:result}});});}};';
                                        //}
                                        //else if (valueselecteddependancy[defaultfieldId] == "SummerNote" && !value.includes("InitSummernote")) {
                                        //    /*Do not remove this comment:InitSummernote*/
                                        //    valueset = SubDefautOptionObj(GetDefaultSummerNoteOption.toString());
                                        //    //'{init:function(element){/*Do not remove this comment:InitSummernote*/var obj={height:200,hint:[{match:/:([\-+\w]+)$/,search:function(keyword,callback){callback($.grep(emojis,function(item){return item.indexOf(keyword)===0;}));},template:function(item){var content=emojiUrls[item];return"<img src=\""+content+"\" width=\"20\" /> :"+item+":";},content:function(item){var url=emojiUrls[item];if(url){return $("<img />").attr("src",url).css("width",20)[0];}return"";}}]};$(element).summernote(obj);}};';
                                        //}
                                        //else if (valueselecteddependancy[defaultfieldId] == "CodeEditor" && !value.includes("InitCodeEditor")) {
                                        //    /*Do not remove this comment:InitCodeEditor*/
                                        //    valueset = SubDefautOptionObj(GetDefaultCodeEditorOption.toString());
                                        //        //'init:function(element){/*Do not remove this comment:InitCodeEditor*/try{var editor=ace.edit(element);editor.setTheme("ace/theme/monokai");editor.session.setUseWrapMode(true);editor.session.setNewLineMode("windows");editor.session.setMode("ace/mode/javascript");var value=$(element).data("value");if(typeof(value)=="string"){editor.setValue(value);}else{editor.setValue(JSON.stringify(value));}ace.edit(element).setOptions({enableBasicAutocompletion:true,enableLiveAutocompletion:true,enableSnippets:false});var beautify=ace.require("ace/ext/beautify");setTimeout(function(){beautify.beautify(editor.session);},3000);}catch(e){console.error(e);}}};';
                                        //}
                                        //if (valueset != "") {
                                        //    editor.setValue(valueset);                       
                                        //}



                                    });

                                }
                                catch (e) {

                                }
                            }
                        }
                        
                        
           
                    }
                    else {
                        $(element).hide();
                    }
                    
                }
                else {
                    //console.log(fieldName + "-" + "Cant hided.");
                }
            }    

        }
    });

   

}
function SubDefautOptionObj(str) {
    var returnstr = "return ";
    var indexStart = str.indexOf(returnstr) + returnstr.length;
    var endstr = "};";
    var endindex = str.lastIndexOf(endstr) + endstr.length;
    return str.substr(indexStart, endindex - indexStart);
}
function GetDependancyValue(id,name) {
   
    var element = $("[name='"+name+id+"']");
    if ($(element).hasClass("ICheckList")) {
        var checkelements = $(element).closest("div.FieldInput").first();
        var checklist = $(checkelements).find("input[name='" + name + id + "']")
        var checked = [];
        jQuery.each(checklist, function (i, valcheck) {
            if (valcheck.checked) {
                checked.push(valcheck.value);
            }
        });
        return checked;

    }
    else if ($(element).is(":checkbox")) {
        return [$(element).prop("checked").toString()];

    }
    else if ($(element).hasClass("select2-multiple") || $(element).hasClass("ajax-sortable") || $(element).hasClass("sortable") || $(element).hasClass("js-data-ajax-multiple")) {
        return $(element).val();
    }
    else if ($(element).hasClass("select2") || $(element).hasClass("js-data-ajax") || $(element).hasClass("js-data-ajax")) {
      return [$(element).val()];
    }
    else if ($(element).hasClass("ICheckRadio")) {
      return [$(element).find("input[name=" + name + id + "]:checked").val()];
    }
    else if ($(element).hasClass("Mask")) {
       return [$(element).cleanVal()];
    }
    else if ($(element).hasClass("date")) {
        return [$(element).find("input").val()];
    }
    else {
        return [$(element).val()];
    }
}



function InitReadOnly() {
    var elementinput = $(".FieldInput");
    jQuery.each(elementinput, function (i, val) {
        var readonly = parseBool($(val).data("readonly"));
        if (readonly) {

            if ($(val).hasClass("switchbox")) {
                $(val).bootstrapSwitch('disabled', true);
            }
            else {
                $(val).prop('disabled', true);
                $(val).find("*").prop('disabled', true);

            }

        }
    });
}

function InitOnChangedEvent() {
  
    var element = $('.Summernote');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on('summernote.blur', function (we, contents, $editable) {
                var event = $(val).data("onchanged");
                if (event && !disableChangeEvent) {
                    eval(event);
                    fn(val, data);

                }

            });

            TriggerDefaultValue(val);
        }
    });

    element = $('.switchbox');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on('switchChange.bootstrapSwitch', function (event, state) {
                var event = $(val).data("onchanged");
                if (event && !disableChangeEvent) {
                    eval(event);
                    fn(val, data);
                    //console.log(this);
                }
            });

            TriggerDefaultValue(val);
        }
    });

    element = $('.datetime_picker');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on('change.datetimepicker', function (e) {
                var event = $(val).data("onchanged");
                if (event && !disableChangeEvent) {
                    eval(event);
                    fn(val, data);
                    //console.log(this);
                }
            });

            TriggerDefaultValue(val);

        }
    });

    //Handle ICheck Box Toggle
    element = $('.ICheck,.ICheckTable');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on(
                'ifToggled',
                function (e) {
                    var event = $(val).data("onchanged");
                    if (event && !disableChangeEvent) {
                        eval(event);
                        fn(val, data);
                        //console.log(this);
                    }
                });

            TriggerDefaultValue(val);
        }
    });

    element = $('.ICheckList');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on(
                'ifToggled',
                function (e) {
                    var event = $(val).data("onchanged");
                    if (event && !disableChangeEvent) {
                        eval(event);
                        fn(val, data);
                        //console.log(this);
                    }
                });

            TriggerDefaultValue(val);
        }
    });

    element = $('.ICheckRadio');
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on(
                'ifChecked',
                function (e) {
                    var event = $(val).data("onchanged");
                    if (event && !disableChangeEvent) {
                        eval(event);
                        fn(val, data);
                        //console.log(this);
                    }
                });

            TriggerDefaultValue(val);
        }
    });
    element = $(".codeEditor");
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            var event = $(val).data("onchanged");           
            $(val).on("commitchange", function (e) {          
                if (event && !disableChangeEvent) {
                    eval(event);
                    fn(val, data);
                    //console.log(this);
                }

            });
            TriggerDefaultValue(val);
        }
    });

    element = $(".select2sortable, .textbox, .encryptedtext, .Mask, .sourceEditor, .ikeyvalue, select.select2, select.select2-multiple, select.js-data-ajax, select.js-data-ajax-multiple, .colorpicker ");
    jQuery.each(element, function (i, val) {
        var initialize = $(val).attr("initializechange");
        if (!initialize) {
            $(val).attr("initializechange", "true");
            $(val).on("change", function (e) {
                var event = $(val).data("onchanged");
                if (event && !disableChangeEvent) {
                    eval(event);
                    fn(val, data);
                    //console.log(this);
                }

            });
            TriggerDefaultValue(val);
 
        }
    });
  
}
function TriggerDefaultValue(element) {
    var setdefault = $(element).data("setdefault");
    if ( setdefault && setdefault.toLowerCase() == "true") {
        var event = $(element).data("onchanged");
        if (event && !disableChangeEvent) {
            eval(event);
            fn(element, data);
            //console.log(this);
        }
    }
}
$(document).ready(function () {

    $("#modalbasic").on('hidden.bs.modal', function () {
        ClearModal();
    });
});
function parseBool(val) {
    if (!val) {
        return false;
    }
    return val.toString().toLowerCase() == 'true';
}

function RedirectUrl(history) {

    LoadPage(history);

}
function AjaxRequestHistory(url, params) {
    var jsonData;
    $.ajax({
        url: url,
        type: "Post",
        data: params,
        async: false,
        success: function (data) {
            jsonData = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
    if (jsonData) {
        switch (jsonData.Code) {
            case "Success":
                return jsonData;
                break;
            case "Fail":
                CaptureError(jsonData.Message);
                break;
            case "SessionExpired":
        
                break;
        }
    }

    return jsonData;
}



function UploadImage(fileinputid) {
    $("#" + fileinputid).submit();
}

function CaptureError(errorMessage) {
    ShowModal("<span style='color:red'> Error </span>", errorMessage);
    //alert(errorMessage);
}

function ShowBusy() {
    $.blockUI({ message: "Loading" });
}

function ShowReady() {
    $.unblockUI();
}

function ClearModal() {
    $("#modaltitle").html("");
    $("#modalbody").html("");

}
function CloseModal() {
    ClearModal();
    $("#modalbasic").modal('hide');
}
function ShowModal(title, bodycontent) {
    //$("#PageContent").html(bodycontent);
    //bootbox.alert({
    //    size: "large",
    //    title: title,
    //    message: bodycontent,
    //    callback: function () { }
    //});


    //bootbox.alert({ size: "large", title: title, message: bodycontent });
    $("#modaltitle").html(title);
    $("#modalbody").html(bodycontent);
    ////InitControl();
    $("#modalbasic").modal('show');
    //ViewModal("#modalbasic");

}
function ViewModal(modalid) {
    var e = $(modalid).clone();
    $(e).attr("id", $(e).attr("id") + "clone");
    $(e).modal();
    $(e).on('hidden.bs.modal', function () {
        $(e).remove();
    });
    //if (e) {
    //    var title = $(e).find(".modal-title").cloneNode(true);
    //    var content = $(e).find(".modal-body").cloneNode(true);
    //    ShowModal(title, content);
    //}
}

function EditTableRecord(element, isNew) {
    ShowBusy();
    ClearArray(formChangeRecord);
    var id = $(element).parent().data("id");
    var searchterm = $(element).data("searchterm");
    var tableName = $(element).closest("table").data("table");
    var url = $(element).closest("table").data("urledit");
    var updateUrl = $(element).closest("table").data("urlupdate");
    var deleteUrl = $(element).closest("table").data("urldelete");
    var urlsubtable = $(element).closest("table").data("urlsubtable");
    var param = { "tableName": tableName, "guid": id, "searchterm": searchterm, "isNew": isNew, "enableSubTable": true, "updateUrl": updateUrl, "deleteUrl": deleteUrl, "subtableUrl": urlsubtable };
    $.ajax({
        "dataType": "json",
        "contentType": "application/x-www-form-urlencoded; charset=UTF-8",
        "type": "POST",
        "url": url,
        "data": param,
        "success":
            function (data) {

                ShowReady();
                switch (data.Code) {
                    case "Success":
                        //ShowModal("Edit Form",data.Obj)
                        $("#PageContent").html(data.Obj);                    
                        RecordPageUrl([{ "PageType": "FormPage", "Url":  url, "Param": param}]);
                        InitControl();
                        InitDependancy();
                        break;
                    case "Fail":
                        alert(data.Message);
                        break;
                    case "SessionExpired":
                        window.location.href = data.Obj;
                        break;
                }
             

            },
        "error": function (jqXHR, exceptionC) {
            var msg = "";
            if (jqXHR.status === 0) {
                msg = "Not connect.\n Verify Network.";
            } else if (jqXHR.status == 404) {
                msg = "Requested page not found. [404]";
            } else if (jqXHR.status == 500) {
                msg = "Internal Server Error [500].";
            } else if (exception === "parsererror") {
                msg = "Requested JSON parse failed.";
            } else if (exception === "timeout") {
                msg = "Time out error.";
            } else if (exception === "abort") {
                msg = "Ajax request aborted.";
            } else {
                msg = "Uncaught Error.\n";
            }
            CaptureError(msg + " " + jqXHR.responseText);
            ShowReady();
        }
    });
}

function DeleteRecord(element, trash, multiple) {

    if (multiple) {
        var tablename = $(element).closest("table").data("table");
        var url = $(element).closest("table").data("urldelete");
        var checklist = $("input[name='" + tablename + "'].ICheckTable");
        var idlist = [];
        jQuery.each(checklist, function (i, val) {
            var id = $(element).parent().data("id");

            if (id && val.checked) {
                idlist.push(id);

            }
        });
        var jsonReturn = AjaxRequest(url, { "tableName": tablename, "idlist": idlist, "trash": trash });
        for (var x in idlist) {
            $("*[data-id='" + idlist[x] + "']").closest("tr").remove();
            //$("tr[id='" + idlist[x] + "']").remove();
        }


    }
    else {
        var id = $(element).parent().data("id");
        var tableName = $(element).closest("table").data("table");
        var url = $(element).closest("table").data("urldelete");
        var jsonReturn = AjaxRequest(url, { "tableName": tableName, "idlist": [id], "trash": trash });
        $("*[data-id='" + id + "']").closest("tr").remove();

    }

}

function UpdateTableData(elementTable) {

    var updateRecord = [];
    var searchterm = $(elementTable).data("searchterm");
    var url = $(elementTable).data("url");
    var tableName = $(elementTable).data("tablename");
    var idlist = Object.keys(formChangeRecord);
    if (idlist.length > 0) {
        for (var id in idlist) {
            updateRecord.push(formChangeRecord[idlist[id]]);
        }

        if (updateRecord.length > 0) {
            UpdateData(url, tableName, id, parentid, updateRecord);
        }
        else {
            bootbox.alert("No changes are made.");
        }

    }
    else {
        bootbox.alert("No changes are made.");

    }
    ShowReady();

}


function UpdateFileUploadChange(id, name) {
    var element = $("#" + name + id);
    var images = $("#Uploaded_" + name + id).find("img");
    var filesuploaded = [];
    jQuery.each(images, function (i, val) {
        if ($(val).data("path")) {
            filesuploaded.push({ Data: $(val).data("path"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
        }
        else {
            filesuploaded.push({ Data: $(val).attr("src"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
        }

    });
    if (!(id in formChangeRecord)) {
        formChangeRecord[id] = {};
    }
    formChangeRecord[id][name] = filesuploaded;
}



function LoadTrash(element) {
    var tableName = $(element).data("tablename");
    var id = $(element).data("id");
    var url = $(element).data("url");
    var searchterm = $(element).data("searchterm");
    var searchTerm = {};
    searchTerm["ParentId"] = parentid;
    var param = { "tableName": tableName, "trash": true, "searchTerm": searchTerm };
    var jsonData = AjaxRequest(url, param);
    RecordPageUrl(url, param, "Table");
    $("#PageContent").html(jsonData.Obj);
    //ShowModal(TableName, jsonData.Obj);
    InitControl();

}

function GetHistory(element) {
    var url = $(element).data("url");
    var tableName = $(element).data("tablename");
    var recordId = $(element).data("recordid");
    var fieldName = $(element).data("fieldname");
    var jsonrecord = AjaxRequest(url, { "tableName": tableName, "id": recordId, "fieldName": fieldName });
    ShowModal(jsonrecord.Message, jsonrecord.Obj);

    setTimeout(function () { InitDataTable(); }, 800);
  
    //InitControl();
}

function Logout() {
    var url = UrlAction("Logout", "Home");
    var jsonData = AjaxRequest(url, {})
    window.location.href = jsonData.Obj;
}

function AjaxRequestAsync(url, params, callback, forwardParam, errCallback) {
    $.ajax({
        url: url,
        type: "Post",
        data: params,
        async: true,
        success: function (data) {
            switch (data.Code) {
                case "Success":
                    if (callback && callback != null) {
                        callback(data, forwardParam);
                    }
                    break;
                case "Fail":
                    if (errCallback) {
                        //User Customize Error
                        errCallback(data);
                    }
                    else {
                        CaptureError(data.Message);
                    }

                    break;
                case "SessionExpired":
                    window.location.href = data.Obj;
                    break;
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
}

function AjaxRequest(url, params, errCallback) {
    var jsonData;
    $.ajax({
        url: url,
        type: "Post",
        data: params,
        async: false,
        success: function (data) {    
            jsonData = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
    if (jsonData) {
        switch (jsonData.Code) {
            case "Success":
                return jsonData;
                break;
            case "Fail":
                if (errCallback) {
                    //User Customize Error
                    errCallback(jsonData);
                }
                else {
                    CaptureError(jsonData.Message);
                }
                break;
            case "SessionExpired":
                window.location.href = jsonData.Obj;
                break;
        }
    }
    return jsonData;
}

function LoadContent(contentlist) {
    var html = "";
    $.each(contentlist, function (x, val) {
        var jsondata = contentlist[x];

        if (jsondata) {
            if (jsondata.Code && jsondata.Code == "Success") {
                //$("#PageContent").html("Test");
                // console.log(jsondata.Obj);
                html += jsondata.Obj;
            }
            else if (typeof (jsondata) == "string") {
                html += jsondata;
            }
        }
 
    }); 
    if (html == "") {
        return false;
    }
    
    $("#PageContent").html(html);
    return true;
}

function clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function InitTabTable(element, tabId, tabName) {
    var searchterm = $(element).data("searchterm");
    var defaultUrl = UrlAction("GetHtmlTable", "Control");
    var jsonData = AjaxRequest(defaultUrl, { "tableName": tabName, "searchTerm": JSON.stringify(searchterm), "trash": false });
  
    $("#Tab_" + tabId).html(jsonData.Obj);
    InitDataTable();
    Redraw();
}

function ClearArray(arr) {
    for (var variableKey in arr) {
        delete arr[variableKey];
    }
}

function PreviewMapping(element, url, fileuploadid) {
    var filepath = $(element).data("filepath");
    var readonly = parseBool($("#" + fileuploadid).data("readonly"));
    if (readonly) {
        $("#" + fileuploadid).prop('disabled', false);
        $("#" + fileuploadid).find("*").prop('disabled', false);
    }


    var tableName = $("#" + fileuploadid).val();
    if (readonly) {
        $("#" + fileuploadid).prop('disabled', true);
        $("#" + fileuploadid).find("*").prop('disabled', true);
    }

    var params = { "tableName": tableName, "filePath": filepath };
    var data = AjaxRequest(url, params);
    ShowModal("Import", data.Obj);
    InitControl();
}

function SaveIOMapping(tableName, url) {
    var dataMap = [];
    var e = $("[id='" + tableName + "'] tr.FieldInput");
    jQuery.each(e, function (index, value) {
        var eSelect = $(e[index]).find(".select2.FieldInput");
        var iomapping = eSelect.data("iomapping");
        var selectedField = $(eSelect).val();
        var eCheck = $(e[index]).find(".ICheck.FieldInput");
        var skipField = $(eCheck[0]).is(":checked");

        var primaryKey = $(eCheck[1]).is(":checked");
        iomapping.FieldId = selectedField;
        iomapping.SkipImport = skipField;
        iomapping.PrimaryKey = primaryKey;
        dataMap.push(iomapping);
    });

    var jsonResult = AjaxRequest(url, { "tableName": tableName, "param": dataMap });
    if (jsonResult) {
        bootbox.alert(jsonResult.Obj);
    }

}

function DeleteUpload(element, url, uploadId) {

    var params = { "uploadId": uploadId, "filePath": $(element).data("filepath") }
    var jsonObj = AjaxRequest(url, params);

    //ShowModal("Remove", jsonObj.Obj);
    $(element).closest("tr").remove();
}


function OnTableToggleCheck(element, data) {
    //Table Check Toggle every row check
    $(".ICheckTable[name='" + data.name + "']").iCheck(element.checked ? "check" : "uncheck");
}

function OnAllToggleCheck(element, data) { 
    $(".ICheckList[name='" + data.name + data.id + "']").iCheck(element.checked ? "check" : "uncheck");
}
function hideSelection(selectedObject) {
    return false;
};
function Select2Add(selector, options, selectedvalue, newTag, allowSpace) {
    var element = $(selector);
 

    jQuery.each(element, function (i, val) {
        var placeholder = val.getAttribute("data-placeholder-text");
        var elementSelect = $(element[i]).select2({
            placeholder: {
                id: "",
                placeholder: placeholder
            },
            width: "100%",
            tags: newTag,
            hideSelectionFromResult: hideSelection
        });


        for (var x in selectedvalue) {
            // states.push(propertynode[x].text);          
            var text = options[selectedvalue[x]];     
            var option = new Option(text, selectedvalue[x], true, true);
            elementSelect.append(option);
        }

        for (var x in options) {
            // states.push(propertynode[x].text);
            var selected = selectedvalue.indexOf(x) >= 0;
            //console.log(x + ": " + selected);
            if (!selected) {
                var option = new Option(options[x], x, selected, selected);
                elementSelect.append(option);
            }

        }
        if (allowSpace) {
            var space = "";
            var option = new Option("Space", "&nbsp", false, false);
            elementSelect.append(option);
        }

        $(element[i]).trigger("change");
        $(elementSelect).on("select2:select", function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });

        var isSortable = $(val).data("sortable");
        if (isSortable) { 
     
            $(elementSelect).select2({ tags: true, hideSelectionFromResult: false}).on("select2:select", function (evt) {
                var id = evt.params.data.id;

                var element = $(this).children("option[value=" + id + "]");

                moveElementToEndOfParent(element);

                $(this).trigger("change");
            });
            var ele = $(elementSelect).parent().find("ul.select2-selection__rendered");
            ele.sortable({
                containment: 'parent',
                update: function () {
                    orderSortedValues();
                    console.log("" + $(elementSelect).val())
                }
            });

            orderSortedValues = function () {
                var value = ''
                $(elementSelect).parent().find("ul.select2-selection__rendered").children("li[title]").each(function (i, obj) {
                    var element = $(elementSelect).children('option').filter(function () { return $(this).html() == obj.title });
                    moveElementToEndOfParent(element)
                });
            };

            moveElementToEndOfParent = function (element) {
                var parent = element.parent();

                element.detach();

                parent.append(element);
            };
       
        }
 

    });
}

function ViewImage(element) {
    ShowModal("Image", "<img src='" + $(element).attr("src") + "' >")
}

function SynchTable(url,tableName)
{
    var params = { "tableName": tableName }
    var jsonObj = AjaxRequest(url, params);
    ShowModal(tableName, jsonObj.Obj);
}

function SetFullScreen(element, tableid) {
    
    if ($(element).hasClass('fa-window-minimize')) {

        $("#Table_" + tableid).removeClass("fullscreenMode");
        $("#sidebar-container").addClass("d-md-block");
        $("#bodyContainer").addClass("col-10");
        $("#bodyContainer").removeClass("col-12 ml-3 mr-3");
        $("#sidebar-container").show();
        $(element).removeClass("fa-window-minimize");
        $(element).addClass("fa-expand-arrows-alt");     
        $("#PageContainer").css("z-index", '9999');
    }
    else {

        $("#Table_" + tableid).addClass("fullscreenMode");
        $("#sidebar-container").removeClass("d-md-block");
        $("#bodyContainer").removeClass("col-10");
        $("#bodyContainer").addClass("col-12 ml-3 mr-3");        
        $(element).removeClass("fa-expand-arrows-alt");
        $(element).addClass("fa-window-minimize");
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        $("#PageContainer").css("z-index", '9999');
        
    }


}


function UploadCsv(element, tablename, parentid) {
    var url = UrlAction("GetUploadCsv", "Control");
    var params = {};
    params["tablename"] = tablename;
    params["parentid"] = parentid;
    AjaxRequestAsync(url, { "tableName": tablename, "parentId": parentid }, ShowUploadCsv, params);
}
function ShowUploadCsv(result,param) {
    if (result.Obj) {
        ShowModal("Upload your csv file here", result.Obj);
        InitFileUpload();
    }
}
function DownloadCsv(tableId) {
    //var allinputs = $("#" + tableid + " .fieldsearch"); 
    //var columnList = [];
    ShowBusy();
    AjaxRequestAsync("/Data/DownloadCsv", { "tableId": tableId }, DownloadCallBack, tableId);
   

}

function ToggleAllDownloadCSVCheck(element, data) {
    $(".ICheck." +  data.id ).iCheck(element.checked ? "check" : "uncheck");
}

function DownloadCallBack(data, tableId) {
    ShowReady();
    if (data.Obj) {
        ShowModal("Download", data.Obj);
        InitControl();
    }
 
}
function DownloadCheckedCSV() {
    ShowBusy();
    var checklist = $(".ICheck.csvfield");
    var checkitem = [];
    $.each(checklist, function (key, value) {
        if (value.checked) {
            checkitem.push($(value).attr("name"));
        }
    });
    AjaxRequestAsync("/Data/DownloadSelectedFieldCsv", { "columnsId": checkitem }, DownloadCallBack);
}

function InitTablesUpload(tableName, parentId) {

    var propertynode = GetDataStructurePropertyNodes();
    var states = [];

    var element = $('select.select2table');
    jQuery.each(element, function (i, val) {
        if (!$(val).hasClass('select2-hidden-accessible')) {
            var placeholder = val.getAttribute("data-placeholder-text");
            $(element[i]).select2({
                placeholder: placeholder,
                width: null,
                readonly: true,
                data: null
            });
            for (var x in propertynode) {
                // states.push(propertynode[x].text);
                if (propertynode[x].NodeObject.Name == tableName) {
                    var option = new Option(propertynode[x].text, propertynode[x].NodeObject.Name, true, true);
                    element[i].append(option);
                }

            }
            element.trigger("change");

            $(val).rules("add", {
                required: true
            });
        }

    });

}


function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}
function EditField() {

}

function TemplateClick(url, tableid, recordid, param, requiredAuth) {
    if (requiredAuth) {
        AjaxRequestAsync("/Data/TemplateClick", { "url": url, "tableid": tableid, "recordid": recordid, "param": param });
    }
    else {
        AjaxRequestAsync(url, {"tableid": tableid, "recordid": recordid, "param": param });
    }
}

function CheckAllVisible(jstreeid) {
    jstreeid = '#' + jstreeid;
    var nodelist = $(jstreeid).jstree(true).get_json('#', { flat: true });
    var foundlist = [];
    $.each(nodelist, function (i, val) {
        if (!val.state.hidden) {
            foundlist.push(val.id);
        }
    });
    $(jstreeid).jstree("check_node", foundlist);

}
function UnCheckAllVisible(jstreeid) {
    jstreeid = '#' + jstreeid;
    var nodelist = $(jstreeid).jstree(true).get_json('#', { flat: true });
    var foundlist = [];
    $.each(nodelist, function (i, val) {
        var node = $(jstreeid).jstree(true).get_node(val.id);
        if (!node.state.hidden && node.children.length == 0) {
            foundlist.push(val.id);
        }
    });
    $(jstreeid).jstree("uncheck_node", foundlist);
}

function EditEncryptedField(tableid, fieldid, recordid) {
    var id = fieldid + recordid;
    ShowModal("Edit", "<div class='row'><input id='" + id + "encrypted' type='password' class='form-control col-md-9' /></div><div class='row justify-content-end mt-3'><button class='btn btn-primary col-md-1' onclick=\"SaveEncrypted('" + tableid + "', '" + fieldid + "', '" + recordid + "');\" >Save</button></div> ");

}
function SaveEncrypted(tableid, fieldid, recordid) {
    var id = fieldid + recordid;
    var data = AjaxRequest("/Data/UpdateEncrypted", {"tableId": tableid, "fieldId": fieldid, "recordId": recordid, "fieldvalue": $("#" + id + "encrypted").val()});
    if (data.Message) {
        ShowModal("Updated Encrypted", data.Message);
    }
}

function ViewEncryptedField(tableid, fieldid, recordid) {

    var id = fieldid + recordid;
    ShowModal("Login", "<div class='row'><label class='col-md-3'>Password</label><input id='" + id + "passwordlogin' type='password' class='form-control col-md-9' /></div><div class='row justify-content-end mt-3'><div id='" + id + "error' class='text-danger p-1'></div><div id='" + id +"error' class='text-danger'></div> <button class='btn btn-primary col-md-1' onclick=\"VerifyPassword('" + id + "','" + tableid + "', '" + fieldid + "', '" + recordid +"')\" >Login</button></div> ");
    
}
function VerifyPassword(elementid, tableid, fieldid, recordid)
{
    var id = fieldid + recordid;
    var data = AjaxRequest("/Home/VerifyPassword", { "password": $("#" + elementid + "passwordlogin").val() });
    if (data) {
        $("#" + id + "error").html("");
        data = AjaxRequest("/Data/ViewEncrypted", { "tableId": tableid, "fieldId": fieldid, "recordId": recordid });
        if (data.Code.toLowerCase() == "success") {
            ShowModal("View Encrypted", "<div class='input-group' ><input id='" + recordid + "password' class='form-control' type='password'><div class='input-group-addon'><i style='cursor:pointer;' onclick=\"ViewPassword('" + recordid + "')\" class='fa fa-eye-slash'aria-hidden='true'  ></i></div>");
            $("#" + recordid + "password").val(data.Obj);
        }
    }
    else {
        $("#" + id + "error").html("Invalid Password");
    }
}
function ViewPassword(recordid) {
    var inputelement = $("#" + recordid + "password");
    if ($(inputelement).attr("type") == "text") {
        $(inputelement).attr('type', 'password');
        $(this).addClass("fa-eye-slash");
        $(this).removeClass("fa-eye");
    }
    else {
        $(inputelement).attr('type', 'text');
        $(this).removeClass("fa-eye-slash");
        $(this).addClass("fa-eye");
    }
}
