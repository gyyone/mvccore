﻿(function ($) {
  
    function LoadSelectedKeyValue(element, options) {
     
 
        var this_e = element;
        if ($(this_e).data("keyvalue")) {
            return this_e;
        }
        else {
            $(this_e).data("keyvalue", true);
        }
        var elementlist = [];
        var container = CreateDiv("container-fluid p-0");
        var keyvalueelement = CreateDiv("row no-gutters ");
        var keyelement = CreateSelect2(options.select2KeyOption);
        elementlist.push(keyelement);
        var valuediv;
        var valueElement;
        if (options.select2ValueOption.Sortable) {
            valuediv = CreateSelect2Div(options.select2ValueOption);
            valueElement = $(valuediv).find("select")[0];
        }
        else {
            valuediv = CreateSelect2(options.select2ValueOption);
            valueElement = valuediv;
        }
 
        elementlist.push(valuediv);

        $.each(elementlist, function (x, val) {
            var colkeyvalue = CreateDiv("col p-0");
            colkeyvalue.appendChild(elementlist[x]);
            keyvalueelement.appendChild(colkeyvalue);
        });
   
        var thplush = CreateDivChild("col-1 pl-1", CreateIcon("fas fa-plus-square fa-lg", "color:lightgreen;cursor:pointer;"));

      
        thplush.onclick = function () {         
            var value = $(valuediv).val();
            if (!options.SelectedKeyValue) {
                options.SelectedKeyValue = {};
            }
            var selectedkeys = $(keyelement).val();
            if (!jQuery.isArray(selectedkeys)) {
                selectedkeys = [selectedkeys];
            }
            $.each(selectedkeys, function (idx, valkey) {
                options.SelectedKeyValue[valkey] = jQuery.isArray(value) ? value : [value];
                AddRow(keyelement, valuediv, valkey, options.SelectedKeyValue[valkey]);
                $(this_e).val(options.SelectedKeyValue);
            });
 
            $(this_e).trigger("change");
        };
        keyvalueelement.appendChild(thplush);
        container.appendChild(keyvalueelement);
        this_e.append(container);
        $(keyelement).select2(options.select2KeyOption);
        if (options.select2ValueOption.Sortable) {
            $(valuediv).sortableselect2(options.select2ValueOption);
        }
        else {      
            $(valuediv).select2(options.select2ValueOption);            
        }
        if (options.ReadOnly) {
            $(valueElement).prop("disabled", true);
            $(keyelement).prop("disabled", true);
        }
        if (options.WaitForKey) {
            $(valueElement).prop("disabled", true);
        }
        $(keyelement).on("change", function () {
            if(options.WaitForKey)
            {
                if (options.select2ValueOption.Sortable) {
                    $(valuediv).val(null);
                    $(valuediv).trigger("change");
                    $(valuediv).sortableselect2("clear");
                }
                var gropdiv = $(valueElement).closest(".groupdiv");
                var dependent = ParseToObj($(gropdiv).data("dependentvalue"));
                if (!dependent) {
                    dependent = {};
                }
                dependent["key"] = [$(keyelement).val()];
                $(gropdiv).data("dependentvalue", JSON.stringify(dependent));
                //console.log("Select Depent:" + dependent);
                $(valueElement).prop("disabled", false);
              
            }
        });

        $(valueElement).on("change", function () {
          
        });

        $.each(options.SelectedKeyValue, function (x, val) {
            AddRow(keyelement, valueElement, x, options.SelectedKeyValue[x]);
        });
  
        function AddRow(keyE,valueE,rowKey, rowValue) {

            var rowelement = $(container).find("[name='" + rowKey + "']");           
            if (rowelement.length == 1) {
                $(rowelement[0]).remove();
            }
     
            var row = CreateDiv("row  no-gutters border m-2");
            $(row).attr("name",rowKey);
            var col = CreateDiv("col border m-2");
            var dependent = ParseToObj($(keyE).closest(".groupdiv").data("dependentvalue"));
            if (!dependent) {
                dependent = {};
             
            }
            dependent["key"] = [rowKey];
            if ($.isArray(rowValue)) {
                var htmlvalue = "";

                $.each(rowValue, function (y, val) {
                    htmlvalue += options.select2ValueOption.keytotext(rowValue[y], JSON.stringify(dependent));
                });
            
                $(col).html(htmlvalue);
            }
            else {
                $(col).html(options.select2ValueOption.keytotext(rowValue, JSON.stringify(dependent)));
            }
            row.appendChild(CreateDivText("col border m-1", options.select2KeyOption.keytotext(rowKey, JSON.stringify(dependent))));
            row.appendChild(col);
   
            var divedit = CreateDivChild("pl-1 m-1", CreateIcon("fas fa-edit", "color:blue;cursor:pointer;"));
            $(divedit).click({ id: rowKey, value: rowValue }, function (event) {
                if (!options.ReadOnly) {
                    var optionkey = new Option(options.select2KeyOption.keytotext(event.data.id), event.data.id, true, true);
                    $(keyE).append(optionkey).trigger("change");
                    
                    if ($.isArray(event.data.value)) {
                        $(valueE).val([]);
                        $.each(event.data.value, function (y, val) {
                            var optionvalue = new Option(options.select2ValueOption.keytotext(event.data.value[y], JSON.stringify(dependent)), event.data.value[y], true, true);
                            $(valueE).append(optionvalue);
                           
                        });
              
                        $(valueE).trigger("change");
                    }
                    else {
                        var optionvalue = new Option(options.select2ValueOption.keytotext(event.data.value, event.data.id), event.data.value, true, true);
                        $(valueE).append(optionvalue);
                        $(valueE).trigger("change");
                    }
                }
                
               
            });
            var divremove = CreateDivChild("pl-1 m-1", CreateIcon("fas fa-trash-alt", "color:Crimson;cursor:pointer;"));
            $(divremove).click({ id: rowKey }, function (event) {
                if (!options.ReadOnly) {
                    delete options.SelectedKeyValue[event.data.id];
                    $(row).remove();
                    $(this_e).val(options.SelectedKeyValue);
                    $(this_e).trigger("change");
                }
       
            });
            row.appendChild(divedit);
            row.appendChild(divremove);
            container.appendChild(row);
        }
        function CreateSelect2Div(select2Option) {
            var div = document.createElement("div");
            div.appendChild(CreateSelect2(select2Option));
            return div;
        }

        function CreateSelect2(select2Option) {
            var selecte = document.createElement("select");
            if (select2Option.multiple) {
                selecte.setAttribute("multiple","multiple");
            }
            if (select2Option.ajax) {
                var option = document.createElement("option");
                option.text = "";
                selecte.add(option);
            }
            else {
                $.each(select2Option.results, function (idx, x) {
                    var option = new Option(select2Option.results[idx].text, select2Option.results[idx].id, true, true);
                    //var option = document.createElement("option");
                    //option.text = select2Option.results[idx].text;
                    //option.id = select2Option.results[idx].id;
                    selecte.add(option);
                });
         
            }    
            return selecte;
        }
    }
    $.fn.iKeyValue = function (options) {

        // This is the easiest way to have default options.
        var settings = $.extend({            
        }, options);
       
        options.select2KeyOption.keytotext = options.select2KeyOption.keytotext || function (text,dependent) {    
            return text;
        };
        options.select2ValueOption.keytotext = options.select2ValueOption.keytotext || function (text, dependent) {      
            return text;
        };
        options.WaitForKey = options.WaitForKey || true;
        options.select2ValueOption.Sortable = options.select2ValueOption.Sortable || false;
        LoadSelectedKeyValue(this, options);

  
        return this;
    };

}(jQuery));

function InitKeyValue()
{
    var e_keyvalue = $(".ikeyvalue");
    jQuery.each(e_keyvalue, function (i, val) {
        var initialize = $(val).attr("initializedkeyvalue");
        if (initialize) {
            return;
        }
    
        $(e_keyvalue[i]).html("");
        //var keyurl = $(val).data("keyurl");
        //var valueurl = $(val).data("valueurl");
        var keysource = $(val).data("keysource");
        var keyplaceholder = $(val).data("keyplaceholder");
        var tablename = $(val).data("tablename");
        var recordid = $(val).data("recordid");
        var readonly = parseBool($(val).data("readonly"));
        var valuesource = $(val).data("valuesource");
     
        var valueplaceholder = $(val).data("valueplaceholder");
        var selectedkeyvalue = $(val).data("selectedkeyvalue");
        var ValueSourceMultipleValue = $(val).data("valuesourcemultiplevalue");

        $(e_keyvalue[i]).iKeyValue({
            select2KeyOption: GetSelect2OptionBySource(keysource, $(this), false, keyplaceholder),
            select2ValueOption: GetSelect2OptionBySource(valuesource, $(this), ValueSourceMultipleValue, valueplaceholder),
            SelectedKeyValue: selectedkeyvalue,
            ReadOnly: readonly
        });
        $(e_keyvalue[i]).attr("initializedkeyvalue", true);
        ////ReadOnly Setting
        InitReadOnly();
    });
}