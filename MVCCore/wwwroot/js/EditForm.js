﻿
function UpdateFormData(element) {
    var searchterm = $(element).data("searchterm");
    var url = $(element).data("url");
    var tableName = $(element).data("tablename");
    var id = $(element).data("id");  

    if (formChangeRecord[id] ) {
        UpdateData(url, tableName, id, searchterm, formChangeRecord[id]);
    }
    else {
        bootbox.alert("No changes are made.");
    }

}

function Save(element) {
    $(element).closest(".portlet-body").find(".fa-save").click();
}
function UpdateFileAccess(fileid, fieldid, recordid) {
    var selecte = $("#" + fileid);
    if (selecte) {
        AjaxRequest("/Upload/UpdateAccess", { "fileid": fileid, "recordid": recordid, "fieldId": fieldid, accessvalue: $(selecte).val() });
    }
 
}
function UpdateData(url, tableName, id, searchterm, updateRecord) {
    ShowBusy();
    try {
        // signarRServer.server.signalChanged(userName, updateRecord);

        var data = { "tableName": tableName, "id": id, "searchterm": searchterm, "updateRecords": updateRecord };
        $.ajax({
            "dataType": "json",
            "contentType": "application/json",
            "type": "POST",
            "url": url,
            "data": JSON.stringify(data),
            "async": true,
            "success":
                function (data) {
                    ShowReady();
                    switch (data.Code) {
                        case "Success":
                            // $("#" + id + " .status").html("<span style='color:red;'>Record Saved</span>");

                            bootbox.alert(data.Obj);
                            ClearArray(formChangeRecord);
                            delete formChangeRecord;

                            break;
                        case "Fail":
                            alert(data.Message);
                            break;
                        case "SessionExpired":
                            window.location.href = data.Obj;
                            break;
                    }

                },
            "error": function (jqXHR, exceptionC) {

                var msg = "";
                if (jqXHR.status === 0) {
                    msg = "Not connect.\n Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg = "Time out error.";
                } else if (exception === "abort") {
                    msg = "Ajax request aborted.";
                } else {
                    msg = "Uncaught Error.\n";
                }
                CaptureError(msg + " " + jqXHR.responseText);

            }
        });



    }
    catch (err) {
        CaptureError(err);
        ShowReady();
    }


}

function GetFormValues(id) {
    var formsvalue = {};
    var formelements = $("[name*='" + id + "']");
    jQuery.each(formelements, function (i, val) {
        var name = $(formelements[i]).attr("name").replace(id,"");
        var element = val;
        if ($(element).hasClass("ICheckList")) {
            var checkelements = $(element).closest("div.FieldInput").first();
            var checklist = $(checkelements).find("input[name='" + name + id + "']")
            var checked = [];
            jQuery.each(checklist, function (i, valcheck) {
                if (valcheck.checked) {
                    checked.push(valcheck.value);
                }
            });
            formsvalue[name] = checked;

        }
        else if ($(element).hasClass("ikeyvalue")) {
            formChangeRecord[name] = $(element).val();
        }
        else if ($(element).is(":checkbox")) {
            formsvalue[name] = element.checked;

        }
        else if ($(element).hasClass("Summernote")) {
            formsvalue[name] = $(element).summernote("code");
        }
        else if ($(element).hasClass("sortablelist")) {

            var listitem = $(element).find("li.liitem");
            var selectedid = [];
            $.each(listitem, function (index, value) {
                selectedid.push($(value).data("key"));
            });
            formsvalue[name] = selectedid;
        }
        else if ($(element).hasClass("select2-multiple") || $(element).hasClass("ajax-sortable") || $(element).hasClass("sortable") || $(element).hasClass("js-data-ajax-multiple")) {
            formsvalue[name] = $(element).val();
        }
        else if ($(element).hasClass("select2") || $(element).hasClass("js-data-ajax") || $(element).hasClass("js-data-ajax")) {
            formsvalue[name] = [$(element).val()];
        }
        else if ($(element).hasClass("ICheckRadio")) {
            formsvalu[name] = $(element).find("input[name=" + name + id + "]:checked").val();
        }
        else if ($(element).hasClass("Mask")) {
            formsvalue[name] = $(element).cleanVal();
        }
        else if ($(element).hasClass("date")) {
            formsvalue[name] = $(element).find("input").val();
        }
        else if ($(element).hasClass("codeEditor")) {
            var editor = ace.edit(element);            
            formsvalue[name] =editor.getValue();

        }
        else if (element.type == "file") {
            try {
                
                var uploadid = element.id;
                var images = $("#Uploaded_" + uploadid).find(".uploadfile");

                var filesuploaded = [];
                jQuery.each(images, function (i, val) {
                    if ($(val).data("path")) {
                        filesuploaded.push({ Data: $(val).data("path"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                    }
                    else {
                        if ($(val).attr("src")) {
                            filesuploaded.push({ Data: $(val).attr("src"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                        }
                        else {
                            filesuploaded.push({ Data: $(val).data("data"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                        }

                    }

                });
                formsvalue[name] = JSON.stringify(filesuploaded);
                jQuery.each(element.files, function (i, val) {
                    if (val.type.indexOf("image") >= 0) {
                        // console.log(val);
                        //Save Quality
                        canvasResize(val, {
                            width: 1080,
                            height: 0,
                            crop: false,
                            quality: 80,
                            //rotate: 90,
                            callback: function (imagebyte, width, height) {
                                filesuploaded.push({ Data: imagebyte, FileName: val.name, FileSize: val.size, FileType: val.type });
                                formsvalue[name] = JSON.stringify(filesuploaded);
                                $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><img  class='uploadfile' style='width:100px;' src='" + imagebyte + "' data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  /><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                                if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                                    DisplayLast("Uploaded_" + uploadid);
                                }

                            }
                        });
                    }
                    else {
                        var reader = new FileReader();
                        reader.onloadend = function () {
                            filesuploaded.push({ Data: reader.result, FileName: val.name, FileSize: val.size, FileType: val.type });
                            formsvalue[name] = JSON.stringify(filesuploaded);
                            $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><p data-data='" + reader.result + "' class='uploadfile' style='width:100px;'  data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  />" + val.name + " </p><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                            if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                                DisplayLast("Uploaded_" + uploadid);
                            }
                        }
                        reader.readAsDataURL(val);

                    }

                });

            }
            catch (ex) {
                ShowModal(ex);
            }
           

            //console.log(element.files);
            //var file = val.files[0];

        }
        else if ($(element).hasClass("sourceEditor"))
        {
            formsvalue[name] = $(element).val();
        }
        else {
            formsvalue[name] = $(element).val();
        }
    });
    return formsvalue;
}
function OnChangeForm(element, data) {

   
    var id = data.id;
    var name = data.name;
    if (!(id in formChangeRecord)) {
        formChangeRecord[id] = {};
    }
    InitDependancy();
    if ($(element).hasClass("ICheckList")) {
        var checkelements = $(element).closest("div.FieldInput").first();
        var checklist = $(checkelements).find("input[name='" + name + id + "']")
        var checked = [];
        jQuery.each(checklist, function (i, valcheck) {
            if (valcheck.checked) {
                checked.push(valcheck.value);
            }
        });
        formChangeRecord[id][name] = checked;
        $(element).closest(".FieldInput").addClass("onchangeform");
    }
    else if ($(element).hasClass("ikeyvalue")) {
        formChangeRecord[id][name] = $(element).val();
    }
    else if ($(element).is(":checkbox")) {
        formChangeRecord[id][name] = element.checked;
        $(element).parent().addClass("onchangeform");
    }
    else if ($(element).hasClass("Summernote")) {
        formChangeRecord[id][name] = $(element).summernote("code");
        $(element).parent().addClass("onchangeform");
    }
    else if ($(element).hasClass("sortablelist")) {

        var listitem = $(element).find("li.liitem");
        var selectedid = [];
        $.each(listitem, function (index, value) {
            selectedid.push($(value).data("key"));
        });
        formChangeRecord[id][name] = selectedid;
    }
    else if ($(element).hasClass("select2-multiple") || $(element).hasClass("ajax-sortable") || $(element).hasClass("sortable") || $(element).hasClass("js-data-ajax-multiple")) {
        formChangeRecord[id][name] = $(element).val();
        $(element).parent().addClass("onchangeform");
    }
    else if ($(element).hasClass("select2") || $(element).hasClass("js-data-ajax") || $(element).hasClass("js-data-ajax")) {
        formChangeRecord[id][name] = [$(element).val()];
        $(element).parent().addClass("onchangeform");
    }
    else if ($(element).hasClass("ICheckRadio")) {
        formChangeRecord[id][name] = $(element).find("input[name=" + name + id + "]:checked").val();
    }
    else if ($(element).hasClass("Mask")) {
        formChangeRecord[id][name] = $(element).cleanVal();
    }
    else if ($(element).hasClass("date")) {
        formChangeRecord[id][name] = $(element).find("input").val();
    }
    else if ($(element).hasClass("codeEditor"))
    {    
        var editor = ace.edit(element);
        var mode = ace.edit($(".codeEditor")[0]).session.getMode()["$id"];
        var theme = ace.edit($(".codeEditor")[0]).getTheme();
        var option = { "option": JSON.stringify({ "mode": mode, "theme": theme }), Value: editor.getValue()
    };

        formChangeRecord[id][name] = JSON.stringify(option);
   
    }
    else if ($(element).hasClass("sourceEditor")) {
        formChangeRecord[id][name] = $(element).val();
    }
    else if (element.type == "file") {
        try {
            ShowBusy();
            var uploadid = element.id;
            var images = $("#Uploaded_" + uploadid).find(".uploadfile");

            var filesuploaded = [];
            jQuery.each(images, function (i, val) {
                if ($(val).data("path")) {
                    filesuploaded.push({ Data: $(val).data("path"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                }
                else {
                    if ($(val).attr("src")) {
                        filesuploaded.push({ Data: $(val).attr("src"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                    }
                    else {
                        filesuploaded.push({ Data: $(val).data("data"), FileName: $(val).data("filename"), FileSize: $(val).data("size"), FileType: $(val).data("type") });
                    }

                }

            });
            formChangeRecord[id][name] = JSON.stringify(filesuploaded);
            jQuery.each(element.files, function (i, val) {
                if (val.type.indexOf("image") >= 0) {
                    // console.log(val);
                    //Save Quality
                    canvasResize(val, {
                        width: 1080,
                        height: 0,
                        crop: false,
                        quality: 80,
                        //rotate: 90,
                        callback: function (imagebyte, width, height) {
                            filesuploaded.push({ Data: imagebyte, FileName: val.name, FileSize: val.size, FileType: val.type });
                            formChangeRecord[id][name] = JSON.stringify(filesuploaded);
                            $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><img  class='uploadfile' style='width:100px;' src='" + imagebyte + "' data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  /><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                            if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                                DisplayLast("Uploaded_" + uploadid);
                            }

                        }
                    });



                }
                else {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        filesuploaded.push({ Data: reader.result, FileName: val.name, FileSize: val.size, FileType: val.type });
                        formChangeRecord[id][name] = JSON.stringify(filesuploaded);
                        $("#Uploaded_" + uploadid).append("<div class='col-md-3' style='margin:10px;' ><p data-data='" + reader.result + "' class='uploadfile' style='width:100px;'  data-filename='" + val.name + "' data-size='" + val.size + "' data-type='" + val.type + "'  />" + val.name + " </p><br/><a   class='btn btn-xs red' style='width:100px' onclick='$(this).parent().remove();UpdateFileUploadChange(\"" + id + "\", \"" + name + "\")' ><i class='fas fa-trash'></i></a></div>");
                        if ($("#Uploaded_" + uploadid).children().hasClass("showFirstOnly")) {
                            DisplayLast("Uploaded_" + uploadid);
                        }
                    }
                    reader.readAsDataURL(val);

                }

            });

        }
        catch (ex) {
            ShowModal(ex);
        }
        ShowReady();

        //console.log(element.files);
        //var file = val.files[0];

    } 
    else {
        formChangeRecord[id][name] = $(element).val();
    }
    $(element).addClass("onchangeform");
}
