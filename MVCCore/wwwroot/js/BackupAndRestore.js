﻿

function InitBackupAndRestore() {
    var url = UrlAction("GetSchema", "Schema");
    $.ajax({
        url: url,
        type: "Post",
        data: { "ParentId": "*" ,"TableOnly":"true"},
        async: true,
        success: function (e) {
           
            $("#schemabackup").on('state_ready.jstree', function () {

            }).jstree({
                "core": {
                    "themes": {
                        "responsive": true
                    },
                    "data": e
                },
                "search": {
                    "case_insensitive": true,
                    "show_only_matches": true,
                    "show_only_matches_children": true,      
                },
                "plugins": ["search", "contextmenu", "unique", "checkbox"],

            });
            var searching = false;
            $('#schemabackupsearch').keyup(function () {

                if (!searching) {
                    searching = true;
                    setTimeout(function () {
                        searching = false;
                        var v = $('#schemabackupsearch').val();
                        $("#schemabackup").jstree(true).search(v);
                        $("#ControlInput").html("<button></button>");
                    }, 1000);
                }

            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
            CaptureError(thrownError + xhr.responseText);
        }
    });
    LoadBackupTable();
    

}
function BackupSchema() {
  
    var selectedId = $('#schemabackup').jstree('get_selected');
    ShowBusy();
    $.ajax({
        url: "/Schema/BackupData",
        type: "Post",
        data: {
            "SchemaIdlist": selectedId
        }, 
        success: function (data) {
            if (data) {
                switch (data.Code) {
                    case "Success":                      
                        bootbox.alert("Backup Completed");    
                        break;
                    case "Fail":
                        CaptureError(data.Message);
                        break;
                    case "SessionExpired":
                        window.location.href = data.Obj;
                        break;
                }
            }
            ShowReady();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowReady();
         
        }
    });


}
function LoadBackupTable() {
  
    var tableName = "BJCwIuFFsAFrCAEDEvJCJtEwFJFuuEJHDu";
    var param = { "tableName": tableName, "searchTerm": {}, "trash": false };
    var html = AjaxRequest("/Control/GetHtmlTable", param).Obj;
    //console.log(html);
    $("#BackupTable").html(html);
    InitDataTable();

}
function download(filename, text) {
    var content = text;
    // any kind of extension (.txt,.cpp,.cs,.bat)
    var blob = new Blob([content], {
        type:"application/zip"
    });

    saveAs(blob, filename);
}



