﻿(function ($) {
       
   
    $.fn.iSourceEditor = function (methodOrOptions) {

        if ($(this).data("iSourceEditor")) {
            if (methodOrOptions.ShowValueSource) {
                $(this).find(".ValueSource").show();
            }
            else {
                $(this).find(".ValueSource").hide();
            }

            return this;
        }
        else {
            $(this).data("iSourceEditor", true);
        }
        var methods = {
            init: function (options) {
                var inputTagChecked = false;
                var keysourceArr = {};
                var sourceVal = {};
                // This is the easiest way to have default options.
                var settingsext = $.extend({
                }, options);
                var this_e = this;
            
                //Key Source

                settingsext.SourceTypes = { "TableSource": "Tables", "KeyValueSource": "Items", "APISource": "Rest Api" };
                settingsext.SelectTableUrl = settingsext.SelectTableUrl || UrlAction("GetAllTables", "Schema");
                settingsext.SelectFieldUrl = settingsext.SelectFieldUrl || UrlAction("GetAllFields", "Schema");
              
                settingsext.ShowValueSource = settingsext.ShowValueSource || false;
                keysourceArr["KeySource"] = {};
                keysourceArr["ValueSource"] = {};
                sourceVal["KeySource"] = settingsext["KeySource"];
                sourceVal["ValueSource"] = settingsext["ValueSource"];
                sourceVal["ValueSourceMultipleValue"] = settingsext["ValueSourceMultipleValue"];
                AddSourceType(this_e, "KeySource", settingsext);         
                AddSourceType(this_e, "ValueSource", settingsext);
                if (methodOrOptions.ShowValueSource) {
                    $(this).find(".ValueSource").show();
                }
                else {
                    $(this).find(".ValueSource").hide();
                }
                function AddSourceType(this_e, sourceType, settings) {
                    var SelectedSourceType;
                    if (((sourceType in settings && settings[sourceType].length > 0))
                    ) {
                        if ("LookupSource" in settings[sourceType][0]) {
                            SelectedSourceType = settings[sourceType][0]["ClassType"] || "TableSource";
                        }
                        else {
                            SelectedSourceType = "TableSource";
                        }
                    }
                    else {
                        SelectedSourceType = "TableSource";
                    }
           
                    var keysourceradio = document.createElement("div");
                    keysourceradio.className = "row " +  sourceType;;
                    this_e.append(keysourceradio);
                    for (var key in settings.SourceTypes) {

                        var radio = CreateRadio(sourceType, key, settings.SourceTypes[key], false);
                        keysourceradio.appendChild(radio);                    
                    }
                    $(keysourceradio).find("input").prop("disabled", settings.ReadOnly);
                    $(keysourceradio).iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue',
                    });
                   
           

                    var KeySourcedivContainer = document.createElement("div");
                    KeySourcedivContainer.className = sourceType;
                    this_e.append(KeySourcedivContainer);
                    $(keysourceradio).on("ifChanged", function () {

                        if ($(keysourceradio).find("input[name=" + sourceType + "]:checked").length > 0) {
                            $(KeySourcedivContainer).empty();
                            var checked = $(keysourceradio).find("input[name=" + sourceType + "]:checked").val();
                            if (checked == "TableSource") {
                                var rowDiv = document.createElement("div");
                                rowDiv.className = "row";
                                AddMultiplevalue(rowDiv, sourceType);

                                KeySourcedivContainer.appendChild(rowDiv);
                                var tablesInputDiv = document.createElement("div");
                                tablesInputDiv.className = "d-inline-flex col-12";
                                KeySourcedivContainer.appendChild(tablesInputDiv);


                                var select2Key = CreateSelect2(GetSelectOption(settings.SelectTableUrl, GetWords("Select Table", false), false,false), tablesInputDiv);
                                if (settingsext.ReadOnly) {
                                    $(select2Key).prop("disabled", true);
                                }
                                var select2KeyFields = CreateSelect2($.extend({
                                    results: [{ id: "id", text: GetWords("id", false) }]
                                }, GetSelectOption(settings.SelectFieldUrl, GetWords("Select Key Fields", false), false, true)), tablesInputDiv);
                                $(select2KeyFields).prop("disabled", true);
                                var select2fielddiv = CreateSelect2(GetSelectOption(settings.SelectFieldUrl, GetWords("Display Fields", false), true, false), tablesInputDiv);
                                var select2fields = $(select2fielddiv).find("select")[0];
                                $(select2fields).prop("disabled", true);
                                var thplush = CreateDivChild("col-1 pl-1", CreateIcon("fas fa-plus", "color:lightgreen;cursor:pointer;"));
                                tablesInputDiv.appendChild(thplush);

                                if (sourceType in settings && settings[sourceType].length > 0 && "LookupSource" in settings[sourceType][0]) {

                                    $.each(settings[sourceType], function (x, val) {
                                        var source = settings[sourceType][x]["LookupSource"];
                                        if (source == null) {
                                            settings[sourceType][x]["LookupSource"] = {};
                                            source = settings[sourceType][x]["LookupSource"];
                                        }
                                        if (typeof (source) != "string" && "TableId" in source) {
                                            if (!("Filter" in source)) {
                                                source["Filter"] = {};
                                            }
                                            AddTableRow(this_e, KeySourcedivContainer, sourceType, select2Key, select2KeyFields, select2fielddiv, source["TableId"], source["SelectedKeyField"], source["Fields"], source["Filter"]);
                                        }
                                        else {
                                            keysourceArr[sourceType] = {};
                                        }
                                    }); 
                                }

                                thplush.onclick = function () {
                                    if (!settings.ReadOnly) {
                                        var table = $(select2Key).val();
                                        var selectfieldkey = $(select2KeyFields).val();
                                        var selectfield = $(select2fielddiv).val();
                                        var filter = {};
                                        if ((sourceType in keysourceArr) && (table in keysourceArr[sourceType]) && "Filter" in keysourceArr[sourceType][table]) {
                                            filter = keysourceArr[sourceType][table]["Filter"];
                                        }

                                        AddTableRow(this_e, KeySourcedivContainer, sourceType, select2Key, select2KeyFields, select2fielddiv, table, selectfieldkey, selectfield, filter);

                                    }
             
                                };

                                $(select2Key).on("change", function () {
                                    $(select2KeyFields).data("dependentvalue", $(select2Key).val());                                    
                                    $(select2KeyFields).prop("disabled", false);
                                    $(select2fields).data("dependentvalue", $(select2Key).val());                                 
                                  
                                    $(select2fields).prop("disabled", false);
                                    $(select2fielddiv).sortableselect2("clear");
                                    $(select2fielddiv).val([]);
                                });
                            }
                            else if (checked == "KeyValueSource") {
                                var rowDiv = document.createElement("div");
                                rowDiv.className = "row";
                                var ichecktag = document.createElement("input");                             
                                var tag = CreateCheckBox(ichecktag, GetWords("New Tags"), false);
                                rowDiv.appendChild(tag);
                                
                                $(tag).on('ifToggled',
                                    function (e) {
                                        inputTagChecked = $(this).find("input")[0].checked;
                                        sourceVal = ConvertItemToLookupSource(sourceVal, sourceType, keysourceArr[sourceType]);                                  
                                        $(this_e).trigger("change");
                                    });
                                
                                AddMultiplevalue(rowDiv, sourceType);

                                KeySourcedivContainer.appendChild(rowDiv);


                                var tablesInputDiv = document.createElement("div");
                                tablesInputDiv.className = "d-inline-flex col-12";


                                KeySourcedivContainer.appendChild(tablesInputDiv);
                                var inputKey = document.createElement("input");
                                inputKey.placeholder = GetWords("Enter Key", false);
                                inputKey.className = "form-control";
                                $(inputKey).prop("readonly", settingsext.ReadOnly);
                                var inputValue = document.createElement("input");
                                $(inputValue).prop("readonly", settingsext.ReadOnly);
                                inputValue.placeholder = GetWords("Enter Value", false);
                                inputValue.className = "form-control";
                                var thplush = CreateDivChild("col-1 pl-1", CreateIcon("fas fa-plus", "color:lightgreen;cursor:pointer;"));
                                thplush.onclick = function () {
                                    if (!settingsext.ReadOnly) {
                                        var selectfieldkey = $(inputKey).val();
                                        var selectfield = $(inputValue).val();
                                        AddItemRow(this_e, KeySourcedivContainer, sourceType, inputKey, inputValue, selectfieldkey, selectfield);
                                    }                              

                                };

                                tablesInputDiv.appendChild(inputKey);
                                tablesInputDiv.appendChild(inputValue);
                                tablesInputDiv.appendChild(thplush);


                                if (sourceType in settings && settings[sourceType].length > 0 && "LookupSource" in settings[sourceType][0]) {
                                    if (typeof(settings[sourceType][0]["LookupSource"]) != "string"  && "Items" in settings[sourceType][0]["LookupSource"]) {
                                        if ("NewTag" in settings[sourceType][0]["LookupSource"]) {
                                            if (settings[sourceType][0]["LookupSource"]["NewTag"]) {
                                                $(tag).iCheck("check");
                                            }
                                            else {
                                                $(tag).iCheck("uncheck");
                                            }                                          
                                        }
                                        $.each(settings[sourceType][0]["LookupSource"]["Items"], function (x, value) {
                                            if (typeof (settings[sourceType][0]["LookupSource"]) != "string" && "Items" in settings[sourceType][0]["LookupSource"]) {
                                                var source = settings[sourceType][0]["LookupSource"]["Items"];
                                                AddItemRow(this_e, KeySourcedivContainer, sourceType, inputKey, inputValue, x, source[x]);
                                            }
                                        });
                                 
                                    }
                                    else {
                                        keysourceArr[sourceType] = {};
                                    }

                                }


                            }
                            else if (checked == "APISource") {
                                var rowDiv = document.createElement("div");
                                rowDiv.className = "row";
                                AddMultiplevalue(rowDiv, sourceType);

                                var inputKey = document.createElement("input");
                                inputKey.placeholder = GetWords("Enter Url", false);
                                inputKey.className = "form-control";
                                $(inputKey).prop("readonly", settingsext.ReadOnly);
                                $(inputKey).on("change", function () {
                                    if (!settingsext.ReadOnly) {

                                        if (sourceVal[sourceType] == null || sourceVal[sourceType].length >= 2) {
                                            sourceVal[sourceType] = [{}];
                                        }
                                        if (!("LookupSource" in sourceVal[sourceType][0])) {
                                            sourceVal[sourceType][0]["LookupSource"] = {};
                                        }
                                        sourceVal[sourceType][0]["ClassType"] = "APISource";
                                        sourceVal[sourceType][0]["LookupSource"] = {
                                            "$type": "ClassLib.ApiSource, ClassLib",
                                            "Url": $(inputKey).val()
                                        }                                  
                                        $(this_e).val(sourceVal);
                                        $(this_e).trigger("change");
                                    }
                       
                                });

                                if (sourceType in settings && settings[sourceType].length > 0 && "LookupSource" in settings[sourceType][0]) {
                                    if (typeof (settings[sourceType][0]["LookupSource"]) == "string") {
                                        $(inputKey).val(settings[sourceType][0]["LookupSource"]);
                                    }
                                    else if (settings[sourceType][0]["LookupSource"] == null)
                                    {
                                        $(inputKey).val("");
                                    }
                                    else if ("Url" in settings[sourceType][0]["LookupSource"]) {
                                        $(inputKey).val(settings[sourceType][0]["LookupSource"]["Url"]);
                                    }

                                }

                                rowDiv.appendChild(inputKey);
                                KeySourcedivContainer.appendChild(rowDiv);

                            }
                        }

                    });
                    $(keysourceradio).find("input[value='" + SelectedSourceType + "']").iCheck("check", true);
                }

                function AddMultiplevalue(thise, sourcetype) {
                    if (sourcetype == "ValueSource") {
                        var multipleValue = document.createElement("input");
                        var divmultiplevalue = CreateCheckBox(multipleValue, GetWords("Multiple Value"), sourceVal["ValueSourceMultipleValue"]);
                        thise.appendChild(divmultiplevalue);
                        setTimeout(function () {
                            $(multipleValue).iCheck({
                                checkboxClass: 'icheckbox_flat-blue',
                                radioClass: 'iradio_flat-blue',
                            });
                            $(multipleValue).on("ifChanged", function () {
                                sourceVal["ValueSourceMultipleValue"] = $(this).prop("checked");
                                $(this_e).val(sourceVal);
                                $(this_e).trigger("change");
                            });
                        }, 1000);
                       
                    }
                }

                function CreateSelect2(select2Option, element) {
                    select2Option.results = select2Option.results || [];

                    if (select2Option.Sortable) {
                        var div = document.createElement("div");
                        div.className = "col-4";
                        element.append(div);
                        var selecte = document.createElement("select");
                        div.appendChild(selecte);
                        $(div).sortableselect2(select2Option);
                        return div;
                    }
                    else {
                        var selecte = document.createElement("select");

                        if (select2Option.ajax) {
                            for (var x in select2Option.results) {
                                var option = document.createElement("option");
                                option.text = select2Option.results[x].text;
                                option.id = select2Option.results[x].id;
                                option.selected = true;
                                selecte.add(option);
                            }
                        }
                        else {
                            for (var x in select2Option.results) {
                                var option = document.createElement("option");
                                option.text = select2Option.results[x].text;
                                option.id = select2Option.results[x].id;
                                option.selected = true;
                                selecte.add(option);
                            }
                        }
                        element.append(selecte);
                        $(selecte).select2(select2Option);
                    }


                    return selecte;

                }

                function ConvertTableToLookupSource(selectsource, sourceType, tablesource) {
                    //sourceType :KeySource, ValueSource
                    if (!("KeySource" in selectsource) || !selectsource["KeySource"]) {
                        selectsource["KeySource"] = [{}];
                    }
                    if (!("ValueSource" in selectsource) || !selectsource["ValueSource"]) {
                        selectsource["ValueSource"] = [{}];
                    }
                    selectsource[sourceType] = [];
                    for (var x in tablesource) {
                        selectsource[sourceType].push({
                            "LookupSource": {
                                "$type": tablesource[x]["$type"],
                                "TableId": tablesource[x]["TableId"],
                                "SelectedKeyField": tablesource[x]["SelectedKeyField"],
                                "Fields": tablesource[x]["Fields"],
                                "NewTag": tablesource[x]["NewTag"],
                                "Filter": tablesource[x]["Filter"]
                            },
                            "ClassType": tablesource[x]["ClassType"]
                        }
                        );
                    }
                    return selectsource;
                }
                function ConvertItemToLookupSource(selectsource, sourceType, itemSource) {
                    //sourceType :KeySource, ValueSource
              
                    if (!("KeySource" in selectsource) || !selectsource["KeySource"]) {
                        selectsource["KeySource"] = [{}];
                    }
                    if (!("ValueSource" in selectsource) || !selectsource["ValueSource"]) {
                        selectsource["ValueSource"] = [{}];
                    }
                    selectsource[sourceType] = [];
                    selectsource[sourceType].push({});
                    selectsource[sourceType][0]["LookupSource"] = {};
                    selectsource[sourceType][0]["LookupSource"]["$type"] = "ClassLib.KeyValueSource, ClassLib";
                    selectsource[sourceType][0]["LookupSource"]["NewTag"] = inputTagChecked;
                    selectsource[sourceType][0]["ClassType"] = "KeyValueSource";
                    selectsource[sourceType][0]["LookupSource"]["Items"] = {};
                    for (var x in itemSource) {

                        selectsource[sourceType][0]["LookupSource"]["Items"][x] = itemSource[x];
                    }
                    return selectsource;
                }


                function AddTableRow(thisElement, KeySourcedivContainer, sourceType, select2Key, select2KeyFields, select2fielddiv, table, selectfieldkey, selectfield,filter) {

                    if (!table || table == "" || !selectfieldkey || selectfieldkey == "" || !selectfield || selectfield == "") {
                        alert("Please select");

                    }
                    else {
                        var tableid = "table" + sourceType + table;
                        var divrowflex = $("#" + tableid);
                        if (divrowflex || divrowflex.length > 0) {
                            $(divrowflex).remove();
                        }
                        divrowflex = document.createElement("div");
                        divrowflex.id = tableid;

                        divrowflex.className = "d-flex flex-row border";
                     
                        var editicon = CreateDivChild("", CreateIcon("fas fa-edit fa-lg m-2", settingsext.ReadOnly ? "" : "color:blue;cursor:pointer;"));
                        var deleteicon = CreateDivChild("", CreateIcon("fas fa-trash fa-lg m-2", settingsext.ReadOnly ? "" :  "color:red;cursor:pointer;"));
                        var filtericon = CreateDivChild("", CreateIcon("fas fa-filter fa-lg m-2", settingsext.ReadOnly ? "" :  "color:grey;cursor:pointer;"));
                        divrowflex.appendChild(editicon);
                        divrowflex.appendChild(deleteicon);
                        divrowflex.appendChild(filtericon);
                        divrowflex.appendChild(CreateDivText("col-4 m-1 p-1 bg-light border", GetValue(settingsext.SelectTableUrl, table) ));
                        divrowflex.appendChild(CreateDivText("col-2 m-1 p-1 bg-light border", GetValue(settingsext.SelectFieldUrl, selectfieldkey) ));

                        var joinedfield = [];

                        $.each(selectfield, function (index, value) {
                            joinedfield.push(GetValue(settingsext.SelectFieldUrl, value));
                        });
                   
                        divrowflex.appendChild(CreateDivText("col-5 m-1 p-1 bg-light border", joinedfield.join(",")));
                        var tableDisplay = document.createElement("div");
                        KeySourcedivContainer.appendChild(tableDisplay);
                        tableDisplay.append(divrowflex);
                    
                        keysourceArr[sourceType][table] = { "Filter": filter, "ClassType": "TableSource", "TableId": table, "SelectedKeyField": selectfieldkey, "Fields": selectfield, "NewTag": false, "$type": "ClassLib.TableSource, ClassLib" };
                        sourceVal = ConvertTableToLookupSource(sourceVal, sourceType, keysourceArr[sourceType])

                        $(thisElement).val(sourceVal);
                        $(thisElement).trigger("change");
                        $(deleteicon).click({ TableId: table }, function (event) {
                            if (!settingsext.ReadOnly) {
                                divrowflex.remove();
                                delete keysourceArr[sourceType][event.data.TableId];
                                sourceVal = ConvertTableToLookupSource(sourceVal, sourceType, keysourceArr[sourceType]);
                                $(thisElement).val(sourceVal);
                                $(thisElement).trigger("change");
                            }
      
                        });
                        $(filtericon).click({ sourceType: sourceType, Table: table }, function (event) {
                            if (!settingsext.ReadOnly) {
                                ShowModal("Edit Filter", "<div class='portlet-body groupdiv' style='overflow:auto;height:80vh;' ><div id='EditFilter' class=\"col-md-10 \" data-tablename=\"" + event.data.Table + "\" data-valuesourcemultiplevalue=\"true\" data-recordid=\"" + event.data.Table + "\" data-onchanged=\"var fn=OnChangeForm; var data={id:'" + event.data.Table + "',name:'" + event.data.Table + "'}\" data-keyurl=\"/Data/Select\" data-keysource=\"/Schema/GetAvailableLookupFields?tableId=" + event.data.Table + "\" data-keyplaceholder=\"Select Field\"  data-valuesource=\"/Schema/GetAvailableValue?tableId=" + event.data.Table +"&fieldIdofSelect=key\" data-valueplaceholder=\"Select Value\" data-selectedkeyvalue=\"\" ></div></div>");
                                var obj = event.data;

                                var e_keyvalue = $("#EditFilter");
                                $(e_keyvalue).html("");
                                //var keyurl = $(e_keyvalue).data("keyurl");
                                //var valueurl = $(e_keyvalue).data("valueurl");
                                var keysource = $(e_keyvalue).data("keysource");
                                var keyplaceholder = $(e_keyvalue).data("keyplaceholder");
                                var tablename = obj.Table;
                                var recordid = $(e_keyvalue).data("recordid");
                                var valuesource = $(e_keyvalue).data("valuesource");
                              
                                var valueplaceholder = $(e_keyvalue).data("valueplaceholder");
                                var selectedkeyvalue = keysourceArr[sourceType][obj.Table]["Filter"];
                                var ValueSourceMultipleValue = $(e_keyvalue).data("valuesourcemultiplevalue");
                             

                                $(e_keyvalue).iKeyValue({
                                    select2KeyOption: GetSelect2OptionBySource(keysource, e_keyvalue, false, keyplaceholder),
                                    select2ValueOption: GetSelect2OptionBySource(valuesource, e_keyvalue, ValueSourceMultipleValue, valueplaceholder),
                                    SelectedKeyValue: selectedkeyvalue
                                });
                                var initialize = $(e_keyvalue).attr("initialize");
                                if (!initialize) {
                                    $(e_keyvalue).attr("initialize", "true");

                                    $(e_keyvalue).on("change", function (e) {

                                        var changeevent = $(e.target).data("onchanged");
                                        if (changeevent) {
                                            eval(changeevent);
                                            keysourceArr[sourceType][obj.Table]["Filter"] = $(e_keyvalue).val();
                                            sourceVal = ConvertTableToLookupSource(sourceVal, sourceType, keysourceArr[sourceType]);
                                            $(thisElement).val(sourceVal);
                                            $(thisElement).trigger("change");

                                        }
                                    });
                                }
                            }
                            
                         
                        });
                 
                        $(editicon).click({ sourceType: sourceType, Table: table }, function (event) {
                            if (!settingsext.ReadOnly) {
                                var obj = event.data;
                                var tableName = GetValue(settingsext.SelectTableUrl, obj.Table);
                                var tableoption = new Option(tableName, obj.Table, false, false);
                                select2Key.append(tableoption);
                                $(select2Key).val(obj.Table);
                                $(select2Key).trigger('change');



                                var selectedfieldname = GetValue(settingsext.SelectFieldUrl, keysourceArr[obj.sourceType][obj.Table]["SelectedKeyField"]);
                                var selectkeyoption = new Option(selectedfieldname, keysourceArr[obj.sourceType][obj.Table]["SelectedKeyField"], false, false);
                                select2KeyFields.append(selectkeyoption);
                                $(select2KeyFields).val(keysourceArr[obj.sourceType][obj.Table]["SelectedKeyField"]);
                                $(select2KeyFields).trigger('change');

                                var selectedfields = [];
                                $.each(keysourceArr[obj.sourceType][obj.Table]["Fields"], function (x, val) {
                                    selectedfields.push({ "id": keysourceArr[obj.sourceType][obj.Table]["Fields"][x], text: GetValue(settingsext.SelectFieldUrl, keysourceArr[obj.sourceType][obj.Table]["Fields"][x]) });
                                });
                            
                                $(select2fielddiv).sortableselect2("append", selectedfields);
                            }
                           
                           
                     
                        });

                    }
                }

                function AddItemRow(thisElement, KeySourcedivContainer, sourceType, itemkeyinput, itemvalueinput, inputkey, inputvalue) {
                    if (!inputkey || inputkey == "" || !inputvalue || inputvalue == "") {
                        alert("Please enter value");
                    }
                    else {
                        var divrowflex = $("#inputkey" + inputkey);
                        if (divrowflex || divrowflex.length > 0) {
                            $(divrowflex).remove();
                        }
                        divrowflex = document.createElement("div");
                        divrowflex.id = "inputkey" + inputkey;

                        divrowflex.className = "d-flex flex-row border";
                        var editicon = CreateDivChild("", CreateIcon("fas fa-edit fa-lg m-2", settingsext.ReadOnly? "": "color:blue;cursor:pointer;"));
                        var deleteicon = CreateDivChild("", CreateIcon("fas fa-trash fa-lg m-2", settingsext.ReadOnly ? "" :"color:red;cursor:pointer;"));                     
                        divrowflex.appendChild(editicon);
                        divrowflex.appendChild(deleteicon);                      
                        divrowflex.appendChild(CreateDivText("col-4 m-1 p-1 bg-light border", inputkey));
                        divrowflex.appendChild(CreateDivText("col-2 m-1 p-1 bg-light border", inputvalue));

                        var tableDisplay = document.createElement("div");
                        KeySourcedivContainer.appendChild(tableDisplay);
                        tableDisplay.append(divrowflex);
                        $(editicon).data("value", { "key": inputkey, "value": inputvalue });
                        keysourceArr[sourceType][inputkey] = inputvalue;


                        sourceVal = ConvertItemToLookupSource(sourceVal, sourceType, keysourceArr[sourceType]);
                        $(thisElement).val(sourceVal);
                        $(thisElement).trigger("change");
                        $(deleteicon).click({ key: inputkey, "Items": inputvalue }, function (event) {
                            if (!settingsext.ReadOnly) {
                                divrowflex.remove();
                                delete keysourceArr[sourceType][event.data.key];
                                sourceVal = ConvertItemToLookupSource(sourceVal, sourceType, keysourceArr[sourceType]);
                                $(thisElement).val(sourceVal);
                                $(thisElement).trigger("change");
                            }
                  
                        });


                        $(editicon).click({ key: inputkey, value: inputvalue }, function (event) {
                            if (!settingsext.ReadOnly) {
                                $(itemkeyinput).val(event.data.key);
                                $(itemvalueinput).val(event.data.value);
                            }
                   
                        });
                    }
                    //Value Source

                }
            }
            ,
            showvalue: function () {

            }
        };

        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            // Default to "init"
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + options + ' does not exist on jQuery.sortableselect2');
        }   

       
        return this_e;
    };
    function GetValue(urlSelect, key) {
        var result = "";
        $.ajax({
            url: urlSelect,
            dataType: "json",
            type: "Post",
            data: { "name": key,"dependent":"","tags":false },
            async:false,
            success: function (data) {
                if (data.length > 0) {
                    result = data[Object.keys(data)[0]].text;
                }
              
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (thrownError != "abort") {
                    CaptureError(thrownError + xhr.responseText);
                }
            }
        });
        return result;
    }
    function GetSelectOption(urlSelect, placeholder, issortable, unique) {       
        return {  
            Sortable: issortable,
            containerCssClass: "select2-containermodal",
            width: "100%",
            placeholder: placeholder,
            ajax: {
                url: urlSelect,
                dataType: "json",
                type: "Post",
                delay: 500,
                data: function (params) {
                    var dependent = $(this).data("dependentvalue");
                    if (dependent) {
                        return {
                            "name": [params.term],
                            "dependent": dependent,
                            "tags": issortable,
                            "unique": unique
                        };
                    }
                    return {
                        "name": params.term                     
                    };
                },
                success: function (data) {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (thrownError != "abort") {
                        CaptureError(thrownError + xhr.responseText);
                    }
                },
                processResults: function (data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data
                    };
                },
                cache: true
            },
            allowClear: true,
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0

        };
       
    }

}(jQuery));

function InitSourceEditor()
{
    var e_sourceEditor = $(".sourceEditor");
    jQuery.each(e_sourceEditor, function (i, val) {
        var value = $(val).data("value");
        if (value == "" || value == null) {
            value = {};
        }
        var showvalue = $(val).data("showvaluesource");
        if (showvalue == null) {
            showvalue = true;
        }
        value.ShowValueSource = showvalue;
        value.ReadOnly = parseBool($(val).data("readonly"));
        $(e_sourceEditor[i]).iSourceEditor(value);
        ////ReadOnly Setting
        InitReadOnly();
    });
}