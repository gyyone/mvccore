﻿function CollapseSideBar() {
    $("#PageSidebar").removeClass("in");
}
var sidebarid = "#side-menu";
function InitSidebar(actionName, controllerName) {
    $(document).ready(function () {
        var servicesurl = UrlAction(actionName, controllerName);
        $.ajax({
            url: servicesurl,
            type: "Post",
            data: {},
            success: function (data) {
                switch (data.Code) {
                    case "Success":
                        PopulateSidebar(data.Obj);        
                        break;
                    case "Fail":
                        CaptureError(data.Message);
                        PopulateSidebar(data.Obj);
                        break;

                }
               
            },
            error: function (xhr, ajaxOptions, thrownError) {              
                CaptureError(thrownError + " " + xhr.responseText);               
            }

        });
        
    });

    function PopulateSidebar(node) {

        function customMenu(node) {
            // The default set of all items
            var items = {
                renameItem: {
                    // The "rename" menu item
                    label: "Rename",
                    action: function () {
                    }
                },
                deleteItem: {
                    // The "delete" menu item
                    label: "Delete",
                    action: function () {
                    }
                }
            };

            if ($(node).hasClass("folder")) {
                // Delete the "delete" menu item
                delete items.deleteItem;
            }
            return items;
        }
     
        $(sidebarid).jstree({
            "core": {
                "allowdeselectparent":true,
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': node
            },
            search: {
                "case_insensitive": false,
                "show_only_matches": true,
                "show_only_matches_children": true,
                "fuzzy": true,
                search_callback: function (str, node) {

                    return node.text.toLowerCase().indexOf(str.toLowerCase()) !== -1;
                }
            },

            "types": {
                "default": {
                    "icon": "fas fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fas fa-file icon-state-warning icon-lg"
                },
                "#": {
                    "valid_children": ["tablename"]
                },
                "tablename": {
                    "valid_children": ["fieldname"]
                },
                "fieldname": {
                    "valid_children": ["fieldname"]
                }
            },
            "plugins": ["state", "types", "search", "dnd"],

        }).on("state_ready.jstree", function () {
            var html = "";
            //var currentState = history.state;
            //if (currentState) {
            //    RedirectUrl(currentState);
            //}
            $(sidebarid).on("select_node.jstree", function (event, data) {
           
                //CollapseSideBar();
                $(sidebarid).jstree(true).save_state();
                if (data.node.original.NodeObject == null || !data.node.original.NodeObject) {
                    ShowReady();
                    return;
                }
  
                LoadPage(data.node.original.NodeObject);
          
            });

        });
        $("#SideBarSearch").keyup(function () {
            var to = false;
            if (to) {
                clearTimeout(to);
            }
            to = setTimeout(function () {
                var v = $("#SideBarSearch").val();
                $(sidebarid).jstree(true).search(v);
            }, 150);
        });
        $(sidebarid).jstree('open_all');
    }

}

function LoadDefaultPage() {
    var treedata = $('#SideBar').data().jstree.get_json("#", { flat: true });
    var treeObj = Object.keys(treedata);
    var data = $('#SideBar').data().jstree._model.data;

    for (var x in treeObj) {
        if (data[treedata[x].id].original.NodeObject != null && data[treedata[x].id].original.NodeObject.IsDefaultPage) {

            var defaultUrl = UrlAction("GetHtmlTable", "Control");
            var jsonData = AjaxRequest(defaultUrl, { "tableName": data[treedata[x].id].original.NodeObject.Params["TableName"], "searchTerm": {}, "trash": false });
            $("#PageContent").html(jsonData.Obj);
            break;
        }
    }
}

function RecordPageUrl(param) {
    history.pushState(param, null, window.location.href);
}
function LoadPage(nodeParams) {

    var contentlist = [];     
    var initform = false;
    var inittable = false;
    var initjstree = true;
    $.each(nodeParams, function (idx, val) {
        switch (val.PageType) {
            case "Table":
            case "CAECwDvJwCuGAIEsJBIDGAuDErtuIvvFus":
                var param = { "tableName": val.TableId, "searchTerm": JSON.stringify(val.SoftFilter), "sort": val.Sort, "trash": false };
                contentlist.push(AjaxRequest("/Control/GetHtmltable", param));
                inittable = true;
                break;
            case "JsTree": 
                contentlist.push("<div class='row'><input id='search"+val.id+"' type='text' class='col-md-6 form-control' style='max-width:45vh' autocomplete='off' /><a class='col-md-6 btn btn-circle btn-icon-only btn-default'><i class='icon-magnifier'></i></a></div>" +
                    "<div id='tree" + val.id + "' class='row jstreetable' data-guid='" + val.id + "' style='overflow:auto;height:80vh;width:auto;'></div>");   
                initjstree = true;
                break;
            case "Url":
            case "CAAGsEDvIEwswuEvICrEHAHErIGDuwDvuv":
                contentlist.push(AjaxRequest(val.Url));   
                //window.open(val.Url, "_blank");
                break;
            case "OpenNewTabUrl":   
                window.open(val.Url, "_blank");
                break;
            case "FormPage":
                contentlist.push(AjaxRequest(val.Url, val.Param));   
                initform = true;
                break;
        }
      
    });
    RecordPageUrl(nodeParams);
    //contentlist.push(AjaxRequest("/Menu/GetHtmlSettings"));   
    LoadContent(contentlist);
    if (initform) {
        InitControl();
        ClearArray(formChangeRecord);  
    }
    if (inittable) {
        InitDataTable();
    }
    if (initjstree) {
        InitJsTree();
    }

}
function InitJsTree(tablemenuId)
{
    $.each($(".jstreetable"), function (i, element) {
        var guid = $(element).data("guid");
        $("#tree" + guid).on('state_ready.jstree', function () {

            $("#tree" + guid).on('changed.jstree', function (e, data) {
            });

        }).jstree({
            "core": {
                "themes": {
                    "responsive": true
                },
                "data": {
                    "url": "/JsTree/SearchJsTable",
                    "type": "post",
                    "data": function (node) {
                        return { "ParentId": node.id, "tablemenuId": guid };
                    }
                },
                "check_callback": function (operation, node, parent, position, more) {
                    if (operation == "move_node") {
                        if (node.original.NodeObject.ParentId == parent.id) {
                            console.log(node.text + " Move to " + position);
                            //console.log(operation);
                            //console.log(node);
                            //console.log(parent);
                            return true;
                        }
                    }
                    return false;
                }
            },
            "search": {
                "case_insensitive": true,
                "show_only_matches": true,
                "show_only_matches_children": true,
                ajax: {
                    "url": "/JsTree/SearchJsTable",
                    "type": "post",
                    "data": function (str) {
                        return { "str": str, "tablemenuId": guid };
                    }
                }
            },
            "plugins": ["search", "state", "contextmenu", "unique"],
            contextmenu: { items: ContextMenuSchema }
        });
        var searching = false;
        $('#search' + guid).keyup(function () {

            if (!searching) {
                searching = true;
                setTimeout(function () {
                    searching = false;
                    var v = $('#search' + guid).val();
                    $("#tree" + guid).jstree(true).search(v);

                }, 1000);
            }

        });

    });


}
window.addEventListener("popstate", function (e) {
    if (e.state && e.state != null) {
        RedirectUrl(e.state);
    }
});
