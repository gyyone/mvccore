﻿namespace MVCCore.Models
{
    public class DivModel:ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }
        public string ClassName { set; get; }
        public int Height { get; set; }
    }
    public class SourceEditorModel:ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }  

    }
}