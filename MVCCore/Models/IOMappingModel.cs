using System.Collections.Generic;

namespace MVCCore.Models
{
    public class IOMappingModel
    {
        public string TableId { set; get; }
        public string TableName { set; get; }
        public string Url { set; get; }
        public List<DropdownModel> MapList { set; get; }
        public Dictionary<string,InputModel> SkipList { set; get; } 
        public Dictionary<string, InputModel> PromaryKeyList { set; get; } 
    }

}