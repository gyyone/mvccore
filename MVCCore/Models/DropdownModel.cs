﻿using System.Collections.Generic;
using System.Linq;
using MVCCore.Models;
using Newtonsoft.Json;
using Core;

using ClassLib;

namespace MVCCore.Models
{
    public class DropdownModel : ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }

        //select2-multiple, select2
        public string ClassName { set; get; }
        public bool Serverside { set; get; }
        //Get multiple Resource to form Dropdown
        //Serverside:true, Key = tablename, value = FieldName
        //Serverside:false, Key = id, value = displayText
        public Dictionary<string, string> Items { set; get; }
        public List<SelectSource> Source { set; get; } 
        public List<string> SelectedValues { set; get; }
        public string PlaceHolder { set; get; }
      
        public virtual string Multiple { set; get; }
        public string DataSorceToString()
        {
            if(Serverside)
            {
                if(Source.Any(x=> x.GetSource() is ApiSource))
                {
                    return JsonConvert.SerializeObject(Source.FirstOrDefault().GetSource()) ;
                }
                else
                {
                   List<TableSource> tbsource =  Source.Select(x => x.GetSource() as TableSource).ToList();
                    return JsonConvert.SerializeObject(tbsource);
                }
            }
            else
            {
                return JsonConvert.SerializeObject(Items.Select(x=> new DropDownItem() { id=x.Key,text=x.Value ,selected = SelectedValues.Contains(x.Key) } ));
            }
        
        }

        public string SelectedValuesToString()
        {
            return JsonConvert.SerializeObject(SelectedValues);
        }
        public bool NewTag { set; get; }
        public string ControlId { set; get; }
        public string SelectUrl { get; set; }
        public bool IsSortable { set; get; }
    }
    public class DropDownItem
    {
        public string id { set; get; }
        public string text { set; get; }
 
        public bool selected { set; get; }
    }

}