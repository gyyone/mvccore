using System.Collections.Generic;

namespace MVCCore.Models
{
    public class WebTableModel
    {
        public int sEcho { set; get; }
        public int iTotalRecords { set; get; }
        public int iTotalDisplayRecords { set; get; }
        public int deferLoading { set; get; }
        public int iDisplayStart { set; get; }
        public List<Dictionary<string,string>> aaData { set; get; }
    }

}