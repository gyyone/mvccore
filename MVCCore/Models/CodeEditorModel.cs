using Newtonsoft.Json;

namespace MVCCore.Models
{
    public class CodeEditorModel : ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }
        public string ClassName { set; get; }
  
    }
    public class CodeEditor
    {
        public CodeEditor()
        {
            LineNumbers = true;
            StyleActiveLine = true;
            StyleActiveLine = true;
            MatchBrackets = true;
            Mode = "javascript";
        }
        public bool LineNumbers { set; get; }
        public bool StyleActiveLine { set; get; }
        public bool MatchBrackets { set; get; }
        public string Mode { set; get; }
    }
}