using System.Collections.Generic;

namespace MVCCore.Models
{
    public class RadioModel : ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }

        public Dictionary<string,string> Items { set; get; }  
        public string ClassName { set; get; }
        public string Icon { set; get; }
        public string CustomData { set; get; }
        public bool Inline { set; get; }
        public string SelectUrl { get; set; }
    }
}