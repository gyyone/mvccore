﻿using Core.Akka;
using System.Collections.Generic;

namespace MVCCore.Models
{
    public class DownloadCsvViewModel
    {
        public DownloadCsvViewModel()
        {
            TableIdToNameDic = new Dictionary<string, string>();
            TableIdToFieldView = new Dictionary<string, List<TableViewField>>();
        }
        public Dictionary<string,string> TableIdToNameDic { set; get; }
        public Dictionary<string,List<TableViewField>> TableIdToFieldView { set; get; }
    }
}