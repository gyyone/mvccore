﻿using System.Collections.Generic;
using Core;

namespace MVCCore.Models
{
    public class FileUploadModel : ControlModel
    {
        public string Uploadid { set; get; }
        public string Url { set; get; }
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }

        public string FilesUrl { set; get; }
        public string Accept { set; get; } 
        public string FieldId { set; get; }
        public bool Multiple { set; get; }

    }

    public class FileSetupItem
    {
        public string UploadId { set; get; }
        public string TableName { set; get; }
        public string FileName { set; get; }
        public string ActualFilePath { set; get; }
        public bool InvalidFormat { set; get; }
        public string RequireFields { set; get; }
        public string PreviewMapUrl { set; get; }
        public string RemoveFileUrl { set; get; }
    
    }

}