using System.Collections.Generic;

namespace MVCCore.Models
{
    public class TabTableModel
    {
        public string TabText { set; get; }
        public string TabName { set; get; }

        public string TabNameJson
        {
            get { return TabName.Replace(@"\",@"\\"); }
        }
        public string TabId
        {
            get { return TabName.Replace(@"\", @""); }
        }

        public string TableName { set; get; }
        public Dictionary<string,object> SoftFilter { set; get; }
        public Dictionary<string, object> HardFilter { set; get; }
      
    }
}