using System.Collections.Generic;

namespace MVCCore.Models
{
    public class ListModel : ControlModel
    {
        public ListModel()
        {
            Items = new List<ListItem>();
        }
        public List<ListItem> Items { set; get; }   
    }

    public class ListItem
    {
        public string Key { set; get; }
        public string Description { set; get; }
    }
    public class TableFieldsModel
    {
        public string ParentId { set; get; }
        public string TableId { set; get; }
        public List<TableFieldProperty> FieldList { set; get; }

        public string UpdateSortUrl { set; get; }
    }
    public class TableFieldProperty
    {
        public string id { set; get; }
        public string Name { set; get; }
        public bool Visible { set; get; }
    }
}