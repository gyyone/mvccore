using System.Collections.Generic;

namespace MVCCore.Models
{
    public class CheckListModel : ControlModel
    {
        public CheckListModel()
        {
            SelectedValues = new List<string>();
            Items = new Dictionary<string, string>();
        }
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }

        public Dictionary<string, string> Items { set; get; }
        public string ClassName { set; get; }
        public string Icon { set; get; }
        public string CustomData { set; get; }
        public List<string> SelectedValues { get; set; }
        public string SelectUrl { get;  set; }
    }
}