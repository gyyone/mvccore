using System.Collections.Generic;

namespace MVCCore.Models
{
    public class PluginModel
    {
        public string ClassName { set; get; }
        public string Url { set; get; }
        public string Onclick { set; get; }
        public string PluginsData { set; get; }
        public List<InputModel> Plugins { set; get; } 
    }
}