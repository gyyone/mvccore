using System.Collections.Generic;

namespace MVCCore.Models
{
    public class DictionaryModel:ControlModel
    {
        public DictionaryModel(Dictionary<string,string> firstOption,Dictionary<string,string> secondOption,Dictionary<string, string> selectedFirstSecond)
        {
            
        }
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? DefaultColMd; }
        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.ValueColMd ?? DefaultValueMd; }
        }


    }
}