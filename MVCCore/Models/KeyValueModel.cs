﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Core;

namespace MVCCore.Models
{
    public class KeyValueModel : ControlModel
    {
        public KeyValueModel()
        {
            SelectedKeyValue = new Dictionary<string, List<string>>();
        }
        public DropdownModel KeyDropdown { set; get; }
        public DropdownModel ValueDropdown { set; get; }

        public Dictionary<string, List<string>> SelectedKeyValue { set; get; }
        public string SelectedKeyValueToString()
        {
            return JsonConvert.SerializeObject(SelectedKeyValue);
        }
        public bool ValueSourceMultipleValue { set; get; }
    }
}