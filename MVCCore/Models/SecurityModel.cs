using System.Collections.Generic;
using Newtonsoft.Json;
using Core;

namespace MVCCore.Models
{
    public class SecurityModel
    {
        public DropdownModel SearchTable { set; get; }
        public DropdownModel SearchField { set; get; }
        public DropdownModel SearchAccess { set; get; }
        public RadioModel BulkChangeAccess { set; get; }

        public string HeaderDic
        {
            get { return JsonConvert.SerializeObject(new Dictionary<string,string>() { { "0", SearchTable.Label}, { "1", SearchField .Label}, { "2", SearchAccess.Label} }); }
        }

        //Id, Table, Field -> security Item
        public Dictionary<string,Dictionary<string, SecurityItem>> FieldAccessDic { set; get; } 
   
    }

    public class SecurityItem
    {
        public string Id { set; get; }
        public RadioModel RadioItem { set; get; }
    }
}