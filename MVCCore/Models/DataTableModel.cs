using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Core;
using Core.Akka;

namespace MVCCore.Models
{
    public class DataTableModel
    {

        public string HeaderDic
        {
            get
            {
                var _temp = new Dictionary<int, string>();
                var _keys = VisibleHeader.Keys.ToList();
                for (var x = 0; x < _keys.Count; x++)
                {
                    _temp.Add(x, _keys[x]);
                }
                return JsonConvert.SerializeObject(_temp);
            }
        }

        public string SubTable => JsonConvert.SerializeObject(SubTableList);
        public string NotSortable => JsonConvert.SerializeObject(NotSortableLIst);
        public List<TableViewField> Headers { set; get; }
        public Dictionary<string,string> VisibleHeader { set; get; } 
        public List<string> NotSortableLIst { set; get; }
        public Dictionary<string,ControlProperty> SeachModelDic { set; get; }
  
        public Users User { set; get; }
        public bool Footer { set; get; } 
        public Dictionary<string,SubTableProperty> SubTableList { set; get; } 
        public string TableId { set; get; }
        public string TableNameDisplay { set; get; }
        public string SelectTableUrl { set; get; }
        public string UpdateUrl { set; get; }
        public string EditUrl { set; get; }
        public string DeleteUrl { set; get; }
        public string SelectUrl { set; get; }
        public string GetHtmlTableUrl { set; get; }
        public string SubtableUrl { set; get; }
        public string CommitUrl { set; get; }
        public int LeftColumnFixed { set; get; }
        public int RightColumnFixed { set; get; }

        public string Token { set; get; }
        public bool Trash { set; get; }
        public bool EnableUpload { set; get; }     
      
        public string TableType { set; get; }
        public Dictionary<string,object> SearchTerm { set; get; }
        public Dictionary<string, string> Sort { set; get; }
        public int PageLength { set; get; }


    }
}