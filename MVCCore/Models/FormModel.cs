using System.Collections.Generic;

namespace MVCCore.Models
{
    public class FormModel
    {
        public string Id { set; get; }
        public Dictionary<string,object> SearchTerm { set; get; }
        public string TableName { set; get; }
        public bool AllowSave { set; get; }
        public bool AllowDelete { set; get; }

        public string UpdateUrl { set; get; }
        public string DeleteUrl { set; get; }
        public string UrlUpload { set; get; }
        public string SubTableUrl { set; get; }
        public List<ControlModel> Controls { set; get; }
        public List<string> HistoryHeader { set; get; }
        public bool IsNew { set; get; }
        public string SubTableHtml { set; get; }
        public string TableNameDisplay { set; get; }
        public Dictionary<string, Dictionary<string, List<string>>> Dependancy { set; get; }
    }
}