﻿namespace MVCCore.Models.Public
{
    public enum JSONCODE
    {
        Fail,
        Success,
        SessionExpired
    }

    public class JsonModel
    {
        public  JsonModel()
        {
            Code = JSONCODE.Success.ToString();
        }
        public JsonModel(object obj)
        {
            Code = JSONCODE.Success.ToString();
            Obj = obj;
        }
        public string Code { set; get; }
        public string Message { set; get; }
        public object Obj { set; get; }
    }
}