using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Core.Akka;

namespace MVCCore.Models
{
    public class ItemHistoryModel
    {
        public string TableName { set; get; }
        public string FieldSearch { set; get; }
        public int PageLength { set; get; }
        public Dictionary<string, string> Headers { set; get; }
        public List<HistoryItem> HistoryRecords { set; get; }
        public Dictionary<string, ControlProperty> SeachModelDic { set; get; }
        public string HeaderDic
        {
            get
            {
                var _temp = new Dictionary<int, string>();
                var _keys = Headers.Keys.ToList();
                for (var x = 0; x < _keys.Count; x++)
                {
                    _temp.Add(x, _keys[x]);
                }
                return JsonConvert.SerializeObject(_temp);
            }
        }
    }
}