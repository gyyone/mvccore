﻿using Core;
using Core.Akka;

namespace MVCCore.Models
{
    public class ControlModel
    {
        public ControlModel()
        {
            Language = KeyWords.FieldsId.English;
        }
        public const string DefaultColMd = "col-md-4";
        public const string DefaultValueMd = "col-md-6";
        public string RecordId { get; set; }
        public string Label { set; get; }
        public string TableName { set; get; }
        public string Name { set; get; }
        public string Value { set; get; }
        public virtual string LabelColMd { set; get; }
        public virtual string ValueColMd { set; get; }
        public string CountText { set; get; }
        public int HistoryCount { set; get; }
        public bool ReadOnly { set; get; }
        public bool Visible { set; get; }
        public string OnChangedEvent { set; get; }
        public string OnCheckAllEvent { set; get; }
        public string HelpBlock { set; get; }
        public string Width { set; get; }
        public string Language { set; get; }
        public string HistoryUrl => "/Control/GetHistoryTable";
        public string Options { set; get; }
        public string FormGroup { set; get; }
        public bool SetDefault { set; get; }
  
    }
}