﻿using Core.Akka;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using MVCCore.Controllers;
using Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Util;
using ClassExtension;
namespace MVCCore.Filter
{

    public class AuthorizeApiFilter :ActionFilterAttribute, IAuthorizationFilter
    {
        private bool ValidateAPICall(string userid, string password,string ip,out string message)
        {
            Users user;
            message = string.Empty;
            //if (Settings.ApiUserDic.ContainsKey(userid))
            //{
            //    //Peformance enhancement, Use Cache to check first
            //    user = Settings.ApiUserDic[userid];
            //    if (user.UserId.ToLower().Equals(userid)
            //        && user.Password.Equals(SimpleSHA.EncryptOneWay(password))
            //        && user.EnableAPICall)
            //    {
            //        return true;
            //    }
            //    //if fail, only get from database               
            //}
            user = Settings.RequestHandler.AskSync<Users>(new GetUser() { UserIdentity = userid });
            if (user != null)
            {
                Settings.ApiUserDic[userid] = user;
            }
            else
            {
                message = "Invalid username or password";
                return false;
            }
            if (user.UserId.ToLower().Equals(userid)
        && user.Password.Equals(SimpleSHA.EncryptOneWay(password))
        && user.EnableAPICall)
            {
                return true;
            }
            if(!(user.UserId.ToLower().Equals(userid)
        && user.Password.Equals(SimpleSHA.EncryptOneWay(password))))
            {
                message = "Invalid username or password";
            }
            else if(!user.EnableAPICall)
            {
                message = $"API access denial. API user {userid} is not enable.";
            }
            else if(!user.HostIP.Contains(ip))
            {
                message = $"API access denial.Remote server {ip} is not allow.";
            }
            return false;

        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {

            if (Settings.cfg.EnableWebApi)
            {
                IPAddress remoteIpAddress = context.HttpContext.Connection.RemoteIpAddress;
                string authHeader = context.HttpContext.Request.Headers["Authorization"];
                if (authHeader != null && authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
                {
                    var token = authHeader.Substring("Basic ".Length).Trim();
                    //System.Console.WriteLine(token);
                    var credentialstring = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                    var credentials = credentialstring.Split(':');
                    string userid = credentials[0].ToLower();
                    string password = credentials[1];
                    string message;
                    if(ValidateAPICall(userid,password, remoteIpAddress.ToString(),out message))
                    {
                        context.HttpContext.Items.Add(SessionName.User, Settings.ApiUserDic[userid]);
                        return;
                    }
                    else
                    {
                        context.Result = new ContentResult()
                        {
                            Content = message,
                            StatusCode = 401
                        };
                    }
        
                }
                else
                {
                    context.Result = new ContentResult()
                    {
                        Content = "unauthorized!",
                        StatusCode = 401
                    };
                }
   
            }
            else
            {
                context.Result = new ContentResult()
                {
                    Content = "Api Call is Not Enabled.",
                    StatusCode = 401
                };
            }
      
        }
    }



}
