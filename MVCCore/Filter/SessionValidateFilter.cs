﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MVCCore.Controllers;
using MVCCore.Models.Public;
using System.Threading.Tasks;
using ClassExtension;
using Core.Akka;
using Newtonsoft.Json;

namespace MVCCore.Filter
{
    public class SessionValidateFilter : ActionFilterAttribute
    {
        public SessionValidateFilter()
        {
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Users user = context.HttpContext.Session.Get<Users>(SessionName.User);
            if (user == null || string.IsNullOrEmpty(user.id))
            {
                user = Settings.RequestHandler.AskSync<Users>(new GetUser() { UserIdentity = "" });
                context.HttpContext.Session.Set<Users>(SessionName.User, user);
                context.Result = new RedirectToRouteResult("Home\\index");


            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
        public override void OnResultExecuting(ResultExecutingContext context)
        {

        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {


      
        }
        public override Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            return base.OnResultExecutionAsync(context, next);
        }
    }



}
