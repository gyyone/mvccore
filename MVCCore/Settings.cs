﻿using System;
using Akka.Actor;
using Microsoft.AspNetCore.SignalR;
using DBHandler;
using System.Collections.Generic;

namespace MVCCore
{
    public static class Settings
    {
        public static string configpath = AppDomain.CurrentDomain.BaseDirectory + "config.json";
        public static Config cfg { set; get; }
        //public static IActorRef LocalActor = ActorRefs.Nobody;
        public static ActorSelection RequestHandler { set; get; }
        public static IActorRef Mediator { set; get; }
        public static string TempProcessPathFile { set; get; }
        public static IActorRef WebServerHandler { set; get; }     
        public static IHubContext<ServerHub> ServerHub { set; get; }
        public static object mutex = new object();
        public static DataStructure dataStructure { set; get; }
        public static Dictionary<string,Users> ApiUserDic { set; get; }
        public static DataStructure ds {
            set { 
                lock(mutex)
                {
                    dataStructure = value;
                }
            }
            get
            {
                lock (mutex)
                {
                    return dataStructure;
                }
            }
        }
    }
}
