﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models;
using MVCCore.Models.Public;
using Core;
using Core.Akka;
using Util;
using ClassLib;
using Security;
using ClassExtension;
using LogHelper;
using System.Threading;

namespace MVCCore.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }

        public IActionResult defaultindex()
        {
            //JsonModel jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            return View("Index", user);
       
        }
        public IActionResult QRScanner()
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            return View("QRScanner", user);
        }

        public IActionResult Index()
        {
            JsonModel jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            Theme usertheme = Settings.ds.GetClass<Theme>(user, user.Theme); 

            Template design = Settings.ds.GetClass<Template>(user, usertheme.Design);
            usertheme.Design = design.GetTemplateText(user, Settings.ds, usertheme);

            if (jm.Code == JSONCODE.Success.ToString() && user.id != Id.UsersAccount.PublisUser)
            {
                return View("MainPage", user);
            }
            //return View("Index", user);
            return View("CustomPage", usertheme);
        }
        public IActionResult CustomPage()
        {
            JsonModel jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            Theme usertheme = Settings.ds.GetClass<Theme>(user, user.Theme);
            Template design = Settings.ds.GetClass<Template>(user, usertheme.Design);
            usertheme.Design = design.GetTemplateText(user, Settings.ds, usertheme);
            return View("CustomPage", usertheme);
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public JsonResult UrlAction( string actionName, string controllerName)
        {
            var js = new JsonModel();
            //Need to check for permission before allow user to redirect to the page
            
            var url = Url.Action(actionName, controllerName);
            js.Code = JSONCODE.Success.ToString();
            js.Obj = url;
            return Json(js);
        }

       
        public JsonResult Logout()
        {
            JsonModel jm = new JsonModel();
            jm.Obj = "/Home/index";
            if (HttpContext.Session.Get<Users>(SessionName.User) != null)
            {
               HttpContext.Session.Set<Users>(SessionName.User, null);
            }
            return Json(jm);
        }

        public ActionResult MainPage(string tableName)
        {

            ViewBag.Message = "Main Page.";
            if (HttpContext.Session.Get<Users>(SessionName.User) == null)
            {
                return RedirectToAction("Index", "Home", new { tableName = "ErrorLog" });
            }
            ViewBag.TableName = tableName;
            var usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            ViewBag.UserConfig = usersetting;
            return View("MainPage", usersetting);
        }
        [HttpPost]
        public bool VerifyPassword(string password)
        {
            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            password = string.IsNullOrWhiteSpace(password) ? "" : SimpleSHA.EncryptOneWay(password);
            if (usersetting != null && ((usersetting.Password == null && (password == "" || password == null)) || (usersetting.Password  != null && usersetting.Password.Equals(password))))
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public async Task<ActionResult> Login(Dictionary<string, string> loginInfo)
        {
            //CancellationToken cancellationToken = HttpContext.RequestAborted;
            //int x = 0;
            //while(true)
            //{
            //    if(!cancellationToken.IsCancellationRequested)
            //    {
            //        x++;
            //        LogHandler.Debug("Processing " + x);
            //        System.Threading.Thread.Sleep(1000);
            //    }
            //    else
            //    {
            //        break;
            //    }
           
            //}
            var js = new JsonModel()
            {
                Code = JSONCODE.Success.ToString(),

            };
            if(!loginInfo.ContainsKey(Users.FieldsId.UserId))
            {
                js.Code = JSONCODE.Fail.ToString();
                js.Message = "Invalid User Name or password";
                return Json(js);
            }
            var token = Unique.NewGuid();

            string userid = loginInfo[Users.FieldsId.UserId] ?? "";
            string password = loginInfo[Users.FieldsId.Password]?? "";
            Users usersetting  = Settings.RequestHandler.AskSync<Users>(new GetUser(){UserIdentity = userid });
            if(string.IsNullOrEmpty(password))
            {
                password = "";
            }
            else
            {
                password = Security.SimpleSHA.EncryptOneWay(password);
            }
            if (usersetting == null || string.IsNullOrWhiteSpace(usersetting.UserId))
            {
                js.Code = JSONCODE.Fail.ToString();
                js.Message = "Invalid User Name or password";
                return Json(js);
            }
            else if(usersetting.UserId.ToLower() != userid.ToLower() || !password.Equals(usersetting.Password))
            {
                js.Code = JSONCODE.Fail.ToString();
                js.Message = "Invalid User Name or password";
                return Json(js);
            }
            else
            {
                HttpContext.Session.Set(SessionName.User, usersetting);
                string previousPage = Request.Headers["Referer"].ToString();
                if (previousPage.Contains("Home/defaultindex"))
                {
                    var url = Url.Action("MainPage", "Home");
                    js.Code = JSONCODE.Success.ToString();
                    js.Obj = url;
                    return Json(js);
                }
                else
                {
                    //var url = Url.Action("MainPage", "Home");
                    var url = Url.Action("CustomPage", "Home");
                    js.Code = JSONCODE.Success.ToString();
                    js.Obj = url;
                    return Json(js);
                }
            }
        }

        [HttpPost]
        public JsonResult RegisterHug(string connectionId)
        {
            var js = new JsonModel();
            //Need to check for permission before allow user to redirect to the page
            HttpContext.Session.Set(SessionName.ConnectionID, connectionId);
            js.Code = JSONCODE.Success.ToString();
            js.Obj = "";
            return Json(js);
        }
    }
}
