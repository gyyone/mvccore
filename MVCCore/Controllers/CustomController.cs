﻿using Core;
using Core.Akka.Query;
using Microsoft.AspNetCore.Mvc;
using MVCCore.Models.Public;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using ClassExtension;
namespace MVCCore.Controllers
{

    public class CustomController : Controller
    {
      
        private Dictionary<string, string> SelecFieldValues(string []search, string tableId, string fieldId, bool searchbykey)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);         
            string host = $"{Request.Scheme}://{Request.Host}";
            Dictionary<string, string> result = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryFieldValue() { FieldId = fieldId, TableId = tableId, HostUrl = host, SeachName = search, Searchbykey = searchbykey, UserIdentity = user.id }, 300);

            return result;
        }
        //Select Fields List available from table for Dependancy use
  
        private List<Dictionary<string,string>> SelectFields(string []search, string tablename, string fieldId, bool searchbykey)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            List<Dictionary<string, string>> searchresult = new List<Dictionary<string, string>>();
            foreach(var s in search)
            {
               string stemp = s ?? "";
                Dictionary<string, string>  searchterm = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryPropertyFields() { TableName = tablename, SeachName = stemp, UserIdentity = user.id, FieldId = fieldId, SearchByKey = searchbykey }, timeOut: 60);
                if(searchterm == null)
                {
                    searchterm = new Dictionary<string, string>();
                }
                searchresult.AddRange(searchterm.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }));
            }

            return searchresult;
        }


        //Select Dependancy Value of the Field
        [HttpPost]
        public JsonResult QueryDependantFieldsValue([FromBody]ApiSelectArgument args, string fieldIdOfTable = "", string fieldIdOfField = "")
        {
            List<Dictionary<string, string>> resultdic = new List<Dictionary<string, string>>();
            if(args == null)
            {
                resultdic.Add(new Dictionary<string, string>() { { "id", "Err" }, { "text", "Object Null Reference" }, { "disabled", "true" } });
                args = new ApiSelectArgument();
            }
            args.search = args.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            if (args.search.Any(x=>  x.ToLower().Equals("all")) && args.searchbykey)
            {
                resultdic.Add(new Dictionary<string, string>() { { "id", "All" }, { "text", "All" } });
            }
            else
            {
                Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
                string tableid = !dependentdic.ContainsKey(fieldIdOfTable) ? "" : dependentdic[fieldIdOfTable].FirstOrDefault();
                string fieldid = !dependentdic.ContainsKey(fieldIdOfField) ? "" : dependentdic[fieldIdOfField].FirstOrDefault();

                Dictionary<string, string> results = SelecFieldValues(args.search, tableid, fieldid, args.searchbykey);
                if (!args.searchbykey)
                {
                    resultdic.Add(new Dictionary<string, string>() { { "id", "All" }, { "text", "All" } });
                }
                foreach (var r in results)
                {
                    resultdic.Add(new Dictionary<string, string>() { { "id", r.Key }, { "text", r.Value } });
                }
            }

            return Json(new JsonModel(resultdic));
        }

        [HttpPost]
        public JsonResult QueryFields([FromBody]ApiSelectArgument args, string fieldIdOfTable = "", string fieldIdOfField = "")
        {
            Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
            string tableid = !dependentdic.ContainsKey(fieldIdOfTable) ? "" : dependentdic[fieldIdOfTable].FirstOrDefault();
            string fieldid = !dependentdic.ContainsKey(fieldIdOfField) ? "" : dependentdic[fieldIdOfField].FirstOrDefault();
            return Json(new JsonModel( SelectFields(args.search, tableid, fieldid, args.searchbykey)));
        }
        [HttpPost]
        public JsonResult QueryFieldsAndIcons([FromBody] ApiSelectArgument args, string fieldIdOfTable = "", string fieldIdOfField = "")
        {
            Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
            string tableid = !dependentdic.ContainsKey(fieldIdOfTable) ? "" : dependentdic[fieldIdOfTable].FirstOrDefault();
            string fieldid = !dependentdic.ContainsKey(fieldIdOfField) ? "" : dependentdic[fieldIdOfField].FirstOrDefault();
            List<Dictionary<string,string>> result =  SelectFields(args.search, tableid, fieldid, args.searchbykey);
            List<Icons> icons =   Settings.dataStructure.Query(Icons.ClassID).docs.Select(x=> x.CastToObject<Icons>()).ToList();

            return Json(new JsonModel(result));
        }

        [HttpPost]
        public JsonResult QueryFieldValue([FromBody]ApiSelectArgument args, string fieldIdOfTable = "", string fieldIdOfField = "")
        {
            Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
            string tableid = !dependentdic.ContainsKey(fieldIdOfTable) ? "" : dependentdic[fieldIdOfTable].FirstOrDefault();
            string fieldid = !dependentdic.ContainsKey(fieldIdOfField) ? "" : dependentdic[fieldIdOfField].FirstOrDefault();
            Dictionary<string, string> result = SelecFieldValues(args.search, tableid, fieldid, args.searchbykey);           
            return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList()));
        }


        [HttpPost]
        public JsonResult SelectUniqueList([FromBody]ApiSelectArgument args, string fieldIdOfTable = "", string fieldIdOfField = "")
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            args.search = args.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
            string tableid = !dependentdic.ContainsKey(fieldIdOfTable) ? "" : dependentdic[fieldIdOfTable].FirstOrDefault();
            string fieldid = !dependentdic.ContainsKey(fieldIdOfField) ? "" : dependentdic[fieldIdOfField].FirstOrDefault();
            Dictionary<string, string> searchterm = new Dictionary<string, string>();        
            searchterm = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryUniqueListFields() { SearchByKey = args.searchbykey, TableId = tableid, SeachName = args.search, UserIdentity = user.id }, 60);


            return Json(new JsonModel(searchterm.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList()));
        }

        [HttpPost]
        public JsonResult UniqueListKey([FromBody]ApiSelectArgument args)
        {          
            Dictionary<string, string> searchterm = new Dictionary<string, string>();
            args.search = args.search.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            if (args.searchbykey)
            {
                foreach(var s in args.search)
                {
                    
                    searchterm.Add(s??"", s??"");
                }
            }
            else
            {
                searchterm.Add("Primary_Map", "Primary Map");
                foreach (var s in args.search)
                {
                    if (!searchterm.ContainsKey(s))
                    {
                        searchterm.Add(s, s);
                    }
                }         
            }           
            return Json(new JsonModel(searchterm.Where(x => !string.IsNullOrWhiteSpace(x.Key)).Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList()));
        }
    }
}