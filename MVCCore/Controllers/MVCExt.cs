using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MVCCore.Models;
using MVCCore.Models.Public;
using Newtonsoft.Json;
using Core;
using Core.Akka;
using Core.Akka.Query;
using MVCCore.Actor;
using System.Linq;

using ClassLib;
using Util;

namespace MVCCore.Controllers
{
    public static class MVCExt
    {
        
   
        public static KeyWordsDic KeyWords { set; get; }
        public static Dictionary<string, string> GetItems(string userIdentity, List<TableSource> sources, List<string> searchlist,bool searchbyId,LuceneQuery queryType, LuceneOccur occur)
        {
            List<string> search = new List<string>();
            foreach(var s in searchlist)
            {
                search.AddRange(TypeCheck.CastToList(s).Where(x=> !string.IsNullOrWhiteSpace(x)));
            }
      
            LookupQuery query = new LookupQuery() {UserIdentity = userIdentity, QueryType = queryType,Occur = occur,SearchbyId = searchbyId,Searchlist = search, Sources = sources};
            Dictionary<string, string> queryReturn =
                 Message<Dictionary<string, string>>.Query(MVCCore.Settings.RequestHandler, query,int.MaxValue);
            return queryReturn;

        }
   
        public static InputModel ControlInputMask(string name, string label, string placeholder, string value, string icon,
    string onChangeEvent, bool readOnly,string options, int historyCount = 0, string labelColMd = null, string valueColMd = null)
        {
            var im = new InputModel();
            im.ClassName = "form-control FieldInput Mask";
            im.Icon = icon;
            im.Type = "text";
            im.Label = label;
            im.Name = name;
            im.PlaceHolder = placeholder;
            //im.CustomData = "data-maskoption=\"" + JsonConvert.SerializeObject(option) + "\"";
            im.Options = options;
            im.Value = value;
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static InputModel ControlInput(string name, string label, string placeholder, string value, string icon,
            string onChangeEvent, bool readOnly, int historyCount = 0, string labelColMd = null, string valueColMd = null)
        {
            var im = new InputModel();
            im.ClassName = "form-control FieldInput textbox";
            im.Icon = icon;
            im.Type = "text";
            im.Label = label;
            im.Name = name;
            im.PlaceHolder = placeholder;
            im.CustomData = "";
            im.Value = value;
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }
        public static FileUploadModel FileUploadInput(string name, string label,
    string onChangeEvent, bool readOnly, int historyCount = 0, string labelColMd = null, string valueColMd = null)
        {
            var im = new FileUploadModel();        
            im.Label = label;
            im.Name = name;         
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static InputModel ControlCheckBox(string name, string label, bool isChecked,
            string onChangeEvent,string customData, string icon, bool readOnly, int historyCount = 0, string labelColMd = null,
            string valueColMd = null)
        {
            var im = new InputModel();
            im.ClassName = "ICheck FieldInput";
            im.Icon = "";
            im.Type = "checkbox";
            im.Label = label;
            im.Name = name;
            im.Icon = icon;
            im.CustomData = isChecked ? customData + " checked " : customData;
            im.Value = "";
            im.OnChangedEvent = onChangeEvent;
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static InputModel ControlSwitch(string name, string label, string placeholder, bool isChecked,
            string onChangeEvent, string icon, bool readOnly, int historyCount = 0, string labelColMd = null,
            string valueColMd = null)
        {
            var im = new InputModel();
            im.ClassName = "form-control FieldInput switchbox";
            im.Icon = "";
            im.Type = "checkbox";
            im.Label = label;
            im.Name = name;
            im.Icon = icon;
            im.PlaceHolder = placeholder;
            var customdata = "";
            im.CustomData = isChecked ? customdata + " checked " : customdata;
            im.Value = "";
            im.OnChangedEvent = onChangeEvent;
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static DropdownModel ControlComboBox(string name, string label, string placeholder, bool multipleSelect,
            List<string> selectedValues,List<SelectSource> source,  string onChangeEvent,bool newtag, bool readOnly,
            int historyCount =0, string labelColMd = null, string valueColMd = null)
        {
            var im = new DropdownModel();
            bool ServerSide = false;
            if (source.Any(x => x.GetSource() is TableSource) || source.Any(x => x.GetSource() is ApiSource))
            {
                //List<TableSource> datasource = source.Where(x => x.GetSource() is TableSource).Select(x => x.GetSource() as TableSource).ToList();
                im.Source = source;
                ServerSide = true;
            }       
            else
            {
                List<KeyValueSource> datasource = source.Where(x => x.GetSource() is KeyValueSource).Select(x => x.GetSource() as KeyValueSource).ToList();
                if (datasource.Count > 0)
                {
                    im.Items = datasource[0].Items;
                    newtag = datasource[0].NewTag;
                    if(newtag)
                    {
                        foreach (var x in selectedValues)
                        {
                            if (!im.Items.ContainsKey(x))
                            {
                                im.Items.Add(x,x);
                            }
                        }
                    }
           
                 
                  
                }
                else
                {
                    im.Items = new Dictionary<string, string>();
                }
            }
            im.Name = name;                 
            im.PlaceHolder = placeholder;
            im.NewTag = newtag;
            im.Label = label;
            if(ServerSide)
            {
                im.ClassName = multipleSelect ? "js-data-ajax-multiple form-control FieldInput" : "js-data-ajax form-control FieldInput";
            }
            else
            {
                im.ClassName = multipleSelect ? "select2-multiple form-control FieldInput" : "select2 form-control FieldInput";
            }
           
            im.Multiple = multipleSelect? "multiple" : "";
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.Serverside = ServerSide;
            im.SelectedValues = selectedValues;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }
        public static DropdownModel ControlSortableComboBox(string name, string label, string placeholder,
         List<string> selectedValues, List<SelectSource> source, string onChangeEvent, bool newtag, bool readOnly,
         int historyCount = 0, string labelColMd = null, string valueColMd = null)
        {
            var im = new DropdownModel();
            bool ServerSide = false;           
            if (source.Any(x => x.GetSource() is TableSource) || source.Any(x => x.GetSource() is string) || source.Any(x => x.GetSource() is ApiSource))
            {                
                im.Source = source;
                ServerSide = true;
            }
            else
            {
                List<KeyValueSource> datasource = source.Where(x => x.GetSource() is KeyValueSource).Select(x => x.GetSource() as KeyValueSource).ToList();
                if (datasource.Count > 0)
                {
                    im.Items = datasource[0].Items;
                }
                else
                {
                    im.Items = new Dictionary<string, string>();
                }
            }
            im.Name = name;
            im.PlaceHolder = placeholder;
            im.NewTag = newtag;
            im.Label = label;
            if (ServerSide)
            {
                im.ClassName =  "ajax-sortable form-control FieldInput";
            }
            else
            {
                im.ClassName =  "sortable form-control FieldInput";
            }

            im.Multiple = "multiple";
            im.LabelColMd = labelColMd;
            im.ValueColMd = valueColMd;
            im.Serverside = ServerSide;
            im.SelectedValues = selectedValues;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            im.IsSortable = true;
            return im;
        }
        public static DivModel ControlDefaultEditor(string name, string label,string value, string onchangeEvent, bool readOnly,string options,
    int historyCount = 0, int height = 500, int width = 500)
        {
            var im = new DivModel();
            im.Name = name;
            im.Label = label;
            im.Value = value;
            im.ClassName = "FieldInput Summernote";
            im.Height = height;
            im.Width = width + "px";
            im.OnChangedEvent = onchangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            im.Options = options;
            return im;
        }

        public static InputModel ControlColorPicker(string name, string text, string value,string onchangeEvent, bool readOnly, int historyCount =0)
        {
            var im = new InputModel();
            im.ClassName = "form-control colorpicker FieldInput";
            im.Type = "text";
            im.Label = text;
            im.Name = name;
            im.OnChangedEvent = onchangeEvent;
            im.CustomData = "wheel";
            im.Value = string.IsNullOrWhiteSpace(value)? "#f0f0f0" : value;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static CodeEditorModel ControlCodeEditor(string name, string text,string value,string option,string onChangeEvent, bool readOnly, int historyCount = 0)
        {
            var im = new CodeEditorModel();
            im.Label = text;
            im.Name = name;
            im.Value = value;
            im.OnChangedEvent = onChangeEvent;
            im.ClassName = "FieldInput";
            im.Options = option ?? "";
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static RadioModel ControlRadio(string name, string text, string value,Dictionary<string,string> items,string onChangeEvent, bool readOnly, int historyCount = 0,string valueColMd = null, string labelColMd = null)
        {
            var im = new RadioModel();
            im.ValueColMd = valueColMd;
            im.LabelColMd = labelColMd;
            im.ClassName = " FieldInput";
            im.Label = text;
            im.Name = name;
            im.Items = items;
            im.CustomData = "";
            im.Value = value;
            im.OnChangedEvent = onChangeEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }

        public static CheckListModel ControlCheckList(string name, string text, List<string> selectedValues, Dictionary<string, string> items, string onChangeEvent, string onCheckAllEvent, bool readOnly, int historyCount = 0, string valueColMd = null, string labelColMd = null)
        {
            var im = new CheckListModel();
            im.ValueColMd = valueColMd;
            im.LabelColMd = labelColMd;
            im.ClassName = "FieldInput";
            im.Label = text;
            im.Name = name;
            im.Items = items;
            im.CustomData = "";
            im.SelectedValues = selectedValues;
            im.OnChangedEvent = onChangeEvent;
            im.OnCheckAllEvent = onCheckAllEvent;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            return im;
        }



        public static DateTimePickerModel ControlDateTimePicker(string name, string text, string classname, string value,string onChangeEvent, bool readOnly, string dateOption, int historyCount = 0)
        {
            var im = new DateTimePickerModel();
            im.ClassName = "FieldInput date " + classname;

            im.Options = dateOption;           
            im.Type = "text";
            im.Label = text;
            im.Name = name;
            im.OnChangedEvent = onChangeEvent;
            im.CustomData = "";
            im.Value =  value;
            im.HistoryCount = historyCount;
            im.ReadOnly = readOnly;
            
            return im;
        }

    }
}