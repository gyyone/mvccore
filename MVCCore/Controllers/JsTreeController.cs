﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLib;
using Core;
using Core.Akka.Query;
using Microsoft.AspNetCore.Mvc;
using MVCCore.Models.Public;
using JsTable = MenuProperty.Pages.JsTable;
using ClassExtension;
using Akka.Util;
using System.Text;

namespace MVCCore.Controllers
{
    public class JsTreeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetJsTable(string tablemenuId)
        {
            List<JsTable> jstables = Settings.dataStructure.JsTableIdDic.Where(x=> x.Value.ParentId == tablemenuId).Select(x=> x.Value).ToList();

            return Json(jstables);
        }

        [HttpPost]
        public JsonResult SearchJsTable(string tablemenuId, Dictionary<string, string> search)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            List<JsTable> jstables = Settings.dataStructure.JsTableIdDic.Where(x => x.Value.ParentId == tablemenuId).Select(x=> x.Value).ToList();
            List<TreeNode> treenode = new List<TreeNode>();
            foreach(var js in jstables)
            {
                Dictionary<string, object> searchTerm = new Dictionary<string, object>();
                if(js.SoftFilter != null)
                {
                    foreach (var s in js.SoftFilter)
                    {
                        searchTerm[s.Key] = s.Value;
                    }
                }
                if(js.HardFilter != null)
                {
                    foreach (var s in js.HardFilter)
                    {
                        searchTerm[s.Key] = s.Value;
                    }
                }
                searchTerm = searchTerm.Where(x => !string.IsNullOrEmpty(x.Key) && !x.Value.CastToList().Where(x=> !string.IsNullOrWhiteSpace(x)).Any() ).ToDictionary(x => x.Key, y => (object)y.Value.CastToList().Where(z => !string.IsNullOrWhiteSpace(z)));
                List<Dictionary<string, object>> results =  Settings.dataStructure.Query(js.Table, searchTerm).docs;
                foreach(var row in results)
                {
                    TreeNode tr = new TreeNode();
                    tr.id = ConcateValue(js.JstreeId,row);
                    tr.parent = ConcateValue(js.JstreeParentId, row);
                    tr.text = ConcateValue(js.JstreeDisplay, row);
                    tr.NodeObject = row;
                    tr.icon = ConcateValue(js.JstreeIcon, row);
                    treenode.Add(tr);
                }
             
            }

            foreach(TreeNode tr in  treenode)
            {
                tr.children = treenode.Any(x=> x.parent == tr.id);
            }
            if (search.ContainsKey("ParentId"))
            {
                return Json(treenode.Where(x=> x.parent == search["ParentId"]));
            }
            else if (search.ContainsKey("str"))
            {
                List<TreeNode> trSearch = treenode.Where(x => x.text.Contains(search["str"])).ToList();

                return Json(GetTreeNodeSearch(treenode, trSearch));
            }      
            return Json(new List<TreeNode>());
        }

        private List<TreeNode> GetTreeNodeSearch(List<TreeNode> fullTreenode, List<TreeNode> search)
        {
            List<TreeNode> found = new List<TreeNode>();
            foreach(var s in search)
            {
                if(!found.Any(x => x.id == s.id) && s.parent != "#")
                {
                    List<TreeNode> result = GetTreeNodeSearch(fullTreenode, fullTreenode.Where(x => x.id == s.parent).ToList());
                    if(result.Any(x => x.parent == "#"))
                    {
                        found.AddRange(result.Where(x=> !found.Any(y=> y.id == x.id)));
                    }
                }
                else if(!found.Any(x => x.id == s.id) && s.parent == "#")
                {
                    found.Add(s);
                }
            }
            return found;
        }
        private List<TreeNode> GetTreeNodeSearch(List<TreeNode> fullTreenode, TreeNode search)
        {
            List<TreeNode> found = new List<TreeNode>();
            if (search.parent != "#")
            {
              
                found.AddRange(GetTreeNodeSearch(fullTreenode, search));
            }
            else
            {
                found.Add(search);
            }
            return found;
        }
        private string ConcateValue(List<string> selectedId, Dictionary<string, object> row)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var id in selectedId)
            {
                if (row.ContainsKey(id))
                {
                    sb.Append(row[id].ToString());
                }
                else
                {
                    sb.Append(id);
                }
            }
            return sb.ToString();
        }
    }
}
