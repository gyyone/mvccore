﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Akka.Actor;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models;
using MVCCore.Models.Public;
using MVCCore.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Core;
using Core.Akka;
using Core.Akka.Query;
using Util;
using ClassLib;
using RestApi;
using DirectoryHelper;
using ClassExtension;
using CoreApi;

namespace MVCCore.Controllers
{

    public class DataController : BaseController
    {
        public DataController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }
        [HttpPost]
        public JsonResult ViewEncrypted(string tableId, string fieldId, string recordId)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            ReturnEncryptedField lg = Settings.RequestHandler.AskSync<ReturnEncryptedField>(new MessageViewEncryptedField()
            {
                UserIdentity = user.id,
                FieldId = fieldId,
                RecordId = recordId,
                TableId = tableId           
            });
            if ((jm = this.ValidData(lg)).Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            return Json(jm);
        }
        [HttpPost]
        public JsonResult UpdateEncrypted(string tableId, string fieldId,string recordId,string fieldvalue)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            ReturnEncryptedField lg = Settings.RequestHandler.AskSync<ReturnEncryptedField>(new MessageUpdateEncryptedField() {
                UserIdentity = user.id,
                FieldId = fieldId,
                RecordId = recordId,
                TableId = tableId,
                FieldValue = fieldvalue
            });
            if ((jm = this.ValidData(lg)).Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public JsonResult InsertOrUpdate([FromBody]JObject param)
        {
            Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());

            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            try
            {
                Dictionary<string, object> dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(temp["updateRecords"].ToString());
                Dictionary<string, object> searchterm = JsonConvert.DeserializeObject<Dictionary<string, object>>(temp["searchterm"].ToString());
                dic[ClassLib.FieldNames.id] = temp[ClassLib.FieldNames.id].ToString();         
                if (!dic.Any())
                {
                    jm.Message = "No record is updated";
                    jm.Obj = "No record is updated";
                    jm.Code = JSONCODE.Fail.ToString();
                    return Json(jm);
                }
                Dictionary<string, object> record = new Dictionary<string, object>();
                foreach (var x in dic)
                {
                    record[x.Key] = TypeCheck.CastToObject(x.Value);
                }
                string parentid = Settings.dataStructure.GetFieldIdByName(temp["tableName"].ToString(), ClassLib.FieldNames.ParentId);
                MessageFeedBack lg = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageInsertOrUpdate
                {
                    UserIdentity = user.id,
                    TableId = temp["tableName"].ToString(),
                    DataList = new List<Dictionary<string, object>>() { record },
                    ParentId = searchterm.ContainsKey(parentid) ?  searchterm[parentid].ToString() : "#"
                }, 30); 
                

                jm = this.ValidData(lg);
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }
          
            return Json(jm);
        }
        [HttpPost]
        public JsonResult UpdateOnly([FromBody]JObject param)
        {
            Dictionary<string,object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());

            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            try
            {
                Dictionary<string, object>  dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(temp["updateRecord"].ToString());               
                Dictionary<string, object> searchterm = JsonConvert.DeserializeObject < Dictionary<string, object>>(temp["searchterm"].ToString());
                Dictionary<string, object> seachFilter = new Dictionary<string, object>();

                foreach (var x in searchterm)
                {
                    List<string> values = TypeCheck.CastToList(x.Value).Where(y=> !string.IsNullOrWhiteSpace(y)).ToList();
                    if(values.Any())
                    {
                        seachFilter.Add(x.Key, values);
                    }
                   
                }
                if (!dic.Any())
                {
                    jm.Message = "No record is updated";
                    jm.Obj = "No record is updated";
                    jm.Code = JSONCODE.Fail.ToString();
                    return Json(jm);
                }
                Dictionary<string, object> record = new Dictionary<string, object>();
                foreach (var x in dic)
                {
                    record[x.Key] = TypeCheck.CastToObject(x.Value);
                }
                MessageFeedBack lg = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageUpdateOnly
                { UserIdentity = user.id, TableId = temp["tableName"].ToString(),
                    SearchTerm = searchterm,
                    Data = record
                },30);
                jm = this.ValidData(lg);                

            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }

            return Json(jm);
        }

        [HttpPost]
        public async Task<JsonResult> Delete( string tableName, List<string> idlist,bool trash,bool dontkeep=false)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            try
            {
                MessageFeedBack returnresult = await Message<MessageFeedBack>.QueryTask(Settings.RequestHandler, new MessageDeleteData() { UserIdentity = user.id, TableId = tableName, IdList = idlist});
                if ((jm = this.ValidData(returnresult)).Code != JSONCODE.Success.ToString())
                {
                    return Json(jm);
                }
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }

            return Json(jm);
        }
        [HttpPost]
        public JsonResult TemplateClick([FromBody]JObject param)
        {

            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
  
            try
            {            
                Dictionary<string, object> temp = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());
                string url = temp["url"].ToString();
                temp.Remove("url");
                //string tableId = temp["tableid"].ToString();
                //string recordid = temp["recordid"].ToString();
                //object extraparam = temp["param"];

                ServerApi.Post<Dictionary<string, object>>(url, temp, new ApiAuth(user.UserId,user.Password),60);

            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }

            return Json(jm);
        }

        //Use for drop down
        [HttpPost]
        public JsonResult Select( List<string> searchlist, List<TableSource> source,
            bool searchById = false)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            
            var _DropdownList = new List<DropDownItem>();

            if (searchlist == null || !searchlist.Any())
            {
                searchlist = new List<string>() {""};              
            }
            if(source == null)
            {
                jm.Obj = _DropdownList;
                return Json(jm);
            }
            LuceneQuery queryType;
            LuceneOccur occur;
            if (searchById)
            {
                occur = LuceneOccur.MUST;
                queryType = LuceneQuery.PhraseQuery;
            }
            else
            {
                occur = LuceneOccur.SHOULD;
                queryType = LuceneQuery.QueryParse;
            }
            Dictionary<string,string> items = MVCExt.GetItems(user.id, source, searchlist, searchById, queryType, occur);
            foreach (var item in items)
            {
                _DropdownList.Add(new DropDownItem
                {
                    id = item.Key,
                    text = item.Value

                });
            }
            jm.Obj = _DropdownList;
            return Json(jm);
        }
        [HttpPost]
        public JsonResult GetSourceText(List<string> idlist,string fieldId, bool KeySource = true)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            var _DropdownList = new List<DropDownItem>();
            LuceneQuery queryType = LuceneQuery.PhraseQuery;
            LuceneOccur occur = LuceneOccur .MUST;
            List<SelectSource> sources = KeySource ? Settings.dataStructure.FieldIdDic[fieldId].Source.KeySource.ToList()
            : Settings.dataStructure.FieldIdDic[fieldId].Source.ValueSource;
            if (sources.FirstOrDefault().GetSource() is TableSource)
            {
                try
                {
     
                   Dictionary<string, string> items = MVCExt.GetItems(user.id, sources.Select(x=> x.GetSource() as TableSource).ToList(), idlist, true, queryType, occur);
                    foreach (var item in items)
                    {
                        _DropdownList.Add(new DropDownItem
                        {
                            id = item.Key,
                            text = item.Value

                        });
                    }
                }
                catch(Exception ex)
                {

                }
                
            }
            else if(sources.FirstOrDefault().GetSource() is KeyValueSource)
            {
                KeyValueSource keyvaluesource = sources.FirstOrDefault().GetSource() as KeyValueSource;
                foreach( var item in keyvaluesource.Items)
                {
                    _DropdownList.Add(new DropDownItem
                    {
                        id = item.Key,
                        text = item.Value

                    });
                }
            }           
  
            jm.Obj = _DropdownList;
            return Json(jm);
        }

        [HttpPost]
        public JsonResult GetApiSourceText(string []id, string fieldId, bool KeySource = true,bool searchByKey  = true, string dependent ="", string dependantFieldid = "")
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            var _DropdownList = new List<DropDownItem>();
            List<SelectSource> sources = KeySource ? Settings.dataStructure.FieldIdDic[fieldId].Source.KeySource.ToList()
            : Settings.dataStructure.FieldIdDic[fieldId].Source.ValueSource;
            if (sources.FirstOrDefault().GetSource() is ApiSource)
            {
                try
                {
                    foreach(var s in sources)
                    {
                        ApiSource apisource = s.GetSource() as ApiSource;
                        string hostUrl = $"{Request.Scheme}://{Request.Host}";
                        string Url = apisource.Url;
                        string host = hostUrl;
                        if (!Url.ToLower().Contains("http"))
                        {
                            if (hostUrl.LastIndexOf("/") == hostUrl.Length - 1 && Url.IndexOf("/") == 0)
                            {
                                host = hostUrl.Remove(hostUrl.LastIndexOf("/"), 1) + Url;
                            }
                            else
                            {
                                host = hostUrl + Url;
                            }
                        }


                        //ApiSelectArgument argument = new ApiSelectArgument();
                        //argument.search = id;
                        //argument.fieldId = fieldId;
                        //argument.dependent = dependent;
                        //argument.searchbykey = searchByKey;
                  
                        ApiSelectArgument args = new ApiSelectArgument();
                        args.search = id;
                        args.searchbykey = searchByKey;
                        args.fieldId = fieldId;
                        args.dependent = dependent;
                        JsonModel jmresult = ServerApi.Post<JsonModel>(host, args, new ApiAuth() { UserName = user.UserId, Password = user.Password });
                        List<Dictionary<string, string>> temp = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JsonConvert.SerializeObject(jmresult.Obj));

                        foreach (var i in temp)
                        {
                            _DropdownList.Add(new DropDownItem() { id = i["id"].ToString(), text = i["text"].ToString() });
                        }
                    }
                }
                catch (Exception ex)
                {

                }

            }         

            jm.Obj = _DropdownList;
            return Json(jm);
        }
        [HttpPost]
        public JsonResult SelectTable()
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            string tableName = HttpContext.Request.Form["iTableName"];

            string searchtermstr = HttpContext.Request.Form["searchterm"].ToString();
            Dictionary<string, List<string>> uisearchterm;
            try
            {
                uisearchterm = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(searchtermstr);
                if (uisearchterm == null)
                {
                    uisearchterm = new Dictionary<string, List<string>>();
                }
            }
            catch(Exception ex)
            {
                uisearchterm = new Dictionary<string, List<string>>();
            }
        
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            var tabletype = HttpContext.Request.Form["iTabletype"];
            bool trash = bool.Parse(HttpContext.Request.Form["iTrash"]);
            var sSearch = HttpContext.Request.Form["sSearch"];
            var pageSize = HttpContext.Request.Form["iDisplayLength"].ToString().ToLower() == "nan"
                ? 0
                : int.Parse(HttpContext.Request.Form["iDisplayLength"]);

            var startIndex = int.Parse(HttpContext.Request.Form["iDisplayStart"]);
            var sEcho = int.Parse(HttpContext.Request.Form["sEcho"]);
            var iColumns = int.Parse(HttpContext.Request.Form["iColumns"]);


            var searchterm = uisearchterm.ToDictionary(x=> x.Key, y=> (object)y.Value);
            var _sortingDic = new Dictionary<string, string>();

            var tableHeader = new List<string>();
 
            for (var x = 0; x < iColumns; x++)
            {
                var columnName = HttpContext.Request.Form[x.ToString()];
                var valuecolumn = HttpContext.Request.Form[columnName];
                if (string.IsNullOrWhiteSpace(sSearch))
                {
                    if(!string.IsNullOrWhiteSpace(valuecolumn))
                    {
                        var columnValue = TypeCheck.CastToList(valuecolumn);

                        if (columnValue.Where(y => !string.IsNullOrWhiteSpace(y.ToString())).Any())
                        {
                            searchterm[columnName]= columnValue;
                        }
                    }
         
                }
                else
                {
                    searchterm[columnName]=new List<string>() { sSearch };
                }
                tableHeader.Add(columnName);
            }


            //Default Sorting
            var sortColumns = int.Parse(HttpContext.Request.Form["iSortingCols"]);
            for (int x = 0; x < iColumns && sortColumns == 0; x++)
            {
                if(HttpContext.Request.Form.ContainsKey("sort" + x.ToString()))
                {
                    string sortdefault = HttpContext.Request.Form["sort" + x].ToString();
                    var sortdir = HttpContext.Request.Form["sortdir" + x].ToString();
                    if (!string.IsNullOrWhiteSpace(sortdefault) && !string.IsNullOrEmpty(sortdir))
                    {
                        _sortingDic.Add(sortdefault, sortdir);
                    }

                }
            }

          
            for (var x = 0; x < sortColumns; x++)
            {
                var sortdirection = HttpContext.Request.Form["sSortDir_" + x].ToString();
                int sortCol;
                    int.TryParse(HttpContext.Request.Form["iSortCol_" + x].ToString(),out sortCol);
               
                if (tableHeader.Count > sortCol && !string.IsNullOrWhiteSpace(tableHeader[sortCol]))
                {        
                    _sortingDic[tableHeader[sortCol]] = sortdirection;
                }
              
            }
            var wm = new WebTableModel();
            try
            {
                MessageQueryReturn lg = QueryResult(user.id, tableName, searchterm, pageSize, startIndex, _sortingDic,  string.IsNullOrWhiteSpace(sSearch)? LuceneOccur.MUST : LuceneOccur.SHOULD);
                jm = this.ValidData(lg);
                if (jm.Code != JSONCODE.Success.ToString())
                {
                    return Json(jm);
                }

                var Footer = new Dictionary<string, string>();
                if (lg != null)
                {
                    wm.aaData = new List<Dictionary<string,string>>();
                    //foreach(var sort in _sortingDic)
                    //{
                    //    if(sort.Value.ToLower().Equals("asc"))
                    //    {
                    //        lg.Data = lg.Data.OrderBy(x => x[sort.Key]).ToList();
                    //    }
                    //    else
                    //    {
                    //        lg.Data = lg.Data.OrderByDescending(x => x[sort.Key]).ToList();
                    //    }
                    //}
                    
                    for (var x = 0; x < lg.Data.Count; x++)
                    {

                        var recordDic = lg.Data[x]; 
                        var record = new Dictionary<string, string>();
                        string id = recordDic[FieldNames.id].ToString();

                        //record.Add("DT_RowId" , id);
                        
                        for (int headeridx=0; headeridx < tableHeader.Count; headeridx++)
                        {
                            string headername = tableHeader[headeridx];
                            string name = tableHeader[headeridx];
                            if (!recordDic.ContainsKey(tableHeader[headeridx]))
                            {                                
                                record.Add(headername, "");
                                continue;
                            }
                            else if(tableHeader[headeridx].ToLower().Equals("button"))
                            {
                                record.Add(headername,  recordDic[tableHeader[headeridx]].ToString());
                            }
                            else
                            {
                                record.Add(headername, recordDic[tableHeader[headeridx]].ToString());
                            }
                           // record.Add(headername, $"<div class='editable' onclick=EditField(\"{recordDic[BaseField.id.ToString()]}\",\"{name}\") >{ recordDic[tableHeader[headeridx]].ToString()}</div>");
                            //string onchangeForm = "var fn=OnChangeForm; var data={id:'" + recordDic[BaseField.id.ToString()] + "',name:'" + name + "'}";

                        }
                        wm.aaData.Add(record);
                    }

                }


                wm.sEcho = sEcho + 1;
                wm.iDisplayStart = lg.StartIndex;
                wm.iTotalRecords = lg.Total;
                wm.iTotalDisplayRecords = lg.Total;
                wm.deferLoading = lg.Total;
                var _obj = new Dictionary<string, object>();
                _obj.Add("aaData", wm);
                _obj.Add("Footer", Footer);
                jm.Obj = _obj;
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }
            return Json(jm);
        }

        public JsonResult SelectTableTx()
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            string tableName = HttpContext.Request.Form["iTableName"];

            string searchtermstr = HttpContext.Request.Form["searchterm"].ToString();
            Dictionary<string, List<string>> uisearchterm;
            try
            {
                uisearchterm = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(searchtermstr);
                if(uisearchterm == null)
                {
                    uisearchterm = new Dictionary<string, List<string>>();
                }
            }
            catch (Exception ex)
            {
                uisearchterm = new Dictionary<string, List<string>>();
            }

            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            var tabletype = HttpContext.Request.Form["iTabletype"];
            bool trash = bool.Parse(HttpContext.Request.Form["iTrash"]);
            var sSearch = HttpContext.Request.Form["sSearch"];
            var pageSize = HttpContext.Request.Form["iDisplayLength"].ToString().ToLower() == "nan"
                ? 0
                : int.Parse(HttpContext.Request.Form["iDisplayLength"]);

            var startIndex = int.Parse(HttpContext.Request.Form["iDisplayStart"]);
            var sEcho = int.Parse(HttpContext.Request.Form["sEcho"]);
            var iColumns = int.Parse(HttpContext.Request.Form["iColumns"]);


            var searchterm = uisearchterm.ToDictionary(x => x.Key, y => (object)y.Value);
            var _sortingDic = new Dictionary<string, string>();

            var tableHeader = new List<string>();

            for (var x = 0; x < iColumns; x++)
            {
                var columnName = HttpContext.Request.Form[x.ToString()];
                var valuecolumn = HttpContext.Request.Form[columnName];
                if (string.IsNullOrWhiteSpace(sSearch))
                {
                    if (!string.IsNullOrWhiteSpace(valuecolumn))
                    {
                        var columnValue = TypeCheck.CastToList(valuecolumn);

                        if (columnValue.Where(y => !string.IsNullOrWhiteSpace(y.ToString())).Any())
                        {
                            searchterm[columnName] = columnValue;
                        }
                    }

                }
                else
                {
                    searchterm[columnName] = new List<string>() { sSearch };
                }
                tableHeader.Add(columnName);
            }


            //Default Sorting
            for (int x = 0; x < iColumns; x++)
            {
                if (HttpContext.Request.Form.ContainsKey("sort" + x.ToString()))
                {
                    string sortdefault = HttpContext.Request.Form["sort" + x].ToString();
                    var sortdir = HttpContext.Request.Form["sortdir" + x].ToString();
                    if (!string.IsNullOrWhiteSpace(sortdefault) && !string.IsNullOrEmpty(sortdir))
                    {
                        _sortingDic.Add(sortdefault, sortdir);
                    }

                }
            }
          
            int sortColumns;
            int.TryParse(HttpContext.Request.Form["iSortingCols"].ToString(),out sortColumns);
            for (var x = 0; x < sortColumns; x++)
            {
                var sortdirection = HttpContext.Request.Form["sSortDir_" + x];
                var sortCol = HttpContext.Request.Form["iSortCol_" + x];

                if (!string.IsNullOrWhiteSpace(sortCol))
                {
                    _sortingDic.Add(HttpContext.Request.Form[sortCol], sortdirection);
                }

            }
            var wm = new WebTableModel();
            try
            {

                MessageReturnHistoryItem lg = Message<MessageReturnHistoryItem>.Query(Settings.RequestHandler, new MessageQueryHistoryItem()
                {
                    UserIdentity = user.id,
                    TableId = tableName,
                    SearchTerm = searchterm,
                    PageSize = pageSize,
                    StartIndex = startIndex,
                    SortTerm = _sortingDic,
                    QueryType = LuceneQuery.QueryParse,
                    Trash = trash,
                    HostUrl = $"{Request.Scheme}://{Request.Host}"
                },60);


                if (jm.Code != JSONCODE.Success.ToString())
                {
                    return Json(jm);
                }

                var Footer = new Dictionary<string, string>();
                if (lg != null)
                {
                    wm.aaData = new List<Dictionary<string, string>>();


                    for (var x = 0; x < lg.Data.Count; x++)
                    {

                        var recordDic = lg.Data[x];

                        
                        var record = new Dictionary<string, string>();
                        record.Add("DT_RowId", recordDic[FieldNames.id].ToString());
                        for (int headeridx = 0; headeridx < tableHeader.Count; headeridx++)
                        {
                            string name = tableHeader[headeridx] + recordDic[FieldNames.id];
                            string headername = tableHeader[headeridx];                          
                            record.Add(headername, recordDic[tableHeader[headeridx]].ToString());

                        }
                        wm.aaData.Add(record);
                    }

                }


                wm.sEcho = sEcho + 1;
                wm.iDisplayStart = lg.StartIndex;
                wm.iTotalRecords = lg.Total;
                wm.iTotalDisplayRecords = lg.PageSize;
                wm.deferLoading = lg.Total;
                var _obj = new Dictionary<string, object>();
                _obj.Add("aaData", wm);
                _obj.Add("Footer", Footer);
                jm.Obj = _obj;
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }
            return Json(jm);
        }
       



     

 

        public async Task<JsonResult> DownloadPlugins( List<string> tables)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            MessageReturnDownloadPlugin lg = await Message<MessageReturnDownloadPlugin>.QueryTask(Settings.RequestHandler, new MessageDownloadPlugin() { UserIdentity = user.id, TableNames = tables});
            jm = this.ValidData(lg);
            return Json(jm);
        }

        [HttpGet]
        public ActionResult Download(string file)
        {
            //https://www.codeproject.com/Tips/1156485/How-to-Create-and-Download-File-with-Ajax-in-ASP-N
            //get the temp folder and file path in server
            //string fullPath = Path.Combine(Server.MapPath("~/temp"), file);
            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(file, "application/vnd.ms-excel", Path.GetFileName(file));
        }

        [HttpPost]
        public JsonResult ViewUploadPlugins(List<IFormFile> files)
        {
            JsonModel jm = new JsonModel();
            List<InputModel> pluginmodel = new List<InputModel>();
            try
            {
                foreach (IFormFile file in files)
                {
                    var fileContent = file.FileName;
                    if (fileContent != null && fileContent.Length > 0)
                    {
                        // get a stream

                        var stream = file.OpenReadStream();
                        byte[] buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        var bytesAsString = Encoding.ASCII.GetString(buffer);
                        Dictionary<string, Plugins> plugins = JsonConvert.DeserializeObject<Dictionary<string, Plugins>>(bytesAsString);
                        pluginmodel.Add(MVCExt.ControlCheckBox("", "", false, "var fn=OnToggleAllPlugin;var data={};", "", "", false));
                        foreach (var plugin in plugins)
                        {
                            pluginmodel.Add(MVCExt.ControlCheckBox(plugin.Key, plugin.Value.TableText, false, "", "", "", false));
                        }
                        PluginModel pluginimport = new PluginModel();
                        pluginimport.Plugins = pluginmodel;
                        pluginimport.PluginsData = bytesAsString;
                        pluginimport.Onclick = "UploadPlugins(this)";
                        pluginimport.Url = Url.Action("UploadPlugins", "Data");
                        pluginimport.ClassName = "fas fa-upload";
                        jm.Obj = this._viewRenderService.RenderToString("PluginsViewPartial", pluginimport);
                        jm.Code = JSONCODE.Success.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }

            return Json(jm);
        }


        [HttpPost]
        public JsonResult DownloadCsv(string tableId)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            JsonModel jm;
     
            if(Settings.ds.TableToFieldDic.ContainsKey(tableId))
            {
                jm = new JsonModel();
                DownloadCsvViewModel csvDownloadModel = new DownloadCsvViewModel();       
                List<TableViewField> tableViews =  Settings.ds.GetTableViewFields(user, tableId);
                csvDownloadModel.TableIdToFieldView[tableId] = tableViews.Where(x=> x.AllowView).ToList();
                csvDownloadModel.TableIdToNameDic[tableId] = Settings.ds.TableIdDic[tableId].Name;
                List<string> subtables = Settings.ds.GetSubTableId(tableId);
                foreach(var table in subtables)
                {
                    List<TableViewField> subtableViews = Settings.ds.GetTableViewFields(user, table);
                    csvDownloadModel.TableIdToFieldView[table] = subtableViews.Where(x => x.AllowView).ToList();
                    csvDownloadModel.TableIdToNameDic[table] = Settings.ds.TableIdDic[table].Name;
                }
                jm.Code = JSONCODE.Success.ToString();
                jm.Obj = this._viewRenderService.RenderToString("CSVDownloadViewPartial", csvDownloadModel);
            }
            else
            {
                return Json(new JsonModel() {  Code = JSONCODE.Fail.ToString(), Message = "Invalid Table Id"});
            }
      
            if (jm.Code == JSONCODE.Fail.ToString())
            {
                return Json(jm);
            }        
            return Json(jm);
        }

        [HttpPost]
        public JsonResult DownloadSelectedFieldCsv(string[] columnsId)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            JsonModel jm;
            ReturnMessage result = Settings.RequestHandler.AskSync<ReturnMessage>(new MessageDownloadCSV() {
                FieldList =columnsId.ToList(),UserIdentity=user.id, SearchTerm = new Dictionary<string, object>(), SortTerm = new Dictionary<string, string>(),
                HostUrl = $"{Request.Scheme}://{Request.Host}",
                StatusSender = Settings.WebServerHandler
            });
            jm = this.ValidData(result);
            if (jm.Code == JSONCODE.Fail.ToString())
            {
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public  JsonResult Commit(string tableName)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            JsonModel jm = new JsonModel();

            MessageFeedBack result = Settings.RequestHandler.AskSync<MessageFeedBack>(new CommitTable() { TableName = tableName });
            jm.Obj = result.Message;
            jm = this.ValidData(result);    
            return Json(jm);
        }
        [HttpPost]
        public JsonResult AutoComplete(string search, string table,string field,ulong rows)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            JsonModel jm = new JsonModel();
            List<Dictionary<string, object>> temp = Settings.RequestHandler.AskSync<List<Dictionary<string, object>>>(new AutoCompleteQuery() { TableName= table, FieldName= field, Text= search, UserIdentity= user.id, Rows= rows });
            List<string> result = new List<string>();
            foreach(var x in temp)
            {
                result.Add(x[field].ToString());
            }

            return Json(result);

        }

        public string GetWords(string keyWord, bool noregexclean = false)
        {
            Users user = this.HttpContext.Session.Get<Users>(SessionName.User);
            if (string.IsNullOrWhiteSpace(keyWord) || user == null)
            {
                return "";
            }
            return MVCExt.KeyWords.GetWord(user.Language, keyWord, noregexclean);
        }

        

    }


}