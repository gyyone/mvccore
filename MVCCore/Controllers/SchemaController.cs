﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models;
using MVCCore.Models.Public;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Core;
using Core.Akka;
using Core.Akka.Query;
using Util;
using ClassLib;
using System.IO;
using FileInfo = Util.FileInfo;
using SolrNetCore;
using System.Net.Mime;
using System.Net.Http.Headers;
using Microsoft.Net.Http.Headers;
using ContentDispositionHeaderValue = Microsoft.Net.Http.Headers.ContentDispositionHeaderValue;
using System.IO.Compression;
using FieldProperty = TableProperty.FieldProperty;
using JsonHelper;
using DirectoryHelper;
using ClassExtension;
using Akka.Actor;

namespace MVCCore.Controllers
{
    public class SchemaController : BaseController
    {
        public SchemaController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }

        public IActionResult Index()
        {
            return View("Settings", HttpContext.Session.Get<Users>(SessionName.User));
        }

        [HttpPost]
        public JsonResult GetSchema2(Dictionary<string, string> search)
        {
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                List<Dictionary<string, string>> tree = Settings.RequestHandler.AskSync<List<Dictionary<string, string>>>(new QuerySchema() { SearchValue = search, UserIdentity = HttpContext.Session.Get<Users>(SessionName.User).id }, 600);
                return Json(tree);
            }
            return Json(new List<Dictionary<string, string>>());
        }
        [HttpPost]
        public JsonResult GetSchema(Dictionary<string, string> search)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                List<TreeNode> tree = Settings.RequestHandler.AskSync<List<TreeNode>>(new QuerySchema() { SearchValue = search, UserIdentity = user.id}, 600);
                if (search.ContainsKey("str"))
                {
                    return Json(tree.Select(x => x.id).ToList());
                }
                else if (search.ContainsKey("ParentId"))
                {
                    return Json(tree);
                }
            }
            return Json(new List<TreeNode>());
        }
        [HttpPost]
        public JsonResult GetAllTables(string name)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            Dictionary<string, string> search = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(name))
            {
                name = "";
            }
            if (jm.Code == JSONCODE.Success.ToString())
            {
                search = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryTables() { SearchName = name, UserIdentity = user.id }, 60);
            }
            return Json(search.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList());
        }


        [HttpPost]
        public JsonResult GetAllFields(string name, string dependent, bool tags, bool unique)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);

            Dictionary<string, string> search = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(name))
            {
                name = "";
            }
            if (jm.Code == JSONCODE.Success.ToString())
            {
                search = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryFields() { TableId = dependent, SeachName = name, UserIdentity = user.id, Unique = unique });
            }
            if (tags)
            {
                Dictionary<string, string> temp = new Dictionary<string, string>();
                name = string.IsNullOrEmpty(name) ? " " : name;
                if(!search.ContainsValue(name))
                {
                    temp.Add(name, name);
                }              

                foreach (var x in search)
                {
                    temp[x.Key] = x.Value;
                }
                search = temp;
            }

            if (search == null)
            {
                return Json(new List<Dictionary<string, string>>());
            }
            return Json(search.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList());
        }

        [HttpPost]
        public JsonResult Reorder(string tableId, string parentId)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);   
            if (jm.Code == JSONCODE.Success.ToString())
            {


                TableFieldsModel tableFields = new TableFieldsModel();
                switch (tableId)
                {
                    case MenuProperty.ClassID:
                        tableFields.FieldList = Settings.ds.MenuIdDic.Where(x => x.Value.ParentId == parentId).OrderBy(x => x.Value.RowNo).Select(x => new TableFieldProperty() { id = x.Value.id, Name = x.Value.Name, Visible = x.Value.HasAccess(user.UserGroups, user.id) }).ToList();
                        break;
                    case TableProperty.ClassID:
                        tableFields.FieldList = Settings.ds.TableIdDic.Where(x => x.Value.ParentId == parentId).OrderBy(x => x.Value.RowNo).Select(x => new TableFieldProperty() { id = x.Value.id, Name = x.Value.Name, Visible = x.Value.HasAccess(user.UserGroups, user.id) }).ToList();

                        break;
                    case TableProperty.FieldProperty.ClassID:
                        tableFields.FieldList = Settings.ds.FieldIdDic.Where(x => x.Value.ParentId == parentId).OrderBy(x => x.Value.RowNo).Select(x => new TableFieldProperty() { id = x.Value.id, Name = x.Value.Name, Visible = x.Value.HasAccess(user.UserGroups, user.id) }).ToList();

                        break;
                }

                tableFields.ParentId = parentId;
                tableFields.TableId = tableId;
                tableFields.UpdateSortUrl = "/Schema/UpdateOrder";
                jm.Obj = this._viewRenderService.RenderToString("SortFieldViewPartial", tableFields);
            }


            return Json(jm);
        }

        [HttpPost]
        public JsonResult UpdateOrder(string tableId, string parentId, List<string> orderFields)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                MessageInsertOrUpdate update = new MessageInsertOrUpdate();
                update.DataList = new List<Dictionary<string, object>>();
                update.TableId = tableId;
                update.UserIdentity = user.id;
                update.UpdateOrderOnly = true;
                int seqNumber = 0;       
                foreach (var field in orderFields)
                {
                    seqNumber++;
                    Dictionary<string, object> updateValue = new Dictionary<string, object>();
                    updateValue[Settings.ds.GetFieldIdByName(tableId, ClassLib.FieldNames.RowNo)] = seqNumber;
                    updateValue[Settings.ds.GetFieldIdByName(tableId, ClassLib.FieldNames.ParentId)] = parentId;
                    updateValue["id"] = field;
                    update.DataList.Add(updateValue);                  
                }
                MessageFeedBack m = Settings.RequestHandler.AskSync<MessageFeedBack>(update, 120);

                jm = this.ValidData(m);
          
            }
            return Json(jm);

        }
        [HttpPost]
        public JsonResult SaveTableView(string tableId, Dictionary<string, bool> orderFields)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
          
            if (jm.Code == JSONCODE.Success.ToString())
            {
                MessageInsertOrUpdate update = new MessageInsertOrUpdate();
                update.DataList = new List<Dictionary<string, object>>();
                update.TableId = TableViewSetting.ClassID;
                update.UserIdentity = user.id;
                int seqNumber = 0;
                foreach (var field in orderFields)
                {
                    seqNumber++;
                    Dictionary<string, object> updateValue = new Dictionary<string, object>();
                    updateValue[ClassLib.FieldNames.id] = field.Key;
                    updateValue[TableViewSetting.FieldsId.RowNo] = seqNumber;
                    updateValue[TableViewSetting.FieldsId.Enable] = field.Value;
                    update.DataList.Add(updateValue);
                }

                MessageFeedBack m = Settings.RequestHandler.AskSync<MessageFeedBack>(update, 60);

                jm = this.ValidData(m);

            }
            return Json(jm);

        }
        [HttpPost]
        public JsonResult SelectTable()
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            string tableName = HttpContext.Request.Form["iTableName"];

            string parentid = HttpContext.Request.Form[FieldNames.ParentId];
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            var tabletype = HttpContext.Request.Form["iTabletype"];
            bool trash = bool.Parse(HttpContext.Request.Form["iTrash"]);
            var sSearch = HttpContext.Request.Form["sSearch"];
            var pageSize = HttpContext.Request.Form["iDisplayLength"].ToString().ToLower() == "nan"
                ? 0
                : int.Parse(HttpContext.Request.Form["iDisplayLength"]);
            var startIndex = int.Parse(HttpContext.Request.Form["iDisplayStart"]);
            var sEcho = int.Parse(HttpContext.Request.Form["sEcho"]);
            var iColumns = int.Parse(HttpContext.Request.Form["iColumns"]);


            var searchterm = new Dictionary<string, object>();
            var _sortingDic = new Dictionary<string, string>();

            var tableHeader = new List<string>();
            for (var x = 0; x < iColumns; x++)
            {
                var columnName = HttpContext.Request.Form[x.ToString()];
                var valuecolumn = HttpContext.Request.Form[columnName];
                if (string.IsNullOrWhiteSpace(sSearch))
                {
                    if (!string.IsNullOrWhiteSpace(valuecolumn))
                    {
                        var columnValue = JsonConvert.DeserializeObject<List<object>>(valuecolumn);

                        if (columnValue.Where(y => !string.IsNullOrWhiteSpace(y.ToString())).Any())
                        {
                            searchterm.Add(columnName, columnValue);
                        }
                    }
                }
                else
                {
                    searchterm.Add(columnName, new List<string>() { sSearch });
                }
                tableHeader.Add(columnName);
            }
            if (!string.IsNullOrEmpty(parentid) && !searchterm.ContainsKey(FieldNames.ParentId))
            {
                searchterm[FieldNames.ParentId] = JsonConvert.DeserializeObject<List<string>>(parentid);
            }
            var sortColumns = int.Parse(HttpContext.Request.Form["iSortingCols"]);
            for (var x = 0; x < sortColumns; x++)
            {
                var sortdirection = HttpContext.Request.Form["sSortDir_" + x];
                var sortCol = HttpContext.Request.Form["iSortCol_" + x];

                if (!string.IsNullOrWhiteSpace(sortCol))
                {
                    _sortingDic.Add(HttpContext.Request.Form[sortCol], sortdirection);
                }
            }
            var wm = new WebTableModel();
            try
            {
                // = QueryResult(tableName, searchterm, pageSize, startIndex, _sortingDic, trash, jm, string.IsNullOrWhiteSpace(sSearch) ? LuceneOccur.MUST : LuceneOccur.SHOULD);
                MessageQueryReturn lg = Settings.RequestHandler.AskSync<MessageQueryReturn>(new MessageQuery()
                {
                    TableId = tableName,
                    SearchTerm = searchterm,
                    PageSize = pageSize,
                    StartIndex = startIndex,
                    SortTerm = _sortingDic,
                    QueryOccur = LuceneOccur.SHOULD,
                    QueryType = LuceneQuery.QueryParse,
                    HostUrl = $"{Request.Scheme}://{Request.Host}"
                });

                if (jm.Code != JSONCODE.Success.ToString())
                {
                    return Json(jm);
                }

                var Footer = new Dictionary<string, string>();
                if (lg != null)
                {
                    wm.aaData = new List<Dictionary<string, string>>();
                    for (var x = 0; x < lg.Data.Count; x++)
                    {

                        var recordDic = lg.Data[x];
                        var sbhtml = new StringBuilder();

                        recordDic.Add(FieldNames.Button, sbhtml.ToString());
                        recordDic.Add(FieldNames.RowNo, (x + 1).ToString());
                        var record = new Dictionary<string, string>();
                        string id = recordDic[FieldNames.id].ToString();

                        record.Add("DT_RowId", id);

                        for (int headeridx = 0; headeridx < tableHeader.Count; headeridx++)
                        {
                            string headername = tableHeader[headeridx];
                            string name = tableHeader[headeridx];
                            if (!recordDic.ContainsKey(tableHeader[headeridx]))
                            {
                                record.Add(headername, "");
                                continue;
                            }

                            else
                            {
                                record.Add(headername, $"<div class='editable' onclick=EditField(\"{recordDic[FieldNames.id]}\",\"{name}\") >{ recordDic[tableHeader[headeridx]].ToString()}</div>");
                            }

                            string onchangeForm = "var fn=OnChangeForm; var data={id:'" + recordDic[FieldNames.id] + "',name:'" + name + "'}";

                        }
                        wm.aaData.Add(record);
                    }

                }


                wm.sEcho = sEcho + 1;
                wm.iDisplayStart = lg.StartIndex;
                wm.iTotalRecords = lg.Total;
                wm.iTotalDisplayRecords = lg.Total;
                wm.deferLoading = lg.Total;
                var _obj = new Dictionary<string, object>();
                _obj.Add("aaData", wm);
                _obj.Add("Footer", Footer);
                jm.Obj = _obj;
            }
            catch (Exception ex)
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = ex.ToString();
            }
            return Json(jm);
        }

      
        [HttpPost]
        public JsonResult FreeText(string search, string tablename, string recordid, bool searchbykey)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Dictionary<string, string> searchterm = new Dictionary<string, string>();
            search = search ?? "";
            if (!searchterm.ContainsKey(search))
            {
                searchterm.Add(search, search);
            }

            jm.Obj = searchterm.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList();
            return Json(jm);
        }
     
   



        [HttpPost]
        public JsonResult GetAvailableLookupFields([FromBody]ApiSelectArgument args,string tableId="")
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }


            string host = $"{Request.Scheme}://{Request.Host}";
            Dictionary<string, string> searchterm = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new MessageGetFieldFilterLookup() { TableId = tableId, HostUrl = host, SeachName = args.search, Searchbykey = args.searchbykey, UserIdentity = user.id }, 300);

            jm.Obj = searchterm.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList();
            return Json(jm);
        }

        [HttpPost]
        public JsonResult GetAvailableValue([FromBody]ApiSelectArgument args,string tableId = "",string fieldIdofSelect = "key")
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");
            string selectedfieldid = !dependentdic.ContainsKey(fieldIdofSelect) ? "" : dependentdic[fieldIdofSelect].FirstOrDefault();   
            string host = $"{Request.Scheme}://{Request.Host}";
            Dictionary<string, string> searchterm = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new MessageGetFieldFilterLookupValue() { TableId = tableId, SelectedFieldId = selectedfieldid, HostUrl = host, SeachName = args.search, Searchbykey = args.searchbykey, UserIdentity = user.id }, 60);

            jm.Obj = searchterm.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList();
            return Json(jm);
        }


        [HttpPost]
        public JsonResult SelectTableViewFields([FromBody]ApiSelectArgument args)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);

            Dictionary<string, string> searchdic = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(args.dependent))
            {
                args.dependent = "{}";
            }
            Dictionary<string, object> forms = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.dependent);


            if (!forms.Any() || !forms.ContainsKey(TableViewSetting.FieldsId.TableId))
            {
                
                return Json(args.search.Select(x=> new Dictionary<string, string>() { {x,x } }).ToList());
            }
            else if (forms.ContainsKey(TableViewSetting.FieldsId.TableId))
            {
                string selectedTable = JsonConvert.DeserializeObject<List<string>>(forms[TableViewSetting.FieldsId.TableId].ToString()).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(selectedTable))
                {
                    if (jm.Code == JSONCODE.Success.ToString())
                    {
                        searchdic = Settings.RequestHandler.AskSync<Dictionary<string, string>>(new QueryFields() { TableId = selectedTable, SeachName = args.search.FirstOrDefault(), UserIdentity = user.id });
                    }

                    return Json(searchdic.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList());
                }

            }

            return Json(new List<Dictionary<string, string>>() { new Dictionary<string, string>() { { "", "No Table Id" } } });
        }

        [HttpPost]
        public JsonResult AddTable(Dictionary<string, string> selectedTable)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new AddTable() { SelectedTable = new TableProperty() { id = selectedTable["id"], ParentId = selectedTable["ParentId"] }, UserIdentity = user.id, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }


        [HttpPost]
        public JsonResult AddTableField(Dictionary<string, string> selectedTable)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new AddTableField() { Selected = new BaseProperty() { id = selectedTable["id"], ParentId = selectedTable["ParentId"] }, UserIdentity = user.id, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public JsonResult DeleteTable(Dictionary<string, string> selectedTable, bool deletedata)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new DeleteTable() { Table = new TableProperty() { id = selectedTable["id"], ParentId = selectedTable["ParentId"] }, UserIdentity = user.id, DeleteData = deletedata, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public JsonResult DeleteField(Dictionary<string, string> selectedfield, bool deletedata)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new DeleteField() { Field = new FieldProperty() { id = selectedfield["id"], ParentId = selectedfield["ParentId"] }, UserIdentity = user.id, DeleteData = deletedata, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public JsonResult CopyTable(string targetCopyId, string targetDestId)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);       

            IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new CopyTableTo()
            {
                TargetCopyTable = targetCopyId,
                ToTable = targetDestId,
                UserIdentity = user.id,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);
            return Json(jm);

        }

        [HttpPost]
        public JsonResult CopyField(string targetCopyFieldId, string targetDestId)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);    

            IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new CopyFieldTo()
            {
                TargetCopyField = targetCopyFieldId,
                ToId = targetDestId,
                UserIdentity = user.id,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);
            return Json(jm);

        }

        [HttpPost]
        public JsonResult BackupData(string[] SchemaIdlist)
        {
            // return File(System.Text.Encoding.Unicode.GetBytes("ABCD"), "APPLICATION/octet-stream", "Schema.json");

            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);  

            BackupResponse message = Settings.RequestHandler.AskSync<BackupResponse>(new MessageBackupScema()
            {
                SchemaIds = SchemaIdlist.ToList(),
                UserIdentity = user.id
            }, 600);
            jm = this.ValidData(message);

            if (jm.Code == JSONCODE.Fail.ToString())
            {
                return Json(jm);
            }
            int IndexFolder = 0;
            string SelectedFolder = "";
            List<FileInfo> fileinfoslist = new List<FileInfo>();

            foreach (string folder in Settings.ds.UploadedFolders)
            {
                long freespace = DirHelper.FreeSpace(folder);
                if (freespace > Settings.ds.MinSpaceRequired)
                {
                    string fullpath = Path.GetFullPath(folder);
                    SelectedFolder = fullpath;
                    break;

                }
                IndexFolder++;

            }

            if (!string.IsNullOrWhiteSpace(SelectedFolder) && DirHelper.FreeSpace(SelectedFolder) >= Settings.ds.MinSpaceRequired)
            {
                Backup databackup = new Backup();
               
                databackup.id = Unique.NewGuid();
                databackup.DateVersion = DateTime.Now;
                databackup.Name = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

              
                string newpathfile = $"{SelectedFolder}/{Backup.FieldsId.Files}/{databackup.id}/schema.zip";
                DirHelper.CreateDirectory(newpathfile);
                System.IO.File.WriteAllBytes(newpathfile, message.Data);

                databackup.BackupLocation = newpathfile;    
                StringBuilder sb = new StringBuilder();
                sb.Append("<div style='height:10vh;overflow:auto;' onclick=\"ShowModal('Description',$(this).html())\" >");
                foreach (var tableid in message.TableIdToNameDic.Where(x => SchemaIdlist.Contains(x.Key)))
                {
                    sb.AppendLine($"<div class='row' ><ul><li><h4>{tableid.Value}</h4><ul>");
                    foreach (var fieldid in message.TableToFieldNameDic[tableid.Key])
                    {
                        sb.AppendLine($"<li>{fieldid}</li>");
                    }
                    sb.AppendLine($"</ul></li></ul></div>");
                }
                sb.Append("</div>");
                databackup.Description = sb.ToString();

                MessageFeedBack lg = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageInsertOrUpdate
                {
                    UserIdentity = user.id,
                    TableId = Backup.ClassID ,
                    DataList = new List<Dictionary<string, object>>() { databackup.ToDictionary(true) }
                }, 3000); ;

                jm = base.ValidData(lg);

            }
            else
            {
                jm.Message = "Not enough space to upload. Min Sqace required " + Settings.ds.MinSpaceRequired / 1000000 + " MB";
                jm.Code = JSONCODE.Fail.ToString();
                return Json(jm);
            }



            return Json(jm);

        }

        [HttpPost]
        public JsonResult Restore(string tableid, string recordid, Dictionary<string, object> param)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            ReturnMessage message = Settings.RequestHandler.AskSync<ReturnMessage>(new MessageRestore()
            {
                TableID = tableid,
                RecordId = recordid,
                UserIdentity = user.id,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);

            return Json(jm);
        }

        [HttpPost]
        public JsonResult ClearData(string tableid)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            ReturnMessage message = Settings.RequestHandler.AskSync<ReturnMessage>(new ClearData()
            {
                TableId = tableid,
                UserIdentity = user.id,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);

            return Json(jm);
        }

        [HttpPost]
        public JsonResult RebuildSchema(string[] tableidlist)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            ReturnMessage message = Settings.RequestHandler.AskSync<ReturnMessage>(new RebuildSchema()
            {
                TableIdList = tableidlist,
                UserIdentity = user.id,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);

            return Json(jm);
        }

        [HttpPost]
        public JsonResult DownloadClasses(string[] SchemaIdlist)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            StringBuilder sbtext = new StringBuilder();
            Template t = new Template();
            string templateid = Template.ClassID;
            string templateName = TypeCheck.GetJsonPropertyName(t, nameof(t.Name));
            ResponseDetail details = Settings.ds.Query(templateid, new Dictionary<string, object>(), strict: false);
            List<Template> templateList = details.docs.Select(x => x.CastToObject<Template>()).ToList();
            Template tablepropertytemplate = templateList.FirstOrDefault(x => x.Name.ToString().Equals("Class Header"));
            List<string> templatelist = new List<string>();
            string temppath = AppDomain.CurrentDomain.BaseDirectory + "temp/" + Unique.NewGuid() + "/";           
            string classpath = $"{temppath}classes";
            DirHelper.CreateDirectory(classpath+ "/");
            List<string> ToptableidList = SchemaIdlist.Where(x => Settings.ds.TableIdDic[x].ParentId == "#").ToList();
            byte[] fileoutput = new byte[0];
            string fileName = ToptableidList.Count == 1 ? Settings.ds.TableIdDic[ToptableidList.FirstOrDefault()].Name + ".cs" : "classes.zip";
            
            if (ToptableidList.Count == 1)
            {
                string tableid = ToptableidList.FirstOrDefault();
                string tableName = Settings.ds.TableIdDic[tableid].GetCleanName();
                fileName = tableName + ".cs";
                string templatetext = tablepropertytemplate.GetTemplateText(user,Settings.dataStructure, Settings.ds.QueryById(tablepropertytemplate.ForTable, tableid).docs.FirstOrDefault());
                fileoutput = Encoding.Unicode.GetBytes(templatetext);

                //Enable for trouble shooting
                //using (StreamWriter sw = new StreamWriter($"{classpath}/{tableName}.cs"))
                //{
                //    sw.Write(GetTemplateStr(templateList, tablepropertytemplate, Settings.ds.QueryById(tablepropertytemplate.ForTable, tableid).docs.FirstOrDefault()));
                //}

            }
            else
            {
                foreach (var tableid in ToptableidList)
                {
                    string tableName = Settings.ds.TableIdDic[tableid].GetCleanName();
                    using (StreamWriter sw = new StreamWriter($"{classpath}/{tableName}.cs"))
                    {              
                        fileName = tableName + ".cs";
                        string templatetext = tablepropertytemplate.GetTemplateText(user, Settings.dataStructure, Settings.ds.QueryById(tablepropertytemplate.ForTable, tableid).docs.FirstOrDefault());
                        sw.Write(templatetext);
                    }
                }
                string zippath = $"{temppath}/classes.zip";
                ZipFile.CreateFromDirectory(classpath, zippath);
                fileoutput = System.IO.File.ReadAllBytes(zippath);
                fileName = "classes.zip";
            }    
            System.IO.Directory.Delete(temppath, true);          

            jm.Obj = new Dictionary<string, object>() { { "File", fileoutput.Select(x=> (int)x).ToArray() },{"FileName", fileName } };
            return Json(jm);
        }

        [HttpPost]
        public JsonResult RequestLock(string tableId,bool islocked)
        {
            var jm = new JsonModel();
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            if (Settings.dataStructure.TableIdDic.ContainsKey(tableId) && Settings.dataStructure.TableIdDic[tableId].HasAccess(user.UserGroups, user.id))
            {

                MessageFeedBack message = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageInsertOrUpdate() { 
                    
                    ParentId = Settings.dataStructure.TableIdDic[tableId].ParentId,
                    DataList = new List<Dictionary<string, object>>() {new Dictionary<string, object>(){{ TableProperty.FieldsId.Lock, islocked },{FieldNames.id, tableId } } },                    UpdateOrderOnly = true,
                    UserIdentity = user.id,
                    TableId = TableProperty.ClassID,
                    
                
                });
                jm = this.ValidData(message);
            }
            else
            {
                jm.Code = JSONCODE.Fail.ToString();
                jm.Message = "Access Denial: You does not have permission to change lock state.";
            }
       
            return Json(jm);
        }
    }

}