﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using ClassLib;
using Core.Akka;
using CoreApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCore.Models.Public;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Util;
using ClassExtension;
namespace MVCCore.Controllers.Api
{

    [Route("api/[controller]")] 
    [ApiController]
    [MVCCore.Filter.AuthorizeApiFilter]
    public class QueryController : Controller
    {
        [HttpPost("Select")]
        public JsonResult Select([FromBody]QueryItem Qitem)
        {
            Users user = this.Request.HttpContext.Items[SessionName.User] as Users;
            if(!Settings.dataStructure.TableIdDic.ContainsKey(Qitem.TableId) )
            {
                return Json($"Invalid TableId {Qitem.TableId ?? ""}");
            }
  
            string tableid = Qitem.TableId;
            MessageQueryReturn lg = Settings.RequestHandler.AskSync<MessageQueryReturn>(new MessageQuery
            {
                UserIdentity = user.id,
                TableId = tableid,
                SearchTerm = Qitem.SearchTerm.ToDictionary(x=> x.Key, y=> TypeCheck.CastToObject(y.Value)),
                PageSize = Qitem.Limit,
                StartIndex = Qitem.Start,
                SortTerm = Qitem.Sort,
                QueryType = Core.LuceneQuery.QueryParse,
                QueryOccur = Core.LuceneOccur.SHOULD,
                RawData = true,
                Columns = Qitem.Columns ,
                HostUrl = Settings.cfg.HostUrl
            }, Qitem.TimeOut);
            if(lg == null)
            {
                return Json(new QueryResult() { Error = true, Message = "Request Timeout"});
            }
          
            return Json(new QueryResult() { Items= lg.Data,Message = lg.Message, Start= Qitem.Start, TotalFound = lg.Total ,Error = lg.Error});
        }


        [HttpPost("Delete")]
        public JsonResult Delete([FromBody]DeleteItem Ditem)
        {
            Users user = this.Request.HttpContext.Items[SessionName.User] as Users;

            MessageFeedBack lg = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageDeleteData
            {
                UserIdentity = user.id,
                TableId = Ditem.TableId,
                IdList = Ditem.IdList
            }, Ditem.TimeOut);
            if (lg == null)
            {
                return Json(new QueryResult() { Error = true, Message = "Request Timeout" });
            }

            return Json(new DeleteResult() { Message = lg.Message, Error = lg.Error });
        }
        [HttpPost("InsertOrUpdate")]
        public JsonResult InsertOrUpdate([FromBody] InsertOrUpdateItem iuItem)
        {
            Users user = this.Request.HttpContext.Items[SessionName.User] as Users;
            string ParentId = Settings.dataStructure.GetFieldIdByName(iuItem.TableId, FieldNames.ParentId);
            MessageFeedBack lg = Settings.RequestHandler.AskSync<MessageFeedBack>(new MessageInsertOrUpdate
            {
                UserIdentity = user.id,
                TableId = iuItem.TableId,
                DataList = iuItem.updateItems.Select(z=> z.ToDictionary(x => x.Key, y => TypeCheck.CastToObject(y.Value))).ToList(),
                ParentId = iuItem.ParentId
            }, 30);
            if(lg.ValidToken)
            {
                return Json(new InsertOrUpdateResult() { Error = lg.Error, Message = lg.Message });
            }

            return Json(new InsertOrUpdateResult() { Error = true, Message = lg.Message });
        }
    }
}