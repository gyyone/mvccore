﻿using ClassLib;
using Core.Akka;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SolrNetCore;
using System.Collections.Generic;
using System.Linq;
using Util;
using ClassExtension;
namespace MVCCore.Controllers
{
    public class AceEditorCompleterController:BaseController
    {
        public AceEditorCompleterController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {

        }

        [HttpPost]
        public JsonResult GetSchemaIntellesense([FromBody]JObject param)
        {
            var jm = this.ValidSession(HttpContext.Session);
            List<Dictionary<string, string>> resultlist = new List<Dictionary<string, string>>();
            Dictionary<string, object> paramdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());
            string name = paramdic["name"].ToString();
            Dictionary<string, object> dependency = JsonConvert.DeserializeObject<Dictionary<string, object>>(paramdic["dependent"].ToString());

            //Field Id -> Field Value selectedd
            List<string> tableidlist = JsonConvert.DeserializeObject<List<string>>(dependency["tableid"].ToString());
            foreach (var tableid in tableidlist)
            {
                foreach (var tablefield in Settings.ds.TableToFieldDic[tableid])
                {
                    if (tablefield.Value.Name.Contains(name))
                    {
                        Dictionary<string, string> result = new Dictionary<string, string>();
                        result["caption"] = tablefield.Value.Name;
                        result["value"] = $"{"{"}{tablefield.Value.Name};{tableid}{"}"}";
                        result["desc"] = $"{tablefield.Value.Name}";
                        result["type"] = "snippet";
                        resultlist.Add(result);
                    }
                }
                List<string> tablelist = Settings.ds.GetChildTableId(tableid, true);
                foreach (var tid in tablelist)
                {
                    if (!Settings.ds.TableIdDic[tid].Name.Contains(name))
                    {
                        continue;
                    }
                    Dictionary<string, string> result = new Dictionary<string, string>();
                    result["caption"] = Settings.ds.TableIdDic[tid].Name;
                    result["value"] = $"{"{"}{Settings.ds.TableIdDic[tid].Name};{Settings.ds.TableIdDic[tid].id}{"}"}";
                    result["desc"] = $"{Settings.ds.GetFullTablename(tid)}";
                    result["type"] = "snippet";
                    resultlist.Add(result);
                }
            }

            jm.Obj = resultlist;
            return Json(jm);

        }
        [HttpPost]
        public JsonResult GetCSharpCompleter([FromBody]JObject param)
        {
            var jm = this.ValidSession(HttpContext.Session);
            List<Dictionary<string, string>> resultlist = new List<Dictionary<string, string>>();
            Dictionary<string, object> paramdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());
            string name = paramdic["name"].ToString();
            //Dictionary<string, object> dependency = JsonConvert.DeserializeObject<Dictionary<string, object>>(paramdic["dependent"].ToString());

            //Field Id -> Field Value selectedd
            List<Dictionary<string, string>>  record = Settings.RequestHandler.AskSync<List<Dictionary<string,string>>>(new MessageGetAutoComple() {  Search = name},60);

            jm.Obj = record;
            return Json(jm);

        }

        [HttpPost]
        public JsonResult GetTemplateCompleter([FromBody]JObject param)
        {
            var jm = this.ValidSession(HttpContext.Session);
            List<Dictionary<string, string>> resultlist = new List<Dictionary<string, string>>();
            
            Dictionary<string, object> paramdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(param.ToString());
            string search = paramdic["name"].ToString();
            Dictionary<string, object> dependency = JsonConvert.DeserializeObject<Dictionary<string, object>>(paramdic["dependent"].ToString());
            List<string> tableIdlist = TypeCheck.CastToList(dependency["TableId"]);
    

  
            foreach (var tableId in tableIdlist)
            {
                foreach (var tablefield in Settings.ds.TableToFieldDic[tableId])
                {
                    if(tableId == "BJEHvFAECFEDCtEFEvIDJFDuBIIEBIDvJJ")
                    {
                        Dictionary<string, string> resulttoReturn = new Dictionary<string, string>();
                        resulttoReturn["caption"] = "getDataType";
                        resulttoReturn["value"] = $"{"{["}getDataType; Get Datatype{"]}"}";
                        resulttoReturn["desc"] = $"Get Datatype of this field property";
                        resulttoReturn["type"] = "snippet";
                        resultlist.Add(resulttoReturn);

                        Dictionary<string, string> defaultvalue = new Dictionary<string, string>();
                        defaultvalue["caption"] = "getDefaultValue";
                        defaultvalue["value"] = $"{"{["}getDefaultValue; Get Default Value{"]}"}";
                        defaultvalue["desc"] = $"Get Default Value of this field property";
                        defaultvalue["type"] = "snippet";
                        resultlist.Add(defaultvalue);
                    }

                    string fieldsearch = search.Trim(new char[] { '\"', '\'', ' ' });
                    if (tablefield.Value.Name.Contains(fieldsearch))
                    {
                        Dictionary<string, string> result = new Dictionary<string, string>();
                        string idname = tablefield.Value.Name.Equals("id") ? "id" : tablefield.Value.id;
                        result["caption"] = tablefield.Value.Name;
                        result["value"] = $"{"{["}{tablefield.Value.Name};{idname}={tablefield.Value.Name}{"]}"}";
                        result["desc"] = $"{tablefield.Value.Name}";
                        result["type"] = "snippet";
                        resultlist.Add(result);
                        Dictionary<string, string> resultclean = new Dictionary<string, string>();

                        resultclean["caption"] = tablefield.Value.Name + " clean";
                        resultclean["value"] = $"{"{[clean"};{idname}={tablefield.Value.Name}{"]}"}";
                        resultclean["desc"] = $"Clean the {tablefield.Value.Name}. Remove any space, punc and so on.";
                        resultclean["type"] = "snippet";
                        resultlist.Add(resultclean);       

                    }
                }
            }
            List<string> childList = Settings.ds.TableIdDic.Where(x=> tableIdlist.Contains(x.Value.ParentId)).Select(x=> x.Key).ToList();
            Template t = new Template();
            string templateid = Template.ClassID;
            Settings.RequestHandler.AskSync<MessageRestore>(new MessageQuery() { });
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            if(search.ToLower().Contains("template:"))
            {
                string fortableid = TypeCheck.GetJsonPropertyName(t, nameof(t.ForTable));
                string parentId = TypeCheck.GetJsonPropertyName(t, nameof(t.ParentId));
                string nameid = TypeCheck.GetJsonPropertyName(t, nameof(t.Name));
                string groupnameid = TypeCheck.GetJsonPropertyName(t, nameof(t.GroupName));
                List<string> templatelist = new List<string>();
                string searchkey = search.Replace("template:","") + "*";
                ResponseDetail rstemplate =  Settings.ds.Query(templateid, new Dictionary<string, object>() {
                    {parentId, childList},
                    {nameid, searchkey },
                    {groupnameid, searchkey }
                }, strict:false, orQuery:true);
                foreach (var x in rstemplate.docs)
                {                  
                    Template trecord = x.CastToObject<Template>();        
                    if(tableIdlist.Contains(trecord.ForTable))
                    {
                        Dictionary<string, string> result = new Dictionary<string, string>();
                        result["caption"] = "template:" + trecord.Name;
                        result["value"] = $"{"{["}template;{trecord.id}={trecord.Name}{"]}"}";
                        result["desc"] = $"Name : {trecord.Name } , Group : {trecord.Name }";
                        result["type"] = "snippet";
                        resultlist.Add(result);
                    }

                    if(childList.Contains(trecord.ForTable) ||  Settings.ds.TableIdDic[trecord.ForTable].SelfReference)
                    {
                        Dictionary<string, string> result2 = new Dictionary<string, string>();
                        result2["caption"] = "template:" + trecord.Name + " child";
                        result2["value"] = $"{"{["}template:child;{trecord.id}={trecord.Name}{"]}"}";
                        result2["desc"] = $"Name : {trecord.Name } , Group : {trecord.Name }";
                        result2["type"] = "snippet";
                        resultlist.Add(result2);
                    }
            
               
                }

            }
            
            if(search.Contains("{["))
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                result["caption"] = "{[]}";
                result["value"] = "{[]}";
                result["desc"] = "Key for text Replace";
                result["type"] = "snippet";
                resultlist.Add(result);
            }



            jm.Obj = resultlist;
            return Json(jm);

        }
    }

}