﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models.Public;
using System.IO;
using System.Threading.Tasks;
using Core;
using Core.Akka;
using Util;
using System.Collections.Generic;
using ClassExtension;
namespace MVCCore.Controllers
{
    public class UrlController : BaseController
    {

        public UrlController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
     
        }
        public  string Action(string controller, string action)
        {
            return Url.Action(controller, action);
        }
    }
    public class BaseController : Controller
    {
        public readonly IViewRenderService _viewRenderService;
     
        private IViewRenderService viewRenderService;

        public BaseController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext)
        {            
            _viewRenderService = viewRenderService;

            Settings.ServerHub = hubContext;
        }


        public JsonModel ValidSession(ISession session)
        {
            var jm = new JsonModel();
            jm.Code = JSONCODE.Success.ToString();
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            if (user == null ||  string.IsNullOrEmpty(user.id))
            {
                user = Settings.RequestHandler.AskSync<Users>(new GetUser() { UserIdentity = "" });     
                HttpContext.Session.Set<Users>(SessionName.User, user);
                jm.Code = JSONCODE.SessionExpired.ToString();
                return jm;
            }   

            return jm;
        }

        public JsonModel ValidData(IReturnMessage message)
        {
            var jm = new JsonModel();

            if (message == null)
            {
                jm.Message = "Null return message";
                jm.Code = JSONCODE.Fail.ToString();
                return jm;
            }
            else if (!message.ValidToken)
            {
                HttpContext.Response.Redirect(this.Url.Action("Index", "Home"));
                jm.Obj = this.Url.Action("Index", "Home");
                jm.Code = JSONCODE.SessionExpired.ToString();
                return jm;
            }
            else if (message.Error)
            {
                jm.Message = message.Message;
                jm.Code = JSONCODE.Fail.ToString();
                return jm;
            }
            jm.Obj = message.Message;
            jm.Code = JSONCODE.Success.ToString();
            return jm;
        }      

        public MessageQueryReturn QueryResult(string userIdentity,string tableName, Dictionary<string, object> searchterm, int pageSize = 10, int startIndex = 0, Dictionary<string, string> _sortingDic = null, LuceneOccur occur = LuceneOccur.SHOULD, LuceneQuery query = LuceneQuery.QueryParse,bool rawData = false)
        {        
            MessageQueryReturn lg = Message<MessageQueryReturn>.Query(Settings.RequestHandler, new MessageQuery
            {
                UserIdentity = userIdentity,
                TableId = tableName,
                SearchTerm = searchterm,
                PageSize = pageSize,
                StartIndex = startIndex,
                SortTerm = _sortingDic,
                QueryType = query,
                QueryOccur = occur,    
                RawData = rawData,
                HostUrl = Settings.cfg.HostUrl
            });
            return lg;
        }
        

    }
}