﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models.Public;
using MVCCore.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Core;
using Core.Akka;
using Core.Akka.Query;
using Util;
using ClassLib;
using ClassExtension;
namespace MVCCore.Controllers
{
    public class MenuController : BaseController
    {
        public MenuController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }
        [HttpPost]
        public JsonResult GetHtmlSettings()
        {

            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            if (usersetting == null || string.IsNullOrWhiteSpace(usersetting.UserId))
            {
                jm.Obj = Url.Action("Index", "Home");
                jm.Code = JSONCODE.SessionExpired.ToString();
                return Json(jm);
            }

            jm.Obj = _viewRenderService.RenderToString("Settings", usersetting);
            return Json(jm);
        }

        [HttpPost]
        public JsonResult GetMenu(Dictionary<string, string> search)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                List<TreeNode> tree = Settings.RequestHandler.AskSync<List<TreeNode>>(new QueryMenu() { SearchValue = search, UserIdentity = user.id }, 600);
                if (search.ContainsKey("str"))
                {
                    return Json(tree.Select(x => x.id).ToList());
                }
                else if (search.ContainsKey(FieldNames.ParentId))
                {
                    return Json(tree);
                }
            }
            return Json(new List<TreeNode>());
        }

        [HttpPost]
        public JsonResult DeleteMenu(Dictionary<string,string> selectedMenu)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new DeleteMenu() { Menu = new MenuProperty() { id = selectedMenu["id"], ParentId = selectedMenu[FieldNames.ParentId] }, UserIdentity = user.id, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }

        [HttpPost]
        public JsonResult AddMenu(Dictionary<string,string> selectedMenu,bool addAsSub)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code == JSONCODE.Success.ToString())
            {
                IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new AddMenu() { SelectedMenu = new MenuProperty() { id = selectedMenu["id"], ParentId = selectedMenu["ParentId"] }, UserIdentity = user.id, AddAsSubMenu = addAsSub, StatusSender = Settings.WebServerHandler }, 600);
                jm = this.ValidData(message);
                return Json(jm);
            }
            return Json(jm);
        }
        [HttpPost]


        [HttpPost]
        public JsonResult CopyMenu(string targetCopyMenu, string targetDestMenu,bool copyAsSub)
        {
            var jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);  

            IReturnMessage message = Settings.RequestHandler.AskSync<IReturnMessage>(new CopyMenuTo()
            {
                TargetCopyMenu = new MenuProperty() { id= targetCopyMenu },
                ToMenu = new MenuProperty() { id =  targetDestMenu } ,
                UserIdentity = user.id,
                CopyAsSubMenu = copyAsSub,
                StatusSender = Settings.WebServerHandler
            }, 600);

            jm = this.ValidData(message);
            return Json(jm);

        }

        [HttpPost]
        public JsonResult SideBar()
        {
            JsonModel js = this.ValidSession(HttpContext.Session);
            if(js.Code == JSONCODE.Success.ToString())
            {
                Users user = HttpContext.Session.Get<Users>(SessionName.User);

                //List<TreeNode> sidebarnodes = Settings.RequestHandler.AskSync<List<TreeNode>>(new MessageRequest { RequestType = Core.Akka.Request.SideBar, UserIdentity = user.id }, 10);
                var sidebarnodes = new List<TreeNode>();
               
                foreach (var valuepair in Settings.dataStructure.MenuIdDic.Select(x => x.Value).OrderBy(x => x.RowNo))
                {
                    if (!valuepair.HasAccess(user.UserGroups, user.id))
                    {
                        continue;
                    }

                    var trtable = new TreeNode
                    {
                        id = $"sb{valuepair.id}",
                        text = MVCExt.KeyWords.GetWord(user.Language, valuepair.Name),
                        parent = (string.IsNullOrWhiteSpace(valuepair.ParentId) || valuepair.ParentId == "#") ? "#" : $"sb{valuepair.ParentId}",
                        NodeObject = Settings.dataStructure.PagesIdDic.Where(x => x.Value.ParentId == valuepair.id).Select(x => new TableMenuItem()
                        {
                            id = x.Value.id,
                            ParentId = x.Value.ParentId,
                            TableId = x.Value.TableId,
                            PageType = x.Value.PageType,
                            Url = x.Value.Url,
                            SoftFilter = x.Value.SoftFilter
                        }),
                        li_attr = valuepair.GetLiAttr(),
                        state = TreeState.GetTreeState(opened: true),
                        icon = string.IsNullOrEmpty(valuepair.Icon) ? "" : Settings.dataStructure.GetTextById(Icons.ClassID, new List<string>() { Icons.FieldsId.ClassName }, valuepair.Icon)

                    };

                    sidebarnodes.Add(trtable);
                }
                if (sidebarnodes != null)
                {
                    js.Code = JSONCODE.Success.ToString();
                    js.Obj = sidebarnodes;
                }
                else
                {
                    js.Code = JSONCODE.Fail.ToString();
                    js.Obj = new List<TreeNode>();
                    js.Message = "Server Manager Not Running";
                    ((List<TreeNode>)(js.Obj)).Add(new TreeNode
                    {
                        id = "NA",
                        text = "No Response",
                        parent = "#",
                        type = "datastructure"
                    });
                }

            }           
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter());
            return Json(js, settings);
        }

 

   

    }
}