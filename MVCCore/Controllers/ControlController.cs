﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using MVCCore.Models;
using MVCCore.Models.Public;
using Newtonsoft.Json;
using Core;
using Core.Akka;
using Util;
using ClassExtension;
using ClassLib;



namespace MVCCore.Controllers
{
    public class ControlController : BaseController
    {
        public ControlController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }




        [HttpPost]
        public JsonResult GetHtmlTabTable(string token,string tableName, Dictionary<string,object> searchTerm,Dictionary<string,string> tabDic)
        {
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            List<TabTableModel> tablist = new List<TabTableModel>();

            foreach (var keyvaluepair in tabDic)
            {
                TabTableModel tb = new TabTableModel();
                tb.TableName = tableName;
                tb.SoftFilter = searchTerm;
                tb.TabName = keyvaluepair.Key;
                tb.TabText = keyvaluepair.Value;
                tablist.Add(tb);
            }

            jm.Obj = this._viewRenderService.RenderToString("TabTableViewPartial", tablist);
            return Json(jm);
        }

        [HttpPost]
        public JsonResult GetHtmlTable(string tableName,string searchTerm, Dictionary<string, string> sort, bool trash)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            Dictionary<string, List<string>> seachterm;
            if (!string.IsNullOrEmpty(searchTerm))
            {
                seachterm =  JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(searchTerm);
            }
            else
            {
                seachterm = new Dictionary<string, List<string>>();
            }
            

            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }           

            var dt = new DataTableModel();
            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            dt.PageLength = 10;
            dt.TableId = tableName;
            dt.SelectUrl = Url.Action("Select", "Data");
            dt.SelectTableUrl = Url.Action("SelectTable", "Data");
            dt.EditUrl = Url.Action("GetHtmlForm", "Control");
            dt.DeleteUrl = Url.Action("Delete", "Data");
            dt.SubtableUrl = Url.Action("GetHtmlTabTable", "Control");
            dt.GetHtmlTableUrl = Url.Action("GetHtmlTable", "Control");
            dt.CommitUrl = Url.Action("Commit", "Data");
            dt.Trash = trash;
            dt.Sort = sort;
            dt.EnableUpload = true;

            MessageReturnTableHeader returnresult = Settings.RequestHandler.AskSync<MessageReturnTableHeader>(new MessageTableHeader()
            {
                UserIdentity = user.id,
                TableId = dt.TableId,
                Trash = trash,                
                SearchTerm = seachterm.ToDictionary(x=> x.Key, y=> (object)y.Value)
            }, 30);
 
           
            if ((jm = this.ValidData(returnresult)).Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
    
     
            dt.TableNameDisplay = returnresult.TableNameDisplay;
            dt.Headers = returnresult.Header;
            dt.VisibleHeader = returnresult.Header.Where(x=> x.AllowView && x.Enable).ToDictionary(x => x.FieldId, y => y.FieldName);

            dt.Footer = returnresult.HasFooter;
            dt.User = usersetting;
            dt.SeachModelDic = returnresult.SearchDic;
            dt.SearchTerm = returnresult.SearchTerm;
            dt.LeftColumnFixed = returnresult.LeftColumnFix;
            dt.RightColumnFixed = returnresult.RightColumnFix;
          
            dt.NotSortableLIst = returnresult.NotSortableList;
            //footerKeyPair:key {field1,field2,field3}
            dt.TableType = returnresult.TableType;
            //footerKeyPair:key {field1,field2,field3}
            dt.SubTableList = returnresult.SubTableList;
            jm.Obj = this._viewRenderService.RenderToString("TableViewPartial", dt);
            return Json(jm);
        }

        [HttpPost]
        public async Task<JsonResult> GetHistoryTable(string tableName,string id, string fieldName)
        {
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            var dt = new DataTableModel();
            Users usersetting = HttpContext.Session.Get<Users>(SessionName.User);
            dt.PageLength = 10;
            dt.TableId = tableName;
            dt.SelectUrl = Url.Action("Select", "Data");
            dt.SelectTableUrl = Url.Action("SelectTableTx", "Data");
            dt.EditUrl = Url.Action("GetHtmlForm", "Control");
            dt.UpdateUrl = Url.Action("InsertOrUpdate", "Data");
            dt.DeleteUrl = Url.Action("Delete", "Data");
            dt.SubtableUrl = Url.Action("GetHtmlTabTable", "Control");
            dt.GetHtmlTableUrl = Url.Action("GetHtmlTable", "Control");
            dt.CommitUrl = Url.Action("Commit", "Data");
            dt.Trash = false;
            dt.EnableUpload = false;
            MessageReturnTableHeader returnresult = await
                Message<MessageReturnTableHeader>.QueryTask(Settings.RequestHandler, new MessageQueryHistoryTable()
                {
                    UserIdentity = user.id,
                    Id = id,
                    TableId = tableName,
                    FieldId = fieldName
                });

            if ((jm = this.ValidData(returnresult)).Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            dt.Headers = returnresult.Header;
            dt.VisibleHeader = returnresult.Header.Where(x=> x.Enable && x.AllowView).ToDictionary(x => x.FieldId, y => y.FieldName);


            dt.Footer = returnresult.HasFooter;
            dt.User = usersetting;
            dt.SeachModelDic = returnresult.SearchDic;
    
            dt.NotSortableLIst = returnresult.NotSortableList;
            //footerKeyPair:key {field1,field2,field3}
            dt.TableType = returnresult.TableType;
            //footerKeyPair:key {field1,field2,field3}

            //jm.Obj = this._viewRenderService.RenderToString("HistoryItemView", dt);
            jm.Message = Settings.dataStructure.TableIdDic[tableName].Name;
            jm.Obj = this._viewRenderService.RenderToString("TableViewPartial", dt);
            return Json(jm);
        }


        [HttpPost]
        public async Task<JsonResult> GetHtmlForm(string tableName, string guid,Dictionary<string,string> searchterm, bool isNew,bool enableSubTable,string updateUrl,string deleteUrl,string subtableUrl)
        {        
            Users user = HttpContext.Session.Get<Users>(SessionName.User); 
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }

            MessageQueryReturnForm returnresult = await Message<MessageQueryReturnForm>.QueryTask(Settings.RequestHandler, new MessageQueryForm { UserIdentity = user.id, TableId = tableName, Id = guid, SearchTerm = searchterm.ToDictionary((KeyValuePair<string, string> x)=> x.Key,(KeyValuePair<string, string> y)=> (object)y.Value) });
            if ((jm = this.ValidData(returnresult)).Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            var formModels = new List<FormModel>();
            var fm = new FormModel();
            fm.Dependancy = returnresult.Dependancy;
            fm.SearchTerm = returnresult.SearchTerm;


            if (returnresult != null)
            {
                var _controls = new List<ControlModel>();


                foreach (var record in returnresult.Fields)
                {
                    string onchangeForm = "var fn=OnChangeForm; var data={id:'" + returnresult.Id + "',name:'" + record.Name + "'}";
                    var c = record.ControlType;
                    string controlName = record.Name;
                    if (record.Value == null)
                    {
                        record.Value = "";
                    }
                    ControlModel control = null;
                    switch (c)
                    {
                        case Const.ControlTypes.CheckBox:
                            {
                                bool result;
                                if (!bool.TryParse(record.Value.ToString(), out result))
                                {
                                    result = false;
                                }
                                control = MVCExt.ControlCheckBox(record.Name, record.Text, result, onchangeForm, "", "",
                                record.ReadOnly, record.HistoryCount);
         
                            }

                            break;
                        case Const.ControlTypes.ComboBox:
                            {
                                if (record.Value == null || string.IsNullOrWhiteSpace(record.Value.ToString()))
                                {
                                    record.Value = new List<string>();
                                }
                                List<string> itemsControl;
                                if (TypeCheck.IsMultiValue(record.Value))
                                {
                                    itemsControl = TypeCheck.CastToList(record.Value);
                                }
                                else
                                {
                                    try
                                    {
                                        itemsControl = JsonConvert.DeserializeObject<List<string>>(record.Value.ToString());
                                    }
                                    catch (Exception)
                                    {
                                        itemsControl = new List<string>() { record.Value.ToString() };
                                    }

                                }
                                if(record.Source != null)
                                {                           
                                    control = MVCExt.ControlComboBox(controlName, record.Text, "", record.MultipleValue, itemsControl, record.Source.KeySource, onchangeForm, false, record.ReadOnly, record.HistoryCount, null, null);
                           
                                    ((DropdownModel) control).SelectUrl = Url.Action("Select", "Data");
                                }

                 
                            }
                            break;

                        case Const.ControlTypes.SwitchButton:
                            {
                                bool result = TypeCheck.CastToBool(record.Value);
                                control = MVCExt.ControlSwitch(controlName, record.Text, "", result, onchangeForm, "", record.ReadOnly, record.HistoryCount);
                   
                            }
                            break;
                        case Const.ControlTypes.TextBox:
                            {
                                control = MVCExt.ControlInput(controlName, record.Text, "",
                                record.Value.ToString(), "", onchangeForm, record.ReadOnly, record.HistoryCount);
                                ((InputModel)control).Type = "text";               
                            }                   
                            break;

                        case Const.ControlTypes.SummerNote:
                            {
                                control = MVCExt.ControlDefaultEditor(controlName, record.Text, record.Value.ToString(), onchangeForm, record.ReadOnly, record.Options, record.HistoryCount, 500, 500);
                       
                            }
                          
                            break;

                        case Const.ControlTypes.ColorPicker:
                            {
                                control = MVCExt.ControlColorPicker(controlName, record.Text, record.Value.ToString(), onchangeForm, record.ReadOnly, record.HistoryCount);
                               
                            }                            
                            break;
                        case Const.ControlTypes.CodeEditor:
                            {
                                control = MVCExt.ControlCodeEditor(controlName, record.Text, record.Value.ToString(), record.Options, onchangeForm, record.ReadOnly, record.HistoryCount);
                              
                            }                            
                            break;
                        case Const.ControlTypes.RadioButton:
                            {
                             
                                Dictionary<string, string> radioItem;
                                if (record.Source.KeySource.Any(x => x.GetSource() is TableSource))
                                {
                                    radioItem = MVCExt.GetItems(user.id, record.Source.KeySource.Where(x => x.GetSource() is TableSource).Select(x => x.GetSource() as TableSource).ToList(), new List<string>() { "" }, false, LuceneQuery.QueryParse,
                                        LuceneOccur.SHOULD);
                                }
                                else
                                {
                                    radioItem = record.Source.KeySource.Where(x => x.GetSource() is KeyValueSource).Select(x => ((KeyValueSource)x.GetSource()).Items).FirstOrDefault();
                                }
                                control = MVCExt.ControlRadio(controlName, record.Text, record.Value.ToString(), radioItem, onchangeForm, record.ReadOnly, record.HistoryCount);
                                ((RadioModel)control).SelectUrl = Url.Action("Select", "Data");
                         
                            }

                            break;
                        case Const.ControlTypes.DatetimePicker:
                            {
                                control = MVCExt.ControlDateTimePicker(controlName, record.Text, "", record.Value.ToString(), onchangeForm, record.ReadOnly, record.Options, record.HistoryCount);
                    
                            }
                           
                            break;
                        case Const.ControlTypes.InputMask:
                            {
                                control = MVCExt.ControlInputMask(controlName, record.Text, "", record.Value.ToString(), "", onchangeForm, record.ReadOnly, record.Options, record.HistoryCount);
                          
                            }
                          
                            break;
                        case Const.ControlTypes.CheckList:
                            {
                            
                                string oncheckAll = "var fn=OnAllToggleCheck; var data={id:'" + returnresult.Id + "',name:'" + record.Name + "'}";
                                List<string> itemsChecklist = TypeCheck.CastToList(record.Value);
                                Dictionary<string, string> CheckListitems;
                                if (record.Source.KeySource.Any(x => x.GetSource() is TableSource))
                                {
                                    CheckListitems = MVCExt.GetItems(user.id, record.Source.KeySource.Where(x => x.GetSource() is TableSource).Select(x => x.GetSource() as TableSource).ToList(), new List<string>() { "" }, false, LuceneQuery.QueryParse,
                                        LuceneOccur.SHOULD);
                                }
                                else
                                {
                                    CheckListitems = record.Source.KeySource.Where(x => x.GetSource() is KeyValueSource).Select(x => ((KeyValueSource)x.GetSource()).Items).FirstOrDefault();

                                }
                                control = MVCExt.ControlCheckList(controlName, record.Text, itemsChecklist, CheckListitems, onchangeForm, oncheckAll, record.ReadOnly, record.HistoryCount);

                                ((CheckListModel)control).SelectUrl = Url.Action("Select", "Data");
                       
                            }               

                            break;
                        case Const.ControlTypes.FileUpload:
                            {
                                control = MVCExt.FileUploadInput(controlName, record.Text, onchangeForm, record.ReadOnly, record.HistoryCount);
                                //fum.FilesUrl = (record.Options).OptionValue.ToString();
                                control.Options = record.Options;
                                ((FileUploadModel)control).FieldId = controlName;    
                                ((FileUploadModel)control).Multiple = record.MultipleValue;
                         
                            }
       
                            break;
                        case Const.ControlTypes.KeyValueBox:
                            {
                                Dictionary<string, object> keyvalueitems;
                                if (record.Value == null || record.Value.ToString() == "null" || string.IsNullOrWhiteSpace(record.Value.ToString()))
                                {
                                    keyvalueitems = new Dictionary<string, object>();
                                }
                                else
                                {
                                    keyvalueitems = TypeCheck.CastToDic(record.Value.ToString());
                                }
                                control = new KeyValueModel();
                                control.Name = controlName;
                                ((KeyValueModel)control).KeyDropdown = MVCExt.ControlComboBox(controlName, "", "", false, new List<string>(), record.Source.KeySource, onchangeForm, false, record.ReadOnly, record.HistoryCount, null, "col-md-4");
                                ((KeyValueModel)control).KeyDropdown.SelectUrl = Url.Action("Select", "Data");
                                ((KeyValueModel)control).KeyDropdown.ClassName = ((KeyValueModel)control).KeyDropdown.ClassName;
                                ((KeyValueModel)control).KeyDropdown.Width = "100%";
                                ((KeyValueModel)control).KeyDropdown.OnChangedEvent = "var fn=OnChangeForm; var data={id:'" + returnresult.Id + "',name:'" + record.Name + ",iskey:true'}";
                                ((KeyValueModel)control).ValueDropdown = MVCExt.ControlComboBox(controlName, "", "", true, new List<string>(), record.Source.ValueSource, onchangeForm, false, record.ReadOnly, record.HistoryCount, null, "col-md-4");
                                ((KeyValueModel)control).ValueDropdown.SelectUrl = Url.Action("Select", "Data");
                                ((KeyValueModel)control).ValueDropdown.ClassName = ((KeyValueModel)control).ValueDropdown.ClassName;
                                ((KeyValueModel)control).ValueDropdown.Width = "100%";
                                ((KeyValueModel)control).ValueDropdown.OnChangedEvent = "var fn=OnChangeForm; var data={id:'" + returnresult.Id + "',name:'" + record.Name + ",iskey:false'}";
                                ((KeyValueModel)control).ValueDropdown.Multiple = "multiple";
                                ((KeyValueModel)control).Label = record.Text;
                                ((KeyValueModel)control).SelectedKeyValue = keyvalueitems.ToDictionary(x => x.Key, y => TypeCheck.CastToList(y.Value));
                                ((KeyValueModel)control).OnChangedEvent = onchangeForm;
                                ((KeyValueModel)control).ValueSourceMultipleValue = record.Source.ValueSourceMultipleValue;               
                              
                            }
                            
                            break;
                        case Const.ControlTypes.SourceEditor:
                            {
                                control = new SourceEditorModel();
                                control.Name = controlName;
                                control.Label = record.Text;
                                control.Value = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<DataSource>(record.Value.ToString()));                        
                                control.OnChangedEvent = onchangeForm;                  
                              
                            }
                            break;
                        case Const.ControlTypes.SortableComboBox:
                            {
                                if (record.Value == null || string.IsNullOrWhiteSpace(record.Value.ToString()))
                                {
                                    record.Value = new List<string>();
                                }
                                List<string> itemsSortable;
                                if (TypeCheck.IsMultiValue(record.Value))
                                {
                                    itemsSortable = TypeCheck.CastToList(record.Value);
                                }
                                else
                                {
                                    itemsSortable = new List<string>() { record.Value.ToString() };
                                }
                              
                                if(record.Source == null)
                                {
                                    record.Source = new DataSource();
                                }
                                control = MVCExt.ControlSortableComboBox(controlName, record.Text, "", itemsSortable, record.Source.KeySource, onchangeForm, false, record.ReadOnly, record.HistoryCount, null, null);
                                ((DropdownModel)control).SelectUrl = Url.Action("Select", "Data");
                 
                            }
  
                            break;
                        case Const.ControlTypes.EncrytText:
                            {
                                control = new EncryptTextModel();
                                ((EncryptTextModel)control).ClassName = "form-control FieldInput";
                                control.Label = record.Text;
                                control.Name = controlName;
                                control.Value = record.Value.ToString();
                                ((EncryptTextModel)control).Type = "password";                           
                                control.OnChangedEvent = onchangeForm;
                     
                            }
                            break;
                        default:
                            control = MVCExt.ControlInput(controlName, record.Text, "", record.Value.ToString(), "", onchangeForm, record.ReadOnly, record.HistoryCount);
                   
                            break;

                    }

                    if(control != null)
                    {
                        control.ReadOnly = record.ReadOnly;
                        control.Visible = record.Visible;
                        control.HelpBlock = record.Hint;
                        control.FormGroup = string.Join(" ", record.GroupFrom.CastToList());
                        control.SetDefault = record.SetDefault;
                        _controls.Add(control);
                    }
     
                }
                fm.TableNameDisplay = returnresult.TableNameDisplay;
                fm.Id = returnresult.Id;
                fm.TableName = tableName;
                fm.IsNew = isNew;
                foreach (var controlModel in _controls)
                {
                    controlModel.TableName = tableName;
                    controlModel.RecordId = returnresult.Id;
                }

                fm.UpdateUrl = updateUrl ?? Url.Action("InsertOrUpdate", "Data");
                fm.DeleteUrl = deleteUrl ?? Url.Action("Delete", "Data");
                fm.SubTableUrl = subtableUrl ?? Url.Action("GetHtmlTabTable", "Control");
                fm.Controls = _controls;
                if (enableSubTable)
                {
                    string subtable = string.Empty;
                    if (returnresult.SubTableList != null && returnresult.SubTableList.Count > 0)
                    {

                        List<TabTableModel> tablist = new List<TabTableModel>();

                        foreach (var keyvaluepair in returnresult.SubTableList)
                        {
                            TabTableModel tb = new TabTableModel();
                            tb.TableName = tableName;
                            tb.SoftFilter = keyvaluepair.Value.SearchTerm.ToDictionary(x=> x.Key,y=> (object)y.Value.CastToList());

                            tb.TabName = keyvaluepair.Key;
                            tb.TabText = keyvaluepair.Value.Name;
                            tablist.Add(tb);
                        }

                        subtable = this._viewRenderService.RenderToString("TabTableViewPartial", tablist);
                    }
                    fm.SubTableHtml = subtable;
                }


                formModels.Add(fm);
                jm.Obj = this._viewRenderService.RenderToString("HorizontalFormViewPartial", formModels);
            }
            return Json(jm);
        }
      
        //[HttpPost]
        //public JsonResult GetHtmlMenuForm(string token, string nodeId, MenuProperty nodeproperty)
        //{
        //    var jm = this.ValidSession(HttpContext.Session);
        //    if (jm.Code != JSONCODE.Success.ToString())
        //    {
        //        return Json(jm);
        //    }
        //    Users user = HttpContext.Session.Get<Users>(SessionName.User);

        //    string evNodeTextChange = "var fn=OnchangeNodeText;var data={nodeId:'" + nodeId + "', selectedNode:selectedMenuNode}";
        //    string evNodeCheck = "var fn=OnNodeCheckChange;var data={nodeId:'" + nodeId + "', selectedNode:selectedMenuNode}";
        //    string evNodeParamChange = "var fn=OnNodeParamChange;var data={nodeId:'" + nodeId + "', selectedNode:selectedMenuNode}";
        //    string evNodeSelect = "var fn=OnNodeSelectChange;var data={nodeId:'" + nodeId + "', selectedNode:selectedMenuNode}";

        //    var _controls = new List<ControlModel>();
        //    _controls.Add(MVCExt.ControlInput("Name", Words.Get( "Name",user.Language), "", nodeproperty.Name, "", evNodeTextChange, false));
        //    _controls.Add(MVCExt.ControlInput("ActionName", Words.Get( "ActionName",user.Language), "", nodeproperty.ActionName, "", evNodeTextChange, false));

        //    _controls.Add(MVCExt.ControlInput("ControllerName", Words.Get( "ControllerName",user.Language), "", nodeproperty.ControllerName, "", evNodeTextChange, false));

     
        //    _controls.Add(MVCExt.ControlComboBox("PageType", Words.Get( "PageType",user.Language), "", false,new List<string>() {nodeproperty.PageType.ToString()},new List<SelectSource>(){ new SelectSource(new Dictionary<string, string>() { { "Standard", "Standard" }, { "Menu", "Menu" }, { "Redirect", "Redirect" }})}, evNodeSelect, false, false));
        //    _controls.Add(MVCExt.ControlInput("Params", Words.Get( "Params",user.Language), "", JsonConvert.SerializeObject(nodeproperty.Params), "", evNodeParamChange, false));

        //    _controls.Add(MVCExt.ControlSwitch("IsDefaultPage", Words.Get( "IsDefaultPage",user.Language), "", nodeproperty.IsDefaultPage, evNodeCheck, "", false));

            
        //    //_controls.Add(MVCExt.ControlComboBox("Roles", Words.Get( "Roles",user.Language),"",true, nodeproperty.Roles,new List<SelectSource>() { new SelectSource(new TableSource() { TableId = "Roles", Fields = new List<string>() { "RoleName" } }) }  , evNodeSelect, false,false));
        //    _controls.Add(MVCExt.ControlComboBox("Icon", Words.Get( "Icon",user.Language), "",false,new List<string>() {nodeproperty.Icon}, new List<SelectSource>() { new SelectSource(new TableSource() { TableId = "Icons", Fields = new List<string>() { "<i class='", "ClassName", " fa-2x'></i>" } }) }, evNodeSelect, false, false));
        //    var formModels = new List<FormModel>();
        //    var fm = new FormModel();
        //    fm.Controls = _controls;
        //    formModels.Add(fm);
        //    jm.Code = JSONCODE.Success.ToString();
        //    jm.Obj = this._viewRenderService.RenderToString("HorizontalFormViewPartial", formModels);

        //    return Json(jm);
        //}

        [HttpPost]
        public JsonResult GetHtmlTableForm(string token, string nodeId, TableProperty nodeproperty)
        {
            var jm = this.ValidSession(HttpContext.Session);
            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }


            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            string evNodeCheck = "var fn=OnNodeCheckChange;var data={nodeId:'" + nodeId + "', selectedNode:selectedStructureNode}";
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            var _controls = new List<ControlModel>();
            _controls.Add(MVCExt.ControlInput("Name", Words.Get( "Name",user.Language), "", nodeproperty.Name, "", "var fn=OnchangeNodeText;var data={nodeId:'" + nodeId + "',selectedNode:selectedStructureNode}", false));
            _controls.Add(MVCExt.ControlSwitch("TextOnly", Words.Get( "TextOnly",user.Language), "", nodeproperty.TextOnly, evNodeCheck, "", false));

            var controlType = new Dictionary<string, string>();
            foreach (var control in Enum.GetNames(typeof (TableTypes)))
            {
                controlType.Add(control, Words.Get( control,user.Language));
            }
            var selectedcontrol = new List<string>();
            selectedcontrol.Add(nodeproperty.TableType);
            _controls.Add(MVCExt.ControlComboBox("TableType", Words.Get( "TableType",user.Language), "", false, selectedcontrol,new List<SelectSource>() {new SelectSource(controlType) }, "var fn=OnchangeNodeText;var data={nodeId:'" + nodeId + "',selectedNode:selectedStructureNode}", false, false));

            _controls.Add(MVCExt.ControlComboBox("UniquesList", Words.Get( "UniquesList",user.Language), "", true, new List<string>(),new List<SelectSource>(), "", false, false));

            var formModels = new List<FormModel>();
            var fm = new FormModel();
            fm.Controls = _controls;
            formModels.Add(fm);
            jm.Code = JSONCODE.Success.ToString();
            jm.Obj = this._viewRenderService.RenderToString("HorizontalFormViewPartial", formModels);

            return Json(jm);
        }

        [HttpPost]
        public JsonResult GetUploadCsv()
        {
            var jm = this.ValidSession(HttpContext.Session);

            if (jm.Code != JSONCODE.Success.ToString())
            {
                return Json(jm);
            }
            FileUploadModel fm = new FileUploadModel();
            fm.TableName = UploadStage.ClassID;
            fm.RecordId = Unique.NewGuid();
            fm.FieldId = UploadStage.FieldsId.Files;            
            fm.Url = "/Upload/UploadFiles";          
            jm.Obj = this._viewRenderService.RenderToString("FormUploadViewPartial", fm);

            return Json(jm);
        }

    }
}