﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClassLib;
using Core.Akka;
using DirectoryHelper;
using JsonHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.StaticFiles;
using MVCCore.Models.Public;
using Util;
using ClassExtension;
using Newtonsoft.Json;
using Core;

namespace MVCCore.Controllers
{   
   
    public class ViewDataUploadFilesResult
    {
        public string url;
        public string thumbnailUrl;
        public string deleteUrl;

        public string relativePath { set; get; }
        public string name { get; set; }
        public long size { get; set; }  
        public bool uploaded { set; get; }
        public string error { set; get; }
        public string access { set; get; }
        public string relativeFile { set; get; }
        public string fieldid { set; get; }
        public string recordid { set; get; }
        public string deleteType { set; get; }

    }
    public class JsonFiles
    {
        public ViewDataUploadFilesResult[] files;
        public JsonFiles(List<ViewDataUploadFilesResult> filesList)
        {
            files = new ViewDataUploadFilesResult[filesList.Count];
            for (int i = 0; i < filesList.Count; i++)
            {
                files[i] = filesList.ElementAt(i);
            }

        }
    }
   
    public class UploadController : BaseController
    {
        public static string SelectedFolder { set; get; }
        public static long freespace = 0;
    
        public static Dictionary<string, object> dicLock = new Dictionary<string, object>();
        public List<string> ImageExtensions = new List<string> { ".JPG", ".JPE", ".BMP", ".GIF", ".PNG" };
        public ActionResult Upload()
        {
            return View("");
        }
        public UploadController(IViewRenderService viewRenderService, IHubContext<ServerHub> hubContext) : base(viewRenderService, hubContext)
        {
        }
        public IActionResult Index()
        {
            JsonModel jm = this.ValidSession(HttpContext.Session);
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
           
            return View("Page/FileUpload", user);
        
        }
       
        public JsonResult DeleteFile(string recordid, string fieldId, string relativeFile)
        {
       
            Users user = HttpContext.Session.Get<Users>(SessionName.User); 
            if(!HasAccess(fieldId, recordid,"Delete"))
            {
                return Json(new ViewDataUploadFilesResult()
                {
                    name = "",
                    size = 0,
                    uploaded = false,
                    error = $"Access denial.",
                    access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public })
                });
            }
          
            foreach (string folder in Settings.ds.UploadedFolders)
            {
                string fullpath = $"{folder}/{fieldId}/{recordid}/{relativeFile}";
                if (!System.IO.File.Exists(fullpath))
                {
                    continue;
                }
                System.IO.File.Delete(fullpath);
                return Json(new ViewDataUploadFilesResult() { name = Path.GetFileName(relativeFile), uploaded = false });

            }
            return Json(new ViewDataUploadFilesResult() { });
        }

        public JsonResult UploadFiles()
        {
            var resultList = new List<ViewDataUploadFilesResult>();
            JsonFiles files;
            try
            {

                var CurrentContext = HttpContext;

                string fieldId = CurrentContext.Request.Form["fieldid"].ToString();
                string recordid = CurrentContext.Request.Form["recordid"].ToString();
                //if (!HasAccess(fieldId, recordid, "Modify"))
                //{
                //    resultList.Add(new ViewDataUploadFilesResult()
                //    {
                //        name = "",
                //        size = 0,
                //        uploaded = false,
                //        error = $"Access denial.",
                //        access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public })
                //    });
                //    files = new JsonFiles(resultList);
                //    return Json(files);
                //}

                int x = 0;
                foreach (IFormFile file in CurrentContext.Request.Form.Files)
                {
                    if(!CurrentContext.Request.Form.ContainsKey("relativepath" + (x)))
                    {
                        continue;
                    }
                    string relativepath = (CurrentContext.Request.Form["relativepath" + (x++)].ToString()).ToFSlashClean();
                    string relativeFile = (relativepath + file.FileName).ToFSlashClean();

                    if (string.IsNullOrWhiteSpace(SelectedFolder) || freespace < Settings.ds.MinSpaceRequired)
                    {
                        SelectedFolder = "";
                        foreach (string folder in Settings.ds.UploadedFolders)
                        {
                            string fullpath = Path.GetFullPath(folder).ToFSlashClean();
                            freespace = DirHelper.FreeSpace(fullpath);
                            if (freespace > Settings.ds.MinSpaceRequired)
                            {
                                SelectedFolder = $"{fullpath}";
                                break;
                            }
                        }
                    }
                    freespace = DirHelper.FreeSpace(SelectedFolder);
                    if (!string.IsNullOrWhiteSpace(SelectedFolder) && freespace >= Settings.ds.MinSpaceRequired)
                    {                   
                        MemoryStream ms = new MemoryStream();
                        file.CopyTo(ms);
                        string newpathfile = $"{SelectedFolder}/{fieldId}/{recordid}/{relativeFile}";
                        DirHelper.CreateDirectory(newpathfile);
                        System.IO.File.WriteAllBytes(newpathfile, ms.ToArray());
                        resultList.Add(new ViewDataUploadFilesResult()
                        {
                            deleteUrl = HasAccess(fieldId, recordid, "Delete") ? $"/Upload/DeleteFile?recordid={recordid}&fieldId={fieldId}&relativeFile={relativeFile}" : "",
                            name = file.FileName,
                            size = file.Length,
                            uploaded = true,
                            relativePath = relativeFile.ToFSlashDirectory(),
                            url = GetFilesUrl(fieldId,recordid, relativeFile),
                            thumbnailUrl = GetHumbnailUrl(fieldId, recordid, relativeFile),
                            access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public }),
                            relativeFile = relativeFile,
                            fieldid = fieldId,
                            recordid = recordid,
                            deleteType = "post"
                        });
                        if (fieldId == UploadStage.FieldsId.Files)
                        {                            
                            Users user = HttpContext.Session.Get<Users>(SessionName.User);
                            MessageUploadCSV muc = new MessageUploadCSV() { UserIdentity = user.id, ParentId = "#", UploadId = recordid, StatusSender = Settings.WebServerHandler };
                            Settings.RequestHandler.Tell(muc);
                        }
                    }
                    else
                    {
                        resultList.Add(new ViewDataUploadFilesResult()
                        {
                            name = file.FileName,
                            size = file.Length,
                            uploaded = false,
                            error = $"Not enough free space on server. Available Space {freespace / 1024 / 1024} MB to upload.",
                            access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public })
                        });

                    }

                }
             
                files = new JsonFiles(resultList);
                return Json(files);
            }
            catch (Exception ex)
            {
                resultList.Add(new ViewDataUploadFilesResult()
                {
                    name = "",
                    size = 0,
                    uploaded = false,
                    error = ex.ToString(),
                    access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public })
                });
                files = new JsonFiles(resultList);
                return Json(files);
            }


        }

        public IActionResult GetFile(string fieldid,string recordid,string relativeFile)
        {
            //var file = Path.Combine(Directory.GetCurrentDirectory(),
            //                        "MyStaticFiles", "images", "banner1.svg");

            //return PhysicalFile(file, "image/svg+xml");         
         
            if ((string.IsNullOrWhiteSpace(fieldid) || string.IsNullOrWhiteSpace(recordid)) || (!HasAccess(fieldid, recordid, "Modify","View")))
            {             
                foreach (string folder in Settings.ds.UploadedFolders)
                {
                    string filedir = $"{folder}/Ban.jpg";
                    if (System.IO.File.Exists(filedir))
                    {
                        return PhysicalFile(filedir, GetMimeTypeByWindowsRegistry(Path.GetFileName(filedir)));
                    }
                }
                return PhysicalFile("Ban.jpg", "image/jpg");
            }
            foreach (string folder in Settings.ds.UploadedFolders)
            {
                string filedir = $"{folder}/{fieldid}/{recordid}/{relativeFile}";
                if (System.IO.File.Exists(filedir))
                {
                    return PhysicalFile(filedir, GetMimeTypeByWindowsRegistry(Path.GetFileName(filedir)));
                    //return $"/{x}/Files/{filename}";
                }
            }

            foreach (string folder in Settings.ds.UploadedFolders)
            {
                string filedir = $"{folder}/NotFound.jpg";
                if (System.IO.File.Exists(filedir))
                {
                    return PhysicalFile(filedir, GetMimeTypeByWindowsRegistry(Path.GetFileName(filedir)));                 
                }
            }
            return PhysicalFile("NotFound.jpg", "image/jpg");
        }
        public JsonResult GetUploadedFiles(string recordid,string fieldId)
        {
            var resultList = new List<ViewDataUploadFilesResult>();
            if (!HasAccess(fieldId, recordid, "View"))
            {
                resultList.Add(new ViewDataUploadFilesResult()
                {
                    name = "",
                    size = 0,
                    uploaded = false,
                    error = $"Access denial.",
                    access = JsonConvert.SerializeObject(new List<string>() { Id.Role.Public })
                });       
                return Json(new JsonFiles(resultList));
            }

            List<ViewDataUploadFilesResult> list = new List<ViewDataUploadFilesResult>();      
            foreach (string folder in Settings.ds.UploadedFolders)
            {
                string dir = $"{folder}/{fieldId}/{recordid}/".ToFSlashFullPath();
              
                if (Directory.Exists(dir))
                {
                    string[] files = Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
                    foreach (var f in files)
                    {
                        string file = f.ToFSlashFullPath();
                        int trimindex = file.IndexOf(dir);
                        string relativeFile = $"{file.Substring(trimindex + dir.Length)}";

                        list.Add(new ViewDataUploadFilesResult() { 
                            deleteUrl = HasAccess(fieldId, recordid, "Delete") ? $"/Upload/DeleteFile?recordid={recordid}&fieldId={fieldId}&relativeFile={relativeFile}" : "",
                            deleteType = "post", 
                            relativePath = relativeFile.ToFSlashDirectory(),
                            name = Path.GetFileName(file), 
                            url = GetFilesUrl(fieldId, recordid, relativeFile),
                            thumbnailUrl = GetHumbnailUrl(fieldId, recordid, relativeFile), 
                            uploaded = true, 
                            size = new System.IO.FileInfo(file).Length,                     
                            relativeFile = relativeFile, 
                            fieldid = fieldId, 
                            recordid = recordid });
                    }
                }               
            }
            return Json(list);
        }

        private string GetHumbnailUrl(string fieldid,string recordid,string filepath)
        {
            return ImageExtensions.Where(y => y.Contains(Path.GetExtension(filepath).ToUpper())).Any() ? GetFilesUrl(fieldid, recordid,filepath) : "";
        }
              
        public static string GetMimeTypeByWindowsRegistry(string fileNameOrExtension)
        {
            string mimeType = "application/unknown";
            string ext = (fileNameOrExtension.Contains(".")) ? System.IO.Path.GetExtension(fileNameOrExtension).ToLower() : "." + fileNameOrExtension;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null) mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
        public string GetFilesUrl(string fieldid,string recordid,string filepath)
        {           
            return $"/Upload/GetFile?fieldid={fieldid}&recordid={recordid}&relativeFile={filepath}";
        }
        private bool HasAccess(string fieldId, string recordid,params string []accesstype)
        {
            return true;
            Users user = HttpContext.Session.Get<Users>(SessionName.User);
            
            if (Settings.dataStructure.FieldIdDic.ContainsKey(fieldId) && !Settings.dataStructure.FieldIdDic[fieldId].GetAccess(user.UserGroups, user.id).Any(x=> accesstype.Contains(x)))
            {
                return false;
            }
            string tableid = Settings.dataStructure.FieldIdDic[fieldId].ParentId;
            //For Upload, Allow to upload withouth create any record
            if (tableid == UploadStage.ClassID)
            {
                return true;
            }

            string accessrightId = Settings.dataStructure.TableToFieldDic[tableid].FirstOrDefault(x => x.Value.Name == FieldNames.AccessRight).Value.GetId();
            Dictionary<string, object> searchterm = new Dictionary<string, object>();
            searchterm["id"] = recordid;
            Dictionary<string, List<string>>  accessright = Settings.RequestHandler.AskSync<Dictionary<string, List<string>>>(new GetRecordAccessRight() { TableId = tableid, RecordId = recordid },60);
        
            List<string> roles = new List<string>();
            List<string> accessmode = new List<string>();
            roles.Add(Id.Role.Public);
            if(user.UserGroups != null && user.UserGroups.Count > 0)
            {
                roles.AddRange(user.UserGroups);
            }
            roles.Add(user.id);
        
            foreach (var key in accessright.Keys.Where(x => roles.Contains(x)))
            {
                accessmode.AddRange(accessright[key]);
            }
            return accessmode.Any(x=> accesstype.Contains(x));           
        }

        [HttpPost]
        public JsonResult QueryFiles([FromBody] ApiSelectArgument args, string uploadfieldId = "")
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if(args == null)
            {
                result["NA"] = "args == null";
                return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } ,{"disabled", "disabled" } }).ToList()));              
            }
            try
            {
                Dictionary<string, List<string>> dependentdic = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(args.dependent ?? "{}");            

                if (!dependentdic.ContainsKey("id") || !HasAccess(uploadfieldId, dependentdic["id"].FirstOrDefault(), "View", "Modify") )
                {
                    result["NA"] = "Access Denial";
                    return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value }, { "disabled", "disabled" } }).ToList()));
                }
                else
                {
                    string recordid = dependentdic["id"].FirstOrDefault();
                    foreach (string folder in Settings.ds.UploadedFolders)
                    {
                        string dir = $"{folder}/{uploadfieldId}/{recordid}/".ToFSlashFullPath();
                        if (args.searchbykey)
                        {
                            foreach (string search in args.search.Where(x=> !string.IsNullOrWhiteSpace(x)))
                            {                    
                                if (System.IO.File.Exists($"{dir}{search}"))
                                {
                                    result[search] = search;
                                }
                            }
                        }
                        else
                        {
                            foreach (string search in args.search)
                            {
                                if (Directory.Exists(dir))
                                {
                                    string s = string.IsNullOrWhiteSpace(search) ? "*.*" : $"*{search}*";
                                    string[] files = Directory.GetFiles(dir, s, SearchOption.AllDirectories);
                                    foreach (var f in files)
                                    {
                                        string file = f.ToFSlashFullPath();
                                        int trimindex = file.IndexOf(dir);
                                        string relativeFile = $"{file.Substring(trimindex + dir.Length)}";
                                        result[relativeFile] = relativeFile;
                                    }
                                }
                            }
                        }


                    }
                }
                if(args.searchbykey)
                {
                    return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } ,{"selected","true" } }).ToList()));
                }
                return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList()));
            }
            catch(Exception ex)
            {
                result["error"] = ex.ToString();
                return Json(new JsonModel(result.Select(x => new Dictionary<string, string>() { { "id", x.Key }, { "text", x.Value } }).ToList()));
            }


        }

    }
}