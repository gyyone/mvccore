﻿using Core.Actor;
using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using ComObject;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using Core;
using Core.Akka;
using MVCCore.Controllers;
namespace MVCCore.Actor
{
    public class ServerHandler:ConfirmSendActor,
        IHandle<AddNewLanguageKey>,
        IHandle<UpdateProgressStatus>,
        IHandle<MessageConfig>
    {
        string dbHandlerResourceId { set; get; }

        public ServerHandler(string resourceId):base(resourceId)
        {
            do
            {
                dbHandlerResourceId = Connect(Settings.RequestHandler, resourceId);
            } while (string.IsNullOrWhiteSpace(dbHandlerResourceId));

            ConfirmFireEvent(dbHandlerResourceId,Self,R.Event.WebServerConnect.ToString(), R.Event.WebServerConnect);
   
        }

        public void Handle(AddNewLanguageKey message)
        {
            foreach(var languagekey in message.Language.Keys)
            {    
                foreach(var keyword in message.Language[languagekey].Keys)
                {
                    if(!MVCExt.KeyWords.Languages.ContainsKey(languagekey) )
                    {
                        MVCExt.KeyWords.Languages.Add(languagekey, new Dictionary<string, string>());
                    }
                    MVCExt.KeyWords.Languages[languagekey][keyword] = message.Language[languagekey].ContainsKey(keyword) ? message.Language[languagekey][keyword] : keyword;

                }
            }
        }

        public void Handle(UpdateProgressStatus message)
        {
            if(Settings.ServerHub !=null)
            {
                Settings.ServerHub.Clients.All.SendAsync("UpdateProgressStatus", message.Id,message.Current,message.Total,message.Message);
                
            }
        }

        public void Handle(MessageConfig message)
        {
           
            Settings.ds = message.DataStructure;
            Settings.ds.RebuidStructure();
        }
    }
}