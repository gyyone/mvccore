﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Core;
using Security;
using Util;
using Akka.Actor;
using MVCCore.Actor;
using MVCCore.Controllers;
using Core.Akka;
using LogHelper;
using JsonHelper;

namespace MVCCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
 
            LogHandler.InitLog(null);
            Settings.cfg = JsonIO.Load<Config>(Settings.configpath);
            Settings.cfg.AkkaParam["seed-nodes"] = AkkaHandler.GetClusterSeed(R.SystemName.MainSystem, int.Parse(Settings.cfg.AkkaParam["remoteport"]), Settings.cfg.AkkaParam["enable-ssl"].ToLower() == "true", Settings.cfg.ClusterHostList);
            //var webserverSystem = ActorSystem.Create(R.SystemName.WebServices, AkkaHandler.GetConfig(Settings.cfg.AkkaParam));
            
            var webserverSystem = ActorSystem.Create(R.SystemName.WebServices, AkkaHandler.GetConfig(Settings.cfg.AkkaParam));
            Settings.RequestHandler =
            webserverSystem.ActorSelection(AkkaHandler.GetSelector(R.SystemName.MainSystem, R.ActorName.DataHandlerMasterActor, Settings.cfg.AkkaParam));


            Settings.WebServerHandler = webserverSystem.ActorOf(Props.Create(() => new ServerHandler(Settings.cfg.ResourceId)), R.ActorName.WebServerHandlerActor);
            MVCExt.KeyWords = new KeyWordsDic();
            KeyWordsDic.LanguageActor = Settings.RequestHandler;
            Settings.RequestHandler.Ask<Dictionary<string, Dictionary<string, string>>>(
            new MessageRequest
            {
                RequestType = Core.Akka.Request.LanguageDic
            }).ContinueWith((Task<Dictionary<string, Dictionary<string, string>>> t) =>
            {
                MVCExt.KeyWords.Languages = t.Result;
            }).Wait(10000);
            while(Settings.ds == null || !Settings.ds.UploadedFolders.Any())
            {
                System.Threading.Thread.Sleep(3000);
            }
                JsonIO.Save(Settings.configpath,Settings.cfg);
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseUrls(Settings.cfg.HostUrl)
                .UseStartup<Startup>();
    }
}
