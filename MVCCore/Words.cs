using MVCCore.Actor;
using MVCCore.Controllers;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Core;
using Core.Akka;
using Microsoft.AspNetCore.Http;

public static class Words
{

    public static string Get(string word,string language)
    {
       
        string keyword = KeyWordsDic.GetKey(word);
     
        if (!MVCExt.KeyWords.Languages.ContainsKey(language))
        {
            MVCExt.KeyWords.Languages.Add(language, new Dictionary<string, string>());
        }
        if (!MVCExt.KeyWords.Languages[language].ContainsKey(keyword))
        {
            Dictionary<string, Dictionary<string, string>> newkey = new Dictionary<string, Dictionary<string, string>>();
            newkey.Add(language,new Dictionary<string, string>() { { keyword, word}});
            MVCExt.KeyWords.Languages[language].Add(keyword, word);
            if (keyword.Length >= 30)
            {
                throw new Exception("Key Word Too Long");
            }
            MVCCore.Settings.RequestHandler.Tell(new AddNewLanguageKey() {Language = newkey });
        }
        return MVCExt.KeyWords.Languages[language][keyword];
    }
}