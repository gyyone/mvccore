﻿using System.Threading.Tasks;

namespace MVCCore
{
    public interface IViewRenderService
    {
        string RenderToString(string viewName, object model);
    }
}
