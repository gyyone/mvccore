﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System.Linq;
using System.Collections.Generic;

namespace MVCCore
{
    public class ViewLocationExpander : IViewLocationExpander
    {

        /// <summary>
        /// Used to specify the locations that the view engine should search to 
        /// locate views.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="viewLocations"></param>
        /// <returns></returns>
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            //{2} is area, {1} is controller,{0} is the action
            string[] locations = new string[] {"/Views/{1}/Partials/{0}.cshtml",
            "/Views/Shared/Component/{0}.cshtml",
             "/Views/Account/{0}.cshtml" };
            return locations.Union(viewLocations);          //Add mvc default locations after ours
        }


        public void PopulateValues(ViewLocationExpanderContext context)
        {
            context.Values["customviewlocation"] = nameof(ViewLocationExpander);
        }
    }
    public class ViewRenderService : IViewRenderService
    {
        private readonly IRazorViewEngine _razorViewEngine;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IServiceProvider _serviceProvider;
        private readonly IHttpContextAccessor _httpcontextAccessor;
        //public ViewRenderService(IRazorViewEngine razorViewEngine,
        //    ITempDataProvider tempDataProvider,
        //    IServiceProvider serviceProvider)
        //{
        //    _razorViewEngine = razorViewEngine;
        //    _tempDataProvider = tempDataProvider;
        //    _serviceProvider = serviceProvider;
        //}
        public ViewRenderService(IRazorViewEngine razorViewEngine,
   ITempDataProvider tempDataProvider,
   IServiceProvider serviceProvider, IHttpContextAccessor httpcontextAccessor)
        {
            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
            _httpcontextAccessor = httpcontextAccessor;
        }


        public string RenderToString(string viewName, object model)
        {
            var actionContext = new ActionContext(_httpcontextAccessor.HttpContext, new RouteData(), new ActionDescriptor());
            var viewEngineResult = _razorViewEngine.FindView(actionContext, viewName, false);
            var view = viewEngineResult.View;

            var viewData = new ViewDataDictionary<object>(new EmptyModelMetadataProvider(), new ModelStateDictionary());
            viewData.Model = model;

            var tempData = new TempDataDictionary(_httpcontextAccessor.HttpContext, _tempDataProvider);

            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext(actionContext, view, viewData, tempData, output, new HtmlHelperOptions());
                viewContext.RouteData = _httpcontextAccessor.HttpContext.GetRouteData();   //set route data here

                var task = view.RenderAsync(viewContext);
                task.Wait();

                return output.ToString();
            }
        }
        //public string RenderToString(string viewName, object model)
        //{
        //    var httpContext = _httpcontextAccessor.HttpContext;
        //    var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

        //    using (var sw = new StringWriter())
        //    {
        //        var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

        //        if (viewResult.View == null)
        //        {
        //            throw new ArgumentNullException($"{viewName} does not match any available view");
        //        }

        //        var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
        //        {
        //            Model = model
        //        };

        //        var viewContext = new ViewContext(
        //            actionContext,
        //            viewResult.View,
        //            viewDictionary,
        //            new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
        //            sw,
        //            new HtmlHelperOptions()
        //        );

        //        viewResult.View.RenderAsync(viewContext).Wait();
        //        return sw.ToString();
        //    }
        //}
    }
}
