﻿using Newtonsoft.Json;
using System;
using Core;
using Core.Device;
using Util;
using System.Collections.Generic;

namespace MVCCore
{
   
    public class Config
    {
        public Config()
        {
            ResourceId = Guid.NewGuid().ToString();         
            AkkaParam = AkkaHandler.GetParam(remotehost:"localhost",remoteport:7000,hostname:"0.0.0.0",port:0);    

            HostUrl = "http://localhost:5001/";
            ClusterHostList = new List<string>();
  
        }
        public string ResourceId { get;set; }
        public string HostUrl { set; get; }       
        public string HardwareId { set; get; }   
        public Dictionary<string,string> AkkaParam { get;  set; }
        public List<string> ClusterHostList { get; internal set; }
        public bool EnableWebApi { set; get; }
    }
}
