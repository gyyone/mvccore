using Microsoft.AspNetCore.SignalR;
using MVCCore.Actor;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Core.Akka;

namespace MVCCore
{
    public class ServerHub : Hub
    {
        public void RegisterHub(string userId )
        {
            //Make sure signar Is Working only
            var id = Context.ConnectionId;
        
            //dynamic allClients = context.Clients.All.broadcastMessage("Call By Server", "Test Call broadcast");
            Clients.Client(id).SendAsync("UpdateLoginName",userId,id);
        }

        public void SignalChanged(string userId, List<Dictionary<string, string>> changeValue)
        {
            //Call from other side use global host
           // var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
            Clients.Others.SendAsync("UpdateChanged",userId, changeValue);
        }
      

        public void SendToServer(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            var id = Context.ConnectionId;

            Clients.Client(id).SendAsync("broadcastMessage", name, message);
            //MessageFileProcess temp = new MessageFileProcess("",ProcessType.Delete );          
            //SystemActors.LocalActor.Tell(temp);
        }

        public void SendToClient(DataTable d)
        {
            //var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
            //dynamic allClients = context.Clients.All.broadcastMessage("Call By Server", "Test Call broadcast");
            //context.Clients.Client(id).broadcastMessage("Call By Server", "Server Call Back");
        }

    }
}